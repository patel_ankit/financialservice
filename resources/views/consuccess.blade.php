<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>.btn {
            background: #183b68;
            padding: 10px 15px;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
        }

        .logo {
            background-color: #286db5;
            padding-top: 5px;
        }

        .logo img {
            width: 155px;
            BACKGROUND: #FFF;
            PADDING: 17PX;
        }

        .display-3 {
            color: #183b68;
        }</style>
</head>
<body>
<center>
    <div class="jumbotron text-xs-center">
        <div class="logo"><img src="http://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt=""></div>
        <h1 class="display-3">Thank You!</h1>

        <hr>
        <p>
            Your message successfully sent, we will contact soon...
        </p>
        <BR>

        <p class="lead">
            <a class="btn btn-primary btn-sm" href="{{url('/contacts')}}" role="button">Continue to Contact Us</a>
        </p>
    </div>
</center>

</body>
</html>