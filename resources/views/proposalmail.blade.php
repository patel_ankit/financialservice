<div class="proposal" style="width: 850px;padding: 10px;margin: auto;border: 1px solid #000000;">
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px" style="margin:10px auto;display:block;" class="img-responsive">
    <p style="font-size: 16px;font-weight: 600;margin-top: 10px;text-align:center;">
        <span>4550 Jimmy Carter Blvd.</span>
        <span style="padding: 0px 4px;"> | </span> Norcroos
        <span style="padding: 0px 4px;"> | </span> GA
        <span style="padding: 0px 4px;"> | </span> FAX : +1 (770) 414-5351
    </p>
    <hr style="border-top: 2px solid #8e8e8e;margin-bottom:10px;">
    <h3 style="margin-top: 20px;font-weight: 600;font-size: 26px;text-align: center !important;">Proposal</h3>
    <div class="row" style="margin-top:-60px;">
        <div style="width:50%;float:left;">
            <strong>Proposal To:</strong>
            <p class="lbl_preview_client_name" style="margin: 3px 0px;">{{$client_name}}</p>
            <p class="lbl_preview_address" style="margin: 3px 0px;">{{$client_address}}</p>
            <p class="lbl_preview_city" style="margin: 3px 0px;">{{$city}}</p>
            <p class="lbl_preview_state" style="margin: 3px 0px;">{{$state}}</p>
        </div>
        <div class="col-md-6" style="width:50%;float:left;">
            <h4 id="lbl_proposal_no" style="margin-bottom:0px;text-align:right;margin:0;"><p style="width: 330px; margin: 0; float: left;">Proposal No : </p>
                <p style="float: right; margin: 0; font-weight: 100;">PRO-{{date('y')}}-{{$pid}} </p></h4>
            <h4 id="lbl_proposal_date" style="margin-bottom:0px;text-align:right;margin:0;"><p style="margin-bottom:0px;text-align:right;margin: 5px 0;">Proposal Date : </p>
                <p style="margin-bottom:0px;text-align:right;margin: 5px 0;"> {{$duedate}} </p></h4>
        </div>
    </div>
    <div class="table-responsive">
        <table border="1" style="font-size:14px;color:#333;font-family:Helvetica, Arial, sans-serif;width:100%;margin:auto;border-collapse:collapse;border-spacing:0;margin-bottom:20px;">
            <thead style="background:#ffff99">
            <tr>
                <th style="padding: 5px 8px 5px 8px !important; text-align:center;">Priority</th>
                <th style="padding: 5px 8px 5px 8px !important; text-align:center;">Payment Terms</th>
                <th style="padding: 5px 8px 5px 8px !important; text-align:center;">Sales Rep ID</th>
                <th style="padding: 5px 8px 5px 8px !important; text-align:center;" width="150px">Due Date</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p id="lbl_priority" style="margin: 5px 0px 3px;">{{$priority}}</p></td>
                <td style="text-align:center;padding: 5px 8px 5px 8px !important;">Net Due</td>
                <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p style="margin: 5px 0px 3px;">{{Auth::user()->minss}} {{ Auth::user()->fname }} {{ Auth::user()->mname }} {{ Auth::user()->lname }}</p></td>
                <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p id="lbl_due_date" style="margin: 5px 0px 3px;">{{$duedate}}</p></td>
            </tr>
            </tbody>
        </table>
        <table border="1" style="font-size:14px;color:#333;font-family:Helvetica, Arial, sans-serif;width:100%;margin:auto;border-collapse:collapse;border-spacing:0;">
            <thead style="background:#ffff99">
            <tr>
                <th style="text-align:center !important;padding: 5px 8px 5px 8px !important;">Description</th>
                <th style="padding: 5px 8px 5px 8px !important;text-align:center;" width="150px">Amount</th>
            </tr>
            </thead>
            <tbody>
            <tr style="height:130px;">
                <td style="padding:5px 8px 5px 8px!important;vertical-align: initial;">
                    <div class="lbl_description">{{$description}}</div>
                </td>
                <td style="text-align:right;padding:5px 8px 5px 8px!important;vertical-align: initial;"><p class="_lbl_preview_amount" style="margin:2px 0px 2px;">${{$fsc_fee}}</p></td>
            </tr>
            <tr>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;"><strong>Total Proposal Amount</strong></td>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;"><p id="total_prop_price" style="margin: 7px 0px 5px;">${{$fsc_fee}}</p></td>
            </tr>
            <tr>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><strong>Discount Amount</strong></td>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><p id="lbl_preview_discountbox" style="margin: 5px 0px;">${{$discount}}</p></td>
            </tr>
            <tr>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><strong>Required Advance Payment</strong></td>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><p id="total_prop_advprice" style="margin:5px 0px;">${{$adv_payment}}</p></td>
            </tr>
            <tr>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><strong>Due Balance</strong></td>
                <td style="text-align:right;border-bottom: none !important;padding: 0px 8px 0px;border-top: none;"><p id="total_prop_dueprice" style="margin:5px 0px;">${{$due_bal}}</p></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<a class="btn btn-primary" href="{{$link}}">For Acceptation</a>