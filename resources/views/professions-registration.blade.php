@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Profession Registration</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div">
            @foreach($category as $cat)
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
                    <a href="@if($cat->link == 'comman-registration/create') {{url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])}} @else {{url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])}} @endif"><img src="{{URL::asset('public/category/')}}/{{$cat->business_cat_image}}"/></a>
                    <center>
                        <div class="services-tab"><a href="@if($cat->link == 'comman-registration/create') {{url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])}} @else {{url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])}} @endif"><span><div class="st_title">{{$cat->business_cat_name}}</div></span></a></div>
                    </center>
                </div>
        @endforeach()
        <!--@foreach($category as $cat)
            <form id="logout-form1" action="@if($cat->link == 'comman-registration') {{route('comman-registration.create')}} @else {{url($cat->link,$cat->id)}} @endif" method="get">
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
	<button type="submit" name="submit"><img src="{{URL::asset('category/')}}/{{$cat->business_cat_image}}"/></button>
		<center><div class="services-tab"><button type="submit" name="submit" value="submit"><span><div class="st_title">{{$cat->business_cat_name}}</div></span></button></div></center>
	</div>
	<input type="hidden" value="{{$cat->bussiness_name}}" name="business_id">
		 @if(isset($_GET['user_type']))
                <input type="hidden" value="{{$_GET['user_type']}}" name="user_type">
		 @endif
                    <input type="hidden" value="{{$cat->id}}" name="business_cat_id">
		 <input type="hidden" id="business_brand_id" name="business_brand_id" value="" />
		 <input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="" />
	</form>
	@endforeach()-->


        </div>


    </div>
@endsection()