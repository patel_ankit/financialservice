@extends('front-section.app')
@section('main-content')
    <style>
        .star-required {
            color: red
        }

        .help-block {
            font-size: 14px
        }

        .Libre {
            color: red
        }
    </style>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 fsc-content-head">
                <h4>LOGIN</h4>
            </div>
        </div>
        <h2 style="margin-top:3%; margin-bottom:2%;">Choose any login type :</h2>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs">
                <li><img data-toggle="tab" href="#home" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('public/frontcss/images/login-client.png')}}"/></li>
                <li><img data-toggle="tab" href="#menu1" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('public/frontcss/images/client-employee.png')}}"/></li>
                <li><img data-toggle="tab" href="#menu2" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('public/frontcss/images/login-user.png')}}"/></li>
                <li><img data-toggle="tab" href="#menu3" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('public/frontcss/images/login-vendor.png')}}"/></li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="home" role="tabpanel" class="tab-pane fade in @if( session()->has('error') ) active @endif">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>CLIENT LOGIN</h4>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 fsc-content-box">
                        <form class="form-horizontal" method="POST" id="client-login" action="{{ URL::to('login') }}">
                            <div id="smartwizard register_container">
                                {{ csrf_field() }}
                                <div class="tab-content">
                                    <div id="step-1" role="form">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Client ID : <span class="star-required">*</span></label>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="hidden" placeholder="Enter Your Email Id" class="form-control fsc-input" name="type" id="type" value="1">
                                                    <input type="hidden" placeholder="Enter Your Email Id" class="form-control fsc-input" name="type1" id="type1" value="0">
                                                    <input id="clientid" type="text" placeholder="Enter Your Client Id" class="form-control fsc-input" name="clientid" autofocus>
                                                    <span id="err_client"></span>
                                                    @if ($errors->has('clientid'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('clientid') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">User ID : <span class="star-required">*</span></label>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" autofocus>
                                                    <span id="err_user"></span>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="button" class="btn btn-primary btn-lg fsc-form-submit" id="btn-submit2">Ok</button>
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                        </div>
                                        <center>
                                            <div class="display-error alert alert-danger" style="display:none">Your Client id and User Id not Valid</div>
                                        </center>
                                    </div>
                                    <div id="step-2" style="display:none">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input id="password" type="password" class="form-control fsc-input" value="" name="password" placeholder="Enter Your Password">
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" id="btn-submit21" class="btn btn-primary btn-lg fsc-form-submit">LOGIN</button>
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div id="Register11"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div id="error" style="margin-top:30px;width: 100%;display: inline-block;"></div>
                                    </center>
                                    @if ( session()->has('success') )<br><br>
                                    <div class="alert alert-success alert-dismissable" style="display: inline-block;">{{ session()->get('success') }}</div>
                                    @endif
                                    @if ( session()->has('error') )<br><br>
                                    <div class="alert alert-danger alert-dismissable" style="display: inline-block;">{{ session()->get('error') }}</div>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

            </div>

            <div id="menu1" role="tabpanel" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>CLIENT'S EMPLOYEE LOGIN</h4>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10  fsc-content-box">
                        <form class="form-horizontal" method="post" id="admin-login" action="{{ URL::to('login1') }}">
                            <div id="register_container1">
                                {{ csrf_field() }}
                                <div class="tab-content">
                                    <div id="step-4" role="form">

                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="form-group{{ $errors->has('emp_email') ? ' has-error' : '' }}">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">User ID : <span class="star-required">*</span></label>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" autofocus>
                                                    <span id="err_user1"></span>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input id="password1" type="password" class="form-control fsc-input" name="password" placeholder="Enter Your Password">
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" id="btn-submit3">Ok</button>
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                        </div>
                                        <center>
                                            <div class="display-error alert alert-danger" style="display:none">Your Client id and User Id not Valid</div>
                                        </center>
                                    </div>
                                    <div id="step-3" style="display:none">

                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" id="btn-submit4" class="btn btn-primary btn-lg fsc-form-submit">LOGIN</button>
                                            </div>

                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                            </div>

                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div id="Register11"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div id="error" style="margin-top:30px;width: 100%;display: inline-block;"></div>
                                    </center>
                                    @if ( session()->has('success') )<br><br>
                                    <div class="alert alert-success alert-dismissable" style="display: inline-block;">{{ session()->get('success') }}</div>
                                    @endif
                                    @if ( session()->has('error') )<br><br>
                                    <div class="alert alert-danger alert-dismissable" style="display: inline-block;">{{ session()->get('error') }}</div>
                                    @endif


                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                </div>

            </div>

            <div id="menu2" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>USER LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>
                <form class="form-horizontal" method="POST" action="">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-1 col-sm-1 col-xs-1"></div>

                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">User ID : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="password" type="password" class="form-control fsc-input" name="password" required placeholder="Enter Your Password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                </div>
                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                            </div>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    </div>
                </form>
            </div>

            <div id="menu3" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>VENDOR LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Vendor : <span class="star-required">*</span></label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="email" class="form-control fsc-input" id="" placeholder="Vendor ID">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="email" class="form-control fsc-input" id="" placeholder="Password">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <a href="#" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                            </div>

                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .breadcrumb {
            width: 120px;
            float: right;
            margin-right: 114px;
            background: transparent;
        }

        .breadcrumb li a {
            padding: 0.75rem 1rem !important;
            margin-bottom: 1rem;
            list-style: none;
            background-color: #103b68;
            border-radius: 0.25rem;
            width: 120px;
            text-align: center;
        }

        .breadcrumb li a:hover {
            background-color: #103b68;
        }

        .nav > li > a {
            color: #fff;
            font-size: 15px;
            margin: 0;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script>
        $.ajaxSetup({headers: {'X-CSRF-Token': $('input[name="_token"]').val()}});
        $(document).ready(function () {
            $('#btn-submit2').click(function (e) {
                e.preventDefault(); //alert();
                var data = $("#client-login").serialize();
                var clientid = $("#clientid").val();
                var email = $("#email").val();
                if (clientid == '') {
                    $('#err_client').html('<p style="color:red;font-size:14px">Please Enter Your Client Id</p>');
                } else if (email == '') {
                    $('#err_user').html('<p style="color:red;font-size:14px">Please Enter Your User Id</p>');
                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{ URL::to('login') }}",
                        dataType: "json",
                        data: data,
                        success: function (data) {
                            if (data == "2") {
                                $('#step-2').show();
                                $('#step-1').hide();
                                $('#type1').val('1');
                            } else if (data == "0") {
                                $(".display-error").css("display", "inline-block");
                            } else {
                                $('#step-1').show();
                                $('#step-0').hide();
                            }
                        }
                    });
                }
            });
            /*  $('#btn-submit3').click(function(e){
                    e.preventDefault(); //alert();
                   var data = $("#employeelogin").serialize();
                    var clientid = $("#employeeid").val();
                      var type1 = $("#type1").val();
                    var email = $("#emp_email").val();
            if(clientid=='')
            {
             $('#err_client1').html('<p style="color:red;font-size:14px">Please Enter Your Client Employee Id</p>');
            }
            else if(email=='')
            {
            $('#err_user1').html('<p style="color:red;font-size:14px">Please Enter Your User Id</p>');
            }
            else
            {
                    $.ajax({
                        type: "POST",
                        url: "{{route('fscemployee.login')}}",
            dataType: "json",
            data: data,
            success : function(data){
                if (data== "2"){ //alert();
                    $('#step-3').show();
                     $('#step-4').hide();
                     $('#type2').val('1');
                } else if(data== "0"){
                    $(".display-error").css("display","inline-block");
                }
                else {
                     $('#step-4').show();
                     $('#step-5').hide();
                }
            }
        });
}

      });*/
        });

    </script>
    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
$('#admin-login').bootstrapValidator({
    message: 'This value is not valid',
    live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    submitHandler: function(validator, form, submitButton) {
        $.ajax({
          type: "POST",
          url: "",
          data: $('#admin-login').serialize(),
          success: function(msg){            
			  $('#Register').html('<span class="alert alert-success">You Are SuccessFully Login.</span>');
			  setTimeout(function(){ location.href="{{ URL::to('/clientemployee/home') }}"; }, 0);
          },
          error: function(){
            $('#Register').html('<span class="alert alert-danger">Your Username And Password Invalid.</span>');
          }
        });//close ajax
    },
    fields: {
        email: {              
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Email.'
                },
				emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					}					
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Password.'
                }
            }
        },
    } // end field
});
	</script>-->
@endsection