@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Brand or Non Brand Registration</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div" style="margin-top: 8%;">
            @foreach($businessbrand as $business)
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
                    <a href="@if($business->link == 'comman-registration/create'){{url($business->link,[Request::segment(3),Request::segment(4),Request::segment(2),$business->id])}} @else {{url($business->link,[$business->id,Request::segment(3),Request::segment(2),Request::segment(4)])}} @endif"><img src="{{url('public/businessbrand')}}/{{$business->business_brand_image}}"></a>

                    <center>
                        <div class="services-tab"><a href="@if($business->link == 'comman-registration/create') {{url($business->link,[Request::segment(3),Request::segment(4),Request::segment(2),$business->id])}} @else {{url($business->link,[$business->id,Request::segment(3),Request::segment(2),Request::segment(4)])}} @endif"><span><div class="st_title">{{$business->business_brand_name}}</div></span></a></div>
                    </center>
                </div>
        @endforeach
        <!--@foreach($businessbrand as $business)
            <form id="logout-form1" action="@if($business->link == 'comman-registration') {{route('comman-registration.create')}} @else {{url($business->link,$business->id)}} @endif" method="get">
	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
		
	<center><div class="services-tab"><button type="submit" name="submit" value="submit"><span><div class="st_title">{{$business->business_brand_name}}</div></span></button></div></center>
	</div>
	@if(isset($_GET['user_type']))
                <input type="hidden" value="{{$_GET['user_type']}}" name="user_type">
	@endif
                    <input type="hidden" value="{{$business->business_id}}" name="business_id">
	<input type="hidden" value="{{$business->business_cat_id}}" name="business_cat_id">
	<input type="hidden" id="business_brand_id" name="business_brand_id" value="{{$business->id}}" />
	<input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="" />
	</form>
	@endforeach -->
        </div>
    </div>
@endsection()