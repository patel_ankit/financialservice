<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>FSC</title>
</head>
<body>

<img src="http://financialservicecenter.net/public/frontcss/images/fsc_logo.png" alt="img">
<br><br>
<table border='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;'>
    <tr style='background:#535da6;'>
        <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Thank You for Applying, <br>We're currently in the process of taking applications for this position.If you are selected to continue to the interview process, our human resources department will be in contact with you</td>
    </tr>


    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Fullname :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'>

            <p>
                {{ $firstname }}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Email :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>

            <p>
                {{ $email }}
            </p>
        </td>
    </tr>
</table>


</body>
</html>