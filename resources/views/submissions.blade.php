@extends('front-section.app')
@section('main-content')
    <!--<script type="text/javascript" src="https://financialservicecenter.net/public/frontcss/js/jquery.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

    <style>
        label.error {
            color: red;
        }
    </style>
    <script>


    </script>
    <script>
        $(document).ready(function () {

            //$(".telephone").mask("(999) 999-9999");
            $("#submitform").validate({
                rules: {

                    name: {
                        required: true,
                    },
                    requestform: {
                        required: true,

                    },
                    note: {
                        required: true,

                    },
                    email: {
                        required: true,
                        email: true
                    }

                },


                messages: {


                    email: {
                        required: "Please Enter Your Email Address",
                        email: "Please Enter A Valid Email Address"

                    },
                    name: {
                        required: "Please Enter Name"
                    },
                    requestform: {
                        required: "Please Select Your Request Form"
                        //email: "Please Enter A Valid Email Address",
                    },
                    note: {
                        required: "Please Enter Note",
                        //email: "Please Enter A Valid Email Address",
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },


                // if(count !='3')
                //{
                //  alert('Please Enter 3 character');
                //}
            });
        });
    </script>
    <script>
        $(document).ready(function () {

            $(".telephone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("(" + curval + ")" + " ");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                    $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 9) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });

        });
    </script>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable" id="successMessage">{{session()->get('success') }}</div>
        @endif
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>SUBMISSION</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
            <div class="sub">


                @foreach($submission as $sub)
                    <?php //echo "<pre>";print_r($sub);?>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
                        <a data-toggle="modal" data-target="#myModalbutton_<?php echo $sub->id;?>" href="#"><img style="cursor:pointer;" class="img-responsive" src="{{URL::asset('public/submission/')}}/{{$sub->submission_image}}"/></a>
                    </div>
                    <div id="myModalbutton_<?php echo $sub->id;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h4>Request Form</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <form action="{{url('submissions/store')}}" method="post" id="submitform">
                                    {{csrf_field()}}
                                    <div class="modal-body">
                                        <div class="modalform">
                                            <div class="col-md-12 margintop">
                                                <div class="col-md-4 labels">Name</div>
                                                <div class="col-md-8"><input type="text" name="name" class="form-control" required/></div>
                                                <input type="hidden" name="id" value="<?php echo $sub->id;?>">
                                            </div>
                                            <div class="col-md-12 margintop">
                                                <div class="col-md-4 labels">Telephone No.</div>
                                                <div class="col-md-8"><input type="tel" name="telephone" class="form-control telephone" required/></div>
                                            </div>
                                            <div class="col-md-12 margintop">
                                                <div class="col-md-4 labels">Email</div>
                                                <div class="col-md-8"><input type="email" name="email" class="form-control" required/></div>
                                            </div>

                                            <div class="col-md-12 margintop">
                                                <div class="col-md-4 labels">Request Form</div>
                                                <div class="col-md-8"><input type="text" name="requestform" class="form-control" required/></div>
                                            </div>
                                            <div class="col-md-12 margintop">
                                                <div class="col-md-4 labels">Note</div>
                                                <div class="col-md-8"><textarea cols="100" rows="4" name="note" class="form-control" required/></textarea></div>
                                            </div>


                                            <div class="modalform buttonbox ">
                                                <input type="submit" name="submit" class="btn btn-primary margintop" value="Submit"/>
                                                <a href="{{$sub->link}}/?subid={{$sub->id}}" class="btn btn-danger margintop"/>Cancel</a>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection()


<script>
    $(function () {
        setTimeout(function () {
            $('#successMessage').fadeOut('fast');
        }, 5000);

    });
</script>

<style>
    .modal-backdrop.fade {
        opacity: 0.5 !important;
    }

    .modal-header, .modal-body, .modal-footer {
        width: 100% !important;
    }

    .modal-header h4 {
        width: 100%;
    }

    #myModalbutton {
        top: 20%;
    }

    #myModalbutton label {
        font-size: 15px;
        width: 150px;
    }

    #myModalbutton .form-group {
        width: 100% !important;
    }

    .modal-header {
        margin-top: 55px;
        font-size: 20px;
    }

    .modalform {
        margin-bottom: 15px;
    }

    .modalform label {
        font-size: 13px;
    }

    #myModalbutton .btn {
        font-size: 18px !important;
        margin: 0px 5px;
    }

    .buttonbox {
        text-align: right;
        flex-direction: row;
        align-items: flex-end;
        justify-content: flex-end;
    }

    .labels {
        font-size: 12px;
        font-weight: bold;
    }

    .margintop {
        margin-top: 20px;
    }

    .buttonbox a {
        font-size: 14px;
    }

    .buttonbox input {
        font-size: 14px;
    }

    .modal-header h4 {
        font-size: 18px !important;
    }
</style>

<!-- Modal -->