<!DOCTYPE html>

<html>

<head>

    <title>Navigation</title>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="http://malsup.github.com/jquery.form.js"></script>

</head>

<body>


<div class="container">

    <h1>Navigation</h1>


    <form action="{{route('navigation') }}" method="POST">

        <div class="alert alert-danger print-error-msg" style="display:none">

            <ul></ul>

        </div>


        <input type="hidden" name="_token" value="{{ csrf_token() }}">


        <div class="form-group">

            <label>Menu:</label>

            <input type="text" name="main_menu" class="form-control" placeholder="Add Menu">

        </div>
        <div class="form-group">

            <label>Slag:</label>

            <input name="slag" class="form-control" placeholder="Add Slag" type="text">
        </div>

        <div class="form-group">
            <input name="status" class="form-control" placeholder="Add content" value="0" type="hidden">
            <button class="btn btn-success upload-image" type="submit">Upload Menu</button>

        </div>


    </form>


</div>


<script type="text/javascript">

    $("body").on("click", ".upload-image", function (e) {

        $(this).parents("form").ajaxForm(options);

    });


    var options = {

        complete: function (response) {

            if ($.isEmptyObject(response.responseJSON.error)) {
                $("input[name='main_menu']").val('');
                $("input[name='slag']").val('');
                $("input[name='stauts']").val('');
                alert('menu Upload Successfully.');

            } else {

                printErrorMsg(response.responseJSON.error);

            }

        }

    };


    function printErrorMsg(msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display', 'block');

        $.each(msg, function (key, value) {

            $(".print-error-msg").find("ul").append('<li>' + value + '</li>');

        });

    }

</script>


</body>

</html>