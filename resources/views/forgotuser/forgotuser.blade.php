@extends('front-section.app')
@section('main-content')
    <br>
    <br><br>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Forgot Username</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissable">{{session()->get('error') }}</div>
            @endif
            <form class="form-horizontal" method="POST" action="{{ route('forgotuser.store') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>


                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Send Username Reset Link
                        </button>
                    </div>

                </div>
            </form>


        </div>
    </div>

@endsection