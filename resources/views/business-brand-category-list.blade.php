@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Brand List</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div" style="margin-top: 8%;">
            @foreach($category1 as $busi)
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
                    <a href="@if($busi->link == 'comman-registration/create') {{url($busi->link,[Request::segment(3),Request::segment(5),Request::segment(4),Request::segment(2),$busi->id])}} @else {{url($busi->link,[$busi->id,Request::segment(3),Request::segment(2),Request::segment(4),Request::segment(5)])}} @endif"><img style="cursor:pointer;" class="img-responsive" src="{{url('public/businessbrandcategory')}}/{{$busi->business_brand_category_image}}"/></a>
                    <div class="services-tab"><a href="@if($busi->link == 'comman-registration/create') {{url($busi->link,[Request::segment(3),Request::segment(5),Request::segment(4),Request::segment(2),$busi->id])}} @else {{url($busi->link,[$busi->id,Request::segment(3),Request::segment(2),Request::segment(4),Request::segment(5)])}} @endif"><span><div class="st_title">{{$busi->business_brand_category_name}}</div></span></a></div>
                </div>
        @endforeach()
        <!--@foreach($category1 as $busi)
            <form id="logout-form1" action="@if($busi->link == 'comman-registration') {{route('comman-registration.create')}} @else {{url($busi->link,$busi->id)}} @endif" method="get">
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	<button type="submit" name="submit" value="submit"><img style="cursor:pointer;" class="img-responsive" src="{{url('businessbrandcategory')}}/{{$busi->business_brand_category_image}}"/></button>
	<center><div class="services-tab"><button type="submit" name="submit" value="submit"><span><div class="st_title">{{$busi->business_brand_category_name}}</div></span></button></div></center>
    </div>
	@if(isset($_GET['user_type']))
                <input type="hidden" value="{{$_GET['user_type']}}" name="user_type">
	@endif
                    <input type="hidden" value="{{$busi->business_id}}" name="business_id">
	<input type="hidden" value="{{$busi->business_cat_id}}" name="business_cat_id">
	<input type="hidden" id="business_brand_id" name="business_brand_id" value="{{$busi->business_brand_id}}" />
	<input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="{{$busi->id}}"" />
	</form>
     @endforeach() -->
        </div>
    </div>
@endsection()