@extends('front-section.app')
@section('main-content')
    <style>
        .Branch {
            width: 100%;
            float: left;
            margin: 10px 0 20px 0;
            text-align: center;
            background: #fff2b3;
            border: 2px solid #103b68;
            padding: 4px 0;
        }

        .Branch h1 {
            font-size: 2rem;
        }

        .title-form-group #checkBox {
            margin-bottom: 13px;
        }

        .star-reqire {
            color: #ff0000;
        }

        .error {
            font-size: 16px;
            color: #ff0000;
            font-weight: normal;
        }

        .not-reqire {
            color: transparent;
        }

        .pager li > a:focus, .pager li > a:hover {
            text-decoration: none;
            background-color: #3b5998;
        }

        .ag {
            position: relative;
            top: -25px;
            font-size: 13px;
            left: -100px;
        }

        .green-border {
            background-color: #003b6d !important;
        }

        .fsc-apply-btn {
            padding: 5px 33px !important;
            margin-top: 0 !important;
            margin-right: 0 !important;
            font-size: 21px !important;
        }

        .tess {
            text-align: left;
            float: right;
            font-size: 11px;
            margin-right: 20px;
        }

        .star-required1 {
            color: #f5f5f5;
        }

        .fsc-reg-sub-header {
            padding-left: 0;
        }

        .pager li > a, .pager li > span {
            width: 110px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            color: #fff;
            background-color: green;
        }

        .bread {
        }

        ul.nav.nav-tabs.tabs-lists {
            background: #e6f9ff;
        }

        .nav-tabs > li > a {
            margin-right: 0px !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #fff !important;
            cursor: pointer;
            background-color: #59aaff !important;
        }

        #form-step-2 .nav > li > a:focus, #form-step-2 .nav > li > a:hover {
            background-color: #c6e2ff !important;
        }

        i.fa.fa-chevron-left {
            float: left;
            margin-top: 5px;
            margin-right: 10px;
        }

        i.fa.fa-chevron-right {
            float: left;
            margin-top: 5px;
            margin-left: 10px;
        }

        .title-form-group p {
            font-size: 12px
        }

        li.next {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .check-box-fsc {
            padding-left: 2.25rem;
            margin-bottom: 0;
            cursor: pointer;
            margin: 0 10px;
            font-size: 12px;
        }

        .nav-pills > li > a {
            border-radius: 0 !important;
        }

        li.previous {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .fsc-reg-sub-header-div {
            border-radius: 0;
            padding: 6px 4%;
            border-top: 2px solid #fff;
            border-bottom: 2px solid #fff;
        }

        .breadcrumb {
            padding: 0px;
            background: #D4D4D4;
            list-style: none;
            overflow: hidden;
            margin-top: -20px;
        }

        .breadcrumb > li + li:before {
            padding: 0;
        }

        .breadcrumb li {
            float: left;
            width: 100%;
            text-align: center;
            font-size: 15px;
        }

        .chck-box-fscc {
            margin: 10px 20px !important;
        }

        .breadcrumb > li.ddd.active1 a {
            background: green !important;
        }

        .breadcrumb li.active a {
            background: brown;
            background: green;
        }

        .breadcrumb li.completed a {
            background: brown;
            background: hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li.active a:after {
            border-left: 30px solid green;
        }

        .breadcrumb li.completed a:after {
            border-left: 30px solid hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li a {
            width: 95%;
            color: white;
            text-decoration: none;
            padding: 10px 0 10px 0px;
            position: relative;
            display: inline-block;
        }

        /*.breadcrumb li a:after {  content: " ";  display: block;  width: 0;  height: 0; border-top: 50px solid transparent; border-bottom: 50px solid transparent; border-left: 30px solid hsla(0, 0%, 83%, 1); position: absolute; top: 50%; margin-top: -50px;  left: 100%; z-index: 2; }
        .breadcrumb li a:before { content: " "; display: block; width: 0; height: 0; border-top: 50px solid transparent; border-bottom: 50px solid transparent; border-left: 30px solid white; position: absolute; top: 50%; margin-top: -50px; margin-left: 1px; left: 100%; z-index: 1; }
        */
        .breadcrumb li:first-child a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .breadcrumb li:nth-o a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .pager .disabled > a, .pager .disabled > a:focus, .pager .disabled > a:hover, .pager .disabled > span {
            color: #fff;
            cursor: not-allowed;
            background-color: #428bca;
            font-size: 14px;
        }

        .pager li > a, .pager li > span {
            display: inline-block;
            padding: 5px 14px;
            background-color: #0472d0;
            border: 1px solid #ddd;
            border-radius: 15px;
            font-size: 14px;
            color: #fff;
        }

        .breadcrumb li a:hover {
            background: green !important;
        }

        .breadcrumb li a:hover:after {
            border-left-color: green !important;
        }

        .form-control-insu {
            border: 2px solid #286db5;
            border-radius: 3px;
            font-weight: bold;
        }

        .share_tabs_main {
            float: left;
            width: 100%;
            margin-bottom: 0px;
        }

        .share_tabs {
            float: left;
            width: 100%;
        }

        .share_tabs_other {
            float: left;
            width: 100%;
        }

        .share_tab {
            float: left;
            margin: 10px 1%;
        }

        .share_firstn {
            width: 34%;
        }

        .share_m {
            width: 14%;
        }

        .share_lastn {
            width: 34%;
        }

        .share_position {
            width: 26%;
        }

        .share_persentage {
            width: 14%;
        }

        .share_date {
            width: 15%;
        }

        .share_add {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_remove {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_total {
            font-size: 12px;
            line-height: 30px;
            text-align: right;
            width: 100%;
        }

        .addbtn {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #359e0b;
            border-radius: 2px;
            vertical-align: top;
        }

        .removebtn, .remove {
            display: block;
            width: 40px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #d9534f;
            border-radius: 2px;
            vertical-align: top;
            float: right;
            position: relative;
            top: -41px;
            right: 210px;
        }

        .share_tab input {
            font-size: 14px;
        }

        .tabs-lists {
            width: 100%;
        }

        .tabs-lists > li {
            width: 50%;
            text-align: center;
        }

        .tabs-lists > li > a {
            position: relative;
            display: block;
            padding: 10px 0;
            font-size: 18px;
        }

        .nav-tabs > li {
            margin-bottom: 0px !important;
            border: 1px solid #1265bd;
        }

        .fsc-form-label {
            font-family: 'Proza Libre', serif;
            font-size: 1.26em;
            margin-top: 4%;
            white-space: nowrap;
        }

        .tab-content .col-lg-12 {
            padding-left: 0px ! improtant;
        }

        .tab-content .col-lg-12 .col-lg-3:first-child {
            padding-left: 0px !important;
        }

        .tab-content .col-lg-12 .col-lg-9 .col-lg-3:first-child {
            padding-left: 15px !important;
        }
    </style>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4> {{Request::segment(4)}}</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head"></div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
                <form enctype="multipart/form-data" class="changepassword" action="{{route('applyservices.store')}}" id="registrationForm" method="post">
                    {{csrf_field()}}
                    <div id="smartwizard">
                        <ul class="breadcrumb nav nav-pills">
                            <li class="active"><a href="#step-1" data-toggle="tab">Step-1</a></li>
                            <li class="ddd"><a href="#step-2" data-toggle="tab">Step-2</a></li>
                            <li><a href="#step-3" data-toggle="tab">Step-3</a></li>
                        </ul>
                        <br>
                        <div class="tab-content">
                            <div class="tab-pane active" data-step="0" id="step-1">
                                <div id="form-step-0" role="form" data-toggle="validator">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label" for="typeofservice">Type of Service : <span class="star-reqire">*</span></label>
                                            </div>
                                            <input value="{{Request::segment(3)}}" type="hidden" class="form-control fsc-input" id="serviceid" name="serviceid" placeholder="Rreservation">
                                            <input value="{{uniqid()}}" type="hidden" class="form-control fsc-input" id="unique" name="unique" placeholder="Rreservation">
                                            <input value="@if(Request::segment(4)=='Accounting Bookkeeping & Taxation Service') Accounting Bookkeeping Taxation Service @else {{Request::segment(4)}} @endif" type="hidden" class="form-control fsc-input" id="typeofcorp" name="typeofcorp" placeholder="Rreservation">
                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select type="text" class="form-control fsc-input" name="typeofservice" id="typeofservice" placeholder="Enter Your Company Name">
                                                    <option value="">---Select---</option>
                                                    @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service')
                                                        <option value="Bookkeeping Service">Bookkeeping Service</option>
                                                        <option value="Payroll Service">Payroll Service</option>
                                                        <option value="Taxation Service">Taxation Service</option>
                                                        <option value="I Do Not Know">I Do Not Know</option>
                                                        <option value="Please Recommend">Please Recommend</option>
                                                    @else
                                                    @endif
                                                    @if(Request::segment(4)=='Residential Mortgage Services')
                                                        <option value="Home Purchase Loan">Home Purchase Loan</option>
                                                        <option value="Refinance">Refinance</option>
                                                        <option value="Home Equity Line">Home Equity Line</option>
                                                    @endif
                                                    @if(Request::segment(4)=='Commercial Mortgage Services')
                                                        <option value="Business Purchase">Business Purchase</option>
                                                        <option value="Refinance">Refinance Business Loan</option>
                                                        <option value="SBA Loan">SBA Loan</option>
                                                        <option value="Convential Loan">Convential Loan</option>
                                                        <option value="Hard Money Loan">Hard Money Loan</option>
                                                        <option value="Business Credit Line">Business Credit Line</option>
                                                        <option value="Construction Loan">Construction Loan</option>
                                                        <option value="Hotel / Motel Loan">Hotel / Motel Loan</option>
                                                        <option value="Franchise Business Loan">Franchise Business Loan</option>
                                                        <option value="Restaurant Loan">Restaurant Loan</option>
                                                        <option value="Land Loan">Land Loan</option>
                                                        <option value="Equipment Lease / Loan">Equipment Lease / Loan</option>
                                                        <option value="Church Loan">Church Loan</option>
                                                        <option value="Taxation Service">Under $ 50,000 Loan</option>
                                                        <option value="Taxation Service">Above $ 50,000 Loan</option>
                                                    @endif
                                                    @if(Request::segment(4)=='Financial Services')
                                                        <option value="Form a New Corporation / LLC">Form a New Corporation / LLC - Services</option>
                                                        <option value="Licenses">Licenses</option>
                                                    @endif
                                                    @if(Request::segment(4)=='Insurance Service')
                                                        <option value="Health Insurance-Individual">Health Insurance-Individual</option>
                                                        <option value="Health Insurance-Group">Health Insurance-Group</option>
                                                        <option value="Visitor Medical Insurance">Visitor Medical Insurance</option>
                                                        <option value="Life Insurance">Life Insurance</option>
                                                        <option value="Annuity">Annuity</option>
                                                        <option value="Other products of Medical & Life Insurance">Other products of Medical & Life Insurance</option>
                                                    @endif
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(Request::segment(4)=='Residential Mortgage Services')
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                                    <h3 class="Libre fsc-reg-sub-header">Basic Information :</h3>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Person Name : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <select class="form-control fsc-input" id="nametype" name="nametype">
                                                                    <option value="mr">Mr.</option>
                                                                    <option value="mrs">Mrs.</option>
                                                                    <option value="miss">Miss.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input textonly" id="fname" name="fname" placeholder="First">
                                                            </div>
                                                            <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <div class="row">
                                                                    <input type="text" class="form-control fsc-input textonly" id="mname" name="mname" placeholder="M">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input textonly" id="lname" name="lname" placeholder="Last">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="address" name="address" placeholder="Address">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <select name="state" id="state" class="form-control fsc-input">
                                                                <option value="GA">GA</option>
                                                                <option value="AK">AK</option>
                                                                <option value="AS">AS</option>
                                                                <option value="AZ">AZ</option>
                                                                <option value="AR">AR</option>
                                                                <option value="CA">CA</option>
                                                                <option value="CO">CO</option>
                                                                <option value="CT">CT</option>
                                                                <option value="DE">DE</option>
                                                                <option value="DC">DC</option>
                                                                <option value="FM">FM</option>
                                                                <option value="FL">FL</option>
                                                                <option value="GU">GU</option>
                                                                <option value="HI">HI</option>
                                                                <option value="ID">ID</option>
                                                                <option value="IL">IL</option>
                                                                <option value="IN">IN</option>
                                                                <option value="IA">IA</option>
                                                                <option value="KS">KS</option>
                                                                <option value="KY">KY</option>
                                                                <option value="LA">LA</option>
                                                                <option value="ME">ME</option>
                                                                <option value="MH">MH</option>
                                                                <option value="MD">MD</option>
                                                                <option value="MA">MA</option>
                                                                <option value="MI">MI</option>
                                                                <option value="MN">MN</option>
                                                                <option value="MS">MS</option>
                                                                <option value="MO">MO</option>
                                                                <option value="MT">MT</option>
                                                                <option value="NE">NE</option>
                                                                <option value="NV">NV</option>
                                                                <option value="NH">NH</option>
                                                                <option value="NJ">NJ</option>
                                                                <option value="NM">NM</option>
                                                                <option value="NY">NY</option>
                                                                <option value="NC">NC</option>
                                                                <option value="ND">ND</option>
                                                                <option value="MP">MP</option>
                                                                <option value="OH">OH</option>
                                                                <option value="OK">OK</option>
                                                                <option value="OR">OR</option>
                                                                <option value="PW">PW</option>
                                                                <option value="PA">PA</option>
                                                                <option value="PR">PR</option>
                                                                <option value="RI">RI</option>
                                                                <option value="SC">SC</option>
                                                                <option value="SD">SD</option>
                                                                <option value="TN">TN</option>
                                                                <option value="TX">TX</option>
                                                                <option value="UT">UT</option>
                                                                <option value="VT">VT</option>
                                                                <option value="VI">VI</option>
                                                                <option value="VA">VA</option>
                                                                <option value="WA">WA</option>
                                                                <option value="WV">WV</option>
                                                                <option value="WI">WI</option>
                                                                <option value="WY">WY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input zip" id="zip" name="zip" placeholder="Zipcode">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone # : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input phone" placeholder=" (999) 999-9999" id="phonenumber" name="phonenumber">
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="dropdown">
                                                            <select name="phonetype" id="phonetype" class="form-control fsc-input">
                                                                <option value="">Type</option>
                                                                <option value="Mobile">Mobile</option>
                                                                <option value="Home">Home</option>
                                                                <option value="Work">Work</option>
                                                                <option value="Work">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input zip" id="ext" name="ext" readonly placeholder="Ext.">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Website : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="url" class="form-control fsc-input" id="website" name="website" placeholder="Website">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Email : <span class="not-reqire">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="email" name="email" placeholder="Email">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if((Request::segment(4)=='Accounting Bookkeeping and Taxation Service') or (Request::segment(4)=='Commercial Mortgage Services') or (Request::segment(4)=='Insurance Service'))
                                        <div class="form-group">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label" for=""> Business : <span class="not-reqire">*</span> </label>
                                                </div>
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label class="fsc-form-label" for="newbusiness"><input type="radio" class="fsc-input" value="New Business" id="newbusiness" name="newbusiness1"> New Business</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="fsc-form-label" for="newbusiness1"><input type="radio" class="fsc-input" value="Already Existance" id="newbusiness1" name="newbusiness1"> Already Existance</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(Request::segment(4)=='Financial Services')
                                        <div class="form-group checks1" style="display:none;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 fsc-form-row"></div>
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <select type="text" class="form-control fsc-input" name="typeofservice1" id="typeofservice1" placeholder="Enter Your Company Name">
                                                        <option value="">---Select---</option>
                                                        <option value="Create New Corporation / LLC" style="display:none;" class="checkm"> Create New Corporation / LLC</option>
                                                        <option value="Transfer of Share / Membership" style="display:none;" class="checkm">Transfer of Share / Membership</option>
                                                        <option value="New License Service" style="display:none;" class="checkl">New License Service</option>
                                                        <option value="Renew Licenser Service" style="display:none;" class="checkl">Renew Licenser Service</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 checks" style="display:none;"></div>
                                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 checks" style="display:none;">
                                            <div class="row">
                                                <div class="chck-box-fscc">
                                                    <input type="checkbox" class="form-check-input" value="Bear/Wine Licenses" id="exampleCheck1" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck1">Bear/Wine Licenses</label>
                                                    <input type="checkbox" class="form-check-input" value="EBT Licenses" id="exampleCheck2" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck2">EBT Licenses</label>
                                                    <input type="checkbox" class="form-check-input" value="Lotto Licenses" id="exampleCheck3" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck3">Lotto Licenses</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12  checks" style="display:none;"></div>
                                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12  checks" style="display:none;">
                                            <div class="row">
                                                <div class="chck-box-fscc">
                                                    <input type="checkbox" class="form-check-input" value="Tobacco Licenses" id="exampleCheck4" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck4">Tobacco Licenses</label>
                                                    <input type="checkbox" class="form-check-input" value="COAM(Amusement Machine)" id="exampleCheck5" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck5">COAM(Amusement Machine)</label>
                                                    <input type="checkbox" class="form-check-input" value="Other Licences" id="exampleCheck6" name="linense_type[]">
                                                    <label class="form-check-label check-box-fsc" for="exampleCheck6">Other Licences</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label"> Business : <span class="not-reqire">*</span> </label>
                                            </div>
                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="fsc-form-label" for="newbusiness"><input type="radio" class="fsc-input" onclick="show1();" value="Domestic" id="newbusiness" name="newbusiness1"> Domestic </label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="fsc-form-label" for="newbusiness1"><input type="radio" class="fsc-input" onclick="show2();" value="Foreign Company" id="newbusiness1" name="newbusiness1"> Foreign Company</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%; display:none" style="" id="ddd">
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Domestic Business : <span class="not-reqire">*</span> </label>
                                            </div>
                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select type="text" class="form-control fsc-input" name="domestic_business" id="domestic_business" placeholder="Enter Your Company Name">
                                                    <option value="">---Select Domestic---</option>
                                                    <option value="Domestic Profit Corporation">Domestic Profit Corporation</option>
                                                    <option value="Domestic Nonprofit Corporation">Domestic Nonprofit Corporation</option>
                                                    <option value="Domestic Limited Liability Corporation">Domestic Limited Liability Corporation</option>
                                                    <option value="Domestic Limited Liability Partenership">Domestic Limited Liability Partenership</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;display:none" id="dddd">
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Foreign Business : <span class="not-reqire">*</span> </label>
                                            </div>
                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select type="text" class="form-control fsc-input" name="foreign_business" id="foreign_business" placeholder="Enter Your Company Name">
                                                    <option value="">---Select Foreign Compnay---</option>
                                                    <option value="Foreign Profit Corporation">Foreign Profit Corporation</option>
                                                    <option value="Foreign Non-Profit Corporation">Foreign Non-Profit Corporation</option>
                                                    <option value="Foreign Limited Liability Corporation">Foreign Limited Liability Corporation</option>
                                                    <option value="Foreign Limited Liability Partenership">Foreign Limited Liability Partenership</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service' or (Request::segment(4)=='Commercial Mortgage Services') or (Request::segment(4)=='Financial Services') or (Request::segment(4)=='Insurance Service'))
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                                    <h3 class="Libre fsc-reg-sub-header">Company Information :</h3>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Company Legal Name @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service')
                                                                    @else 1 @endif: <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="company_name" name="company_name" placeholder="Company Legal Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service')
                                                @else
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                    <label class="fsc-form-label">Company Legal Name 2 : <span class="not-reqire">*</span></label>
                                                                </div>
                                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="company_name_1" name="company_name_1" placeholder="Company Legal Name">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                    <label class="fsc-form-label">Company Legal Name 3 : <span class="not-reqire">*</span></label>
                                                                </div>
                                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="company_name_2" name="company_name_2" placeholder="Company Legal Name">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Name (DBA) : <span class="not-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="business_name" name="business_name" placeholder="Business Name (DBA)">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service' or (Request::segment(4)=='Commercial Mortgage Services') or (Request::segment(4)=='Financial Services') or (Request::segment(4)=='Insurance Service'))
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                                    <h3 class="Libre fsc-reg-sub-header">Business Information :</h3>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <select class="form-control fsc-input">
                                                                    <option>Select</option>
                                                                    <option>Any legal purpose</option>
                                                                    <option>Unknown</option>
                                                                </select>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Type of Business : </label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="type_of_business" name="type_of_business" placeholder="Type of Business">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Register Business Address : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="address" name="address" placeholder="Business Location Address">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">City / State / Zip : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control  fsc-input textonly" id="city" name="city" placeholder="City">
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <select name="state" id="state" class="form-control fsc-input">
                                                                    <option value="GA">GA</option>
                                                                    <option value="AK">AK</option>
                                                                    <option value="AS">AS</option>
                                                                    <option value="AZ">AZ</option>
                                                                    <option value="AR">AR</option>
                                                                    <option value="CA">CA</option>
                                                                    <option value="CO">CO</option>
                                                                    <option value="CT">CT</option>
                                                                    <option value="DE">DE</option>
                                                                    <option value="DC">DC</option>
                                                                    <option value="FM">FM</option>
                                                                    <option value="FL">FL</option>
                                                                    <option value="GA">GA</option>
                                                                    <option value="GU">GU</option>
                                                                    <option value="HI">HI</option>
                                                                    <option value="ID">ID</option>
                                                                    <option value="IL">IL</option>
                                                                    <option value="IN">IN</option>
                                                                    <option value="IA">IA</option>
                                                                    <option value="KS">KS</option>
                                                                    <option value="KY">KY</option>
                                                                    <option value="LA">LA</option>
                                                                    <option value="ME">ME</option>
                                                                    <option value="MH">MH</option>
                                                                    <option value="MD">MD</option>
                                                                    <option value="MA">MA</option>
                                                                    <option value="MI">MI</option>
                                                                    <option value="MN">MN</option>
                                                                    <option value="MS">MS</option>
                                                                    <option value="MO">MO</option>
                                                                    <option value="MT">MT</option>
                                                                    <option value="NE">NE</option>
                                                                    <option value="NV">NV</option>
                                                                    <option value="NH">NH</option>
                                                                    <option value="NJ">NJ</option>
                                                                    <option value="NM">NM</option>
                                                                    <option value="NY">NY</option>
                                                                    <option value="NC">NC</option>
                                                                    <option value="ND">ND</option>
                                                                    <option value="MP">MP</option>
                                                                    <option value="OH">OH</option>
                                                                    <option value="OK">OK</option>
                                                                    <option value="OR">OR</option>
                                                                    <option value="PW">PW</option>
                                                                    <option value="PA">PA</option>
                                                                    <option value="PR">PR</option>
                                                                    <option value="RI">RI</option>
                                                                    <option value="SC">SC</option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="TN">TN</option>
                                                                    <option value="TX">TX</option>
                                                                    <option value="UT">UT</option>
                                                                    <option value="VT">VT</option>
                                                                    <option value="VI">VI</option>
                                                                    <option value="VA">VA</option>
                                                                    <option value="WA">WA</option>
                                                                    <option value="WV">WV</option>
                                                                    <option value="WI">WI</option>
                                                                    <option value="WY">WY</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input zip" id="zip" name="zip" placeholder="Zipcode">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Telephone : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input phone" placeholder="(999) 999-9999" id="business_telephone" name="business_telephone" placeholder="Business Telephone" maxlength="15">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <div class="dropdown">
                                                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                        <option value="">Type</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                        <option value="Home">Home</option>
                                                                        <option value="Work">Work</option>
                                                                        <option value="Work">Other</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input zip" id="ext1" name="ext1" readonly placeholder="Ext.">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">County / Code : <span class="not-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <select name="physical_county" id="physical_county" class="form-control fsc-input">
                                                                    <option value="Appling">Appling</option>
                                                                    <option value="Atkinson">Atkinson</option>
                                                                    <option value="Bacon">Bacon</option>
                                                                    <option value="Baker">Baker</option>
                                                                    <option value="Baldwin">Baldwin</option>
                                                                    <option value="Banks">Banks</option>
                                                                    <option value="Barrow">Barrow</option>
                                                                    <option value="Bartow">Bartow</option>
                                                                    <option value="Ben Hill">Ben Hill</option>
                                                                    <option value="Berrien">Berrien</option>
                                                                    <option value="Bibb">Bibb</option>
                                                                    <option value="Bleckley">Bleckley</option>
                                                                    <option value="Brantley">Brantley</option>
                                                                    <option value="Brooks">Brooks</option>
                                                                    <option value="Bryan">Bryan</option>
                                                                    <option value="Bullochq">Bullochq</option>
                                                                    <option value="Burke">Burke</option>
                                                                    <option value="Butts">Butts</option>
                                                                    <option value="Calhoun">Calhoun</option>
                                                                    <option value="Camden">Camden</option>
                                                                    <option value="Candler">Candler</option>
                                                                    <option value="Carroll">Carroll</option>
                                                                    <option value="Catoosa">Catoosa</option>
                                                                    <option value="Charlton">Charlton</option>
                                                                    <option value="Chatham">Chatham</option>
                                                                    <option value="Chattahoochee">Chattahoochee</option>
                                                                    <option value="Chattooga">Chattooga</option>
                                                                    <option value="Cherokee">Cherokee</option>
                                                                    <option value="Clarke">Clarke</option>
                                                                    <option value="Clay">Clay</option>
                                                                    <option value="Clayton">Clayton</option>
                                                                    <option value="Clinch">Clinch</option>
                                                                    <option value="Cobb">Cobb</option>
                                                                    <option value="Coffee">Coffee</option>
                                                                    <option value="Colquitt">Colquitt</option>
                                                                    <option value="Columbia">Columbia</option>
                                                                    <option value="Cook">Cook</option>
                                                                    <option value="Coweta">Coweta</option>
                                                                    <option value="Crawford">Crawford</option>
                                                                    <option value="Crisp">Crisp</option>
                                                                    <option value="Dade">Dade</option>
                                                                    <option value="Dawson">Dawson</option>
                                                                    <option value="Decatur">Decatur</option>
                                                                    <option value="DeKalb (Not Atlanta)">DeKalb (Not Atlanta)</option>
                                                                    <option value="DeKalb (In Atlanta)">DeKalb (In Atlanta)</option>
                                                                    <option value="Dodge">Dodge</option>
                                                                    <option value="Dooly">Dooly</option>
                                                                    <option value="Dougherty">Dougherty</option>
                                                                    <option value="Douglas">Douglas</option>
                                                                    <option value="Early">Early</option>
                                                                    <option value="Echols">Echols</option>
                                                                    <option value="Effinghamq">Effinghamq</option>
                                                                    <option value="Elbert">Elbert</option>
                                                                    <option value="Emanuel">Emanuel</option>
                                                                    <option value="Evans">Evans</option>
                                                                    <option value="Fannin">Fannin</option>
                                                                    <option value="Fayette">Fayette</option>
                                                                    <option value="Floyd">Floyd</option>
                                                                    <option value="Forsyth">Forsyth</option>
                                                                    <option value="Franklin">Franklin</option>
                                                                    <option value="Fulton (Not Atlanta)">Fulton (Not Atlanta)</option>
                                                                    <option value="Fulton (In Atlanta)">Fulton (In Atlanta)</option>
                                                                    <option value="Gilmer">Gilmer</option>
                                                                    <option value="Glascock">Glascock</option>
                                                                    <option value="Glynn">Glynn</option>
                                                                    <option value="Gordon">Gordon</option>
                                                                    <option value="Grady">Grady</option>
                                                                    <option value="Greene">Greene</option>
                                                                    <option value="Gwinnett" selected="">Gwinnett</option>
                                                                    <option value="Habersham">Habersham</option>
                                                                    <option value="Hall">Hall</option>
                                                                    <option value="Hancock">Hancock</option>
                                                                    <option value="Haralson">Haralson</option>
                                                                    <option value="Harris">Harris</option>
                                                                    <option value="Hart">Hart</option>
                                                                    <option value="Heard">Heard</option>
                                                                    <option value="Henry">Henry</option>
                                                                    <option value="Houston">Houston</option>
                                                                    <option value="Irwin">Irwin</option>
                                                                    <option value="Jackson">Jackson</option>
                                                                    <option value="Jasper">Jasper</option>
                                                                    <option value="Jeff Davis">Jeff Davis</option>
                                                                    <option value="Jefferson">Jefferson</option>
                                                                    <option value="Jenkins">Jenkins</option>
                                                                    <option value="Johnson">Johnson</option>
                                                                    <option value="Jones">Jones</option>
                                                                    <option value="Lamar">Lamar</option>
                                                                    <option value="Lanier">Lanier</option>
                                                                    <option value="Laurens">Laurens</option>
                                                                    <option value="Lee">Lee</option>
                                                                    <option value="Liberty">Liberty</option>
                                                                    <option value="Lincoln">Lincoln</option>
                                                                    <option value="Lincoln">Lincoln</option>
                                                                    <option value="Long">Long</option>
                                                                    <option value="Lowndes">Lowndes</option>
                                                                    <option value="Lumpkin">Lumpkin</option>
                                                                    <option value="Macon">Macon</option>
                                                                    <option value="Madison">Madison</option>
                                                                    <option value="Marion">Marion</option>
                                                                    <option value="McDuffie">McDuffie</option>
                                                                    <option value="McIntosh">McIntosh</option>
                                                                    <option value="Meriwether">Meriwether</option>
                                                                    <option value="Miller">Miller</option>
                                                                    <option value="Mitchell">Mitchell</option>
                                                                    <option value="Monroe">Monroe</option>
                                                                    <option value="Montgomery">Montgomery</option>
                                                                    <option value="Morgan">Morgan</option>
                                                                    <option value="Murray">Murray</option>
                                                                    <option value="Muscogee">Muscogee</option>
                                                                    <option value="Newton">Newton</option>
                                                                    <option value="Oconee">Oconee</option>
                                                                    <option value="Oglethorpe">Oglethorpe</option>
                                                                    <option value="Paulding">Paulding</option>
                                                                    <option value="Peach">Peach</option>
                                                                    <option value="Pickens">Pickens</option>
                                                                    <option value="Pierce">Pierce</option>
                                                                    <option value="Pike">Pike</option>
                                                                    <option value="Polk">Polk</option>
                                                                    <option value="Pulaski">Pulaski</option>
                                                                    <option value="Putnam">Putnam</option>
                                                                    <option value="Quitman">Quitman</option>
                                                                    <option value="Rabun">Rabun</option>
                                                                    <option value="Randolph">Randolph</option>
                                                                    <option value="Richmond">Richmond</option>
                                                                    <option value="Rockdale">Rockdale</option>
                                                                    <option value="Schley">Schley</option>
                                                                    <option value="Screven">Screven</option>
                                                                    <option value="Seminole">Seminole</option>
                                                                    <option value="Spalding">Spalding</option>
                                                                    <option value="Stephens">Stephens</option>
                                                                    <option value="Stewart">Stewart</option>
                                                                    <option value="Sumter">Sumter</option>
                                                                    <option value="Talbot">Talbot</option>
                                                                    <option value="Taliaferro">Taliaferro</option>
                                                                    <option value="Tattnall">Tattnall</option>
                                                                    <option value="Taylor">Taylor</option>
                                                                    <option value="Telfair">Telfair</option>
                                                                    <option value="Terrell">Terrell</option>
                                                                    <option value="Thomas">Thomas</option>
                                                                    <option value="Tift">Tift</option>
                                                                    <option value="Toombs">Toombs</option>
                                                                    <option value="Towns">Towns</option>
                                                                    <option value="Treutlen">Treutlen</option>
                                                                    <option value="Troup">Troup</option>
                                                                    <option value="Turner">Turner</option>
                                                                    <option value="Twiggs">Twiggs</option>
                                                                    <option value="Union">Union</option>
                                                                    <option value="Upson">Upson</option>
                                                                    <option value="Walker">Walker</option>
                                                                    <option value="Walton">Walton</option>
                                                                    <option value="Ware">Ware</option>
                                                                    <option value="Warren">Warren</option>
                                                                    <option value="Washington">Washington</option>
                                                                    <option value="Wayne">Wayne</option>
                                                                    <option value="Webster">Webster</option>
                                                                    <option value="Wheeler">Wheeler</option>
                                                                    <option value="White">White</option>
                                                                    <option value="Whitfield">Whitfield</option>
                                                                    <option value="Wilcox">Wilcox</option>
                                                                    <option value="Wilkes">Wilkes</option>
                                                                    <option value="Wilkinson">Wilkinson</option>
                                                                    <option value="Worth">Worth</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input zip" id="physical_county_no" readonly name="physical_county_no" placeholder="Code">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group has-feedback has-success">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Website : <span class="not-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="website" name="website" placeholder="Website">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if(Request::segment(4)=='Accounting Bookkeeping and Taxation Service' or (Request::segment(4)=='Commercial Mortgage Services') or (Request::segment(4)=='Financial Services') or (Request::segment(4)=='Insurance Service'))
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                                    <h3 class="Libre fsc-reg-sub-header">Contact Person Information :</h3>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Contact Person Name : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                        <select class="form-control fsc-input" id="nametype" name="nametype">
                                                                            <option value="mr">Mr.</option>
                                                                            <option value="mrs">Mrs.</option>
                                                                            <option value="miss">Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px;">
                                                                        <input type="text" class="form-control fsc-input textonly" id="fname" name="fname" placeholder="First">
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>
                                                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                        <div class="row">
                                                                            <input type="text" class="form-control fsc-input textonly" maxlength="1" id="mname" name="mname" placeholder="M">
                                                                        </div>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                        <input type="text" class="form-control fsc-input textonly" id="lname" name="lname" placeholder="Last">
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Same as Above Business Information :</label>
                                                            </div>
                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <!--<input type="checkbox" class="paycheckbox" id="directpay" name="directpay" readonly value="Direct Pay" checked placeholder="A/c No." style="width:15px;height:15px;margin-top:10px;">-->
                                                                <input type="checkbox" class="paycheckbox" id="sameasbusiness" onclick="FillBilling1(this.form)" name="sameasbusiness" value="1" style="width:15px;height:15px;margin-top:10px;">
                                                                <label for="sameasbusiness"></label>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Telephone # : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input phone phonenumber123" id="phonenumber" name="phonenumber" placeholder="(999) 999-9999">
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <div class="dropdown">
                                                                    <select name="phonetype" id="phonetype" class="form-control fsc-input phonetype123">
                                                                        <option value="">Type</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                        <option value="Home">Home</option>
                                                                        <option value="Work">Work</option>
                                                                        <option value="Work">Other</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input zip ext123" id="ext" name="ext" readonly placeholder="Ext.">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Email : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="email" class="form-control fsc-input" id="email" name="email" placeholder="Email">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    @if(Request::segment(4)=='Financial Services')
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                            <h3 class="Libre fsc-reg-sub-header">Shareholder / Officer Information :</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="mainfile">
                                                <div class="input_fields_wrap input_fields_wrap_shareholder">
                                                    <div class="input_fields_wrap_1">
                                                        <input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                        <div class="share_tabs_main">
                                                            <div class="share_tabs ">
                                                                <div class="share_tab share_position" style="width: 10%;">
                                                                    <select class="form-control" style="font-size: 14px;" id="agentnametype" name="agentnametype[]">
                                                                        <option value="mr">Mr.</option>
                                                                        <option value="mrs">Mrs.</option>
                                                                        <option value="miss">Miss.</option>
                                                                    </select>
                                                                </div>
                                                                <div class="share_tab share_firstn">
                                                                    <input name="agent_fname1[]" value="" type="text" id="agent_fname2" placeholder="First name" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_m">
                                                                    <input name="agent_mname1[]" value="" type="text" placeholder="M" id="agent_mname2" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_lastn">
                                                                    <input name="agent_lname1[]" value="" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_position">
                                                                    <select name="agent_position[]" style="font-size: 14px;" id="agent_position" class="form-control agent_position">
                                                                        <option value="">Position</option>
                                                                        <option value="Agent">Agent</option>
                                                                        <option value="CEO">CEO</option>
                                                                        <option value="CFO">CFO</option>
                                                                        <option value="Secretary">Secretary</option>
                                                                        <option value="Sec">CEO / CFO / Sec.</option>
                                                                    </select>
                                                                </div>
                                                                <div class="share_tab share_persentage">
                                                                    <input name="agent_per[]" value="" type="text" placeholder="Owner %" id="agent_per" placeholder="Feb-07-1997" class="txtOnly form-control numeric1  num"/>
                                                                    <div class="cc"></div>
                                                                </div>
                                                                <div class="share_tab share_date">
                                                                    <input name="effective_date[]" value="" maxlength="10" type="text" placeholder="Effective Date" id="effective_date" class="txtOnly form-control effective_date2"/>
                                                                </div>
                                                                <div class="share_add">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="share_tabs_main">
                                                <div class="share_tabs_other">
                                                    <div class="share_tab share_firstn">&nbsp;</div>
                                                    <div class="share_tab share_m">&nbsp;</div>
                                                    <div class="share_tab share_lastn">&nbsp;</div>
                                                    <div class="share_tab share_position" style="width: 67%;">
                                                        <label class="share_total">Total :</label>
                                                    </div>
                                                    <div class="share_tab share_persentage">
                                                        <input name="total" value="" type="text" placeholder="" id="total" class="txtOnly form-control total" readonly/>
                                                        <p style="display:none;color:red;float: left;" id="t1">This should be not more then 100.00%</p>
                                                    </div>
                                                    <div class="share_tab share_remove">
                                                        <button type="button" id="add_row1" class="btn btn-success addbtn">ADD</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                        <ul class="pager wizard">
                                            <li class="next"><a href="#" class="nex1">Next <i class="fa fa-chevron-right" style="float: right;margin-top: 5px;"></i> </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step-2" data-step="2">
                                <div id="form-step-2" role="form" data-toggle="validator">
                                    <div class="form-section">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;">
                                                    <h3 class="Libre fsc-reg-sub-header text-center">Payment Information :</h3>
                                                </div>
                                                <div class="col-md-12" style="margin-top:10px;">
                                                    <!-- Nav tabs -->
                                                    <div class="">
                                                        <ul class="nav nav-tabs tabs-lists" role="tablist">
                                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Credit Card</a></li>
                                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">ACH</a></li>
                                                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Direct Pay</a></li>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="home">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <img src="https://financialservicecenter.net/public/frontcss/images/cards.png" alt="payment-cards" style="width:350px;" class="img-responsive">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Card No. : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="" name="" placeholder="A/c No.">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Expiry Date : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <div class="col-md-6" style="padding-left:0px;">
                                                                                    <input type="date" class="form-control fsc-input" id="" name="" placeholder="">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                                <div class="col-md-6" style="padding-right:0px;display: flex;">
                                                                                    <label class="fsc-form-label" style="padding-right:15px">CVV : <span class="star-reqire">*</span></label>
                                                                                    <input type="text" maxlength="3" class="form-control fsc-input" style="width:120px" id="" name="" placeholder="CVV">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Name : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="" name="" placeholder="Card Holder Name">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Account No. : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="acno" name="acno" placeholder="A/c No.">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Confirm Account No. : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="cacno" name="cacno" placeholder="Confirm A/C No.">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Routing Number : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="routing" name="routing" placeholder="Routing Number">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Bank Name : <span class="star-reqire">*</span></label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input type="text" class="form-control fsc-input" id="bankname" name="bankname" placeholder="Bank Name">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Direct Pay at Office :</label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <!--<input type="checkbox" class="paycheckbox" id="directpay" name="directpay" readonly value="Direct Pay" checked placeholder="A/c No." style="width:15px;height:15px;margin-top:10px;">-->
                                                                                <input type="checkbox" class="paycheckbox" id="directpay" name="directpay" value="Direct Pay" checked placeholder="A/c No." style="width:15px;height:15px;margin-top:10px;">

                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                <label class="fsc-form-label">Note : </label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">

                                                                                <!--<input type="text" class="form-control fsc-input" id="availabilitynote" name="availabilitynote" placeholder="Enter Note">-->
                                                                                <textarea rows="3" cols="50" name="availabilitynote" id="availabilitynote" class="form-control" style="width:300px;height:100px!important;font-size:15px;"></textarea>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div">
                                                    @foreach($ser as $serimg)
                                                        @if(Request::segment(3)==$serimg->id)
                                                            {!!$serimg->description1!!}
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                                    <div class="title-form-group"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                                    <div class="title-form-group"></div>
                                                </div>
                                            </div>
                                            <div class="row"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                        <ul class="pager wizard">
                                            <li class="previous first" style="display:none;"><a href="#" class="btn btn-primary">First</a></li>
                                            <li class="previous"><a href="#" class="nex1"><i class="fa fa-chevron-left" style="float: left;margin-top: 5px;"></i> Previous</a></li>
                                            <li class="next last" style="display:none;"><a href="#" class="btn btn-primary">Last</a></li>
                                            <li class="next"><a href="#" class="nex11">Next <i class="fa fa-chevron-right" style="float: right;margin-top: 5px;"></i></a></li>

                                            <!--<li class="next paycheckboxdone paycheckboxdonefirst" style="display:none;"><a href="#" class="nex11">Next1</a></li>-->
                                            <!--<li class="next paycheckboxno" style="display:none;"><a href="#" class="nex112">Next2</a></li>-->

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step-3" data-step="3">
                                <div id="form-step-3" role="form" data-toggle="validator">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Username (Email) : <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control fsc-input" id="cemail" readonly name="cemail" placeholder="Username/Email" value="">
                                                        <label class="fsc-form-label"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)"> &nbsp; Same As Email</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Old Code-->

                                    <!--<div class="form-group">-->
                                    <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">-->
                                    <!--      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		-->
                                    <!--         <label class="fsc-form-label">Password : <span class="star-required">*</span></label>-->
                                    <!--      </div>-->
                                    <!--      <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">-->
                                    <!--         <input type="password" class="form-control fsc-input" id="password" name="password" placeholder="Password" value="">-->
                                    <!--         <div id="messages"></div>-->
                                    <!--      </div>-->
                                    <!--   </div>-->
                                    <!--</div>-->

                                    <!--<div class="form-group">-->
                                    <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                                    <!--      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		-->
                                    <!--         <label class="fsc-form-label">Confirm Password: <span class="star-required">*</span></label>-->
                                    <!--      </div>-->
                                    <!--      <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">-->
                                    <!--         <input type="password" class="form-control fsc-input" id="cpassword" name="cpassword" placeholder="Confirm Password" value="">-->
                                    <!--         <div class="help-block with-errors"></div>-->
                                    <!--      </div>-->
                                    <!--   </div>-->
                                    <!--</div-->

                                    <!--Old Code End-->


                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <!--<input type="password" class="form-control fsc-input" id="password" name="password" placeholder="Password" value="">-->
                                                <input id="password" type="password" name="password" placeholder="" class="form-control input-md" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" onKeyUp="passwordStrength(this.value)">
                                                <div id="popover-password">
                                                    <ul class="list-unstyled passwordbox">
                                                        <li class=""><span class="low-upper-case"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; 1 lowercase &amp; 1 uppercase</li>
                                                        <li class=""><span class="one-number"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 number (0-9)</li>
                                                        <li class=""><span class="one-special-char"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 Special Character (!@#$%^&*).</li>
                                                        <li class=""><span class="eight-character"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Atleast 8 Character</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Confirm Password: <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="password" class="form-control fsc-input" id="cpassword" name="cpassword" placeholder="Confirm Password" value="">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Code:</label>
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input class="form-control thecode">
                                                <input type="hidden" id="getthecode" name="gethecode">
                                            </div>

                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <a class="btn btn-success form-control getcode" style="font-size:15px;font-weight:bold;color:#fff;">Get The Code</a>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">&nbsp;</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <p style='color:red;'>Please check your email for the code</p>
                                                <p style='color:red;'>Please enter the email</p>
                                                <p style='color:red;'>If you do not receive email please click again 'Get The Code'</p>

                                            </div>


                                        </div>
                                    </div>


                                    <div class="form-group showread" style="display:none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row"></div>
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <h5 style="text-align: left;"><label><input type="checkbox" name="terms" value="1" class="termcheckbox"> I agrees to FSC <a href="#" data-toggle="modal" data-target="#termspopup"> Terms Of Use</a> and acknowledge I have read the <a href="#" data-toggle="modal" data-target="#privacypopup">Privacy Policy</a>.</label></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                            <ul class="pager wizard">
                                                <li class="previous"><a href="#" class="nex1"> <i class="fa fa-chevron-left" style="float: left;margin-top: 5px;"></i> Previous</a></li>
                                                <li class="next last"><input type="submit" value="Submit" class="pull-right btn fsc-apply-btn termbutton" style="display:none;"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>


    <script>

        $(document).ready(function () {

            $('.changepassword').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            regexp:
                                {
                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$#!%*?&])[A-Za-z\d$@#$!%*?&]{8}/,
                                    message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                },

                        }
                    },

                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and can\'t be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            },

                            // different: {   equalTo: "#password"
                            // 	field: 'oldpassword',
                            // 	message: 'The password can\'t be the same as Old Password'
                            // }
                        }
                    }
                }

            }).on('success.form.bv', function (e) {
                $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                $('.changepassword').data('bootstrapValidator').resetForm();
                // Prevent form submission
                e.preventDefault();
                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    // console.log(result);
                }, 'json');
            });
        });


        $(document).ready(function () {


            $('#password').keyup(function () {
                var password = $('#password').val();
                if (checkStrength(password) == false) {
                    //  $('#sign-up').attr('disabled', true);
                }
            });

            function checkStrength(password) {
                var strength = 0;


                //If password contains both lower and uppercase characters, increase strength value.
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                    strength += 1;
                    $('.low-upper-case').addClass('text-success');
                    $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');


                } else {
                    $('.low-upper-case').removeClass('text-success');
                    $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                }

                //If it has numbers and characters, increase strength value.
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                    strength += 1;
                    $('.one-number').addClass('text-success');
                    $('.one-number i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.one-number').removeClass('text-success');
                    $('.one-number i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                }

                //If it has one special character, increase strength value.
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                    strength += 1;
                    $('.one-special-char').addClass('text-success');
                    $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.one-special-char').removeClass('text-success');
                    $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                }

                if (password.length > 7) {
                    strength += 1;
                    $('.eight-character').addClass('text-success');
                    $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.eight-character').removeClass('text-success');
                    $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                }


                // If value is less than 2

                if (strength < 2) {
                    $('#result').removeClass()
                    $('#password-strength').addClass('progress-bar-danger');

                    $('#result').addClass('text-danger').text('Very Week');
                    $('#password-strength').css('width', '10%');
                } else if (strength == 2) {
                    $('#result').addClass('good');
                    $('#password-strength').removeClass('progress-bar-danger');
                    $('#password-strength').addClass('progress-bar-warning');
                    $('#result').addClass('text-warning').text('Week')
                    $('#password-strength').css('width', '60%');
                    return 'Week'
                } else if (strength == 4) {
                    $('#result').removeClass()
                    $('#result').addClass('strong');
                    $('#password-strength').removeClass('progress-bar-warning');
                    $('#password-strength').addClass('progress-bar-success');
                    $('#result').addClass('text-success').text('Strength');
                    $('#password-strength').css('width', '100%');

                    return 'Strong'
                }

            }

        });

    </script>

    <script>
        function FillBilling1(f) {
            //business_telephone,telephoneNo1Type,ext1
            if (f.sameasbusiness.checked == true) {
                f.phonenumber.value = f.business_telephone.value;
                f.phonetype.value = f.telephoneNo1Type.value;
                f.ext.value = f.ext1.value;
                // f.second_state.value = f.stateId.value;
                // f.second_zip.value = f.zip.value;
            } else {
                $('.phonenumber123').val('');
                $('.phonetype123').val('');
                $('.ext123').val('');
            }
        }
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            //business_telephone,telephoneNo1Type,telephoneNo1Type

            // $("#sameasbusiness").click(function ()
            // {
            //     var sameasbusiness = $("#sameasbusiness").val();
            //     var business_telephone=$('#business_telephone').val();
            //     var telephoneNo1Type=$('#telephoneNo1Type').val();
            //     var ext1=$('#ext1').val();
            //     alert(sameasbusiness);
            //     if(sameasbusiness ==1)
            //     {
            //       // phonenumber123,phonetype,ext123
            //         $('#phonenumber123').val(phonenumber);
            //         $('#phonetype').val(phonetype);
            //         $('#ext123').val(ext1);
            // 	}
            // 	else
            // 	{

            // 	}
            // });

            /* $(".termcheckbox").change(function()
             {
                 var termcheck=$('.termcheckbox').val();
                 //alert(termcheck);
                 if(termcheck==1)
                 {
                     $('.termbutton').show();
                 }
                 else
                 {
                     $('.termbutton').hide();
                 }
             });*/
        });

        $(function () {


            $(".termcheckbox").change(function () {
                if (this.checked) {
                    $('.termbutton').show();

                } else {
                    $('.termbutton').hide();
                }
            });


            // $(".paycheckbox").change(function()
            // {
            //     if(this.checked)
            //     {
            //         $('.paycheckboxdonefirst').hide();
            //         $('.paycheckboxdone').show();
            //         $('.paycheckboxno').hide();
            //     }
            //     else
            //     {
            //         $('.paycheckboxdonefirst').hide();
            //         $('.paycheckboxdone').hide();
            //         $('.paycheckboxno').show();
            //     }
            // });


            $("#typeofservice").change(function () {
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                if (selectedValue == 'Form a New Corporation / LLC') {
                    $(".checks1").show();
                    $(".checkm").show();
                    $(".checks").hide();
                    $(".checkl").hide();
                } else if (selectedValue == 'Licenses') {
                    $(".checks").show();
                    $(".checks1").show();
                    $(".checkl").show();
                    $(".checkm").hide();
                } else {
                }
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/bootstrap-combined.min.css')}}">
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.wizard/1.3.2/jquery.bootstrap.wizard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('public/dashboard/js/password.validation.js')}}"></script>
    <script src="https://financialservicecenter.net/public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="https://financialservicecenter.net/public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {
            jQuery.validator.addMethod("accept", function (value, element, param) {
                return value.match(new RegExp("." + param + "$"));
            });
            $.validator.addMethod("regex", function (value, element, regexpr) {
                return regexpr.test(value);
            }, "The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character");
            var $validator = $("#registrationForm").validate({
                rules: {

                    typeofservice: {
                        required: true,

                    },
                    company_name: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    state: {
                        required: true,
                    },
                    zip: {
                        required: true,
                    },
                    /*	acno: {
                    required: true,
                    },
                        cacno: {
                    required: true,
                    equalTo: "#acno"
                    },
                        routing: {
                    required: true,
                    },
                        bankname: {
                    required: true,
                    },*/
                    business_telephone: {
                        required: true,
                    },
                    telephoneNo1Type: {
                        required: true,
                    },


                    fname: {
                        required: true,
                        accept: "[a-zA-Z]+"
                    },
                    lname: {
                        required: true,
                        accept: "[a-zA-Z]+"
                    },
                    phonenumber: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ URL::to('/akAvailability') }}",
                            type: "post"
                        }
                    },
                    billingtoo: {
                        required: true,
                    },


//   password: {
//   required: true,
//   regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/

//   },
                    website: {
                        required: false,
                        url: true
                    }
                    ,
                    terms: {
                        required: true,
                        //regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/
                    },
//   cpassword: {
//   required: true,       
//   equalTo: "#password"
//   }
                },
                messages: {


                    billingtoo: {
                        required: "Please Checked ",
                    },
                    email: {
                        required: "Please Enter Your Email Address",
                        email: "Please Enter A Valid Email Address",
                        remote: "Email already in use!"
                    },
                    typeofservice: {
                        required: "Please Select Your Type of Service",
                    },

                    address: {
                        required: "Please Enter Your Address",
                    },
                    city: {
                        required: "Please Enter Your City",
                    },
                    zip: {
                        required: "Please Enter Your Zip",
                    },
                    business_telephone: {
                        required: "Please Enter Your Telephone",
                    },
                    telephoneNo1Type: {
                        required: "Please Select Tel. Type",
                    },
                    company_name: {
                        required: "Please Enter Your Company Name",
                    },

                    fname: {
                        required: "Please Enter Your First Name",
                        accept: "Please Enter Only Charactor",
                    },

                    phonenumber: {
                        required: "Please Enter Your Telephone ",
                    },
                    lname: {
                        required: "Please Enter Your Last Name",
                        accept: "Please Enter Only Charactor",
                    },
                    /*acno: {
                  required: "Please Enter Your A/c No",
                  },
                  cacno: {
                  required: "Please Enter Your Confirm A/c No",
                  cacno: " Enter Confirm A/c No Same as A/c No"
                  },
                  routing: {
                  required: "Please Enter Your Routing",
                  //	accept: "Please Enter Only Charactor",
                  },
                  bankname: {
                  required: "Please Enter Your Bank Name",
                  //accept: "Please Enter Only Charactor",
                  },*/

//   directpay:{
//       required: "Please Check Your Pay in Office",
//   },

                    terms: {
                        required: "Please Check Your Term and Condition",
                        //email: "Please Enter A Valid Email Address",
                    },
                    password: {
                        required: "Please Select Your Password",
                        //minlength: "The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character",
                        regex: "The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character",
                    },
                    cpassword: {
                        required: "Please Select Your Confirm Password",
                        //email: "Please Enter A Valid Email Address",
                        cpassword: " Enter Confirm Password Same as Password"
                    },
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".form-group").addClass("has-feedback");
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group, .has-feedback').removeClass('has-success').addClass('has-error')
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group, .has-feedback').removeClass('has-error').addClass('has-success');
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                }
            });
            $('#smartwizard').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'onNext': function (tab, navigation, index) {
                    var $valid = $("#registrationForm").valid();
                    if (index > 1) {
                        $(".ddd").addClass('active1');
                    }
                    if (index == 1) { // alert();
                        $(".ddd").removeClass('active1');
                    }
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                },
                'onTabClick': function (activeTab, navigation, currentIndex, nextIndex) {
                    if (nextIndex <= currentIndex) {
                        return;
                    }
                    var $valid = $("#registrationForm").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                    if (nextIndex > currentIndex + 1) {
                        return false;
                    }
                }
            });
        });
    </script>
    <script>
        function FillBilling(f) {
            if (f.billingtoo.checked == true) {
                f.cemail.value = f.email.value;
            }
        }

        //   function FillBilling2(f)
        //   {
        //   	if(f.directpay.checked == true) {
        //   	    //f.cemail.value = f.email.value;
        //   	}
        //   }
        function show1() {
            document.getElementById('ddd').style.display = 'block';
            document.getElementById('dddd').style.display = 'none';
        }

        function show2() {
            document.getElementById('ddd').style.display = 'none';
            document.getElementById('dddd').style.display = 'block';
        }
    </script>
    <script>
        var dat1 = $('#ext1').val();
        $('#telephoneNo1Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>
    <script>
        $(".getcode").click(function () {
            var email = $('#cemail').val();
            $.ajax({
                type: "post",
                url: "{!!route('service.getthecode')!!}",
                data: {'email': email},
                success: function (data) {
                    if (data != 'false') {
                        $('#getthecode').val(data);
                    }
                    //     alert('Successfully Added');

                },
                error: function (data) {
                    alert("Error")
                }
            });

        });

        $(".thecode").keyup(function () {
            var thiss = $(this).val();
            var code = $('#getthecode').val();

            if (thiss == code) {
                $('.showread').show();
            } else {
                $('.showread').hide();
            }

        });


    </script>
    <script>
        var dat1 = $('#ext2').val();
        $('#telephoneNo2Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })


        var dat1 = $('#ext').val();
        $('#phonetype').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext').removeAttribute('readonly');
                $('#ext').val();
            } else {
                document.getElementById('ext').readOnly = true;
                $('#ext').val('');
            }
        })
        $(document).ready(function () {
            $(".effective_date2").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
                //endDate: "today"
            });
            /***phone number format***/
            $(".phone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("(" + curval + ")" + " ");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                    $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 9) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });
    </script>
    <script>
        var $select = $(".agent_position");
        $select.on("change", function () {
            var selected = [];
            $.each($select, function (index, select) {
                if (select.value !== "") {
                    selected.push(select.value);
                }
                // alert(select.value);
            });
            $("option").prop("hidden", false);
            for (var index in selected) {
                $('option[value="' + selected[index] + '"]').prop("hidden", true);

            }
        });

        $(document).ready(function () {
            $(".effective_date2").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
            });
            var maxGroup = 120;
            count = 1;
            $(".btn-success").click(function () {
                $(".effective_date2").datepicker({
                    autoclose: true,
                    format: "mm/dd/yyyy",
                });
                count += 1;
                var aa = $('body').find('.input_fields_wrap_shareholder').length;
                if (count > aa) { //alert();
                    if ($('body').find('.input_fields_wrap_shareholder').length < maxGroup) {

                        var fieldHTML = '<div class="input_fields_wrap_shareholder" style="display:block">' + $(".input_fields_wrap_1").html() + '	<a href="javascript:void(0)" id="add_row1" class="btn btn-danger remove" title="Add field" ><i class="fa fa-trash"></i></a></div>';
                        $('body').find('.input_fields_wrap_shareholder:last').after(fieldHTML);
                        var $select = $(".agent_position");
                        $select.on("change", function () {
                            $(".effective_date2").datepicker({
                                autoclose: true,
                                format: "mm/dd/yyyy",
                            });
                            var selected = [];
                            $.each($select, function (index, select) {

                                if (select.value !== "") {
                                    var dd = selected.push(select.value); //alert(dd);
                                    if (dd == '2' && dd == '3' && dd == '4') {

                                    }
                                }
                            });
                            $("option").prop("hidden", false);
                            for (var index in selected) {
                                $('option[value="' + selected[index] + '"]').prop("hidden", true);
                            }
                        });
                    } else {
                        alert('Maximum ' + maxGroup + ' Persons are allowed.');
                    }
                } else {
                    $('.input_fields_wrap_shareholder').css('display', 'block');

                }
            });

            //remove fields group
            $("body").on("click", ".remove", function () {
                $(this).parents(".input_fields_wrap_shareholder").remove();
                //var dd =  $("#agent_position").val(); alert(dd);
                // alert($('select option:hidden').val();

            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#physical_county', function () {
                var selectedCountry = $("#fedral_state option:selected").val(); //alert(selectedCountry);
                var id = $(this).val();// alert(id);
                $.get('{!!URL::to('applycounty')!!}?id=' + id, function (data) {
                    $('#physical_county_no').empty();

                    $.each(data, function (index, subcatobj) {
                        $('#physical_county_no').val(subcatobj.countycode);

                    })

                });

            });
        });
    </script>
    <script>
        $(document).on("change", ".numeric1", function () {
            var sum = 0;
            $(".numeric1").each(function () {
                sum += +$(this).val().replace("%", "");
                var num = parseFloat($(this).val());
                $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");

                if ($(this).val() == 'NaN%') {
                    // alert();
                    // $(this).val(parseFloat($(this).val('0')).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"%");
                    $(this).val('0.00%');

                }
            });
            if (sum > 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').attr('disabled', 'disabled');
                $('#t1').show();
            } else if (sum < 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').attr('disabled', 'disabled');
                $('#t1').show();
            } else if (sum = 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').removeAttr('disabled');
                $('#t1').hide();
            } else {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').removeAttr('disabled');
            }
        });
    </script>
    <div id="termspopup" class="modal fade " role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <p>Terms & Condition</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="privacypopup" class="modal fade " role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        privacy popup
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection()