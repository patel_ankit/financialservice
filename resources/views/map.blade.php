<!DOCTYPE html>
<html>
<head>
    <title>Zipcode Lookup Demo</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.js"></script>
    <style>
        input, label, select {
            display: block;
            width: 100%;
            margin: 0;
            width: 150px;
        }

        body {
            background-color: #eee;
        }
    </style>

</head>
<body>
<p>Enter your zipcode and watch the city and state appear magically!</p>

<form>
    <label for="zip">Zip:</label>
    <input id="zip" name="zip"/>
    <label for="city">City:</label>
    <div id="city_wrap"><input id="city" name="city"/></div>
    <label for="state">State:</label>
    <input id="state" name="state"/>
</form>

<p><a href="http://kovalent.co/blog/zip-code-to-city-state-lookup">Read more at the Kovalent blog.</a></p>
<script>
    //when the user clicks off of the zip field:
    $('#zip').blur(function () {
        var zip = $(this).val();
        var city = '';
        var state = '';

        //make a request to the google geocode api
        $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + zip).success(function (response) {
            //find the city and state
            var address_components = response.results[0].address_components;
            $.each(address_components, function (index, component) {
                var types = component.types;
                $.each(types, function (index, type) {
                    if (type == 'locality') {
                        city = component.long_name;
                    }
                    if (type == 'administrative_area_level_1') {
                        state = component.short_name;
                    }
                });
            });

            //pre-fill the city and state
            $('#city').val(city);
            $('#state').val(state);
        });
    });
</script>


<?php

// take an array with some elements
$array = array(9, 2, 18, 34, 3, 10, 15);
// get the size of array
$count = count($array);
echo "<pre>";
// Print array elements before sorting
print_r($array);
for ($i = 0; $i < $count; $i++) {
    for ($j = $i + 1; $j < $count; $j++) {
        if ($array[$i] > $array[$j]) {
            $temp = $array[$i];
            $array[$i] = $array[$j];
            $array[$j] = $temp;
        }
    }
}
echo "Sorted Array:" . "<br/>";
// Print array elements after sorting
print_r($array);
?>
</body>
</html>