<?php
$db7 = DB::table('commonregisters')->where('id', '=', Auth::user()->user_id)->first();
//print_r($db7);die;
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="active"><a href="{{url('client/home')}}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle-o sidebar-icon"></i>
                    <span>Client Profile</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                <!--<li><a href="{{route('profile.index')}}"><i class="fa fa-circle-o"></i> Profile</a></li>-->
                    <li><?php
                        if(isset($_REQUEST['filename']) != '')
                        {
                        ?>
                        <a href="https://financialservicecenter.net/client/profile?filename=<?php echo $_REQUEST['filename'];?>"><i class="fa fa-circle-o"></i> Profile</a>
                        <?php
                        }
                        else
                        {
                        ?>
                        <a href="{{route('profile.index')}}"><i class="fa fa-circle-o"></i> Profile </a>
                        <?php
                        }
                        ?>

                    </li>
                    <li><a href="{{route('userchangepassword.index')}}"><i class="fa fa-circle-o"></i> Login Info</a></li>
                </ul>
            </li>
            <!--<li><a href="#"><i class="fa fa-home"></i> <span>Project</span></a></li>-->
        <!--<li><a href="{{url('client/clientmsg')}}"><i class="fa fa-envelope sidebar-icon"></i><span> Message </span></a></li>-->

            @if(!empty($db7->subscription_active)==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Employee</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('client/cli-employee')}}"><i class="fa fa-circle-o"></i><span> View / Edit Employee</span></a></li>
                        <li><a href="{{url('client/cli-employee/create')}}"><i class="fa fa-circle-o"></i> <span>Add Employee</span></a></li>
                    </ul>
                </li>
            @endif

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tasks"></i> <span>Task</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    @if(!empty($db7->subscription_active)==1)
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Employee
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{url('client/clientschedule')}}"><i class="fa fa-circle-o"></i><span> Schedule</span></a></li>
                                <li><a href="{{url('client/workresponsibilty')}}"><i class="fa fa-circle-o"></i><span> Work Responsivility</span></a></li>
                            </ul>
                        </li>
                    @endif

                    <li><a href="{{url('client/submissions')}}"><i class="fa fa-circle-o"></i><span> Submission </span></a></li>

                    @if(!empty($db7->subscription_active)==1)
                        <li><a href="#"><i class="fa fa-circle-o"></i><span> Task </span></a></li>
                        <li><a href="{{url('client/clientmsg')}}"><i class="fa fa-circle-o"></i><span> Message </span></a></li>
                    @endif
                </ul>
            </li>

            @if(!empty($db7->subscription_active)==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-file-text"></i> <span>Report</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i>Client Employee <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                            <!--<li><a href="{{url('client/clientschedule')}}"><i class="fa fa-circle-o"></i> <span> Schedule</span></a></li>-->
                                <li><a href="{{url('client/employeereport')}}"><i class="fa fa-circle-o"></i> <span> Timesheet</span></a></li>
                                <li><a href="{{url('client/employeeschedule')}}"><i class="fa fa-circle-o"></i> <span> Schedule</span></a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> <span> workreport</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cog"></i> <span>Setup</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>

                    <ul class="treeview-menu">

                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Employee
                                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                            </a>
                            <!--<ul class="treeview-menu">-->
                        <!--   <li><a href="{{url('client/clientschedulesetup')}}"><i class="fa fa-circle-o"></i> <span> Schedule Formula Setup</span></a></li>-->
                            <!--</ul>-->

                            <ul class="treeview-menu">
                                <li><a href="{{url('client/clientschedulesetup/create')}}"><i class="fa fa-angle-right"></i> <span> Add Schedule </span></a></li>
                                <li><a href="{{url('client/clientschedulesetup')}}"><i class="fa fa-angle-right"></i><span> View / Edit Schedule</span> </a></li>
                            </ul>

                        </li>
                    </ul>

                </li>
            @endif

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-plus"></i> <span>Client Support</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @if(!empty($db7->subscription_active)==1)
                        <li><a href="{{url('client/clientsupport')}}"><i class="fa fa-circle-o"></i> <span>Employee Support </span></a></li>
                @endif
                <!--<li><a href="{{url('client/clientsupport/create')}}"><i class="fa fa-circle-o"></i> <span>Technical Support</span></a></li>-->

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Technical Support
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{url('client/technicalsupports/create')}}"><i class="fa fa-circle-o"></i><span> Add Technical Support</span></a></li>
                            <li><a href="{{url('client/technicalsupports')}}"><i class="fa fa-circle-o"></i><span> View / Edit Support</span></a></li>
                        </ul>
                    </li>

                </ul>
            </li>

            @if(!empty($db7->subscription_active)==1)
                <li class="treeview">
                    <a href="#"><i class="fa fa-question-circle sidebar-icon"></i> <span>Info</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>

                    <ul class="treeview-menu">
                        <li><a href="{{url('client/clientvendor')}}"><i class="fa fa-circle-o"></i> <span>Vendor View</span></a></li>
                        <li><a href="#"><i class="fa fa-chevron-right"></i><span>License</span></a></li>
                        <li><a href="#"><i class="fa fa-chevron-right"></i><span> Taxation  </span></a></li>
                    </ul>
                </li>
            @endif

            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out sidebar-icon"></i><span>Logout</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>