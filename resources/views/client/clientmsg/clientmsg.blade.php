@extends('client.layouts.app')
@section('title', 'Message')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1> Message</h1>
        </section>
        <div class="">
            <br>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-title">
                        </div>
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="sampleTable3">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Assign By</th>
                                    <th>Employee Name</th>
                                    <th>Subject</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($task as $com)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>@foreach($admin as $com2) @if($com2->id==$com->admin_id) {{ucwords($com2->fname).' '.ucwords($com2->lname)}} @endif @endforeach</td>
                                        <td> <?php $ex = explode(',', $com->employeeid);?> @foreach($ex as $tk) @if($tk==$employee->id) {{ucwords($employee->first_name.' '.$employee->middle_name.' '.$employee->last_name)}} @endif @endforeach </td>
                                        <td>{{$com->title}}</td>
                                        <td>{!! $com->content !!}</td>
                                        <td><a class="btn-action btn-view-edit" href="{{route('clientmsg.edit',$com->id)}}"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection()