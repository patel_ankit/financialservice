@extends('client.layouts.app')
@section('main-content')
    <style>
        .nav-tabs {
            padding: 12px 12px 12px 18px;
        }

        .panel-primary > .panel-heading {
            color: #fff;
            background-color: transparent;
            border-color: transparent;
        }

        .nav-tabs > li > a {
            margin-bottom: 0px !important;
            height: 42px;
        }

        .Certificate-btn {
            font-size: 10.5px !important;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > a > span:not(.pull-right), .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > .treeview-menu {
            left: 72px;
            background: #275C95;
        }

        .text1 {
            float: left;
            position: relative;
            top: 12px;
            bottom: 0;
            width: 0px;
            text-align: center;
            right: 5px;
        }

        .tab-pane {
            width: 100%;
            float: left;
            margin-top: 20px;
        }

        .form-group small {
            position: absolute;
            width: 100%;
            top: 18px;
            left: -24px;
            right: 0;
            bottom: 0;
        }

        .payroll-btn {
            margin-top: 1%;
            background: #ffff99;
            width: 220px;
            text-align: center;
            padding: 10px;
            border-radius: 5px;
            color: #000;
            border: solid 2px #000;
            font-size: 15px;
        }

        .togglebox {
            float: right;
            margin-right: 10px;
        }

        .togglebox label {
            padding-right: 10px;
        }

        .customfieldsbox {
            border: 1px solid #ccc;
            padding: 15px;
            margin-bottom: 15px;
        }

        .mt30 {
            margin-top: 30px;
        }

        .btn-toggle a {
            margin: 0px 5px;
            border-radius: 4px !important;
        }

        .nav-tabs > li {
            width: 19.6% !important;
        }

        .btnyes.active {
            background-color: #00a65a !important;
            border-color: #008d4c !important;
            color: #fff !important;
        }

        .table-bordered {
            border: 1px solid #cccccc !important;
            background: none !important;
        }

        .panel.panel-default .panel-heading h4 {
            font-size: 20px !important;
            padding: 10px !important;
            height: 40px;
            background: #b3e4a6 !important;
        }

        .panel-default > .panel-heading {
            background: #b3e4a6 !important;
            padding: 5px;
        }

        .panel.panel-default .panel-heading h4 a:hover, .panel.panel-default .panel-heading h4 a {
            color: #103b68 !important;
        }

        .panel-title .glyphicon-plus {
            display: none;
        }

        .panel-title .collapsed .glyphicon-plus, .panel-title .glyphicon-minus {
            display: block;
        }

        .collapsed .glyphicon-minus {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length select {
            height: 35px !important;
        }

        .form-control, .form-control-insu, select.greenText, .statusselectbox select {
            height: 40px !important;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;
        }

        .ac_name_first {
            font-size: 17px !important;
        }

        .ac_name_first {
            float: left;
            width: 41%;
        }

        .ac_name_middel {
            width: 56%;
            float: left;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #cccccc;
        }

        .table > thead > tr > th {
            /* background: #98c4f2; */
            background: #ffff99;
            border-top: 1px solid #222 !important;
            border-left: 1px solid #222 !important;
            border-right: 1px solid #222 !important;
        }

        .form-control-accordion {
            width: 100%;
            padding: 6px;
        }

        .imgbox img {
            height: 45px;
        }

        .nav-tabs.profiletabs li {
            width: 16% !important;
        }

        .nav-tabs.profiletabs li a {
            padding: 8px 6px !important;
            height: auto !important;
            cursor: pointer;
        }

        .panel-body {
            padding-top: 0px ! imortant;
        }

        .tab-pane {
            margin-top: 0px !important;
        }

    </style>
    <div class="content-wrapper">
        <section class="content-header" style="padding:10px 0px; height:63px;">
            <h1 style="width:200px; float:left; margin-top:8px;"> Profile</h1>

            <div class="col-md-12 col-sm-12 col-xs-12 imgbox" style="width:520px; float:right">
                @if(empty($common->business_cat_id))
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @else
                            @if(empty($common->business_brand_category_id))
                                <div class="col-md-offset-2 col-md-3 col-sm-12 col-xs-12">
                                    @else
                                        <div class="col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                                            @endif
                                            @endif
                                            @foreach($business as $busi)
                                                @if($busi->id==$common->business_id)
                                                    <center>
                                                        <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive" style="width:175px">
                                                    </center>
                                        </div>
                                    @endif
                                    @endforeach
                                    @foreach($category as $cate)
                                        @if($common->business_cat_id==$cate->id)
                                            <center>
                                                @if(empty($common->business_brand_category_id))
                                                    <div class="text1">
                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        @else
                                                            <div class="text1">
                                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                @endif
                                                                <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive" style="width:175px">
                                                            </div>


                                                @endif
                                            </center>
                                            @endforeach
                                            @foreach($cb as $bb1)
                                                @if($common->business_brand_category_id == $bb1->id)
                                                    <div class="text1">
                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                                                    <center>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive" style="width:175px"></div>
                                                    </center>
                                                @endif
                                            @endforeach
                                </div>

                                <div class="clear"></div>
        </section>
        <div class="">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if ( session()->has('success') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                        @endif
                        @if ( session()->has('error') )
                            <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                        @endif
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs profiletabs" id="myTab" style="padding:2px 0px;">
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" class="active"><a href="#tab1primary" data-toggle="tab">Basic Information</a></li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;"><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Contact Information</a></li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;"><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Information</a></li>


                                    <!--<li style="margin-left:0px !important;margin-top: 0px !important;" new><a href="#tab9primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Service Information</a></li>-->
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab9primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Service Information</a> @else<a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Service Information</a>  @endif </li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;">@if($common->active_tabs==1)<a href="#tab8primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Interview Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Interview Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab10primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Business Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Business Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;">@if($common->active_tabs==1)<a href="#tab2primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Location Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Location Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;">@if($common->active_tabs==1)<a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Formation Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Formation Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;">@if($common->active_tabs==1)<a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">License Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">License Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab11primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Taxation Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Taxation Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab12primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Revenue Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Revenue Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab13primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Expense Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Expense Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab14primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Payroll Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Payroll Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;" new>@if($common->active_tabs==1)<a href="#tab15primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Banking Information</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Banking Information</a> @endif</li>
                                    <li style="margin-left:0px !important;margin-top: 0px !important;">@if($common->active_tabs==1)<a href="#tab7primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Notes</a>@else <a id="tabs_notactive1" class="hvr-shutter-in-horizontal">Notes </a> @endif</li>
                                </ul>
                            </div>

                            <form method="post" action="{{ route('profile.update',$common->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <div class="Branch">
                                                <h1>Basic Information</h1>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="form-group">
                                                        <input type="hidden" name="id" value="{{$common->id}}" readonly>
                                                        <input type="hidden" id="fileno" name="fileno" value="{{$common->cid}}" readonly>

                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Client ID : </label>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input cc" name="filename" value="{{$common->filename}}" readonly>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label pull-right">Status : </label>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="status" id="status" value="{{$common->status}}" readonly>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Superwise Code : </label>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="code" name="code" value="{{$common->code}}">
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Company Name : <span class="star-required">*</span>
                                                                <span class="clearfix"></span>
                                                                <small>(Legal Name)</small>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="company_name" id="company_name" value="{{$common->company_name}}">
                                                            @if ($errors->has('company_name'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('company_name') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_name') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Business Name : <span class="star-required">*</span>
                                                                <span class="clearfix"></span>
                                                                <small>(DBA Name)</small>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                            <input type="text" class="form-control fsc-input" name="business_name" id="business_name" value="{{$common->business_name}}">
                                                            @if($errors->has('business_name'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('business_name') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Business Address 1: </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="address" id="address" value="{{$common->address}}">
                                                            @if($errors->has('address'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('address') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address1') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Business Address 2: </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="address1" id="address1" value="{{$common->address1}}">
                                                            @if($errors->has('address1'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('address1') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_address') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Country :</label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown">
                                                                        <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="{{$common->countryId}}"></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        .
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">City / State / Zip : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City" value="{{$common->city}}">
                                                                    @if($errors->has('city'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('city') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="stateId" id="stateId" class="form-control fsc-input  bfh-states" data-country="countries_states1" data-state="{{$common->stateId}}"> </select>
                                                                        @if($errors->has('stateId'))
                                                                            <span class="help-block">
                                                <strong>{{ $errors->first('stateId') }}</strong>
                                                </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input zip" id="zip" name="zip" value="{{$common->zip}}" placeholder="Zip">
                                                                    @if($errors->has('zip'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('zip') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label"></label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <label class="fsc-form-label" style="text-align: left;"><input class="" name="billingtoo" onclick="FillBilling(this.form)" type="checkbox"> Same As Above Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_address') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Mailing Address 1: </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="mailing_address" name="mailing_address" value="{{$common->mailing_address}}" placeholder="Mailing Address">
                                                            @if($errors->has('mailing_address'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('mailing_address') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_address1') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Mailing Address 2: </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="mailing_address1" name="mailing_address1" value="{{$common->mailing_address1}}" placeholder="Mailing Address 1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_city') ? 'has-error' : ''}} {{ $errors->has('mailing_state') ? 'has-error' : ''}}{{ $errors->has('mailing_zip') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">City / State / Zip : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="mailing_city" name="mailing_city" placeholder="City" value="{{$common->mailing_city}}">

                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="mailing_state" id="mailing_state" class="form-control fsc-input">
                                                                            @if($common->mailing_state==NULL)
                                                                                <option value=""> State</option>
                                                                            @else
                                                                                <option value="{{$common->mailing_state}}">{{$common->mailing_state}}</option>
                                                                            @endif
                                                                            <option value='AK'>AK</option>
                                                                            <option value='AS'>AS</option>
                                                                            <option value='AZ'>AZ</option>
                                                                            <option value='AR'>AR</option>
                                                                            <option value='CA'>CA</option>
                                                                            <option value='CO'>CO</option>
                                                                            <option value='CT'>CT</option>
                                                                            <option value='DE'>DE</option>
                                                                            <option value='DC'>DC</option>
                                                                            <option value='FM'>FM</option>
                                                                            <option value='FL'>FL</option>
                                                                            <option value='GA'>GA</option>
                                                                            <option value='GU'>GU</option>
                                                                            <option value='HI'>HI</option>
                                                                            <option value='ID'>ID</option>
                                                                            <option value='IL'>IL</option>
                                                                            <option value='IN'>IN</option>
                                                                            <option value='IA'>IA</option>
                                                                            <option value='KS'>KS</option>
                                                                            <option value='KY'>KY</option>
                                                                            <option value='LA'>LA</option>
                                                                            <option value='ME'>ME</option>
                                                                            <option value='MH'>MH</option>
                                                                            <option value='MD'>MD</option>
                                                                            <option value='MA'>MA</option>
                                                                            <option value='MI'>MI</option>
                                                                            <option value='MN'>MN</option>
                                                                            <option value='MS'>MS</option>
                                                                            <option value='MO'>MO</option>
                                                                            <option value='MT'>MT</option>
                                                                            <option value='NE'>NE</option>
                                                                            <option value='NV'>NV</option>
                                                                            <option value='NH'>NH</option>
                                                                            <option value='NJ'>NJ</option>
                                                                            <option value='NM'>NM</option>
                                                                            <option value='NY'>NY</option>
                                                                            <option value='NC'>NC</option>
                                                                            <option value='ND'>ND</option>
                                                                            <option value='MP'>MP</option>
                                                                            <option value='OH'>OH</option>
                                                                            <option value='OK'>OK</option>
                                                                            <option value='OR'>OR</option>
                                                                            <option value='PW'>PW</option>
                                                                            <option value='PA'>PA</option>
                                                                            <option value='PR'>PR</option>
                                                                            <option value='RI'>RI</option>
                                                                            <option value='SC'>SC</option>
                                                                            <option value='SD'>SD</option>
                                                                            <option value='TN'>TN</option>
                                                                            <option value='TX'>TX</option>
                                                                            <option value='UT'>UT</option>
                                                                            <option value='VT'>VT</option>
                                                                            <option value='VI'>VI</option>
                                                                            <option value='VA'>VA</option>
                                                                            <option value='WA'>WA</option>
                                                                            <option value='WV'>WV</option>
                                                                            <option value='WI'>WI</option>
                                                                            <option value='WY'>WY</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input zip" id="mailing_zip" name="mailing_zip" value="{{$common->mailing_zip}}" placeholder="Zip">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('email') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Business Email : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="email" class="form-control fsc-input" id="email" name='email' readonly placeholder="abc@abc.com" value="{{$common->email}}">
                                                            @if($errors->has('email'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_no') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Business Telephone # : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                    <input type="text" class="form-control phone fsc-input bfh-phone" data-country="countries_states1" id="business_no" value="{{$common->business_no}}" name="business_no" placeholder="(999) 999-9999">
                                                                    @if($errors->has('business_no'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('business_no') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_fax') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Fax # : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control phone fsc-input bfh-phone" data-country="countries_states1" value="{{$common->business_fax}}" id="business_fax" name="business_fax" placeholder="(999) 999-9999">
                                                                    @if($errors->has('business_fax'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('business_fax') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('website') ? 'has-error' : ''}}">
                                                    <div class="form-group">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Website : </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="website" value="{{$common->website}}" name="website" placeholder="Website address">
                                                            @if($errors->has('website'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('website') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="tab4primary">
                                            <div class="Branch"><h1>Contact Information</h1></div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Name :</label>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <select type="text" class="form-control txtOnly" id="nametype" name="nametype">
                                                                    <option value="mr" @if($common->nametype=='mr') selected="" @endif>Mr.</option>
                                                                    <option value="mrs" @if($common->nametype=='mrs') selected="" @endif>Mrs.</option>
                                                                    <option value="miss" @if($common->nametype=='miss') selected="" @endif>Miss.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <input type="text" class="form-control textonly" id="firstname" name="firstname" value="{{$common->firstname}}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <input type="text" class="form-control textonly" id="middlename" name="middlename" maxlength="1" value="{{$common->middlename}}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <input type="text" class="form-control textonly" id="lastname" name="lastname" value="{{$common->lastname}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Address 1 :</label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" id="contact_address1" name="contact_address1" value="{{$common->contact_address1}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Address 2 :</label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" id="contact_address2" name="contact_address2" value="{{$common->contact_address2}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">City / State / Zip :</label>
                                                    <div class="col-md-2">
                                                        <input name="city_1" value="{{$common->city_1}}" type="text" id="city_1" class="textonly form-control">
                                                    </div>
                                                    <div class="">
                                                        <div class="col-md-2">
                                                            <select name="state" id="stateId_1" class="form-control fsc-input">
                                                                <option value="GA">GA</option>
                                                                <option value="AK">AK</option>
                                                                <option value="AS">AS</option>
                                                                <option value="AZ">AZ</option>
                                                                <option value="AR">AR</option>
                                                                <option value="CA">CA</option>
                                                                <option value="CO">CO</option>
                                                                <option value="CT">CT</option>
                                                                <option value="DE">DE</option>
                                                                <option value="DC">DC</option>
                                                                <option value="FM">FM</option>
                                                                <option value="FL">FL</option>
                                                                <option value="GA">GA</option>
                                                                <option value="GU">GU</option>
                                                                <option value="HI">HI</option>
                                                                <option value="ID">ID</option>
                                                                <option value="IL">IL</option>
                                                                <option value="IN">IN</option>
                                                                <option value="IA">IA</option>
                                                                <option value="KS">KS</option>
                                                                <option value="KY">KY</option>
                                                                <option value="LA">LA</option>
                                                                <option value="ME">ME</option>
                                                                <option value="MH">MH</option>
                                                                <option value="MD">MD</option>
                                                                <option value="MA">MA</option>
                                                                <option value="MI">MI</option>
                                                                <option value="MN">MN</option>
                                                                <option value="MS">MS</option>
                                                                <option value="MO">MO</option>
                                                                <option value="MT">MT</option>
                                                                <option value="NE">NE</option>
                                                                <option value="NV">NV</option>
                                                                <option value="NH">NH</option>
                                                                <option value="NJ">NJ</option>
                                                                <option value="NM">NM</option>
                                                                <option value="NY">NY</option>
                                                                <option value="NC">NC</option>
                                                                <option value="ND">ND</option>
                                                                <option value="MP">MP</option>
                                                                <option value="OH">OH</option>
                                                                <option value="OK">OK</option>
                                                                <option value="OR">OR</option>
                                                                <option value="PW">PW</option>
                                                                <option value="PA">PA</option>
                                                                <option value="PR">PR</option>
                                                                <option value="RI">RI</option>
                                                                <option value="SC">SC</option>
                                                                <option value="SD">SD</option>
                                                                <option value="TN">TN</option>
                                                                <option value="TX">TX</option>
                                                                <option value="UT">UT</option>
                                                                <option value="VT">VT</option>
                                                                <option value="VI">VI</option>
                                                                <option value="VA">VA</option>
                                                                <option value="WA">WA</option>
                                                                <option value="WV">WV</option>
                                                                <option value="WI">WI</option>
                                                                <option value="WY">WY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <div class="col-md-2">
                                                            <input name="zip_1" value="{{$common->zip_1}}" type="text" id="zip_1" class="form-control zip">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Telephone No. 1:</label>
                                                    <div class="col-md-2">
                                                        <input name="mobile_1" placeholder="(999) 999-9999" value="{{$common->mobile_1}}" type="tel" id="mobile_1" class="phone form-control">
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                        <select name="mobiletype_1" id="mobiletype_1" class="form-control fsc-input" style="height:auto">
                                                            <option value="Home" @if($common->mobiletype_1=='Home') selected @endif>Office</option>
                                                            <option value="Mobile" @if($common->mobiletype_1=='Mobile') selected @endif>Mobile</option>
                                                            <option value="Other" @if($common->mobiletype_1=='Other') selected @endif>Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input class="form-control fsc-input" id="ext2_1" maxlength="5" name="ext2_1" readonly value="{{$common->ext2_1}}" placeholder="Ext" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Telephone No. 2:</label>
                                                    <div class="col-md-2">
                                                        <input name="mobile_2" placeholder="(999) 999-9999" value="{{$common->mobile_2}}" type="tel" id="mobile_2" class="phone form-control">
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                        <select name="mobiletype_2" id="mobiletype_2" class="form-control fsc-input" style="height:auto">
                                                            <option value="Home" @if($common->mobiletype_2	=='Home') selected @endif>Office</option>
                                                            <option value="Mobile" @if($common->mobiletype_2	=='Mobile') selected @endif>Mobile</option>
                                                            <option value="Other" @if($common->mobiletype_2 =='Other') selected @endif>Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input class="form-control fsc-input" readonly id="ext2_2" maxlength="5" name="ext2_2" value="{{$common->ext2_2}}" placeholder="Ext" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-md-3">Fax No. :</label>
                                                    <div class="col-md-2">
                                                        <input name="contact_fax_1" placeholder="(999) 999-9999" value="{{$common->contact_fax_1}}" type="tel" id="contact_fax_1" class="form-control phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Email :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="email_1" name="email_1" value="{{$common->email_1}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab10primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="Branch">
                                                    <h1>Business Information</h1>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Type of Business</label>
                                                    <div class="col-md-6">
                                                        @foreach($category as $cate)
                                                            @if($cate->id==$common->business_cat_id)
                                                                <input type="text" readonly class="form-control" value="{{$cate->business_cat_name}}">
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Business Start Date</label>
                                                    <div class="col-md-3">
                                                        <?php $dbcolors = explode(',', $common->type_of_entity_answer);?>
                                                        @foreach($question as $ques)
                                                            @if($ques->type=='Type of Business')

                                                                @if($ques->question=='What is your business start date ?')
                                                                    <input type="text" id="due_date02" name="due_date02" readonly value="{{$common->type_of_entity_answer1}}" class="form-control"/>
                                                                    <style>#sss {
                                                                            display: none
                                                                        }</style>
                                                                @else

                                                                @endif

                                                            @endif
                                                        @endforeach
                                                        <input style="width:227px;" type="text" class="form-control" readonly id="sss" name="sss" value="{{$common->due_date02}}">
                                                    </div>
                                                    <div class="col-md-3" style="padding-left:0px;">
                                                        <select class="form-control fsc-input" id="businessone" name="newbusiness">
                                                            <option id="dd0" value="">Select</option>
                                                            <option id="dd1" value="NewCreated" @if($common->newbusiness=='NewCreated') selected @endif>New Created Business</option>
                                                            <option id="dd2" value="Purchase" @if($common->newbusiness=='Purchase') selected @endif>Purchase Business</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="purchage_business box" @if($common->newbusiness=='Purchase')  style="display:block" @else style="display:none" @endif style="display:none" id="Purchase">
                                                    <div class="Branch">
                                                        <h1>Purchase Business Information</h1>
                                                    </div>
                                                    <div class="" id="disableboxes">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Purchase Date</label>
                                                            <div class="col-md-6">
                                                                <input style="width:200px;" type="text" class="form-control effective_date1" id="due_date03" name="due_date03" value="{{$common->due_date03}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Seller Name (Company Name)</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="sellername" value="{{$common->sellername}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Person Name </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="personname_saller" value="{{$common->personname_saller}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note </label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="purchagenote">{{$common->purchagenote}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Business Close Date</label>
                                                    <div class="col-md-3">
                                                        <input style="width:227px;" type="text" class="form-control" readonly id="due_date02" name="closedate" value="{{$common->closedate}}">
                                                    </div>
                                                    <div class="col-md-3" style="padding-left:0px;">
                                                        <select class="form-control fsc-input" id="salesbusiness" name="saleofbusiness">
                                                            <option id="dd0" value="">Select</option>
                                                            <option id="dd3" value="Sale" @if($common->saleofbusiness=='Sale') selected @endif>Sale of Business</option>
                                                            <option id="dd4" value="Transfer" @if($common->saleofbusiness=='Transfer') selected @endif>Transfer of Business</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="sale_business" @if($common->saleofbusiness=='Sale')  style="display:block" @else style="display:none" @endif>
                                                    <div class="Branch">
                                                        <h1>Sale of Business Information</h1>
                                                    </div>
                                                    <div class="" id="saleofbus">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Sales Date </label>
                                                            <div class="col-md-6">
                                                                <input style="width:200px;" type="text" class="form-control effective_date1" id="due_date04" name="due_date04" value="{{$common->due_date04}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Buyer Name:(Company Name) </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" value="{{$common->buyername}}" name="buyername">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Person Name: </label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="personname_buyer" value="{{$common->personname_buyer}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note: </label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="buyer_note">{{$common->buyer_note}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="transfer_business" @if($common->saleofbusiness=='Transfer')  style="display:block" @else style="display:none" @endif>
                                                    <div class="Branch">
                                                        <h1>Transfer of Business Information</h1>
                                                    </div>
                                                    <div class="">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Transfer Date: </label>
                                                            <div class="col-md-6">
                                                                <input style="width:200px;" type="text" style="width:200px;" class="form-control effective_date1" id="due_date06" name="due_date06" value="{{$common->due_date06}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Transfer To: </label>
                                                            <div class="col-md-6">
                                                                <input type="text" style="width:200px;" class="form-control effective_date1" id="due_date07" name="due_date07" value="{{$common->due_date07}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="tab6primary">
                                            <div class="Branch">
                                                <h1>Security Information</h1>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">UserName :</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="" readonly value="{{ Auth::user()->email }}" name="name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 1 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question1" id="question1" class="form-control">
                                                                <option @if(Auth::user()->question1=='What was your favorite place to visit as a child?') selected @endif value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                                <option @if(Auth::user()->question1=='Who is your favorite actor, musician, or artist?') selected @endif value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                                <option value="What is the name of your favorite pet?" @if(Auth::user()->question1=='What is the name of your favorite pet?') selected @endif>What is the name of your favorite pet?</option>
                                                                <option value="In what city were you born?" @if(Auth::user()->question1=='In what city were you born?') selected @endif>In what city were you born?</option>
                                                                <option value="What is the name of your first school?" @if(Auth::user()->question1=='What is the name of your first school?') selected @endif>What is the name of your first school?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 2 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question2" id="question2" class="form-control">
                                                                <option value="What is your favorite movie?" @if(Auth::user()->question2=='What is your favorite movie?') selected @endif>What is your favorite movie?</option>
                                                                <option value="What was the make of your first car?" @if(Auth::user()->question2=='What was the make of your first car?') selected @endif>What was the make of your first car?</option>
                                                                <option value="What is your favorite color?" @if(Auth::user()->question2=='What is your favorite color?') selected @endif>What is your favorite color?</option>
                                                                <option value="What is your father's middle name?" @if(Auth::user()->question2=='What is your fathers middle name?') selected @endif>What is your father's middle name?</option>
                                                                <option value="What is the name of your first grade teacher?" @if(Auth::user()->question2=='What is the name of your first grade teacher?') selected @endif>What is the name of your first grade teacher?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 3 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question3" id="question3" class="form-control">
                                                                @if (Auth::user()->question3)
                                                                    <option value="{{ Auth::user()->question3 }}">{{ Auth::user()->question3 }}</option>
                                                                @else
                                                                    <option value="">---Select---</option>
                                                                    @endelse
                                                                @endif
                                                                <option value="What was your high school mascot?" @if(Auth::user()->question3=='What was your high school mascot?') selected @endif>What was your high school mascot?</option>
                                                                <option value="Which is your favorite web browser?" @if(Auth::user()->question3=='Which is your favorite web browser?') selected @endif>Which is your favorite web browser?</option>
                                                                <option value="In what year was your father born?" @if(Auth::user()->question3=='In what year was your father born?') selected @endif>In what year was your father born?</option>
                                                                <option value="What is the name of your favorite childhood friend?" @if(Auth::user()->question3=='What is the name of your favorite childhood friend?') selected @endif>What is the name of your favorite childhood friend?</option>
                                                                <option value="What was your favorite food as a child?" @if(Auth::user()->question3=='What was your favorite food as a child?') selected @endif>What was your favorite food as a child?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="col-md-8">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="col-md-8">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 1:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer1" value="{{ Auth::user()->answer1 }}" type="text" id="answer1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 2:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer2" value="{{ Auth::user()->answer2 }}" type="text" id="answer2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 3:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer3" value="{{ Auth::user()->answer3 }}" type="text" id="answer3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade" id="tab2primary">
                                            <div class="Branch"><h1>Location Information</h1></div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-6">
                                                        <input type="checkbox" id="samecom" name="billingtoo11" value="1" @if(!empty($common->billingtoo11)) checked @else @endif onclick="FillBilling1(this.form)"><label for="samecom" class="fsc-form-label">&nbsp; Same as Company Address</label>
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                    <label class="control-label col-md-3">Physical Address 1 : </label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control fsc-input" @if(!empty($common->billingtoo11)) readonly @endif name="business_address_2" id="business_address" value="{{$common->business_address_2}}" placeholder="Address">
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                    <label class="control-label col-md-3">Physical Address 2 : </label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control fsc-input" name="business_address_3" @if(!empty($common->billingtoo11)) readonly @endif id="business_address_1" value="{{$common-> business_address_3 }}" placeholder="Address">
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} ">
                                                    <label class="control-label col-md-3">City / State / Zip : </label>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <input type="text" class="form-control fsc-input" @if(!empty($common->billingtoo11)) readonly @endif id="business_city_1" name="business_city_1" placeholder="business_city" value="{{$common-> business_city_1}}">
                                                                @if($errors->has('city'))
                                                                    <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <?php
                                                                    if($common->business_state_1 != '')
                                                                    {
                                                                    ?>
                                                                    <input class="form-control" VALUE="<?php echo $common->business_state_1;?>" name="business_state_1">
                                                                    <?PHP
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                    <select name="business_state_1" id="business_state_1" class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    <?PHP
                                                                    }


                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control fsc-input" id="bussiness_zip_1" @if(!empty($common->billingtoo11)) readonly @endif name="bussiness_zip_1" value="{{$common->bussiness_zip_1}}" placeholder="Zip">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                    <label class="control-label col-md-3">Country : </label>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="dropdown">
                                                                    <?php
                                                                    if($common->business_country_1 != '')
                                                                    {
                                                                    ?>
                                                                    <input class="form-control" readonly value="<?php echo $common->business_country_1;?>" name="business_country_1">
                                                                    <?PHP
                                                                    }
                                                                    ELSE
                                                                    {
                                                                    ?>
                                                                    <select name="business_country_1" id="business_country_1" class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        <option value='INDIA' @if($common->business_country_1=='IND') selected @endif>INDIA</option>
                                                                        <option value='USA' @if($common->business_country_1=='USA') selected @endif>USA</option>
                                                                    </select>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">County / Code : </label>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="dropdown">
                                                                    <select name="location_county" id="location_county" readonly class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        @foreach($taxstate as $v)
                                                                            <option value="{{$v->county}}" @if($v->county==$common->location_county) selected @endif>{{$v->county}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 ">
                                                                <input name="physical_county_no" readonly value="{{$common->physical_county_no}}" @if(!empty($common->billingtoo11)) readonly @endif type="text" id="physical_county_no" readonly class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane fade  newcheckbox" id="tab3primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                <div class="Branch">
                                                    <h1>Company Formation Information</h1>
                                                </div>
                                                <br/>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">State of Formation :</label>
                                                        <div class="col-md-2">
                                                            <input type="text" name="" value="{{$common->formation_register_entity}}" class="form-control" readonly/>
                                                        </div>

                                                        <label class="control-label col-md-2" style="text-align:left !important;width:19% !important;">Date of Incorporation :</label>
                                                        <div class="col-md-2" style="width:14.3% !important;">
                                                            <input type="text" id="due1_date02" name="due1_date02" readonly value="<?php if ($common->formation_date) {
                                                                echo date('m/d/Y', strtotime($common->formation_date));
                                                            } else {
                                                                echo '';
                                                            }?>" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Legal Name :</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="legal_name1" readonly="" name="legal_name1" value="{{$common->company_name}}"><input type="text" style="display:none" class="form-control" id="legal_name" readonly="" name="legal_name" value="FINANCIAL SERVICE CENTER INC">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Control Number :</label>
                                                        <div class="col-md-2">
                                                            <input type="text" class="form-control" maxlength="8" id="contact_number1" name="contact_number" value="{{$common->contact_number}}" style="text-transform: capitalize;">
                                                        </div>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" maxlength="8" id="" name="" placeholder="Corporation Status" READONLY value="<?php if ($common->record_status == '') {
                                                                echo 'Active Owes Curr. Yr. AR';
                                                            } else {
                                                                echo $common->record_status;
                                                            }?>" style="text-transform: capitalize;">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SOS - Certificate of Incorporation:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">

                                                                <?php if($common->soscertificate != '')
                                                                {?>
                                                                <div class="col-md-6">
                                                                    <a data-toggle="modal" num="SOS Certificate" class="btn btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtncertificate Pro-btn">Certificate of Corporation</a>
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>

                                                                <div class="col-md-6">

                                                                    <label class="file-upload btn btn-primary">
                                                                        <input type="file" name="soscertificate" id="soscertificate" class="form-control"/>Browse for file ... </label>
                                                                    <input type="hidden" name="soscertificate1" value="{{$common->soscertificate}}">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SOS - Articles Of Incorporation:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">

                                                                <?php if($common->sosaoi != '')
                                                                {?>
                                                                <div class="col-md-6">

                                                                    <a data-toggle="modal" num="SOS-AOI" class="btn btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnaoi Pro-btn">Articles of Incorporation</a>
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>

                                                                <div class="col-md-6">
                                                                    <label class="file-upload btn btn-primary">
                                                                        <input type="file" name="sosaoi" id="sosaoi" class="form-control"/>Browse for file ... </label>
                                                                    <input type="hidden" name="sosaoi1" value="{{$common->sosaoi}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Agent Name:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input name="agent_fname" value="{{$common->agent_fname}}" type="text" id="agent_fname" placeholder="First name" class="textonly form-control">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input name="agent_mname" value="{{$common->agent_mname}}" maxlength="1" type="text" placeholder="M" id="agent_mname" class="textonly form-control">
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input name="agent_lname" value="{{$common->agent_lname}}" type="text" placeholder="Last Name" id="agent_lname" class="textonly form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Address Same AS:</label>
                                                        <div class="col-md-3">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="address_type" id="address_type" class="form-control fsc-input address_type">
                                                                    <option value=''>Select</option>
                                                                    <option value='Business' @if($common->address_type=='Business') selected @endif >Business</option>
                                                                    <option value='Physical' @if($common->address_type=='Physical') selected @endif>Physical</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Registered Address:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input name="formation_address" value="<?php echo $common->formation_address;?>" <?php if($common->address_type == 'Business' || $common->address_type == 'Physical') {?> readonly <?php }?> type="text" id="formation_address" placeholder="Address"
                                                                           class="form-control formation_address">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} ">
                                                        <label class="control-label col-md-3">City / State / Zip :<?php echo $common->address_type;?> </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="text" class="form-control fsc-input formation_city" id="formation_city" name="formation_city" placeholder="City" value="<?php echo $common->formation_city;?>" <?php if($common->address_type == 'Business' || $common->address_type == 'Physical') {?> readonly <?php }?>>
                                                                </div>
                                                                <div class="col-md-3 formation_states">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <?php if($common->address_type == '')
                                                                        {?>
                                                                        <select name="formation_state" id="formation_state" class="form-control fsc-input formation_state hidee">
                                                                            <option value="">Select</option>
                                                                            <option value='AK' <?php if ($common->formation_state == 'AK') {
                                                                                echo 'Selected';
                                                                            }?>>AK
                                                                            </option>
                                                                            <option value='AS' <?php if ($common->formation_state == 'AS') {
                                                                                echo 'Selected';
                                                                            }?>>AS
                                                                            </option>
                                                                            <option value='AZ' <?php if ($common->formation_state == 'AZ') {
                                                                                echo 'Selected';
                                                                            }?>>AZ
                                                                            </option>
                                                                            <option value='AR' <?php if ($common->formation_state == 'AR') {
                                                                                echo 'Selected';
                                                                            }?>>AR
                                                                            </option>
                                                                            <option value='CA' <?php if ($common->formation_state == 'CA') {
                                                                                echo 'Selected';
                                                                            }?>>CA
                                                                            </option>
                                                                            <option value='CO' <?php if ($common->formation_state == 'CO') {
                                                                                echo 'Selected';
                                                                            }?>>CO
                                                                            </option>
                                                                            <option value='CT' <?php if ($common->formation_state == 'CT') {
                                                                                echo 'Selected';
                                                                            }?>>CT
                                                                            </option>
                                                                            <option value='DE' <?php if ($common->formation_state == 'DE') {
                                                                                echo 'Selected';
                                                                            }?>>DE
                                                                            </option>
                                                                            <option value='DC' <?php if ($common->formation_state == 'DC') {
                                                                                echo 'Selected';
                                                                            }?>>DC
                                                                            </option>
                                                                            <option value='FM' <?php if ($common->formation_state == 'FM') {
                                                                                echo 'Selected';
                                                                            }?>>FM
                                                                            </option>
                                                                            <option value='FL' <?php if ($common->formation_state == 'FL') {
                                                                                echo 'Selected';
                                                                            }?>>FL
                                                                            </option>
                                                                            <option value='GA' <?php if ($common->formation_state == 'GA') {
                                                                                echo 'Selected';
                                                                            }?>>GA
                                                                            </option>
                                                                            <option value='GU' <?php if ($common->formation_state == 'GU') {
                                                                                echo 'Selected';
                                                                            }?>>GU
                                                                            </option>
                                                                            <option value='HI' <?php if ($common->formation_state == 'HI') {
                                                                                echo 'Selected';
                                                                            }?>>HI
                                                                            </option>
                                                                            <option value='ID' <?php if ($common->formation_state == 'ID') {
                                                                                echo 'Selected';
                                                                            }?>>ID
                                                                            </option>
                                                                            <option value='IL' <?php if ($common->formation_state == 'IL') {
                                                                                echo 'Selected';
                                                                            }?>>IL
                                                                            </option>
                                                                            <option value='IN' <?php if ($common->formation_state == 'IN') {
                                                                                echo 'Selected';
                                                                            }?>>IN
                                                                            </option>
                                                                            <option value='IA' <?php if ($common->formation_state == 'IA') {
                                                                                echo 'Selected';
                                                                            }?>>IA
                                                                            </option>
                                                                            <option value='KS' <?php if ($common->formation_state == 'KS') {
                                                                                echo 'Selected';
                                                                            }?>>KS
                                                                            </option>
                                                                            <option value='KY' <?php if ($common->formation_state == 'KY') {
                                                                                echo 'Selected';
                                                                            }?>>KY
                                                                            </option>
                                                                            <option value='LA' <?php if ($common->formation_state == 'LA') {
                                                                                echo 'Selected';
                                                                            }?>>LA
                                                                            </option>
                                                                            <option value='ME' <?php if ($common->formation_state == 'ME') {
                                                                                echo 'Selected';
                                                                            }?>>ME
                                                                            </option>
                                                                            <option value='MH' <?php if ($common->formation_state == 'MH') {
                                                                                echo 'Selected';
                                                                            }?>>MH
                                                                            </option>
                                                                            <option value='MD' <?php if ($common->formation_state == 'MD') {
                                                                                echo 'Selected';
                                                                            }?>>MD
                                                                            </option>
                                                                            <option value='MA' <?php if ($common->formation_state == 'MA') {
                                                                                echo 'Selected';
                                                                            }?>>MA
                                                                            </option>
                                                                            <option value='MI' <?php if ($common->formation_state == 'MI') {
                                                                                echo 'Selected';
                                                                            }?>>MI
                                                                            </option>
                                                                            <option value='MN' <?php if ($common->formation_state == 'MN') {
                                                                                echo 'Selected';
                                                                            }?>>MN
                                                                            </option>
                                                                            <option value='MS' <?php if ($common->formation_state == 'MS') {
                                                                                echo 'Selected';
                                                                            }?>>MS
                                                                            </option>
                                                                            <option value='MO' <?php if ($common->formation_state == 'MO') {
                                                                                echo 'Selected';
                                                                            }?>>MO
                                                                            </option>
                                                                            <option value='MT' <?php if ($common->formation_state == 'MT') {
                                                                                echo 'Selected';
                                                                            }?>>MT
                                                                            </option>
                                                                            <option value='NE' <?php if ($common->formation_state == 'NE') {
                                                                                echo 'Selected';
                                                                            }?>>NE
                                                                            </option>
                                                                            <option value='NV' <?php if ($common->formation_state == 'NV') {
                                                                                echo 'Selected';
                                                                            }?>>NV
                                                                            </option>
                                                                            <option value='NH' <?php if ($common->formation_state == 'NH') {
                                                                                echo 'Selected';
                                                                            }?>>NH
                                                                            </option>
                                                                            <option value='NJ' <?php if ($common->formation_state == 'NJ') {
                                                                                echo 'Selected';
                                                                            }?>>NJ
                                                                            </option>
                                                                            <option value='NM' <?php if ($common->formation_state == 'NM') {
                                                                                echo 'Selected';
                                                                            }?>>NM
                                                                            </option>
                                                                            <option value='NY' <?php if ($common->formation_state == 'NY') {
                                                                                echo 'Selected';
                                                                            }?>>NY
                                                                            </option>
                                                                            <option value='NC' <?php if ($common->formation_state == 'NC') {
                                                                                echo 'Selected';
                                                                            }?>>NC
                                                                            </option>
                                                                            <option value='ND' <?php if ($common->formation_state == 'ND') {
                                                                                echo 'Selected';
                                                                            }?>>ND
                                                                            </option>
                                                                            <option value='MP' <?php if ($common->formation_state == 'MP') {
                                                                                echo 'Selected';
                                                                            }?>>MP
                                                                            </option>
                                                                            <option value='OH' <?php if ($common->formation_state == 'OH') {
                                                                                echo 'Selected';
                                                                            }?>>OH
                                                                            </option>
                                                                            <option value='OK' <?php if ($common->formation_state == 'OK') {
                                                                                echo 'Selected';
                                                                            }?>>OK
                                                                            </option>
                                                                            <option value='OR' <?php if ($common->formation_state == 'OR') {
                                                                                echo 'Selected';
                                                                            }?>>OR
                                                                            </option>
                                                                            <option value='PW' <?php if ($common->formation_state == 'PW') {
                                                                                echo 'Selected';
                                                                            }?>>PW
                                                                            </option>
                                                                            <option value='PA' <?php if ($common->formation_state == 'PA') {
                                                                                echo 'Selected';
                                                                            }?>>PA
                                                                            </option>
                                                                            <option value='PR' <?php if ($common->formation_state == 'PR') {
                                                                                echo 'Selected';
                                                                            }?>>PR
                                                                            </option>
                                                                            <option value='RI' <?php if ($common->formation_state == 'RI') {
                                                                                echo 'Selected';
                                                                            }?>>RI
                                                                            </option>
                                                                            <option value='SC' <?php if ($common->formation_state == 'SC') {
                                                                                echo 'Selected';
                                                                            }?>>SC
                                                                            </option>
                                                                            <option value='SD' <?php if ($common->formation_state == 'SD') {
                                                                                echo 'Selected';
                                                                            }?>>SD
                                                                            </option>
                                                                            <option value='TN' <?php if ($common->formation_state == 'TN') {
                                                                                echo 'Selected';
                                                                            }?>>TN
                                                                            </option>
                                                                            <option value='TX' <?php if ($common->formation_state == 'TX') {
                                                                                echo 'Selected';
                                                                            }?>>TX
                                                                            </option>
                                                                            <option value='UT' <?php if ($common->formation_state == 'UT') {
                                                                                echo 'Selected';
                                                                            }?>>UT
                                                                            </option>
                                                                            <option value='VT' <?php if ($common->formation_state == 'VT') {
                                                                                echo 'Selected';
                                                                            }?>>VT
                                                                            </option>
                                                                            <option value='VI' <?php if ($common->formation_state == 'VI') {
                                                                                echo 'Selected';
                                                                            }?>>VI
                                                                            </option>
                                                                            <option value='VA' <?php if ($common->formation_state == 'VA') {
                                                                                echo 'Selected';
                                                                            }?>>VA
                                                                            </option>
                                                                            <option value='WA' <?php if ($common->formation_state == 'WA') {
                                                                                echo 'Selected';
                                                                            }?>>WA
                                                                            </option>
                                                                            <option value='WV' <?php if ($common->formation_state == 'WV') {
                                                                                echo 'Selected';
                                                                            }?>>WV
                                                                            </option>
                                                                            <option value='WI' <?php if ($common->formation_state == 'WI') {
                                                                                echo 'Selected';
                                                                            }?>>WI
                                                                            </option>
                                                                            <option value='WY' <?php if ($common->formation_state == 'WY') {
                                                                                echo 'Selected';
                                                                            }?>>WY
                                                                            </option>
                                                                        </select>
                                                                        <?php
                                                                        }
                                                                        else
                                                                        {
                                                                        ?>
                                                                        <input type="text" class="form-control hidee1" readonly name="formation_state" style="display:none;" value="<?php echo $common->formation_state;?>" id="formation_state_1">
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control fsc-input formation_zip" id="formation_zip" name="formation_zip" value="<?php echo $common->formation_zip;?>" <?php if($common->address_type == 'Business' || $common->address_type == 'Physical') {?> readonly <?php }?>placeholder="Zip">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Renewal Date : </label>
                                                        <div class="col-md-2">
                                                            <?php

                                                            if($common->formation_yearbox == 1)
                                                            {
                                                            ?>
                                                            <input name="agent_expiredate" readonly value="Apr-01-2020" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
                                                            <?php
                                                            }
                                                            else if($common->formation_yearbox == 2)
                                                            {
                                                            ?>
                                                            <input name="agent_expiredate" readonly value="Apr-01-2021" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
                                                            <?php
                                                            }
                                                            else if($common->formation_yearbox == 3)
                                                            {
                                                            ?>
                                                            <input name="agent_expiredate" readonly value="Apr-01-2022" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
                                                            <?php
                                                            }
                                                            else
                                                            {
                                                            ?>
                                                            <input name="agent_expiredate" readonly value="" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
                                                            <?php
                                                            }
                                                            ?>


                                                        </div>
                                                        <!--<div class="">-->
                                                        <!--<div class="col-md-2">-->
                                                    <!--    <a style="display:block; cursor:pointer; margin-top:6px;"  href="https://financialservicecenter.net/VARR-Admin/workrecord?id=<?php echo $common->cid;?>" class="btn_new btn-renew">Renew Record</a>-->
                                                        <!--</div>-->
                                                        <!--<div class="col-md-2">-->
                                                        <!--<a style="display:block; margin-top:6px; padding: 0px; height: 34px; padding-top: 3px;" href="https://ecorp.sos.ga.gov/Account" target="_blank" class="btn_new btn-renew">Renew Here</a>-->
                                                        <!--</div>-->
                                                        <!--</div>-->

                                                    </div>


                                                </div>
                                                <div class="Branch">
                                                    <h1>Shareholder / Officer Information</h1>
                                                </div>
                                                <br/>
                                                <br/>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                    <input type="checkbox" id="billcheck" class="" name="billingtoo" onclick="FillBilling(this.form)" value="agent" @if($common->billingtoo=='agent') checked @endif><label for="billcheck" class="fsc-form-label"> &nbsp; Same as Agent</label>
                                                </div>
                                                <div class="">
                                                    <input class="form-control" name="agent1" type="hidden" id="agent1" class="textonly form-control"/>
                                                    <input class="form-control" name="agent2" type="hidden" id="agent2" class="textonly form-control"/>
                                                    <input class="form-control" name="agent3" type="hidden" id="agent3" class="textonly form-control"/>
                                                    <input class="form-control" name="agent4" type="hidden" id="agent4" class="textonly form-control"/>
                                                    <input class="form-control" name="agent5" type="hidden" id="agent5" class="textonly form-control"/>
                                                    <input class="form-control" name="agent6" type="hidden" id="agent6" class="textonly form-control"/>
                                                    <div class="">
                                                        <div class="share_tabs_main" style="margin-bottom:10px;>
                                             <div class=" share_tabs
                                                        ">
                                                        <div class="share_tab share_firstn" style="margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">First Name</label>
                                                        </div>
                                                        <div class="share_tab share_m" style="margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">M</label>
                                                        </div>
                                                        <div class="share_tab share_lastn" style="margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">Last Name</label>
                                                        </div>
                                                        <div class="share_tab share_position" style="margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">Position</label>
                                                        </div>
                                                        <div class="share_tab share_persentage" style="width: 9%; margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">Percentage</label>
                                                        </div>
                                                        <div class="share_tab share_date" style="margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">Effective Date</label>
                                                        </div>
                                                        <div class="share_tab share_date " style="width: 9%;margin: 10px 1% -10px 1%;">
                                                            <label style="font-size: 14px;color:#404040">Status</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php  $total_price = 0;?>
                                            @foreach($shareholder as $ak)
                                                @if($ak->agent_fname1==NULL)
                                                @else
                                                    <?php echo $ak->per;?>

                                                    <style>
                                                        .input_fields_wrap_shareholder {
                                                            display: none
                                                        }

                                                        ;
                                                    </style>
                                                    <div class="">
                                                        <div class="share_tabs_main" id="input_fields_wrap_2">
                                                            <div class="share_tabs {{ $errors->has('agent_fname1') ? ' has-error' : '' }}">
                                                                <div class="share_tab share_firstn">
                                                                    <input name="conid[]" value="{{$ak->id}}" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent_fname1[]" value="{{ $ak->agent_fname1}}" type="text" id="agent_fname1" placeholder="First name" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_m">
                                                                    <input class="form-control" name="agent_mname1[]" value="{{ $ak->agent_mname1}}" type="text" placeholder="M" id="agent_mname1" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_lastn">
                                                                    <input class="form-control" name="agent_lname1[]" value="{{ $ak->agent_lname1}}" type="text" placeholder="Last Name" id="agent_lname1" class="textonly form-control"/>
                                                                </div>
                                                                <div class="share_tab share_position">
                                                                    <select class="form-control" name="agent_position[]" id="agent_position11">
                                                                        <option value="">Position</option>

                                                                        <option @if($ak->agent_position=='CEO') Selected @endif  value="CEO">CEO</option>
                                                                        <option @if($ak->agent_position=='sec')  Selected @endif  value="sec">CEO / CFO / Sec.</option>
                                                                        <option @if($ak->agent_position=='CFO') Selected @endif value="CFO">CFO</option>
                                                                        <option @if($ak->agent_position=='Secretary') Selected @endif value="Secretary">Secretary</option>

                                                                    </select>
                                                                </div>
                                                                <div class="share_tab share_persentage" style="width: 9%;">
                                                                    <input name="agent_per[]" value="@if($ak->agent_per >0)  {{$ak->agent_per}} @endif" type="text" placeholder="" id="agent_per11" class="txtOnly form-control num numeric1"/>
                                                                    <div class="cc"></div>
                                                                </div>
                                                                <div class="share_tab share_date" style="width: 11%;">
                                                                    <?php $date = $ak->effective_date;?>
                                                                    <input name="effective_date[]" value="{{$date}}" placeholder="Effective Date" id="effective_date" class="form-control effective_date2"/>
                                                                </div>
                                                                <div class="share_tab share_date statusselectbox" style="width: 9%;">
                                                                    <select class="form-control greenText" onchange="this.className=this.options[this.selectedIndex].className"
                                                                            class="greenText" name="agentstatus[]" id="agentstatus">
                                                                        <option value="">Status</option>
                                                                        <option value="Active" @if($ak->agentstatus=='Active')  Selected @endif class="greenText">Active</option>
                                                                        <option value="In-Active" @if($ak->agentstatus=='In-Active')  Selected @endif class="redText">In-Active</option>
                                                                    </select>
                                                                </div>
                                                                <div class="share_add">
                                                                    <a href="#myModalk_{{$ak->id}}" id="add_row1" role="button" class="btn btn-danger removebtn" title="Add field" data-toggle="modal">Remove</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                            <div class="">
                                                <div class="mainfile">
                                                    <div class="input_fields_wrap input_fields_wrap_shareholder">
                                                        <div class="input_fields_wrap_1">
                                                            <input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                            <div class="share_tabs_main">
                                                                <div class="share_tabs ">
                                                                    <div class="share_tab share_firstn">
                                                                        <input name="agent_fname1[]" value="" type="text" id="agent_fname2" placeholder="First name" class="textonly form-control"/>
                                                                    </div>
                                                                    <div class="share_tab share_m">
                                                                        <input name="agent_mname1[]" value="" type="text" placeholder="M" id="agent_mname2" class="textonly form-control"/>
                                                                    </div>
                                                                    <div class="share_tab share_lastn">
                                                                        <input name="agent_lname1[]" value="" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control"/>
                                                                    </div>
                                                                    <div class="share_tab share_position">
                                                                        <select name="agent_position[]" id="agent_position" class="form-control agent_position">
                                                                            <option value="">Position</option>
                                                                            <option value="CEO">CEO</option>
                                                                            <option value="CFO">CFO</option>
                                                                            <option value="Secretary">Secretary</option>
                                                                            <option value="Sec">CEO / CFO / Sec.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="share_tab share_persentage" style="width: 9%;">
                                                                        <input name="agent_per[]" value="" type="text" placeholder="" id="agent_per" class="txtOnly form-control numeric1  num"/>
                                                                        <div class="cc"></div>
                                                                    </div>
                                                                    <div class="share_tab share_date" style="width: 11%;">
                                                                        <input name="effective_date[]" value="" maxlength="10" type="text" placeholder="Effective Date" id="effective_date" class="txtOnly form-control effective_date2"/>
                                                                    </div>
                                                                    <div class="share_tab share_date" style="width: 9%;">
                                                                        <select class="form-control greenText" onchange="this.className=this.options[this.selectedIndex].className"
                                                                                class="greenText" name="agentstatus[]" id="agentstatus">
                                                                            <option value="">Status</option>
                                                                            <option value="Active" class="greenText">Active</option>
                                                                            <option value="In-Active" class="redText">In-Active</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="share_add">
                                                                        <a href="javascript:void(0)" id="add_row1" class="btn btn-danger remove" title="Add field">Remove</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="share_tabs_main">
                                                <div class="share_tabs_other">
                                                    <div class="share_tab share_firstn">&nbsp;</div>
                                                    <div class="share_tab share_m">&nbsp;</div>
                                                    <div class="share_tab share_lastn">&nbsp;</div>
                                                    <div class="share_tab share_position">
                                                        <label class="share_total">Total :</label>
                                                    </div>
                                                    <div class="share_tab share_persentage">
                                                        <input name="total" style="width:67%;margin-left:1.3%; !important" value="" type="text" id="total" class="txtOnly form-control total" readonly/>
                                                        <p style="display:none;color:red;float: left;" id="t1">This should be not more then 100.00%</p>
                                                    </div>
                                                    <div class="share_tab share_remove">
                                                        <button type="button" id="add_row1" class="btn btn-success addbtn">ADD</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane fade" id="tab7primary">
                                        <div class="Branch"><h1>Admin Notes</h1></div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="input_fields_wrap">
                                                    <?php $l = 1; $notecon = count($admin_notes);?>
                                                    @if($notecon!=NULL)
                                                        @foreach($admin_notes as $notes)
                                                            <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                            <input name="usid[]" value="{{$notes->admin_id}}" type="hidden" placeholder="" id="usid" class="textonly form-control">
                                                            <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Note <?php echo $l; $l++;?>:</label>
                                                                <div class="col-md-6">
                                                                    <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                                </div>
                                                                @if($l==2)
                                                                    <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                @else
                                                                    <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>

                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control">
                                                        <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                        <input name="notetype[]" value="admin" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note :</label>
                                                            <div class="col-md-6">
                                                                <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            </div>
                                                            @if($l==2)

                                                            @else
                                                                <div class="col-md-1">

                                                                </div>
                                                            @endif
                                                        </div>

                                                    @endif

                                                </div>
                                                <div id="education_fields"></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="Branch"><h1>For FSC Only</h1></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">&nbsp;</div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="input_fields_wrap">
                                                    <?php $l = 1; $notecon = count($fsc);?>
                                                    @if($notecon!=NULL)
                                                        @foreach($fsc as $notes)
                                                            <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                            <input name="usid[]" value="{{$notes->admin_id}}" type="hidden" placeholder="" id="usid" class="textonly form-control">
                                                            <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Note <?php echo $l; $l++;?>:</label>
                                                                <div class="col-md-6">
                                                                    <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                                </div>
                                                                @if($l==2)
                                                                    <button class="btn btn-success" type="button" onclick="education_field();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                @else
                                                                    <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control">
                                                        <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                        <input name="notetype[]" value="fsc" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note :</label>
                                                            <div class="col-md-6">
                                                                <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-success" type="button" onclick="education_field();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            </div>
                                                            @if($l==2)

                                                            @else
                                                                <div class="col-md-1">

                                                                </div>
                                                            @endif
                                                        </div>

                                                    @endif
                                                </div>
                                                <div id="education_field"></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="Branch"><h1>For Client Only</h1></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">&nbsp;</div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="input_fields_wrap">
                                                    <?php $l = 1; $notecon = count($client);?>
                                                    @if($notecon!=NULL)
                                                        @foreach($client as $notes)
                                                            <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                            <input name="usid[]" value="{{$notes->admin_id}}" type="hidden" placeholder="" id="usid" class="textonly form-control">
                                                            <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Note <?php echo $l; $l++;?>:</label>
                                                                <div class="col-md-6">
                                                                    <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                                </div>
                                                                @if($l==2)
                                                                    <button class="btn btn-success" type="button" onclick="education_field1();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                @else
                                                                    <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <input name="usid[]" value="{{$common->cid}}" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control">
                                                        <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                        <input name="notetype[]" value="client" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note :</label>
                                                            <div class="col-md-6">
                                                                <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-success" type="button" onclick="education_field1();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            </div>
                                                            @if($l==2)

                                                            @else
                                                                <div class="col-md-1">

                                                                </div>
                                                            @endif
                                                        </div>

                                                    @endif
                                                </div>

                                                <div id="education_field1"></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="Branch"><h1>For Everybody</h1></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">&nbsp;</div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="input_fields_wrap">
                                                    <?php $l = 1; $notecon = count($every);?>
                                                    @if($notecon!=NULL)
                                                        @foreach($every as $notes)
                                                            <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                            <input name="usid[]" value="" type="hidden" placeholder="" id="usid" class="textonly form-control">
                                                            <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Note <?php echo $l; $l++;?>:</label>
                                                                <div class="col-md-6">
                                                                    <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                                </div>
                                                                @if($l==2)
                                                                    <button class="btn btn-success" type="button" onclick="education_field2();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                @else
                                                                    <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control">
                                                        <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                        <input name="notetype[]" value="Everybody" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Note :</label>
                                                            <div class="col-md-6">
                                                                <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-success" type="button" onclick="education_field2();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            </div>
                                                            @if($l==2)

                                                            @else
                                                                <div class="col-md-1">
                                                                </div>
                                                            @endif
                                                        </div>
                                                    @endif
                                                </div>
                                                <div id="education_field2"></div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="tab-pane fade" id="tab5primary">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="text-align:left; padding-left:15px;">
                                                <h1>Business License</h1>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group {{ $errors->has('business_license_jurisdiction') ? ' has-error' : '' }}" style="margin-bottom:5px;">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-3">
                                                        <label class="control-label" style="font-size:15px;">Jurisdiction :</label>
                                                        <select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">
                                                            <option value="">Select</option>
                                                            <option value="City" @if($common->business_license_jurisdiction=='City') selected @endif>City</option>
                                                            <option value="County" @if($common->business_license_jurisdiction=='County') selected @endif>County</option>
                                                        </select>
                                                        @if ($errors->has('business_license_jurisdiction'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('business_license_jurisdiction') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label id="city-change" class="control-label" style="display:none;">City :</label>
                                                        <label id="county-change" class="control-label">County :</label>
                                                        @if($common->business_license_jurisdiction=='County')
                                                            <select type="text" class="form-control" id="business_license2" name="business_license2">
                                                                <option value="">Select</option>
                                                                <option value="{{ $common->business_license2}}" @if($common->business_license2) selected @endif>{{ $common->business_license2}}</option>
                                                                @foreach($taxstate as $v)
                                                                    <option value="{{$v->county}}" @if($v->county==$common->business_license2) selected @endif>{{$v->county}}</option>
                                                                @endforeach
                                                            </select>
                                                        @elseif($common->business_license_jurisdiction=='City')
                                                            <input type="text" class="form-control" id="business_license3" name="business_license2" value="{{$common->business_license2}}">
                                                        @else
                                                            <select type="text" class="form-control" id="business_license2" name="business_license2" @if($common->business_license_jurisdiction=='City') style="display:none;" @endif>
                                                                <option value="">Select</option>
                                                                @foreach($taxstate as $v)
                                                                    <option value="{{$v->county}}" @if($v->county==$common->business_license2) selected @endif>{{$v->county}}</option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                        <div id="business_license4"></div>
                                                        <div id="business_license5"></div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label id="city-change1" class="control-label" style="display:none;">City # :</label>
                                                        <label id="county-change1" class="control-label">County # :</label>
                                                        <input name="business_license1" placeholder="" value="{{ $common->business_license1}}" type="text" id="business_license1" class="form-control"/>
                                                        @if ($errors->has('business_license1'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('business_license1') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label id="county-change1" class="control-label">License # :</label>
                                                        <?php
                                                        if(isset($buslicense) != '')
                                                        {
                                                        ?>
                                                        <input name="business_license3" placeholder="License No" value="{{$buslicense->license_no}}" type="text" id="business_license1" class="form-control" readonly/>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <input name="business_license3" placeholder="License No" value="{{$common->business_license3}}" type="text" id="business_license1" class="form-control" readonly/>
                                                        <?php
                                                        }
                                                        ?>

                                                        @if ($errors->has('business_license3'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('business_license3') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Expire Date :</label>
                                                        <?php
                                                        if(isset($buslicense->license_year) != '' && isset($buslicense->license_year) == '2020')
                                                        {
                                                        ?>
                                                        <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="Mar-31-2021" readonly>
                                                        <?php
                                                        }
                                                        else if(isset($buslicense->license_year) != '' && isset($buslicense->license_year) == '2021')
                                                        {
                                                        ?>
                                                        <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="Mar-31-2021" readonly>
                                                        <?php
                                                        }
                                                        else if(isset($buslicense->license_year) != '' && isset($buslicense->license_year) == '2022')
                                                        {
                                                        ?>
                                                        <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="Mar-31-2022" readonly>
                                                        <?php
                                                        }
                                                        else if(isset($buslicense->license_year) != '' && isset($buslicense->license_year) == '2023')
                                                        {
                                                        ?>
                                                        <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="Mar-31-2023" readonly>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="{{ $common->due_date2}}" readonly>
                                                        <?php
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-6">
                                                        <label class="control-label" style="font-size:15px;padding:0;">Note :</label>
                                                        <?php
                                                        if(isset($buslicense->license_year) != '')
                                                        {
                                                        ?>
                                                        <input name="notes" placeholder="Note" value="{{$buslicense->license_note}}" type="text" id="" class="form-control" readonly>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <input name="notes" placeholder="Note" value="{{$common->notes}}" type="text" id="" class="form-control" readonly>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                <?php $id1 = $common->business_license_jurisdiction;?>
                                                <!--<div class="col-md-2">-->
                                                    <!--   <label></label>-->
                                                    <!--   <a class="btn_new btn-view-license">License History</a>-->
                                                    <!--</div>-->
                                                    <div class="col-md-2">
                                                        <label></label>

                                                        <?php
                                                        if(isset($buslicense) != '')
                                                        {
                                                        ?>
                                                        <a style="display:block;" data-toggle="modal" class="btn_new openBtn007 btn-view-license">View License</a>

                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <a style="display:block;" data-toggle="modal" num="{{$id1}}" class="btn_new openBtn btn-view-license">View License</a>
                                                        <?php
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="col-md-2">
                                                        @foreach($upload as $up)
                                                            @if($up->upload_name==$id1)
                                                                <style>.nn {
                                                                        display: none !important
                                                                    }</style>
                                                                <label></label>
                                                                <a style="display:block;" href="{{$up->website_link}}" target="_blank" class="btn_new btn-renew">Renew Here</a>
                                                            @endif
                                                        @endforeach
                                                        <label></label>
                                                        <a href="#" class="btn_new btn-renew nn">Renew Here</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="text-align:left; padding-left:15px;">
                                                <h1>Professional License</h1>
                                            </div>
                                        </div>
                                        <?php $i = 0; $pro = count($admin_professional);?>
                                        @if($pro != NULL)
                                            @foreach($admin_professional as $ak1)
                                                <?php $i; $i++;?>
                                                @if($ak1->profession==NULL)
                                                @else
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="field_wrapper">
                                                            <div id="field0">
                                                                <div class="professional_tabs_main">
                                                                    <div class="professional_tabs">
                                                                        <div class="professional_tab professional_profession">
                                                                            <label>Profession :</label>
                                                                            <select type="text" class="form-control-insu" id="" name="profession[]">
                                                                                <option value="">Select</option>
                                                                                <option value="CPA" @if($ak1->profession=='CPA') selected @endif>CPA</option>
                                                                                <option value="ERO" @if($ak1->profession=='ERO') selected @endif>ERO</option>
                                                                                <option value="Insurance Agent" @if($ak1->profession=='Insurance Agent') selected @endif>Insurance Agent</option>
                                                                                <option value="Insurance Broker" @if($ak1->profession=='Insurance Broker') selected @endif>Insurance Broker</option>
                                                                                <option value="MLO" @if($ak1->profession=='MLO') selected @endif>MLO</option>
                                                                                <option value="Mortgage Broker" @if($ak1->profession=='Mortgage Broker') selected @endif>Mortgage Broker</option>
                                                                                <option value="PTIN" @if($ak1->profession=='PTIN') selected @endif>PTIN</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="professional_tab professional_state">
                                                                            <label>State :</label>
                                                                            <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                                                @if(empty($ak1->profession_state)) @else
                                                                                    <option value="{{$ak1->profession_state}}">{{$ak1->profession_state}}</option>
                                                                                @endif
                                                                                <option value="AK">AK</option>
                                                                                <option value="AS">AS</option>
                                                                                <option value="AZ">AZ</option>
                                                                                <option value="AR">AR</option>
                                                                                <option value="CA">CA</option>
                                                                                <option value="CO">CO</option>
                                                                                <option value="CT">CT</option>
                                                                                <option value="DE">DE</option>
                                                                                <option value="DC">DC</option>
                                                                                <option value="FM">FM</option>
                                                                                <option value="FL">FL</option>
                                                                                <option value="GA">GA</option>
                                                                                <option value="GU">GU</option>
                                                                                <option value="HI">HI</option>
                                                                                <option value="ID">ID</option>
                                                                                <option value="IL">IL</option>
                                                                                <option value="IN">IN</option>
                                                                                <option value="IA">IA</option>
                                                                                <option value="KS">KS</option>
                                                                                <option value="KY">KY</option>
                                                                                <option value="LA">LA</option>
                                                                                <option value="ME">ME</option>
                                                                                <option value="MH">MH</option>
                                                                                <option value="MD">MD</option>
                                                                                <option value="MA">MA</option>
                                                                                <option value="MI">MI</option>
                                                                                <option value="MN">MN</option>
                                                                                <option value="MS">MS</option>
                                                                                <option value="MO">MO</option>
                                                                                <option value="MT">MT</option>
                                                                                <option value="NE">NE</option>
                                                                                <option value="NV">NV</option>
                                                                                <option value="NH">NH</option>
                                                                                <option value="NJ">NJ</option>
                                                                                <option value="NM">NM</option>
                                                                                <option value="NY">NY</option>
                                                                                <option value="NC">NC</option>
                                                                                <option value="ND">ND</option>
                                                                                <option value="MP">MP</option>
                                                                                <option value="OH">OH</option>
                                                                                <option value="OK">OK</option>
                                                                                <option value="OR">OR</option>
                                                                                <option value="PW">PW</option>
                                                                                <option value="PA">PA</option>
                                                                                <option value="PR">PR</option>
                                                                                <option value="RI">RI</option>
                                                                                <option value="SC">SC</option>
                                                                                <option value="SD">SD</option>
                                                                                <option value="TN">TN</option>
                                                                                <option value="TX">TX</option>
                                                                                <option value="UT">UT</option>
                                                                                <option value="VT">VT</option>
                                                                                <option value="VI">VI</option>
                                                                                <option value="VA">VA</option>
                                                                                <option value="WA">WA</option>
                                                                                <option value="WV">WV</option>
                                                                                <option value="WI">WI</option>
                                                                                <option value="WY">WY</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="professional_tab professional_effective">
                                                                            <label>Effective Date :</label>
                                                                            <input name="profession_effective_date[]" placeholder="Effective Date" value="{{$ak1->profession_epr_date1}}" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                                        </div>
                                                                        <div class="professional_tab professional_license">
                                                                            <label>License # :</label>
                                                                            <input name="profession_license[]" placeholder="License" value="{{$ak1->profession_license}}" type="text" id="profession_license" class="form-control-insu">
                                                                        </div>
                                                                        <div class="professional_tab professional_expire">
                                                                            <label>Expire Date :</label>
                                                                            @foreach($upload as $up)
                                                                                @if($up->upload_name==$ak1->pro_id)
                                                                                    <label></label>
                                                                                    <input name="profession_epr_date[]" placeholder="Expire Date" value="{{$up->expired_date}}" type="text" id="profession_epr_date" class="form-control-insu" readonly>
                                                                                    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                            display: none
                                                                                        }</style>
                                                                                @endif
                                                                            @endforeach
                                                                            <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}" readonly>
                                                                        </div>
                                                                        <div class="professional_add">
                                                                            @if($i==2)
                                                                            @else
                                                                                <a href="#myModalm99_{{$ak1->id}}" id="remove_button_pro" role="button" class="btn_new btn-remove" data-toggle="modal"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                            @endif
                                                                        </div>
                                                                        <div class="professional_tab professional_note">
                                                                            <label>Note :</label>
                                                                            <input name="profession_note[]" placeholder="Note" value="{{$ak1->profession_note}}" type="text" id="profession_note" class="form-control-insu">
                                                                            <input name="profession_id[]" placeholder="Expire Date" value="{{$ak1->id}}" type="hidden" class="form-control-insu">
                                                                        </div>
                                                                        <div class="professional_tab professional_effective">
                                                                            <label></label>
                                                                            <a data-toggle="modal-history" num="{{$ak1->profession.'-'.$ak1->profession_state.'-'.$ak1->profession_license}}" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
                                                                        </div>
                                                                        <div class="professional_tab professional_license">
                                                                            @foreach($upload as $up)
                                                                                @if($up->upload_name==$ak1->pro_id)
                                                                                    <label></label>
                                                                                    <a data-toggle="modal" num="{{$ak1->profession.'-'.$ak1->profession_state}}" class="btn_new btn-view-license openBtn Pro-btn">View License</a>
                                                                                    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                            display: none
                                                                                        }</style>
                                                                                @endif
                                                                            @endforeach
                                                                            <label class="btn-success-{{$ak1->pro_id}}"></label>
                                                                            <a class="btn_new btn-view-license Pro-btn  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">File Not Upload</a>
                                                                        </div>
                                                                        <div class="professional_tab professional_expire">
                                                                            @foreach($upload as $up)
                                                                                @if($up->upload_name==$ak1->pro_id)
                                                                                    <label></label>
                                                                                    <a href="{{$up->website_link}}" class="btn_new btn-renew" target='_blank'>Renew Now</a>
                                                                                    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                            display: none
                                                                                        }</style>
                                                                                @endif
                                                                            @endforeach
                                                                            <label class="btn-success-{{$ak1->pro_id}}"></label>
                                                                            <a href="javascript:void(0);" class="btn_new btn-renew btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">Renew Now</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="field_wrapper">
                                                    <div id="field0">
                                                        <div class="professional_tabs_main">
                                                            <div class="professional_tabs">
                                                                <div class="professional_tab professional_profession">
                                                                    <label>Profession :</label>
                                                                    <select type="text" class="form-control-insu" id="" name="profession[]">
                                                                        <option value="">Select</option>
                                                                        <option value="ERO">ERO</option>
                                                                        <option value="CPA">CPA</option>
                                                                        <option value="Mortgage Broker">Mortgage Broker</option>
                                                                        <option value="PTIN">PTIN</option>
                                                                        <option value="Insurance Agent">Insurance Agent</option>
                                                                        <option value="Insurance Broker">Insurance Broker</option>
                                                                        <option value="MLO">MLO</option>
                                                                    </select>
                                                                </div>
                                                                <div class="professional_tab professional_state">
                                                                    <label>State :</label>
                                                                    <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                                        <option value="AK">AK</option>
                                                                        <option value="AS">AS</option>
                                                                        <option value="AZ">AZ</option>
                                                                        <option value="AR">AR</option>
                                                                        <option value="CA">CA</option>
                                                                        <option value="CO">CO</option>
                                                                        <option value="CT">CT</option>
                                                                        <option value="DE">DE</option>
                                                                        <option value="DC">DC</option>
                                                                        <option value="FM">FM</option>
                                                                        <option value="FL">FL</option>
                                                                        <option value="GA">GA</option>
                                                                        <option value="GU">GU</option>
                                                                        <option value="HI">HI</option>
                                                                        <option value="ID">ID</option>
                                                                        <option value="IL">IL</option>
                                                                        <option value="IN">IN</option>
                                                                        <option value="IA">IA</option>
                                                                        <option value="KS">KS</option>
                                                                        <option value="KY">KY</option>
                                                                        <option value="LA">LA</option>
                                                                        <option value="ME">ME</option>
                                                                        <option value="MH">MH</option>
                                                                        <option value="MD">MD</option>
                                                                        <option value="MA">MA</option>
                                                                        <option value="MI">MI</option>
                                                                        <option value="MN">MN</option>
                                                                        <option value="MS">MS</option>
                                                                        <option value="MO">MO</option>
                                                                        <option value="MT">MT</option>
                                                                        <option value="NE">NE</option>
                                                                        <option value="NV">NV</option>
                                                                        <option value="NH">NH</option>
                                                                        <option value="NJ">NJ</option>
                                                                        <option value="NM">NM</option>
                                                                        <option value="NY">NY</option>
                                                                        <option value="NC">NC</option>
                                                                        <option value="ND">ND</option>
                                                                        <option value="MP">MP</option>
                                                                        <option value="OH">OH</option>
                                                                        <option value="OK">OK</option>
                                                                        <option value="OR">OR</option>
                                                                        <option value="PW">PW</option>
                                                                        <option value="PA">PA</option>
                                                                        <option value="PR">PR</option>
                                                                        <option value="RI">RI</option>
                                                                        <option value="SC">SC</option>
                                                                        <option value="SD">SD</option>
                                                                        <option value="TN">TN</option>
                                                                        <option value="TX">TX</option>
                                                                        <option value="UT">UT</option>
                                                                        <option value="VT">VT</option>
                                                                        <option value="VI">VI</option>
                                                                        <option value="VA">VA</option>
                                                                        <option value="WA">WA</option>
                                                                        <option value="WV">WV</option>
                                                                        <option value="WI">WI</option>
                                                                        <option value="WY">WY</option>
                                                                    </select>
                                                                </div>
                                                                <div class="professional_tab professional_effective">
                                                                    <label>Effective Date :</label>
                                                                    <input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                                </div>
                                                                <div class="professional_tab professional_license">
                                                                    <label>License # :</label>
                                                                    <input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu">
                                                                </div>
                                                                <div class="professional_tab professional_expire">
                                                                    <label>Expire Date :</label>
                                                                    <label></label>
                                                                    <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>
                                                                </div>
                                                                <div class="professional_add">
                                                                </div>
                                                                <div class="professional_tab professional_note">
                                                                    <label>Note :</label>
                                                                    <input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu">
                                                                    <input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" class="form-control-insu">
                                                                </div>
                                                                <div class="professional_tab professional_effective">
                                                                    <label></label>
                                                                    <a data-toggle="modal-history" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
                                                                </div>
                                                                <div class="professional_tab professional_license">
                                                                    <label class="btn-success"></label>
                                                                    <a class="btn_new btn-view-license Pro-btn">File Not Upload</a>
                                                                </div>
                                                                <div class="professional_tab professional_expire">
                                                                    <label class="btn-success"></label>
                                                                    <a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="field_wrapper">
                                                <div id="field0">
                                                    <div class="professional_tabs_main" id="professional_tabs_main">
                                                        <div class="professional_tabs" style="background:transparent;border: transparent;">
                                                            <div class="professional_add">
                                                                <a href="javascript:void(0);" id="add_button_pro" class="btn_new btn-add"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if($common->business_cat_id=='14')
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="Branch" style="text-align:left; padding-left:15px;">
                                                    <h1>Tobacco License</h1>
                                                </div>
                                            </div>
                                            <br/>
                                            @foreach($position as $position1)
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group" style="margin-bottom:5px;">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-md-3">
                                                                <label class="control-label" style="font-size:15px;">Jurisdiction :</label>
                                                                <input type="text" class="form-control" value="{{$position1->type}}" name="business_license_jurisdiction1">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label id="city-change" class="control-label">State :</label>
                                                                <input type="text" class="form-control" id="business_licensestate" name="business_licensestate" value="{{$position1->state}}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label id="county-change1" class="control-label">Authority Name :</label>
                                                                <input name="authorityname" placeholder="" value="{{$position1->authorityname}}" type="text" id="authorityname" class="form-control"/>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label id="county-change1" class="control-label">License # :</label>
                                                                <input name="business_license3" placeholder="" value="{{$common->business_license3}}" type="text" id="business_license1" class="form-control"/>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="control-label">Expire Date :</label>
                                                                <input type="text" class="form-control" id="due_dateduedate1" name="due_dateduedate1" value="{{$position1->date}}-{{$position1->duedate1}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-md-6">
                                                                <label class="control-label" style="font-size:15px;padding:0;">Note :</label>
                                                                <input name="" placeholder="Note" value="{{$position1->date}}" type="text" id="" class="form-control">
                                                            </div>
                                                            <?php $id1 = $common->business_license_jurisdiction;?>
                                                            <div class="col-md-2">
                                                                <label></label>
                                                                <a class="btn_new btn-view-license">License History</a>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label></label>
                                                                <a style="display:block;" data-toggle="modal" num="{{$id1}}" class="btn_new openBtn btn-view-license">View License</a>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label></label>
                                                                <a style="display:block;" href="https://{{$position1->renewalwebsite}}" target="_blank" class="btn_new btn-renew">Renew Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>


                                    <div class="tab-pane fade" id="tab11primary">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch">
                                                <h1>For Income-Tax Purpose, Select Type of Corporation :</h1>
                                            </div>


                                            <div class="form-group">
                                                <div class="">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fsc-form-row">
                                                        <label class="col-md-4 fsc-form-label pr-0" for="" style="padding-top:8px; width:33%;">Type of Corp :</label>
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pr-0 pl-0">


                                                            <input name="typeofcorp" type="text" placeholder="Type of Entity" class="form-control" id="typeofservice22" value="{{$common->interview_type_of_entity}}" readonly/>
                                                            <input type="hidden" class="form-control-insu" id="typeofservice33" value="{{$common->formation_register_entity}}" readonly>


                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 fsc-form-row">
                                                        <div class="row">
                                                            <label class="col-md-6 fsc-form-label" for="" style="padding-top:8px;">Form No : </label>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pl-0 ">
                                                                <input type="text" name="typeofcorp1" class="form-control" id="typeofcorps" placeholder="Enter Form No" value="{{$common->typeofcorp1}}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="col-md-6 fsc-form-label pr-0 pl-0" for="" style="padding-top:8px;">S election effective Date :</label>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pr-0 pl-0 fsc-form-col fsc-element-margin">
                                                            <input name="typeofcorp_effect" value="<?php echo date('m-d-Y', strtotime($common->formation_start_date));?>" readonly maxlength="12" type="text" placeholder="" id="typeofcorp_effect" class="form-control  effective_date1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="background:#b1cbfb;">
                                                <div class="col-md-3" style="text-align:left;"><h1>Federal / State</h1></div>
                                                <div class="col-md-6"><h1>Income Tax </h1></div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group {{ $errors->has('federal_id') ? ' has-error' : '' }}">

                                                    <div class="col-md-4">
                                                        <label class="control-label">Federal ID :</label>
                                                        <input type="text" class="form-control txtOnly federal_id" id="federal_id" maxlength="9" name="federal_id" value="{{ $common->federal_id}}">
                                                        @if($errors->has('federal_id'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('federal_id') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="control-label">Form To File (Federal) :</label>
                                                        <input type="text" class="form-control" id="type_form_file" name="type_form" value="{{$common->type_form}}" readonly>
                                                        @if($errors->has('type_form'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('type_form') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="control-label">Due Date :</label>
                                                        <input type="text" class="form-control" id="due_date_1" name="due_date" readonly value="{{$common->due_date}}">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="control-label">Extension Due Date :</label>
                                                        <input type="text" class="form-control" id="extension_due_date_1" readonly name="extension_due_date" value="{{$common->extension_due_date}}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <label class="control-label">State :</label>
                                                        <input type="text" readonly="" class="form-control" id="state_id_2" name="state" value="GA">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">State ID :</label>
                                                        <input type="text" class="form-control txtOnly" id="state_id" maxlength="9" name="state_id" value="{{$common->state_id}}">
                                                        @if($errors->has('state_id'))
                                                            <span class="help-block">
                                                 <strong>{{ $errors->first('state_id') }}</strong>
                                                 </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="control-label">Form To File (State) :</label>
                                                        <input type="text" class="form-control" id="type_form_file2" name="type_form_file2" value="{{$common->type_form_file2}}" readonly>
                                                        @if ($errors->has('type_form'))
                                                            <span class="help-block">
                                             <strong>{{ $errors->first('type_form') }}</strong>
                                             </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Due Date :</label>
                                                        <input type="text" class="form-control" id="due_date_2" name="due_date_2" value="{{$common->due_date_2}}" readonly>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">Extension Due Date :</label>
                                                        <input type="text" class="form-control" id="extension_due_date_2" name="extension_due_date2" value="{{$common->extension_due_date2}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        if($common->client_payroll == 1)
                                        {
                                        ?>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="background:#b3fff8;">
                                                <div class="col-md-3" style="text-align:left;"><h1>Federal / State</h1></div>
                                                <div class="col-md-6"><h1>Payroll Tax</h1></div>
                                            </div>
                                            <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">
                                                <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <h3>Federal Withholding & FICA Tax</h3>
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu"> Name :</label>
                                                                    <input type="text" class="form-control-insu" id="federal_name" name="federal_name" value="IRS" readonly>
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                </div>

                                                                <div class="col-md-3" style="width:24.5%;">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="federal_ga_dept" name="federal_ga_dept" value="Internal Revenue Service" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Form No. :</label>
                                                                    <select type="text" class="form-control-insu" id="form_number_1" name="form_number_1">
                                                                        <option value="">Select</option>
                                                                        <option value="Form-941" @if($common->form_number_1=='Form-941') selected @endif>Form-941</option>
                                                                        <option value="Form-944" @if($common->form_number_1=='Form-944') selected @endif>Form-944</option>
                                                                    </select>
                                                                </div>

                                                                <!--<div class="col-md-2 business2">-->
                                                                <!--	<label class="control-label-insu">Quarter Period:</label>-->
                                                                <!--	<select type="text" class="form-control-insu" id="quarter_type" name="quarter_type">-->
                                                                <!--	    <option value="">Select</option>-->
                                                            <!--	    <option value="1st Qtr" @if($common->quarter_type=='1st Qtr') selected @endif>1st Qtr</option>-->
                                                            <!--		<option value="2nd Qtr" @if($common->quarter_type=='2nd Qtr') selected @endif>2nd Qtr</option>-->
                                                            <!--		<option value="3rd Qtr" @if($common->quarter_type=='3rd Qtr') selected @endif>3rd Qtr</option>-->
                                                            <!--		<option value="4th Qtr" @if($common->quarter_type=='4th Qtr') selected @endif>4th Qtr</option>-->
                                                                <!--	</select>-->
                                                                <!--</div>-->

                                                                <div class="col-md-2 business2" @if($common->federal_frequency =='Annually') style="display:none;" @endif style="width:13%;">
                                                                    <label class="control-label-insu">Quarter Period:</label>
                                                                    <select type="text" class="form-control-insu" id="quarter_type" name="quarter_type">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" @if($common->quarter_type=='1st Qtr') selected @endif>1st Qtr.</option>
                                                                        <option value="2nd Qtr" @if($common->quarter_type=='2nd Qtr') selected @endif>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" @if($common->quarter_type=='3rd Qtr') selected @endif>3rd Qtr.</option>
                                                                        <option value="4th Qtr" @if($common->quarter_type=='4th Qtr') selected @endif>4th Qtr.</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2" style="width:13.5%;">
                                                                    <label class="control-label-insu">Period End Date</label>
                                                                    <input type="text" class="form-control-insu" id="quarter_date" name="quarter_date" value="{{$common->quarter_date}}" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Filing Frequency</label>
                                                                    <input type="text" class="form-control-insu" id="federal_frequency11" name="federal_frequency" value="{{$common->federal_frequency}}" readonly>
                                                                    @if ($errors->has('federal_frequency'))
                                                                        <span class="help-block">
															<strong>{{ $errors->first('federal_frequency') }}</strong>
															</span>
                                                                    @endif
                                                                </div>

                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="federal_frequency_due_date" name="federal_frequency_due_date" value="{{$common->federal_frequency_due_date}}" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">
                                                                <div class="col-md-4" style="width:35.5%;">
                                                                    <label class="control-label-insu">Federal Note :</label>
                                                                    <input type="text" class="form-control-insu" id="federal_note_2" name="federal_note_2" value="{{$common->federal_note_2}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">EFTPS PIN :</label>
                                                                    <input type="text" class="form-control-insu" id="eptpspin" name="eptpspin" value="{{$common->eptpspin}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">PW </label>
                                                                    <input type="text" class="form-control-insu" id="pay_pw" name="pay_pw" value="{{$common->pay_pw}}">
                                                                </div>

                                                            <!--                                       <div class="col-md-2 federal_payment_frequency_941" @if($common->federal_payment_frequency=='Annually') style="display:none;" @endif>-->
                                                                <!--	<label class="control-label-insu">Payment Frequency </label>-->
                                                                <!--	<select type="text" class="form-control-insu" id="federal_payment_frequency" name="federal_payment_frequency">-->
                                                                <!--	    <option value="">-Select-</option>-->
                                                            <!--	    <option value="Monthly"   @if($common->federal_payment_frequency =='Monthly') selected @endif>Monthly</option>-->
                                                            <!--	    <option value="Quarterly" @if($common->federal_payment_frequency =='Quarterly') selected @endif>Quarterly</option>-->
                                                                <!--    </select>-->
                                                                <!--</div>-->

                                                            <!--<div class="col-md-2 federal_payment_frequency_944"  @if($common->federal_payment_frequency=='Annually') style="display:block;"  @else   style="display:none;" @endif>-->
                                                                <!--	<label class="control-label-insu">Payment Frequency </label>-->
                                                                <!--	<select type="text" class="form-control-insu " id="federal_payment_frequency_annually" name="federal_payment_frequency">-->
                                                                <!--	    <option value="">-Select-</option>-->
                                                            <!--	    <option value="Quarterly" @if($common->federal_payment_frequency =='Quarterly') selected @endif>Quarterly</option>-->
                                                            <!--	    <option value="Annually" @if($common->federal_payment_frequency =='Annually') selected @endif>Annually</option>-->
                                                                <!--    </select>-->
                                                                <!--</div>-->


                                                                <div class="col-md-2 federal_payment_frequency_941" @if($common->federal_payment_frequency=='Annually') style="display:none;" @endif>
                                                                    <label class="control-label-insu">Payment Frequency </label>
                                                                    <select type="text" class="form-control-insu" id="federal_payment_frequency" name="federal_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Monthly" @if($common->federal_payment_frequency =='Monthly') selected @endif>Monthly</option>
                                                                        <option value="Quarterly" @if($common->federal_payment_frequency =='Quarterly') selected @endif>Quarterly</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2 federal_payment_frequency_944" @if($common->federal_payment_frequency=='Annually') style="display:block;" @else   style="display:none;" @endif>
                                                                    <label class="control-label-insu">Payment Frequency </label>
                                                                    <select type="text" class="form-control-insu " id="federal_payment_frequency_annually" name="federal_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Quarterly" @if($common->federal_payment_frequency =='Quarterly') selected @endif>Quarterly</option>
                                                                        <option value="Annually" @if($common->federal_payment_frequency =='Annually') selected @endif>Annually</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Payment Due Date </label>
                                                                    <input type="text" class="form-control-insu form-control" name="federal_payment_frequency_date" id="federal_payment_frequency_date2" value="{{$common->federal_payment_frequency_date}}" readonly>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <h3>Federal Unemployment Tax (FUTA)</h3>
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu">Name :</label>
                                                                    <input type="text" class="form-control-insu" name="payroll_name" value="IRS" readonly>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="ga_dept" name="ga_dept" value="Internal Revenue Service" readonly>
                                                                </div>

                                                                <div class="col-md-3" style="width:26.5%;">
                                                                    <label class="control-label-insu">Form No. :</label>
                                                                    <input type="text" class="form-control-insu" id="form_number_2" readonly name="form_number_2" value="Form-940">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Filing Frequency</label>
                                                                    <input type="text" class="form-control-insu" name="frequency" id="frequency" value="Annually" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="frequency_due_date" name="frequency_due_date" value="<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>" readonly>
                                                                    @if ($errors->has('frequency_due_date'))
                                                                        <span class="help-block">
															<strong>{{ $errors->first('frequency_due_date') }}</strong>
															</span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">

                                                                <div class="col-md-8" style="width:68.5%;">
                                                                    <label class="control-label-insu">Note :</label>
                                                                    <input type="text" class="form-control-insu" id="federal_note" name="federal_note" value="{{$common->federal_note}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Payment Frequency </label>
                                                                    <select type="text" class="form-control-insu" name="payment_frequency" id="payment_frequency">
                                                                        <option value="Quarterly" @if($common->payment_frequency =='Quarterly') selected @endif>Quarterly</option>
                                                                        <option value="Annually" @if($common->payment_frequency =='Annually') selected @endif>Annually</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Payment Due Date </label>
                                                                    <input type="text" class="form-control-insu form-control" name="payment_frequency_date" id="payment_frequency_date" value="{{$common->payment_frequency_date}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <h3>State Withholding</h3>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu">Name :</label>
                                                                    <input type="text" class="form-control-insu" name="fedral_state_1" value="GA" readonly>
                                                                </div>

                                                                <div class="col-md-3" style="width:26.5%;">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="ga_dept_1" name="ga_dept_1" value="GA Dept of Revenue" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Form No. :</label>
                                                                    <input type="text" class="form-control-insu" id="form_number_3" readonly name="form_number_3" value="Form-G-7">
                                                                </div>

                                                                <div class="col-md-2 business2" style="width:13%;">
                                                                    <label class="control-label-insu">Quarter Period:</label>
                                                                    <select type="text" class="form-control-insu" id="quarter_type_holding1" name="quarter_type_holding1">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" @if($common->quarter_type_holding1=='1st Qtr') selected @endif>1st Qtr.</option>
                                                                        <option value="2nd Qtr" @if($common->quarter_type_holding1=='2nd Qtr') selected @endif>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" @if($common->quarter_type_holding1=='3rd Qtr') selected @endif>3rd Qtr.</option>
                                                                        <option value="4th Qtr" @if($common->quarter_type_holding1=='4th Qtr') selected @endif>4th Qtr.</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Period End Date</label>
                                                                    <input type="text" class="form-control-insu" id="quarter_date_holding" name="quarter_date_holding" value="{{$common->quarter_date_holding}}" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Filing Frequency</label>
                                                                    <input type="text" class="form-control-insu" id="frequency_type_holding1" name="frequency_type_holding1" value="Quarterly" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Due Date:</label>
                                                                    <input type="text" class="form-control-insu form-control" id="federal_frequency_due_date_holding" name="federal_frequency_due_date_holding" value="{{$common->federal_frequency_due_date_holding}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">State Withholding No:</label>
                                                                    <input type="text" class="form-control-insu" id="state_holding_no" name="state_holding_no" value="{{$common->state_holding_no}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">PIN :</label>
                                                                    <input type="text" class="form-control-insu" id="pin" name="pin" value="{{$common->pin}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Username :</label>
                                                                    <input type="text" class="form-control-insu" id="holding_uname" name="holding_uname" value="{{$common->holding_uname}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Password :</label>
                                                                    <input type="password" class="form-control-insu" id="holding_password" name="holding_password" value="{{$common->holding_password}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Payment Frequency </label>
                                                                    <select type="text" class="form-control-insu" id="payment_frequency_type_holding2" name="payment_frequency_type_holding2">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Monthly" @if($common->payment_frequency_type_holding2 =='Monthly') selected @endif>Monthly</option>
                                                                        <option value="Quarterly" @if($common->payment_frequency_type_holding2 =='Quarterly') selected @endif>Quarterly</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Payment Due Date </label>
                                                                    <input type="text" class="form-control-insu form-control" id="payment_frequency_due_date_holding" name="payment_frequency_due_date_holding" value="{{$common->payment_frequency_due_date_holding}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <h3>State Unemployment (SUTA)</h3>
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu">Name :</label>
                                                                    <input type="text" class="form-control-insu" name="fedral_state_2" value="GA" readonly>
                                                                </div>

                                                                <div class="col-md-3" style="width:25.5%;">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="ga_dept_labour" name="ga_dept_labour" value="GA Dept of Labor" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:14%;">
                                                                    <label class="control-label-insu">Form No. :</label>
                                                                    <input type="text" class="form-control-insu" id="form_number_4" readonly name="form_number_4" value="Form-DOL-4N">
                                                                </div>

                                                                <div class="col-md-2 business2" style="width:13%;">
                                                                    <label class="control-label-insu">Quarter Period:</label>
                                                                    <select type="text" class="form-control-insu" id="quarter_type_unemploy1" name="quarter_type_unemploy1">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" @if($common->quarter_type_unemploy1=='1st Qtr') selected @endif>1st Qtr.</option>
                                                                        <option value="2nd Qtr" @if($common->quarter_type_unemploy1=='2nd Qtr') selected @endif>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" @if($common->quarter_type_unemploy1=='3rd Qtr') selected @endif>3rd Qtr.</option>
                                                                        <option value="4th Qtr" @if($common->quarter_type_unemploy1=='4th Qtr') selected @endif>4th Qtr.</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Period End Date</label>
                                                                    <input type="text" class="form-control-insu" id="quarter_date_unemploy" name="quarter_date_unemploy" value="{{$common->quarter_date_unemploy}}" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Filing Frequency</label>
                                                                    <input type="text" class="form-control-insu" id="frequency_type_unemploy1" name="frequency_type_unemploy1" value="Quarterly" readonly>
                                                                </div>

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="federal_frequency_due_date_unemploy" name="federal_frequency_due_date_unemploy" value="{{$common->federal_frequency_due_date_unemploy}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="fe_state_tab">


                                                                <div class="col-md-4" style="width:35.5%;">
                                                                    <label class="control-label-insu">State Unemployment No. :</label>
                                                                    <input type="text" class="form-control-insu" id="state_unemploy_no" name="state_unemploy_no" value="{{$common->state_unemploy_no}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Username. :</label>
                                                                    <input type="text" class="form-control-insu" id="unemploy_uname" name="unemploy_uname" value="{{$common->unemploy_uname}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Password. :</label>
                                                                    <input type="password" class="form-control-insu" id="unemploy_password" name="unemploy_password" value="{{$common->unemploy_password}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Payment Frequency </label>
                                                                    <input type="text" class="form-control-insu" id="payment_frequency_type_unemploy2" name="payment_frequency_type_unemploy2" value="Quarterly">
                                                                    <!--   <select type="text" class="form-control-insu" id="payment_frequency_type_unemploy2" name="payment_frequency_type_unemploy2">-->
                                                                    <!--    <option value="">-Select-</option>-->
                                                                <!--<option value="Monthly" @if($common->payment_frequency_type_unemploy2 =='Monthly') selected @endif>Monthly</option>-->
                                                                <!--	<option value="Quarterly" @if($common->payment_frequency_type_unemploy2 =='Quarterly') selected @endif>Quarterly</option>-->
                                                                    <!--</select>-->
                                                                </div>

                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Payment Due Date </label>
                                                                    <input type="text" class="form-control-insu form-control" id="payment_frequency_due_date_unemploy" name="payment_frequency_due_date_unemploy" value="{{$common->payment_frequency_due_date_unemploy}}" readonly>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        }
                                        ?>

                                        <?php
                                        if($common->client_intangible == 1)
                                        {
                                        ?>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="background:#95f7c1;">
                                                <div class="col-md-3" style="text-align:left;"><h1>State</h1></div>
                                                <div class="col-md-6"><h1>Sales Tax</h1></div>
                                            </div>
                                            <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">
                                                <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <!--<h3>Federal Withholding & FICA Tax</h3>-->
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu"> State :</label>
                                                                    <input type="text" class="form-control-insu" id="saltax_state_name" name="saltax_state_name" value="GA" readonly>
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                </div>

                                                                <div class="col-md-3" style="width:26.5%;">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="saltax_ga_dept" name="saltax_ga_dept" value="GA Dept of Revenue" readonly>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">County :</label>
                                                                    <input type="text" class="form-control" id="saltax_county" name="saltax_county" value="{{$common->location_county}}"/>
                                                                </div>

                                                                <!--<div class="col-md-2" style="width:13%;">-->
                                                                <!--	<label class="control-label-insu">County No.</label>-->
                                                            <!--	<input type="text" class="form-control-insu" id="saltax_county_no" name="saltax_county_no" value="{{$common->physical_county_no}}"> -->
                                                                <!--</div>-->

                                                                <!--<div class="col-md-3">-->
                                                                <!--	<label class="control-label-insu">County :</label>-->
                                                                <!--	<select type="text" class="form-control-insu" id="saltax_county" name="saltax_county">-->
                                                                <!--	        <option value="">Select</option>-->
                                                            <!--	    @foreach($taxstate as $county)-->
                                                            <!--	        <option value="{{$county->county}}">{{$county->county}}</option>-->
                                                                <!--                                               @endforeach-->
                                                                <!--	</select>-->
                                                                <!--</div>-->

                                                                <div class="col-md-2" style="width:13%;">
                                                                    <label class="control-label-insu">County No.</label>
                                                                    <input type="text" class="form-control-insu" id="saltax_county_no" name="saltax_county_no" value="{{$common->physical_county_no}}">
                                                                </div>

                                                                <div class="col-md-2 business2" style="width:13%;">
                                                                    <label class="control-label-insu">Period:</label>
                                                                    <select type="text" class="form-control-insu" id="saltax_period" name="saltax_period">
                                                                        <!--<option value="">Select</option>-->
                                                                        <option value="Monthly" @if($common->saltax_period =='Monthly') selected @endif>Monthly</option>
                                                                        <option value="Quarterly" @if($common->saltax_period =='Quarterly') selected @endif>Quarterly</option>
                                                                        <option value="Annually" @if($common->saltax_period =='Annually') selected @endif>Annually</option>
                                                                    </select>
                                                                </div>

                                                                <!--                                    <div class="col-md-2" style="width:13%;">-->
                                                                <!-- <label class="control-label-insu">Filing Frequency</label>-->
                                                                <!-- <input type="text" class="form-control-insu" id="federal_frequency1111" name="federal_frequency11"  readonly>-->
                                                            <!--		@if ($errors->has('federal_frequency'))-->
                                                                <!--		<span class="help-block">-->
                                                            <!--		<strong>{{ $errors->first('federal_frequency') }}</strong>-->
                                                                <!--		</span>-->
                                                                <!--		@endif-->
                                                                <!--</div>-->

                                                                <div class="col-md-2" style="width:14%;">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="saltax_due_date" name="saltax_due_date" value="{{$common->saltax_due_date}}" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">State Sales Tax No.</label>
                                                                    <input type="text" class="form-control-insu" id="saltax_tax_no" name="saltax_tax_no" value="{{$common->saltax_tax_no}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">UN :</label>
                                                                    <input type="text" class="form-control-insu" id="saltax_uname" name="saltax_uname" value="{{$common->saltax_uname}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">PW</label>
                                                                    <input type="text" class="form-control-insu" name="saltax_password" id="saltax_password" value="{{$common->saltax_password}}">
                                                                </div>

                                                                <?php
                                                                if(isset($commontax->state_tax_personal_rate) && $commontax->state_tax_personal_rate != '' && $commontax->state_tax_personal_rate != null)
                                                                {

                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">State Sales Tax Rate <span class="plusbox">+</span></label>
                                                                    <input type="text" class="form-control-insu form-control" name="saltax_state_rt" id="saltax_state_rt" value="{{$commontax->state_tax_personal_rate}}" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">State Sales Tax Rate <span class="plusbox">+</span></label>
                                                                    <input type="text" class="form-control-insu form-control" name="saltax_state_rt" id="saltax_state_rt" value="" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>


                                                                <?php
                                                                if(isset($countytax->county_tax_personal_rate) && $countytax->county_tax_personal_rate != '' && $countytax->county_tax_personal_rate != null)
                                                                {

                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">County Sales Tax Rate <span class="plusbox"> = </span></label>
                                                                    <input type="text" class="form-control-insu form-control" name="saltax_county_rt" id="saltax_county_rt" value="{{$countytax->county_tax_personal_rate}}" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">County Sales Tax Rate <span class="plusbox"> = </span></label>
                                                                    <input type="text" class="form-control-insu form-control" name="saltax_county_rt" id="saltax_county_rt" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>





                                                                <?php
                                                                if (isset($commontax->state_tax_personal_rate) && $commontax->state_tax_personal_rate != '' && isset($countytax->county_tax_personal_rate) && $countytax->county_tax_personal_rate != '') {
                                                                    $abc = $commontax->state_tax_personal_rate;
                                                                    $jj = substr($abc, 0, 1);
                                                                    $def = $countytax->county_tax_personal_rate;
                                                                    $mm = substr($def, 0, 1);
                                                                    $tax1 = $mm + $jj;
                                                                    $tax1st = '.00%';
                                                                    $taxtotal = $tax1 . $tax1st;
                                                                } else if (isset($commontax->state_tax_personal_rate) && $commontax->state_tax_personal_rate != '') {
                                                                    $abc = $commontax->state_tax_personal_rate;
                                                                    $jj = substr($abc, 0, 1);
                                                                    $tax1 = $jj;
                                                                    $tax1st = '.00%';
                                                                    $taxtotal = $tax1 . $tax1st;
                                                                } else if (isset($countytax->county_tax_personal_rate) && $countytax->county_tax_personal_rate != '') {
                                                                    $def = $countytax->county_tax_personal_rate;
                                                                    $mm = substr($def, 0, 1);
                                                                    $tax1 = $mm;
                                                                    $tax1st = '.00%';
                                                                    $taxtotal = $tax1 . $tax1st;
                                                                }


                                                                ?>


                                                                <?php
                                                                if(isset($taxtotal) && $taxtotal != '' )
                                                                {

                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Sales Tax Rate </label>
                                                                    <input type="text" class="form-control-insu form-control txtinput_1" name="saltax_rt" id="saltax_rt" value="<?php if ($taxtotal != '') {
                                                                        echo $taxtotal;
                                                                    } else {
                                                                    }?>" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Sales Tax Rate </label>
                                                                    <input type="text" class="form-control-insu form-control txtinput_1" name="saltax_rt" id="saltax_rt" readonly style="text-align: right;">
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <?php
                                        }
                                        ?>

                                        <?php
                                        if($common->client_tobacco == 1)
                                        {
                                        ?>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch" style="background:#b0d3ff;">
                                                <div class="col-md-3" style="text-align:left;"><h1>State</h1></div>
                                                <div class="col-md-6"><h1>Tobacco Excise Tax</h1></div>
                                            </div>
                                            <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">
                                                <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <!--<h3>Federal Withholding & FICA Tax</h3>-->
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu"> State :</label>
                                                                    <input type="text" class="form-control-insu" id="excise_state_name" name="excise_state_name" value="GA" readonly>
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                </div>

                                                                <div class="col-md-5">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="excise_ga_dept" name="excise_ga_dept" value="GA Dept of Revenue" readonly>
                                                                </div>


                                                                <div class="col-md-3 business2">
                                                                    <label class="control-label-insu">Period:</label>
                                                                    <input type="text" class="form-control" id="excise_period" name="excise_period" value="Monthly" readonly/>
                                                                </div>


                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="excise_due_date" name="excise_due_date" value="Apr-10-2020" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">
                                                                <div class="col-md-6">
                                                                    <label class="control-label-insu">Tobacco Excise Tax No. :</label>
                                                                    <input type="text" class="form-control-insu" id="excise_tax_no" name="excise_tax_no" value="{{$common->excise_tax_no}}">
                                                                </div>


                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">UN </label>
                                                                    <input type="text" class="form-control-insu" name="excise_uname" id="excise_uname" value="{{$common->excise_uname}}">
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">PW</label>
                                                                    <input type="text" class="form-control-insu form-control" name="excise_password" id="excise_password" value="{{$common->excise_password}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <?php
                                        }
                                        ?>


                                        <?php
                                        if($common->client_coam == 1)
                                        {
                                        ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch">
                                                <div class="col-md-3" style="text-align:left;"><h1>State</h1></div>
                                                <div class="col-md-6"><h1>COAM</h1></div>
                                            </div>
                                            <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">
                                                <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <!--<h3>Federal Withholding & FICA Tax</h3>-->
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu"> State :</label>
                                                                    <input type="text" class="form-control-insu" id="coam_state_name" name="coam_state_name" value="GA" readonly>
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                </div>

                                                                <div class="col-md-5">
                                                                    <label class="control-label-insu"> Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="coam_ga_dept" name="coam_ga_dept" value="GA Dept of Lottery" readonly>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">Period:</label>
                                                                    <input type="text" class="form-control" id="coam_period" name="coam_period" value="Monthly" readonly/>
                                                                </div>

                                                                <div class="col-md-3 business2">
                                                                    <label class="control-label-insu">Due Date</label>
                                                                    <input type="text" class="form-control-insu form-control" id="coam_due_date" name="coam_due_date" value="Apr-20-2020" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">
                                                                <div class="col-md-6">
                                                                    <label class="control-label-insu">Coam No :</label>
                                                                    <input type="text" class="form-control-insu" id="coam_no" name="coam_no" value="{{$common->coam_no}}">
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">UN :</label>
                                                                    <input type="text" class="form-control-insu" id="coam_unmae" name="coam_unmae" value="{{$common->coam_unmae}}">
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label-insu">PW : </label>
                                                                    <input type="text" class="form-control-insu" name="coam_password" id="coam_password" value="{{$common->coam_password}}">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <?php
                                        }
                                        ?>


                                        <?php
                                        if($common->client_pay == 1)
                                        {
                                        ?>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch">
                                                <div class="col-md-3" style="text-align:left;"><h1>County</h1></div>
                                                <div class="col-md-6"><h1>Personal Property Tax</h1></div>
                                            </div>
                                            <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">
                                                <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="fe_state_tab_main">
                                                            <div class="fe_state_tab">
                                                                <div class="col-md-12">
                                                                    <!--<h3>Federal Withholding & FICA Tax</h3>-->
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <label class="control-label-insu"> State :</label>
                                                                    <input type="text" class="form-control-insu" id="personal_state_name" name="property_state_name" value="GA" readonly>
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                    <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                </div>

                                                                <div class="col-md-2" style="width:20%;">
                                                                    <label class="control-label-insu">County :</label>
                                                                    <input type="text" class="form-control-insu" id="personal_county" name="personal_county" value="{{$common->location_county}}">
                                                                </div>

                                                                <div class="col-md-2" style="width:12%;">
                                                                    <label class="control-label-insu">County No:</label>
                                                                    <input type="text" class="form-control-insu" id="personal_county_no" name="personal_county_no" value="{{$common->physical_county_no}}">
                                                                </div>

                                                                <div class="col-md-3" style="width:32%;">
                                                                    <label class="control-label-insu">Department Authority :</label>
                                                                    <input type="text" class="form-control-insu" id="personal_ga_dep" name="personal_ga_dep" value="{{$common->personal_ga_dep}}">
                                                                </div>

                                                                <div class="col-md-2 business2" style="width:13%;">
                                                                    <label class="control-label-insu"> Period:</label>
                                                                    <input type="text" class="form-control-insu" id="personal_period" name="personal_period" value="Annually" readonly>
                                                                </div>


                                                                <div class="col-md-2" style="width:14.5%;">
                                                                    <label class="control-label-insu">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control" id="personal_due_date" name="personal_due_date" value="Apr-1-2020" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="fe_state_tab">
                                                                <div class="col-md-4">
                                                                    <label class="control-label-insu">PPT A/C No. :</label>
                                                                    <input type="text" class="form-control-insu" id="personal_ac_no" name="personal_ac_no" value="{{$common->personal_ac_no}}">
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Online Payment :</label>
                                                                    <select class="form-control" id="online_payment" name="online_payment">
                                                                        <option value="No" @if($common->online_payment=='No') selected @endif>No</option>
                                                                        <option value="Yes" @if($common->online_payment=='Yes') selected @endif>Yes</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <label class="control-label-insu">Online Filing :</label>
                                                                    <select class="form-control" id="personal_filing" name="personal_filing">
                                                                        <option value="No" @if($common->personal_filing=='No') selected @endif>No</option>
                                                                        <option value="Yes" @if($common->personal_filing=='Yes') selected @endif>Yes</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-2 uname" @if($common->personal_filing =='No') style="display:none;" @endif>
                                                                    <label class="control-label-insu">UN </label>
                                                                    <input type="text" class="form-control-insu" name="personal_uname" id="personal_uname" value="{{$common->personal_uname}}">
                                                                </div>

                                                                <div class="col-md-2 password" @if($common->personal_filing =='No') style="display:none;" @endif>
                                                                    <label class="control-label-insu">PW </label>
                                                                    <input type="text" class="form-control-insu form-control" name="personal_password" id="personal_password" value="{{$common->personal_password}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <?php
                                        }
                                        ?>

                                    </div>

                                    <div class="tab-pane fade" id="tab12primary">
                                        <div class="Branch"><h1>Revenue Information</h1></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 control-label">Type of Business</label>
                                                <div class="col-md-6 col-sm-6"><input type="text" class="form-control"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab13primary">
                                        <div class="Branch"><h1>Expense Information</h1></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 control-label ">Type of Business</label>
                                                <div class="col-md-6 col-sm-6"><input type="text" class="form-control"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab14primary">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div style=" width:100%; display:table; ">

                                                <div id="add-new-payroll">

                                                    <div class="form-group ">
                                                        <label class="control-label col-md-3"></label>
                                                        <div>
                                                            <div class="Branch">
                                                                <h1>Payroll Services</h1>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="col-md-6">
                                                            <div class="payroll-btn">
                                                                Payroll - 1
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label class="control-label col-md-3">Payroll Period : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="business_state" id="business_state" class="form-control fsc-input">
                                                                            <option value="Weekly">Weekly</option>
                                                                            <option value="Bi-weekly">Bi-weekly</option>
                                                                            <option value="Semi monthly">Semi monthly</option>
                                                                            <option value="Monthly">Monthly</option>
                                                                            <option value="Other">Other</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Period Start Date : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="date" class="form-control fsc-input" name="" id="" value="" placeholder="">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control fsc-input" name="" id="" value="" placeholder="Monday">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Period End Date : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="date" class="form-control fsc-input" name="" id="" value="" placeholder="">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control fsc-input" name="" id="" value="" placeholder="Monday">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Pay Date : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="date" class="form-control fsc-input" name="" id="" value="" placeholder="">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control fsc-input" name="" id="" value="" placeholder="Monday">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> </label>
                                                    <div class="col-md-6 text-right">
                                                        <a href="javascript:void(0)" onclick="afterText()" class="btn btn-success addmore"><span style="margin-right: 0px;" class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add </a>
                                                    </div>
                                                </div>
                                                <script>
                                                    function afterText() {
                                                        var txt1 = "<b>I dlfkdfdljfdkjfdj djfdjfjdfd</b>";  // Create element with HTML
                                                        $("#add-new-payroll").after(txt1,);    // Insert new elements after img
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab15primary">
                                        <?php
                                        if (isset($bankdata) != '') {
                                            $bankcount = count($bankdata);
                                        }

                                        ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Branch">
                                                <h1 class="titleleft">Banking Information</h1>
                                                <div class="togglebox">
                                                    <label class="control-label">Do You have Business Bank Account</label>
                                                    <div class="btn-group btn-toggle">

                                                        @if($bankcount>0)
                                                            <a class="btn btnyes active" onclick="showbanknamevid();" id="btnyes">Yes</a>
                                                            <a class="btn btnno btn-default" onclick="hidebanknamevid();" id="btnno">No</a>
                                                        @else

                                                            <a class="btn btnyes btn-default" onclick="showbanknamevid();" id="btnyes">Yes</a>
                                                            <a class="btn btnno active" onclick="hidebanknamevid();" id="btnno">No</a>
                                                        @endif
                                                    </div>
                                                    <a href="#" id="uparrow" onClick="hidebanknamevid02();" style="display:none;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                                                    <a href="#" id="dwnarrow" onClick="showbanknamevid02();" style="display:none;"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                </div>
                                            </div>

                                            @if($bankcount>0)
                                                <div class="row">
                                                    @else
                                                        <div class="row" id="banknamediv" @if($common->banktype=='1') @else style="display:none" @endif>
                                                            @endif

                                                            <div class="col-md-12">
                                                                <div class="form-group row">
                                                                    <label class="control-label col-md-4">How many bank accounts do you have</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control" value="<?php if ($bankcount > 0) {
                                                                            echo $bankcount;
                                                                        } else {
                                                                            echo '1';
                                                                        } ?>" name="bank_accounts" style="width:100px" readonly/>
                                                                    </div>
                                                                    <!--       <div class="col-md-2">-->
                                                                    <!--            <div class="col-md-2"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; -->
                                                                    <!--<a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>-->
                                                                    <!--       </div>-->
                                                                    <div class="col-md-2">
                                                                        <a class="btn btn-primary addCFnew">Add</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @if($bankcount>0)

                                                                <?php
                                                                $i = 1;
                                                                ?>

                                                                @foreach($bankdata as $bankdetail)

                                                                    <div class="col-md-12">
                                                                        <div id="customFieldsbo">
                                                                            <div class="customfieldsbox">
                                                                                <div class="row form-group">
                                                                                    <div class="col-md-3">
                                                                                        <input type="hidden" name="bank_id[]" value="{{$bankdetail->id}}">
                                                                                        <label class="control-label">Bank Name </label>
                                                                                    <!--<input type="text" class="form-control" name="bank_name[]" value="{{$bankdetail->bank_name}}"/>-->
                                                                                        <select class="form-control" name="bank_name[]">
                                                                                            <option value="">Select</option>
                                                                                            @foreach($bankmaster as $bkmaster)
                                                                                                <option value="{{$bkmaster->bankname}}" @if($bkmaster->bankname==$bankdetail->bank_name) selected @endif>{{$bkmaster->bankname}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Account Nick Name</label>
                                                                                        <input type="text" class="form-control" name="nick_name[]" value="{{$bankdetail->nick_name}}"/>
                                                                                    </div>
                                                                                    <div class="col-md-3" style="width:20%">
                                                                                        <label class="control-label">Last Four digit of A/C #</label>
                                                                                        <input type="text" class="form-control fourdigit" name="fourdigit[]" value="{{$bankdetail->fourdigit}}"/>
                                                                                    </div>
                                                                                    <div class="col-md-2" style="width:20%">
                                                                                        <label class="control-label">Statement</label>
                                                                                        <select class="form-control" name="statement[]">
                                                                                            <option value="">Select</option>
                                                                                            <option value="E-Statement" @if($bankdetail->statement=='E-Statement') selected @endif>E-Statement</option>
                                                                                            <option value="Paper-Statement" @if($bankdetail->statement=='Paper-Statement') selected @endif>Paper-Statement</option>
                                                                                            <option value="FSC get Online" @if($bankdetail->statement=='FSC get Online') selected @endif>FSC get Online</option>

                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-1">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="row form-group">
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Check Stubs</label>
                                                                                        <select class="form-control" name="stubs[]">
                                                                                            <option value="">Select</option>
                                                                                            <option value="St. With Check Image" @if($bankdetail->stubs=='St. With Check Image') selected @endif>St. With Check Image</option>
                                                                                            <option value="Counter Check" @if($bankdetail->stubs=='Counter Check') selected @endif>Counter Check</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label">Open Date</label>
                                                                                        <input type="text" class="form-control opendate" name="opendate[]" value="{{$bankdetail->opendate}}" readonly/>
                                                                                    </div>
                                                                                    <div class="col-md-3" style="width:20%">
                                                                                        <label class="control-label">Status</label>
                                                                                        <select class="form-control statuss" name="bankstatus[]" id="bankstatus{{$bankdetail->id}}">
                                                                                            <option value="">Select</option>
                                                                                            <option value="Active" @if($bankdetail->bankstatus=='Active') selected @endif>Active</option>
                                                                                            <option value="Close" @if($bankdetail->bankstatus=='Close') selected @endif>Close</option>

                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-3 closes" id="closedate{{$bankdetail->id}}" style="width:20%;">
                                                                                        <label class="control-label">Close Date</label>
                                                                                        <input type="text" class="form-control closedate" name="bankclosedate[]" value="{{$bankdetail->bankclosedate}}" readonly/>
                                                                                    </div>

                                                                                    <div class="col-md-1">
                                                                                        @if($bankcount>0)
                                                                                            <a href="#bankModal_{{$bankdetail->id}}" id="remove_button_pro" role="button" class="btn btn-danger mt30" data-toggle="modal">Remove</a>
                                                                                        @else

                                                                                        @endif

                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            var pro = $('#bankstatus{{$bankdetail->id}}').val();

                                                                            if (pro == 'Close') {
                                                                                $("#closedate{{$bankdetail->id}}").show();
                                                                            } else {
                                                                                $("#closedate{{$bankdetail->id}}").hide();
                                                                            }
                                                                        });
                                                                    </script>

                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#bankstatus{{$bankdetail->id}}").on("click", function () {

                                                                                var pro = $('#bankstatus{{$bankdetail->id}}').val();

                                                                                if (pro == 'Close') {

                                                                                    $("#closedate{{$bankdetail->id}}").show();
                                                                                } else {

                                                                                    $("#closedate{{$bankdetail->id}}").hide();
                                                                                }
                                                                            });

                                                                        });


                                                                    </script>


                                                                @endforeach

                                                                <?php
                                                                $i++;
                                                                ?>
                                                            @else

                                                                <div class="col-md-12">
                                                                    <div id="customFieldsbo">
                                                                        <div class="customfieldsbox">
                                                                            <div class="row form-group">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Bank Name</label>
                                                                                    <select class="form-control" name="bank_name[]">
                                                                                        <option value="">Select</option>
                                                                                        @foreach($bankdatalist as $bkmaster)
                                                                                            <option value="{{$bkmaster->bankname}}">{{$bkmaster->bankname}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="col-md-1" style="padding-left:0px">

                                                                                    <label class="control-label" style="margin-top:44px;"></label>
                                                                                    <a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>
                                                                                    <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a>

                                                                                </div>

                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Account Nick Name</label>
                                                                                    <input type="text" class="form-control" name="nick_name[]"/>
                                                                                </div>
                                                                                <div class="col-md-3" style="width:20%">
                                                                                    <label class="control-label">Last Four digit of A/C #</label>
                                                                                    <input type="text" class="form-control fourdigit" name="fourdigit[]"/>
                                                                                </div>
                                                                                <div class="col-md-2" style="width:20%">
                                                                                    <label class="control-label">Statement</label>
                                                                                    <select class="form-control" name="statement[]">
                                                                                        <option value="">Select</option>
                                                                                        <option value="E-Statement">E-Statement</option>
                                                                                        <option value="Paper-Statement">Paper-Statement</option>
                                                                                        <option value="FSC get Online">FSC get Online</option>

                                                                                    </select>
                                                                                </div>
                                                                                <!-- <div class="col-md-1">-->

                                                                                <!--</div>-->
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Check Stubs</label>
                                                                                    <select class="form-control" name="stubs[]">
                                                                                        <option value="">Select</option>
                                                                                        <option value="St. With Check Image">St. With Check Image</option>
                                                                                        <option value="Counter Check">Counter Check</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Open Date</label>
                                                                                    <input type="text" class="form-control opendate" name="opendate[]" readonly/>
                                                                                </div>
                                                                                <div class="col-md-3" style="width:20%">
                                                                                    <label class="control-label">Status</label>
                                                                                    <select class="form-control statuss" name="bankstatus[]">
                                                                                        <option value="">Select</option>
                                                                                        <option value="Active">Active</option>
                                                                                        <option value="Close">Close</option>

                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-3 closes" style="width:20%;display:none;">
                                                                                    <label class="control-label">Close Date</label>
                                                                                    <input type="text" class="form-control closedate" name="bankclosedate[]" readonly/>
                                                                                </div>

                                                                                <!-- <div class="col-md-1">-->
                                                                                <!--   <a class="btn btn-primary mt30 addCFnew">Add</a>-->
                                                                                <!--</div>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            @endif

                                                        </div> <br/><br/>
                                                        <div class="Branch">

                                                            <?php
                                                            if (isset($carddata) != '') {
                                                                $cardcount = count($carddata);
                                                            }
                                                            ?>
                                                            <h1 class="titleleft">Credit Card Information</h1>
                                                            <div class="togglebox">
                                                                <label class="control-label">Do You have Business Credit Card</label>
                                                                <div class="btn-group btn-toggle">


                                                                    @if($cardcount>0)
                                                                        <a class="btn btnyes active" onClick="showcreditcardnamevid();">Yes</a>
                                                                        <a class="btn btnno btn-default" onClick="hidecreditcardnamevid();">No</a>
                                                                    @else
                                                                        <a class="btn btnyes btn-default" onclick="showcreditcardnamevid();" id="btnyes">Yes</a>
                                                                        <a class="btn btnno active" onclick="hidecreditcardnamevid();" id="btnno">No</a>
                                                                    @endif


                                                                    <a href="#" id="uparrow2" onClick="showcreditcardnamevid02();" style="display:none;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                                                                    <a href="#" id="dwnarrow2" onClick="hidecreditcardnamevid02();" style="display:none;"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        @if($cardcount>0)
                                                            <div class="row">
                                                                @else
                                                                    <div class="row" id="creditcardnamediv" @if($common->banktype1=='1') @else style="display:none" @endif>
                                                                        @endif

                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label col-md-4">How many Business Credit Card do you have </label>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" class="form-control" name="business_credit_card" value="<?php if ($cardcount > 0) {
                                                                                        echo $cardcount;
                                                                                    } else {
                                                                                        echo '1';
                                                                                    }?>" style="width:100px" readonly/>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <a class="btn btn-primary addCFnewccd">Add</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                        $i = 1;
                                                                        ?>
                                                                        @if($cardcount>0)
                                                                            @foreach($carddata as $cardetail)

                                                                                <div class="col-md-12">
                                                                                    <div id="customFieldsboccard">
                                                                                        <div class="customfieldsbox">
                                                                                            <div class="row form-group">
                                                                                                <div class="col-md-3">
                                                                                                    <input type="hidden" class="form-control" name="card_id[]" value="{{$cardetail->id}}">
                                                                                                    <label class="control-label">Credit Card Name </label>
                                                                                                    <input type="text" class="form-control" name="card_bank_name[]" value="{{$cardetail->card_bank_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-3">
                                                                                                    <label class="control-label">Account Nick Name</label>
                                                                                                    <input type="text" class="form-control" name="card_nick_name[]" value="{{$cardetail->card_nick_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-3" style="width:20%">
                                                                                                    <label class="control-label">Last Four digit of A/C #</label>
                                                                                                    <input type="text" class="form-control cardfourdigit<?php echo $i;?>" name="card_fourdigit[]" value="{{$cardetail->card_fourdigit}}">
                                                                                                </div>


                                                                                                <div class="col-md-2" style="width:20%">
                                                                                                    <label class="control-label">Statement</label>
                                                                                                    <select class="form-control" name="card_statement[]">
                                                                                                        <option value="">Select</option>
                                                                                                        <option value="E-Statement" @if($cardetail->card_statement=='E-Statement') selected @endif>E-Statement</option>
                                                                                                        <option value="Paper-Statement" @if($cardetail->card_statement=='Paper-Statement') selected @endif>Paper-Statement</option>
                                                                                                        <option value="FSC get Online" @if($cardetail->card_statement=='FSC get Online') selected @endif>FSC get Online</option>

                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-md-1">

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row form-group">
                                                                                                <div class="col-md-3">
                                                                                                    <label class="control-label">Note</label>
                                                                                                    <input type="text" class="form-control" name="card_notes[]" value="{{$cardetail->card_notes}}">
                                                                                                </div>
                                                                                                <div class="col-md-3">
                                                                                                    <label class="control-label">Open Date</label>
                                                                                                    <input type="text" class="form-control cardopendate<?php echo $i;?>" name="card_opendate[]" value="{{$cardetail->card_opendate}}" readonly>
                                                                                                </div>
                                                                                                <div class="col-md-3" style="width:20%">
                                                                                                    <label class="control-label">Status</label>
                                                                                                    <select class="form-control" name="card_bankstatus[]" id="card_bankstatus{{$cardetail->id}}">
                                                                                                        <option value="">Select</option>

                                                                                                        <option value="Active" @if($cardetail->card_bankstatus=='Active') selected @endif>Active</option>
                                                                                                        <option value="Close" @if($cardetail->card_bankstatus=='Close') selected @endif>Close</option>


                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-md-3" style="width:20%;display:none;" id="card_closedate{{$cardetail->id}}">
                                                                                                    <label class="control-label">Close Date</label>
                                                                                                    <input type="text" class="form-control cardclosedate<?php echo $i;?>" name="card_closedate[]" value="{{$cardetail->card_closedate}}" readonly>
                                                                                                </div>

                                                                                                <div class="col-md-1">
                                                                                                    @if($bankcount>0)
                                                                                                        <a href="#cardModal_{{$cardetail->id}}" id="remove_button_pro" role="button" class="btn btn-danger mt30" data-toggle="modal">Remove</a>
                                                                                                    @else

                                                                                                    @endif

                                                                                                </div>


                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>


                                                                                <script>
                                                                                    $(document).ready(function () {
                                                                                        $(".cardfourdigit<?php echo $i;?>").mask("9999");
                                                                                        $(".cardopendate<?php echo $i;?>").datepicker({
                                                                                            autoclose: true,
                                                                                            format: "mm/dd/yyyy",
                                                                                        });
                                                                                        $(".cardclosedate<?php echo $i;?>").datepicker({
                                                                                            autoclose: true,
                                                                                            format: "mm/dd/yyyy",
                                                                                        });

                                                                                        var pro = $('#card_bankstatus{{$cardetail->id}}').val();
                                                                                        //alert(pro);
                                                                                        if (pro == 'Close') {
                                                                                            $("#card_closedate{{$cardetail->id}}").show();
                                                                                        } else {
                                                                                            $("#card_closedate{{$cardetail->id}}").hide();
                                                                                        }


                                                                                    });
                                                                                </script>

                                                                                <script>
                                                                                    $(document).ready(function () {
                                                                                        $("#card_bankstatus{{$cardetail->id}}").on("click", function () {

                                                                                            var pro = $('#card_bankstatus{{$cardetail->id}}').val();
                                                                                            //alert(pro);
                                                                                            if (pro == 'Close') {
                                                                                                $("#card_closedate{{$cardetail->id}}").show();
                                                                                            } else {
                                                                                                $("#card_closedate{{$cardetail->id}}").hide();
                                                                                            }
                                                                                        });

                                                                                    });
                                                                                </script>

                                                                            @endforeach
                                                                            <?php
                                                                            $i++;
                                                                            ?>


                                                                        @else

                                                                            <div class="col-md-12">
                                                                                <div id="customFieldsboccard">
                                                                                    <div class="customfieldsbox">
                                                                                        <div class="row form-group">
                                                                                            <div class="col-md-3">
                                                                                                <label class="control-label">Credit Card Name</label>
                                                                                                <select class="form-control" name="card_bank_name[]">
                                                                                                    <option value="">Select</option>
                                                                                                    @foreach($carddatalist as $cdmaster)
                                                                                                        <option value="{{$cdmaster->cardname}}">{{$cdmaster->cardname}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>

                                                                                            <div class="col-md-1" style="padding-left:0px">

                                                                                                <label class="control-label" style="margin-top:44px;"></label>
                                                                                                <a href="#" data-toggle="modal" data-target="#cardExampleModal" class="redius"><i class="fa fa-plus"></i></a>
                                                                                                <a href="#" data-toggle="modal" data-target="#cardExampleModal3" class="redius"><i class="fa fa-minus"></i></a>

                                                                                            </div>

                                                                                            <div class="col-md-3">
                                                                                                <label class="control-label">Account Nick Name</label>
                                                                                                <input type="text" class="form-control" name="card_nick_name[]"/>
                                                                                            </div>
                                                                                            <div class="col-md-3" style="width:20%">
                                                                                                <label class="control-label">Last Four digit of A/C #</label>
                                                                                                <input type="text" class="form-control fourdigit" name="card_fourdigit[]"/>
                                                                                            </div>
                                                                                            <div class="col-md-2" style="width:20%">
                                                                                                <label class="control-label">Statement</label>
                                                                                                <select class="form-control" name="card_statement[]">
                                                                                                    <option value="">Select</option>
                                                                                                    <option value="E-Statement">E-Statement</option>
                                                                                                    <option value="Paper-Statement">Paper-Statement</option>
                                                                                                    <option value="FSC get Online">FSC get Online</option>

                                                                                                </select>
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="row form-group">
                                                                                            <div class="col-md-3">
                                                                                                <label class="control-label">Note</label>
                                                                                                <input type="text" class="form-control" name="card_notes[]"/>
                                                                                            </div>

                                                                                            <div class="col-md-3">
                                                                                                <label class="control-label">Open Date</label>
                                                                                                <input type="text" class="form-control opendate" name="card_opendate[]" readonly/>
                                                                                            </div>

                                                                                            <div class="col-md-3" style="width:20%">
                                                                                                <label class="control-label">Status</label>
                                                                                                <select class="form-control statuss2" name="card_bankstatus[]">
                                                                                                    <option value="">Select</option>
                                                                                                    <option value="Active">Active</option>
                                                                                                    <option value="Close">Close</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-3 closes2" style="width:20%;display:none;">
                                                                                                <label class="control-label">Close Date</label>
                                                                                                <input type="text" class="form-control closedate" name="card_closedate[]" readonly/>
                                                                                            </div>

                                                                                            <!-- <div class="col-md-1">-->
                                                                                            <!--   <a class="btn btn-primary mt30 addCFnewccd">Add</a>-->
                                                                                            <!--</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>

                                                                        @endif

                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="Branch">
                                                                        <h1 class="titleleft">Loan Information</h1>
                                                                        <div class="togglebox">
                                                                            <label class="control-label">Do You have Business Loan</label>
                                                                            <div class="btn-group btn-toggle">
                                                                                <a class="btn  @if($common->businessloan=='1') btn-success active @else btn-default @endif" onClick="showcreditcardnamevid1();">Yes</a>
                                                                                <a class="btn btnno  @if($common->businessloan=='1') btn-default @else btn-success active @endif" onClick="showcreditcardnamevid2();">No</a>
                                                                                <input type="hidden" value="{{$common->businessloan}}" id="businessloan" name="businessloan">
                                                                            </div>
                                                                            <a href="#" id="uparrow3" onClick="showcreditcardnamevid102();" style="display:none;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                                                                            <a href="#" id="dwnarrow3" onClick="hidecreditcardnamevid102();" style="display:none;"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row" id="creditcardnamediv1" @if($common->businessloan=='1') @else style="display:none" @endif>
                                                                        <label class="control-label col-md-4">Type of Loan / Note</label>
                                                                        <div class="col-md-4">
                                                                            <Select class="form-control" name="loannote">
                                                                                <option value="">Select</option>
                                                                                <option value="Business Loan" @if($common->loannote=='Business Loan') selected @endif>Business Loan</option>
                                                                                <option value="Property Loan" @if($common->loannote=='Property Loan') selected @endif>Property Loan</option>
                                                                                <option value="Personal Loan" @if($common->loannote=='Personal Loan') selected @endif>Personal Loan</option>
                                                                                <option value="Seller Note" @if($common->loannote=='Seller Note') selected @endif>Seller Note</option>
                                                                            </Select>
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <br/>
                                                            </div>
                                                </div>


                                                <div class="tab-pane fade" id="tab8primary">
                                                    <div class="col-md-12">
                                                        <div class="Branch">
                                                            <h1>Client Interview</h1>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                        <div class="panel-group" id="accordion" role="tablist">
                                                            <div class="panel panel-default">

                                                                <div class="panel-heading" role="tab" id="headingtwo">
                                                                    <h4 class="panel-title" style="margin-bottom:0px;">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwo" aria-expanded="false">
                                                                            <span class="ac_name_first">Type of Business</span>
                                                                            <span class="ac_name_middel">Business Information</span>
                                                                            <i class="more-less glyphicon glyphicon-plus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>

                                                                <div style="clear:both;"></div>
                                                                <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                                                    <div class="panel-body accordion-body" style="padding-top:15px;">

                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered table-hover" id="tab_logic">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="text-center" style="width:5%;">No</th>
                                                                                    <th class="text-center" style="width:60%;">Question</th>
                                                                                    <th class="text-center">Answer</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr class="ccd" data-patienttype="No" id="2">
                                                                                    <td class="text-center">1</td>
                                                                                    <td>
                                                                                        <input type="text" placeholder="" value="Type of Entity?" class="form-control-accordion" readonly="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="interview_type_of_entity" id="interview_type_of_entity" class="form-control-accordion">
                                                                                            <option value="">--Select--</option>
                                                                                            <option value="Tax Exempt">Tax Exempt</option>
                                                                                            <option value="Single Member LLC">Single Member LLC</option>
                                                                                            <option value="S Corporation" selected="">S Corporation</option>
                                                                                            <option value="LLC-2 or more member">LLC-2 or more member</option>
                                                                                            <option value="Individual">Individual</option>
                                                                                            <option value="Fiduciary">Fiduciary</option>
                                                                                            <option value="Estate">Estate</option>
                                                                                            <option value="C Corporation">C Corporation</option>
                                                                                            <option value="Any Entity">Any Entity</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd" data-patienttype="No" id="3">
                                                                                    <td class="text-center">2</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" placeholder="Name" value="What is your business start Date ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="date" name="interview_business_sdate" value="2019-02-20" class="form-control-accordion">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd" data-patienttype="No" id="4">
                                                                                    <td class="text-center">3</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" placeholder="Name" value="Exactly What you Do? Explain in Detail ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" name="interview_business_detail" value="Conv. store with gas station" class="form-control-accordion">
                                                                                    </td>
                                                                                </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                                                                            <span class="ac_name_first">Type of Formation</span>
                                                                            <span class="ac_name_middel">Formation Information</span>
                                                                            <i class="more-less glyphicon glyphicon-plus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                                                    <div class="panel-body accordion-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered table-hover" id="tab_logic">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="text-center" style="width:5%;">No</th>
                                                                                    <th class="text-center" style="width:60%;">Question</th>
                                                                                    <th class="text-center">Answer</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr class="ccd" data-patienttype="No" id="2">
                                                                                    <td class="text-center">1</td>
                                                                                    <td>
                                                                                        <input type="text" value="Are you Registered Yes or No ?" class="form-control-accordion" readonly="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="formation_register" id="formation_register" class="form-control-accordion">
                                                                                            <option value="0">No</option>
                                                                                            <option value="1" selected="">Yes</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>


                                                                                <tr class="ccd" data-patienttype="No" id="3">
                                                                                    <td class="text-center">2</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" value="State of Registered Entity ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="formation_register_entity" id="formation_register_entity" class="form-control-accordion">
                                                                                            <option value="">Select</option>

                                                                                            <option value="AK">AK</option>
                                                                                            <option value="AR">AR</option>

                                                                                            <option value="AS">AS</option>
                                                                                            <option value="AZ">AZ</option>
                                                                                            <option value="CA">CA</option>
                                                                                            <option value="CO">CO</option>
                                                                                            <option value="CT">CT</option>
                                                                                            <option value="DC">DC</option>

                                                                                            <option value="DE">DE</option>
                                                                                            <option value="FL">FL</option>

                                                                                            <option value="FM">FM</option>
                                                                                            <option value="GA" selected="">GA</option>
                                                                                            <option value="GU">GU</option>
                                                                                            <option value="HI">HI</option>
                                                                                            <option value="IA">IA</option>

                                                                                            <option value="ID">ID</option>
                                                                                            <option value="IL">IL</option>
                                                                                            <option value="IN">IN</option>
                                                                                            <option value="KS">KS</option>
                                                                                            <option value="KY">KY</option>
                                                                                            <option value="LA">LA</option>
                                                                                            <option value="MA">MA</option>

                                                                                            <option value="MD">MD</option>

                                                                                            <option value="ME">ME</option>
                                                                                            <option value="MH">MH</option>
                                                                                            <option value="MI">MI</option>
                                                                                            <option value="MN">MN</option>
                                                                                            <option value="MO">MO</option>

                                                                                            <option value="MS">MS</option>
                                                                                            <option value="MT">MT</option>

                                                                                            <option value="NC">NC</option>
                                                                                            <option value="ND">ND</option>
                                                                                            <option value="NE">NE</option>
                                                                                            <option value="NH">NH</option>

                                                                                            <option value="NJ">NJ</option>
                                                                                            <option value="NM">NM</option>


                                                                                            <option value="NV">NV</option>
                                                                                            <option value="NY">NY</option>
                                                                                            <option value="MP">MP</option>
                                                                                            <option value="OH">OH</option>
                                                                                            <option value="OK">OK</option>
                                                                                            <option value="OR">OR</option>
                                                                                            <option value="PA">PA</option>
                                                                                            <option value="PR">PR</option>

                                                                                            <option value="PW">PW</option>
                                                                                            <option value="RI">RI</option>
                                                                                            <option value="SC">SC</option>
                                                                                            <option value="SD">SD</option>
                                                                                            <option value="TN">TN</option>
                                                                                            <option value="TX">TX</option>

                                                                                            <option value="UT">UT</option>
                                                                                            <option value="VA">VA</option>

                                                                                            <option value="VI">VI</option>

                                                                                            <option value="VT">VT</option>

                                                                                            <option value="WA">WA</option>
                                                                                            <option value="WI">WI</option>

                                                                                            <option value="WV">WV</option>
                                                                                            <option value="WY">WY</option>


                                                                                        </select>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr class="ccd" data-patienttype="No" id="3">
                                                                                    <td class="text-center">3</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" value="Date of Incorporation / Organization ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="date" name="formation_date" value="2019-02-05" class="form-control-accordion">
                                                                                    </td>
                                                                                </tr>

                                                                                <tr class="ccd" data-patienttype="No" id="3">
                                                                                    <td class="text-center">4</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" value="Type of Entity ? (For Income Tax Purpose)" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="formation_type_of_entity" id="formation_type_of_entity" class="form-control-accordion">
                                                                                            <option value="">--Select--</option>
                                                                                            <option value="Tax Exempt">Tax Exempt</option>
                                                                                            <option value="Single Member LLC">Single Member LLC</option>
                                                                                            <option value="S Corporation">S Corporation</option>
                                                                                            <option value="LLC-2 or more member">LLC-2 or more member</option>
                                                                                            <option value="Individual">Individual</option>
                                                                                            <option value="Fiduciary">Fiduciary</option>
                                                                                            <option value="Estate">Estate</option>
                                                                                            <option value="C Corporation">C Corporation</option>
                                                                                            <option value="Any Entity">Any Entity</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr class="ccd hidefive" style="display:none;" data-patienttype="No" id="5">
                                                                                    <td class="text-center">5</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" value="S election effective date" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="date" name="formation_start_date" value="" class="form-control-accordion">
                                                                                    </td>
                                                                                </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingFour">
                                                                    <h4 class="panel-title">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false">
                                                                            <span class="ac_name_first">Type of License</span>
                                                                            <span class="ac_name_middel">License Information</span>
                                                                            <i class="more-less glyphicon glyphicon-plus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>

                                                                <div style="clear:both;"></div>
                                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                                                    <div class="panel-body accordion-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered table-hover" id="tab_logic">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="text-center" style="width:5%;">No</th>
                                                                                    <th class="text-center" style="width:60%;">Question</th>
                                                                                    <th class="text-center">Answer</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr class="ccd" data-patienttype="No" id="new">
                                                                                    <td class="text-center">1</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" placeholder="Name" value="Are you require business license?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select class="form-control yess" name="license_yesno">
                                                                                            <option value="">Select</option>
                                                                                            <option value="Yes" selected="">Yes</option>
                                                                                            <option value="No">No</option>

                                                                                        </select>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd hidetr" style="" data-patienttype="No" id="2">
                                                                                    <td class="text-center">2</td>
                                                                                    <td>
                                                                                        <input type="text" value="Type of corp Entity ?" class="form-control-accordion" readonly="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="license_entity" id="license_entity" class="form-control-accordion">
                                                                                            <option value="GA">GA</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd hidetr" style="" data-patienttype="No" id="3">
                                                                                    <td class="text-center">3</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" placeholder="Name" value="Type of LLC / Corp ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" name="license_corporation" value="" lass="form-control-accordion">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd hidetr" style="" data-patienttype="No" id="4">
                                                                                    <td class="text-center">4</td>
                                                                                    <td>
                                                                                        <input type="text" readonly="" value="What is your License Date ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="date" name="license_date" value="2020-06-23" class="form-control-accordion">
                                                                                    </td>
                                                                                </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingFive">
                                                                    <h4 class="panel-title" style="margin-bottom:0px;">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed" aria-expanded="false">
                                                                            <span class="ac_name_first">Type of Taxation</span>
                                                                            <span class="ac_name_middel">Taxation Information</span>
                                                                            <i class="more-less glyphicon glyphicon-plus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>

                                                                <div style="clear:both;"></div>
                                                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseFive" aria-expanded="false" style="height: 0px;">
                                                                    <div class="panel-body accordion-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered table-hover" id="tab_logic">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="text-center" style="width:5%;">No</th>
                                                                                    <th class="text-center" style="width:60%;">Question</th>
                                                                                    <th class="text-center">Answer</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr class="ccd" data-patienttype="No" id="2">
                                                                                    <td class="text-center">1</td>
                                                                                    <td>
                                                                                        <input type="text" name="type_of_entity[]" placeholder="Name" value="Do you have payroll--Or do you pay salaries/Wages ?" class="form-control-accordion" readonly="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="client_payroll" id="patient_type_2" class="form-control-accordion">
                                                                                            <option value="0">No</option>
                                                                                            <option value="1" selected="">Yes</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd" data-patienttype="No" id="3">
                                                                                    <td class="text-center">2</td>
                                                                                    <td>
                                                                                        <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you sell any intangible-- OR Do you collect sales tax ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="client_intangible" id="patient_type_2" class="form-control-accordion">
                                                                                            <option value="">---Select Answers---</option>
                                                                                            <option value="0" selected="">No</option>
                                                                                            <option value="1">Yes</option>

                                                                                        </select>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="ccd" data-patienttype="No" id="4">
                                                                                    <td class="text-center">3</td>
                                                                                    <td>
                                                                                        <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you buy any tobacco or cigar from mfg. company ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="client_tobacco" id="patient_type_2" class="form-control-accordion">
                                                                                            <option value="0" selected="">No</option>
                                                                                            <option value="1">Yes</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr class="ccd" data-patienttype="No" id="6">
                                                                                    <td class="text-center">4</td>
                                                                                    <td>
                                                                                        <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you have COAM in your business ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="client_coam" id="patient_type_2" class="form-control-accordion">
                                                                                            <option value="0" selected="">No</option>
                                                                                            <option value="1">Yes</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr class="ccd" data-patienttype="No" id="5">
                                                                                    <td class="text-center">5</td>
                                                                                    <td>
                                                                                        <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you have to pay Personal Property Tax ?" class="form-control-accordion">
                                                                                    </td>
                                                                                    <td>
                                                                                        <select name="client_pay" id="patient_type_2" class="form-control-accordion">
                                                                                            <option value="0" selected="">No</option>
                                                                                            <option value="1">Yes</option>
                                                                                        </select>
                                                                                    </td>
                                                                                </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="panel-group" id="accordion" role="tablist" style="display:none">
                                                        <div class="panel panel-default">

                                                            <div class="panel-heading" role="tab" id="headingtwo">
                                                                <h4 class="panel-title" style="margin-bottom:0px;">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwo">
                                                                        <span class="ac_name_first">Type of Business</span>
                                                                        <span class="ac_name_middel">Business Information</span>
                                                                        <i class="more-less glyphicon glyphicon-plus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div style="clear:both;"></div>
                                                            <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                <div class="panel-body accordion-body" style="padding-top:15px;">

                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered table-hover" id="tab_logic">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center" style="width:5%;">No</th>
                                                                                <th class="text-center" style="width:60%;">Question</th>
                                                                                <th class="text-center">Answer</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr class="ccd" data-patienttype="No" id="2">
                                                                                <td class="text-center">1</td>
                                                                                <td>
                                                                                    <input type="text" placeholder="" value="Type of Entity?" class="form-control-accordion" readonly>
                                                                                </td>
                                                                                <td>
                                                                                    <select name="interview_type_of_entity" id="interview_type_of_entity" class="form-control-accordion">
                                                                                        <option value="">--Select--</option>
                                                                                        @foreach($typeofentity as $entity)
                                                                                            @if($common->interview_type_of_entity!='')
                                                                                                <option VALUE="{{$entity->typeentity}}" @if($entity->typeentity==$common->interview_type_of_entity) selected @endif>{{$entity->typeentity}}</option>
                                                                                            @else
                                                                                                <option value="{{$entity->typeentity}}">{{$entity->typeentity}}</option>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd" data-patienttype="No" id="3">
                                                                                <td class="text-center">2</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" placeholder="Name" value="What is your business start Date ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="date" name="interview_business_sdate" value="{{$common->interview_business_sdate}}" class="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd" data-patienttype="No" id="4">
                                                                                <td class="text-center">3</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" placeholder="Name" value="Exactly What you Do? Explain in Detail ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" name="interview_business_detail" value="{{$common->interview_business_detail}}" class="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>


                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingThree">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                                                                        <span class="ac_name_first">Type of Formation</span>
                                                                        <span class="ac_name_middel">Formation Information</span>
                                                                        <i class="more-less glyphicon glyphicon-plus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div style="clear:both;"></div>
                                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="panel-body accordion-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered table-hover" id="tab_logic">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center" style="width:5%;">No</th>
                                                                                <th class="text-center" style="width:60%;">Question</th>
                                                                                <th class="text-center">Answer</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr class="ccd" data-patienttype="No" id="2">
                                                                                <td class="text-center">1</td>
                                                                                <td>
                                                                                    <input type="text" value="Are you Registered Yes or No ?" class="form-control-accordion" readonly>
                                                                                </td>
                                                                                <td>
                                                                                    <select name="formation_register" id="formation_register" class="form-control-accordion" readonly>
                                                                                        <option value="0" @if($common->formation_register=='0') selected @endif>No</option>
                                                                                        <option value="1" @if($common->formation_register=='1') selected @endif>Yes</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>


                                                                            <tr class="ccd" data-patienttype="No" id="3">
                                                                                <td class="text-center">2</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" value="State of Registered Entity ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="formation_register_entity" id="formation_register_entity" class="form-control-accordion" readonly>
                                                                                        <option value=''>Select</option>

                                                                                        <option value='AK' <?php if ($common->formation_register_entity == 'AK') {
                                                                                            echo 'Selected';
                                                                                        }?>>AK
                                                                                        </option>
                                                                                        <option value='AR' <?php if ($common->formation_register_entity == 'AR') {
                                                                                            echo 'Selected';
                                                                                        }?>>AR
                                                                                        </option>

                                                                                        <option value='AS' <?php if ($common->formation_register_entity == 'AS') {
                                                                                            echo 'Selected';
                                                                                        }?>>AS
                                                                                        </option>
                                                                                        <option value='AZ' <?php if ($common->formation_register_entity == 'AZ') {
                                                                                            echo 'Selected';
                                                                                        }?>>AZ
                                                                                        </option>
                                                                                        <option value='CA' <?php if ($common->formation_register_entity == 'CA') {
                                                                                            echo 'Selected';
                                                                                        }?>>CA
                                                                                        </option>
                                                                                        <option value='CO' <?php if ($common->formation_register_entity == 'CO') {
                                                                                            echo 'Selected';
                                                                                        }?>>CO
                                                                                        </option>
                                                                                        <option value='CT' <?php if ($common->formation_register_entity == 'CT') {
                                                                                            echo 'Selected';
                                                                                        }?>>CT
                                                                                        </option>
                                                                                        <option value='DC' <?php if ($common->formation_register_entity == 'DC') {
                                                                                            echo 'Selected';
                                                                                        }?>>DC
                                                                                        </option>

                                                                                        <option value='DE' <?php if ($common->formation_register_entity == 'DE') {
                                                                                            echo 'Selected';
                                                                                        }?>>DE
                                                                                        </option>
                                                                                        <option value='FL' <?php if ($common->formation_register_entity == 'FL') {
                                                                                            echo 'Selected';
                                                                                        }?>>FL
                                                                                        </option>

                                                                                        <option value='FM' <?php if ($common->formation_register_entity == 'FM') {
                                                                                            echo 'Selected';
                                                                                        }?>>FM
                                                                                        </option>
                                                                                        <option value='GA' <?php if ($common->formation_register_entity == 'GA') {
                                                                                            echo 'Selected';
                                                                                        }?>>GA
                                                                                        </option>
                                                                                        <option value='GU' <?php if ($common->formation_register_entity == 'GU') {
                                                                                            echo 'Selected';
                                                                                        }?>>GU
                                                                                        </option>
                                                                                        <option value='HI' <?php if ($common->formation_register_entity == 'HI') {
                                                                                            echo 'Selected';
                                                                                        }?>>HI
                                                                                        </option>
                                                                                        <option value='IA' <?php if ($common->formation_register_entity == 'IA') {
                                                                                            echo 'Selected';
                                                                                        }?>>IA
                                                                                        </option>

                                                                                        <option value='ID' <?php if ($common->formation_register_entity == 'ID') {
                                                                                            echo 'Selected';
                                                                                        }?>>ID
                                                                                        </option>
                                                                                        <option value='IL' <?php if ($common->formation_register_entity == 'IL') {
                                                                                            echo 'Selected';
                                                                                        }?>>IL
                                                                                        </option>
                                                                                        <option value='IN' <?php if ($common->formation_register_entity == 'IN') {
                                                                                            echo 'Selected';
                                                                                        }?>>IN
                                                                                        </option>
                                                                                        <option value='KS' <?php if ($common->formation_register_entity == 'KS') {
                                                                                            echo 'Selected';
                                                                                        }?>>KS
                                                                                        </option>
                                                                                        <option value='KY' <?php if ($common->formation_register_entity == 'KY') {
                                                                                            echo 'Selected';
                                                                                        }?>>KY
                                                                                        </option>
                                                                                        <option value='LA' <?php if ($common->formation_register_entity == 'LA') {
                                                                                            echo 'Selected';
                                                                                        }?>>LA
                                                                                        </option>
                                                                                        <option value='MA' <?php if ($common->formation_register_entity == 'MA') {
                                                                                            echo 'Selected';
                                                                                        }?>>MA
                                                                                        </option>

                                                                                        <option value='MD' <?php if ($common->formation_register_entity == 'MD') {
                                                                                            echo 'Selected';
                                                                                        }?>>MD
                                                                                        </option>

                                                                                        <option value='ME' <?php if ($common->formation_register_entity == 'ME') {
                                                                                            echo 'Selected';
                                                                                        }?>>ME
                                                                                        </option>
                                                                                        <option value='MH' <?php if ($common->formation_register_entity == 'MH') {
                                                                                            echo 'Selected';
                                                                                        }?>>MH
                                                                                        </option>
                                                                                        <option value='MI' <?php if ($common->formation_register_entity == 'MI') {
                                                                                            echo 'Selected';
                                                                                        }?>>MI
                                                                                        </option>
                                                                                        <option value='MN' <?php if ($common->formation_register_entity == 'MN') {
                                                                                            echo 'Selected';
                                                                                        }?>>MN
                                                                                        </option>
                                                                                        <option value='MO' <?php if ($common->formation_register_entity == 'MO') {
                                                                                            echo 'Selected';
                                                                                        }?>>MO
                                                                                        </option>

                                                                                        <option value='MS' <?php if ($common->formation_register_entity == 'MS') {
                                                                                            echo 'Selected';
                                                                                        }?>>MS
                                                                                        </option>
                                                                                        <option value='MT' <?php if ($common->formation_register_entity == 'MT') {
                                                                                            echo 'Selected';
                                                                                        }?>>MT
                                                                                        </option>

                                                                                        <option value='NC' <?php if ($common->formation_register_entity == 'NC') {
                                                                                            echo 'Selected';
                                                                                        }?>>NC
                                                                                        </option>
                                                                                        <option value='ND' <?php if ($common->formation_register_entity == 'ND') {
                                                                                            echo 'Selected';
                                                                                        }?>>ND
                                                                                        </option>
                                                                                        <option value='NE' <?php if ($common->formation_register_entity == 'NE') {
                                                                                            echo 'Selected';
                                                                                        }?>>NE
                                                                                        </option>
                                                                                        <option value='NH' <?php if ($common->formation_register_entity == 'NH') {
                                                                                            echo 'Selected';
                                                                                        }?>>NH
                                                                                        </option>

                                                                                        <option value='NJ' <?php if ($common->formation_register_entity == 'NJ') {
                                                                                            echo 'Selected';
                                                                                        }?>>NJ
                                                                                        </option>
                                                                                        <option value='NM' <?php if ($common->formation_register_entity == 'NM') {
                                                                                            echo 'Selected';
                                                                                        }?>>NM
                                                                                        </option>


                                                                                        <option value='NV' <?php if ($common->formation_register_entity == 'NV') {
                                                                                            echo 'Selected';
                                                                                        }?>>NV
                                                                                        </option>
                                                                                        <option value='NY' <?php if ($common->formation_register_entity == 'NY') {
                                                                                            echo 'Selected';
                                                                                        }?>>NY
                                                                                        </option>
                                                                                        <option value='MP' <?php if ($common->formation_register_entity == 'MP') {
                                                                                            echo 'Selected';
                                                                                        }?>>MP
                                                                                        </option>
                                                                                        <option value='OH' <?php if ($common->formation_register_entity == 'OH') {
                                                                                            echo 'Selected';
                                                                                        }?>>OH
                                                                                        </option>
                                                                                        <option value='OK' <?php if ($common->formation_register_entity == 'OK') {
                                                                                            echo 'Selected';
                                                                                        }?>>OK
                                                                                        </option>
                                                                                        <option value='OR' <?php if ($common->formation_register_entity == 'OR') {
                                                                                            echo 'Selected';
                                                                                        }?>>OR
                                                                                        </option>
                                                                                        <option value='PA' <?php if ($common->formation_register_entity == 'PA') {
                                                                                            echo 'Selected';
                                                                                        }?>>PA
                                                                                        </option>
                                                                                        <option value='PR' <?php if ($common->formation_register_entity == 'PR') {
                                                                                            echo 'Selected';
                                                                                        }?>>PR
                                                                                        </option>

                                                                                        <option value='PW' <?php if ($common->formation_register_entity == 'PW') {
                                                                                            echo 'Selected';
                                                                                        }?>>PW
                                                                                        </option>
                                                                                        <option value='RI' <?php if ($common->formation_register_entity == 'RI') {
                                                                                            echo 'Selected';
                                                                                        }?>>RI
                                                                                        </option>
                                                                                        <option value='SC' <?php if ($common->formation_register_entity == 'SC') {
                                                                                            echo 'Selected';
                                                                                        }?>>SC
                                                                                        </option>
                                                                                        <option value='SD' <?php if ($common->formation_register_entity == 'SD') {
                                                                                            echo 'Selected';
                                                                                        }?>>SD
                                                                                        </option>
                                                                                        <option value='TN' <?php if ($common->formation_register_entity == 'TN') {
                                                                                            echo 'Selected';
                                                                                        }?>>TN
                                                                                        </option>
                                                                                        <option value='TX' <?php if ($common->formation_register_entity == 'TX') {
                                                                                            echo 'Selected';
                                                                                        }?>>TX
                                                                                        </option>

                                                                                        <option value='UT' <?php if ($common->formation_register_entity == 'UT') {
                                                                                            echo 'Selected';
                                                                                        }?>>UT
                                                                                        </option>
                                                                                        <option value='VA' <?php if ($common->formation_register_entity == 'VA') {
                                                                                            echo 'Selected';
                                                                                        }?>>VA
                                                                                        </option>

                                                                                        <option value='VI' <?php if ($common->formation_register_entity == 'VI') {
                                                                                            echo 'Selected';
                                                                                        }?>>VI
                                                                                        </option>

                                                                                        <option value='VT' <?php if ($common->formation_register_entity == 'VT') {
                                                                                            echo 'Selected';
                                                                                        }?>>VT
                                                                                        </option>

                                                                                        <option value='WA' <?php if ($common->formation_register_entity == 'WA') {
                                                                                            echo 'Selected';
                                                                                        }?>>WA
                                                                                        </option>
                                                                                        <option value='WI' <?php if ($common->formation_register_entity == 'WI') {
                                                                                            echo 'Selected';
                                                                                        }?>>WI
                                                                                        </option>

                                                                                        <option value='WV' <?php if ($common->formation_register_entity == 'WV') {
                                                                                            echo 'Selected';
                                                                                        }?>>WV
                                                                                        </option>
                                                                                        <option value='WY' <?php if ($common->formation_register_entity == 'WY') {
                                                                                            echo 'Selected';
                                                                                        }?>>WY
                                                                                        </option>


                                                                                    </select>
                                                                                </td>
                                                                            </tr>

                                                                            <tr class="ccd" data-patienttype="No" id="3">
                                                                                <td class="text-center">3</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" value="Date of Incorporation / Organization ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="date" name="formation_date" value="{{$common->formation_date}}" class="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>

                                                                            <tr class="ccd" data-patienttype="No" id="3">
                                                                                <td class="text-center">4</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" value="Type of Entity ? (For Income Tax Purpose)" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="formation_type_of_entity" id="formation_type_of_entity" class="form-control-accordion" readonly>
                                                                                        <option value="">--Select--</option>
                                                                                        @foreach($typeofentity as $entity)
                                                                                            @if($common->formation_type_of_entity!='')
                                                                                                <option VALUE="{{$entity->typeentity}}" @if($entity->typeentity==$common->formation_type_of_entity) selected @endif>{{$entity->typeentity}}</option>
                                                                                            @else
                                                                                                <option value="{{$entity->typeentity}}">{{$entity->typeentity}}</option>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </select>
                                                                                </td>
                                                                            </tr>

                                                                            <tr class="ccd hidefive" style="display:none;" data-patienttype="No" id="5">
                                                                                <td class="text-center">5</td>
                                                                                <td>
                                                                                    <input type="text" readonly value="S election effective date" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="date" name="formation_start_date" value="{{$common->formation_start_date}}" class="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>


                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="panel-heading" role="tab" id="headingFour">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed">
                                                                        <span class="ac_name_first">Type of License</span>
                                                                        <span class="ac_name_middel">License Information</span>
                                                                        <i class="more-less glyphicon glyphicon-plus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div style="clear:both;"></div>
                                                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="panel-body accordion-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered table-hover" id="tab_logic">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center" style="width:5%;">No</th>
                                                                                <th class="text-center" style="width:60%;">Question</th>
                                                                                <th class="text-center">Answer</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr class="ccd" data-patienttype="No" id="new">
                                                                                <td class="text-center">1</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" placeholder="Name" value="Are you require business license?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select class="form-control yess" name="license_yesno" readonly>
                                                                                        <option value="">Select</option>
                                                                                        <option value="Yes" @if($common->license_yesno=='Yes') selected @endif>Yes</option>
                                                                                        <option value="No" @if($common->license_yesno=='No') selected @endif>No</option>

                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd hidetr" style="display:none;" data-patienttype="No" id="2">
                                                                                <td class="text-center">2</td>
                                                                                <td>
                                                                                    <input type="text" value="Type of corp Entity ?" class="form-control-accordion" readonly>
                                                                                </td>
                                                                                <td>
                                                                                    <select name="license_entity" id="license_entity" class="form-control-accordion" readonly>
                                                                                        @if($common->license_entity!='')
                                                                                            <option value="{{$common->license_entity}}">{{$common->license_entity}}</option>
                                                                                        @else


                                                                                            <option value="">--Select--</option>
                                                                                            <option value='AK'>AK</option>
                                                                                            <option value='AS'>AS</option>
                                                                                            <option value='AZ'>AZ</option>
                                                                                            <option value='AR'>AR</option>
                                                                                            <option value='CA'>CA</option>
                                                                                            <option value='CO'>CO</option>
                                                                                            <option value='CT'>CT</option>
                                                                                            <option value='DE'>DE</option>
                                                                                            <option value='DC'>DC</option>
                                                                                            <option value='FM'>FM</option>
                                                                                            <option value='FL'>FL</option>
                                                                                            <option value='GA'>GA</option>
                                                                                            <option value='GU'>GU</option>
                                                                                            <option value='HI'>HI</option>
                                                                                            <option value='ID'>ID</option>
                                                                                            <option value='IL'>IL</option>
                                                                                            <option value='IN'>IN</option>
                                                                                            <option value='IA'>IA</option>
                                                                                            <option value='KS'>KS</option>
                                                                                            <option value='KY'>KY</option>
                                                                                            <option value='LA'>LA</option>
                                                                                            <option value='ME'>ME</option>
                                                                                            <option value='MH'>MH</option>
                                                                                            <option value='MD'>MD</option>
                                                                                            <option value='MA'>MA</option>
                                                                                            <option value='MI'>MI</option>
                                                                                            <option value='MN'>MN</option>
                                                                                            <option value='MS'>MS</option>
                                                                                            <option value='MO'>MO</option>
                                                                                            <option value='MT'>MT</option>
                                                                                            <option value='NE'>NE</option>
                                                                                            <option value='NV'>NV</option>
                                                                                            <option value='NH'>NH</option>
                                                                                            <option value='NJ'>NJ</option>
                                                                                            <option value='NM'>NM</option>
                                                                                            <option value='NY'>NY</option>
                                                                                            <option value='NC'>NC</option>
                                                                                            <option value='ND'>ND</option>
                                                                                            <option value='MP'>MP</option>
                                                                                            <option value='OH'>OH</option>
                                                                                            <option value='OK'>OK</option>
                                                                                            <option value='OR'>OR</option>
                                                                                            <option value='PW'>PW</option>
                                                                                            <option value='PA'>PA</option>
                                                                                            <option value='PR'>PR</option>
                                                                                            <option value='RI'>RI</option>
                                                                                            <option value='SC'>SC</option>
                                                                                            <option value='SD'>SD</option>
                                                                                            <option value='TN'>TN</option>
                                                                                            <option value='TX'>TX</option>
                                                                                            <option value='UT'>UT</option>
                                                                                            <option value='VT'>VT</option>
                                                                                            <option value='VI'>VI</option>
                                                                                            <option value='VA'>VA</option>
                                                                                            <option value='WA'>WA</option>
                                                                                            <option value='WV'>WV</option>
                                                                                            <option value='WI'>WI</option>
                                                                                            <option value='WY'>WY</option>
                                                                                        @endif
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd hidetr" style="display:none;" data-patienttype="No" id="3">
                                                                                <td class="text-center">3</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" placeholder="Name" value="Type of LLC / Corp ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" name="license_corporation" value="{{$common->license_corporation}}" lass="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd hidetr" style="display:none;" data-patienttype="No" id="4">
                                                                                <td class="text-center">4</td>
                                                                                <td>
                                                                                    <input type="text" readonly="" value="What is your License Date ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <input type="date" name="license_date" value="{{$common->license_date}}" class="form-control-accordion" readonly>
                                                                                </td>
                                                                            </tr>


                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="panel-heading" role="tab" id="headingFive">
                                                                <h4 class="panel-title" style="margin-bottom:0px;">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed">
                                                                        <span class="ac_name_first">Type of Taxation</span>
                                                                        <span class="ac_name_middel">Taxation Information</span>
                                                                        <i class="more-less glyphicon glyphicon-plus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div style="clear:both;"></div>
                                                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseFive">
                                                                <div class="panel-body accordion-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered table-hover" id="tab_logic">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center" style="width:5%;">No</th>
                                                                                <th class="text-center" style="width:60%;">Question</th>
                                                                                <th class="text-center">Answer</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr class="ccd" data-patienttype="No" id="2">
                                                                                <td class="text-center">1</td>
                                                                                <td>
                                                                                    <input type="text" name="type_of_entity[]" placeholder="Name" value="Do you have payroll--Or do you pay salaries/Wages ?" class="form-control-accordion" readonly>
                                                                                </td>
                                                                                <td>
                                                                                    <select name="client_payroll" id="patient_type_2" class="form-control-accordion" readonly>
                                                                                        <option value="0" @if($common->client_payroll == 0) selected @endif>No</option>
                                                                                        <option value="1" @if($common->client_payroll == 1) selected @endif>Yes</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd" data-patienttype="No" id="3">
                                                                                <td class="text-center">2</td>
                                                                                <td>
                                                                                    <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you sell any intangible-- OR Do you collect sales tax ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="client_intangible" id="patient_type_2" class="form-control-accordion" readonly>
                                                                                        <option value="">---Select Answers---</option>
                                                                                        <option value="0" @if($common->client_intangible == 0) selected @endif>No</option>
                                                                                        <option value="1" @if($common->client_intangible == 1) selected @endif>Yes</option>

                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="ccd" data-patienttype="No" id="4">
                                                                                <td class="text-center">3</td>
                                                                                <td>
                                                                                    <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you buy any tobacco or cigar from mfg. company ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="client_tobacco" id="patient_type_2" class="form-control-accordion" readonly>
                                                                                        <option value="0" @if($common->client_tobacco == 0) selected @endif>No</option>
                                                                                        <option value="1" @if($common->client_tobacco == 1) selected @endif>Yes</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>

                                                                            <tr class="ccd" data-patienttype="No" id="6">
                                                                                <td class="text-center">4</td>
                                                                                <td>
                                                                                    <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you have COAM in your business ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="client_coam" id="patient_type_2" class="form-control-accordion" readonly>
                                                                                        <option value="0" @if($common->client_coam == 0) selected @endif>No</option>
                                                                                        <option value="1" @if($common->client_coam == 1) selected @endif>Yes</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>

                                                                            <tr class="ccd" data-patienttype="No" id="5">
                                                                                <td class="text-center">5</td>
                                                                                <td>
                                                                                    <input type="text" name="type_of_entity[]" readonly="" placeholder="Name" value="Do you have to pay Personal Property Tax ?" class="form-control-accordion">
                                                                                </td>
                                                                                <td>
                                                                                    <select name="client_pay" id="patient_type_2" class="form-control-accordion" readonly>
                                                                                        <option value="0" @if($common->client_pay == 0) selected @endif>No</option>
                                                                                        <option value="1" @if($common->client_pay == 1) selected @endif>Yes</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>


                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="tab-pane fade" id="tab9primary">
                                                    <div class="Branch">
                                                        <h1>Service Information</h1>
                                                    </div>
                                                    <div class="">
                                                        <div class="form-group bg-color">
                                                            <input class=" form-control fsc-input" type="hidden" value='' name="text1" id="text1">
                                                            <label class="control-label col-md-3" style="width: 20%;text-align:right;">Price Selection :</label>
                                                            <div class="col-md-2">
                                                                <input name="pricetype" type="text" id="pricetype" class="form-control" style="pointer-events:none; background-color: lightgreen!important;" value="@if(empty($clientser5->pricetype)) @else {{$clientser5->pricetype}} @endif">
                                                            </div>
                                                            <label class="control-label col-md-1" style="width: 106px;">Currency :</label>
                                                            <div class="col-md-2">
                                                                <input @foreach($currency as $cur) @if(empty($clientser5->currency)) @else @if($clientser5->currency==$cur->id) value="{{$cur->currency.' '.$cur->sign}}" @endif @endif   @endforeach name="currency" type="text" id="currency" class="form-control" style="pointer-events:none;background-color: lightgreen!important;">
                                                                <input name="ckq" type="hidden" id="ckq" class="ckq" value="@if(empty($clientser5->sign)) @else {{$clientser5->sign}} @endif">
                                                            </div>
                                                            <label class="control-label col-md-1" style="width: 142px;">Service Period :</label>
                                                            <div class="col-md-2">
                                                                <input @foreach($period as $period1) @if(empty($clientser5->serviceperiod)) @else @if($clientser5->serviceperiod==$period1->id) value="{{$period1->period}}" @endif @endif  @endforeach name="serviceperiod" type="text" id="serviceperiod" class="form-control" style="pointer-events:none;background-color: lightgreen!important;">
                                                            </div>
                                                        </div>
                                                        <?php $clientser2 = count($clientser);?>
                                                        @if($clientser2 !=NULL)
                                                            @foreach($clientser as $clientser1)
                                                                <?php //print_r($clientser1);
                                                                ?>
                                                                <div class="form-group ser" id="ser_{{$clientser1->id}}">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Type of Service :</label>
                                                                            <input name="typeofservice[]" type="text" @foreach($typeofser as $typeofser1) @if($clientser1->typeofservice==$typeofser1->id)
                                                                            value="{{$typeofser1->typeofservice}}" @endif  @endforeach class="form-control" style="pointer-events:none; background:#ccc !important;">
                                                                        </div>
                                                                        <div id="regular" @if(!empty($clientsertitle1->id) && ($clientsertitle1->servicetype==$clientser1->typeofservice)) style="display:none" @endif>
                                                                            <div class="col-md-6">
                                                                                <label class="control-label">Service Includes :</label>
                                                                                <input name="serviceincludes[]" type="text" id="serviceincludes_{{$clientser1->id}}" class="form-control" value="{{$clientser1->serviceincludes}}" style="pointer-events:none; background:#ccc !important;">
                                                                                <input name="serviceid[]" type="hidden" id="serviceid" class="form-control" value="{{$clientser1->id}}">
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <label class="control-label">Employee Responsible :</label>
                                                                                <input name="eeresponce[]" type="text" id="eeresponce{{$clientser1->id}}" class="form-control" @foreach($empname as $empname1) <?php $ename = $empname1->firstName . ' ' . $empname1->middleName . ' ' . $empname1->lastName;?>  @if($clientser1->employee_id==$empname1->id)
                                                                                value="{{$ename}}" @endif  @endforeach style="pointer-events:none; background:#ccc !important;">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    @foreach($clientsertitle as $clientsertitle1)
                                                                        <div class="input_fields_wrap_notes col-md-12" @if(!empty($clientsertitle1->id) && ($clientsertitle1->servicetype==$clientser1->typeofservice)) style="margin-top: 20px;" @else style="display:none" @endif >
                                                                            <table class="table" id="table">
                                                                                <thead>
                                                                                <tr style="background:#00a0e3;">
                                                                                    <th scope="col">Service</th>
                                                                                    <th scope="col">Note</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td><input type="hidden" name="taxid[]" value="{{$clientsertitle1->id}}"></td>
                                                                                    <td><input type="text" placeholder="Note" class="form-control" value="{{$clientsertitle1->taxnote}}" name="taxnote[]"></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="form-group" style="background-color:#fff5dd;padding:15px 0 23px 0;;width: 100%;margin: auto;">
                                                                <div class="col-md-3">
                                                                    <label class="control-label">Type of Service :</label>
                                                                    <select name="typeofservice[]" type="text" id="typeofservice" class="form-control">
                                                                        <option value="">Type of Service</option>
                                                                        @foreach($typeofser as $typeofser1)
                                                                            <option value="{{$typeofser1->id}}">{{$typeofser1->typeofservice}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div id="regular">
                                                                    <div class="col-md-4">
                                                                        <label class="control-label">Service Includes :</label>
                                                                        <input name="serviceincludes[]" type="text" id="serviceincludes" class="form-control">
                                                                        <input name="serviceid[]" type="hidden" id="serviceid" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label">Employee Responsible :</label>
                                                                    <input name="eeresponce[]" type="text" id="eeresponce" class="form-control">
                                                                </div>

                                                                <br>
                                                                <br>
                                                                <div class="input_fields_wrap_notes" style="display:none">
                                                                    <br>
                                                                    <br>
                                                                    <table class="table" id="table">
                                                                        <thead>
                                                                        <tr style="background:#00a0e3;">
                                                                            <th scope="col" style="width: 262px;">Title</th>
                                                                            <th scope="col">Note</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="width: 262px;"></td>
                                                                            <td><input type="text" placeholder="Note" class="form-control" name="taxnote[]"><input type="hidden" placeholder="Note" class="form-control" name="taxid[]"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="education_service" id="education_service"></div>
                                                </div>

                                        </div>

                                        <div class="card-footer">
                                            <div class="col-md-2 col-md-offset-3">
                                                <input class="btn_new_save" type="submit" value="Save">
                                            </div>
                                            <div class="col-md-2 row">
                                                <a class="btn_new_cancel" href="{{url('/client/home')}}">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- copy of input fields group -->
        <div class="form-group fieldGroupCopy" style="display: none;">
            <div class="input-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Contact Person Name : </label>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input" name="contact_person_name[]" id="contact_person_name" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Telephone No. : </label>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input telephone" name="telephone[]" id="telephone" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Business : </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input" name="business[]" id="business" value="">
                    </div>
                </div>
                <input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Business Fax : </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" id="business_fax1" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Residence : </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input" name="residence[]" id="residence" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Cell : </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Residence Fax : </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                        <label class="fsc-form-label">Email: </label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="">
                    </div>
                    <div class="pull-right" style="margin-top: 10px;">
                        <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div id="myModalafterlogin" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="headings">Instruction to Client</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">

                            <table class="table table-bordered tablestriped">
                                <thead>
                                <tr colspan="5">
                                    <th class="text-center" rowspan="5"><h4>Please subscribe</h4></th

                                </tr>
                                </thead>

                            </table>

                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="modal-footer">
                        <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!---------------------->


        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <script>
            $(document).ready(function () {

                $(document).on('click', '#tabs_notactive1', function () {
                    //alert();
                    $("#myModalafterlogin").modal('show');

                });

                $(document).on('change', '#location_county', function () {
                    var id = $(this).val();
                    $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                        $('#physical_county_no').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#physical_county_no').val(subcatobj.countycode);
                        })
                    });
                });
                $(document).on('change', '#business_state_1', function () {
                    var id = $(this).val();
                    $.get('{!!URL::to('getcounty1')!!}?id=' + id, function (data) {
                        $('#location_county').empty();
                        $('#physical_county_no').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#location_county').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                            $('#physical_county_no').val(subcatobj.countycode);
                        })
                    });
                });

            });


            $(document).ready(function () {
                $(document).on('change', '#business_license2', function () {
                    var id = $(this).val();
                    $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                        $('#business_license1').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_license1').val(subcatobj.countycode);
                        })
                    });
                });
            });
        </script>

        <script>
            var bus = $('.address_type').val();
            if (bus == 'Business') {
                $('.hidee1').show();
                $('.hidee').hide();
            }
            if (bus == 'Physical') {
                $('.hidee1').show();
                $('.hidee').hide();

            }


            $('.address_type').change(function () {

                var thiss = $(this).val();
                if (thiss == 'Business') {
                    $('.hidee').hide();
                    $('.hidee1').show();

                    var address = $('#address').val();
                    $('.formation_address').val(address);

                    var city = $('#city').val();
                    $('.formation_city').val(city);

                    var state = $('#stateId').val();
                    //alert(state);
                    $('#formation_state_1').val(state);


                    var zip = $('#zip').val();
                    $('.formation_zip').val(zip);

                    $('.formation_address').prop('readonly', true);
                    $('.formation_city').prop('readonly', true);
                    $('.formation_zip').prop('readonly', true);
                    $('#formation_state_1').prop('readonly', true);
                } else if (thiss == 'Physical') {
                    $('.hidee').hide();
                    $('.hidee1').show();
                    $('.formation_state').hide();
                    var state = $('#stateId').val();
                    $('#formation_state_1').val(state);

                    var address = $('#business_address').val();
                    //alert(address);
                    $('.formation_address').val(address);

                    var city = $('#business_city_1').val();
                    $('.formation_city').val(city);

                    var zip = $('#bussiness_zip_1').val();
                    $('.formation_zip').val(zip);

                    $('.formation_address').prop('readonly', true);
                    $('.formation_city').prop('readonly', true);
                    $('.formation_zip').prop('readonly', true);
                    $('#formation_state_1').prop('readonly', true);
                } else {
                    $('.hidee').show();
                    $('.hidee1').hide();
                    $('.formation_address').show();
                    $('.formation_address').val('');
                    $('.formation_city').val('');
                    $('.formation_zip').val('');


                    $('.formation_address').prop('readonly', false);
                    $('.formation_city').prop('readonly', false);
                    $('.formation_zip').prop('readonly', false);
                }

            });

            $('#saleofbusiness').change(function () {
                if ($(this).is(":checked")) {
                    $('#saleofbus').removeClass('disablebox');
                } else {
                    $('#saleofbus').addClass('disablebox');
                }
            });

            $('#transferbusiness').change(function () {
                if ($(this).is(":checked")) {
                    $('#trfbus').removeClass('disablebox');
                } else {
                    $('#trfbus').addClass('disablebox');
                }
            });
        </script>

        <script>
            $(document).on("change", ".numeric1", function () {
                var sum = 0;
                //	alert(sum);
                $(".numeric1").each(function () {
                    sum += +$(this).val().replace("%", "");
                    var num = parseFloat($(this).val());
                    $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                });
                if (sum > 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.primary').addClass("event");
                } else if (sum < 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("event");
                } else if (sum = 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.primary').removeClass("event");
                } else {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.primary').removeClass("event");
                }
            });
        </script>
        <script>
            function FillBilling1(f) {
                if (f.billingtoo11.checked == true) {
                    f.business_address_2.value = f.address.value;
                    f.business_address_3.value = f.address1.value;
                    f.business_city_1.value = f.city.value;
                    f.business_state_1.value = f.stateId.value;
                    f.bussiness_zip_1.value = f.zip.value;
                    f.business_country_1.value = f.countryId.value;

                    $('#business_city_1').prop("readonly", true);
                    $('#business_state_1').addClass("important");
                    $('#bussiness_zip_1').prop("readonly", true);
                    $('#business_country_1').addClass("important");
                    $('#location_county').addClass("important");
                    $('#business_address').prop("readonly", true);
                    $('#business_address_1').prop("readonly", true);
                    var id = f.stateId.value;//alert(id);
                    $.get('{!!URL::to('getcounty1')!!}?id=' + id, function (data) {
                        $('#location_county').empty();
                        $('#physical_county_no').empty();
                        $.each(data, function (index, subcatobj) {

                            $('#location_county').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                            $('#physical_county_no').val(subcatobj.countycode);
                        })
                    });
                } else {
                    f.business_address_2.value = '';
                    f.business_address_3.value = '';
                    f.business_city_1.value = '';
                    f.business_state_1.value = '';
                    f.bussiness_zip_1.value = '';
                    f.business_country_1.value = '';
                    f.location_county.value = '';
                    f.physical_county_no.value = '';
                    $('#business_city_1').prop("readonly", false);
                    $('#business_state_1').removeClass("important");
                    $('#bussiness_zip_1').prop("readonly", false);
                    $('#business_country_1').removeClass("important");
                    // $('#business_city_1').prop( "readonly", false );
                    //  $('#location_county').removeClass("important");
                    $('#business_address').prop("readonly", false);
                    $('#business_address_1').prop("readonly", false);
                }
            }

            function FillBilling2(f) {
                if (f.billingtoo1.checked == true) {
                    f.agent_fname2.value = f.agent_fname.value;
                    f.agent_mname2.value = f.agent_mname.value;
                    f.agent_lname2.value = f.agent_lname.value;
                }
            }
        </script>
        <script>
            $('.yess').on('change', function () {
                var ths = $(this).val();
                if (ths == 'Yes') {
                    $('.hidetr').show();
                } else {
                    $('.hidetr').hide();
                }

            });

            $(document).ready(function () {

                function showbanknamevid() {
                    $('#banknamediv').show();
                    $('#uparrow').show();
                    // $('#dwnarrow').show();

                    $('#banktype').val('1');
                }

                function showbanknamevid02() {
                    $('#banknamediv').show();
                    $('#uparrow').show();
                    $('#dwnarrow').hide();

                    // $('#banktype').val('1');
                }

                function hidebanknamevid() {
                    $('#banknamediv').hide();
                    $('#uparrow').hide();
                    $('#dwnarrow').hide();
                    $('#banktype').val('2');
                }

                function hidebanknamevid02() {
                    $('#banknamediv').hide();
                    $('#uparrow').hide();
                    $('#dwnarrow').show();
                    // $('#banktype').val('2');
                }

                function showcreditcardnamevid() {
                    $('#creditcardnamediv').show();
                    $('#uparrow2').show();
                    $('#dwnarrow2').hide();
                    $('#banktype1').val('1');
                }

                function showcreditcardnamevid02() {
                    $('#creditcardnamediv').hide();
                    $('#uparrow2').hide();
                    $('#dwnarrow2').show();
                }

                function showcreditcardnamevid1() {
                    $('#creditcardnamediv1').show();
                    $('#businessloan').val('1');
                    $('#uparrow3').show();
                    $('#dwnarrow3').hide();
                }

                function showcreditcardnamevid102() {
                    $('#creditcardnamediv1').hide();
                    $('#uparrow3').hide();
                    $('#dwnarrow3').show();
                    $('#businessloan').val('1');
                }


                function showcreditcardnamevid2() {
                    $('#creditcardnamediv1').hide();
                    $('#uparrow3').hide();
                    $('#dwnarrow3').hide();
                    $('#businessloan').val('2');
                }

                function hidecreditcardnamevid102() {
                    $('#creditcardnamediv1').show();
                    $('#uparrow3').show();
                    $('#dwnarrow3').hide();
                    $('#businessloan').val('2');
                }

                function hidecreditcardnamevid() {
                    $('#creditcardnamediv').hide();
                    $('#banktype1').val('2');
                }


                function hidecreditcardnamevid02() {
                    $('#creditcardnamediv').hide();
                    $('#uparrow2').hide();
                    $('#dwnarrow2').show();
                    //$('#banktype1').val('2');
                }


                var var22 = $('#typeofservice22').val();
                var var33 = $('#typeofservice33').val();

                if (var22 == '' || var33 == '') {
                    $("#typeofcorps").val('');
                    $("#due_date_1").val('');
                    $("#due_date_2").val('');
                    $("#extension_due_date_1").val('');
                    $("#extension_due_date_2").val('');
                    $("#type_form_file").val('');
                    $("#type_form_file2").val('');
                }

                if (var22 == 'S Corporation' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-1120S');
                    $("#due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#type_form_file").val('Form-1120S');
                    $("#type_form_file2").val('Form-600S');
                }

                if (var22 == 'C Corporation' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-1120');
                    $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_1").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#type_form_file").val('Form-1120');
                    $("#type_form_file2").val('Form-600');
                } else if ((var22 == 'Single Member LLC' || var22 == 'Individual') && var33 == 'GA') {
                    $("#typeofcorps").val('Form-1040');
                    $("#due_date_1").val('15-Apr');
                    $("#due_date_2").val('15-Apr');
                    $("#extension_due_date_1").val('15-Oct');
                    $("#extension_due_date_2").val('15-Oct');
                    $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_1").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#type_form_file").val('Form-1040');
                    $("#type_form_file2").val('Form-500');
                } else if (var22 == 'LLC-2 or more member' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-1065');
                    $("#due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#type_form_file").val('Form-1065');
                    $("#type_form_file2").val('Form-700');
                } else if (var22 == 'Estate' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-706');
                    $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#type_form_file").val('Form-706');
                    $("#type_form_file2").val('');
                } else if (var22 == 'Fiduciary' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-1041');
                    $("#due_date_1").val('');
                    $("#due_date_2").val('');
                    $("#extension_due_date_1").val('');
                    $("#extension_due_date_2").val('');
                    $("#type_form_file").val('Form-1041');
                    $("#type_form_file2").val('Form-501');
                } else if (var22 == 'Tax Exempt' && var33 == 'GA') {
                    $("#typeofcorps").val('Form-990');
                    $("#due_date_1").val('<?php echo date("M", strtotime("2 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    $("#due_date_2").val('');
                    $("#extension_due_date_1").val('');
                    $("#extension_due_date_2").val('');
                    $("#type_form_file").val('Form-990');
                    $("#type_form_file2").val('');
                }


                $('#typeofservice22').on('change', function () {
                    // alert(this.value);
                    var state = $("#typeofservice33").val();
                    typeofcorp = $("#typeofservice22").val();
                    alert(state);
                    $("#typeofcorp").val(typeofcorp);
                    if (this.value == 'C Corporation' && state == 'GA') {
                        $("#typeofcorps").val('Form-1120');
                        $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_1").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#type_form_file").val('Form-1120');
                        $("#type_form_file2").val('Form-600');
                    } else if (this.value == 'S Corporation' && state == 'GA') {
                        $("#typeofcorps").val('Form-1120S');
                        $("#due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#type_form_file").val('Form-1120S');
                        $("#type_form_file2").val('Form-600S');

                    } else if ((this.value == 'Single Member LLC' || this.value == 'Individual') && state == 'GA') {
                        $("#typeofcorps").val('Form-1040');
                        $("#due_date_1").val('15-Apr');
                        $("#due_date_2").val('15-Apr');
                        $("#extension_due_date_1").val('15-Oct');
                        $("#extension_due_date_2").val('15-Oct');
                        $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_1").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#type_form_file").val('Form-1040');
                        $("#type_form_file2").val('Form-500');
                    } else if (this.value == 'LLC-2 or more member' && state == 'GA') {
                        $("#typeofcorps").val('Form-1065');
                        $("#due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#type_form_file").val('Form-1065');
                        $("#type_form_file2").val('Form-700');
                    } else if (this.value == 'Estate' && state == 'GA') {
                        $("#typeofcorps").val('Form-706');
                        $("#due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_1").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#extension_due_date_2").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#type_form_file").val('Form-706');
                        $("#type_form_file2").val('');
                    } else if (typeofcorp == 'Fiduciary' && state == 'GA') {
                        $("#typeofcorps").val('Form-1041');
                        $("#due_date_1").val('');
                        $("#due_date_2").val('');
                        $("#extension_due_date_1").val('');
                        $("#extension_due_date_2").val('');
                        $("#type_form_file").val('Form-1041');
                        $("#type_form_file2").val('Form-501');
                    } else if (this.value == 'Tax Exempt' && state == 'GA') {
                        $("#typeofcorps").val('Form-990');
                        $("#due_date_1").val('<?php echo date("M", strtotime("2 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        $("#due_date_2").val('');
                        $("#extension_due_date_1").val('');
                        $("#extension_due_date_2").val('');
                        $("#type_form_file").val('Form-990');
                        $("#type_form_file2").val('');
                    }
                });


                $('#form_number_1').on('change', function () {
                    if (this.value == '') {
                        $("#quarter_date").val('')
                        $("#federal_frequency11").val('');
                        $(".federal_payment_frequency").show();
                    } else if (this.value == 'Form-941') {
                        $("#federal_frequency11").val('Quarterly');
                        $(".federal_payment_frequency_941").show();
                        $(".federal_payment_frequency_944").hide();
                        $(".business2").show();

                    } else if (this.value == 'Form-944') {
                        $("#quarter_date").val('')
                        $("#federal_frequency11").val('Annually');
                        $(".federal_payment_frequency_944").show();
                        $(".federal_payment_frequency_941").hide();
                        $("#federal_frequency_due_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        $("#quarter_date").val('<?php echo date("M", strtotime("9 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $(".business2").hide();
                    }
                });


                $('#federal_payment_frequency').on('change', function () {
                    if (this.value == '') {
                        $(".federal_payment_frequency").show();
                    } else if (this.value == 'Monthly') {
                        if (this.value == 'Monthly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'May' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        }
                    } else if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                });


                $('#federal_payment_frequency_annually').on('change', function () {
                    if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                    if (this.value == 'Annually') {
                        $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });


                $('#quarter_type').on('change', function () {
                    if (this.value == '') {
                        $("#quarter_date").val('');
                        $("#federal_frequency_due_date").val('');
                        $("#federal_payment_frequency_date2").val('');
                    } else if (this.value == '1st Qtr') {
                        $("#quarter_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                    } else if (this.value == '2nd Qtr') {
                        $("#quarter_date").val('<?php echo date("M", strtotime("3 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '3rd Qtr') {
                        $("#quarter_date").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_payment_frequency_date2").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '4th Qtr') {
                        $("#quarter_date").val('<?php echo date("M", strtotime("9 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        $("#federal_payment_frequency_date2").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');

                    }
                });


                //Federal Unemployment Tax (FUTA)

                $('#payment_frequency').on('change', function () {
                    if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                    if (this.value == 'Annually') {
                        $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });


                //State withholding

                $('#quarter_type_holding1').on('change', function () {
                    if (this.value == '') {
                        $("#quarter_date_holding").val('');
                        $("#federal_frequency_due_date_holding").val('');
                        $("#payment_frequency_due_date_holding").val('');

                    } else if (this.value == '1st Qtr') {
                        $("#quarter_date_holding").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                    } else if (this.value == '2nd Qtr') {
                        $("#quarter_date_holding").val('<?php echo date("M", strtotime("3 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_holding").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '3rd Qtr') {
                        $("#quarter_date_holding").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_holding").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '4th Qtr') {
                        $("#quarter_date_holding").val('<?php echo date("M", strtotime("9 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_holding").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        $("#payment_frequency_due_date_holding").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });


                $('#payment_frequency_type_holding2').on('change', function () {

                    if (this.value == 'Monthly') {
                        if (this.value == 'Monthly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'May' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        }
                    } else if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_holding").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                });

                //State Unemployment (SUTA)

                $('#quarter_type_unemploy1').on('change', function () {
                    if (this.value == '') {
                        $("#quarter_date_unemploy").val('');
                        $("#federal_frequency_due_date_unemploy").val('');
                        $("#payment_frequency_due_date_unemploy").val('');

                    } else if (this.value == '1st Qtr') {
                        $("#quarter_date_unemploy").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#federal_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                    } else if (this.value == '2nd Qtr') {
                        $("#quarter_date_unemploy").val('<?php echo date("M", strtotime("3 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '3rd Qtr') {
                        $("#quarter_date_unemploy").val('<?php echo date("M", strtotime("6 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');
                        $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>');

                    } else if (this.value == '4th Qtr') {
                        $("#quarter_date_unemploy").val('<?php echo date("M", strtotime("9 month", strtotime(date("M")))) . '-' . "31" . '-' . date("Y");?>')
                        $("#federal_frequency_due_date_unemploy").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });


                $('#payment_frequency_type_unemploy2').on('change', function () {

                    // if(this.value == 'Monthly')
                    // {
                    //     if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    //     else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                    //     {
                    //         $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    //     }
                    // }
                    if (this.value == 'Quarterly') {
                        //alert();
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                });


                //Federal Unemployment Tax (FUTA)
                $('#frequency').on('change', function () {
                    if (this.value == 'Annually') {
                        $("#frequency_due_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });

                $('#payment_frequency').on('change', function () {
                    if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    }
                    // else if(this.value == 'Annually')
                    // {
                    //     $("#payment_frequency_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    // }
                });


                //Sales Tax

                $('#saltax_period').on('change', function () {

                    if (this.value == 'Monthly') {
                        if (this.value == 'Monthly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'May' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        }
                    } else if (this.value == 'Quarterly') {
                        if (this.value == 'Quarterly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'May' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "30" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("4 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo date("M", strtotime("7 month", strtotime(date("M")))) . '-' . "20" . '-' . date("Y");?>');
                        } else if (this.value == 'Quarterly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo "Jan" . '-' . "20" . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo "Jan" . '-' . "20" . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        } else if (this.value == 'Quarterly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#saltax_due_date").val('<?php echo "Jan" . '-' . "20" . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                        }
                    } else if (this.value == 'Annually') {
                        $("#saltax_due_date").val('<?php echo "Jan" . '-' . "20" . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                    }
                });


                //Tobacco Excise Tax
                $('#excise_period').on('change', function () {
                    alert(this.value);
                    if (this.value == 'Monthly') {
                        if (this.value == 'Monthly' && 'Jan' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Feb' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Mar' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Apr' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'May' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jun' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Jul' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Aug' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Sep' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Oct' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Nov' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else if (this.value == 'Monthly' && 'Dec' == '<?php echo date("M");?>') {
                            $("#excise_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        }
                    }

                });

                //Personal Property Tax
                $('#personal_filing').on('change', function () {
                    if (this.value == 'No') {
                        $('.uname').hide();
                        $('.password').hide();
                    } else if (this.value == 'Yes') {
                        $('.uname').show();
                        $('.password').show();
                    }
                });


                $(document).on('change', '#typeofservice', function () {
                    //console.log('htm');
                    var id = $(this).val();// alert(id);
                    $.get('{!!URL::to('/faderal1')!!}?id=' + id, function (data) {
                        $('#typeofcorp').empty();
                        $('#typeofcorp1').empty();
                        $('#due_date_1').empty();
                        $('#type_form').empty();
                        $('#due_date_2').empty();
                        $('#type_form1').empty();
                        $('#fedral_state').empty();
//$('#type_form').append('<option value="">Select</option>');
//$('#type_form1').append('<option value="">Select</option>');
//$('#fedral_state').append('<option value="">Select</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#typeofcorp').val(subcatobj.authority_name);
                            $('#typeofcorp1').append('<option value="' + subcatobj.telephone + '">' + subcatobj.authority_name + ' (' + subcatobj.telephone + ')</option>');
                            $('#type_form').append('<option value="' + subcatobj.telephone + '">' + subcatobj.telephone + '</option>');
                            // $('#type_form').val(subcatobj.telephone);
                            $('#due_date_1').val(subcatobj.address);
                            $('#due_date_2').val(subcatobj.address);
                            $('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                            $('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#typeofcorp1', function () {
                    //console.log('htm');
                    var id = $(this).val(); //alert(id);
                    $.get('{!!URL::to('/getforms')!!}?id=' + id, function (data) {
                        $('#due_date_1').empty();
                        $('#type_form').empty();
                        $('#due_date_2').empty();
                        $('#type_form1').empty();
                        $('#fedral_state').empty();

                        $.each(data, function (index, subcatobj) {

                            $('#type_form').append('<option value="' + subcatobj.telephone + '">' + subcatobj.telephone + '</option>');
                            $('#typeofcorp').val(subcatobj.authority_name);
                            $('#due_date_1').val(subcatobj.address);
                            $('#due_date_2').val(subcatobj.address);
                            $('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                            $('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            // $("#business_no").mask("(999) 999-9999");
            $(".ext").mask("999");
            //$("#business_fax").mask("(999) 999-9999");
            $(".business_fax1").mask("(999) 999-9999");
            $(".residence_fax").mask("(999) 999-9999");
            $("#mobile_no").mask("(999) 999-9999");
            $(".cell").mask("(999) 999-9999");
            $(".telephone").mask("(999) 999-9999");
            $(".usapfax").mask("(999) 999-9999");
            $("#zip").mask("9999");
            $("#mailing_zip").mask("9999");
            $("#bussiness_zip").mask("9999");
        </script>
        <script>
            $(document).ready(function () {
                //group add limit
                var maxGroup = 3;

                //add more fields group
                $(".addMore").click(function () {
                    if ($('body').find('.fieldGroup').length < maxGroup) {
                        var fieldHTML = '<div class="form-group fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                        $('body').find('.fieldGroup:last').after(fieldHTML);
                    } else {
                        alert('Maximum ' + maxGroup + ' Persons are allowed.');
                    }
                });

                //remove fields group
                $("body").on("click", ".remove", function () {
                    $(this).parents(".fieldGroup").remove();
                });
            });
        </script>
        <script>
            function FillBilling(f) {
                if (f.billingtoo.checked == true) { //alert(f.zip.value);
                    f.mailing_address.value = f.address.value;
                    f.mailing_address1.value = f.address1.value;
                    f.mailing_city.value = f.city.value;
                    f.mailing_state.value = f.stateId.value;
                    f.mailing_zip.value = f.zip.value;
                } else {
                    f.mailing_address.value = '';
                    f.mailing_address1.value = '';
                    f.mailing_city.value = '';
                    f.mailing_state.value = '';
                    f.mailing_zip.value = '';
                }
            }
        </script>
        <script>
            function showDiv(elem) {
                if (elem.value == 'Federal') {
                    document.getElementById('hidden_div').style.display = "none";
                } else {
                    document.getElementById('hidden_div').style.display = "block";
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getcat1')!!}?id=' + id, function (data) {
                        $('#user_type').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#user_type').append('<option value="' + subcatobj.bussiness_name + '">' + subcatobj.bussiness_name + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();//alert(id);
                    $.get('{!!URL::to('getRequest2')!!}?id=' + id, function (data) {
                        $('#business_cat_id').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>

            $(document).ready(function () {
                $('.openBtn').on('click', function () {

                    //console.log('htm');
                    var num = $(this).attr("num"); //alert(num);
//alert(num);
                    $.get('{!!URL::to('/getimage12')!!}?id=' + num, function (data) {// alert();
                        if (data == "") {
                            $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                        } else {

                            $.each(data, function (index, subcatobj) { //alert();
                                if (num === subcatobj.upload_name) {
                                    $('#myModal').modal({show: true});

                                    $('#viewmodel').html('<embed src="http://financialservicecenter.net/public/clientupload/' + subcatobj.upload + '" width="100%" height="300px">');
                                } else {
                                    alert();
                                }
                            });
                        }
                    });
                });
            });
        </script>
        @foreach($professional as $ak1)
            @if($ak1->profession==NULL)
            @else


                <div id="myModalm99_{{$ak1->id}}" class="modal fade">

                    <div class="modal-dialog">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                <h4 class="modal-title">Confirmation</h4>

                            </div>

                            <div class="modal-body">

                                <p>Do you want to this record</p>


                            </div>

                            <div class="modal-footer">
                                <a href="{{ route('clientd.clientdelete',$ak1->id) }}" class="btn btn-danger">Delete</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                            </div>

                        </div>

                    </div>

                </div>

            @endif
        @endforeach


        @foreach($shareholder as $ak)
            @if($ak->agent_fname1==NULL)
            @else


                <div id="myModalk_{{$ak->id}}" class="modal fade">

                    <div class="modal-dialog">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                <h4 class="modal-title">Confirmation</h4>

                            </div>

                            <div class="modal-body">

                                <p>Do you want to this record</p>


                            </div>

                            <div class="modal-footer">
                                <a href="{{ route('client.clientdelete2',$ak->id) }}" class="btn btn-danger">Delete</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                            </div>

                        </div>

                    </div>

                </div>

            @endif
        @endforeach
        <div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">View License</h4>
                    </div>
                    <div class="modal-body">
                        <div id="viewmodel"></div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal2" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">View License</h4>
                    </div>
                    <div class="modal-body">
                        <h2>File Not Uploaded</h2>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).on("change", ".numeric1", function () {
                var sum = 0;
                $(".numeric1").each(function () {
                    sum += +$(this).val().replace("%", "");
                    var num = parseFloat($(this).val());
                    $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                    //$(".numeric2").val(num.toFixed(2));
                });
                if (sum > 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                } else if (sum < 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                } else if (sum = 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                } else {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                }
            });
        </script>
        <script>
            $(".effective_date").datepicker();
            var room = 1;

            function education_field11() {
                room++;
                var objTo = document.getElementById('education_field11')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass" + room);
                divtest.innerHTML = '<div class="col-md-2"><input name="agent_fname1[]" value="" type="text" id="agent_fname_1" placeholder="First name" class="textonly form-control" /></div><div class="col-md-1"><input name="agent_mname1[]" value="" type="text" placeholder="M" id="agent_mname" class="textonly form-control" /></div><div class="col-md-2"><input name="agent_lname1[]" value="" type="text" placeholder="Last Name" id="agent_lname_1" class="textonly form-control" /></div><div class=""><div class="col-md-2"><select id="agent_position_1" name="agent_position[]"  class="form-control fsc-input agent_position"><option value="">Position</option><option value="CEO">CEO</option><option value="CFO">CFO</option><option value="Secretary">Secretary</option><option value="Sec" >CEO / CFO / Sec.</option></select></div><div class="col-md-2"><input name="agent_per[]" value="" type="text" placeholder="" id="agent_per" class="txtOnly numeric1 form-control  num" /><input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid_1" class="textonly form-control" /><div class="cc"></div></div><div class=""><div class="col-md-2"><input name="effective_date[]" value="" type="text" placeholder="Effective Date" id="effective_date1" class="txtOnly form-control effective_date" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                var rdiv = 'removeclass' + room;
                var rdiv1 = 'Schoolname' + room;

                objTo.appendChild(divtest)
                $(".effective_date").datepicker();
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
            }


            $(document).ready(function () {
                $('#patient_type_2').change(function () {
                    if ($(this).val() == "No") {
                        $(".ccd").hide();
                        $('.ccd:first').show();
                        $('.ccd:nth-child(2)').show();
                    } else {
                        $(".ccd").show();
                    }

                });
                $('#patient_type_1').change(function () {
                    if ($(this).val() == "") {
                        $("#typeofservice").val('');
                    } else {
                        $("#typeofservice").val($(this).val());
                    }

                });


            });
        </script>
        <script type="text/javascript">
            $(function () {
                $(document).on("click", ".date", function () {
                    $(this).datepicker({
                        changeMonth: true,
                        changeYear: true,
                        format: 'M-dd-yyyy'
                    }).datepicker("show");
                });
            });


        </script>
        <script>
            var room1 = 0;
            var coun = 0;
            var z = room1 + coun;

            function education_field_note() {
                room1++;
                z++;
                var objTo = document.getElementById('input_fields_wrap_notes')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + z + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                objTo.appendChild(divtest)
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
//z--;
                room1--;
            }
        </script>
        <script>
            $(".effective_date").datepicker({
                autoclose: true,
                orientation: "top",
                endDate: "today"
            });
            var room = 1;

            function education_fields() {
                room++;
                var objTo = document.getElementById('education_fields')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass" + room);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + room + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="notetype[]" value="admin" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div></div>';
                var rdiv = 'removeclass' + room;
                var rdiv1 = 'Schoolname' + room;

                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
            }

            $('#filename').mask('aa-999-9999999');
            $('input[name="filename"]').focusout(function () {
                $('input[name="filename"]').val(this.value.toUpperCase());
            });
            var room1 = 1;

            function education_field() {

                room1++;
                var objTo = document.getElementById('education_field')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass1" + room1);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + room1 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="notetype[]" value="fsc" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><button class="btn btn-danger" type="button" onclick="remove_education_field(' + room1 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div>';
                var rdiv = 'removeclass1' + room1;
                var rdiv1 = 'Schoolname' + room1;

                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
            }

            function remove_education_field(rid) {
                $('.removeclass1' + rid).remove();
            }

            var room2 = 1;

            function education_field1() {
                room2++;
                var objTo = document.getElementById('education_field1')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass2" + room2);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + room2 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="notetype[]" value="client" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="usid[]" value="{{$common->cid}}" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><button class="btn btn-danger" type="button" onclick="remove_education_field1(' + room2 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div>';
                var rdiv = 'removeclass2' + room2;
                var rdiv1 = 'Schoolname' + room2;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
            }

            function remove_education_field1(rid) {
                $('.removeclass2' + rid).remove();
            }

            var room3 = 1;

            function education_field2() {

                room3++;
                var objTo = document.getElementById('education_field2')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass3" + room3);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + room3 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="notetype[]" value="Everybody" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><button class="btn btn-danger" type="button" onclick="remove_education_field2(' + room3 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div>';
                var rdiv = 'removeclass3' + room3;
                var rdiv1 = 'Schoolname' + room3;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
            }

            function remove_education_field2(rid) {
                $('.removeclass3' + rid).remove();
            }
        </script>
@endsection()