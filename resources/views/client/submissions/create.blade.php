@extends('client.layouts.app')
@section('title', 'Create Message')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>New Message </h1>
        </div>
        <section class="content" style="background-color: #fff;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('clientmsg.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date / Day / Time:</label>
                                    <div class="col-md-2" style="width: 150px;">
                                        <div class="">
                                            <input type="text" name="date" id="date" style="text-align:center;background-color: #ffff99;" readonly class="form-control" value="{{date('m/d/Y')}}" placeholder="Date">
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="width: 150px;">
                                        <div class="">
                                            <input type="text" name="day" id="day" style="text-align:center;background-color: #ffff99;" readonly class="form-control" placeholder="Day" value="{{date('l')}}">
                                        </div>
                                    </div>
                                    <input type="hidden" name="state" readonly id="state" value="employee" class="form-control">
                                    <div class="col-md-2" style="width: 145px;">
                                        <div class="">
                                            <input type="text" name="time" id="time" style="text-align:center;background-color: #ffff99;" readonly class="form-control" value="{{date("H:i a")}}" placeholder="Time">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Massage From :</label>
                                    <div class="col-md-3" style="width: 26.8% !important;">
                                        <div class="">
                                            <select name="type" id="type" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="Active">Client</option>
                                                <option value="employee">FSC-EE / User</option>
                                                <option value="Other Person">Other Person</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="emp" style="display:none">
                                    <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> :</label>
                                    <div class="col-md-3" style="width: 26.8% !important;">
                                        <div class="">
                                            <select name="employee" id="employee" class="form-control selectpicker fsc-input">
                                                <option value="">Select</option>
                                                @foreach($client as  $employee2)
                                                    <option value="{{$employee2->id}}">{{$employee2->first_name}} {{$employee2->middle_name}} {{$employee2->last_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="width:13% !important;">

                                        <input type="text" name="busname1" class="form-control" style="height:39px !important;text-align:center;background-color:#ffff99 !important;" readonly id="busname1">

                                    </div>
                                </div>
                                <div class="form-group" id="busname3" style="display:none">
                                    <label class="control-label col-md-3">Company Name :</label>
                                    <div class="col-md-5" style="width: 39.8% !important;">
                                        <div class="">
                                            <select name="busname2[]" id="busname2" class="form-control busname2 selectpicker fsc-input" multiple>
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group client" style="display:none">
                                    <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Business Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="busname" readonly id="busname" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client3" style="display:none">
                                    <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientname" readonly id="clientname" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clien2" style="display:none">
                                    <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> File # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientfile" readonly id="clientfile" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client1" style="display:none">
                                    <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Telephone # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientno" readonly id="clientno" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Massage To :</label>
                                    <div class="col-md-3" style="width: 26.8% !important;">
                                        <div class="">
                                            <select name="employees" id="employees" class="form-control selectpicker fsc-input">
                                                <option value="">Select</option>
                                                @foreach($employee1 as  $employee2)
                                                    <option value="{{$employee2->user_id}}" @if($employee->id==$employee2->user_id) style="display:none" @endif>{{$employee2->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Call Purpose :</label>
                                    <div class="col-md-4" style="width:26.8% !important;">
                                        <div class="">
                                            <select name="purpose" id="purpose2" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                @foreach($purpose as $pr)
                                                    <option value="{{$pr->purposename}}">{{$pr->purposename}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button></div>-->
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">What is the Message :</label>
                                    <div class="col-md-5" style="width:40% !important;">
                                        <div class="">
                                            <input type="text" name="othermsg" id="othermsg" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Message Priority :</label>
                                    <div class="col-md-2" style="width:27% !important;">
                                        <div class="">

                                            <select name="purpose1" id="purpose1" class="form-control fsc-input">
                                                <option value="Regular">Regular</option>
                                                <option value="Important">Important</option>
                                                <option value="Urgent">Urgent</option>

                                            </select>
                                            <input type="hidden" value="" name="other" id="other" class="form-control fsc-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-3">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input class="btn_new_save  btn-primary1 primary1" type="submit" name="submit" value="send">
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="btn_new_cancel" href="{{url('/client/clientmsg')}}">Cancel</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
    <script>
        $(".selectpicker").select2({})
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#type', function () {
                var id = $(this).val();
                $('#clientno').val("");
                $('#busname').val("");
                $('#clientfile').val("");
                if (id == 'Active') {
                    $('.client').hide();
                    $('.client1').hide();
                    $('.emp2').hide();
                    $('.emp3').show();
                    $('#busname2').show();
                    $('#emp').show();
                    $('.client3').hide();
                }
                if (id == 'employee') {
                    $('#busname2').show();
                    $('.emp3').hide();
                    $('#emp').show();
                    $('.emp2').show();
                    $('.client').hide();
                    $('.client1').hide();
                    $('.client3').hide();
                }
                $('#employee').empty();
                if (id == 'Other Person') {
                    $('.clien2').hide();
                    $('.emp3').hide();
                    $('#emp').hide();
                    $('.emp2').hide();
                    $('#busname2').hide();
                    $('.client').show();
                    $('.client1').show();
                    $('.client3').show();
                    document.getElementById('busname').removeAttribute('readonly');
                    document.getElementById('clientfile').removeAttribute('readonly');
                    document.getElementById('clientname').removeAttribute('readonly');
                    document.getElementById('clientno').removeAttribute('readonly');
                }
                $.get('{!!URL::to('/getmessagesclient')!!}?id=' + id, function (data) {
                    $('#employee').empty();
                    $('#busname1').empty();
                    // $('#employee').append('<option value="">---Select---</option>');
                    $.each(data, function (index, subcatobj) {
                        if (id == 'employee') {
                            $('#busname1').val(subcatobj.employee_id);
                            $('#employee').append('<option value="' + subcatobj.id + '" selected>' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');
                        }
                        if (id == 'Active') {
                            $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.last_name + '</option>');
// $('#busname1').val(subcatobj.filename);
                        }
                    })
                });
            });
            $(document).on('change', '#employee', function () {
                var id = $(this).val(); //alert(id);
                $.get('{!!URL::to('/getmessageclients')!!}?id=' + id, function (data) {
                    //   $('#employee').empty();
                    $('#busname1').empty();
                    $('.busname2').empty();
                    // $('.busname2').append('<option value="">---Select---</option>');
                    $.each(data, function (index, subcatobj) {
                        $('.busname2').append('<option value="' + subcatobj.id + '">' + subcatobj.company_name + ' (' + subcatobj.business_name + ') (' + subcatobj.filename + ')</option>');
                        $('#busname3').show();

//  $('.busname2').html(subcatobj.company_name);

                    })
                });
            });
        });

        $("#date").datepicker({
            'dateFormat': 'yy-mm-dd',
            onSelect: function (dateText) { //alert();
                var seldate = $(this).datepicker('getDate');
                seldate = seldate.toDateString();
                seldate = seldate.split(' ');
                var weekday = new Array();
                weekday['Mon'] = "Monday";
                weekday['Tue'] = "Tuesday";
                weekday['Wed'] = "Wednesday";
                weekday['Thu'] = "Thursday";
                weekday['Fri'] = "Friday";
                weekday['Sat'] = "Saturday";
                weekday['Sun'] = "Sunday";
                var dayOfWeek = weekday[seldate[0]];
                $('#day').val(dayOfWeek);
            }
        });
    </script>
    <script>
        $(document).ready(function () {
            $("select[name='busname2'] option:selected").index();
//$("#busname2 option:first").attr('selected','selected');
            $(document).on('change', '#employee', function () {
                var id = $("#employee").select2('val');
                var selectedCountry = $("#type option:selected").val();
                $.get('{!!URL::to('/getmessagesclient')!!}?id=' + id + '&state=' + selectedCountry, function (data) {
                    $('#clientno').val("");
                    $('#busname').val("");
                    $('#clientfile').val("");
                    $.each(data, function (index, subcatobj) {
                        document.getElementById('busname').readOnly = true;
                        document.getElementById('clientfile').readOnly = true;
                        document.getElementById('clientno').readOnly = true;
                        if ('employee' == subcatobj.type) {
                        }
                        if ('Active' == subcatobj.status) {
                            $('.client').hide();
                            $('.client1').hide();
                            $('.clien2').hide();
                            $('#clientno').val(subcatobj.business_no);
                            $('#busname').val(subcatobj.business_name);
                            $('#clientfile').val(subcatobj.filename);
                            $('#busname1').val(subcatobj.filename);
                        }
                    })
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#date").change(function () {
                var startdate = $("#date").val();
                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var durtion = $('#duration').val();
                var date = new Date(startdate);
                var day = weekday[date.getDay()];
                var monthly = 30;
                var weekly = 7;
                var bimonthly = 15;
                var biweekly = 14;
                var monthss = monthNames[(date.getMonth())];
                var yearss = date.getFullYear();
                var yyy = yearss % 4;

                if (durtion == "Weekly") {
                    var totaldays = 6;
                } else if (durtion == "Monthly") {
                    if (monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec') {
                        var totaldays = 30;
                    } else if (monthss == 'Feb') {
                        //if(years / 4 = 0)
                        if (yyy == 0) {
                            var totaldays = 28;
                        } else if (yyy == 1) {
                            var totaldays = 27;
                        }
                    } else if (monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov') {
                        var totaldays = 29;
                    }
                } else if (durtion == "Bi-Weekly") {
                    var totaldays = 13;
                } else if (durtion == "Bi-Monthly") {
                    var totaldays = 14;
                }
                // var vv = day + totaldays;

                date.setDate(date.getDate() + totaldays);// alert(vv);
                var date1 = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
                // alert(date1);
                var newdate = new Date(date1);
                var day1 = weekday[newdate.getDay()];
                //alert(newdate);
                var date2 = monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear();
                // $('#sch_end_date').val(date1);
                $('#day').val(day);
                $('#sch_end_day').val(day1);
                //document.write(date2);
            });
            $("#duration").change(function () {
                $('#sch_end_date').val('');
                $('#sch_start_date').val('');
            });
        });
        $("#clientno").mask("(999) 999-9999");
    </script>
    <style>
        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 39px;
            border-redius: 4px;
            user-select: none;
            -webkit-user-select: none;
        }

        .select2 {
            width: 100% !important;
        }

        .select2-container .select2-selection--single {
            border: 2px solid #00468F;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: blue;
        }

    </style>
@endsection()