@extends('client.layouts.app')
@section('title', 'Employee Reports')
@section('main-content')
    <style>
        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 0px !important;
            vertical-align: inherit !important;
        }

        .headerclr {
            background: #ffff99 !important;
        }

        .table > thead > tr > th {
            background: none !important;
        }

        .addagemodal .form-control:focus, .addagemodal .form-control:hover {
            background: #ffff99;
        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h2>Employee Timesheet Report</h2>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success" style="margin-bottom:0px !important;">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12 col-md-offset-1">
                            <form enctype='multipart/form-data' class="form-horizontal changepassword" action="{{url('client/employeereport')}}" id="changepassword" method="GET">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group ">
                                            <label for="focusedinput" class="col-md-3 control-label">Employee Name : <span class="required"> * </span></label>
                                            <div class="col-sm-4">
                                                <div class="">
                                                    <select class="form-control" name="emp_name" id="emp_name">
                                                        <option value="">---Select Employee---</option>
                                                        @foreach($schedule1 as $schedule2)
                                                            @foreach($emp as $e)
                                                                @if($schedule2->emp_name==$e->id)
                                                                    <option value="{{$schedule2->emp_name}}">{{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}}</option>
                                                                @endif @endforeach
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="focusedinput" class="col-md-3 control-label">Period : <span class="required"> * </span></label>
                                            <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                                                <div class="col-sm-3">
                                                    <input class="form-control" name="startdate" id="startdate" type="text">
                                                </div>
                                                <label for="focusedinput" class="control-label" style="width: 0px;float: left;margin-right: 1px;position: relative;left: -6px;">To</label>
                                                <div class="col-sm-3">
                                                    <input class="form-control" name="enddate" id="enddate" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="focusedinput" class="col-md-3 control-label"></label>
                                            <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                                                <div class="col-sm-3">
                                                    <button class="btn-success btn" type="submit" style="width: 50%;" name="search"> Ok</button>
                                                </div>
                                                <div class="col-sm-9">

                                                    <button type='button' id='btn' style="margin:0px 0px 0px 15px" class='pull-right buttons-print' value='Print' onclick='printDiv();' style="color:#fff" title="Print"><i class="fa fa-print"></i> Print</button>
                                                    <button type="button" id="btnExport" style="margin:0px 0px 0px 15px" value="PDF" class="pull-right buttons-pdf" title="Pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>
                                                    <button style="margin:0px 0px 0px 15px; color:#ffffff;" class="pull-right buttons-excel" id="btnexcel" title="Excel" style="color:#fff;"><i class="fa fa-file-text-o"></i> Excel</button>
                                                    <a href="#" id="add_report1" style="background:#e67300 !important;" role='button' data-toggle="modal" style="margin:0px 0 0px 15px;" data-target="#myModal2" data-sfid='{{$user_id}}' class="pull-right btn btn-primary">Add Day</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    </form>
                </div>
            </div>
            <style>
                .table > thead > tr > th {
                    background: #98c4f2;
                    text-align: center;
                }

                td {
                    text-align: center !important;
                }

                .form-group {
                    margin-bottom: 15px;
                    width: 100%;
                    float: left;
                }
            </style>
            <?php
            if(empty($_GET['enddate']) && empty($_GET['startdate']))
            {
                //echo "<p>Data not Found</p>";
            }
            else
            {
            ?>

            <div class="row">

                <div class="col-md-12">
                    <div class="card" style="background:#fff">
                        <div class="card-body">

                        </div>
                        <div class="card-body" id="dvContents">
                            <table class="table table-hover table-bordered" style="margin-top:0px !important;" id="sampleTable3">
                                <thead>
                                <tr>
                                    <th colspan="9" style="background: #fff;"><h2 class="text-center"><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="140px" alt=""></h2></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                    <th style="display:none"></th>
                                </tr>
                                @foreach($emp as $e)
                                    @if($e->id==$user_id)
                                        <tr class="headerclr">
                                            <th colspan="3" style="background: aquamarine !important;"><p style="margin-top:10px;" class="text-center">Employee ID: <span style="margin-left:5px">{{ucfirst($e->employee_id)}}</span></p></th>
                                            <th colspan="3" style="background: aquamarine !important;"><p style="margin-top:10px;" class="text-center">Employee Name: <span style="margin-left:5px">{{ucfirst($e->firstName.' '.$e->middleName.' '.$e->lastName)}}</span></p></th>
                                            <th colspan="3" style="background: aquamarine !important;"><p style="margin-top:10px;" class="text-center"><b>Period: <span style="margin-left:5px"><?php echo $_REQUEST['startdate'];?> To <?php echo $_REQUEST['enddate'];?></span></b></p></th>
                                            <th style="display:none"></th>
                                            <th style="display:none"></th>
                                            <th style="display:none"></th>
                                            <th style="display:none"></th>
                                            <th style="display:none"></th>
                                            <th style="display:none"></th>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr class="headerclr">
                                    <th>Date <br> Day</th>
                                    <th>Clock <br> In</th>
                                    <th>Break 1 <br> In / Out</th>
                                    <th>Break 2 <br> In / Out</th>
                                    <th>Clock <br> Out</th>
                                    <th>Total <br> Hours(D)</th>
                                    <th style="width: 300px;">Employee <br> Notes</th>
                                    <th>IP Address</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $startDate = strtotime($_GET['startdate']);
                                $endDate = strtotime($_GET['enddate']);
                                $interval = $endDate - $startDate;
                                $days = floor($interval / (60 * 60 * 24));

                                //echo $days . " until Christmas";
                                function time_to_decimal($time)
                                {
                                    $timeArr = explode(':', $time);
                                    $decTime = (($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60)) / 60;
                                    return $decTime;
                                }
                                $sum = 0;
                                $sum2 = 0;
                                $sum3 = 0;
                                $overtime = 0;
                                $overtime3 = 0;
                                ?>
                                <?php
                                $count = 1;
                                $count;
                                $startDate = date('Y-m-d', strtotime($_REQUEST['startdate']));
                                $endDate = date('Y-m-d', strtotime($_REQUEST['enddate']));
                                $resultDays = array('Monday' => 0,
                                    'Tuesday' => 0,
                                    'Wednesday' => 0,
                                    'Thursday' => 0,
                                    'Friday' => 0,
                                    'Saturday' => 0,
                                    'Sunday' => 0);

                                // change string to date time object
                                $startDate = new DateTime($startDate);
                                $endDate = new DateTime($endDate);
                                // iterate over start to end date
                                while($startDate <= $endDate ){



                                // find the timestamp value of start date
                                $timestamp = strtotime($startDate->format('d-m-Y'));
                                $weekDay1 = date('Y-m-d', $timestamp);
                                // find out the day for timestamp and increase particular day
                                $weekDay = date('l', $timestamp);
                                if($weekDay == 'Sunday' || $weekDay == 'Saturday'){ ?>
                                <tr bgcolor="lightgray" @if($weekDay == 'Sunday') class="div_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Saturday') class="div1_<?php echo $weekDay1;?>" @endif>
                                    <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay }} </td>
                                    <?php if($weekDay == 'Sunday' || $weekDay == 'Saturday') { ?>
                                    <td colspan="9"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <?php }
                                    else {
                                        ?>
                        <?php
                                    }?>
                                </tr>
                                <?php }

                                if($weekDay == 'Sunday' ){?>
                                @if($schedule->duration=='Bi-Weekly')
                                    <tr bgcolor="#f5b605" style=" height:20px " @if($weekDay == 'Sunday') class="div_<?php echo $weekDay1;?>" @endif >
                                        <td><?php   if (round($count) % 2 != 0) {
                                                echo "<span>1st Week End</span>";
                                            } else {
                                                echo "2nd" . ' ' . "Week End";
                                            } $count++;?>  </td>
                                        <?php if($weekDay == 'Sunday' || $weekDay == 'Saturday') { ?>
                                        <td colspan="9"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td>
                                        <td style="display: none;"></td><?php }
                                        else {
                                            ?>
                        <?php
                                        }?>
                                    </tr>  @endif <?php }?>

                                @foreach($employee2 as $emp)
                                    @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                                        <?php
                                        $in = date('H:i', strtotime($emp->emp_in));
                                        $out = date('H:i', strtotime($emp->emp_out));
                                        $lunch_in = date('H:i', strtotime($emp->launch_in));
                                        $lunch_out = date('H:i', strtotime($emp->launch_out));
                                        $lunch_in1 = date('H:i', strtotime($emp->launch_in_second));
                                        $lunch_out1 = date('H:i', strtotime($emp->launch_out_second));
                                        if ($emp->emp_out == NULL) {
                                            $total = "00:00:00";
                                        } else {
                                            $total = date("H:i:s", strtotime($emp->total));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime1 == NULL) {
                                            $breaktime1 = "00:00:00";
                                        } else {
                                            $breaktime1 = date("H:i:s", strtotime($emp->breaktime1));
                                        }
                                        $total1 = time_to_decimal($total) - (time_to_decimal($breaktime) + time_to_decimal($breaktime1));
                                        $sum2 += $total1;
                                        $time3 = "08.00";
                                        if ($total1 >= $time3) {
                                            $overtime2 = $total1 - $time3;
                                            $overtime += $overtime2;
                                        } else {
                                            $overtime = "0.00";
                                        }
                                        ?>
                                    @endif
                                @endforeach

                                @foreach($employee3 as $emp)
                                    @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                                        <?php
                                        $in = date('H:i', strtotime($emp->emp_in));
                                        $out = date('H:i', strtotime($emp->emp_out));
                                        $lunch_in = date('H:i', strtotime($emp->launch_in));
                                        $lunch_out = date('H:i', strtotime($emp->launch_out));
                                        $lunch_in1 = date('H:i', strtotime($emp->launch_in_second));
                                        $lunch_out1 = date('H:i', strtotime($emp->launch_out_second));
                                        if ($emp->emp_out == NULL) {
                                            $total = "00:00:00";
                                        } else {
                                            $total = date("H:i:s", strtotime($emp->total));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime1 == NULL) {
                                            $breaktime1 = "00:00:00";
                                        } else {
                                            $breaktime1 = date("H:i:s", strtotime($emp->breaktime1));
                                        }
                                        $total1 = time_to_decimal($total) - (time_to_decimal($breaktime) + time_to_decimal($breaktime1));
                                        $sum3 += $total1;
                                        $time11 = "08.00";
                                        if ($total1 >= $time11) {
                                            $overtime2 = $total1 - $time11;
                                            $overtime3 += $overtime2;
                                        } else {
                                            $overtime3 = "00.00";
                                        }
                                        ?>
                                    @endif
                                @endforeach
                                @foreach($employee1 as $emp)
                                    @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                                        <?php
                                        $in = date('H:i', strtotime($emp->emp_in));
                                        $out = date('H:i', strtotime($emp->emp_out));
                                        $lunch_in = date('H:i', strtotime($emp->launch_in));
                                        $lunch_out = date('H:i', strtotime($emp->launch_out));
                                        $lunch_in1 = date('H:i', strtotime($emp->launch_in_second));
                                        $lunch_out1 = date('H:i', strtotime($emp->launch_out_second));
                                        if ($emp->emp_out == NULL) {
                                            $total = "00:00:00";
                                        } else {
                                            $total = date("H:i:s", strtotime($emp->total));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime == NULL) {
                                            $breaktime = "00:00:00";
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($emp->breaktime));
                                        }
                                        if ($emp->breaktime1 == NULL) {
                                            $breaktime1 = "00:00:00";
                                        } else {
                                            $breaktime1 = date("H:i:s", strtotime($emp->breaktime1));
                                        }
                                        $total1 = time_to_decimal($total) - (time_to_decimal($breaktime) + time_to_decimal($breaktime1));
                                        $sum += $total1;
                                        ?>
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Sunday')
                                            <style>.div_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Saturday')
                                            <style>.div1_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Monday')
                                            <style>.div2_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Tuesday')
                                            <style>.div3_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Wednesday')
                                            <style>.div4_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Friday')
                                            <style>.div5_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Thursday')
                                            <style>.div6_<?php echo $weekDay1;?> {
                                                    display: none
                                                }</style> @endif


                                        <tr>
                                            <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay}} </td>
                                            <td @if($emp->clock_in_status=='2') style="color:red" @endif>{{ date("g:i a", strtotime($in))}}</td>
                                            <td @if($emp->launch_in_status=='2') style="color:red" @endif>@if($emp->launch_in== null) --/-- @else {{ date("g:i a", strtotime($lunch_in))}}<br>{{date("g:i a", strtotime($lunch_out))}}@endif</td>
                                            <td @if($emp->launch_in_status_1=='2') style="color:red" @endif>@if($emp->launch_in_second == null) --/-- @else {{ date("g:i a", strtotime($lunch_in1))}}<br>{{date("g:i a", strtotime($lunch_out1))}}@endif</td>
                                            <td @if($emp->clock_out_status=='2') style="color:red" @endif>@if($emp->emp_out== null) <span style="color:red">Running</span> @else{{ date("g:i a", strtotime($out))}}@endif</td>
                                            <td>@if($emp->emp_out== null) --/-- @else{{number_format($total1, 2)}}@endif</td>
                                            <td>@if($emp->work_note== null)  @else
                                                    <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{$emp->work_note}} ">Preview</button>@endif</td>
                                            <td @if($emp->ip_address=='Manually Add') style="color:red" @endif>@if($emp->ip_address== null) --/-- @else {{$emp->ip_address}} @endif</td>
                                            <td id="cur_{{$emp->id}}" style="width: 66px;">
                                                <center><a role='button' data-toggle="modal" data-target="#myModal2_{{$emp->id}}" href="#" style="background: #1685cc;color: #fff;padding: 5px;border-radius: 5px;" data-id='{{$emp->id}}'><i class="fa fa-edit"></i></a>
                                                    <a href="#" id="{{$emp->id}}" class="delete" style="background:red;color: #fff;padding: 5px;border-radius: 5px;"><i class="fa fa-trash"></i></a></center>
                                            </td>
                                        </tr>
                                        @if(date("l",strtotime($emp->emp_in_date)) == 'Sunday')

                                            <tr bgcolor="#f5b605" style=" height:30px ">
                                                <td>First Week End</td>
                                                <td colspan="9"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                                <td style="display: none;"></td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                <?php
                                if($weekDay == 'Monday' || $weekDay == 'Tuesday' || $weekDay == 'Wednesday' || $weekDay == 'Thursday' || $weekDay == 'Friday'){ ?>
                                <tr bgcolor="#C9A5A5" style="font-weight:bold;color:black" @if($weekDay == 'Monday') class="div2_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Tuesday') class="div3_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Friday') class="div5_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Thursday') class="div6_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Wednesday') class="div4_<?php echo $weekDay1;?>" @endif>
                                    <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay }} </td>
                                    <?php if($weekDay == 'Monday' || $weekDay == 'Tuesday' || $weekDay == 'Wednesday' || $weekDay == 'Thursday' || $weekDay == 'Friday') { ?>
                                    <td colspan="8" STYLE="border-bottom:1px solid !important;"></td><?php }  else { ?>
                        <?php } ?>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                </tr>
                                <?php } ?>
                                <?php

                                // increase startDate by 1
                                $startDate->modify('+1 day');
                                }

                                ?>
                                <tr>
                                    <td style="padding:5px !important;"></td>
                                </tr>
                                <tr style="background:rgb(255,255,0)">
                                    <td colspan="3" style="width: 10%;background: #f5b605;"><?php if($days > '13'){
                                            $gg3 = 00.00;
                                            $gg = 00.00;
                                        } else {?><span style="font-size: 16px;"><center>1st Week: Reg. Hrs. <b> <?php if($sum2 > 40){  $gg = $sum2 - 40.00; $gg3 = $sum2 - $gg;?> {{number_format($gg3, 2)}} / OT Hrs. {{number_format($gg, 2)}}<?php }else{ ?>{{number_format($sum2, 2)}} / OT Hrs. <?php echo $gg = "00.00"; $gg3 = $sum2; }?></b></center></span><?php }?></td>
                                    <?php  if ($sum3 > 40) {
                                        $gg1 = $sum3 - 40.00;
                                        $gg2 = $sum3 - $gg1; ?> <?php } else {
                                        $gg1 = $sum3;
                                        $gg2 = 0.00;
                                    }
                                    ?>
                                    <td style="width: 10%;" colspan="3"><span style="font-size: 16px;">
<center>Total Hrs.: <b>{{number_format($sum, 2)}}</b><br> 
<?php $gg1 = $sum3 - 40.00;
    $finalot = $gg + $gg1;?>
    <?php  $gg1 = $sum3 - 40.00; $gg2 = $sum3 - $gg1; ?> <?php $gg2s = $gg2;?><?php  ?>
Reg. Hrs.<b><span><?php $arr = $gg3 + $gg2s;?></span> <?php  if($sum > 80){ $gg5 = $sum - 80.00; $gg6 = $sum - $gg5; $kk = $gg2 + $gg; ?> {{number_format($arr, 2)}} / OT Hrs. {{number_format($finalot, 2)}} <?php } else if($sum < 80){ if ($sum2 < 40) {
            $gg = 00.00;
        } else {
            $gg = $sum2 - 40.00;
        } $gg3 = $sum2 - $gg;  if ($sum3 < 40.00) {
            $gg1 = 00.00;
        } else {
            $gg1 = $sum3 - 40.00;
        } $gl = $gg + $gg1; $gg2 = $sum3 - $gg1; $gg9 = $gg2 + $gg3;?> {{number_format($gg9, 2)}} / OT Hrs. {{number_format($gl, 2)}} <?php }  else{ ?> {{number_format($sum, 2)}}<?php } ?></b></center></span></td>
                                    <td style="width: 10%;background: #f5b605;" colspan="3"><?php if($days > '13'){
                                        } else {?><span style="font-size: 16px;">
<center>2nd Week: Reg. Hrs. <b><?php  if($sum3 > 40){ $gg1 = $sum3 - 40.00; $gg2 = $sum3 - $gg1; ?> {{number_format($gg2, 2)}} / OT Hrs. {{number_format($gg1, 2)}}<?php } else{ ?>{{number_format($sum3, 2)}} / OT Hrs. 0.00 <?php }?> </b></center></span><?php }?></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                </tr>

                                </tbody>
                            </table>
                            <!--<div><input type="checkbox" name="checkshhet" id="checksheet"> I confirmed above timesheet is correct.</div>-->
                        </div>
                    </div>
                </div>
            </div>
        <?php $count;?>
        <?php }?>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function () {
            $(".date").datepicker({
                format: 'mm/dd/yyyy',
            })
                .on("changeDate", function (e) {
                    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var checkInDate = e.date, $checkOut;
                    var day = checkInDate.getDay();
                    var das = weekday[day]
                    $("#clockin_am1").val(das);
                    $("#clockin_am").val(das); //alert(checkInDate);
                });
        });
    </script>
    <script type="text/javascript">

        $(".date1").datepicker({
            autoclose: true,
            format: "mm/dd/yyyy",
            //endDate: "today"
        });
    </script>
    <script>
        $(document).ready(function () {

            $("#startdate").datepicker({
                format: "mm/dd/yyyy",
                todayBtn: true,
                autoclose: true,
                //startDate: new Date()
            })
                .on("changeDate", function (e) {
                    var checkInDate = e.date, $checkOut = $("#enddate");
                    checkInDate.setDate(checkInDate.getDate() + 1);
                    $checkOut.datepicker("setStartDate", checkInDate);
                    $checkOut.datepicker("setDate", checkInDate).focus();
                });

            $("#enddate").datepicker({
                format: "mm/dd/yyyy",
                todayBtn: true,
                autoclose: true
            });

            // Call global the function
            $('.t-datepicker').tDatePicker({
                // options here
            });
        });
    </script>
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog addagemodal">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Day</h4>
                </div>
                <form action="" method="post" id="ajax">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="col-md-11 col-md-offset-0">
                            <input class="form-control" id="fullname" type="hidden" name="fullname" placeholder="fullname" value="@if(empty($user_id)) @else{{$user_id}}@endif"/>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Date / Day :
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <input type="text" class="form-control date" required name="date" id="date" placeholder=" Date">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <input type="text" class="form-control" required name="clockin_am" id="clockin_am1" placeholder="Clock in Day">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Clock In :
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <select class="form-control" name="clockin" id="clockin" required>
                                            <option value="">Select</option>
                                            <?php for($i = 00;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                            <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="">
                                        <div class="input-group">
                                            <select class="form-control" name="clockin_second" id="clockin_second" required>
                                                <option value="">Select</option>
                                                <?php for($i = 00;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Lunch In :
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <select class="form-control" name="lunchin">
                                            <option value="">Select</option>
                                            <?php for($i = 00;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                            <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="">
                                        <div class="input-group">
                                            <select class="form-control" name="lunchin_second">
                                                <option value="">Select</option>
                                                <?php for($i = 00;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Lunch Out :
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <select class="form-control" name="lunchout">
                                            <option value="">Select</option>
                                            <?php for($i = 00;$i < 24;$i++){$value = str_pad($i, 2, "0", STR_PAD_LEFT); ?>
                                            <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="">
                                        <div class="input-group">
                                            <select class="form-control" name="lunchout_second">
                                                <option value="">Select</option>
                                                <?php for($i = 00;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Clock Out :
                                </label>

                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="input-group">
                                        <select class="form-control" name="clockout" id="clockout" required>
                                            <option value="">Select</option>
                                            <?php for($i = 00;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                            <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                    <div class="">
                                        <div class="input-group">
                                            <select class="form-control" name="clockout_second" id="clockout_second" required>
                                                <option value="">Select</option>
                                                <?php for($i = 00;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                            <div class="form-group has-feedback">
                                <label for="confirmPassword" class="col-md-3 control-label">
                                    Note :
                                </label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <textarea type="text" class="form-control" required name="note" placeholder="Please Enter Your  Note"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>

                    </div>

                    <div class="modal-footer" style="width: 100%;display: inline-block;">
                        <input type="button" value="Ok" name="Ok" id="addopt" style="position: relative;width: 98px;left:11px;border-radius: 0;" class="btn btn-primary pull-left col-md-offset-3">
                        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });

    </script>

    <script>

        $(document).ready(function () {
            $('#myModal3').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('id');
                // alert(rowid);
                $('#fullname1').val(rowid);
            });
        });
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="https://www.aspsnippets.com/demos/scripts/table2excel.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#sampleTable3')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Financial-Report.pdf");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#btnexcel").click(function () {
                $("#sampleTable3").table2excel({
                    filename: "Financial-Report.xls"
                });
            });
        });

        function printDiv() {
            var divToPrint = document.getElementById('sampleTable3');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        }
    </script>
    <script>
        $(function () {
            $('#addopt').click(function () {//alert();
                var newopt = $('#date').val();
                var clockin = $('#clockin').val();
                var clockin_second = $('#clockin_second').val();
                var clockin_am = $('#clockin_am').val();
                var clockout = $('#clockout').val();
                var clockout_second = $('#clockout_second').val();
                var clockout_am = $('#clockout_am').val();
                if (newopt == '') {
                    alert('Please Select Your Date');
                } else if (clockin == '') {
                    alert('Please Select Your Clockin Hour');
                } else if (clockin_second == '') {
                    alert('Please Select Your Clockin MInute');
                } else if (clockin_am == '') {
                    alert('Please Select Your Clockin Second');
                } else if (clockout == '') {
                    alert('Please Select Your Clockout Hour');
                } else if (clockout_second == '') {
                    alert('Please Select Your Clockout Minute');
                } else if (clockout_am == '') {
                    alert('Please Select Your Clockout Second');
                } else {
                    $.ajax({
                        type: "post",
                        url: "{!!route('employeereport.store')!!}",
                        dataType: "json",
                        data: $('#ajax').serialize(),
                        success: function (data) {
                            $("tbody").load(" tbody > *");
                            location.reload();//alert();
                            $('#ajax')[0].reset();
                        },
                        error: function (data) {
                            alert("Error")
                        }
                    });
                }
                $('#myModal2').modal('hide');
            });
        });

        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('report.reportremove')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            $("#dvContents").load(" #dvContents > *");
                        }
                    })
                } else {
                    return false;
                }
            });
        });


        $(document).ready(function () {
            $("addopt2").click(function () {
                alert();
            });
        });
    </script>
    <style>.input-group {
            display: inherit;
        }</style>

    @foreach($employee1 as $student)
        <div class="modal fade" id="myModal2_{{$student->id}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Day</h4>
                    </div>
                    <form action="" method="post" id="ajax_{{$student->id}}">
                        <div class="modal-body">
                            {{ csrf_field() }} {{method_field('PATCH')}}

                            <div class="col-md-11 col-md-offset-0">
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Date / Day:
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <input type="text" value="{{date("m/d/Y",strtotime($student->emp_in_date))}}" class="form-control date" required name="date" id="date" placeholder="Date">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <select class="form-control" name="clockin_am" id="clockin_am" required>
                                                <option value="">Select</option>
                                                <?php
                                                $days = array();
                                                for ($i = 0; $i < 7; $i++) { $days[$i] = jddayofweek($i, 1);?>
                                                <option value="<?php echo $days[$i];?>" <?php if (date("l", strtotime($student->emp_in_date)) == $days[$i]) {
                                                    echo "selected";
                                                }?>><?php echo $days[$i];?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>

                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Clock In :
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <select class="form-control" name="clockin" id="clockin" required>
                                                <option value="">Select</option>
                                                <?php for($i = 0;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>" <?php if (date("H", strtotime($student->emp_in)) == $value) {
                                                    echo "selected";
                                                }?>><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="">
                                            <div class="input-group">
                                                <select class="form-control" name="clockin_second" id="clockin_second" required>
                                                    <option value="">Select</option>
                                                    <?php for($i = 00;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                    <option value="<?php echo $value;?>" <?php if (date("i", strtotime($student->emp_in)) == $value) {
                                                        echo "selected";
                                                    }?>><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Lunch In :
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <select class="form-control" name="lunchin">
                                                <option value="">Select</option>
                                                <?php for($i = 0;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>" <?php if ($student->launch_in == '') {
                                                } else {
                                                    if (date("H", strtotime($student->launch_in)) == $value) {
                                                        echo "selected";
                                                    }
                                                }?>><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="">
                                            <div class="input-group">
                                                <select class="form-control" name="lunchin_second">
                                                    <option value="">Select</option>
                                                    <?php for($i = 0;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                    <option value="<?php echo $value;?>" <?php if ($student->launch_in == '') {
                                                    } else {
                                                        if (date("i", strtotime($student->launch_in)) == $value) {
                                                            echo "selected";
                                                        }
                                                    }?>><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Lunch Out :
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <select class="form-control" name="lunchout">
                                                <option value="">Select</option>
                                                <?php for($i = 0;$i < 24;$i++){$value = str_pad($i, 2, "0", STR_PAD_LEFT); ?>
                                                <option value="<?php echo $value;?>" <?php if ($student->launch_out == '') {
                                                } else {
                                                    if (date("H", strtotime($student->launch_out)) == $value) {
                                                        echo "selected";
                                                    }
                                                }?>><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="">
                                            <div class="input-group">
                                                <select class="form-control" name="lunchout_second">
                                                    <option value="">Select</option>
                                                    <?php for($i = 0;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                    <option value="<?php echo $value;?>" <?php if ($student->launch_out == '') {
                                                    } else {
                                                        if (date("i", strtotime($student->launch_out)) == $value) {
                                                            echo "selected";
                                                        }
                                                    }?>><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Clock Out :
                                    </label>

                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="input-group">
                                            <select class="form-control" name="clockout" id="clockout" required>
                                                <option value="">Select</option>
                                                <?php for($i = 0;$i < 24;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                <option value="<?php echo $value;?>" <?php if (empty($student->emp_out)) {
                                                } else {
                                                    if (date("H", strtotime($student->emp_out)) == $value) {
                                                        echo "selected";
                                                    }
                                                };?>><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 inputGroupContainer">
                                        <div class="">
                                            <div class="input-group">
                                                <select class="form-control" name="clockout_second" id="clockout_second" required>
                                                    <option value="">Select</option>
                                                    <?php for($i = 0;$i < 60;$i++){ $value = str_pad($i, 2, "0", STR_PAD_LEFT);?>
                                                    <option value="<?php echo $value;?>" <?php if (empty($student->emp_out)) {
                                                    } else {
                                                        if (date("i", strtotime($student->emp_out)) == $value) {
                                                            echo "selected";
                                                        }
                                                    }?>><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-3 control-label">
                                        Note :
                                    </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <textarea type="text" class="form-control" required name="note" value="" placeholder="">{{$student->work_note}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                            </div>
                        </div>
                        <div class="modal-footer" style="display: inline-block;width: 100%;">
                            <input type="button" value="Ok" name="Ok" id="addopt_{{$student->id}}" style="position: relative;left:11px;border-radius: 0;width: 98px;" class="btn btn-primary pull-left col-md-offset-3">
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        </section>
        </div>
        <script>
            $(function () {
                $('#addopt_{{$student->id}}').click(function () {
                    $.ajax({
                        type: "post",
                        url: "{!!route('employeereport.update',$student->id)!!}",
                        dataType: "json",
                        data: $('#ajax_{{$student->id}}').serialize(),
                        success: function (data) {
                            $("tbody").load(" tbody > *");
                            location.reload();//alert();
                        },
                        error: function (data) {
                            alert("Error")
                        }
                    });
                });
            });
        </script>
    @endforeach
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection()