@extends('client.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title" style="margin-top:0px !important;height:45px !important;"><h1>Dashboard</h1>
            <div class="searchboxmain">
                <input type="text" name="search" id="country_name" class="form-control" placeholder="Search">
                <!--<select class="form-control">-->
                <!--    <option>Select</option>-->
                <!--</select>-->
                <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important;padding: 7px 18px;margin-left: 7px;"
                   href="https://financialservicecenter.net/client/home">Reset</a>{{ csrf_field() }}
                <ul id="countryList"></ul>
            </div>
        </section>

        <style>
            .content-header {
                padding: 12px 30px !important;
            }

            #countryList {
                margin: 40px 0px 0px -30px;
                padding: 0px 0px;
                border: 1px solid #025b90;
                max-height: 200px;
                overflow: auto;
                position: absolute;
                width: 380px;
                z-index: 99;
            }

            #countryList li {
                border-bottom: 1px solid #025b90;
                background: #ef7c30;
            }

            #countryList li a {
                padding: 0px 10px 0px 0px;
                display: block;
                color: #fff !important;
            }

            #countryList li:hover {
                background: #dc6d23;
            }

            .searchboxmain {
                float: right;
                display: FLEX;
                margin-top: -2.9%;
                justify-content: space-between;
            }

            .clear {
                clear: both;
            }

            #countryList li a {
                height: 40px;
            }

            #countryList li a span.bgcolors {
                background: linear-gradient(to bottom, #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;
                padding: 6px 0px;
                text-align: center;
                display: block;
                font-size: 20px;
                font-weight: bold;
                float: left;
                width: 60px;
            }

            #countryList li a span.clientalign {
                display: flex;
                width: 75px;
                float: left;
                line-height: 15px;
                padding: 4px 0px;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                align-content: center;
                min-height: 40px;
                background: #038ee0;
                margin-left: 0;
                padding-left: 5px;
            }

            #countryList li a span.entityname {
                display: flex;
                width: 200px;
                float: left;
                line-height: 15px;
                padding: 4px 0px;
                font-size: 13px;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                align-content: center;
                min-height: 38px;
                margin-left: 10px;
            }

            .modal.fade.in {
                display: block !important;
                opacity: 1 !important;
                padding-top: 50px;
            }

            .customers th {
                border: 1px solid #222222 !important;
                color: black !important;
            }

            .customers td {
                border: 1px solid #222222 !important;
            }

            .table-bordered > tbody > tr > td {
                border: 1px solid !important;
            }

            .customers th {
                background: #ffff99 !important;
            }

            .table-bordered {
                border: 1px solid !important;
            }

            .table-bordered > thead > tr > th {
                border: 1px solid !important;
            }

            .box-title {
                font-weight: bold;
            }

            .panel-title a:hover {
                color: #fff;
            }

            i.fa.fa-plus {
                color: #000;
            }

            i.fa.fa-minus {
                color: #000;
            }

            .customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            .customers td, .customers th {
                border: 1px solid #ddd;
                padding: 8px 5px;
            }

            .customers tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .customers tr:hover {
                background-color: #ddd;
            }

            .customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4cae4c;
                color: white;
            }

            .blink-img {
                left: 16px;
                position: absolute;
                top: 0;
                z-index: 999;
            }

            span.box-number {
                float: right;
                margin: 0 40px 0 0;
                font-size: 15px;
                font-weight: bold;
                background: #3a408e;
                color: #FFF;
                padding: 0px 16px;
                border-radius: 20px;
            }

            span.box-number2 {
                float: right;
                font-size: 15px;
                font-weight: bold;
                background: #b0dff5;
                color: #104b8f;
                padding: 0px 16px;
                border-radius: 20px;
            }

            .ui-dialog {
                z-index: 1999 !important;
            }

            .project-status-table {
                margin-top: 15px;
            }

            .project-status-table tbody tr {
                background: #104b8f;
                margin-bottom: 2px !important;
            }

            .project-status-table tbody tr td {
                color: #fff;
                font-size: 14px;
            }

            .project-status-table thead tr th {
                font-size: 14px;
                background-color: #ffff99 !important;
                border: solid 1px #999 !important;
            }

            .info-box {
                border: 1px solid #cccccc;
            }

            .info-box i {
                font-size: 40px !important;
            }

            .info-box-number {
                font-size: 25px;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                width: 95% !important;
            }

        </style>
        <section class="content" style="padding-top:10px !important;">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Employee</span>
                            <span class="info-box-number">0</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Message</span>
                            <span class="info-box-number">41,410</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sales</span>
                            <span class="info-box-number">760</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-user-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Task</span>
                            <span class="info-box-number">2,000</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>

            <div class="row" style="margin-bottom:20px;">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>

                    <div class="main-box">
                        <div class="box2" style="margin-bottom: 2px;">
                            <a data-toggle="modal" data-target="#exampleModal" class="btn btn-info information-btn3" style="background-color: #00c0ef !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">R</span>eminder</h3></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>
                    <div class="main-box">
                        <div class="box3" style="margin-bottom: 2px;background:#fa3e3e">
                            <a data-toggle="modal" data-target="#exampleModal1" class="btn btn-info information-btn3" style="background-color: #f39c12 !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">N</span>otification</h3></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>
                    <div class="main-box">
                        <div class="box" style="border-top: none;margin-bottom: 2px;background-color: #f39c12 !important;">
                            <a data-toggle="modal" data-target="#exampleModal2" class="btn btn-info information-btn" style="background-color: #dd4b39 !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">W</span>arning</h3></a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->
            <div class="box box-success">
                <div class="">
                    <div id="accordion" class="accordion">
                        <div class="card mb-0">
                            <div class="card-header collapse" data-toggle="collapse" href="#collapseOne">
                                <a class="card-title">
                                    Client
                                </a>
                            </div>
                            <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-aqua">
                                            <span class="info-box-icon"><img style="height:75px;width:75px;" src="https://financialservicecenter.net/public/business/Business.png"></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Business</span>
                                                <span class="info-box-number">24</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Business" style="color:#fff">Go to Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-green">
                                            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Non-Profit Organization</span>
                                                <span class="info-box-number">1</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Non-Profit Organization" style="color:#fff">Go to Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-yellow">
                                            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Service Industry</span>
                                                <span class="info-box-number">7</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Service Industry" style="color:#fff">Go To Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-red">
                                            <span class="info-box-icon"><img style="height:75px;width:75px;" src="https://financialservicecenter.net/public/business/Professions.png"></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Profession</span>
                                                <span class="info-box-number">1</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Profession" style="color:#fff">Go To Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-red">
                                            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Investor</span>
                                                <span class="info-box-number">1</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Investor" style="color:#fff">Go To Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="info-box bg-yellow">
                                            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Personal</span>
                                                <span class="info-box-number">1</span>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                 <a href="VARR-Admin/customer?user_type=Personal" style="color:#fff">Go To Page</a>
                                 </span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                </div>
                            </div>
                            <div class="collapse accordion__answer" style="display: block;">
                                <?php
                                function time_to_decimal($time)
                                {
                                    $timeArr = explode(':', $time);
                                    $decTime = (($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60)) / 60;
                                    return $decTime;
                                }
                                $sum = 0;
                                $k = 1;
                                //print_r($schedule);
                                ?>

                                @foreach($schedule as $schedule1)

                                    @foreach($set as $s)

                                        <?php $k++;?>
                                        @if($s->branch_city==$schedule1->emp_city)
                                            <div class="panel-group" id="accordion{{$s->branch_city}}">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="background-color: #286db5;border-color: #ddd;color: #fff;padding: 11px;">
                                                        <h4 class="panel-title" style="background:#ccffee;border:1px solid black;padding: 12px 20px;">
                                                            <a style="text-transform:uppercase; color:#222222 !important;" data-toggle="collapse" data-parent="#accordion{{$k}}" href="#collapseOne{{$k}}">
                                                                FSC-{{$s->branch_city}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $('#sampleTable{{$s->branch_city}}').DataTable();
                                                    </script>
                                                    <div id="collapseOne{{$k}}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered" id="sampleTable{{$s->branch_city}}">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Employee ID</th>
                                                                        <th>Employee Name</th>
                                                                        <th style="width:10%">Clock In</th>
                                                                        <th style="width:10%">Lunch In / Out</th>
                                                                        <th style="width:10%">Clock Out</th>
                                                                        <th>Status</th>
                                                                        <th style="width:10%">Total Hours(D)</th>
                                                                        <th style="width:15%">Note</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($employee as $emp)
                                                                        @if($emp->fsccity == $s->branch_city)
                                                                            @foreach($set1 as $emp1)
                                                                                @if($emp->employee_id== $emp1->id)
                                                                                    <?php
                                                                                    $in = date('H:i', strtotime($emp->emp_in));
                                                                                    $out = date('H:i', strtotime($emp->emp_out));
                                                                                    $lunch_in = date('H:i', strtotime($emp->launch_in));
                                                                                    $lunch_out = date('H:i', strtotime($emp->launch_out));
                                                                                    $launch_out_second = date('H:i', strtotime($emp->launch_out_second));
                                                                                    $launch_in_second = date('H:i', strtotime($emp->launch_in_second));
                                                                                    $total = date("H:i:s", strtotime($emp->emp_in));
                                                                                    if (empty($emp->emp_out)) {
                                                                                        $total1 = date("H:i:s");
                                                                                    } else {
                                                                                        $total1 = date("H:i:s", strtotime($emp->emp_out));
                                                                                    }
                                                                                    if (empty($emp->lunch_in)) {
                                                                                        $launch_out1 = "00:00:00";
                                                                                        $lunch_in1 = "00:00:00";
                                                                                    } else {
                                                                                        $lunch_in1 = date("H:i:s", strtotime($emp->launch_in));
                                                                                        if (empty($emp->lunch_out)) {
                                                                                            $launch_out1 = date("H:i:s");
                                                                                        } else {
                                                                                            $launch_out1 = date("H:i:s", strtotime($emp->launch_out));
                                                                                        }
                                                                                    }
                                                                                    if (empty($emp->launch_in_second)) {
                                                                                        $launch_in_second = "00:00:00";
                                                                                        $launch_out_second = "00:00:00";
                                                                                    } else {
                                                                                        $launch_in_second = date("H:i:s", strtotime($emp->launch_in_second));
                                                                                        if (empty($emp->launch_out_second)) {
                                                                                            $launch_out_second = date("H:i:s");
                                                                                        } else {
                                                                                            $launch_out_second = date("H:i:s", strtotime($emp->launch_out_second));
                                                                                        }
                                                                                    }
                                                                                    $total2 = (time_to_decimal($total1) - time_to_decimal($total)) - ((time_to_decimal($launch_out1) - time_to_decimal($lunch_in1)) + (time_to_decimal($launch_out1) - time_to_decimal($lunch_in1)));
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td>{{$emp1->employee_id}}</td>
                                                                                        <td>{{$emp1->firstName.' '.$emp1->middleName.' '.$emp1->lastName}}</td>
                                                                                        <td style="text-align:center">{{ date("g:i a", strtotime($in))}}</td>
                                                                                        <td style="text-align:center">@if($emp->launch_in== null) --/-- @else {{ date("g:i a", strtotime($lunch_in)).'-'.date("g:i a", strtotime($lunch_out))}}@endif</td>
                                                                                        <td style="text-align:center">@if($emp->emp_out== null) --/-- @else{{ date("g:i a", strtotime($out))}}@endif</td>
                                                                                        <td style="text-align:center">@if($emp->emp_out== null) <p style="color:green">In</p> @else <p style="color:red">Out</p> @endif</td>
                                                                                        <td style="text-align:center">{{number_format($total2, 2)}}</td>
                                                                                        <td style="text-align:center"><a href="">View</a></td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif

                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
            </div>
            <!-- /.row -->
        </section>
    </div>

    <!-- Modal -->
    <div id="newmyModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row form-group">
                            <label class="control-label col-md-3 text-right">ID:</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="entityids" readonly style="width:150px;"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-md-3 text-right">Name</label>
                            <div class="col-md-7">
                                <input class="form-control" type="text" id="names" readonly/>
                            </div>
                        </div>

                        <div class="companyphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 1</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control teltype" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="Ext" readonly/>
                                </div>


                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 2</label>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" id="teltype2" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="Ext" readonly/>
                                </div>


                            </div>
                        </div>
                        <div class="personalphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right"> Personal Telephone</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control teltype" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Telephone</label>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2Type" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>
                        </div>

                        <div class="row form-group companyemail" style="display:none;">
                            <label class="control-label col-md-3 text-right">Email</label>
                            <div class="col-md-7">
                                <input class="form-control emails" type="text" id="" readonly/>
                            </div>
                        </div>
                        <div class="personalemail" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Personal Email</label>
                                <div class="col-md-7">
                                    <input class="form-control emails" type="text" id="" readonly/>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Email</label>
                                <div class="col-md-7">
                                    <input class="form-control fscemail" type="text" id="" readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12 ">


                            </div>


                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!---------------------->


    <?php
    if(isset($common->productid) != '')
    {
    ?>

    <!-- Modal -->
    <div id="myModalafterlogin2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"> Linked Client</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <table class="table table-bordered tablestriped">
                            <thead>
                            <tr>
                                <th class="text-center">Client ID</th>
                                <th class="text-center">Client Name</th>
                                <th class="text-center">Action</th>


                            </tr>
                            </thead>


                            <tbody>
                            <tr>
                                <?php
                                $strposlink = $common->productid;
                                //print_r($strposlink);die;
                                $splittedstringposlink = explode(",", $strposlink);
                                ?>
                                @foreach($commonuser as $subcustomer1)
                                    <?php
                                    if($subcustomer1->filename != $common->filename)
                                    {
                                    if ($subcustomer1->business_id == '6') {
                                        $entityname = $subcustomer1->first_name . ' ' . $subcustomer1->last_name;
                                    } else {
                                        $entityname = $subcustomer1->company_name;
                                    }

                                    ?>

                                    @foreach($splittedstringposlink as $key => $value)

                                        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==1)
                                            <form method="post">

                                                <td class="text-center">
                                                    {{$subcustomer1->filename}}

                                                </td>
                                                <td class="text-center">{{$entityname}}</td>
                                                <td class="text-center">

                                                    <a href="https://financialservicecenter.net/client/home?filename={{$subcustomer1->filename}}" target="_blank">Show</a>

                                                </td>
                                            </form>
                                        @endif
                                    @endforeach
                                    <?php
                                    }
                                    ?>

                            </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!---------------------->
    <?php
    }
    ?>




    <?php
    if(isset($common->productid) != '')
    {
    ?>
    <!-- Modal -->
    <div id="myModalafterlogin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"> Linked Client</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <table class="table table-bordered tablestriped">
                            <thead>
                            <tr>
                                <th class="text-center">Client ID</th>
                                <th class="text-center">Client Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>


                            <tbody>
                            <tr>
                                <?php
                                $strposlink = $common->productid;
                                //print_r($strposlink);die;
                                $splittedstringposlink = explode(",", $strposlink);

                                ?>
                                @foreach($commonuser as $subcustomer1)
                                    <?php
                                    if($subcustomer1->filename != $common->filename)
                                    {
                                    if ($subcustomer1->business_id == '6') {
                                        $entityname = $subcustomer1->first_name . ' ' . $subcustomer1->last_name;
                                    } else {
                                        $entityname = $subcustomer1->company_name;
                                    }

                                    ?>
                                    @foreach($splittedstringposlink as $key => $value)
                                        <?php $m = 0;?><?php $m++;?>
                                        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==0)

                                            <form method="get" action="{{url('client/getusers',$common->filename)}}">

                                                <td class="text-center">{{$subcustomer1->filename}} </td>
                                                <td class="text-center">{{$entityname}}</td>
                                                <td class="text-center">

                                                    <input type="checkbox" name="filenamess2[]" value="{{$subcustomer1->filename}}">

                                                </td>
                                            @endif

                                            @endforeach

                                            <?php

                                            }
                                            // else if($common->filename!='')
                                            // {
                                            //     if($subcustomer1->business_id =='6')
                                            //       {
                                            //             $entityname=$subcustomer1->first_name.' '.$subcustomer1->last_name;
                                            //       }
                                            //       else
                                            //       {
                                            //             $entityname=$subcustomer1->company_name;
                                            //       }
                                            ?>


                                            <!--@if($subcustomer1->check_status==0)-->

                                            <!--<form method="get" action="{{url('client/getusers',$common->filename)}}">-->

                                            <!--<td class="text-center" style="background-color:#00FF7F;">{{$subcustomer1->filename}}</td>-->
                                            <!--<td class="text-center" style="background-color:#00FF7F;">{{$entityname}}</td>-->
                                                <!--<td class="text-center" style="background-color:#00FF7F;">-->

                                            <!--<input type="checkbox" name="filenamess2[]" value="{{$subcustomer1->filename}}">-->

                                                <!--</td>-->
                                            <!--@endif -->


                                            <?php
                                            // }
                                            ?>

                            </tr>
                            @endforeach

                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="text-center"><input type="submit" value="Submit"></td>


                            </tr>
                            </tbody>
                            </form>
                        </table>

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!---------------------->
    <?php
    }
    ?>




    <script>

        // $("#changefilename").on("change", function()
        // {
        //     var filename = $('#changefilename').val();
        //     //alert(filename);
        //     if(filename==000)
        //     {
        //         window.location = "https://financialservicecenter.net/client/home"; // redirect
        //     }
        //     else
        //     {
        //         window.location = "https://financialservicecenter.net/client/home?filename="+filename; // redirect
        //     }


        // });

        <?php
        if(session()->has('blog'))
        {
        $result = session()->get('blog');
        if($result == '1')
        {

        ?>

        <?php

        $strposlink = $common->productid;
        $splittedstringposlink = explode(",", $strposlink);
        ?>
        @foreach($commonuser as $subcustomer1)
        <?php
        if($subcustomer1->filename != $common->filename)
        {

        ?>
        @foreach($splittedstringposlink as $key => $value)
        <?php $m = 0;?><?php $m++;?>
        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==0)
        <?php
        if($m > 0)
        {
        ?>
        $(document).ready(function () {
            $("#myModalafterlogin").modal('show');
        });
        <?php
        }
        else {

        }
        ?>

        @endif
        @endforeach

        <?php
        }
        // else if($common->filename!='')
        // {
        ?>
        // @if($subcustomer1->check_status==0)

        //     $(document).ready(function()
        //     {
        //         $("#myModalafterlogin").modal('show');
        //     });

        // @endif
        <?php
        //}
        ?>

        @endforeach



        <?php
        $strposlink = $common->productid;
        //print_r($strposlink);die;
        $splittedstringposlink = explode(",", $strposlink);
        $m2 = count($splittedstringposlink);
        ?>
        @foreach($commonuser as $subcustomer1)
        <?php
        if($subcustomer1->filename != $common->filename)
        {

        ?>
        @foreach($splittedstringposlink as $key => $value)

        <?php $m = 0;?>
        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==1)

        <?php
        if($m == $m2)
        {

        }
        else
        {
        ?>
        $(document).ready(function()
        {
            $("#myModalafterlogin2").modal('show');
        }
        )
        ;
        <?php
        }
        ?>

        @endif
        <?php $m++;?>
        @endforeach

        <?php
        }
        ?>

        @endforeach

        <?php
        if (session()->has('blog')) {
            $result = session()->forget('blog');
        }
        }
        }
        ?>


        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });


        function myfun(id, type) {
            var _token = $('input[name="_token"]').val();

            $.ajax({
                url: "{{ route('fetch333') }}",
                method: "POST",
                data: {'id': id, 'type': type, _token: _token},

                success: function (data) {
                    //JSON.parse(data);
                    console.log(data);
                    var datashow = JSON.parse(data);
                    //       alert(datashow.company_name);
                    var entity = datashow.entityid;
                    var fname = datashow.fname;

                    var mname = datashow.mname;
                    var lname = datashow.lname;

                    var telephoneNo2 = datashow.telephoneNo2;
                    var telephoneNo2Type = datashow.telephoneNo2Type;
                    var fscemail = datashow.fscemail;


                    var firstext = datashow.ext1;
                    var secondext = datashow.ext2;


                    if (fname == null) {
                        var fnamess = '';
                    } else {
                        var fnamess = fname;
                    }


                    if (mname == null) {
                        var mnamess = '';
                    } else {
                        var mnamess = mname;
                    }

                    if (lname == null) {
                        var lnamess = '';
                    } else {
                        var lnamess = lname;
                    }


                    if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee' || datashow.Type == 'Vendor') {
                        var usertype = datashow.Type;

                    } else {
                        var usertype = 'Client';
                    }
                    // alert(usertype);
                    if (usertype == 'Client') {
                        if (datashow.business_id == '6') {
                            var headings = fnamess + ' ' + mnamess + ' ' + lnamess;
                        } else {

                            var headings = datashow.company_name;
                        }
                    } else if (datashow.Type == 'Vendor') {
                        var headings = datashow.company_name;
                    } else if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee') {
                        //  alert(datashow.fname);
                        if (fname == null) {
                            var fnamess = '';
                        } else {
                            var fnamess = fname;
                        }


                        if (mname == null) {
                            var mnamess = '';
                        } else {
                            var mnamess = mname;
                        }

                        if (lname == null) {
                            var lnamess = '';
                        } else {
                            var lnamess = lname;
                        }
                        var headings = fnamess + ' ' + mnamess + ' ' + lnamess;

                    }


                    if (usertype == 'Client') {
                        $('.companyphone').show();
                        $('.companyemail').show();

                        $('.personalphone').hide();
                        $('.personalemail').hide();


                        $('.ahref').html('<a href="https://financialservicecenter.net/client/customer/' + datashow.cid + '/edit" class="btn btn-primary" style="width:62px !important;">Go</a>');

                    } else if (usertype == 'employee' || usertype == 'user' || usertype == 'clientemployee' || usertype == 'Vendor') {

                        $('.companyphone').hide();
                        $('.companyemail').hide();

                        $('.personalphone').show();
                        $('.personalemail').show();

                        $('.ahref').html('<a href="https://financialservicecenter.net/client/employee/' + datashow.cid + '/edit" class="btn btn-primary">Go</a>');
                    }

                    if (fname == null) {
                        var fnames = ' ';
                    } else {
                        var fnames = fname;
                    }

                    if (mname == null) {
                        var mnames = ' ';
                    } else {
                        var mnames = mname;
                    }
                    if (lname == null) {
                        var lnames = '';
                    } else {
                        var lnames = lname;
                    }
                    if (datashow.business_id != '6') {
                        var fullname = datashow.firstname + ' ' + datashow.lastname;
                        var telephone = datashow.telephone;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                        var telephone2 = datashow.telephone2;


                    } else {
                        var fullname = fnames + ' ' + mnames + ' ' + lnames;
                        var telephone = datashow.telephone;
                        var telephone2 = datashow.telephone2;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                    }
                    var email = datashow.email;
                    $('#names').val(fullname);

                    $('#entityids').val(entity);
                    $('.emails').val(email);
                    $('.telephones').val(telephone);
                    $('#telephones2').val(telephone2);
                    $('.teltype').val(telephonetype);
                    $('#teltype2').val(telephonetype2);
                    $('#headings').html(headings);
                    $('.exts').val(firstext);
                    $('.extss').val(secondext);
                    $('.telephonesNo2').val(telephoneNo2);
                    $('.telephonesNo2Type').val(telephoneNo2Type);
                    $('.fscemail').val(fscemail);


                    var v = document.getElementById("newmyModal");
                    v.classList.add("in");

                    //  $('#countryList').html(data);


                }
            });


        }

        $('.close').click(function () {
            // alert();
            $('#newmyModal').removeClass('in');
        })

        $('.modal-footer .btn-default').click(function () {
            //  alert();
            $('#newmyModal').removeClass('in');
        })


    </script>
    <script>
        $(document).ready(function () {
            $('.btnpopclose').click(function () {
                // alert();
                $('.modal').removeClass('in');
                $('.modal').hide();
            })

            $('#country_name').keyup(function () {
                var query = $(this).val();
                //alert(query);
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('fetch33') }}",
                        method: "POST",
                        data: {query: query, _token: _token},
                        success: function (data) {
                            $('#countryList').fadeIn();
                            $('#countryList').html(data);


                        }
                    });
                }
            });
            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removemsg.removemsg1')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            location.reload(true);
                        }
                    })
                } else {
                    return false;
                }
            });
        });

        $('.accordion__answer:first').show();
        $('.accordion__question:first').addClass('expanded');

        $('.accordion__question').on('click', function () {
            var content = $(this).next();

            $('.accordion__answer').not(content).slideUp(400);
            $('.accordion__question').not(this).removeClass('expanded');
            $(this).toggleClass('expanded');
            content.slideToggle(400);
        });
    </script>


@endsection