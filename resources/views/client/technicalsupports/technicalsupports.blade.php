@extends('client.layouts.app')
@section('title', 'Technical Support')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title content-header">
            <h1>Edit / View Technical Support</h1>
        </div>


        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable3">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Subject</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rules as $rul)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$rul->subject}}</td>
                                    <td>{!!$rul->details!!}</td>
                                    <td>@if($rul->answer=='') <a href="#" class="btn btn-danger" style="padding: 6px 12px;">Pending</a> @else <a href="#" style="padding: 6px 12px;" class="btn btn-success">Done</a> @endif</td>
                                    <td><a class="btn-action btn-view-edit" href="{{route('technicalsupports.edit',$rul->id)}}"><i class="fa fa-edit"></i></a></td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection