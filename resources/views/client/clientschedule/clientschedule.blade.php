@extends('client.layouts.app')
@section('title', 'Schedule Details')
@section('main-content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1> Schedule Details</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <div class="table-title">
                                    <a href="{{route('clientschedule.create')}}" class="btn btn-primary">Add New Schedule</a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">

                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Employer City</th>
                                        <th>Duration</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($schedule as $employ)
                                        <tr>
                                            <td>@foreach($emp as $employ1)@if($employ1->id==$employ->emp_name){{$employ1->firstName}} @endif @endforeach</td>
                                            <td>{{$employ->emp_city}}</td>
                                            <td>{{$employ->duration}}</td>
                                            <td>{{$employ->sch_start_date}}</td>
                                            <td>{{$employ->sch_end_date}}</td>
                                            <td><a class="btn btn-success" href="{{route('clientschedule.edit', $employ->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('clientschedule.destroy',$employ->id) }}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn btn-danger" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()