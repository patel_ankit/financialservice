@extends('client.layouts.app')
@section('title', 'Create Employee')
@section('main-content')
    <style>
        .hide {
            display: none;
        }

        .fsc-form-label {
            text-align: right;
            width: 100%;
            padding: 4px 0 0;
        }

        .star-required {
            position: absolute;
            right: 6px;
        }

        .help-block {
            color: red;
            font-size: 16px;
        }

        label.file-upload {
            position: relative;
            overflow: hidden;
            float: left;
        }

        input[type="file"] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header page-title">
            <h1>Add Employee </h1>
        </section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('cli-employee.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <br>
                            <input type="hidden" name="clienttype" value="{{$clienttype_id}}">
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                    <div class="">
                                        <label class="control-label col-md-3 col-lg-3 fsc-form-row">Employee ID : <span class="star-required">*</span></span></label>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" name="employee_id" placeholder="GUA-999-9999" id="employee_id"> <input name="password1" value="199456533" class="form-control fsc-input" id="password1" readonly="" type="hidden">
                                                </div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <label class="fsc-form-label">Status : </label>
                                                </div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="check" class="form-control fsc-input Blue" id="check">
                                                        <option value="0" class="Blue">In-Active</option>
                                                        <option class="Green" value="1">Active</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Name : <span class="star-required">*</span></label>
                                    </div>

                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select class="form-control fsc-input" id="nametype" name="nametype">
                                                    <option value="mr">Mr.</option>
                                                    <option value="mrs">Mrs.</option>
                                                    <option value="miss">Miss.</option>
                                                </select>

                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                <input type="text" class="form-control fsc-input textonly" name='firstName' id="firstName" placeholder="First">
                                                @if ($errors->has('firstName'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                                                @endif

                                            </div>
                                            <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <div class="row">
                                                    <input type="text" class="form-control fsc-input textonly" maxlength="1" name='middleName' id="middleName" placeholder="M">
                                                    @if ($errors->has('middleName'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('middleName') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                <input type="text" class="form-control fsc-input textonly" name="lastName" id="lastName" placeholder="Last">
                                                @if ($errors->has('lastName'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('lastName') }}</strong>
                        </span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address1') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Address 1 : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" class="form-control fsc-input" id="address1" name='address1' placeholder="Address">
                                        @if ($errors->has('address1'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('address1') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Address 2 : </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" class="form-control fsc-input" id="address2" name='address2' placeholder="Address">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('countryId') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select name="countryId" id="countries_states1" class="form-control bfh-countries fsc-input" data-country="USA">

                                                </select>
                                                @if ($errors->has('countryId'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('countryId') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('city') ? ' has-error' : '' }}{{ $errors->has('stateId') ? ' has-error' : '' }}{{ $errors->has('zip') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="text" class="form-control textonly fsc-input" id="city" name='city' placeholder="City">@if ($errors->has('city'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" style='height:auto'>

                                                </select>
                                                <select class="form-control bfh-timezones" style="display:none" name="timezone" data-country="countries_states1"></select>
                                                @if ($errors->has('stateId'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('stateId') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="text" class="form-control fsc-input zip" id="zip" name='zip' placeholder="Zip" maxlength='5'>
                                                @if ($errors->has('zip'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo1') ? ' has-error' : '' }} {{ $errors->has('telephoneNo1Type') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Telephone 1 : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" id="telephoneNo1" name='telephoneNo1' placeholder="(999) 999-9999">
                                                @if ($errors->has('telephoneNo1'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input" style='height:auto'>
                                                    <option value='Mobile'>Mobile</option>
                                                    <option value='Home'>Home</option>
                                                    <option value='Work'>Work</option>
                                                    <option value='Office'>Office</option>
                                                    <option value='Other'>Other</option>
                                                </select>
                                                @if ($errors->has('telephoneNo1Type'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1Type') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="text" class="form-control fsc-input" id="ext1" readOnly name='ext1' placeholder="Ext">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo2') ? ' has-error' : '' }} {{ $errors->has('telephoneNo2Type') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Telephone 2 : </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" name='telephoneNo2' id="telephoneNo2" placeholder="(999) 999-9999">
                                                @if ($errors->has('telephoneNo2'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input" style='height:auto'>
                                                    <option value='Mobile'>Mobile</option>
                                                    <option value='Home'>Home</option>
                                                    <option value='Work'>Work</option>
                                                    <option value='Office'>Office</option>
                                                    <option value='Other'>Other</option>
                                                </select>
                                                @if ($errors->has('telephoneNo2Type'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2Type') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="text" class="form-control fsc-input" readOnly name='ext2' id="ext2" placeholder="Ext">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Email : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" class="form-control fsc-input" id="email" name='email' placeholder="Email Address">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('photo') ? ' has-error' : '' }}">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">New Photo : </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file">
                                        </label>
                                        @if ($errors->has('photo'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save" type="submit" value="Add">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="{{url('client/cli-employee')}}">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="" id="Register"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
$(document).ready(function() {

$('#content').bootstrapValidator({        
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
		fields: {


employee_id: {
                validators: {
                   	remote: {
						url: "{{ URL::to('/emp_id') }}",
						data: function(validator) {
							return {
								employee_id: validator.getFieldElements('employee_id').val()
							}
						},
						message: 'This Employee Id Already exit.'
					}
                }
            },
			firstName: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
			
             lastName: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },

email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email Address'
					},				
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					},
					
				
					remote: {
						url: "{{ URL::to('/emp_id1') }}",
						data: function(validator) {
							return {
								email: validator.getFieldElements('email').val()
							}
						},
						message: 'This Email Id Already exit.'
					}
						
                }
            },
            address1: {
                validators: {
                     stringLength: {
						min: 8,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
            city: {
                validators: {
                     stringLength: {
						min: 2,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
            telephoneNo1: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Phone Number'
                    }
                }
            },
            
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			
			
  
         
            }
        }).on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#content').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
               // console.log(result);
            }, 'json');
        });
});
</script>-->

    <script>
        ///  $("#telephoneNo1").mask("(999) 999-9999");
        $(".ext").mask("99999");
        $("#ext1").mask("99999");
        $("#ext2").mask("99999");
        //$("#telephoneNo2").mask("(999) 999-9999");
        // //$("#mobile_no").mask("(999) 999-9999");
        $(".usapfax").mask("(999) 999-9999");
        // $("#zip").mask("99999");
    </script>


    <script>
        var dat2 = $('#ext1').val();
        $('#telephoneNo1Type').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>
    <script>
        var dat2 = $('#ext2').val();
        $('#telephoneNo2Type').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })
    </script>
    <script>
        function show1() {
            document.getElementById('div1').style.display = 'none';
            document.getElementById('div2').style.display = 'block';
        }

        function show2() {
            document.getElementById('div1').style.display = 'block';
            document.getElementById('div2').style.display = 'none';
        }


        //	$('#employee_id').mask('999-999-9999');
        //$('input[name="employee_id"]').focusout(function() {
        //$('input[name="employee_id"]').val( this.value.toUpperCase() );
        //});

    </script>

@endsection()