<!DOCTYPE html>
<html lang="en">
<head>
    @include('front-section.head')
</head>
<body>
<div class="container-fluid">
    <div class="row" style="margin-top: 1%;">
        @include('front-section.header')
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
@include('front-section.leftsidebar')
@section('main-content')
@show
@include('front-section.footer')
</body>
</html