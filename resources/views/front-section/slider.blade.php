@foreach($slider as $slide)
    <div class="item @if ($loop->first) active @endif"><img class="img-responsive" src="{{URL::asset('public/slider')}}/{{$slide->slider_image}}" alt="{{$slide->slider_name}}"></div>
@endforeach	