<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="padding: 0px;" id="sidbarStickly">
    <div>
        <div class="sidebar-nav">
            <div class="navbar navbar-default" style="background-color: white; border-color: white; padding: 0px;" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="visible-xs navbar-brand" style="font-family:'Proza Libre'; color:#103b68; font-size:2.7em; letter-spacing: 2px;">MENU</span>
                </div>
                <div class="navbar-collapse collapse sidebar-navbar-collapse navigation" style="margin : 0px;">
                    <ul class="nav navbar-nav fsc-menu">
                        <li>
                            <a href="{{url('/')}}" class="fsc-menu-link">
                                <img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-01.png')}}">
                                <img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-01.png')}}">
                            </a>
                        </li>
                        <li class="ser">
                            <a href="{{URL::to('service')}}" class="fsc-menu-link">
                                <img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02.png')}}">
                                <img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02.png')}}"></a>
                            <ul class="submenu" style='padding-left:0px;'>
                                <li class="fis"><a class="fsc-menu-link1" id="accounting" href="{{URL::to('service')}}#16"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02-1.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02-1.png')}}"></a></li>
                                <li class="second"><a class="fsc-menu-link2" id="residential" href="{{URL::to('service')}}#17"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02-2.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02-2.png')}}"></a></li>
                                <li class="third"><a class="fsc-menu-link3" id="commercial" href="{{URL::to('service')}}#18"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02-3.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02-3.png')}}"></a></li>
                                <li class="fourth"><a class="fsc-menu-link4" id="financial" href="{{URL::to('service')}}#19"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02-4.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02-4.png')}}"></a></li>
                                <li class="five"><a class="fsc-menu-link5" id="insurance" href="{{URL::to('service')}}#20"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-02-5.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-02-5.png')}}"></a></li>
                            </ul>
                        </li>
                        <li><a href="{{URL::to('employment')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-03.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-03.png')}}"></a></li>
                        <li><a href="{{URL::to('forms')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-04.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-04.png')}}"></a></li>
                        <li><a href="{{URL::to('tools')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-05.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-05.png')}}"></a></li>
                        <li><a href="{{URL::to('links')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-06.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-06.png')}}"></a></li>
                        <li><a href="{{URL::to('contacts')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-07.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-07.png')}}"></a></li>
                        <li><a href="{{URL::to('submissions')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-08.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-08.png')}}"></a></li>

                        <li><a href="{{URL::to('register-user')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-09.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-09.png')}}"></a></li>
                        <li><a href="{{route('login')}}" class="fsc-menu-link"><img class="img-normal img-responsive" src="{{URL::asset('public/frontcss/images/nav-10.png')}}"><img class="img-hover img-responsive" src="{{URL::asset('public/frontcss/images/nav-hover-10.png')}}"></a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>