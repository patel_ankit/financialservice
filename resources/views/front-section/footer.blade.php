</div>
</div>

<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 fsc-pillar-div">
    <img class="img-responsive" style="height:100%;" src="{{URL::asset('public/frontcss/images/pillar.png')}}"/>

</div>
</div>
</div>

<div class="container-fluid" style='padding:0px'>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer fsc-well-footer">
        <div class='col-xs-12 col-sm-12 col-md-2 col-lg-4' style='text-align:left'>
            <div class="logo-l-section hidden-xs">
                <a href="https://www.vimbo.com" target="_blank"><img src="{{URL::asset('public/frontcss/images/footer-logo-left.png')}}" alt="" style="vertical-align:middle; width:100%; margin-left:0;
    margin-top: 7px; "></a>
            </div>

            <div class="social-media">
                <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>


        </div>
        <div class='col-xs-12 col-sm-9 col-md-7 col-lg-6'>

            <div style="padding-top:5px; font-weight:bold; margin-top:0;">
                <!--<h4 class="fsc-home-footer-text" style='margin-top:7px;'>Financial Service Center, Inc. 1997-2018. All Rights Reserved</h4>-->
                <footer><p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <a href="#"><span class="first-letter">F</span>inancial
                                <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</a></strong> &nbsp; All rights reserved. </p></footer>
            </div>
        </div>
        <div class='col-xs-12 col-sm-3 col-md-3 col-lg-2' style='text-align:right'>
            <a href="https://www.vimbo.com" target="_blank"><img src="{{ URL::asset('public/frontcss/images/footer-logo-right.png')}}" alt="" style="vertical-align:middle; width:100%; margin-left:0; margin-top:7px !important;
    margin-top: 0px;"></a>
        </div>
    </div>
</div>
<script type="text/javascript">
    function hideShowImage(id) {

        if ($("#collapse" + id).css('display').toLowerCase() == 'none') {
            $("#collapse" + id).show();
            $("#expand" + id).hide();
        } else {
            $("#collapse" + id).hide();
            $("#expand" + id).show();
        }

    }
</script>
<style>
    footer p {
        text-align: center;
        margin-top: 8px;
        color: #003e66;
        font-size: 17px;
        font-weight: 500;
    }

    footer p strong {
        font-weight: 500
    }

    footer p a {
        color: rgb(88, 88, 88);
        font-size: 17px;
        background: rgb(255, 255, 255);
        padding: 5px 11px;
        border-radius: 20px;
        border: 1px solid;
    }

    footer p a:hover {
        background: #fff;
        font-size: 17px;
        color: #585858;
        padding: 5px 11px;
        font-size: 17px;
        border-radius: 20px;
        border: 1px solid;
    }

    .social-media {
        float: right;
        margin: 0 30px;
    }
</style>

<script>
    $(document).ready(function () {
        $(document).on('change', '#countryId', function () {
            //console.log('htm');
            var id = $(this).val();//alert(id);
            $.get('{!!URL::to('/getstateList')!!}?id=' + id, function (data) {
                $('#stateId').empty();
                $.each(data, function (index, subcatobj) {
                    $('#stateId').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');

                })

            });

        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".textonly").keypress(function (event) {
            var inputValue = event.charCode;
            if (!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0)) {
                event.preventDefault();
            }
        });
    });
    $(document).ready(function () {
        //called when key is pressed in textbox
        $(".zip").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });

</script>
<script>
    (function (n, i, _n, j, a) {
        n.addEventListener("DOMContentLoaded", function () {
            i['liveninja'] = ["https://messenger.net2phone.com/w/5bb77787fcc16b75b84fe4bc?frame=true"];
            var _n = n.createElement('script');
            var j = n.getElementsByTagName('script')[0];
            _n.async = 1;
            _n.src = 'https://messenger.net2phone.com/w/assets/widget.js';
            j.parentNode.insertBefore(_n, j);
        })
    })(document, window);
</script>

</body>
</html>