@extends('fscemployee.layouts.app')
@section('title', 'Dashboard')
@section('main-content')
    <style>
        .gif {
            width: 50px;
            position: absolute;
            top: -14px;
            left: 7px;
        }

        .modal-body {
            width: 100%;
        }

        #mytask1 .panel.panel-default .panel-heading h4 {
            font-size: 16px !important;
            padding: 5px 5px 3px 5px !important;
            border: 2px solid #269ef3 !important;
            background: #ffffff;
            color: #005ed2;
        }

        .ac_name_first {
            width: 100% !important;
            padding: 8px 10px;
            font-weight: bold;
        }

        #mytask1 i.more-less {
            float: right;
            padding: 8px;
            color: #005ed2;
        }

        .countbox {
            background: none !important;
            border: 1px solid #ffffff !important;
            float: left !important;
            color: #ffffff !important;
        }

        .panel-title a.collapsed .glyphicon-minus {
            display: none;
        }

        .panel-title a .glyphicon-minus {
            display: block;
        }

        .panel-title a.collapsed .glyphicon-plus {
            display: block;
        }

        .panel-title a .glyphicon-plus {
            display: none;
        }

        .Branch {
            width: 100%;
            margin: 0px 0 20px 0;
            text-align: center;
            background: #428bca;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            padding: 5px;
        }

        .Branch h1 {
            padding: 0px;
            margin: 0px 0 0 0;
            color: #fff;
            font-size: 24px;
        }

        @charset "utf-8";
        .lunch-clock-time-head {
            width: 100%;
            float: left;
            background: #286DB5;
        }

        .btn.focus, .btn:focus, .btn:hover {
            color: #1a1a1a;
            text-decoration: none;
            transition: 0.3s;
        }

        .lunch-clock-time {
            width: 100%;
            float: left;
            background: #4a95e4;
            padding: 10px 10px;
            text-align: center;
        }

        .btn-danger {
            border: 1px solid #222;
        }

        .btn-success {
            border: 1px solid #222;
        }

        .input-group {
            position: relative;
            display: inline;
            border-collapse: separate;
        }

        .page-title {
            box-shadow: 0 1px 2px rgba(0, 0, 0, .1) !important;
        }

        .lunch-clock-time-head h2 {
            font-size: 18px;
            color: #fff;
            box-shadow: -1px 4px 8px rgba(0, 0, 0, 0.1);
            margin: 0;
            padding: 12px 0;
            text-align: center;
        }

        .lunch-clock-time-text {
            width: 100%;
            float: left;
        }

        .lunch-clock-schedule {
            width: 100%;
            float: left;
            background: #FFF;
            padding: 6px 10px;
            margin: 14px 0;
        }

        .lunch-clock-schedule h3 {
            font-size: 16px;
            color: #4b77be;
            line-height: 24px;
            margin: 5px 0 10px 0;
            text-align: center;
        }

        .lunch-clock-schedule-left {
            width: 50%;
            float: left;
        }

        .info-box {
            border-radius: 10px;
        }

        .lunch-clock-time p {
            font-size: 15px;
            line-height: 28px;
            text-align: center;
            margin: 4px 0;
        }

        .lunch-clock-schedule-right {
            width: 50%;
            float: left;
        }

        .lunch-clock-time-buttons {
            width: 100%;
            float: left;
        }

        .lunch-clock-time-buttons {
            width: 100%;
            float: left;
        }

        .lunch-clock-time-buttons button:disabled {
            display: none;
        }

        .lunch-clock-time-buttons button {
            width: 150px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button.lun-btn-out {
            background: #e61212;
        }

        .lunch-clock-time-buttons button.note-btn {
            background: #e4914a;
            margin: 10px 0;
        }

        .lunch-clock-time-buttons button {
            width: 150px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button {
            width: 132px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button.lun-btn-in {
            background: #0ba20b;
        }

        .icon-btn {
            width: 100%;
        }

        .week {
            display: none
        }

        .info-box-content {
            color: #fff !important;
        }

        #countryList {
            margin: 40px 0px 0px -100px;
            padding: 0px 0px;

            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 380px;
            z-index: 99;
        }

        #countryList li {
            border-bottom: 1px solid #025b90;
            background: #ef7c30;
        }

        #countryList li a {
            padding: 0px 10px 0px 0px !important;
            display: block;
            color: #fff !important;
            margin-left: 0px !important;
        }

        #countryList li:hover {
            background: #dc6d23;
        }

        .searchboxmain {
            float: right;
            display: FLEX;
            margin-top: -6px;
            justify-content: space-between;
        }

        .clear {
            clear: both;
        }

        #countryList li a {
            height: 40px;
        }

        #countryList li a span.bgcolors {
            background: linear-gradient(to bottom, #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;
            padding: 6px 0px;
            text-align: center;
            display: block;
            font-size: 20px;
            font-weight: bold;
            float: left;
            width: 60px;
        }

        #countryList li a span.clientalign {
            display: flex;
            width: 75px;
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 40px;
            background: #038ee0;
            margin-left: 0;
            padding-left: 5px;
        }

        #countryList li a span.entityname {
            display: flex;
            width: 200px;
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            font-size: 13px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 38px;
            margin-left: 10px;
        }

        .modal.fade.in {
            display: block !important;
            opacity: 1 !important;
            padding-top: 50px;
        }

        .form-control, .form-control-insu {
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;

        }

        #exampleModal .panel.panel-default .panel-heading h4 {
            font-size: 16px !important;
            padding: 5px 5px 3px 5px !important;
            border: 2px solid #269ef3 !important;
            background: #ffffff;
            color: #005ed2;
        }

        #exampleModal i.more-less {
            float: right;
            padding: 8px;
            color: #005ed2;
        }

        #exampleModal1 .panel.panel-default .panel-heading h4 {
            font-size: 16px !important;
            padding: 5px 5px 3px 5px !important;
            border: 2px solid #d07f00 !important;
            background: #ffffff;
            color: #d07f00;
        }

        #exampleModal1 i.more-less {
            float: right;
            padding: 8px;
            color: #d07f00;
        }

        #exampleModal2 .panel.panel-default .panel-heading h4 {
            font-size: 16px !important;
            padding: 5px 5px 3px 5px !important;
            border: 2px solid #dd4b39 !important;
            background: #ffffff;
            color: #dd4b39;
        }

        #exampleModal2 i.more-less {
            float: right;
            padding: 8px;
            color: #dd4b39;
        }

        .ac_name_first {
            width: 100% !important;
            padding: 8px 10px;
            color: #000000;
        }

        .panel-title a.collapsed .glyphicon-plus {
            display: block;
        }

        .count_acc {
            float: left;
            padding: 8px;
            background: #fff;
            color: #269ef3;
            border-radius: 5px;
            width: 90px;
            text-align: center;
            font-size: 16px;
        }

        .modal small {
            color: #ffffff;
        }

        .panel-title a .glyphicon-plus {
            display: none;
        }

        .panel-title a .glyphicon-minus {
            display: block;
        }

        .ac_name_first {
            width: 100% !important;
            padding: 8px 10px;
            font-weight: bold;
        }

        .panel-title a.collapsed .glyphicon-minus {
            display: none;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                width: 900px;
                margin: 30px auto;
            }
        }

        table tr td a {
            color: rgb(35, 82, 124);
            text-decoration: none;
        }

        .table tr td.text-center {
            text-align: center !important;
        }

        .modal-body ul {
            margin: 0px;
            padding: 0px;
        }

        .modal-body ul li {
            text-align: left !important;
            list-style-type: none !important;
            font-size: 12px !important;
        }

        .modal.fade.in {
            padding-top: 10px !important;
        }

        #myModalEmloyeeAll .table tr td {
            font-size: 12px !important;
        }

        .box3 h3 {
            font-size: 18px;
        }

        .pager.wizard {
            position: absolute;
            bottom: 10px;
            right: 15px;
        }

        .pager.wizard li.next a {
            background: #0183ab !important;
            border-color: #0183ab !important;
            border-radius: 4px;
            font-size: 15px;

        }

        .logoprintbox {
            display: none;
        }

        .skin-blue, .skin-blue .sidebar-menu > li.active > a, .skin-blue .sidebar-menu > li.menu-open > a {
            background: none !important;
        }

        .whitefont p, .whitefont .table tr td, .whitefont ul li {
            color: #fff !important;
        }

        .modal-header {
            background: #96cbea;
            color: #000;
            border-bottom: 2px solid #062492 !important;
        }

        .modal-header .modal-title {
            text-align: center !important;
        }

        .content-header .page-title a {
            border: 2px solid #2fa6f2 !important;
        }

        .sidebar-menu > li {
            border-bottom: 1px solid #e7e7e7;
            background: #269ef3;
        }

        .badgebox {
            background: #ff0000;
            width: 20px;
            height: 20px;
            display: block;
            position: absolute;
            right: -8px;
            top: -6px;
            border-radius: 20px;
            color: #fff;
            font-size: 12px;
        }

        .bloinkcss {
            animation: blinker 1s linear infinite;
        }

        .content-header.page-title a {
            /*border: 2px solid #2fa6f2!important;*/
        }

        h1.dash {
            float: left;
            margin-top: 7px;
            width: 100% !important;
            text-align: center;
            border-top: 1px solid;
            padding-top: 7px;
        }

        @keyframes blinker {
            0% {
                opacity: 1;
            }
            20% {
                opacity: 0.5;
            }
            50% {
                opacity: 0;
            }
        }

        }
    </style>

    <div class="content-wrapper">
        <div class="" style="margin-bottom:13px;">
            <section class="content-header page-title dash_title" style="">
                <a href="#" data-toggle="modal" data-target="#myModalcustomershseet" style="float:left; margin-top:-6px; cursor:pointer;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btn_conversationsheet.png"/>
                </a>
                <a href="#" data-toggle="modal" data-target="#myModalnotes" style="float:left; margin-top:-6px; cursor:pointer; margin-left:10px;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btnnotes.png"/></a>
                <a href="#" data-toggle="modal" data-target="#mytask1" style="float:left; margin-top:-6px; cursor:pointer; margin-left:10px;position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btntask.png"><?php $counttask = count($taskall); if($counttask != '0'){?><span class="badgebox"><span class="bloinkcss"><?php echo $counttask;?></span></span><?php } ?></a>
                <!--<a href="#" class="btn" style=" float:left; line-height: 11px; margin-top: -9px;padding-top: 2px; padding-bottom: 2px; font-size: 11px; color: #000;cursor:pointer;-->
                <!--background: #fff; margin-left:7px; position:relative; border-color: #50bc3c;"><b>H</b>ow<br><b>T</b>o<br><b>D</b>o</a>-->
                <a href="#" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;border-color: #50bc3c;">
                    <img src="https://financialservicecenter.net/public/images/howtodo.png"/> </a>
                <a href="#" data-toggle="modal" data-target="#modalComplaint" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btncomp.jpg"> </a>
                <a href="{{url('fscemployee/appointment')}}" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;border-color: #50bc3c;">
                    <img src="https://financialservicecenter.net/public/images/btn_appoint.png"> </a>
                <a href="" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;border-color: #50bc3c;">
                    <img src="https://financialservicecenter.net/public/images/btn_work.png"/> </a>

                <a href="{{url('fscemployee/eemail')}}" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;border-color: #50bc3c;">
                    <img src="https://financialservicecenter.net/public/images/extension.png"/> </a>

                <div class="searchboxmain">
                    <input type="text" name="search" id="country_name" class="form-control" placeholder="Search">
                    <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important;padding: 7px 18px;margin-left: 7px;margin-top: 0px;"
                       href="https://financialservicecenter.net/fscemployee/">Reset</a>
                    {{ csrf_field() }}
                    <ul id="countryList"></ul>
                </div>
                <h1 class="dash">Dashboard</h1>
            </section>

            @if(empty($timer->week_code))
                <?php
                $date = date('Y-m-d');
                $weekendDay = false;
                $day = date("D", strtotime($date));
                if ($day == 'Sat' || $day == 'Sun') {
                    $weekendDay = true;
                }
                if($weekendDay){?>
                <style>
                    .timing {
                        pointer-events: none;
                    }

                    .week {
                        display: block !important;
                        background: #103B68;
                        color: #fff;
                        padding: 10px 0;
                        font-size: 13px;
                        font-weight: bold;
                    }
                </style>
                <?php } else {
                    // echo $date . ' falls on a weekday.';
                }
                ?>
            @endif
        </div>
        <section class="content">
            <div class="boxes">
                <div class="">
                    <div class="row main-box-bg">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">
                                    <!--<div class="gif">-->
                                    <!--<img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">-->
                                    <!--</div>-->
                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">C</span>lient</h3>
                                    <img src="https://financialservicecenter.net/public/images/1.png" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/clients')}}" class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box2" style="margin-bottom: 2px;">
                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">W</span>ork</h3>
                                    <img src="https://financialservicecenter.net/public/images/desk_2.png" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/worknew')}}">
                                        <button type="button" class="btn btn-info information-btn2">More Info</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box3" style="margin-bottom: 2px;">

                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">P</span>rofile</h3>
                                    <img src="https://financialservicecenter.net/public/images/desk_man.png" style="width:60px;" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/employeeprofile')}}?tab=0" class="btn btn-info information-btn3">More info</a>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box1 aaa" style="margin-bottom: 2px;">

                                    <h2 class="pull-left">&nbsp;</h2>
                                    <h3 class="pull-left"><span class="big-font">C</span>lient's</h3>
                                    <div class="sidebar2" style="width:60% !important">
                                        <h3 class="pull-left"><span class="big-font">E</span>mployee</h3>
                                        <!--<h3 class="no-m-l"><span class="big-font">E</span>mployee</h3>-->
                                        <!--<img src="https://financialservicecenter.net/public/images/1.png" alt="img" class="images-img">-->
                                        <img src="https://financialservicecenter.net/public/images/desk_audience.png" style="width:90px;" alt="img" class="images-img">
                                    </div>
                                    <a href="{{url('fscemployee/employees')}}" class="btn btn-info information-btn3">More info</a>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">
                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">W</span>ork To Do</h3>
                                    <img src="https://financialservicecenter.net/public/images/Work to Do-04.png" style="width:77px;" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/fschowtodo')}}" class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">

                                    <h2>{{$msg2}}</h2>
                                    <h3><span class="big-font">M</span>essage</h3>


                                    <img src="https://financialservicecenter.net/public/images/mail.png" style="width:90px;" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/sendmessage')}}" class="btn btn-info information-btn">More info</a>

                                    <style>.ms {
                                            display: none !important
                                        }</style>
                                </div>
                            </div>
                        </div>

                    <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

				    @if($tknew>0)
                        <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>
                    @else
                    @endif
                            <div class="main-box">
                        <div class="box3" style="border-top: none;margin-bottom: 2px;">
                            <h2>&nbsp;</h2>
        					<h3><span class="big-font">T</span>ask</h3>
        					<img src="https://financialservicecenter.net/public/images/icon_tasklist.png" style="width:73px;" alt="img" class="images-img">
        					<a href="{{url('fscemployee/tasks')}}" class="btn btn-info information-btn">More info</a>
        				</div>
                    </div>
			    </div>-->


                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">C</span>ontact Us</h3>
                                    <img src="https://financialservicecenter.net/public/images/desk_telephone.png" style="width:90px;" alt="img" class="images-img">
                                    <a href="#" class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                    <h2>&nbsp;</h2>
                                    <h3><span class="big-font">S</span>ubmission</h3>
                                    <img src="https://financialservicecenter.net/public/images/submit(1).png" style="width:90px;" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/submissionrequest')}}" class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            @if($rulescount>0)
                                <div class="gif">
                                    <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                                </div>
                            @else
                            @endif

                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                    <h2><?php echo $rulescount;?></h2>
                                    <h3><span class="big-font">R</span>esponsibility</h3>
                                    <img src="https://financialservicecenter.net/public/images/desk_telephone.png" style="width:90px;" alt="img" class="images-img">
                                    <a href="{{url('fscemployee/employeeprofile')}}?tab=1" class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>

                    <div class="main-box">
                        <div class="box2" style="margin-bottom: 2px;">
                            <a data-toggle="modal" data-target="#exampleModal" class="btn btn-info information-btn3" style="background-color: #00c0ef !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">R</span>eminder</h3></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>
                    <div class="main-box">
                        <div class="box3" style="margin-bottom: 2px;background:#fa3e3e">
                            <a data-toggle="modal" data-target="#exampleModal1" class="btn btn-info information-btn3" style="background-color: #f39c12 !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">N</span>otification</h3></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="gif">
                        <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                    </div>
                    <div class="main-box">
                        <div class="box" style="border-top: none;margin-bottom: 2px;background-color: #f39c12 !important;">
                            <a data-toggle="modal" data-target="#exampleModal2" class="btn btn-info information-btn" style="background-color: #dd4b39 !important;padding: 0 20px; width:100%"><h4 style="font-size: 24px;line-height: 24px;float: left;">1</h4>
                                <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span class="big-font" style="font-size: 24px;">W</span>arnings</h3></a>
                        </div>
                    </div>
                </div>

            </div>


            <div id="accordion" class="accordion">
                <div class="card-header accordion__question">
                    <a class="card-title">
                        FSC Employee Schedule
                    </a>
                </div>
                <div class="collapse accordion__answer" style="display: none; background:#ffffff; padding:15px;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="sampleTableNorcross, GA">
                                <thead>
                                <tr>
                                    <th style="width:150px;">Employee ID</th>
                                    <th style="text-align:left!important;">Employee Name</th>
                                    <th style="width:15%">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td><?php echo $employee->employee_id;?></td>
                                    <td><?php echo $employee->firstName . ' ' . $employee->middleName . ' ' . $employee->lastName;?></td>
                                    <td style="text-align:center"><a href="#myModalEmloyeeAll" data-toggle="modal" data-target="#myModalEmloyeeAll">View</a>
                                        <!-- Modal -->
                                        <div id="myModalEmloyeeAll" class="modal fade" role="dialog">
                                            <div class="modal-dialog" style="width:1120px;">
                                            <?PHP //echo "<pre>";print_r($employee);?>
                                            <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive"
                                                                                     style="width:70px; margin:0px auto;"/>
                                                            <!-- <button onclick="printdivsAll();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print</button>-->
                                                            <button onclick="printDiv('printMe')" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print
                                                            </button>


                                                            <!--<button onclick="print2();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">
    Print
  </button>!-->

                                                        </h4>
                                                    </div>
                                                    <div class="modal-body" style="height:400px; overflow:auto">

                                                        <div id='printMe'>
                                                            <style>
                                                                @media print {
                                                                    .logoprintbox {
                                                                        display: block;
                                                                    }
                                                                }
                                                            </style>

                                                            <div class="logoprintbox">
                                                                <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive"
                                                                     style="width:70px; margin:0px auto;"/>
                                                            </div>
                                                            <h5 style="font-size:18px; text-align:center"><strong> Schedule Date : <?php echo $schedulesetup->sch_start_date;?> To <?php echo $schedulesetup->sch_end_date;?> </strong></h5>
                                                            <?php //echo "<pre>";print_r($scheduleemployee);?>

                                                            <table class="table table-bordered table-striped">
                                                                <thead>
                                                                <?php
                                                                $stdate = strtotime($schedulesetup->sch_start_date);
                                                                $eddate = strtotime($schedulesetup->sch_end_date);

                                                                ?>
                                                                <tr>
                                                                    <th>Date</th>
                                                                    <th>>>>></th>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <th><?php echo date('m/d', $i);?></th>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td style="width:50px;"><b>Emp.#</b></td>
                                                                    <td><b>Emp.Name</b></td>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <td style="width:76px; " class="text-center"><b><?php echo date('D', $i);?></b></td>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <?php
                                                                foreach($scheduleemployee as $schemployee)
                                                                {?>
                                                                <tr>
                                                                    <td><?php echo $schemployee->employee_id;?></td>
                                                                    <td><?php echo $schemployee->firstName . ' ' . $schemployee->middleName . ' ' . $schemployee->lastName;?></td>
                                                                    <?php



                                                                    foreach($scheduledates as $schdates)
                                                                    {
                                                                    if($schemployee->employee_id == $schdates->employee_id)
                                                                    {
                                                                    ?>
                                                                    <td><?php echo $schdates->clockin;?> - <?php echo $schdates->clockout;?> </td>

                                                                    <?php
                                                                    }

                                                                    }
                                                                    /*  for ( $i = $m; $i <= $f; $i = $i + 86400 )
                                    {
                                    ?>
                                    <td><?php echo $sch->schedule_in_time;?> To <?php echo $sch->schedule_out_time;?> </td>
                                    <?php
                                    }*/
                                                                    ?>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <?php
                                                                    foreach($scheduledates as $schdates)
                                                                    {
                                                                    if($schemployee->employee_id == $schdates->employee_id)
                                                                    {
                                                                    ?>
                                                                    <td><?php echo $schdates->clockin;?> - <?php echo $schdates->clockout;?> </td>

                                                                    <?php
                                                                    }

                                                                    }
                                                                    ?>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>

                                                                </tr>
                                                                <?php
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>


                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btnpopclose" data-dismiss="modal" onclick='closemodalpopup()'>Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div id="collapseOne03" class="panel-collapse collapse" aria-expanded="true" style="">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTableNorcross, GA">
                            <thead>
                            <tr>
                                <th style="width:150px;">Employee ID</th>
                                <th style="text-align:center!important;">Employee Name</th>
                                <th style="text-align:center!important;">Period</th>

                                <th style="width:15%">Action</th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($scheduleemployee as $sch)
                            {?>
                            <?PHP
                            //  echo "<pre>"; print_r($scheduledates);
                            $monday = date('Y-m-d', strtotime('Monday this week'));
                            $sunday = date('Y-m-d', strtotime('Sunday this week'));
                            $m = strtotime($monday);
                            $f = strtotime($sunday);

                            ?>
                            <tr>
                                <td><?php echo $sch->employee_id;?></td>
                                <td><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?></td>
                                <td> <?php echo date('M-d Y', strtotime($monday)) . ' To ' . date('M-d Y', strtotime($sunday));?></td>
                                <td style="text-align:center"><a href="#myModal_<?php echo $sch->employee_id;?>" data-toggle="modal">View</a>
                                    <div id="printThis_<?php echo $sch->employee_id;?>">
                                    <!-- <div id="printElements_<?php echo $sch->employee_id;?>">!-->

                                        <div id="myModal_<?php echo $sch->employee_id;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content modal-lg">
                                                    <div class="modal-header" style="">
                                                        <h4 class="modal-title" style="position: relative; width: 98%; float: left;">
                                                            <?php //echo 'AA'.$sch->employee_id;?>
                                                            <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive" style="width:70px; margin:0px auto;">

                                                            <button onclick="printDiv('<?php echo $sch->employee_id;?>')" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print
                                                            </button>

                                                        <!-- <button onclick="printElement('<?php echo $sch->employee_id;?>');" id="printbtn" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">
    Print
  </button>!-->


                                                        </h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>

                                                    <div class="modal-body">
                                                    <!--<div class="row">
                                  <div class="col-md-5 text-left">
                                     <strong>EE ID : </strong> <?php echo $sch->employee_id;?>
                                                            </div>
                                  <div class="col-md-6 text-left">
                                      <strong>Name:</strong><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?><br/>
                                      <strong>Schedule  Status: </strong> <br/>
                                      <strong>Period : </strong>
                                  </div>
                              </div>!-->
                                                        <style>
                                                            .printlogo {
                                                                display: none !important;
                                                            }
                                                        </style>
                                                        <div id="<?php echo $sch->employee_id;?>">
                                                            <style>
                                                                @media print {
                                                                    .printlogo {
                                                                        display: block !important;
                                                                        padding-bottom: 10px;
                                                                        margin-bottom: 10px;
                                                                        border-bottom: 1px solid #ccc;
                                                                    }
                                                                }
                                                            </style>

                                                            <div class="printlogo">
                                                                <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive" style="width:70px; display:block; margin:0px auto;">
                                                            </div>
                                                            <table class="table myclass" border="0">
                                                                <thead>

                                                                <tr>
                                                                    <th colspan="4" class="border-none" style="border: 0 !important; background:none!important;">
                                                                        <h3 style="margin-top:0px !important;"><b>
                                                                                Schedule</b></h3>
                                                                    </th>
                                                                </tr>

                                                                <tr>

                                                                </tr>

                                                                <tr>
                                                                    <th class="border-none" style="border: 0 !important;background:none!important;float:left;">
                                                                        <b>Employee ID:</b>
                                                                        <span style="padding:10px ;border: 1px solid;background:none !important;color:#000 !important;"><?php echo $sch->employee_id;?></span>
                                                                    </th>
                                                                    <th colspan="2" class="border-none" style="border: 0 !important; background:none!important;text-align:right !important;vertical-align: middle;">
                                                                        Period:
                                                                        <?php echo date('M-d Y', strtotime($monday));?>

                                                                        To
                                                                        <?php echo date('M-d Y', strtotime($sunday));?>
                                                                    </th>
                                                                    <th class="border-none" style="border: 0 !important;background:none!important;">
                                                                        <b>Employee Name :</b>
                                                                        <span style="padding:10px ;border: 1px solid;background:none !important;color:#000 !important;"><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?></span>
                                                                    </th>
                                                                </tr>


                                                                </thead>
                                                            </table>


                                                            <table class="table table-bordered table-striped">
                                                                <thead>

                                                                <tr>
                                                                    <?php
                                                                    for ( $i = $m; $i <= $f; $i = $i + 86400 )
                                                                    {
                                                                    // echo "<pre>";print_r($sch);
                                                                    ?>
                                                                    <th><?php echo date('M-d-Y', $i);?> <br/> <?php echo date('l', $i);?></th>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <?php

                                                                    foreach($scheduledates as $schdates)
                                                                    {
                                                                    if($sch->employee_id == $schdates->employee_id)
                                                                    {

                                                                    ?>
                                                                    <td style="text-align:center !important;"><?php echo $schdates->clockin;?> - <br><span style="margin-left:-6% !important;"><?php echo $schdates->clockout;?></span></td>
                                                                    <?php
                                                                    }

                                                                    }
                                                                    /*
                                    for ( $i = $m; $i <= $f; $i = $i + 86400 )
                                    {
                                    ?>
                                    <td><?php echo $sch->schedule_in_time;?> To <?php echo $sch->schedule_out_time;?> </td>
                                    <?php
                                    }*/
                                                                    ?>
                                                                    <td style="width:0px !important;">&nbsp;</td>
                                                                    <td style="width:0px !important;">&nbsp;</td>

                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btnpopclose" data-dismiss="modal" onclick='closemodalpopup()'>Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </td>

                            </tr>

                            <?php
                            }
                            ?>
                            <tr>

                                <td>FSC</td>
                                <td>All</td>
                                <td><?php echo date('M-d Y', strtotime($schedulesetup->sch_start_date));?> to <?php echo date('M-d Y', strtotime($schedulesetup->sch_end_date));?></td>

                                <td style="text-align:center"><a href="#myModalEmloyeeAll" data-toggle="modal" data-target="#myModalEmloyeeAll">View</a>
                                    <!-- Modal -->
                                    <div>
                                        <div id="myModalEmloyeeAll" class="modal fade" role="dialog">
                                            <div class="modal-dialog" style="width:1120px;">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive"
                                                                                     style="width:70px; margin:0px auto;"/>
                                                            <!-- <button onclick="printdivsAll();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print</button>-->
                                                            <button onclick="printDiv('printMe')" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print
                                                            </button>


                                                            <!--<button onclick="print2();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">
    Print
  </button>!-->

                                                        </h4>
                                                    </div>
                                                    <div class="modal-body" style="height:400px; overflow:auto">

                                                        <div id='printMe'>
                                                            <style>
                                                                @media print {
                                                                    .logoprintbox {
                                                                        display: block;
                                                                    }
                                                                }
                                                            </style>

                                                            <div class="logoprintbox">
                                                                <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive"
                                                                     style="width:70px; margin:0px auto;"/>
                                                            </div>
                                                            <h5 style="font-size:18px; text-align:center"><strong> Schedule Date : <?php echo $schedulesetup->sch_start_date;?> To <?php echo $schedulesetup->sch_end_date;?> </strong></h5>
                                                            <?php //echo "<pre>";print_r($scheduleemployee);?>

                                                            <table class="table table-bordered table-striped">
                                                                <thead>
                                                                <?php
                                                                $stdate = strtotime($schedulesetup->sch_start_date);
                                                                $eddate = strtotime($schedulesetup->sch_end_date);

                                                                ?>
                                                                <tr>
                                                                    <th>Date</th>
                                                                    <th>>>>></th>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <th><?php echo date('m/d', $i);?></th>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td style="width:50px;"><b>Emp.#</b></td>
                                                                    <td><b>Emp.Name</b></td>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <td style="width:76px; " class="text-center"><b><?php echo date('D', $i);?></b></td>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <?php
                                                                foreach($scheduleemployee as $schemployee)
                                                                {?>
                                                                <tr>
                                                                    <td><?php echo $schemployee->employee_id;?></td>
                                                                    <td><?php echo $schemployee->firstName . ' ' . $schemployee->middleName . ' ' . $schemployee->lastName;?></td>
                                                                    <?php



                                                                    foreach($scheduledates as $schdates)
                                                                    {
                                                                    if($schemployee->employee_id == $schdates->employee_id)
                                                                    {
                                                                    ?>
                                                                    <td><?php echo $schdates->clockin;?> - <?php echo $schdates->clockout;?> </td>

                                                                    <?php
                                                                    }

                                                                    }
                                                                    /*  for ( $i = $m; $i <= $f; $i = $i + 86400 )
                                    {
                                    ?>
                                    <td><?php echo $sch->schedule_in_time;?> To <?php echo $sch->schedule_out_time;?> </td>
                                    <?php
                                    }*/
                                                                    ?>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <?php
                                                                    foreach($scheduledates as $schdates)
                                                                    {
                                                                    if($schemployee->employee_id == $schdates->employee_id)
                                                                    {
                                                                    ?>
                                                                    <td><?php echo $schdates->clockin;?> - <?php echo $schdates->clockout;?> </td>

                                                                    <?php
                                                                    }

                                                                    }
                                                                    ?>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>

                                                                </tr>
                                                                <?php
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>

                                                            <table class="table table-bordered table-striped">
                                                                <thead>

                                                                <tr>
                                                                    <th>&nbsp;</th>
                                                                    <th> Date >>>></th>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <th><?php echo date('m/d', $i);?></th>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td style="width:50px;"><b>Emp.#</b></td>
                                                                    <td style="width:280px;"><b>Emp.Name</b></td>
                                                                    <?php
                                                                    for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                    {
                                                                    ?>
                                                                    <td style="width:76px; " class="text-center"><b><?php echo date('D', $i);?></b></td>
                                                                    <?PHP
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <?php
                                                                foreach($scheduleemployee as $schemployee)
                                                                {?>
                                                                <tr>
                                                                    <td><?php echo $schemployee->employee_id;?></td>
                                                                    <td><?php echo $schemployee->firstName . ' ' . $schemployee->middleName . ' ' . $schemployee->lastName;?></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>

                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>In</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Break In</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Break Out</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Out</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>


                                                                <?php
                                                                }
                                                                ?>


                                                                </tbody>
                                                            </table>

                                                            <p style="text-align:left"><strong>Note:</strong> Employee Responsibility:</p>
                                                            <ul>
                                                                <li>
                                                                    1. Record clock in and out immediately when they come in/out
                                                                </li>
                                                                <li> 2. All the employee make sure that time recorded paid for that in their payroll
                                                                    If not please notify immediately to payroll department.
                                                                </li>
                                                                <li> 3. Leave request get approved before 7 days in advance</li>
                                                                <li> 4. All the employee must take lunch brake as per company rules</li>
                                                                <li> 5. If you have any question contact office manager immediately</li>
                                                                <li> 6. Office Lunch Hrs. 1.30 p.m. to 2.00 p.m.</li>
                                                                <li> 7. Work as per schdule time - After schdule hrs will not be consider</li>
                                                                <li> 8. Overtime will not be granted without prior approval or sign by supervisor</li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btnpopclose" data-dismiss="modal" onclick='closemodalpopup()'>Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>


            </div>

            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="row" style="display:none">

                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#00c1ec !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>
                                <div class="info-box-content" style="margin-top:6% !important;"><a href="{{url('fscemployee/newtask')}}" style="font-size:20px !important;">Task(s):</a> <span class="" style="font-size:20px;"><!--class:info-box-number-->
                        @if(empty($tk->checked))
                                        @else
                                            @if($tk->checked=='1')
                                                <div class="gif">
                            <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
                            </div>
                                            @else
                                            @endif
                                        @endif
                                        @if(empty($tk->emptotal))
                                            0
                                        @else
                                            <a href="{{url('fscemployee/newtask')}}"> {{$tk->emptotal}}</a>
                                        @endif

                              <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
                                <div class="info-box-content" style="margin-top:6% !important;"><a href="{{url('fscemployee/sendmessage')}}" style="font-size:20px;"> Message(s):</a> <span class="" style="font-size:20px;">

                            <!--@foreach($msg as $ss)-->
                                @if(empty($ss->recieve))
                                @else
                                    @if($ss->recieve=='1')
                                        <div class="gif">
                                            <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
                                        </div>
                                    @else
                                    @endif
                                @endif
                                @if(empty($ss->msgtotal))
                                    0
                                @else
                                    <a href="{{url('fscemployee/sendmessage')}}"> {{$ss->msgtotal}}</a>
                                    <!--<style>.dd{display:none}</style>-->
                            @endif
                            <!--@endforeach-->
                                <a href="{{url('fscemployee/sendmessage')}}" class="dd"> 0</a>
                                <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>


                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#71af8a !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-file-o"></i></span>
                                <div class="info-box-content" style="margin-top:6% !important;"><a href="{{url('')}}" style="font-size:20px;">Responsiblity / Rules:</a> <span class="" style="font-size:20px;">

                                @if(empty($rules))
                                        @else
                                            @if($rules)
                                                <div class="gif">
                                <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
                                </div>
                                            @else
                                            @endif
                                        @endif
                                        @if(empty($rules))
                                            <a>0</a>
                                        @else
                                            <a href="{{url('fscemployee/employeeprofile')}}?tab=1" style="color:#fff !important;"> {{$rules}}</a>
                                        @endif

                                    <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="row" style="display:none">

                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#71af8a !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>
                                <div class="info-box-content" style="font-size:20px;margin-top:6% !important;"> Sample-1:<span class="" style="font-size:20px;">
                            <div class="gif">
                            <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                            </div>
                                <a href="https://financialservicecenter.net/fscemployee/newtask"> 1</a>
                                <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#00c1ec !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
                                <div class="info-box-content" style="font-size:20px;margin-top:6% !important;"> Sample-2:<span class="" style="font-size:20px;">
                            <div class="gif">
                                <img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
                            </div>
                                <a href="https://financialservicecenter.net/fscemployee/sendmessage"> 3</a>
                                <style>.dd {
                                        display: none
                                    }</style>
                                        <a href="https://financialservicecenter.net/fscemployee/sendmessage" class="dd"> 0</a>
                                        <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>


                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-file-o"></i></span>
                                <div class="info-box-content" style="font-size:20px;margin-top:6% !important;"> Sample-3:<span class="" style="font-size:20px;">

                                                  <a>0</a>

                            <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>

                </div>
                <!-- /.col -->
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-12 col-sm-6 col-xs-12 employee" style="text-align:right;">
                    <div class="col-md-6"></div>
                    <div class="timing col-md-6" style="padding-right:0px;">
                        <div class="col-md-12 lunch-clock-time" style="    border: 5px solid #fff;
    border-radius: 20px;">
                            <div class="lunch-clock-time-head">
                                <h2>Time Log <?php
                                    function time_to_decimal($time)
                                    {
                                        $timeArr = explode(':', $time);
                                        $decTime = (($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60)) / 60;
                                        return $decTime;
                                    }
                                    $sum = 0;
                                    foreach ($decimal1 as $del) {
                                        $total = date("H:i:s", strtotime($del->total));
                                        if ($del->breaktime == NULL) {
                                            $breaktime = '00:00:00';
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($del->breaktime));
                                        }

                                        if ($del->breaktime1 == NULL) {
                                            $breaktime1 = '00:00:00';
                                        } else {
                                            $breaktime1 = date("H:i:s", strtotime($del->breaktime1));
                                        }
                                        $all = time_to_decimal($total) - (time_to_decimal($breaktime) + time_to_decimal($breaktime1));
                                        $all;
                                        $sum += $all;
                                    }
                                    if (empty($emp_schedule->emp_out)) {
                                        $sum1 = $sum + $currenttime;
                                    } else {
                                        $sum1 = $sum;
                                    }?>
                                    <span style="float:right;padding-right:10px">Total Hours <?php echo round($sum1, 2);?></span></h2>
                            </div>
                            <div class="lunch-clock-time-text">
                                <div class="lunch-clock-schedule">
                                    <h3><span>Schedule Time :</span> @if(empty($empschedule->clockin))<span>00:00 PM To 00:00 PM</span>@else <span>{{$empschedule->clockin}} To {{$empschedule->clockout}}</span> @endif</h3>
                                    <div class="week">Today is Week day. If you Time start then Contact Your Administration</div>
                                    <div class="lunch-clock-schedule-left">
                                        <p><span>Time In : @if(empty($emp_schedule->emp_in)) 00:00 @else {{date("h:i A",strtotime($emp_schedule->emp_in))}} @endif</span></p>
                                        @if(empty($emp_schedule->launch_in)) @else
                                            <p><span>Breack In : {{date("h:i A",strtotime($emp_schedule->launch_in))}} </span></p>
                                        @endif
                                        @if(empty($emp_schedule->launch_in_second)) @else
                                            <p><span>Breack In Second: {{date("h:i A",strtotime($emp_schedule->launch_in_second))}} </span></p>
                                        @endif
                                    </div>
                                    <div class="lunch-clock-schedule-right">
                                        <p><span>Time Out : @if(empty($emp_schedule->emp_out)) 00:00 @else {{date("h:i A",strtotime($emp_schedule->emp_out))}} @endif</span></p>
                                        @if(empty($emp_schedule->launch_out)) @else
                                            <p><span>Break Out : {{date("h:i A",strtotime($emp_schedule->launch_out))}} </span></p>
                                        @endif
                                        @if(empty($emp_schedule->launch_out_second)) @else
                                            <p><span>Breack Out Second: {{date("h:i A",strtotime($emp_schedule->launch_out_second))}} </span></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <?php //echo "<pre>";print_r($timer);?>
                            <div class="lunch-clock-time-buttons">
                                <div class="lunch-clock-time-buttons">
                                    @if(empty($emp_schedule->emp_in))
                                        @if(empty($empschedule->clockin))
                                        @else
                                            <?php //echo date('h:i A');
                                            $newtime = strtotime(date('h:i A'));
                                            $starttime = strtotime($empschedule->clockin) - (15 * 60);
                                            $date = date('h:i A', $starttime);
                                            $st = strtotime($date);
                                            $starttime1 = strtotime($empschedule->clockin) + (15 * 60);
                                            $date1 = date('h:i A', $starttime1);
                                            $st1 = strtotime($date1);
                                            ?>
                                        @endif
                                        <div class="col-md-6">
                                            <form method="post" action="{{route('home.store')}}" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="emp_in_date" value="{{date('Y-m-d')}}">
                                                <input type="hidden" name="emp_in" value="{{date('Y-m-d H:i:m')}}">
                                                <input type="hidden" name="employee_id" value="{{Auth::user()->user_id}}">
                                                @if(empty($empschedule->clockin))
                                                    <a class="btn lun-btn-out btn-success icon-btn" href="#" data-toggle="modal" data-target="#myModal4">Start</a>
                                                @else
                                                    @if($st < $newtime)

                                                        @if($st1 < $newtime)
                                                            @if(! empty($timer->after_code))
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                            @else
                                                                <a class="btn lun-btn-out btn-success icon-btn addaftercheck" href="#" data-toggle="modal" data-target="#myModal2">Start</a>
                                                            @endif
                                                        @else
                                                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                        @endif

                                                    @else
                                                        @if(! empty($timer->after_code))
                                                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                        @else
                                                            <a class="btn lun-btn-out btn-success icon-btn addaftercheck" href="#" data-toggle="modal" data-target="#myModal2">Start</a>
                                                        @endif
                                                    @endif
                                                @endif
                                            </form>
                                        </div>
                                    @else
                                        @if(empty($emp_schedule->launch_in))
                                            <div class="col-md-6">
                                                <form method="post" action="{{route('home.update',$emp_schedule->id)}}" class="gg" enctype="multipart/form-data">
                                                    {{csrf_field()}}{{method_field('PATCH')}}
                                                    <input type="hidden" name="launch_in" value="{{date('Y-m-d H:i:s')}}">
                                                    <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH IN">
                                                </form>
                                            </div>
                                        @else
                                            @if($emp_schedule->launch_out)
                                                @if($emp_schedule->launch_in_second)

                                                    @if($emp_schedule->launch_out_second)

                                                    @else
                                                        <div class="col-md-6">
                                                            <form method="post" action="{{route('home.update',$emp_schedule->id)}}" class="" enctype="multipart/form-data">
                                                                {{csrf_field()}}{{method_field('PATCH')}}
                                                                <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                                <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                                                                <input type="hidden" name="launch_in_second" value="{{$emp_schedule->launch_in_second}}">
                                                                <input type="hidden" name="launch_out_second" value="{{date('Y-m-d H:i:s')}}">
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH OUT SECOND">
                                                            </form>
                                                        </div>
                                                    @endif
                                                @else
                                                    @if(empty($emp_schedule->emp_out))
                                                        <div class="col-md-6">
                                                            <form method="post" action="{{route('home.update',$emp_schedule->id)}}" class="" enctype="multipart/form-data">
                                                                {{csrf_field()}}{{method_field('PATCH')}}
                                                                <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                                <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                                                                <input type="hidden" name="launch_in_second" value="{{date('Y-m-d H:i:s')}}">
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH IN SECOND">
                                                            </form>
                                                        </div>
                                                    @endif @endif

                                            @else
                                                <div class="col-md-6">
                                                    <form method="post" action="{{route('home.update',$emp_schedule->id)}}" class="" enctype="multipart/form-data">
                                                        {{csrf_field()}}{{method_field('PATCH')}}
                                                        <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                        <input type="hidden" name="launch_out" value="{{date('Y-m-d H:i:s')}}">
                                                        <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH OUT">
                                                    </form>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                    @if(empty($emp_schedule->emp_out))
                                        <div class="col-md-6">
                                            @if(empty($emp_schedule->launch_in))
                                                @if(empty($emp_schedule->emp_in))
                                                    <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                @else
                                                    <?php $newtime = strtotime(date('h:i A'));
                                                    $starttime2 = strtotime($empschedule->clockout) - (15 * 60);
                                                    $date2 = date('h:i A', $starttime2);
                                                    $st2 = strtotime($date2);
                                                    $starttime3 = strtotime($empschedule->clockout) + (15 * 60);
                                                    $date3 = date('h:i A', $starttime3);
                                                    $st3 = strtotime($date3);
                                                    ?>
                                                    @if($st2 < $newtime)
                                                        @if($st3 < $newtime)
                                                            @if(! empty($timer->before_code))
                                                                <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                            @else
                                                                <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">Clock Out / Day Out</a>
                                                            @endif
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                        @endif
                                                    @else
                                                        @if(! empty($timer->before_code))
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">Clock Out / Day Out</a>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else
                                                @if($emp_schedule->launch_out)
                                                    <?php $newtime = strtotime(date('h:i A'));
                                                    $starttime2 = strtotime($empschedule->clockout) - (15 * 60);
                                                    $date2 = date('h:i A', $starttime2);
                                                    $st2 = strtotime($date2);
                                                    $starttime3 = strtotime($empschedule->clockout) + (15 * 60);
                                                    $date3 = date('h:i A', $starttime3);
                                                    $st3 = strtotime($date3);
                                                    ?>
                                                    @if($st2 < $newtime)
                                                        @if($st3 < $newtime)
                                                            @if(! empty($timer->before_code))
                                                                <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                            @else
                                                                <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">Clock Out / Day Out</a>
                                                            @endif
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                        @endif
                                                    @else
                                                        @if(! empty($timer->before_code))
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                        @else
                                                        <!--<a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">End</a>-->
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">Clock Out / Day Out</a>
                                                        @endif
                                                    @endif
                                                @else
                                                    <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal3">Clock Out / Day Out</a>
                                                @endif
                                            @endif
                                        </div>
                                    @else
                                        <style>
                                            .gg {
                                                display: none;
                                            }
                                        </style>
                                    @endif
                                    @if(empty($emp_schedule->launch_out))
                                        <div class="col-md-12" style="padding:15px;">
                                            @else
                                                <div class="col-md-6">
                                                    @endif
                                                    <a class="btn note-btn btn-primary" href="#" data-toggle="modal" style="width: 100%;border: 1px solid #222;background:#ffcc00;color:#222;" data-target="#myModal1">Note</a>
                                                </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.info-box -->
                    </div>


                    <!-- /.col -->
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- /.row -->
                <!-- Main row -->
                <!-- /.row -->
        </section>
    </div>
    @if(empty($emp_schedule->emp_in))
    @else
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Clock Out</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{route('home.update',$emp_schedule->id)}}" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                            <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                            <input type="hidden" name="emp_out" value="{{date('Y-m-d H:i:s')}}">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Employee Code :</label>
                                <input type="text" class="form-control" value="{{$employee->employee_id}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Work</label>
                                <textarea class="form-control" required name="work"></textarea>
                            </div>

                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="End">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Note</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="@if(empty($emp_schedule->emp_in)) {{route('home.store')}} @else {{route('home.update',$emp_schedule->id)}} @endif" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if(empty($emp_schedule->emp_in))
                            <input type="hidden" name="emp_in_date" value="{{date('m-d-Y')}}">
                            <input type="hidden" class="form-control" value="{{Auth::user()->user_id}}" name="employee_id">
                        @else
                            {{method_field('PATCH')}}
                            <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                            <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Employee Code :</label>
                            <input type="text" class="form-control" value="{{$employee->employee_id}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Note</label>
                            <textarea class="form-control" id="txtEditor" cols="55" name="note"></textarea>
                            <input type="hidden" class="form-control" value="note1" name="note1">
                        </div>
                        <input class="btn btn-success icon-btn" type="submit" name="submit" value="Note">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Start Timer</h4>
                </div>
                <form action="@if(empty($timer->id)){{route('home.store')}} @else {{route('home.update',$timer->id)}} @endif" method="post" id="superid">
                    <div class="modal-body">
                        <p id="sk" style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">You can clock in 15 minute before your schedule time. You are early .You can log in your schedule time Or You can contact Your Supervisor.</p>


                        <div class="col-md-12">
                            <div class="col-md-12">

                                {{ csrf_field() }} @if(empty($timer->id)) @else {{method_field('PATCH')}} @endif
                                <div class="form-group has-feedback">
                                    <label for="password" class="col-md-4 control-label">
                                        Supervisor Name:
                                    </label>
                                    <div class="col-md-8  inputGroupContainer">
                                        <div class="input-group">
                                            <select name="supervisorname" id="supervisorname" class="form-control fsc-input">
                                                <option value="">--- Select---</option>
                                                @foreach($emp22 as $ep)
                                                    <option value="{{$ep->id}}">{{$ep->firstName.' '.$ep->middleName.' '.$ep->lastName}}</option>
                                                @endforeach
                                            </select>

                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-4 control-label">
                                        Supervisor Code
                                    </label>
                                    <div class="col-md-8  inputGroupContainer">
                                        <div class="input-group">
                                            <input type="password" class="form-control aftercodes" required name="code" placeholder="Please Enter Your Code">
                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <br>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Supervisor" class="btn btn-primary">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalbefore" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Start Timer</h4>
                </div>
                <form action="@if(empty($timer->id)){{route('home.store')}} @else {{route('home.update',$timer->id)}} @endif" method="post" id="superid1">
                    <div class="modal-body">
                        <p id="skbefore" style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">You can clock in 15 minute before your schedule time. You are early .You can log in your schedule time Or You can contact Your Supervisor.</p>


                        <div class="col-md-12">
                            <div class="col-md-12">

                                {{ csrf_field() }} @if(empty($timer->id)) @else {{method_field('PATCH')}} @endif
                                <div class="form-group has-feedback">
                                    <label for="password" class="col-md-4 control-label">
                                        Supervisor Name:
                                    </label>
                                    <div class="col-md-8  inputGroupContainer">
                                        <div class="input-group">
                                            <select name="supervisorname" id="supervisorname" class="form-control fsc-input">
                                                <option value="">--- Select---</option>
                                                @foreach($emp22 as $ep)
                                                    <option value="{{$ep->id}}">{{$ep->firstName.' '.$ep->middleName.' '.$ep->lastName}}</option>
                                                @endforeach
                                            </select>

                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-4 control-label">
                                        Supervisor Code
                                    </label>
                                    <div class="col-md-8  inputGroupContainer">
                                        <div class="input-group">
                                            <input type="password" class="form-control" required name="code1" placeholder="Please Enter Your Code">
                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <br>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" id="before">Supervisor</a>
                        <input type="submit" value="Supervisor" class="btn btn-primary">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lunch Out</h4>
                </div>
                <div class="modal-body">
                    <p style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">Before end you must have to Lunch out</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal4" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lunch Out</h4>
                </div>
                <div class="modal-body">
                    <p style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">Please Schedule Set</p>
                </div>
            </div>
        </div>
    </div>


    <div id="newmyModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row form-group">
                            <label class="control-label col-md-3 text-right">ID:</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="entityids" readonly style="width:150px;"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-md-3 text-right">Name</label>
                            <div class="col-md-7">
                                <input class="form-control names hidename" type="text" id="names" readonly/>
                                <input class="form-control names1" type="text" id="names1" readonly/>

                            </div>
                        </div>

                        <div class="companyphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 1</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly style="width:150px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control teltype" type="text" id="" readonly style="width:100px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 2</label>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly style="width:150px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" id="teltype2" readonly style="width:100px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>
                        </div>
                        <div class="personalphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right"> Personal Telephone</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly style="width:150px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control teltype" type="text" id="" readonly style="width:100px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Telephone</label>
                                <div class="col-md-4">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly style="width:150px;"/>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2Type" type="text" id="" readonly style="width:100px;"/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="ext" readonly/>
                                </div>


                            </div>
                        </div>

                        <div class="row form-group companyemail" style="display:none;">
                            <label class="control-label col-md-3 text-right">Email</label>
                            <div class="col-md-7">
                                <input class="form-control emails" type="text" id="" readonly/>
                            </div>
                        </div>
                        <div class="personalemail" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Personal Email</label>
                                <div class="col-md-7">
                                    <input class="form-control emails" type="text" id="" readonly/>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Email</label>
                                <div class="col-md-7">
                                    <input class="form-control fscemail" type="text" id="" readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12 ">


                            </div>


                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!----------------------------->

    <script>

        function myfun(id, type) {
            var _token = $('input[name="_token"]').val();

            $.ajax({
                url: "{{ route('employee.fetch111') }}",
                method: "POST",
                data: {'id': id, 'type': type, _token: _token},

                success: function (data) {
                    //JSON.parse(data);
                    console.log(data);
                    var datashow = JSON.parse(data);
                    // alert(datashow.company_name);

                    var entity = datashow.entityid;
                    var fname = datashow.fname;

                    var mname = datashow.mname;
                    var lname = datashow.lname;

                    var telephoneNo2 = datashow.telephoneNo2;
                    var telephoneNo2Type = datashow.telephoneNo2Type;
                    var fscemail = datashow.fscemail;


                    var firstext = datashow.ext1;
                    var secondext = datashow.ext2;


                    if (fname == null) {
                        var fnamess = '';
                    } else {
                        var fnamess = fname;
                    }


                    if (mname == null) {
                        var mnamess = '';
                    } else {
                        var mnamess = mname;
                    }

                    if (lname == null) {
                        var lnamess = '';
                    } else {
                        var lnamess = lname;
                    }


                    if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee' || datashow.Type == 'Vendor') {
                        var usertype = datashow.Type;

                    } else {
                        var usertype = 'Client';
                    }
                    // alert(usertype);
                    if (usertype == 'Client') {
                        $('#names').show();
                        $('.names1').hide();


                        if (datashow.business_id == '6') {
                            var headings = fnamess + ' ' + mnamess + ' ' + lnamess;
                        } else {

                            var headings = datashow.company_name;
                        }
                    } else if (datashow.Type == 'Vendor') {
                        var headings = datashow.company_name;
                        $('.hidename').hide();
                        $('#names').hide();
                        $('.names1').val(headings);


                    } else if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee') {
                        $('#names').show();
                        $('.names1').hide();

                        //  alert(datashow.fname);
                        if (fname == null) {
                            var fnamess = '';
                        } else {
                            var fnamess = fname;
                        }


                        if (mname == null) {
                            var mnamess = '';
                        } else {
                            var mnamess = mname;
                        }

                        if (lname == null) {
                            var lnamess = '';
                        } else {
                            var lnamess = lname;
                        }
                        var headings = fnamess + ' ' + mnamess + ' ' + lnamess;

                    }


                    if (usertype == 'Client') {
                        $('.companyphone').show();
                        $('.companyemail').show();

                        $('.personalphone').hide();
                        $('.personalemail').hide();


                        $('.ahref').html('<a href="https://financialservicecenter.net/fscemployee/clients/' + datashow.cid + '/edit" class="btn btn-primary" style="width:62px !important;">Go</a>');

                    } else if (usertype == 'employee' || usertype == 'user' || usertype == 'clientemployee' || usertype == 'Vendor') {

                        $('.companyphone').hide();
                        $('.companyemail').hide();

                        $('.personalphone').show();
                        $('.personalemail').show();

                        // $('.ahref').html('<a href="https://financialservicecenter.net/fscemployee/employee/' + datashow.cid + '/edit" class="btn btn-primary">Go</a>');
                    }

                    if (fname == null) {
                        var fnames = ' ';
                    } else {
                        var fnames = fname;
                    }

                    if (mname == null) {
                        var mnames = ' ';
                    } else {
                        var mnames = mname;
                    }
                    if (lname == null) {
                        var lnames = '';
                    } else {
                        var lnames = lname;
                    }
                    if (datashow.business_id != '6') {
                        var fullname = datashow.firstname + ' ' + datashow.lastname;
                        var telephone = datashow.telephone;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                        var telephone2 = datashow.telephone2;


                    } else {
                        var fullname = fnames + ' ' + mnames + ' ' + lnames;
                        var telephone = datashow.telephone;
                        var telephone2 = datashow.telephone2;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                    }
                    var email = datashow.email;
                    $('#names').val(fullname);

                    $('#entityids').val(entity);
                    $('.emails').val(email);
                    $('.telephones').val(telephone);
                    $('#telephones2').val(telephone2);
                    $('.teltype').val(telephonetype);
                    $('#teltype2').val(telephonetype2);
                    $('#headings').html(headings);
                    $('.exts').val(firstext);
                    $('.extss').val(secondext);
                    $('.telephonesNo2').val(telephoneNo2);
                    $('.telephonesNo2Type').val(telephoneNo2Type);
                    $('.fscemail').val(fscemail);


                    var v = document.getElementById("newmyModal");
                    v.classList.add("in");

                    //  $('#countryList').html(data);


                }
            });


        }

        $('.close').click(function () {
            // alert();
            $('#newmyModal').removeClass('in');
        })

        $('.modal-footer .btn-default').click(function () {
            //  alert();
            $('#newmyModal').removeClass('in');
        })


    </script>
    <script>
        $(document).ready(function () {


            $('.btnpopclose').click(function () {
                // alert();
                $('.modal').removeClass('in');
                $('.modal').hide();
            })

            $('#country_name').keyup(function () {
                var query = $(this).val();
                //alert(query);
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('employee.fetch11') }}",
                        method: "POST",
                        data: {query: query, _token: _token},
                        success: function (data) {
                            $('#countryList').fadeIn();
                            $('#countryList').html(data);


                        }
                    });
                }
            });
            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removemsg.removemsg1')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            location.reload(true);
                        }
                    })
                } else {
                    return false;
                }
            });
        });

        $('.accordion__answer:first').show();
        $('.accordion__question:first').addClass('expanded');

        $('.accordion__question').on('click', function () {
            var content = $(this).next();

            $('.accordion__answer').not(content).slideUp(400);
            $('.accordion__question').not(this).removeClass('expanded');
            $(this).toggleClass('expanded');
            content.slideToggle(400);
        });
    </script>


    @if(Auth::user()->active==1 )
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    @endif
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {
            $('#superid').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    code: {
                        validators: {

                            notEmpty: {
                                message: 'Please Enter Supervisor Code'
                            },
                            remote: {
                                type: 'POST',
                                url: '{{ URL::to('fscemployee/super') }}',
                                message: 'The  Supervisor Code not available',
                                delay: 2000


                            }


                        }
                    },


                }
            })
                .on('success.form.bv', function (e) {

                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#superid').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {

                        console.log(result);
                    }, 'json');
                });
        });


        $(document).ready(function () {
            $('#superid1').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    code1: {
                        validators: {

                            notEmpty: {
                                message: 'Please Enter Supervisor Code'
                            },
                            remote: {
                                type: 'POST',
                                url: '{{ URL::to('fscemployee/super1') }}',
                                message: 'The  Supervisor Code not available',
                                delay: 2000


                            }


                        }
                    },


                }
            })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#superid1').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        console.log(result);
                    }, 'json');
                });
        });

    </script>

    <?php
    if(empty($remainder->emp_out))
    {
    if($employee->type != 'user')
    {
    if(isset($remainder->remainder_date))
    {
    $remday = $remainder->remainder_date;
    $rem3 = date('Y-m-d', strtotime('-3 days', strtotime($remday)));
    $rem2 = date('Y-m-d', strtotime('-2 days', strtotime($remday)));
    $rem1 = date('Y-m-d', strtotime('-1 days', strtotime($remday)));
    $rem4 = date('Y-m-d', strtotime('-0 days', strtotime($remday)));
    $beforethreeday = strtotime($rem3);
    $enddate = strtotime($remday);
    $current = date('Y-m-d');
    if($current == $rem3 || $current == $rem2 || $current == $rem1)
    {
    ?>
    @if (session()->has('success'))
        <script>
            $(window).load(function () {
                $('#myModalremainder').modal('show');
            });
        </script>
    @endif
    <div id="myModalremainder" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Clock Out Remainder </h4>
                </div>
                <div class="modal-body">
                    <p>You did not clock out previously <br> Please contact your Admin to clock out then after you can clock in.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php
    } else { ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#myModalremainder").modal({
                backdrop: 'static',
                keyword: 'false',
                show: true,
            });

            $('.close').click(function () {
                // alert();
                $('#newmyModal').removeClass('in');
            })

            $('.modal-footer .btn-default').click(function () {
                //  alert();
                $('#newmyModal').removeClass('in');
            })

        });
    </script>
    <div id="myModalremainder" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Clock Out Remainder</h4>
                </div>
                <div class="modal-body">
                    <p>You did not clock out previously <br> Please contact your Admin to clock out then after you can clock in.</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('fscemployee.logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" class="btn btn-default"><i class="fa fa-sign-out sidebar-icon"></i><span>Log Out</span></a>
                    <form id="logout-form" action="{{ route('fscemployee.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>

        </div>
    </div>
    <?php }}}}?>



    <!--Modal Start Notes-->
    <div id="myModalnotes" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">
            <form method="post" action="{{route('addnotesrelated1')}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Note</h4>
                    </div>
                    <div class="modal-body">
                        <div style="max-width: 900px;margin-left: auto;">
                            <div class="row mt-3" style="margin-top:20px;">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-5 text-right" style="padding-right:28px!important;padding-left:0px;width: 115px;"><strong>Date:</strong></label>
                                            <div class="col-md-6" style="padding-left:6px; padding-right:0px">
                                                <input type="text" name="notedate" id="date2" class="form-control text-center" value="{{date('m-d-Y')}}" placeholder="Date" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3 text-right" style="padding-left: 0px !important; padding-right: 20px!important;"><strong>Day:</strong></label>
                                            <div class="col-md-7" style=" padding-right:0px!important; padding-left:0px!important;">
                                                <input type="text" name="noteday" id="day2" class="form-control text-center" placeholder="Day" value="{{date('l')}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3"><strong>Time:</strong></label>
                                            <div class="col-md-7">
                                                <input type="text" name="notetime" id="time2" class="form-control text-center" value="{{date("g:i a")}}" placeholder="Time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-5">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Select:</strong></label>
                                        <div class="col-md-7" style="padding-right: 0px;">
                                            <div class="">
                                                <select class="form-control fsc-input" name="notetype_user" id="notetype_user" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('notesrelated') ? ' has-error' : '' }}  col-md-7">
                                    <div class="row">
                                        <label class="control-label col-md-4 text-right" style="text-align:right;width: 123px;padding-left: 0px;padding-right: 0px;"><strong>Related to :</strong></label>
                                        <div class="col-md-6" style="width:43%;">
                                            <div class="">
                                                <select class="form-control" name="noterelated" id="notesrelatedname" required>
                                                    <option>Select</option>
                                                    @foreach($notesRelated as $notesRelated)
                                                        <option value="{{$notesRelated->id}}">{{$notesRelated->notesrelated}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($errors->has('notesrelated'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('notesrelated') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-1" style="padding: 0px;margin-top:3px;width: 92px;">
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalNotes" class="redius"><i class="fa fa-plus"></i></a>&nbsp;
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal4" class="redius"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-group" id="noteclientid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Client :</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control" name="noteclientid">
                                            <option>Select</option>
                                            @foreach($listclient as $client)
                                                <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="noteemployeeuserid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>EE / User:</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control" name="noteemployeeuserid">
                                            <option>Select</option>
                                            @foreach($listemployeeuser as $employee)
                                                <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="notevendorid" style="display:none;">
                                <div class="row">

                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Vendor :</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control" name="notevendorid">
                                            <option>Select</option>
                                            @foreach($listvendoe as $vendoe)
                                                <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="noteotherid" style="display:none;">
                                <div class="row">

                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Other :</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <input type="text" class="form-control" name="noteotherid">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:120px; text-align:right; padding-left:0px!important;"><strong>Type of Note:</strong></label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="notesrelatedcat" id="notesrelatedcat" required>
                                            <option>Select</option>
                                            <option>Permenant Note</option>
                                            <option>Temporary Note</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:120px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                                    <div class="col-md-9" style="width: 72.5%;">
                                        <textarea rows="3" cols="12" class="form-control" name="notes"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:120px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                    <div class="col-md-2">
                                        <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" href="https://financialservicecenter.net/fscemployee">Cancel</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>
    <!--Modal End Notes-->

    <!--Add plus Notes Value Model Start-->
    <div class="modal fade" id="basicExampleModalNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Related to Notes</h4>

                </div>
                <form action="" method="post" id="ajax2">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="text" id="newnotesrelated" name="newopt2" class="form-control" placeholder="Related Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="notesrelated" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add plus value Model End-->

    <!--Minus Notes value Model Start-->
    <div class="modal fade" id="basicExampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background:#038ee0;">
                    <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#000;">Related to Notes
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                    <div class="curency curency_ref" id="div">
                        @foreach($notesRelated1 as $cur)
                            <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#def9ff;">
                                <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">

                                    <a class="deletenotes" style="color:#000;" id="{{$cur->id}}">{{$cur->notesrelated}}

                                        <span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Minus Notes value Model End-->


    <!-- Modal Start Conversation  -->
    <div id="mytask1" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:900px;">
            <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Task</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingtwo">
                                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapser_2" aria-expanded="false" style="display:inline-flex;width:100%">
                                            <span class="ac_name_first">Daily Responsibility </span>
                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                            <i class="more-less glyphicon glyphicon-minus"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="collapser_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body accordion-body" style="padding-top:15px;">
                                        <table class="table table-hover table-bordered" id="sampleTable2">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Subject</th>
                                                <th width="33%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1;?>
                                            @foreach($respdaily as $resp)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$resp->title}}</td>
                                                    <td class="text-center"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#howtodoresp" style="margin-right:10px" class="redius">HowToDo</a><a class="btn btn-primary getcomplete" rel='{{$resp->id}}' href="#">Complete</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingtwo">
                                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapser_4" aria-expanded="false" style="display:inline-flex;width:100%">
                                            <span class="ac_name_first">Task </span>
                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                            <i class="more-less glyphicon glyphicon-minus"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="collapser_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body accordion-body" style="padding-top:15px;">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered" id="sampleTable2">
                                                <thead>
                                                <tr>
                                                    <th style="width:10%">Task Date</th>
                                                    <th>Created By</th>
                                                    <th>Client / Other</th>
                                                    <th>Subject</th>
                                                    <th>Read Status</th>
                                                    <th>Task Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php //print_r($taskall);?>
                                                @foreach($taskall as $com)
                                                    <tr>

                                                        <td style="width:10%">{{date('M-d-Y',strtotime($com->created_at))}}</td>
                                                        <td style="width:15%">@foreach($admin as $com1) @if($com->admin_id==$com1->id) {{ucwords($com1->name)}} @endif @endforeach</td>
                                                        <td style="width:15%">
                                                            @if($com->whone =='Client')
                                                                @foreach($commonregisterall as $com2)
                                                                    @if($com->client==$com2->id)
                                                                        {{ucwords($com2->company_name)}}
                                                                    @endif
                                                                @endforeach
                                                            @elseif($com->whone =='Other')
                                                                {{$com->othername}}
                                                            @endif</td>
                                                        <td style="width:15%">{{$com->title}}</td>
                                                        <td>@if($com->checked==1) Read @endif @if($com->checked==0) Not Read @endif</td>
                                                        <td>@if($com->status==2) In Progress @endif @if($com->status==0) Wait @endif  @if($com->status==1) Start @endif @if($com->status==3) End @endif</td>
                                                        <td class="text-center"><a class="btn-action btn-view-edit" href="{{route('tasks.edit',$com->id)}}"><i class="fa fa-edit"></i></a><a class="btn-action btn-view-edit" href="{{route('tasks.edit',$com->id)}}"><i class="fa fa-eye"></i></a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3 pull-right" style="padding-left:6px; margin-top:3px; padding-right:0px; text-align:right;">
                            <a href="{{url('fscemployee/tasks')}}" class="btn btn-primary" class="redius">Task List</a>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div id="myModalcustomershseet" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">
            <form method="post" action="{{route('addcoversationsheet1')}}" class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}

            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Conversation Sheet</h4>
                    </div>
                    <div class="modal-body">
                        <div style="max-width: 870px;margin-left: auto;">
                            <div class="row mt-3" style="margin-top:20px;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-5 text-right" style="padding-right:28px!important;padding-left:0px;width: 115px;"><strong>Date:</strong></label>
                                            <div class="col-md-6" style="padding-left:2px; padding-right:0px">
                                                <!--<input type="text" class="form-control effective_date2"/>-->
                                                <input type="text" name="date" id="date2" class="form-control text-center" value="{{date('m-d-Y')}}" placeholder="Date" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3 text-right" style="padding-left: 0px !important; padding-right: 20px!important;"><strong>Day:</strong></label>
                                            <div class="col-md-7" style="padding-right: 0px!important; padding-left: 0px;">
                                                <input type="text" name="day" id="day2" class="form-control text-center" placeholder="Day" value="{{date('l')}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3"><strong>Time:</strong></label>
                                            <div class="col-md-6">
                                                <input type="text" name="time" id="time2" class="form-control text-center" value="{{date("g:i a")}}" placeholder="Time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group{{ $errors->has('type_user') ? ' has-error' : '' }} col-md-5">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Select:</strong></label>
                                        <div class="col-md-8">
                                            <div class="">
                                                <select class="form-control fsc-input" name="type_user" id="type_user" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                            @if($errors->has('type_user'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('type_user') }}</strong>
                        </span>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('conrelatedname') ? ' has-error' : '' }}  col-md-7">
                                    <div class="row">
                                        <label class="control-label col-md-4" style="text-align:right;width: 123px;padding-left: 0px;padding-right: 0px;"><strong>Related to:</strong></label>
                                        <div class="col-md-6" style="width:36%;">
                                            <div class="">
                                                <select class="form-control" name="conrelatedname" id="conrelatedname" required>
                                                    <option>Select</option>
                                                    @foreach($relatedNames as $relatedNames)
                                                        <option value="{{$relatedNames->id}}">{{$relatedNames->relatednames}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($errors->has('conrelatedname'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('conrelatedname') }}</strong>
                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-1" style="padding: 0px;margin-top:3px;width: 92px;">
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="clientid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Client :</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <select class="form-control" name="clientid">
                                            <option>Select</option>
                                            @foreach($listclient as $client)
                                                <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="employeeuserid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>EE / User:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <select class="form-control" name="employeeuserid">
                                            <option>Select</option>
                                            @foreach($listemployeeuser as $employee)
                                                <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="vendorid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Vendor:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <select class="form-control" name="vendorid">
                                            <option>Select</option>
                                            @foreach($listvendoe as $vendoe)
                                                <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="otherid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:115px; text-align:right; padding-left:0px!important;"><strong>Other :</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <input type="text" class="form-control" name="otherid">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:115px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <textarea rows="3" cols="12" class="form-control" name="condescription"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:115px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <textarea rows="3" cols="12" class="form-control" name="connotes"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3" style="width:115px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                    <div class="col-md-2">
                                        <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" href="https://financialservicecenter.net/fscemployee">Cancel</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>
    <!--Modal End Conversation-->


    <!--Add plus Conversation Value Model Start-->
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Related</h4>

                </div>
                <form action="" method="post" id="ajax1">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="text" id="newrelatednames" name="newopt" class="form-control" placeholder="Related Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="addrelatedname" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add plus Conversation value Model End-->
    <div class="modal fade" id="howtodoresp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">HowToDo</h4>

                </div>
                <div class="modal-body">
                    <input type="text" id="newrelatednames" name="newopt" class="form-control" placeholder="Related Name"/>
                </div>
                <div class="modal-footer">
                    <button type="button" id="addrelatedname" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
            <div class="modal-content" style="width:100%;">
                <div class="modal-header" style="background:#00c0ef !important;color:white;text-align:center;">
                    <h3 class="modal-title" id="exampleModalLabel"><span class="btn btn-primary countbox" style="border:none !important;padding-right: 6px;">Count :</span><span class="btn btn-primary countbox" style="float:left;">0</span>Reminder ( 30 Days )
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </h5>
                </div>
                <div class="modal-body" style="width:100%;">
                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_1" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Tobacco (Excise) Tax Rep. </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_2" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Sales Tax Reporting Service  </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_3" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Individual Income Tax Return </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_4" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">COAM Reporting Service </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_5" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Business Income Tax Return </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsr_6" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">941 Reporting Service </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsr_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                                    <span>
                                                        <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                                    </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
            <div class="modal-content" style="width:100%;">
                <div class="modal-header" style="background:#f39c12 !important;color:white;text-align:center;">
                    <h3 class="modal-title" id="exampleModalLabel"><span class="btn btn-primary countbox" style="border:none !important;padding-right: 6px;">Count :</span><span class="btn btn-primary countbox" style="float:left;">0</span>Notification ( 10 Days )
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </h5>
                </div>
                <div class="modal-body" style="width:100%;">
                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_1" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Tobacco (Excise) Tax Rep. </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_2" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Sales Tax Reporting Service  </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_3" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Individual Income Tax Return </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_4" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">COAM Reporting Service </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_5" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">Business Income Tax Return </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsn_6" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">2</span><span class="ac_name_first">941 Reporting Service </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsn_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th>No</th>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Regarding</th>
                                            <th>EE Responsible Person</th>
                                            <th>Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="alphabet">C</span>-</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>



    <!--<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
    <div class="modal-content" style="width:100%;">
      <div class="modal-header" style="background:#dd4b39  !important;color:white;text-align:center;">
        <h3 class="modal-title" id="exampleModalLabel" style="color:#fff !important;"><span class="btn btn-primary countbox" style="float:left;">Count:0</span>Warning (2 Days) <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h5>
      </div>
      <div class="modal-body" style="width:100%;">
        <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_1" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">Tobacco (Excise) Tax Rep. </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_2" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">Sales Tax Reporting Service  </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_3" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">Individual Income Tax Return </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_4" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">COAM Reporting Service </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_5" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">Business Income Tax Return </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_6" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">941 Reporting Service </span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsew_7" aria-expanded="false" style="display:inline-flex;width:100%">
                            <span class="count_acc">2</span><span class="ac_name_first">Corporation</span>
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <i class="more-less glyphicon glyphicon-minus"></i>
                        </a>
                    </h4>
                </div>
                <div style="clear:both;"></div>
                <div id="collapsew_7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body accordion-body" style="padding-top:15px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No</th>
                                    <th>Client ID</th>
                                    <th>Client Name</th>
                                    <th>Regarding</th>
                                    <th>EE Responsible Person</th>
                                    <th>Filling Status</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td><span class="alphabet">C</span>-</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="action">
                                        <span>
                                            <a href="#" class="btn-action btn-view-edit federalTaxation" data-id=""><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
    </div>
!-->

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
            <div class="modal-content">
                <div class="modal-header" style="background:#dd4b39  !important;color:white;text-align:center;">
                    <?php
                    $arrc = array();
                    foreach ($clientoriginal as $corg) {
                        $arrc[] = $corg->client_id;
                    }
                    $cntss = 0;
                    foreach ($commonone1 as $taxfederal2) {
                        if (!in_array($taxfederal2->ids, $arrc)) {


                            $cntss++;

                        }
                    }

                    ?>
                    <?php
                    $ctax1 = array();
                    foreach ($clientttaxation as $ctax) {
                        $ctax1[] = $ctax->taxation_service;
                    }
                    ?>

                    <?php //echo count($c2);?>
                    <h3 class="modal-title" id="exampleModalLabel" style="color:#fff !important;"><span class="btn btn-primary countbox" style="border:none !important;padding-right: 6px;">Count :</span><span class="btn btn-primary countbox"><?php echo $cntss;?></span>Warning ( 2 Days )
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                        @foreach($taxtitles as $taxx)
                            @if(in_array($taxx->id,$ctax1))

                                <?php
                                $cntss1 = 0;
                                $cntss = array();
                                foreach ($commonone1 as $taxfederal1) {

                                    if ($taxfederal1->status == $taxx->id) {

                                        if (!in_array($taxfederal1->ids, $arrc)) {
                                            // echo $taxx->id;
                                            // echo $taxfederal1->tids;
                                            $cntss1++;
                                            // echo $cnts;
                                            if (isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null) {
                                                $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($taxfederal1->expiredate)));
                                                $exp_dates = strtotime($exp_date);

                                                $today = date('M-d-Y');
                                                $match_today = strtotime($today);

                                                if ($match_today >= $exp_dates) {
                                                    $cntss[] = $cntss1++;

                                                }
                                            }
                                        }
                                    }
                                }
                                //echo count($cntss);
                                ?>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingtwo">
                                        <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed"
                                               href="#collapsew_{{$taxx->id}}" aria-expanded="false" style="display:inline-flex;width:100%">
                                                <span style="background: #dd4b39;color: #ffffff;border: 1px solid #ffffff;" class="count_acc"><?php echo count($cntss);?></span><span class="ac_name_first">{{$taxx->title}} </span>
                                                <i class="more-less glyphicon glyphicon-plus"></i>
                                                <i class="more-less glyphicon glyphicon-minus"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div id="collapsew_{{$taxx->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body accordion-body" style="padding-top:15px;">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th>No</th>
                                                    <th>Client ID</th>
                                                    <th>Client Name</th>

                                                    <th>Regarding</th>
                                                    <th>EE Responsible Person</th>
                                                    <th>Filling Status</th>


                                                    <th>Due Date</th>
                                                    <th>Status</th>

                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                //  echo "<pre>";print_r($arrc);
                                                $cnts = 0;
                                                //echo "<pre>";print_r($commonone1);
                                                foreach($commonone1 as $taxfederal1)
                                                {

                                                if($taxfederal1->status == $taxx->id)
                                                {

                                                if(!in_array($taxfederal1->ids, $arrc))
                                                {
                                                // echo $taxx->id;
                                                // echo $taxfederal1->tids;
                                                $cnts++;
                                                // echo $cnts;
                                                if(isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null)
                                                {
                                                $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($taxfederal1->expiredate)));
                                                $exp_dates = strtotime($exp_date);

                                                $today = date('M-d-Y');
                                                $match_today = strtotime($today);

                                                if($match_today >= $exp_dates)
                                                {
                                                //echo "<pre>"; print_r($taxfederal1);
                                                ?>
                                                <tr>

                                                    <td class="text-center">{{$cnts}}</td>
                                                    <td><span class="alphabet">C</span>-{{$taxfederal1->filename}}</td>
                                                    <td>@if($taxfederal1->business_id =='6'){{$taxfederal1->first_name}} {{$taxfederal1->middle_name}} {{$taxfederal1->last_name}} @else {{$taxfederal1->company_name}} @endif</td>

                                                    <td>{{$taxx->title}}</td>
                                                    <td>{{$taxfederal1->firstName}} {{$taxfederal1->middleName}} {{$taxfederal1->lastName}}</td>
                                                    <td>{{$taxfederal1->federalstax}}</td>

                                                    <td><?php if ($taxfederal1->expiredate != '0000-00-00') {
                                                            echo date('M-d Y', strtotime($taxfederal1->expiredate));
                                                        }?></td>
                                                    <td><?php if ($taxfederal1->personalstatus == '0') {
                                                            echo 'Not Done';
                                                        } else {
                                                            echo 'Done';
                                                        }?></td>
                                                    <td>
                                                        <?php
                                                        if($taxfederal1->tids != '')
                                                        {
                                                        ?>
                                                        <a href="{{route('workstatustaxation.edit',$taxfederal1->tids)}}" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>

                                                        <button type="button" class="btn-action btn-view-edit federalTaxation" data-id="{{$taxfederal1->ids}}"><i class="fa fa-edit"></i></button>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>

                                                </tr>
                                                <?php

                                                }
                                                }
                                                }
                                                }
                                                }
                                                ?>


                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsewcorp_4" aria-expanded="false" style="display:inline-flex;width:100%">
                                        <span class="count_acc" style="background: #dd4b39;color: #ffffff;border: 1px solid #ffffff;"><?php //echo count($cntss);?></span><span class="ac_name_first">Corporation </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsewcorp_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th width="3%">No</th>
                                            <th width="13%">Client ID</th>
                                            <th width="14%">Client Name</th>
                                            <th width="20%">Regarding</th>
                                            <th width="18%">EE Responsible Person</th>
                                            <th width="10%">Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">&nbsp;</td>
                                            <td><span class="alphabet">C</span>-&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Minus Convesation value Model Start-->
    <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background:#038ee0;">
                    <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#000;">Related to Convesation
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                    <div class="curency curency_ref" id="div">
                        @foreach($relatedNames1 as $cur)
                            <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#def9ff;">
                                <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">

                                    <a class="deleterelated" style="color:#000;" id="{{$cur->id}}">{{$cur->relatednames}}

                                        <span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--MinusConvesation value Model End-->

    <script>
        $(document).ready(function () {
            $(document).on('click', '.getcomplete', function () {
                var id = $(this).attr('rel');
                // alert(id);

                $.ajax({
                    url: "{{route('getComplete.getComplete1')}}",
                    mehtod: "post",
                    data: {id: id},
                    success: function (data) {
                        location.reload(true);
                    }
                })

            });

            $(document).on('click', '.deletenotes', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removenotenote.removenotesnote1')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            location.reload(true);
                        }
                    })
                } else {
                    return false;
                }
            });

            $(document).on('click', '.deleterelated', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removerelated.removerelated1')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            location.reload(true);
                        }
                    })
                } else {
                    return false;
                }
            });


            $('#notetype_user').on('change', function () {
                //otherid,clientid,employeeuserid,vendorid
                //Client,EE-User,Vendor,Other
                var thisss = $('#notetype_user').val();
                if (thisss == 'Client') {
                    $('#noteclientid').show();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'EE-User') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').show();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'Vendor') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').show();
                    $('#noteotherid').hide();
                } else if (thisss == 'Other') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').show();
                }
            });

            $('#type_user').on('change', function () {
                //otherid,clientid,employeeuserid,vendorid
                //Client,EE-User,Vendor,Other
                var thisss = $('#type_user').val();
                if (thisss == 'Client') {
                    $('#clientid').show();
                    $('#employeeuserid').hide();
                    $('#vendorid').hide();
                    $('#otherid').hide();
                } else if (thisss == 'EE-User') {
                    $('#clientid').hide();
                    $('#employeeuserid').show();
                    $('#vendorid').hide();
                    $('#otherid').hide();
                } else if (thisss == 'Vendor') {
                    $('#clientid').hide();
                    $('#employeeuserid').hide();
                    $('#vendorid').show();
                    $('#otherid').hide();
                } else if (thisss == 'Other') {
                    $('#clientid').hide();
                    $('#employeeuserid').hide();
                    $('#vendorid').hide();
                    $('#otherid').show();
                }
            });
        });
    </script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(function () {

            $('#notesrelated').click(function () { //alert();
                var newopt2 = $('#newnotesrelated').val();

                if (newopt2 == '') {
                    alert('Please enter somethings!');
                    return;
                }

                //check if the option value is already in the select box
                $('#vendor_product option').each(function (index) {
                    if ($(this).val() == newopt2) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "{!!route('notesrelated1.notesrelateds1')!!}",
                    dataType: "json",
                    data: $('#ajax2').serialize(),
                    success: function (data) {
                        alert('Successfully Added');

                        $('#vendor_product').append('<option value=' + newopt2 + '>' + newopt2 + '</option>');
                        $("#div").load(" #div > *");
                        $("#newnotesrelated").val('');
                        location.reload();
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                $('#basicExampleModalposition').modal('hide');
            });


        });

        $(function () {
            $('#addrelatedname').click(function () { //alert();
                var newopt = $('#newrelatednames').val();

                if (newopt == '') {
                    alert('Please enter somethings!');
                    return;
                }

                //check if the option value is already in the select box
                $('#vendor_product option').each(function (index) {
                    if ($(this).val() == newopt) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "{!!route('relatedname.relatednames')!!}",
                    dataType: "json",
                    data: $('#ajax1').serialize(),
                    success: function (data) {
                        alert('Successfully Added');
                        $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                        $("#div").load(" #div > *");
                        $("#newrelatednames").val('');
                        location.reload();
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                $('#basicExampleModalposition').modal('hide');
            });
        });

        //   $('.accordion__answer:first').show();
        //$('.accordion__question:first').addClass('expanded');

        $('.accordion__question').on('click', function () {
            var content = $(this).next();

            $('.accordion__answer').not(content).slideUp(400);
            $('.accordion__question').not(this).removeClass('expanded');
            $(this).toggleClass('expanded');
            content.slideToggle(400);
        });

        $(function () {
            $('.modal').on('hidden.bs.modal', function () {
                // alert('fsdfs');
                $('textarea').val('');
                // $('#noteforms').reset();
                //  $(this).removeData('bs.modal');
            });
        })
    </script>




@endsection