@extends('fscemployee.layouts.app')
@section('title', 'Responsiblity')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Edit / View Responsiblity</h1>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if(session()->has('success') )
                            <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="sampleTable3">

                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Details</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rules as $rul)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$rul->title}}</td>
                                        <td>{!!$rul->rules!!}</td>
                                        <td><a class="btn-action btn-view-edit" href="{{route('resposibilty.edit',$rul->id)}}"><i class="fa fa-edit"></i></a></td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection