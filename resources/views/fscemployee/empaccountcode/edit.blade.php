@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        .select2-container--default .select2-selection--single, .form-control {
            background: #ccc !important;
            pointer-events: none;
        }

        .form-control {
            pointer-events: none;
        }
    </style>
    <div class="content-wrapper">
        <section class="page-title content-header">
            <h1>Edit Account Code </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="#" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}} {{method_field('PATCH')}}

                                <div class="form-group {{ $errors->has('accountcode') ? ' has-error' : '' }}" style="pointer-events: none;">
                                    <label class="control-label col-md-3">Account Code : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <select class="js-example-tags form-control" id="accountcode" name="accountcode" style="backgraund:#ccc">
                                            <option value="">Select</option>
                                            @foreach($accountcode1 as $cur)
                                                <option value="{{$cur->accountcode}}" @if($cur->accountcode==$accountcode->accountcode) selected @endif>{{$cur->accountcode}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('accountcode'))
                                            <span class="help-block">
											<strong>{{ $errors->first('accountcode') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('account_name') ? ' has-error' : '' }}" style="pointer-events: none;">
                                    <label class="control-label col-md-3">Account Name : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="account_name" type="text" id="account_name" class="form-control p-l-10" value="{{$accountcode->account_name}}"/>
                                        @if ($errors->has('account_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('account_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('account_belongs_to') ? ' has-error' : '' }}" style="pointer-events: none;">
                                    <label class="control-label col-md-3">Account Belongs To: <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="account_belongs_to" id="account_belongs_to">
                                            <option value="">Select</option>
                                            <option value="Cost of Sales" @if($accountcode->account_belongs_to=='Cost of Sales') selected @endif>Cost of Sales</option>
                                            <option value="Expenses" @if($accountcode->account_belongs_to=='Expenses') selected @endif>Expenses</option>
                                            <option value="Cash and Bank Balance" @if($accountcode->account_belongs_to=='Cash and Bank Balance') selected @endif>Cash and Bank Balance</option>
                                            <option value="Accounts Payable" @if($accountcode->account_belongs_to=='Accounts Payable') selected @endif>Accounts Payable</option>
                                            <option value="Account Receivable" @if($accountcode->account_belongs_to=='Account Receivable') selected @endif>Account Receivable</option>
                                            <option value="Other Assets" @if($accountcode->account_belongs_to=='Other Assets') selected @endif>Other Assets</option>

                                            <option value="Income" @if($accountcode->account_belongs_to=='Income') selected @endif>Income</option>
                                            <option value="Inventory" @if($accountcode->account_belongs_to=='Inventory') selected @endif>Inventory</option>
                                            <option value="Long Term Liabilities" @if($accountcode->account_belongs_to=='Long Term Liabilities') selected @endif>Long Term Liabilities</option>
                                            <option value="Other Current Assets" @if($accountcode->account_belongs_to=='Other Current Assets') selected @endif>Other Current Assets</option>
                                            <option value="Other Current Liabilities" @if($accountcode->account_belongs_to=='Other Current Liabilities') selected @endif>Other Current Liabilities</option>
                                            <option value="Fixed Assets">Fixed Assets</option>
                                        </select>
                                        @if ($errors->has('account_belongs_to'))
                                            <span class="help-block">
											<strong>{{ $errors->first('account_belongs_to') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2" style="pointer-events: none;">
                                            <input class="btn_new_save" type="button" name="button" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fscemployee/empaccountcode')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 16px !important;
            padding: 0;
            color: #000;
        }

        .p-l-10 {
            padding-left: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-color: #000 transparent transparent transparent;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 6px;
            right: 4px;
        }

        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            /* border: 1px solid #aaa; */
            border-radius: 4px;
            border: 2px solid #2fa6f2;
            height: 40px;
            padding: 8px;
        }</style>
    <script>
        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });

    </script>
@endsection()