@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .dt-buttons {
            display: none;
        }
    </style>
    <style>
        .page-title {
            padding: 8px 0px !important;
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="">
            <div class="">
                <div class="" style="text-align:center;">
                    <h1>List of Account Code <span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header" style="display:none;">
                            <div class="col-md-9" style="margin-left:-1.6%">
                                <div class="row">

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="table-title">
                                    <!--<a href="#">Add New</a>-->
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-title">
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th style="width:5%;">No</th>
                                        <th style="width:11%">Account Code</th>
                                        <th>Account Name</th>
                                        <th>Account Belongs</th>
                                        <th style="width:7%;text-align:center;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($accountcode as $bus)
                                        <tr>
                                            <td style="text-align:center;">{{$loop->index + 1}}</td>
                                            <td style="text-align:center;">{{$bus->accountcode}}</td>
                                            <td>{{$bus->account_name}}</td>
                                            <td>{{$bus->account_belongs_to}}</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('empaccountcode.edit', $bus->id)}}"><i class="fa fa-edit"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>.content-wrapper {
            height: 100%;
        }</style>
@endsection()