@extends('fscemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Telephone Ext. / Email Access</h1>
        </section>
        <style>
            .accessa {
                display: inline-block;
                /*float: right;*/
                font-size: 14px;
                color: #FFF;
                background: #e67300;
                padding: 4px 15px 6px;
                border: 1px solid #333;
            }

            .Branch {
                width: 82%;
                margin: 0 auto 12px auto;
                float: initial;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="Branch" style="margin-bottom:0px;">
                                <h1>Main Office - Email , Telephone , Fax information</h1>
                            </div>
                            <div class="col-md-12" style="padding: 10px 0px;">
                                <div class="box-tools">
                                    <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/email/mainstore">
                                        <input type="hidden" name="_token" value="TP6S3r1b86dT8EucoFBNyizuMqHjOHkY5sZlvK4G">

                                        <div class="col-md-2"></div>
                                        <div class="col-md-3 text-center">
                                            <label>Email</label>
                                            <input readonly type="email" name="main_email" id="" class="form-control" required="" value="{{$maiEmail->mainemail}}" placeholder="Main Email">
                                        </div>
                                        <div class="col-md-2 text-center" style="padding-left:0px;">
                                            <label>Telephone</label>
                                            <input readonly type="text" name="main_telephone" id="main_telephone" required="" class="form-control text-center" value="{{$maiEmail->maintelephone}}" placeholder="Main Telephone">
                                        </div>
                                        <div class="col-md-2 text-center" style="padding-left:0px;">
                                            <label>Fax</label>
                                            <input readonly type="text" name="main_fax" id="main_fax" required="" class="form-control text-center" value="{{$maiEmail->mainfax}}" placeholder="Main Fax">
                                        </div>
                                        <!--<div class="col-md-2" style="padding-left:0px;">-->
                                        <!--    <label>Password</label>-->
                                        <!--    <label onclick="myFunctionone()" style="position: absolute;margin-top: 35px;right: 25px;"><i class="fa fa-eye toggle-password1" style="margin-left:0px;cursor:pointer;color: #337ab7;"></i></label>-->
                                        <!--	<input  type="password" name="" id="pass_one" required="" class="form-control" value="" placeholder="">-->
                                        <!--</div>-->

                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div style="padding:10px;">
                            <div class="Branch" style="margin-bottom:0px;">
                                <h1>{{ ucwords(Auth::user()->name) }}</h1>
                            </div>

                            <div class="col-md-12" style="padding: 10px 20px;">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th width="170px">Telephone</th>
                                            <th width="100px">Ext</th>
                                            <th width="200px">Password</th>
                                            <th width="100px">Get Mail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($email->email))
                                            @if($email->email_access==1)
                                                <tr>
                                                    <td><input readonly type="email" name="main_email" id="" class="form-control" required="" value="{{$email->email}}" placeholder="Main Email"></td>
                                                    <td><input readonly type="text" name="main_telephone" id="main_telephone" required="" class="form-control" value="{{$email->telephone}}" placeholder="Main Telephone"></td>
                                                    <td><input readonly type="text" name="main_fax" id="main_fax" required="" class="form-control" placeholder="Ext" value="{{$email->ext}}"></td>
                                                    <td><label onclick="myFunctionone1()" style="position: absolute;margin-top: 10px;right: 140px;"><i class="fa fa-eye toggle-password1" style="margin-left:0px;cursor:pointer;color: #337ab7;"></i></label>
                                                        <input type="password" name="" id="default_pass_one" required="" class="form-control" value="{{$email->password}}" placeholder="">
                                                    </td>
                                                    <td class="text-center"><a class="btn btn-success" href="https://financialservicecenter.net:2096" target="_blank">Access</a></td>
                                                </tr>
                                            @endif
                                        @endif

                                        @foreach($emp_emai_rights as $key=>$emails)
                                            @if($emails->access==1)
                                                <tr>
                                                    <td><input readonly type="email" name="main_email" id="" class="form-control" required="" value="{{$emails->email}}" placeholder="Main Email"></td>
                                                    <td><input readonly type="text" name="main_telephone" id="main_telephone" required="" class="form-control" value="{{$emails->telephone}}" placeholder="Main Telephone"></td>
                                                    <td><input readonly type="text" name="main_fax" id="main_fax" required="" class="form-control" placeholder="Ext" value="{{$emails->ext}}"></td>
                                                    <td><label onclick="myFunctionone(this)" id="{{$key+1}}" style="position: absolute;margin-top: 10px;right: 140px;"><i class="fa fa-eye toggle-password1" style="margin-left:0px;cursor:pointer;color: #337ab7;"></i></label>
                                                        <input type="password" name="" id="pass_one{{$key+1}}" required="" class="form-control " value="{{$emails->password}}" placeholder="">
                                                    </td>
                                                    <td class="text-center"><a class="btn btn-success" href="{{'https://financialservicecenter.net:2096'}}" target="_blank">Access</a></td>
                                                </tr>
                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!--<div class="col-md-12" style="padding: 10px 0px;">  -->
                            <!--         	<div class="col-md-3">-->
                            <!--         	    <label>Email</label>-->
                        <!--		<input readonly type="email" name="main_email" id="" class="form-control" required="" value="{{$email->email}}" placeholder="Main Email">-->
                            <!--	</div>-->
                            <!--	<div class="col-md-2" style="padding-left:0px;">-->
                            <!--	    <label>Telephone</label>-->
                            <!--		<input readonly type="text" name="main_telephone" id="main_telephone" required="" class="form-control" value="(770) 270-5597" placeholder="Main Telephone">-->
                            <!--	</div>-->

                            <!--	<div class="col-md-2" style="padding-left:0px;">-->
                            <!--	    <label>Ext</label>-->
                            <!--		<input readonly type="text" name="main_fax" id="main_fax" required="" class="form-control" value="(770) 414-5351" placeholder="Main Fax">-->
                            <!--	</div>-->

                            <!--		<div class="col-md-1" style="padding-left:0px;">-->
                            <!--	    <label>GET Mail</label>-->

                            <!--		<div><a class="btn btn-success" href="https://financialservicecenter.net:2095">Access</a></div>-->
                            <!--	</div>-->
                            <!--<div class="col-md-2" style="padding-left:0px;">-->
                            <!--	    <label>Location</label>-->
                        <!--	<input class="form-control" readonly value="{{$email->location}}">-->
                            <!--	</div>-->
                            <!--	<div class="col-md-2" style="padding-left:0px;">-->
                            <!--	    <label>Password</label>-->
                            <!--	    <label onclick="myFunctionone()" style="position: absolute;margin-top: 35px;right: 25px;"><i class="fa fa-eye toggle-password1" style="margin-left:0px;cursor:pointer;color: #337ab7;"></i></label>-->
                            <!--		<input  type="password" name="" id="pass_one" required="" class="form-control" value="" placeholder="">-->
                            <!--	</div>-->

                            <!--       </div>    -->


                            <!--<div class="col-md-12">-->
                        <!--@if ( session()->has('success') )-->
                        <!--    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>-->
                            <!--@endif-->
                        <!--@if ( session()->has('error') )-->
                        <!--    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>-->
                        <!--@endif  -->
                            <!--					<div class="table-responsive">-->
                            <!--					<table class="table table-hover table-bordered" id="sampleTable3">-->
                            <!--							<thead>-->


                            <!--<tr>-->
                            <!--								    <th>No.</th>-->
                            <!--									<th style="text-align:center">Name </th>-->
                            <!--									<th style="text-align:center">Email </th>-->
                            <!--									<th style="text-align:center">Telephone</th>-->
                            <!--									<th style="text-align:center">GET Mail</th>-->
                            <!--									<th style="text-align:center">Ext.</th>-->
                            <!--									<th style="text-align:center">Location</th>-->
                            <!--									<th style="text-align:center">Action</th>-->
                            <!--								</tr>-->
                            <!--							</thead>-->
                            <!--							<tbody>-->
                        <?php //echo $email->for_whom.' '.Auth::user()->user_id;// print_r($email);exit;?>
                        <!--							   	<tr>-->
                            <!--							   	    <td style="text-align:center;">1</td>-->
                            <!--									<td>-->
                        <!--								        {{ucwords($email->firstName.' '.$email->middleName.' '.$email->lastName)}} -->
                            <!--                                   </td>-->
                        <!--									<td>{{$email->email}}</td>-->
                            <!--									<td style="text-align:center;"></td>-->

                            <!--									<td style="text-align:center;"><a class="accessa" href="https://www.financialservicecenter.net:2095" style="margin-right:8px;padding: 9px 15px;" target="_blank">Access</a></td>-->
                        <!--									<td style="text-align:center;">{{$email->ext}}</td>-->
                        <!--									<td>{{$email->location}}</td>-->
                        <!--									<td style="text-align:center;">@if($email->for_whom==Auth::user()->user_id)<a class="btn-action btn-view-edit" href="{{route('eemail.edit', $email->ids)}}"><i class="fa fa-edit"></i></a>@endif</td>-->
                            <!--								</tr>-->
                            <!--                                </tbody>-->
                            <!--						</table>-->
                            <!--					</div>-->
                            <!--				</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        function myFunctionone1() {
            var x = document.getElementById("default_pass_one");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }

        }

        function myFunctionone(e) {
            var eid = e.id;
            var x = document.getElementById("pass_one" + eid);
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#sampleTable1').DataTable({
                dom: 'Bfrtlip',
                buttons: [{
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                    //titleAttr: 'Copy',
                    title: $('h1').text(),
                },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h1').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="A"]', sheet).attr('s', '51');

                            $('c[r^="E"]', sheet).attr('s', '50');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 100, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'center',
                                        image: logo,
                                        height: 50,

                                        margin: [10, 10, -140, 10]
                                    }, {
                                        alignment: 'center',
                                        text: 'List of FSC Email / Telephone Extension',
                                        fontSize: 12,
                                        margin: [-280, 70, 10, 10],
                                    },],
                                    margin: [20, 0, 0, 22], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h6').text(),
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br style="text-align:center;">List of Email / Telephone Extension</center>'
                                );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "targets": 0
                }],
            });

            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });

    </script>
    <script src="https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2"></script>
    <script>
        function generate() {
            var doc = new jsPDF('p', 'pt');
            var res = doc.autoTableHtmlToJson(document.getElementById("sampleTable1"));
            doc.autoTable(res.columns, res.data, {margin: {top: 80}});
            var header = function (data) {
                doc.setFontSize(20);
                doc.setTextColor(10);
                doc.setFontStyle('normal');
                // doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
                doc.text("List of Email / Telephone Extension", data.settings.margin.left, 50);
            };
            var options = {
                beforePageContent: header,
                margin: {
                    top: 80
                },
                startY: doc.autoTableEndPosY() + 20
            };
            doc.autoTable(res.columns, res.data, {margin: {top: 80}}, options);
            doc.save("table.pdf");
        }
    </script>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:650px !important">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" margin-top: -95px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" id="printThis">
                        <div style=" text-align: center; "><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}" alt=""></div>
                        <h5 class="modal-title" id="exampleModalLabel"><p style="text-align:center; margin: 5px; font-size: 16px;">List of FSC Email / Telephone Extension</p></h5>
                        <table class="table table-hover table-bordered" id="sampleTable2">
                            <thead>
                            <tr>
                                <th style="text-align:center">EE / User</th>
                                <th style="text-align:center">Name</th>
                                <th style="text-align:center;width:200px !important;">Email</th>
                                <th style="text-align:center">Ext.</th>
                                <th style="text-align:center">Location</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {{ucwords($email->type)}}
                                </td>
                                <td>
                                    {{ucwords($email->firstName.' '.$email->middleName.' '.$email->lastName)}}
                                </td>
                                <td>{{$email->email}}</td>
                                <td>
                                    <center>{{$email->ext}}</center>
                                </td>
                                <td>{{$email->location}}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="btnPrint" type="button" class="btn btn-default">Print</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementById("btnPrint").addEventListener("click", function () {
            var printContents = document.getElementById('printThis').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        });
    </script>
@endsection()