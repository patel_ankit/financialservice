@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header page-title">
            <div class="">
                <div class="" style="text-align:center;">
                    <h1>List of Schedule Details <span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="col-md-8" style="margin-left:-1.6%">
                                <div class="row">
                                    <div class="col-md-1"><label style="margin-left:28%">Filter: </label></div>
                                    <div class="col-md-5">
                                        <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                                            <option value="1">All</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Bi-Weekly">Bi-Weekly</option>
                                            <option value="Bi-Weekly">Monthly</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tools pull-right">
                                <div class="table-title">
                                    <a href="{{route('schedules.create')}}">Add New Schedule</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr style="text-align:center">
                                        <th style="text-align:center">No</th>
                                        <th style="text-align:center">Employee Name</th>
                                        <th style="text-align:center">Employer City</th>
                                        <th style="text-align:center">Duration</th>
                                        <th style="text-align:center">Start Date</th>
                                        <th style="text-align:center">End Date</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($schedule as $employ)
                                        <tr class="cc{{$employ->id}}">
                                            <td style="text-align:center">@foreach($emp as $employ1) @if($employ1->id==$employ->emp_name){{$employ1->employee_id}}  @if($employ1->check=='1') @else
                                                    <style>.cc{{$employ->id}} {
                                                            display: none
                                                        }</style> @endif @endif @endforeach</td>
                                            <td>@foreach($emp as $employ1)@if($employ1->id==$employ->emp_name){{$employ1->firstName}} {{$employ1->middleName}} {{$employ1->lastName}} @endif @endforeach</td>
                                            <td>{{$employ->emp_city}}</td>
                                            <td>{{$employ->duration}}</td>
                                            <td style="text-align:center">{{$employ->sch_start_date}}</td>
                                            <td style="text-align:center">{{$employ->sch_end_date}}</td>
                                            <td style="text-align:center"><a class="btn-action btn-view-edit" href="{{route('schedules.edit', $employ->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('schedules.destroy',$employ->id) }}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                "order": [[1, "asc"]],
                dom: 'Bfrtip',
                "columnDefs": [{
                    "searchable": false,
                    "orderable": true,
                    "targets": 0
                }],
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                        //titleAttr: 'Copy',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        //   titleAttr: 'PDF',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h1').text(),
                        exportOptions: {
                            columns: ':not(.no-print)'
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
            $("#choice").on("change", function () {
                var _val = $(this).val();
                if (_val == 'Weekly') {
                    table.columns(4).search(_val).draw();
                    table
                        .columns(4)
                        .search('^(?:(?!Bi-Weekly).)*$\r?\n?', true, false)
                        .draw();
                } else if (_val == 'Bi-Weekly') {
                    table.columns(4).search(_val).draw();
                    table
                        .columns(4)
                        .search('^(?:(?!Weekly).)*$\r?\n?', true, false)
                        .draw();
                } else if (_val == 'Monthly') {
                    table.columns(4).search(_val).draw();
                    table
                        .columns(4)
                        .search('^(?:(?!Bi-Weekly).)*$\r?\n?', true, false)
                        .draw();
                    table
                        .columns(4)
                        .search('^(?:(?!Weekly).)*$\r?\n?', true, false)
                        .draw();
                } else {
                    table.columns().search('').draw();
                }
            })
        });
    </script>
@endsection()