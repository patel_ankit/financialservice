@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .active-green-color-bg {
            background: #689203 !important;
        }

        .dataTables_filter {
            display: none;
        }

        .dt-buttons {
            margin-bottom: 10px;
            position: absolute;
            margin-top: -42px;
            margin-left: 849px;
        }

        .search-btn {
            position: absolute;
            top: 9px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        .page-title {
            /*padding:8px 15px !important;*/
        }

        .dt-buttons {
            display: none;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title">
            <div class="row col-md-12" style="padding-right:0px !important;">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of Client</h1>
                </div>
                <div class="col-md-5" style="padding-right:0px !important;text-align:right;">

                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="col-md-8" style="height:32px;">
                                <div class="row">
                                    <div class="col-md-1" style="width: 7% !important;padding-right:0px;"><label style="margin-top: 11px;">Filter: </label></div>
                                    <div class="col-md-3" style="width: 130px;padding-right:0px;">
                                        <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                                            <option value="1">All</option>
                                            <option value="2" selected>Active</option>
                                            <option value="3">Approval</option>
                                            <option value="4">Pending</option>
                                            <option value="5">Hold</option>
                                            <option value="6">Inactive</option>

                                        </select>
                                    </div>


                                    <!--   <div class="col-md-3">
                                            <div class="col-md-"><label style="margin-top: 11px;">Business : </label></div>
                                         <select name="choice1" style="width: 92%;margin-left: 4px;" id="choice1" class="form-control">
                                 <option value="1">All</option>
                                 <option value="Business">Business</option>
                                 <option value="Investor">Investor</option>
                                 <option value="Non-Profit Organization">Non-Profit Organization</option>
                                 <option value="Personal">Personal</option>
                                 <option value="Profession">Profession</option>
                                 <option value="Service Industry">Service Industry</option>

                             </select>

                             </div>-->


                                    <div class="col-md-1" style="padding:0px;"><label style="margin-left:28%;margin-top: 11px;">Search: </label></div>
                                    <div class="col-md-4" style="width: 160px; padding-right:0px;">
                                        <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
                                            <option value="All" selected>All</option>
                                            <option value="Type">Client ID</option>
                                            <option value="EE / User ID">Company Name</option>
                                            <option value="Employee Name">Bussiness Name</option>
                                            <option value="Email ID">Bussiness Telephone</option>
                                            <option value="Tel. Number">Contact Name</option>
                                            <option value="Contact Telephone">Contact Telephone</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">

                                        <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
                                            <tbody>
                                            <tr id="filter_global">
                                                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col2" data-column="1" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col3" data-column="2" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col4" data-column="3" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col5" data-column="4" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col6" data-column="5" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col7" data-column="6" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="width:110px !important;">Client No</th>
                                        <th style="width:160px !important;"> Client <span style="font-size: 13px;">(Legal Name)</span><br>DBA <span style="font-size: 13px;">(Business Name)</span></th>
                                        <th>Contact Name</th>
                                        <th width="10%">Telephone</th>
                                        <th>Type of Business</th>


                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($common as $com)
                                        <tr @if($com->business_id=='6') style="background:#b9f0b2" @endif>
                                            <td style="text-align:center;">{{$loop->index+1}}</td>
                                            <td>{{$com->filename}}</td>
                                        <!--<td>{{$com->company_name}}</td>-->

                                            <td>@if(empty($com->company_name)) {{$com->first_name}} {{$com->middle_name}} {{$com->last_name}} @else{{$com->company_name}}
                                                <br>{{$com->business_name1}} @endif</td>
                                        <!--<td>{{$com->first_name}} {{$com->middle_name}} {{$com->last_name}}</td>-->
                                            <td>{{$com->firstname}} {{$com->middlename}} {{$com->lastname}}</td>
                                            <td>{{$com->business_no}}</td>
                                        <!--<td>{{$com->bussiness_name}}</td>-->

                                            <td><?php if ($com->business_id == '6') {
                                                    echo 'Personal';
                                                } else {
                                                    echo $com->business_cat_name;
                                                }?></td>
                                            <td style="text-align:center"> @if($com->status=='Active')<a style="display:none" class="btn-action btn_edit btn-view-edit" href="#">2</a> <a style="text-align:center" class="btn-action btn_edit btn-view-edit" href="#">Active</a> @elseif($com->status=='Approval')<a style="display:none" class="btn-action btn_approval btn-view-edit" href="#">3</a><a style="background-color: Yellow !important; text-align:center;color:#000 !important" class="btn-action btn_approval btn-view-edit" href="#">Approval</a>  @elseif($com->status=='Pending')<a style="display:none" class="btn-action btn_pending btn-view-edit" href="#">4</a><a style="background-color: Orange !important; text-align:center" class="btn-action btn_pending btn-view-edit" href="#">Pending</a> @elseif($com->status=='Hold')<a style="display:none" class="btn-action btn_hold btn-view-edit"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           href="#">5</a> <a class="btn-action btn_hold btn-delete"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             href="#">Hold</a> @else <a style="display:none"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        class="btn-action btn_inactive btn-view-edit"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        href="#">6</a> <a
                                                        class="btn-action btn_inactive btn-delete" href="#">Inactive</a>  @endif</td>
                                            <td style="text-align:center;">@if($com->newclient==1)<a class="" href="{{route('clients.edit',$com->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif
                                                <a class="btn-action btn-view-edit" href="{{route('clients.edit',$com->cid)}}"><i class="fa fa-edit"></i></a>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')) {event.preventDefault();document.getElementById('delete-id-{{$com->cid}}').submit();} else{event.preventDefault();}" href="{{route('clients.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('clients.destroy',$com->cid) }}" method="post" style="display:none" id="delete-id-{{$com->cid}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                dom: 'Bfrtlip',
                "pagingType": "input",
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> ',
                        //titleAttr: 'Copy',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        // titleAttr: 'Excel',
                        title: $('h1').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> ',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy

                            doc.pageMargins = [20, 60, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'left',
                                        image: logo,
                                        width: 50, margin: [200, 5]
                                    }, {
                                        alignment: 'CENTER',
                                        text: 'List of FSC Email / Telephone Extension',
                                        fontSize: 20,
                                        margin: [10, 35],
                                    },],
                                    margin: [20, 0, 0, 12], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        title: $('h1').text(),
                        exportOptions: {
                            columns: ':not(.no-print)'
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": true,
                    "targets": 0
                }],
                "order": [[1, 'asc']]
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });

        function filterGlobal() {
            $('#example').DataTable().search(
                $('#global_filter').val(),
                $('#global_regex').prop('checked'),
                $('#global_smart').prop('checked')
            ).draw();
        }

        function filterColumn(i) {
            $('#example').DataTable().column(i).search(
                $('#col' + i + '_filter').val(),
                $('#col' + i + '_regex').prop('checked'),
                $('#col' + i + '_smart').prop('checked')
            ).draw();
        }

        $("#types").on('change', function () {
            // For unique choice
            var selVal = $("#types option:selected").val();
            if (selVal == 'Type') {
                $('#filter_global').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col7').hide();
                $('#filter_col2').show();
            } else if (selVal == 'EE / User ID') {
                $('#filter_col7').hide();
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col3').show();
            } else if (selVal == 'Employee Name') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col4').show();
                $('#filter_col7').hide();
            } else if (selVal == 'Email ID') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col7').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col6').hide();
                $('#filter_col5').show();
            } else if (selVal == 'Tel. Number') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col7').hide();
                $('#filter_col6').show();
            } else if (selVal == 'Tel. Number') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col7').show();
            } else {
                $('#filter_global').show();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col7').hide();
                $('#filter_col2').hide();
            }
        });
    </script>

@endsection()