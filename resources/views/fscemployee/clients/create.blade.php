@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .star-required {
            color: red
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .nav-tabs > li {
            width: 165px;
        }
    </style>
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Client </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="float: left;">
                    <div class="card-body">

                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active"><a href="#tab1primary" data-toggle="tab">Basic Information</a></li>
                                    <li><a href="#tab4primary" data-toggle="tab">Contact Information</a></li>
                                    <li><a href="#tab2primary" data-toggle="tab">Location Information</a></li>
                                    <li><a href="#tab3primary" data-toggle="tab">Formation Information</a></li>

                                    <li><a href="#tab5primary" data-toggle="tab">Taxation Information</a></li>
                                    <li><a href="#tab6primary" data-toggle="tab">Service Information</a></li>
                                    <li><a href="#tab7primary" data-toggle="tab">Special Notes</a></li>
                                    <li><a href="#tab8primary" data-toggle="tab">License Information</a></li>
                                </ul>
                            </div>
                            <form method="post" action="{{route('clientsetup.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('filename') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">File No : </label>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" onkeyup="checkemail();" maxlength="10" id="filename" name="filename" value="" placeholder="">
                                                        <div id="email_status"></div>
                                                        @if ($errors->has('filename'))<span class="help-block">
                                       <strong>{{ $errors->first('filename') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" style="float: right;">Status : </label>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="status" id="status" class="form-control fsc-input">
                                                            <option value="">Status</option>
                                                            <option value="Approval">Approval</option>
                                                            <option value="Hold">Hold</option>
                                                            <option value="Pending">Pending</option>
                                                        </select>
                                                        @if ($errors->has('status'))<span class="help-block">
                                       <strong>{{ $errors->first('status') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_name') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Type : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="business_id" id="business_id" class="form-control fsc-input category">
                                                            <option value=''>---Select Business Name---</option>
                                                            @foreach($business as $busi)
                                                                <option value='{{$busi->id}}'>{{$busi->bussiness_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('business_id'))<span class="help-block">
                                       <strong>{{ $errors->first('business_id') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Category Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">
                                                            <option value=''>---Select Business Category Name---</option>
                                                            @foreach($category as $cate)
                                                                <option value='{{$cate->id}}'>{{$cate->business_cat_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('business_catagory_name'))<span class="help-block">
                                       <strong>{{ $errors->first('business_catagory_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('company_name') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Company name : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="company_name" id="company_name" value="">
                                                        @if ($errors->has('company_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('company_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('first_name') ? ' has-error' : '' }} {{ $errors->has('middle_name') ? ' has-error' : '' }} {{ $errors->has('last_name') ? ' has-error' : '' }}" style="margin-top:1%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Name : </label>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input txtOnly " id="first_name" name="first_name" placeholder="First Name" value="">
                                                        @if ($errors->has('first_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('first_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input txtOnly " id="middle_name" name="middle_name" placeholder="M" value="">
                                                        @if ($errors->has('middle_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('middle_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input txtOnly " id="last_name" name="last_name" placeholder="Last Name" value="">
                                                        @if ($errors->has('last_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('last_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_name') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business name : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business_name" id="business_name" value="">
                                                        @if ($errors->has('business_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('legalname') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Legal name : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="legalname" id="legalname" value="">
                                                        @if ($errors->has('legalname'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('legalname') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('dbaname') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">DBA Name : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="dbaname" id="dbaname" value="">
                                                        @if($errors->has('dbaname'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('dbaname') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Address : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="address" id="address">
                                                        @if($errors->has('address'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('address') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address1') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Address 1: </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="address1" id="address1">
                                                        @if($errors->has('address1'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('address1') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="">
                                                                @if($errors->has('city'))
                                                                    <span class="help-block">
                                             <strong>{{ $errors->first('city') }}</strong>
                                             </span>
                                                                @endif
                                                            </div>

                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="stateId" id="stateId" class="form-control fsc-input">
                                                                        <option value="">State</option>
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    @if($errors->has('stateId'))
                                                                        <span class="help-block">
                                                <strong>{{ $errors->first('stateId') }}</strong>
                                                </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="zip" name="zip" value="" placeholder="Zip">
                                                                @if($errors->has('zip'))
                                                                    <span class="help-block">
                                             <strong>{{ $errors->first('zip') }}</strong>
                                             </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  {{ $errors->has('countryId') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Country: </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown">
                                                                    <select name="countryId" id="countryId" class="form-control fsc-input">
                                                                        <option value="">select</option>
                                                                        <option value="USA">USA</option>
                                                                    </select>
                                                                    @if($errors->has('countryId'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('countryId') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Select Address: </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <label class="fsc-form-label"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)"> Same As Above Address</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('second_address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Mailing Address: </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id=second_address" name="second_address" placeholder="Mailing Address">
                                                        @if($errors->has('second_address'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('second_address') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('second_address1') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Mailing Address 1: </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="second_address1" name="second_address1" placeholder="Mailing Address 1">
                                                        @if($errors->has('second_address1'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('second_address1') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('second_city') ? 'has-error' : ''}} {{ $errors->has('second_state') ? 'has-error' : ''}}{{ $errors->has('second_zip') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="second_city" name="second_city" placeholder="City">
                                                                @if($errors->has('second_city'))
                                                                    <span class="help-block">
                                       <strong>{{ $errors->first('second_city') }}</strong>
                                       </span>
                                                                @endif
                                                            </div>

                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="second_state" id="second_state" class="form-control fsc-input">
                                                                        <option value="">State</option>
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    @if($errors->has('second_state'))
                                                                        <span class="help-block">
                                          <strong>{{ $errors->first('second_state') }}</strong>
                                          </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="second_zip" name="second_zip" placeholder="Zip">
                                                                @if($errors->has('second_zip'))
                                                                    <span class="help-block">
                                       <strong>{{ $errors->first('second_zip') }}</strong>
                                       </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('email') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Email : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="abc@abc.com" value="">
                                                        @if($errors->has('email'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('busimobile_noness_no') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Mobile / Cell No. : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="mobile_no" value="" name="mobile_no" placeholder="Business Telephone" onkeypress="PutNo()">
                                                        @if($errors->has('mobile_no'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('mobile_no') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_no') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Telephone # : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="business_no" value="" name="business_no" placeholder="Business Telephone" onkeypress="PutNo()">
                                                        @if($errors->has('business_no'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_no') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_fax') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Fax # : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input business_fax1" value="" id="business_fax" name="business_fax" placeholder="Business Fax">
                                                        @if($errors->has('business_fax'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_fax') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('website') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Website : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="website" value="" name="website" placeholder="Website address">
                                                        @if($errors->has('website'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('website') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab2primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Type of Business : </label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                       <span class="category1">
                                       <input class=" form-control fsc-input" readonly value='' name="user_type" id="user_type">
                                       </span>
                                                    </div>


                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label pull-right">Brand : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="business_brand_name" id="business_brand_name" class="form-control fsc-input">
                                                            <option value=''>Brand</option>
                                                            @foreach($businessbrand as $bb)
                                                                <option value='{{$bb->id}}'>{{$bb->business_brand_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('business_brand_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_brand_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>

                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_brand_category_name') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Brand Category Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="business_brand_category_name" id="business_brand_category_name" class="form-control fsc-input">
                                                            <option value=''>---Select Business Brand Category Name---</option>
                                                            @foreach($cb as $bb1)
                                                                <option value='{{$bb1->id}}'>{{$bb1->business_brand_category_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('business_brand_category_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_brand_category_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('dbaname') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Store Name : </label>
                                                    </div>
                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business_store_name" id="business_store_name" value=""> @if($errors->has('business_store_name'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_store_name') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Address : </label>
                                                    </div>
                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business_address" id="business_address" value="">@if($errors->has('business_address'))
                                                            <span class="help-block">
                                       <strong>{{ $errors->first('business_address') }}</strong>
                                       </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_city') ? 'has-error' : ''}} {{ $errors->has('business_country') ? 'has-error' : ''}} {{ $errors->has('bussiness_zip') ? 'has-error' : ''}} {{ $errors->has('business_country') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / County / State / Zip : </label>
                                                    </div>
                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="business_city" name="business_city" placeholder="city" value="">
                                                                @if($errors->has('business_city'))
                                                                    <span class="help-block">
                                             <strong>{{ $errors->first('business_city') }}</strong>
                                             </span>
                                                                @endif
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown">
                                                                    <select name="business_country" id="business_country" class="form-control fsc-input">
                                                                        <option value="">Country</option>
                                                                        <option value='USA'>USA</option>
                                                                    </select>
                                                                    @if ($errors->has('business_country'))<span class="help-block">
                                       <strong>{{ $errors->first('business_country') }}</strong>
                                       </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="business_state" id="business_state" class="form-control fsc-input">
                                                                        <option value="">State</option>
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    @if ($errors->has('business_state'))<span class="help-block">
                                       <strong>{{ $errors->first('business_state') }}</strong>
                                       </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="bussiness_zip" name="bussiness_zip" value="" placeholder="Zip">
                                                                @if ($errors->has('bussiness_zip'))<span class="help-block">
                                       <strong>{{ $errors->first('bussiness_zip') }}</strong>
                                       </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3primary">Coming Soon</div>
                                        <div class="tab-pane fade" id="tab4primary">
                                            <div class="pull-right">


                                                <a href="javascript:void(0)" class="addMore pull-right"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Field</a>

                                            </div>
                                            <div class="fieldGroup">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Contact Person Name : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="contact_person_name[]" id="contact_person_name" value=""><input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone No. : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input telephone" name="telephone[]" id="telephone" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business[]" id="business" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Fax : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" id="business_fax1" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Residence : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="residence[]" id="residence" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Cell : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Residence Fax : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Email: </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="">
                                                    </div>

                                                </div>
                                                <br>
                                                <br>

                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab5primary">Coming Soon</div>

                                        <div class="tab-pane fade" id="tab7primary">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group {{ $errors->has('level') ? 'has-error' : ''}}">
                                                        <label class="control-label col-md-4">Level :</label>
                                                        <div class="col-md-5">
                                                            <select name="level" id="level" class="form-control" onchange="showDiv(this)">
                                                                <option value="">---Select Level---</option>
                                                                <option value="Federal">Federal</option>
                                                                <option value="State">State</option>
                                                                <option value="County">County</option>
                                                                <option value="Local">Local</option>
                                                            </select>
                                                            @if ($errors->has('level'))<span class="help-block">
                                       <strong>{{ $errors->first('level') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('setup_state') ? 'has-error' : ''}}" id="hidden_div">
                                                        <label class="control-label col-md-4">State :</label>
                                                        <div class="col-md-5">
                                                            <select name="setup_state" id="setup_state" class="form-control">
                                                                <option value=''>---Select State---</option>
                                                                <option value='AK'>AK</option>
                                                                <option value='AS'>AS</option>
                                                                <option value='AZ'>AZ</option>
                                                                <option value='AR'>AR</option>
                                                                <option value='CA'>CA</option>
                                                                <option value='CO'>CO</option>
                                                                <option value='CT'>CT</option>
                                                                <option value='DE'>DE</option>
                                                                <option value='DC'>DC</option>
                                                                <option value='FM'>FM</option>
                                                                <option value='FL'>FL</option>
                                                                <option value='GA'>GA</option>
                                                                <option value='GU'>GU</option>
                                                                <option value='HI'>HI</option>
                                                                <option value='ID'>ID</option>
                                                                <option value='IL'>IL</option>
                                                                <option value='IN'>IN</option>
                                                                <option value='IA'>IA</option>
                                                                <option value='KS'>KS</option>
                                                                <option value='KY'>KY</option>
                                                                <option value='LA'>LA</option>
                                                                <option value='ME'>ME</option>
                                                                <option value='MH'>MH</option>
                                                                <option value='MD'>MD</option>
                                                                <option value='MA'>MA</option>
                                                                <option value='MI'>MI</option>
                                                                <option value='MN'>MN</option>
                                                                <option value='MS'>MS</option>
                                                                <option value='MO'>MO</option>
                                                                <option value='MT'>MT</option>
                                                                <option value='NE'>NE</option>
                                                                <option value='NV'>NV</option>
                                                                <option value='NH'>NH</option>
                                                                <option value='NJ'>NJ</option>
                                                                <option value='NM'>NM</option>
                                                                <option value='NY'>NY</option>
                                                                <option value='NC'>NC</option>
                                                                <option value='ND'>ND</option>
                                                                <option value='MP'>MP</option>
                                                                <option value='OH'>OH</option>
                                                                <option value='OK'>OK</option>
                                                                <option value='OR'>OR</option>
                                                                <option value='PW'>PW</option>
                                                                <option value='PA'>PA</option>
                                                                <option value='PR'>PR</option>
                                                                <option value='RI'>RI</option>
                                                                <option value='SC'>SC</option>
                                                                <option value='SD'>SD</option>
                                                                <option value='TN'>TN</option>
                                                                <option value='TX'>TX</option>
                                                                <option value='UT'>UT</option>
                                                                <option value='VT'>VT</option>
                                                                <option value='VI'>VI</option>
                                                                <option value='VA'>VA</option>
                                                                <option value='WA'>WA</option>
                                                                <option value='WV'>WV</option>
                                                                <option value='WI'>WI</option>
                                                                <option value='WY'>WY</option>
                                                            </select>
                                                            @if ($errors->has('setup_state'))<span class="help-block">
                                       <strong>{{ $errors->first('setup_state') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group  {{ $errors->has('county_name') ? 'has-error' : ''}}">
                                                        <label class="control-label col-md-4">County Name :</label>
                                                        <div class="col-md-5">
                                                            <input type="text" name="county_name" id="county_name" value="" class="form-control">
                                                            @if ($errors->has('county_name'))<span class="help-block">
                                       <strong>{{ $errors->first('county_name') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group  {{ $errors->has('county_no') ? 'has-error' : ''}}">
                                                        <label class="control-label col-md-4">County No :</label>
                                                        <div class="col-md-5">
                                                            <input type="text" value="" name="county_no" id="county_no" class="form-control">
                                                            @if ($errors->has('county_no'))<span class="help-block">
                                       <strong>{{ $errors->first('county_no') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group {{ $errors->has('type_of_activity') ? 'has-error' : ''}}">
                                                        <label class="control-label col-md-4">Type Of Activity:</label>
                                                        <div class="col-md-5">
                                                            <select name="type_of_activity" value="" type="text" id="type_of_activity" class="form-control">
                                                                <option value="">---Select Type Of Activity---</option>
                                                                <option value="Registration">Registration</option>
                                                                <option value="Income Tax">Income Tax</option>
                                                                <option value="Sales Tax">Sales Tax</option>
                                                                <option value="License">License</option>
                                                                <option value="Tobacco Tax">Tobacco Tax</option>
                                                                <option value="Alochol Tax">Alochol Tax</option>
                                                                <option value="COAM ">COAM</option>
                                                                <option value="Check Cashing">Check Cashing</option>
                                                            </select>
                                                            @if ($errors->has('type_of_activity'))<span class="help-block">
                                       <strong>{{ $errors->first('type_of_activity') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Department:</label>
                                                        <div class="col-md-5">
                                                            <input name="department" value="" type="text" id="department" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Due Date:</label>
                                                        <div class="col-md-5">
                                                            <select name="due_date" value="" type="text" id="due_date" class="form-control">
                                                                <?php
                                                                $var = "01/04/" . '' . date('Y');
                                                                $newdate = date('d/m/Y');
                                                                if (strtotime($newdate) <= strtotime($var) )
                                                                {
                                                                ?>
                                                                <option value="<?php echo $var;?>"><?php echo $var;?></option>
                                                                <?php
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                <option value="{{ date('m/d/Y', strtotime('+1 year', strtotime($var)))}}">{{ date('m/d/Y', strtotime('+1 year', strtotime($var)))}}</option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                                    <!-- <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>-->
                                    <div class="col-lg-4 col-lg-offset-4 col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="btn btn-primary fsc-form-submit">Save</button>
                                        <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn btn-primary fsc-form-submit">Cancel</a>
                                    </div>
                                    <!--                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- copy of input fields group -->
    <div class="form-group fieldGroupCopy" style="display: none;">
        <div class="input-group">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Contact Person Name : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="contact_person_name[]" id="contact_person_name" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Telephone No. : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input telephone" name="telephone[]" id="telephone" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Business : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="business[]" id="business" value="">
                </div>
            </div>
            <input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Business Fax : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" id="business_fax1" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Residence : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="residence[]" id="residence" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Cell : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Residence Fax : </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Email: </label>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="">
                </div>
                <div class="pull-right" style="margin-top: 10px;">
                    <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("#business_no").mask("(999) 999-9999");
        $(".ext").mask("999");
        $("#business_fax").mask("(999) 999-9999");
        $(".business_fax1").mask("(999) 999-9999");
        $(".residence_fax").mask("(999) 999-9999");
        $("#mobile_no").mask("(999) 999-9999");
        $(".cell").mask("(999) 999-9999");
        $(".telephone").mask("(999) 999-9999");
        $(".usapfax").mask("(999) 999-9999");
        $("#zip").mask("9999");
        $("#second_zip").mask("9999");
        $("#bussiness_zip").mask("9999");
    </script>

    <script>
        $(document).ready(function () {
            //group add limit
            var maxGroup = 3;

            //add more fields group
            $(".addMore").click(function () {
                if ($('body').find('.fieldGroup').length < maxGroup) {
                    var fieldHTML = '<div class="form-group fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maximum ' + maxGroup + ' Persons are allowed.');
                }
            });

            //remove fields group
            $("body").on("click", ".remove", function () {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
    <script>
        function FillBilling(f) {
            if (f.billingtoo.checked == true) {
                f.second_address.value = f.address.value;
                f.second_address1.value = f.address1.value;
            }
        }
    </script>
    <script>
        function showDiv(elem) {
            if (elem.value == 'Federal') {
                document.getElementById('hidden_div').style.display = "none";
            } else {
                document.getElementById('hidden_div').style.display = "block";
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getRequest')!!}?id=' + id, function (data) {
                    $('#business_catagory_name').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#business_catagory_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                    })

                });

            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getcat')!!}?id=' + id, function (data) {
                    $('#user_type').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#user_type').val(subcatobj.bussiness_name);
                    })

                });

            });
        });
    </script>
    <script type="text/javascript">
        function checkemail() {
            var client_id = document.getElementById("filename").value;
            //alert(client_id);
            if (client_id) {
                $.ajax({
                    type: 'get',
                    url: '{!!URL::to('/getclick')!!}',
                    data: {
                        client_id: client_id,
                    },
                    success: function (response) {
                        $('#email_status').html(response);
                        if (response == "OK") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            } else {
                $('#email_status').html("");
                return false;
            }
        }

        function checkall() {
            var emailhtml = document.getElementById("email_status").innerHTML;

            if ((emailhtml) == "OK") {
                return true;
            } else {
                return false;
            }
        }

    </script>

@endsection()