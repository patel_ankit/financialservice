@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        .table > thead > tr > th {
            background: #ffff99;
            border-bottom: 8px solid #993366;
            text-align: center;
        }

        label {
            float: left;
        }

        .dt-buttons {
            margin-bottom: 10px;
        }

        .page-title {
            padding: 8px 15px;

        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12" style="margin-top:5px;padding-right:0px;">
                <div class="col-md-7" style="text-align:right;">
                    <h2>List of Task</h2> <h6 style="display:none">List of Task</h6>
                </div>
                <div class="col-md-5" style="text-align:right;padding-right:0px;">
                    <h2>Add / View / Edit</h2>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right" style="position: absolute;margin-top: 27px;margin-right: 150px;z-index:9999;">
                                <div class="table-title">
                                    <a href="{{url('fscemployee/task/create')}}">Add New Task</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body col-md-12">

                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">

                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="width:10%">Creation Date</th>
                                        <th>Employee / User</th>

                                        <th>Subject</th>
                                        <th style="width:33%">Description</th>
                                        <th>Priority</th>
                                        <th>Send By</th>
                                        <th>Status</th>
                                        <th style="width:8%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($task as $com)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>@if($com->creattiondate !='') {!! date('m-d-Y',strtotime($com->creattiondate)) !!}@endif</td>

                                            <td> @foreach($emp as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->name)}} @endif @endforeach</td>
                                        <!--<td>{{$com->firstName}} </td>-->

                                            <td>{{$com->title}}</td>
                                            <td>{!! $com->content !!}</td>
                                            <td>{!! $com->priority !!}</td>
                                            <td>@if($com->admin_id==Auth::user()->id) {{Auth::user()->fname.' '.Auth::user()->lname}} @endif
                                                @foreach($emp as $com1) @if($com->admin_id==$com1->id) {{ucwords($com1->name)}} @endif @endforeach</td>
                                            <td> @if($com->status==2) In Progress @endif  @if($com->status==1) Start @endif @if($com->status==3) End @endif</td>
                                            <td>


                                                <a class="btn-action btn-view-edit" href="{{url('fscemployee/task',$com->id)}}/edit"><i class="fa fa-edit"></i></a>

                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('task.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                dom: 'Bfrtip',
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                }],
                "order": [[0, 'asc']],
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                        titleAttr: 'Copy',
                        title: $('h3').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        titleAttr: 'Excel',
                        title: $('h3').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c', sheet).attr('s', '51');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        titleAttr: 'CSV',
                        title: $('h3').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                            doc.pageMargins = [20, 60, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'left',
                                        image: logo,
                                        width: 50, margin: [200, 5]
                                    }, {
                                        alignment: 'CENTER',
                                        text: 'List of Client',
                                        fontSize: 20,
                                        margin: [10, 35],
                                    },],
                                    margin: [20, 0, 0, 12], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        titleAttr: 'PDF',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        titleAttr: 'Print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src=""/></center>'
                                );
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },
                        footer: true,
                        autoPrint: true
                    },],
            });
            $('input.global_filter').on('keyup click', function () {
                filterGlobal();
            });

            $('input.column_filter').on('keyup click', function () {
                filterColumn($(this).parents('tr').attr('data-column'));
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
            table.columns(6)
                .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                .draw();
            $("#choice").on("change", function () {
                var _val = $(this).val();//alert(_val);

                if (_val == 'Inactive') {
                    table.columns(6).search(_val).draw();
                } else if (_val == 'New') {
                    table.columns(6).search(_val).draw();
                } else if (_val == 'Active') {  //alert();
                    table.columns(6).search(_val).draw();
                    table.columns(6)
                        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                        .draw();
                } else {
                    table
                        .columns()
                        .search('')
                        .draw();
                }
            })
        });
    </script>
@endsection()