@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        ul li {
            list-style: decimal;
        }

        /*.Branch h1 {padding: 0px 0 0 10px;float: left;font-size: 15px;font-weight: bold;}*/
        .input-daterange {
            text-align: left;
        }

        .input-daterange input:last-child {
            text-align: left;
            border-radius: 3px;
        }

        .input-daterange input:first-child {
            text-align: left;
            border-radius: 3px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            margin-top: 3px;
        }

        /*.Branch {width: 100%;float: left;margin: 3px 0 20px 0;text-align: center;background: transparent;border: transparent;padding: 4px 0;}*/
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #000;
            line-height: 28px;
            font-weight: bold;
        }

        .select2-container .select2-selection--single {
            height: 36px;
            padding: 2px 0;
            border: 2px solid #286db5 !important;
        }

        .datepicker {
            z-index: 99999 !important;
        }

        .ui-timepicker-container {
            z-index: 999999 !important
        }

        .nav-tabs > li > a {
            margin-right: 0px;
            color: #000 !important;
            font-size: 16px !important;
        }

        .nav-tabs > li {
            width: 24% !important;
            margin: 2px 0 0 8px;
            border: 1px solid #ccc;
            background: #fff;
            border-radius: 8px;
        }

        .select2-container .select2-selection--single {
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2 !important;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;
            background-color: transparent !important;
        }

        td.fc-day {
            background: #FFF !important;
            font-family: 'Roboto', sans-serif;
        }

        td.fc-today {
            background: #FFF !important;
            position: relative;
        }

        .fc-first th {
            font-family: 'Roboto', sans-serif;
            background: #9675ce !important;
            color: #FFF;
            font-size: 14px !important;
            font-weight: 500 !important;

        }

        .fc-event-inner {
            font-family: 'Roboto', sans-serif;
            background: #03a9f3 !important;
            color: #FFF !important;
            font-size: 12px !important;
            font-weight: 500 !important;
            padding: 5px 0px !important;
        }

        .fc {
            direction: ltr;
            text-align: left;
        }

        .fc table {
            border-collapse: collapse;
            border-spacing: 0;
        }


        html .fc,
        .fc table {
            font-size: 1em;
            font-family: "Helvetica Neue", Helvetica;

        }

        .fc td,
        .fc th {
            padding: 0;
            vertical-align: top;
        }

        .fc-header td {
            white-space: nowrap;
            padding: 15px 10px 0px;
        }

        .fc-header-left {
            width: 25%;
            text-align: left;
        }

        .fc-header-center {
            text-align: center;
        }

        .fc-header-right {
            width: 25%;
            text-align: right;
        }

        .fc-header-title {
            display: inline-block;
            vertical-align: top;
            margin-top: -5px;
        }

        .fc-header-title h2 {
            margin-top: 8px;
            white-space: nowrap;
            font-size: 20Px;
            font-weight: 100;
            margin-bottom: 10px;
            font-family: 'Roboto', sans-serif;
        }

        span.fc-button {
            font-family: 'Roboto', sans-serif;
            border-color: #9675ce;
            color: #9675ce;
        }

        .fc-state-down, .fc-state-active {
            background-color: #9675ce !important;
            color: #FFF !important;
        }

        .fc .fc-header-space {
            padding-left: 10px;
        }

        .fc-header .fc-button {
            margin-bottom: 1em;
            vertical-align: top;
        }

        /* buttons edges butting together */

        .fc-header .fc-button {
            margin-right: -1px;
        }

        .fc-header .fc-corner-right, /* non-theme */
        .fc-header .ui-corner-right { /* theme */
            margin-right: 0; /* back to normal */
        }

        /* button layering (for border precedence) */

        .fc-header .fc-state-hover,
        .fc-header .ui-state-hover {
            z-index: 2;
        }

        .fc-header .fc-state-down {
            z-index: 3;
        }

        .fc-header .fc-state-active,
        .fc-header .ui-state-active {
            z-index: 4;
        }

        /* Content
        ------------------------------------------------------------------------*/
        .fc-content {
            clear: both;
            zoom: 1; /* for IE7, gives accurate coordinates for [un]freezeContentHeight */
        }

        .fc-view {
            width: 100%;
            overflow: hidden;
        }

        th.fc-day-header.fc-widget-header {
            background: #9675ce;
            color: #ffffff;
            line-height: 30px;
        }

        td.fc-day-top {
            height: 100px !important;
        }

        .fc-today .fc-day-number {
            background-color: #ff3b30;
            color: #FFFFFF;
            border-radius: 50%;
            font-size: 18px;
            margin: 2px;
            padding: 4px;
            width: 25px;
            height: 25px;
        }

        /* Cell Styles
        ------------------------------------------------------------------------*/

        /* <th>, usually */
        .fc-widget-content { /* <td>, usually */
            border: 1px solid #e5e5e5;
        }

        .fc-widget-header {
            border-bottom: 1px solid #EEE;
        }

        .fc-state-highlight { /* <td> today cell */ /* TODO: add .fc-today to <th> */
            /* background: #fcf8e3; */
        }

        .fc-state-highlight > div > div.fc-day-number {
            background-color: #ff3b30;
            color: #FFFFFF;
            border-radius: 50%;
            margin: 4px;
        }

        .fc-cell-overlay { /* semi-transparent rectangle while dragging */
            background: #bce8f1;
            opacity: .3;
            filter: alpha(opacity=30); /* for IE */
        }

        .fc-button {
            position: relative;
            display: inline-block;
            padding: 0 .6em;
            overflow: hidden;
            height: 1.9em;
            line-height: 1.9em;
            white-space: nowrap;
            cursor: pointer;
        }

        .fc-state-default { /* non-theme */
            border: 1px solid;
        }

        .fc-state-default.fc-corner-left { /* non-theme */
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .fc-state-default.fc-corner-right { /* non-theme */
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .fc-text-arrow {
            margin: 0 .4em;
            font-size: 2em;
            line-height: 23px;
            vertical-align: baseline; /* for IE7 */
        }

        .fc-button-prev .fc-text-arrow,
        .fc-button-next .fc-text-arrow { /* for ‹ › */
            font-weight: bold;
        }

        .fc-button .fc-icon-wrap {
            position: relative;
            float: left;
            top: 50%;
        }

        .fc-button .ui-icon {
            position: relative;
            float: left;
            margin-top: -50%;

            *margin-top: 0;
            *top: -50%;
        }


        .fc-state-default {
            border-color: #ff3b30;
            color: #ff3b30;
        }

        .fc-button-month.fc-state-default, .fc-button-agendaWeek.fc-state-default, .fc-button-agendaDay.fc-state-default {
            min-width: 67px;
            text-align: center;
            transition: all 0.2s;
            -webkit-transition: all 0.2s;
        }

        .fc-state-hover,
        .fc-state-down,
        .fc-state-active,
        .fc-state-disabled {
            color: #333333;
            background-color: #FFE3E3;
        }

        .fc-state-hover {
            color: #ff3b30;
            text-decoration: none;
            background-position: 0 -15px;
            -webkit-transition: background-position 0.1s linear;
            -moz-transition: background-position 0.1s linear;
            -o-transition: background-position 0.1s linear;
            transition: background-position 0.1s linear;
        }

        .fc-state-down,
        .fc-state-active {
            background-color: #ff3b30;
            background-image: none;
            outline: 0;
            color: #FFFFFF;
        }

        .fc-state-disabled {
            cursor: default;
            background-image: none;
            background-color: #FFE3E3;
            filter: alpha(opacity=65);
            box-shadow: none;
            border: 1px solid #FFE3E3;
            color: #ff3b30;
        }

        .fc-event-container > * {
            z-index: 8;
        }

        .fc-event-container > .ui-draggable-dragging,
        .fc-event-container > .ui-resizable-resizing {
            z-index: 9;
        }

        .fc-event {
            border: 1px solid #FFF; /* default BORDER color */
            background-color: #FFF; /* default BACKGROUND color */
            color: #919191; /* default TEXT color */
            font-size: 12px;
            cursor: default;
        }

        .fc-event.chill {
            background-color: #f3dcf8;
        }

        .fc-event.info {
            background-color: #c6ebfe;
        }

        .fc-event.important {
            background-color: #FFBEBE;
        }

        .fc-event.success {
            background-color: #BEFFBF;
        }

        .fc-event:hover {
            opacity: 0.7;
        }

        a.fc-event {
            text-decoration: none;
        }

        a.fc-event,
        .fc-event-draggable {
            cursor: pointer;
        }

        .fc-rtl .fc-event {
            text-align: right;
        }

        .fc-event-inner {
            width: 100%;
            height: 100%;
            overflow: hidden;
            line-height: 15px;
        }

        .fc-event-time,
        .fc-event-title {
            padding: 0 1px;
        }

        .fc .ui-resizable-handle {
            display: block;
            position: absolute;
            z-index: 99999;
            overflow: hidden; /* hacky spaces (IE6/7) */
            font-size: 300%; /* */
            line-height: 50%; /* */
        }

        .fc-event-hori {
            border-width: 1px 0;
            margin-bottom: 1px;
        }

        .fc-ltr .fc-event-hori.fc-event-start,
        .fc-rtl .fc-event-hori.fc-event-end {
            border-left-width: 1px;
            /*
        border-top-left-radius: 3px;
            border-bottom-left-radius: 3px;
        */
        }

        .fc-ltr .fc-event-hori.fc-event-end,
        .fc-rtl .fc-event-hori.fc-event-start {
            border-right-width: 1px;
            /*
        border-top-right-radius: 3px;
            border-bottom-right-radius: 3px;
        */
        }

        .fc-event-hori .ui-resizable-e {
            top: 0 !important; /* importants override pre jquery ui 1.7 styles */
            right: -3px !important;
            width: 7px !important;
            height: 100% !important;
            cursor: e-resize;
        }

        .fc-event-hori .ui-resizable-w {
            top: 0 !important;
            left: -3px !important;
            width: 7px !important;
            height: 100% !important;
            cursor: w-resize;
        }

        .fc-event-hori .ui-resizable-handle {
            _padding-bottom: 14px; /* IE6 had 0 height */
        }

        table.fc-border-separate {
            border-collapse: separate;
        }

        .fc-border-separate th,
        .fc-border-separate td {
            border-width: 1px 0 0 1px;
        }

        .fc-border-separate th.fc-last,
        .fc-border-separate td.fc-last {
            border-right-width: 1px;
        }


        .fc-border-separate tr.fc-last td {

        }

        .fc-border-separate .fc-week .fc-first {
            border-left: 0;
        }

        .fc-border-separate .fc-week .fc-last {
            border-right: 0;
        }

        .fc-border-separate tr.fc-last th {
            border-bottom-width: 1px;
            border-color: #cdcdcd;
            font-size: 16px;
            font-weight: 300;
            line-height: 30px;
        }

        .fc-border-separate tbody tr.fc-first td,
        .fc-border-separate tbody tr.fc-first th {
            border-top-width: 0;
        }

        .fc-grid th {
            text-align: center;
        }

        .fc .fc-week-number {
            width: 22px;
            text-align: center;
        }

        .fc .fc-week-number div {
            padding: 0 2px;
        }

        .fc-grid .fc-day-number {
            float: right;
            padding: 0 2px;
        }

        .fc-grid .fc-other-month .fc-day-number {
            opacity: 0.3;
            filter: alpha(opacity=30); /* for IE */
            /* opacity with small font can sometimes look too faded
               might want to set the 'color' property instead
               making day-numbers bold also fixes the problem */
        }

        .fc-grid .fc-day-content {
            clear: both;
            padding: 2px 2px 1px; /* distance between events and day edges */
        }

        /* event styles */

        .fc-grid .fc-event-time {
            font-weight: bold;
        }

        /* right-to-left */

        .fc-rtl .fc-grid .fc-day-number {
            float: left;
        }

        .fc-rtl .fc-grid .fc-event-time {
            float: right;
        }

        .fc-agenda table {
            border-collapse: separate;
        }

        .fc-agenda-days th {
            text-align: center;
        }

        .fc-agenda .fc-agenda-axis {
            width: 50px;
            padding: 0 4px;
            vertical-align: middle;
            text-align: right;
            white-space: nowrap;
            font-weight: normal;
        }

        .fc-agenda .fc-week-number {
            font-weight: bold;
        }

        .fc-agenda .fc-day-content {
            padding: 2px 2px 1px;
        }

        .fc-agenda-days .fc-agenda-axis {
            border-right-width: 1px;
        }

        .fc-agenda-days .fc-col0 {
            border-left-width: 0;
        }

        /* all-day area */

        .fc-agenda-allday th {
            border-width: 0 1px;
        }

        .fc-agenda-allday .fc-day-content {
            min-height: 34px; /* TODO: doesnt work well in quirksmode */
            _height: 34px;
        }

        /* divider (between all-day and slots) */

        .fc-agenda-divider-inner {
            height: 2px;
            overflow: hidden;
        }

        .fc-widget-header .fc-agenda-divider-inner {
            background: #eee;
        }

        /* slot rows */

        .fc-agenda-slots th {
            border-width: 1px 1px 0;
        }

        .fc-agenda-slots td {
            border-width: 1px 0 0;
            background: none;
        }

        .fc-agenda-slots td div {
            height: 20px;
        }

        .fc-agenda-slots tr.fc-slot0 th,
        .fc-agenda-slots tr.fc-slot0 td {
            border-top-width: 0;
        }

        .fc-agenda-slots tr.fc-minor th.ui-widget-header {
            *border-top-style: solid; /* doesn't work with background in IE6/7 */
        }

        /* Vertical Events
        ------------------------------------------------------------------------*/

        .fc-event-vert {
            border-width: 0 1px;
        }

        .fc-event-vert.fc-event-start {
            border-top-width: 1px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .fc-event-vert.fc-event-end {
            border-bottom-width: 1px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
        }

        .fc-event-vert .fc-event-time {
            white-space: nowrap;
            font-size: 10px;
        }

        .fc-event-vert .fc-event-inner {
            position: relative;
            z-index: 2;
        }

        .fc-event-vert .fc-event-bg { /* makes the event lighter w/ a semi-transparent overlay  */
            position: absolute;
            z-index: 1;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #fff;
            opacity: .25;
            filter: alpha(opacity=25);
        }

        .fc .ui-draggable-dragging .fc-event-bg, /* TODO: something nicer like .fc-opacity */
        .fc-select-helper .fc-event-bg {
            display: none \9; /* for IE6/7/8. nested opacity filters while dragging don't work */
        }

        /* resizable */

        .fc-event-vert .ui-resizable-s {
            bottom: 0 !important; /* importants override pre jquery ui 1.7 styles */
            width: 100% !important;
            height: 8px !important;
            overflow: hidden !important;
            line-height: 8px !important;
            font-size: 11px !important;
            font-family: monospace;
            text-align: center;
            cursor: s-resize;
        }

        .fc-agenda .ui-resizable-resizing { /* TODO: better selector */
            _overflow: hidden;
        }

        thead tr.fc-first {
            background-color: #f7f7f7;
        }

        table.fc-header {
            background-color: #FFFFFF;
            border-radius: 6px 6px 0 0;
        }

        .fc-week .fc-day > div .fc-day-number {
            font-size: 15px;
            margin: 2px;
            min-width: 19px;
            padding: 6px;
            text-align: center;
            width: 30px;
            height: 30px;
        }

        .fc-sun, .fc-sat {
            color: #b8b8b8;
        }

        .fc-week .fc-day:hover .fc-day-number {
            background-color: #B8B8B8;
            border-radius: 50%;
            color: #FFFFFF;
            transition: background-color 0.2s;
        }

        .fc-week .fc-day.fc-state-highlight:hover .fc-day-number {
            background-color: #ff3b30;
        }

        .fc-button-today {
            border: 1px solid rgba(255, 255, 255, .0);
        }

        .fc-view-agendaDay thead tr.fc-first .fc-widget-header {
            text-align: right;
            padding-right: 10px;
        }

        .fc-event {
            background: #fff !important;
            color: #000 !important;
        }

        /* for vertical events */

        .fc-event-bg {
            display: none !important;
        }

        .fc-event .ui-resizable-handle {
            display: none !important;
        }

        .select2-container {
            width: 100% !important;
        }

        .fc-prev-button.fc-button {
            border: 2px solid #5120a7 !important;
            background-image: linear-gradient(to bottom, #c6acf1, #9675ce) !important;
            color: #ffffff !important;
            margin-right: 5px !important;
            border-radius: 3px !important;
        }

        .fc-next-button.fc-button {
            border: 2px solid #5120a7 !important;
            background-image: linear-gradient(to bottom, #c6acf1, #9675ce) !important;
            color: #ffffff !important;
            margin-left: 4px !important;
            border-radius: 3px !important;
        }

        .fc-next-button.fc-button:hover, .fc-prev-button.fc-button:hover {
            background-image: linear-gradient(to top, #c6acf1, #9675ce) !important;
            background-position: 0 0px !important;
        }

        .fc-myCustom1-button {
            background-image: none !important;
            border: 2px solid #5120a7 !important;
            margin-right: 4px !important;
        }

        .fc-myCustom2-button {
            background-image: none !important;
            border: 2px solid #5120a7 !important;
        }

        .fc-toolbar.fc-header-toolbar {
            margin-bottom: 0px !important;
            padding: 3px 0px 10px;
        }

        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
            display: none !important;
        }

        .ui-widget {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
            font-size: 15px !important;
        }

        .select2-container--default .select2-selection--multiple {
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Appointment</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header" style="padding:4px;">
                            <div class="box-tools pull-right"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li><a data-toggle="tab" href="#tab1primary" class="hvr-shutter-in-horizontal">Appointment</a></li>
                                        <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">List of Appointment</a></li>
                                        <li><a data-toggle="tab" href="#tab3primary" class="hvr-shutter-in-horizontal">Calendar</a></li>
                                        <li><a data-toggle="tab" href="#tab4primary" class="hvr-shutter-in-horizontal">Schedule</a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <form method="post" action="{{route('appointment.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <div class="Branch">
                                                    <h1>New Appointment</h1>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Regarding :</label>
                                                    <div class="col-md-7">
                                                        <div class="">
                                                            <select name="regarding" id="regarding" class="form-control fsc-input">
                                                                <option value="">---Select---</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Appointment With :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="employeeids" value="{{ ucwords(Auth::user()->name) }}" readonly id="employee" class="form-control">
                                                        <input type="text" name="employeeid" value="" readonly id="employee" class="form-control">
                                                    </div>
                                                    <label class="control-label col-md-1">Name :</label>
                                                    <div class="col-md-2">
                                                        <div class="">
                                                            <select name="withwhome" id="withwhome" class="form-control fsc-input">
                                                                <option value="">---Select---</option>
                                                                <option value="Approval">Client</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                                    <label class="control-label col-md-2 client" style="display:none;">Client Name :</label>
                                                    <div class="col-md-7 client" style="display:none;">

                                                        <div class="searchboxmain">
                                                            <input type="text" name="search" id="country_name" class="form-control" placeholder="Search">
                                                            {{ csrf_field() }}
                                                            <ul id="countryList"></ul>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-md-2 client1" style="display:none">Client Id :</label>
                                                    <div class="col-md-2 client1" style="display:none">
                                                        <div class="">
                                                            <input type="text" name="clientno" readonly id="clientno" class="form-control">
                                                        </div>
                                                    </div>
                                                    <label class="control-label col-md-2">Type Of Work :</label>
                                                    <div class="col-md-3">
                                                        <select name="work_type_id" id="work_type_id" class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                        </select>
                                                        <input type="hidden" value="" id="work_type" name="work_type"/>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Start Date :</label>
                                                            <div class="col-md-2 input-daterange">
                                                                <input type="text" name="startdate" id="startdate" class="form-control">
                                                            </div>
                                                            <div class="col-md-2" style="width:12.6%;">
                                                                <p name="" id="" placeholder="Day" class="form-control day1" disabled></p>
                                                            </div>
                                                            <div class="end_date">
                                                                <div id="_enddate">
                                                                    <div class="col-md-2 input-daterange">
                                                                        <input type="text" name="enddate1" id="enddate1" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-2" style="width:12.6%;">
                                                                        <p name="" id="" placeholder="Day" class="form-control day2" disabled></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Start Time :</label>
                                                            <div class="col-md-2">
                                                                <input type="text" name="starttime" id="starttime" class="form-control">
                                                            </div>
                                                            <div class="col-md-2" style="width:12.6%;">
                                                                <input type="text" name="endtime" id="endtime" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Participant :</label>
                                                    <div class="col-md-7">
                                                        <select name="participants[]" id="participant" class="form-control fsc-input" multiple>
                                                            <option value="">Select</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Priority :</label>
                                                    <div class="col-md-7">
                                                        <select name="priority" id="priority" class="form-control">
                                                            <option value="">Select</option>
                                                            <option value="Time Sensitive">Time Sensitive</option>
                                                            <option value="Urgent">Urgent</option>
                                                            <option value="Regular">Regular</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Details :</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control" name='details'></textarea>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="row">
                                                        <label class="control-label col-md-2"> </label>
                                                        <div class="col-md-8">
                                                            <input class="btn btn-primary icon-btn btn_new_save" type="submit" name="submit" value="CREATE" style="width:auto;padding:8px 20px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab2primary">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered" id="sampleTable3">
                                                        <thead>
                                                        <tr>
                                                            <th width="4%">No.</th>
                                                            <th width="10%">Appt. Date</th>
                                                            <th width="10%">Appt. Time</th>
                                                            <th width="8%">Priority</th>
                                                            <th width="6%">Type</th>
                                                            <th width="9%">Client ID</th>
                                                            <th width="13%">Team Rep.</th>
                                                            <th width="30%">Regards</th>
                                                            <!--<th width="12%">With Whom</th>-->
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td></td>
                                                            <td></td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3primary">
                                            <div id="calendar"></div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4primary">
                                            <form enctype="multipart/form-data" class="form-horizontal changepassword" action="" id="changepassword" method="GET">
                                                <input type="hidden" name="_token" value="kNQMQb5n4Zhr4CwrmHUY1EeSgTRPAOs6RyGOZJvL">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <div class="form-group ">
                                                            <label for="focusedinput" class="col-md-3 control-label">Schedule Date : <span class="required"> * </span></label>
                                                            <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                                                                <div class="col-sm-3">
                                                                    <input class="form-control" name="startdate" id="startdate" type="text">
                                                                </div>
                                                                <label for="focusedinput" class="col-sm-1 control-label">To</label>
                                                                <div class="col-sm-3">
                                                                    <input class="form-control" name="enddate" id="enddate" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label for="focusedinput" class="col-md-3 control-label"></label>
                                                            <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                                                                <div class="col-sm-3">
                                                                    <button class="btn-success btn" type="submit" name="search"> Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.js"></script>


    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function () {

            /* initialize the external events
             -----------------------------------------------------------------*/
            function init_events(ele) {
                ele.each(function () {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    }

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject)

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    })

                })
            }

            init_events($('#external-events div.external-event'))

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date()
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear()

            //Not changeable area
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,myCustom1,myCustom2,next',
                    center: 'title',
                    right: 'agendaDay,agendaWeek,month'
                },
                allDayText: 'All Day',
                customButtons: {
                    myCustom1: {
                        text: '06-03-2021',
                        // click: function() {
                        //     alert('clicked the custom button!');
                        // }
                    },
                    myCustom2: {
                        text: 'Tuesday',
                        // click: function() {
                        //     alert('clicked the custom button!');
                        // }
                    }
                },
                buttonText: {
                    today: '04-03-2021',
                    month: 'Month',
                    week: 'Week',
                    day: 'Day'
                },
                views: {
                    month: {columnFormat: 'dddd',},
                    week: {columnFormat: 'dddd[ ]M/D[ ]',},
                },
                events: '{{URL('fac-Bhavesh-0554/appointment/getAppointment')}}',
                eventDisplay: 'auto',
                //End Changeable area
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject')

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject)

                    // assign it the date that was reported
                    copiedEventObject.start = date
                    copiedEventObject.allDay = allDay
                    copiedEventObject.backgroundColor = $(this).css('background-color')
                    copiedEventObject.borderColor = $(this).css('border-color')

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove()
                    }

                }
            })

            /* ADDING EVENTS */
            var currColor = '#3c8dbc' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn')
            $('#color-chooser > li > a').click(function (e) {
                e.preventDefault()
                //Save color
                currColor = $(this).css('color')
                //Add color effect to button
                $('#add-new-event').css({'background-color': currColor, 'border-color': currColor})
            })
            $('#add-new-event').click(function (e) {
                e.preventDefault()
                //Get value and make sure it is not null
                var val = $('#new-event').val()
                if (val.length == 0) {
                    return
                }

                //Create events
                var event = $('<div />')
                event.css({
                    'background-color': currColor,
                    'border-color': currColor,
                    'color': '#fff'
                }).addClass('external-event')
                event.html(val)
                $('#external-events').prepend(event)

                //Add draggable funtionality
                init_events(event)

                //Remove event from text input
                $('#new-event').val('')
            })
        })

        $(document).ready(function () {
            $("#startdate").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function (dateText, inst) {
                    var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                    var dateText = $.datepicker.formatDate("DD", date, inst.settings);
                    $(".day1").html(dateText); // Just the day of week
                }
            });
            $("#enddate1").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function (dateText, inst) {
                    var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                    var dateText = $.datepicker.formatDate("DD", date, inst.settings);
                    $(".day2").html(dateText); // Just the day of week
                }
            });
        });

    </script>
@endsection()