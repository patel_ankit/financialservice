@extends('fscemployee.layouts.app')
@section('title', 'Technical Support')
@section('main-content')
    <style>
        .page-title {
            padding: 8px 0px !important;
    </style>
    <div class="content-wrapper">

        <section class="content-header page-title" style="">
            <div class="" style="">
                <div class="" style="text-align:center;">
                    <h1>List of Technical Support <span style="padding-right:10px;float:right;">View / Edit</span></h1>
                </div>

            </div>
        </section>
        <section class="content" style="background-color: #fff;padding-top:20px;">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">

                                    <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th>Creation Date / Day / Time</th>
                                        <th>Employee Name</th>
                                        <th>Tech. Support</th>
                                        <th>Subject</th>
                                        <th>Details</th>
                                        <th width="9%">Status</th>
                                        <th width="6%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rules1 as $rul)
                                        <tr>
                                            <td style="text-align:center;">{{$loop->index+1}}</td>
                                            <td style="text-align:center;">{{$rul->date}}<br>{{$rul->day}}<br>{{$rul->time}}</td>
                                            <td>

                                                @foreach($employee1 as $rul2)
                                                    @if($rul2->id==$rul->emp_id) {{$rul2->firstName.' '.$rul2->middleName.' '.$rul2->lastName}} @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach($employee2 as $rul1)
                                                    @if($rul1->id==$rul->to_supporter) {{$rul1->firstName.' '.$rul1->middleName.' '.$rul1->lastName}} @endif
                                                @endforeach

                                            </td>
                                            <td>{{$rul->subject}}</td>
                                            <td>{!!$rul->details!!}</td>
                                            <td style="text-align:center;">@if($rul->answer=='') <a href="#" class="btn btn-danger" style="padding: 6px 12px;">Pending</a> @else <a href="#" style="padding: 6px 12px;" class="btn btn-success">Done</a> @endif</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('technicalsupport.edit',$rul->id)}}"><i class="fa fa-edit"></i></a></td>

                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection