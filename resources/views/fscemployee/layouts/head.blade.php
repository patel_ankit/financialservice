<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{URL::asset('public/dashboard/images/favicon.png')}}" type="image/x-icon">
<title>FSC - @yield('title')</title>

<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/skins/_all-skins.min.css')}}">
<!-- Morris chart -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/morris.js/morris.css')}}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/bootstrap-combined.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/style.css')}}">
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="{{URL::asset('public/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>

<script src="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/password.validation.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/dist/js/adminlte.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>
<script src="{{URL::asset('public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/fullcalendar.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('public/dashboard/js/jquery.maskedinput.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap.validation.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="https://financialservicecenter.net/public/frontcss/js/bootstrap-formhelpers.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(function () {
        CKEDITOR.replace('editor1');
        $('#editor1').wysihtml5();
    })
    $(function () {
        CKEDITOR.replace('editor2');
        $('#editor2').wysihtml5();
    })
</script>
<style>
    .modal-dialog {
        width: 60%;
        margin: 30px auto;
    }

    .sw-main .sw-container {
        z-index: 9
    }

    footer p {
        text-align: center;
        margin-top: 20px;
        color: #054b94;
        font-size: 17px;
    }

    .nav-tabs > li {
        border: none;
    }

    .help-block {
        color: red;
    }

    .form-control {
        font-style: normal;
        color: #000 !important;
        border: 2px solid #00468F;
    }

    .tab-content .form-control {
        font-size: 14px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location;
        $('ul.sidebar-menu a').filter(function () {
            return this.href == url;
        }).parent().siblings().removeClass('active').end().addClass('active');

// for treeview which is like a submenu
        $('ul.treeview-menu a').filter(function () {
            return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
    });
</script>
<script>
    $("#business_no").mask("(999) 999-9999");
    $("#business_fax").mask("(999) 999-9999");
    $(".business_fax1").mask("(999) 999-9999");
    $(".residence_fax").mask("(999) 999-9999");
    $("#mobile_no").mask("(999) 999-9999");
    $(".cell").mask("(999) 999-9999");
    $(".telephone").mask("(999) 999-9999");
    $(".usapfax").mask("(999) 999-9999");
    $("#zip").mask("9999");
    $("#mailing_zip").mask("9999");
    $("#bussiness_zip").mask("9999");
    $(".ext").mask("99999");
</script>
@guest
@else

    @if(Auth::user()->rule_status=='1')
        @if (session()->has('success'))
            <script>
                $(window).load(function () {
                    $('#myModalrules').modal('show');
                });
            </script>
            <div id="myModalrules" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Rules Status</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please Click Below link to check rules</p>
                        </div>
                        <div class="modal-footer">
                            <a href="{{url('fscemployee/employeeprofile')}}?tab=1" class="btn btn-default ru">Rules</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endif

    <?php
    $remdays = Auth::user()->enddate;
    $no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
    $no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
    $no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
    $no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
    $beforethreeday = strtotime($no1);
    $enddate = strtotime($remdays);
    $currentday = date('Y-m-d');
    if($currentday == $no1 || $currentday == $no2 || $currentday == $no3)
    {
    ?>
    @if (session()->has('success') )
        <script>
            $(window).load(function () {
                $('#myModal').modal('show');
            });
        </script>
    @endif
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <p><?php if($currentday == $no1){?>Please Change Your Password then Your Password Expired after 3 days<?php }?><?php if($currentday == $no2){?>Please Change Your Password then Your Password Expired after 2 days<?php }?><?php if($currentday == $no3){?>Please Change Your Password then Your Password Expired after 1 days<?php }?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php
    }
    elseif($currentday == $no4)
    {
    ?>
    <style>
        .modal-content {
            width: 700px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#myModal").modal({
                backdrop: 'static',
                keyword: 'false',
                show: true,
            });
        });
    </script>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal changepassword" enctype="multipart/form-data" action="{{route('userchangepassword1.update',Auth::user()->id)}}" id="changepassword1">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Old Password :</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" id="oldpassword" value="" name="oldpassword">
                                        <span toggle="#oldpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @if ($errors->has('oldpassword'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                                        @endif
                                        @if ( session()->has('error') )
                                            <span class="help-block" style="color:red">
                              <strong>{{ session()->get('error') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">New Password :</label>
                                    <div class="col-md-8">
                                        <input name="password" value="" type="password" id="password" class="form-control"/>
                                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('confirmpassword') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Confirm Password :</label>
                                    <div class="col-md-8">
                                        <input name="confirmpassword" value="" type="password" id="confirmpassword" class="form-control"/>
                                        @if ($errors->has('confirmpassword'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('confirmpassword') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Reset Days :</label>
                                    <div class="col-md-8">
                                        <select name="resetdays" value="" id="resetdays" class="form-control">
                                            @if(empty(Auth::user()->reset_day))
                                                <option>---Select Reset Days---</option> @endif

                                            <option value="30" @if(Auth::user()->reset_day=='30') selected @endif>30</option>
                                            <option value="90" @if(Auth::user()->reset_day=='90') selected @endif>90</option>
                                            <option value="120" @if(Auth::user()->reset_day=='120') selected @endif>120</option>
                                        </select>
                                        @if ($errors->has('resetdays'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('resetdays') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Change Password">
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {

            $('.changepassword').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    newpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            regexp:
                                {

                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                    message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                },

                            different: {
                                field: 'oldpassword',
                                message: 'The password cannot be the same as Current Password'
                            }
                        }
                    },
                    oldpassword: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Current Password'
                            },
                            remote: {
                                message: 'The Password is not available',
                                url: '{{ URL::to('fscemployee/checkpassword12') }}',
                                data: {
                                    type: 'oldpassword'
                                },
                                type: 'POST'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and can\'t be empty'
                            },
                            identical: {
                                field: 'newpassword',
                                message: 'The password and its confirm are not the same'
                            },
                            different: {
                                field: 'oldpassword',
                                message: 'The password can\'t be the same as Old Password'
                            }
                        }
                    }
                }

            }).on('success.form.bv', function (e) {
                $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                $('.changepassword').data('bootstrapValidator').resetForm();
                // Prevent form submission
                e.preventDefault();
                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    // console.log(result);
                }, 'json');
            });
        });
    </script>
    <?php
    $remdays = Auth::user()->enddate;
    $no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
    $no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
    $no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
    $no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
    $beforethreeday = strtotime($no1);
    $enddate = strtotime($remdays);
    $currentday = date('Y-m-d');
    if($currentday == $no1 || $currentday == $no2 || $currentday == $no3)
    {
    ?>
    @if ( session()->has('success') )
        <script>
            $(window).load(function () {
                $('#myModal').modal('show');
            });
        </script>
    @endif
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <p><?php if($currentday == $no1){?>Please Change Your Password then Your Password Expired after 3 days<?php }?><?php if($currentday == $no2){?>Please Change Your Password then Your Password Expired after 2 days<?php }?><?php if($currentday == $no3){?>Please Change Your Password then Your Password Expired after 1 days<?php }?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php
    }
    elseif($currentday == $no4)
    {
    ?>
    <style>.modal-content {
            width: 700px;
        }</style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#myModal").modal({
                backdrop: 'static',
                keyword: 'false',
                show: true,
            });
        });
    </script>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal changepassword" enctype="multipart/form-data" action="{{route('userchangepassword1.update', Auth::user()->id)}}" id="changepassword">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Old Password :</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" id="oldpassword" value="" name="oldpassword">
                                        @if ($errors->has('oldpassword'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                                        @endif
                                        @if ( session()->has('error') )
                                            <span class="help-block" style="color:red">
                              <strong>{{ session()->get('error') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('newpassword') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">New Password :</label>
                                    <div class="col-md-8">
                                        <input name="newpassword" value="" type="password" id="newpassword" class="form-control"/>
                                        <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        <div id="messages"></div>
                                        @if ($errors->has('newpassword'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('newpassword') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('cpassword') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Confirm Password :</label>
                                    <div class="col-md-8">
                                        <input name="cpassword" value="" type="password" id="cpassword" class="form-control"/>
                                        @if ($errors->has('cpassword'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('cpassword') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Reset Days :</label>
                                    <div class="col-md-8">
                                        <select name="resetdays" value="" id="resetdays" class="form-control">
                                            @if(empty(Auth::user()->resetdays))
                                                <option>---Select Reset Days---</option> @endif

                                            <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                            <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                            <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                                        </select>
                                        @if ($errors->has('resetdays'))
                                            <span class="help-block">
                              <strong>{{ $errors->first('resetdays') }}</strong>
                              </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Change Password">
                        </div>
                        <br>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {

            $('.changepassword').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    newpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            regexp:
                                {

                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                    message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                },

                            different: {
                                field: 'oldpassword',
                                message: 'The password cannot be the same as Current Password'
                            }
                        }
                    },
                    oldpassword: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Current Password'
                            },
                            remote: {
                                message: 'The Password is not available',
                                url: '{{ URL::to('fscemployee/checkpassword12') }}',
                                data: {
                                    type: 'oldpassword'
                                },
                                type: 'POST'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and can\'t be empty'
                            },
                            identical: {
                                field: 'newpassword',
                                message: 'The password and its confirm are not the same'
                            },
                            different: {
                                field: 'oldpassword',
                                message: 'The password can\'t be the same as Old Password'
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                $('.changepassword').data('bootstrapValidator').resetForm();
                // Prevent form submission
                e.preventDefault();
                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    // console.log(result);
                }, 'json');
            });
        });
    </script>
    <?php
    }
    ?>
    <?php
    }
    ?>
    @if(Auth::user()->active==0 )
        <script type="text/javascript">
            $(document).ready(function () {
                $("#myModal").modal({
                    backdrop: 'static',
                    keyword: 'false',
                    show: true,
                });
            });
        </script>
        <style type="text/css">
            #registrationForm .tab-content {
                margin-top: 20px;
            }

            .pager {
                bottom: -4px;
            }
        </style>
        <script>
            $(".ext").mask("99999");
        </script>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        @if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                        <form enctype='multipart/form-data' action="{{route('empprofile.update',Auth::user()->id)}}" id="registration" method="post">
                            {{csrf_field()}} {{method_field('PATCH')}}
                            <div id="smartwizard">
                                <ul class="nav nav-pills">
                                    <li class="active"><a href="#step-1" data-toggle="tab">Login Information</a></li>
                                    <li><a href="#step-2" data-toggle="tab">General Information</a></li>
                                    <li><a href="#step-3" data-toggle="tab">Personal Information</a></li>
                                </ul>
                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" data-step="0" id="step-1">
                                        <div id="form-step-0" role="form" data-toggle="validator">
                                            @if ( session()->has('success') )
                                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                                            @endif
                                            @if ( session()->has('error') )
                                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="newpassword">New Password : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="password" class="form-control fsc-input" name="newpassword" id="newpassword" placeholder="Enter Your New Password" required>
                                                        <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        <div class="help-block with-errors"></div>
                                                        <div id="messages"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="newpassword">Confirm Password : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="password" class="form-control fsc-input" name="cpassword" id="cpassword" placeholder="Enter Your Confirm Password" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question1">Question 1 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question1" id="question1" class="form-control fsc-input" required>
                                                            <option value="">---Select---</option>
                                                            <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                            <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                            <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                                            <option value="In what city were you born?">In what city were you born?</option>
                                                            <option value="What is the name of your first school?">What is the name of your first school?</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer1">Answer 1:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer1" value="" required type="text" id="answer1" class="form-control fsc-input">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question2">Question 2:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question2" id="question2" required class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            <option value="What is your favorite movie?">What is your favorite movie?</option>
                                                            <option value="What was the make of your first car?">What was the make of your first car?</option>
                                                            <option value="What is your favorite color?">What is your favorite color?</option>
                                                            <option value="What is your father's middle name?">What is your father's middle name?</option>
                                                            <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer2">Answer 2:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer2" value="" required type="text" id="answer2" class="form-control fsc-input">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question3">Question 3: </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question3" id="question3" required class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            <option value="What was your high school mascot?">What was your high school mascot?</option>
                                                            <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                                            <option value="In what year was your father born?">In what year was your father born?</option>
                                                            <option value="What is the name of your favorite childhood friend?">What is the name of your favorite childhood friend?</option>
                                                            <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer3">Answer 3:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer3" value="" required type="text" id="answer3" class="form-control fsc-input">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="pager wizard">
                                            <li class="next last" style="display:none;"><a href="#">Last</a></li>
                                            <li class="next"><a href="#">Next</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" id="step-2" data-step="2">
                                        <div id="form-step-2" role="form" data-toggle="validator">

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Employee ID : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="employee_id" name="employee_id" value="{{$employee->employee_id}}" placeholder="Employee ID" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Name : </label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input textonly" id="firstName" name="firstName" placeholder="First Name" value="{{$employee->firstName}}" required>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input textonly" id="middleName" name="middleName" placeholder="Middle Name" value="{{$employee->middleName}}">
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input textonly" id="lastName" name="lastName" placeholder="Last Name" value="{{$employee->lastName}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Email : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="email" readonly class="form-control fsc-input" id="email" name="email" placeholder="abc@abc.com" value="{{$employee->email}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address 1 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="address1" name="address1" placeholder="Enter Your Address" value="{{$employee->address1}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address 2 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="address2" name="address2" placeholder="Enter Your Address" value="{{$employee->address2}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="{{$employee->countryId}}" required></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City" value="{{$employee->city}}" required>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$employee->stateId}}">
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input zip" maxlength="6" id="zip" name="zip" placeholder="Postel Code" value="{{$employee->zip}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 1 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo1" name="telephoneNo1" placeholder="Telephone 1" value="{{$employee->telephoneNo1}}" required>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                            <option value="Mobile" @if($employee->telephoneNo1Type=='Mobile') selected @endif>Work</option>
                                                            <option value="Resident" @if($employee->telephoneNo1Type=='Resident') selected @endif>Resident</option>
                                                            <option value="Office" @if($employee->telephoneNo1Type=='Office') selected @endif>Cell</option>
                                                            <option value="Other" @if($employee->telephoneNo1Type=='Other') selected @endif>Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input ext" id="ext1" maxlength="5" name="ext1" value="{{$employee->ext1}}" placeholder="Ext" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 2 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo2" name="telephoneNo2" placeholder="Telephone 2" value="{{$employee->telephoneNo2}}">

                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                            <option value="Mobile" @if($employee->telephoneNo2Type=='Mobile') selected @endif>Work</option>
                                                            <option value="Resident" @if($employee->telephoneNo2Type=='Resident') selected @endif>Resident</option>
                                                            <option value="Office" @if($employee->telephoneNo2Type=='Office') selected @endif>Cell</option>
                                                            <option value="Other" @if($employee->telephoneNo2Type=='Other') selected @endif>Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input ext" id="ext2" maxlength="5" name="ext2" value="{{$employee->ext2}}" placeholder="Ext" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Select Photo : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="photo" placeholder="Upload Service Image" class="form-control fsc-input" id="photo" type="file">
                                                        <input name="photo1" placeholder="Upload Service Image" value="{{$employee->photo}}" class="form-control fsc-input" id="photo1" type="hidden">
                                                        <br>
                                                        <img style="width: 100px;height: 50px;" src="{{asset('public/employeeimage')}}/{{$employee->photo}}" alt="" style="width:69px;display:none" id="blah">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" class="form-control fsc-input" readonly="" id="active" value="1" name="active" placeholder="">
                                        </div>
                                        <ul class="pager wizard" style="bottom:17px;">
                                            <li class="previous first" style="display:none;"><a href="#">First</a></li>
                                            <li class="previous"><a href="#">Previous</a></li>
                                            <li class="next last" style="display:none;"><a href="#">Last</a></li>
                                            <li class="next"><a href="#">Next</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" id="step-3" data-step="3">
                                        <div id="form-step-3" role="form" data-toggle="validator">
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Gender : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="gender" id="gender" class="form-control fsc-input">
                                                            <option value="">Select</option>
                                                            <option value="Male" @if($employee->gender=='Male') selected @endif>Male</option>
                                                            <option value="Female" @if($employee->gender=='Female') selected @endif>Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Marital Status : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="marital" id="marital" class="form-control fsc-input">
                                                            <option value="">Select</option>
                                                            <option value="Married" @if($employee->marital=='Married') selected @endif>Married</option>
                                                            <option value="UnMarried" @if($employee->marital=='UnMarried') selected @endif>UnMarried</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Date Of Birth : </label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="month" id="month" value="{{$employee->month}}">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">ID Proof 1 : </label>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <select name="pf1" class="form-control fsc-input" id="pf1">
                                                            <option value="">Select Proof</option>
                                                            <option value="Voter Id" @if($employee->pf1=='Voter Id') selected @endif>Voter Id</option>
                                                            <option value="Driving Licence" @if($employee->pf1=='Driving Licence') selected @endif>Driving Licence</option>
                                                            <option value="Pan Card" @if($employee->pf1=='DPan Card') selected @endif>Pan Card</option>
                                                            <option value="Pass Port" @if($employee->pf1=='Pass Port') selected @endif>Pass Port</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="pfid1" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid1" type="file">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">ID Proof 2 : </label>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <select name="pf2" class="form-control fsc-input" id="pf2">
                                                            <option value="">Select Proof</option>
                                                            <option value="Voter Id" @if($employee->pf2=='Voter Id') selected @endif>Voter Id</option>
                                                            <option value="Driving Licence" @if($employee->pf2=='Driving Licence') selected @endif>Driving Licence</option>
                                                            <option value="Pan Card" @if($employee->pf2=='DPan Card') selected @endif>Pan Card</option>
                                                            <option value="Pass Port" @if($employee->pf2=='Pass Port') selected @endif>Pass Port</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="pfid2" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid2" type="file">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;height: 51px;margin: 20px 0;">
                                                <h3 class="Libre fsc-reg-sub-header">Emergency Contact Info</h3>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Person Name : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="firstName_1" name="firstName_1" placeholder="First" value="{{$employee->firstName_1}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Relationship :</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="relation" id="relation" value="{{$employee->relation}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address 1 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="{{$employee->address11}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : </label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px!important;">
                                                        <input type="text" class="form-control fsc-input" id="ecity" name="ecity" placeholder="City" value="{{$employee->ecity}}">
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="estate" id="estate" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$employee->estate}}">
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-left:0px!important;">
                                                        <input type="text" class="form-control fsc-input" id="ezipcode" name="ezipcode" value="{{$employee->ezipcode}}" placeholder="Zip">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="test">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 1 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-md-6" style="padding-right:0px!important;">
                                                                <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format=" (ddd) ddd-dddd" id="etelephone1" name="etelephone1" placeholder="(000)000-0000" value="{{$employee->etelephone1}}">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="eteletype1" id="eteletype1" class="form-control fsc-input">
                                                                    <option value="Mobile" @if($employee->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Resident" @if($employee->eteletype1=='Resident') selected @endif>Resident</option>
                                                                    <option value="Office" @if($employee->eteletype1=='Office') selected @endif>Office</option>
                                                                    <option value="Other" @if($employee->eteletype1=='Other') selected @endif>Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3" style="padding-left:0px!important;">
                                                                <input type="text" class="form-control fsc-input ext" maxlength="5" value="{{$employee->eext1}}" readonly id="eext1" name="eext1" placeholder="Ext">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 2 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-md-6" style="padding-right:0px!important;">
                                                                <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format=" (ddd) ddd-dddd" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="{{$employee->etelephone2}}">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="eteletype2" id="eteletype2" class="form-control fsc-input">
                                                                    <option value="Mobile" @if($employee->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Resident" @if($employee->eteletype1=='Resident') selected @endif>Resident</option>
                                                                    <option value="Office" @if($employee->eteletype1=='Office') selected @endif>Office</option>
                                                                    <option value="Other" @if($employee->eteletype1=='Other') selected @endif>Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3" style="padding-left:0px!important;">
                                                                <input type="text" class="form-control fsc-input ext" maxlength="5" value="{{$employee->eext2}}" readonly id="eext2" name="eext2"
                                                                       placeholder="Ext">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Note For Emergency : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input">{{$employee->comments1}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">

                                                </div>
                                            </div>
                                        </div>
                                        <ul class="pager wizard" style="bottom:17px;">
                                            <li class="previous first" style="display:none;"><a href="#">First</a></li>
                                            <li class="previous"><a href="#">Previous</a></li>
                                            <li><input type="submit" value="Save" class="pull-right btn btn-success"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <a href="{{ route('fscemployee.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger"><i class="fa fa-sign-out"></i><span>Log Out</span></a>
                        <form id="logout-form" action="{{ route('fscemployee.logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap.validation.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/jquery.bootstrap.wizard.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')}}"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers:
                    {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    }
            });
            $(document).ready(function () {
                function adjustIframeHeight() {
                    var $body = $('body'),
                        $iframe = $body.data('iframe.fv');
                    if ($iframe) {
                        // Adjust the height of iframe
                        $iframe.height($body.height());
                    }
                }

                $('#registration').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    excluded: ':disabled',
                    fields: {
                        personname: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Person Name'
                                },
                            }
                        },

                        eaddress1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Address'
                                },
                            }
                        },
                        per_city: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter City'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The City can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        per_stateId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select State'
                                },
                            }
                        },
                        per_zip: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Country'
                                },
                            }
                        },
                        comments: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Comments'
                                },
                            }
                        },


                        newpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and cannot be empty'
                                },
                                regexp:
                                    {

                                        regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                        message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                    },
                                different: {
                                    field: 'oldpassword',
                                    message: 'The password cannot be the same as Current Password'
                                }
                            }
                        },/*
 oldpassword: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Current Password'
					},	
                    remote: {
                        message: 'The Password is not available',
                        url: '{{ URL::to('fscemployee/checkemppassword') }}',
                        data: {
                            type: 'oldpassword'
                        },
                        type: 'POST'
                    }																		
                }
            },*/
                        cpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The confirm password is required and can\'t be empty'
                                },
                                identical: {
                                    field: 'newpassword',
                                    message: 'The password and its confirm are not the same'
                                },
                                different: {
                                    field: 'oldpassword',
                                    message: 'The password can\'t be the same as Old Password'
                                }
                            }
                        },


                        question1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 1'
                                }
                            }
                        },
                        question2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 2'
                                }
                            }
                        },
                        question3: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 3'
                                }
                            }
                        },
                        answer1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 1'
                                }
                            }
                        },
                        answer2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 2'
                                }
                            }
                        },
                        answer3: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 3'
                                }
                            }
                        },
                        business_id: {
                            validators: {
                                message: 'Please Select Your Business Name',
                            }
                        },
                        business_cat_id: {
                            validators: {
                                message: 'Please Select Your Business Category Name',
                            }
                        },
                        business_brand_id: {
                            validators: {
                                message: 'Please Select Your Business Brand Name',
                            }
                        },
                        business_brand_category_id: {
                            validators: {
                                message: 'Please Select Your Business Brand Category Name',
                            }
                        },
                        company_name: {
                            validators: {
                                message: 'Please Select Your Company Name',
                            }
                        },
                        business_name: {
                            validators: {
                                message: 'Please Select Your Business Name',
                            }
                        },
                        first_name1: {
                            validators: {
                                stringLength: {
                                    min: 2,
                                },
                                notEmpty: {
                                    message: 'Please Enter Your First Name'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The First Name can consist of alphabetical characters and spaces only'
                                }
                            }
                        },

                        last_name1: {
                            validators: {
                                stringLength: {
                                    min: 2,
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Last Name'
                                },

                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The Last name can consist of alphabetical characters and spaces only'
                                }
                            }
                        },

                        address: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Address'
                                }
                            }
                        },

                        city: {
                            validators: {
                                stringLength: {
                                    min: 2,

                                },
                                notEmpty: {
                                    message: 'Please Enter Your City'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The City can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        stateId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your State'
                                }
                            }
                        },
                        countryId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Country'
                                }
                            }
                        },
                        zip: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Zip Code'
                                }
                            }
                        },
                        business_no: {
                            validators: {
                                stringLength: {
                                    min: 15,
                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Business Phone Number'
                                },
                                business_no: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }
                            }
                        },
                        mobile_no: {
                            validators: {

                                notEmpty: {
                                    message: 'Please Enter Your Mobile Number'
                                },
                                mobile_no: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }
                            }
                        },
                        business_fax: {
                            validators: {

                                notEmpty: {
                                    message: 'Please Enter Your Business Fax'
                                }, business_fax: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }

                            }
                        },

                    }
                }).bootstrapWizard({
                    tabClass: 'nav nav-pills',
                    onTabClick: function (tab, navigation, index) {
                        return validateTab(index);
                    },
                    onNext: function (tab, navigation, index) {
                        var numTabs = $('#registration').find('.tab-pane').length,
                            isValidTab = validateTab(index - 1); //alert(numTabs);
                        if (!isValidTab) {
                            return false;
                        }

                        if (index === numTabs) {
                            $('#completeModal').modal();
                        }

                        return true;
                    },
                    onPrevious: function (tab, navigation, index) {
                        return validateTab(index + 1);
                    },
                    onTabShow: function (tab, navigation, index) {
                        // Update the label of Next button when we are at the last tab
                        var numTabs = $('#registration').find('.tab-pane').length;
                        $('#registration')
                            .find('.next')
                            .removeClass('disabled')    // Enable the Next button
                            .find('a')
                            .html(index === numTabs - 1 ? '' : 'Next');

                        // You don't need to care about it
                        // It is for the specific demo
                        adjustIframeHeight();
                    }
                });

                function validateTab(index) {
                    var fv = $('#registration').data('formValidation'), // FormValidation instance
                        // The current tab
                        $tab = $('#registration').find('.tab-pane').eq(index);

                    // Validate the container
                    fv.validateContainer($tab);
                    var isValidStep = fv.isValidContainer($tab);
                    if (isValidStep === false || isValidStep === null) {
                        // Do not jump to the target tab
                        return false;
                    }

                    return true;
                }
            });
            $("#month").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
                //endDate: "today"
            });
        </script>
        <script>
            $('.ext').mask("99999");
            var date = $('#ext1').val();
            $('#telephoneNo1Type').on('change', function () {
                if (this.value == 'Office' || this.value == 'Mobile') {
                    document.getElementById('ext1').removeAttribute('readonly');
                    $('#ext1').val();
                } else {
                    document.getElementById('ext1').readOnly = true;
                    $('#ext1').val('');
                }
            })
        </script>

        <script>
            var dat1 = $('#ext2').val();
            $('#telephoneNo2Type').on('change', function () {
                if (this.value == 'Office' || this.value == 'Mobile') {
                    document.getElementById('ext2').removeAttribute('readonly');
                    $('#ext2').val();
                } else {
                    document.getElementById('ext2').readOnly = true;
                    $('#ext2').val('');
                }
            })
        </script>

        <script>
            $('#eteletype2').on('change', function () {
                if (this.value == 'Office' || this.value == 'Mobile') {
                    $('#eext2').val();
                    document.getElementById('eext2').removeAttribute('readonly');
                } else {
                    document.getElementById('eext2').readOnly = true;
                    $('#eext2').val('');
                }
            })
        </script>

        <script>
            var dat2 = $('#eext1').val();
            $('#eteletype1').on('change', function () {
                if (this.value == 'Office' || this.value == 'Mobile') {
                    document.getElementById('eext1').removeAttribute('readonly');
                    $('#eext1').val();
                } else {
                    document.getElementById('eext1').readOnly = true;
                    $('#eext1').val('');
                }
            })
        </script>
        <style>
            .field-icon {
                margin-top: -82px;
            }

            .modal {
                z-index: 99999999999999;
            }
        </style>
    @endif
@endguest