<?php $ccountry = "America/New_York";
date_default_timezone_set($ccountry);
$time = date_default_timezone_get(); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js"></script>
<script type="text/javascript">$(function () {
        setInterval(function () {
            var divUtc = $('#divUTC');
            var divLocal = $('#divLocal');
            divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss a'));
            var localTime = moment.utc(divUtc.text()).toDate();
            $('#divUsa').text(moment.tz('{{$time}}').format('hh:mm:ss a'));
        }, 1000);
    });</script>
<style>
    .buttons-copy, .buttons-csv {
        display: none;
    }

    .top_company {
        float: left;
        margin: 34px 0 0 0;
    }

    .has-feedback .form-control {
        padding-right: 0 !important;
    }

    .top_company.top-company-name-bg {
        background: #fff;
        border-radius: 5px;
        margin: 10px 0;
        padding: 0;
    }

    .top_company.top-company-name-bg h3 {
        margin: 0;
        color: #fff;
        background: #12186b;
        padding: 10px;
        border-radius: 5px 5px 0 0px;
        font-size: 18px;
        text-align: center;
    }

    .top_company.top-company-name-bg p {
        color: #000;
        padding-left: 0;
        padding: 10px;
        text-align: center;
        display: block;
        font-size: 18px;
    }

    .top_company.top-company-name-bg span {
        color: #000;
        font-size: 14px;
    }

    .top_company.top-company-name-bg i {
        color: #000;
    }

    .top_company.top-company-name-bg .top_date, .top_time, .top_day {
        padding: 3px 0;
        width: 100%;
    }

    .bg-aqua {
        background-color: #1b6fa4 !important;
        border-bottom: 4px solid #fff;
        border-right: 3px solid;
        border-left: 1px solid;
    }

    .info-box {
        background: #717bbb !important;
        border: 2px solid #fff;
    }

    .info-box-text {
        color: #fff;
    }

    a {
        color: #fff;
    }

    .wdth47 {
        width: 47% !important;
    }

    .wdth49 {
        width: 49% !important;
    }

    .skin-blue .main-header .navbar .sidebar-toggle {
        top: 0;
        left: 0;
        z-index: 999;
        border: 1px solid #ffffff;
        padding: 9px 13px;
        margin-left: 7px;
        margin-top: 40px;
    }

    .main-header .logo .logo-lg {
        margin: 10px auto;
    }

    .topbar {
        border-radius: 10px;
    }

    .top_name {
        margin-top: 38px;
    }

    .topbar img {
        max-width: 100%;
    }

    .top_company.top-company-name-bg {
        background: #fff;
        border-radius: 5px;
        margin: 14px 0;
        padding: 0;
        width: 100%;
    }

    @media screen and (max-width: 1290px) {
        .wdth47 {
            width: 42% !important;
        }

        .wdth49 {
            width: 49% !important;
            padding-left: 0px !important;
        }

        /*.top_company.top-company-name-bg h3{min-width:200px!important;}*/
        .topbar h1 {
            font-size: 20px;
        }
    }

    @media (max-width: 1100px) {
        .top_company.top-company-name-bg h3 {
            min-width: auto;
        }
    }

    @media (max-width: 900px) {
        .top_company.top-company-name-bg {
            width: 100%;
        }

        .wdth47 {
            width: 90% !important;
        }

        .wdth49 {
            width: 100% !important;
            padding-left: 0px !important;
        }

        .wsm50 {
            width: 48% !important;
        }
    }

    @media (max-width: 767px) {
        .content {
            margin-top: 0px !important;
        }

        .portlet.box.blue {
            margin-top: 0px !important;
            top: 30px;
        }

        .top_company.top-company-name-bg {
            margin: 10px auto;
            float: none !important;
            width: 260px;
        }

        .main-header .navbar-custom-menu {
            margin-top: 0 !important;
            left: 0 !important;
        }

        .skin-blue .main-header .navbar .sidebar-toggle {
            margin-top: 20px;
            position: absolute;
            z-index: 999;
            font-size: 20px;
        }

        .main-sidebar {
            top: 160px !important;
            padding-top: 0 !important;
            z-index: 9999 !important;
        }

        .main-sidebar .sidebar {
            top: 0px;
        }

        .logo .img-responsive {
            width: auto !important;
        }
    }

</style>
<header class="main-header">
    <a href="{{url('/fscemployee')}}" class="logo" style="border:solid 5px #003d6a  !important;">
        <span class="logo-mini"><img style="width:44px" src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive"/></span>
        <span class="logo-lg"><img src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive"/></span>
    </a>
    <nav class="navbar navbar-static-top">
        <div class="">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span></a>

            @guest
            @else
                <div class="col-md-5">
                    <div class="topbar">
                        <img src="https://financialservicecenter.net/public/images/fsclogo.png"/>
                    </div>
                </div>
                <!--<div class="col-md-6 col-xs-12 col-sm-4 wdth47">-->
                <!--<div class="topbar"> <h1><span class="first-letter">F</span>inancial <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</h1></div>-->
                <!--</div>-->
                <div class="col-md-3 col-xs-12">
                    <div class="top_company top-company-name-bg">
                        <h3>FSC @include('fscemployee.layouts.clienttype',array('client'=> DB::table('employees')->where('id','=',Auth::user()->user_id)->get()))</h3>
                        <p><span style="font-size:inherit;">{{ ucwords(Auth::user()->name) }}</span></p>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12" style="padding-right:0px;">
                    <div class="top_date_section top_company top-company-name-bg pull-right" style="padding: 3px 6px;">
                        <div class="top_date"><i class="fa fa-calendar"></i><span>{{ date('F-d-Y')}}</span></div>
                        <div class="top_day"><i class="fa fa-calendar-o"></i><span>{{ date('l')}}</span></div>
                        <div class="top_time"><i class="fa fa-clock-o"></i><span id="divUsa"></span></div>
                    </div>
                </div>

                <div class="navbar-custom-menu ">
                    <div class="col-md-12">
                        <div class="top_name">
                            <li class="dropdown user user-menu" style="float: right; list-style: none;"><a href="index.php" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="fa fa-user" style="font-size: 25px !important; color: #fff" aria-hidden="true"></span>
                                </a>
                                <ul class="dropdown-menu" style="border: 1px solid #021F4E !important;min-width: 120px; width:120px;padding:0px; right:-12px;position: absolute;margin-left: -100px;">
                                    <li class="user-footer" style="border-bottom:1px solid #12186b">
                                        <a href="#" style="background:#c1f0ff !important;padding:7px 5px;border:none;" class="btn btn-default btn-flat">Profile</a>
                                    </li>
                                    <li><a href="#" style="background:#c1f0ff !important;padding:7px 5px;border:none;" class="btn btn-default btn-flat">Sign out</a></li>
                                </ul>
                            </li>
                        </div>
                    </div>
                </div>


            @endguest
        </div>
    </nav>
    <!-- Header Navbar: style can be found in header.less -->
</header>