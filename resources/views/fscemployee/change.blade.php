@extends('employee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Change Password</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form enctype='multipart/form-data' class="form-horizontal changepassword" action="http://financialservicecenter.net/user/userchangepassword/3" id="changepassword" method="post">
                            <input type="hidden" name="_token" value="0gi3MyyDqk1C5PGpCflmG1lmlber6a1hnZ21Gajj">
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group ">
                                        <label class="control-label col-md-3">Old Password :</label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                            <input type="hidden" class="form-control" id="flag" value=" 1 " name="flag">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">New Password :</label>
                                        <div class="col-md-4">
                                            <input name="newpassword" type="password" id="newpassword" class="form-control"/>
                                            <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <div id="messages"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confirm Password :</label>
                                        <div class="col-md-4">
                                            <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary fsc-form-submit" style="">Change Password</button>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {

            $('.changepassword').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    newpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            regexp:
                                {

                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                    message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                },

                            different: {
                                field: 'oldpassword',
                                message: 'The password cannot be the same as Current Password'
                            }
                        }
                    },
                    oldpassword: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Current Password'
                            },
                            remote: {
                                message: 'The Password is not available',
                                url: 'http://financialservicecenter.net/user/checkpassword1',
                                data: {
                                    type: 'oldpassword'
                                },
                                type: 'POST'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and can\'t be empty'
                            },
                            identical: {
                                field: 'newpassword',
                                message: 'The password and its confirm are not the same'
                            },
                            different: {
                                field: 'oldpassword',
                                message: 'The password can\'t be the same as Old Password'
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                $('.changepassword').data('bootstrapValidator').resetForm();
                // Prevent form submission
                e.preventDefault();
                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    // console.log(result);
                }, 'json');
            });
        });
    </script>
@endsection