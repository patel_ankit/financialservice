@extends('fscemployee.layouts.app')
@section('title', 'Task Create')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Task </h1>
        </div>
        <section class="content" style="background-color: #fff;">
            <div class="">
                <div class="col-md-12
         <div class=" card
                ">
                <div class="card-body">
                    <form method="post" action="{{route('newtask.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3">Employee :</label>
                            <div class="col-md-8">
                                <select name="employee" id="employee" class="form-control fsc-input">
                                    <option value="">Select Employee</option>

                                    @foreach($emp1 as $em)
                                        <option value='{{$em->id}}'>{{$em->firstName.' '.$em->middleName.' '.$em->lastName}}</option>

                                    @endforeach
                                </select>
                                @if ($errors->has('employee'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3">Subject :</label>
                            <div class="col-md-8">

                                <input type="text" name="title" id="title" class="form-control fsc-input">
                                <input type="hidden" value="{{ Auth::user()->id}}" name="admin_id" id="admin_id" class="form-control fsc-input">


                                @if ($errors->has('title'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Priority :</label>
                            <div class="col-md-8">
                                <div class="">
                                    <select name="priority" id="priority" class="form-control fsc-input">
                                        <option value="">Select Priority</option>
                                        <option value='Important'>Important</option>
                                        <option value='Urgent'>Urgent</option>
                                        <option value='Very Urgent'>Very Urgent</option>

                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Due Date :</label>
                            <div class="col-md-8">
                                <div class="">
                                    <input type="text" name="duedate" id="duedate" value="" class="form-control fsc-input">
                                </div>

                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3">Description :</label>
                            <div class="col-md-8">
                                <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-md-offset-3 col-md-7">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input class="btn_new_save btn-primary1 primary1" style="margin-left:-6%" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn_new_cancel" style="margin-left:-6%" href="{{url('fscemployee/newtask')}}">Cancel</a>
                                    </div>
                                    <div class="col-md-3">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
    </div>
    </div>
    </section>
    </div>

    <script>
        $(document).ready(function () {
            var dateInput = $('input[name="duedate"]');
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: truncateDate(new Date())
            });

            $('#duedate').datepicker('setStartDate', truncateDate(new Date()));
        });

        function truncateDate(date) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }
    </script>
@endsection()