<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
    <title>FSC - Reset Username</title>
    <style>
        .help-block {
            color: red
        }

        .semibold-text a {
            color: red;
        }
    </style>
</head>

<body>

<div class="employee-logo-bg">

    <section class="employee-login-content">

        <div class="employee-logo">
            <img src="{{asset('public/dashboard/images/logo_employee.png')}}" alt=""/>
        </div>

        <div class="employee-login-box">

            <form class="login-form" method="post" action="{{ route('forgotempusername.update',$business->id) }}" id="admin-login">
                {{ csrf_field() }}{{method_field('PATCH')}}
                <h3 class="login-head">Employee Reset Username</h3>

                <div class="form-group">
                    <label class="employee-control-label">Email :</label>
                    <input class="employee-form-control" type="text" value="{{$_GET['email']}}" readonly name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
                    @endif
                </div>

                <div class="form-group btn-container">
                    <button type="submit" name="submit" class="btn_submit">Reset Email</button>
                </div>


                <center>@if ( session()->has('success') )
                        <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                    @endif
                    @if ( session()->has('error') )
                        <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                    @endif
                    <div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div>
                </center>

            </form>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>

    </section>

</div>

</body>

<script src="{{asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/plugins/pace.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/main.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>


</html>