@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of Tax Authorities</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!--<div class="branch">Federal</div>-->
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">Tax Authorities / Form</h3>
                            <div class="box-tools pull-right">
                                <div class="table-title">

                                    <a href="{{route('taxfederal.create')}}">Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <br>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Type of Entity</th>
                                        <th>Type Of Tax</th>
                                        <th>Forms Name</th>
                                        <th>Due Date</th>
                                        <th>Extension Due Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($price as $bus)

                                        <tr>
                                            <td style="text-align:center;">{{$loop->index + 1}}</td>

                                            <td>{{$bus->authority_name}}</td>
                                            <td>{{$bus->typeofform}}</td>
                                            <td>{{$bus->formname}}</td>
                                            <td style="text-align:center;">{{$bus->due_date}}</td>
                                            <td style="text-align:center;">{{$bus->extension_due_date}}</td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('taxfederal.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('taxfederal.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>

                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection()