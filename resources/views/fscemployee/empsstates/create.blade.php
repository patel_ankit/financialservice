@extends('fscemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>County</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('empsstates.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">State :</label>
                                    <div class="col-md-4">
                                        <select name="state" id="state" class="form-control fsc-input">
                                            <option value="">---Select State---</option>
                                            <option value="AK">AK</option>
                                            <option value="AS">AS</option>
                                            <option value="AZ">AZ</option>
                                            <option value="AR">AR</option>
                                            <option value="CA">CA</option>
                                            <option value="CO">CO</option>
                                            <option value="CT">CT</option>
                                            <option value="DE">DE</option>
                                            <option value="DC">DC</option>
                                            <option value="FM">FM</option>
                                            <option value="FL">FL</option>
                                            <option value="GA">GA</option>
                                            <option value="GU">GU</option>
                                            <option value="HI">HI</option>
                                            <option value="ID">ID</option>
                                            <option value="IL">IL</option>
                                            <option value="IN">IN</option>
                                            <option value="IA">IA</option>
                                            <option value="KS">KS</option>
                                            <option value="KY">KY</option>
                                            <option value="LA">LA</option>
                                            <option value="ME">ME</option>
                                            <option value="MH">MH</option>
                                            <option value="MD">MD</option>
                                            <option value="MA">MA</option>
                                            <option value="MI">MI</option>
                                            <option value="MN">MN</option>
                                            <option value="MS">MS</option>
                                            <option value="MO">MO</option>
                                            <option value="MT">MT</option>
                                            <option value="NE">NE</option>
                                            <option value="NV">NV</option>
                                            <option value="NH">NH</option>
                                            <option value="NJ">NJ</option>
                                            <option value="NM">NM</option>
                                            <option value="NY">NY</option>
                                            <option value="NC">NC</option>
                                            <option value="ND">ND</option>
                                            <option value="MP">MP</option>
                                            <option value="OH">OH</option>
                                            <option value="OK">OK</option>
                                            <option value="OR">OR</option>
                                            <option value="PW">PW</option>
                                            <option value="PA">PA</option>
                                            <option value="PR">PR</option>
                                            <option value="RI">RI</option>
                                            <option value="SC">SC</option>
                                            <option value="SD">SD</option>
                                            <option value="TN">TN</option>
                                            <option value="TX">TX</option>
                                            <option value="UT">UT</option>
                                            <option value="VT">VT</option>
                                            <option value="VI">VI</option>
                                            <option value="VA">VA</option>
                                            <option value="WA">WA</option>
                                            <option value="WV">WV</option>
                                            <option value="WI">WI</option>
                                            <option value="WY">WY</option>
                                        </select>
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('county') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">County Name :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="county" id=county" class="form-control" placeholder="County Name"/>
                                        @if ($errors->has('county'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('county') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('countycode') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">County Code :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="countycode" id="countycode" class="form-control " placeholder="County Code"/>
                                        @if ($errors->has('countycode'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('countycode') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="type" id="type" readonly value="County" class="form-control num" placeholder="Type"/>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('rate') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Rate :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="rate" id="rate" class="form-control " placeholder="Rate"/>
                                        @if ($errors->has('rate'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('rate') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="{{url('fscemployee/empsstates')}}">Cancel</a>
                                    </div>
                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
@endsection()