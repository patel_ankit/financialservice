@extends('fscemployee.layouts.app')
@section('main-content')
    <style>
        .search-btn {
            position: absolute;
            top: 46px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        .searchboxmain {
            float: left;
            display: FLEX;
            margin-top: 0px;
            justify-content: space-between;
        }

        .arrow {
            width: 0px;
            position: relative;
            float: left;
        }

        .searchboxmain .form-control {
            margin-right: 10px !important;
        }

        .clear {
            clear: both;
        }

        .page-title h1 {
            float: left;
            margin-left: 15%;
            margin-top: 6px;
        }

        .page-title {
            padding: 5px 15px 0px !important;
            border-bottom: 3px solid #00a65a;
        }

        .headerbox {
        }

        .padtop7 {
            padding-top: 7px;
        }

        .disablebox input[type="text"], .disablebox select, .disablebox input[type="email"], .disablebox input[type="tel"], .disablebox input[type="number"], .disablebox textarea {
            background: #eeeeee !important;
            cursor: no-drop !important;
        }

        .pointernone {
            pointer-events: none
        }

        .pad20 {
            padding: 20px;
        }

        .nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
            cursor: default;
        }

        .nav-tabs > li {
            margin: 0px 0 0 8px;
        }

        .nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
            border-color: #000 !important;
            color: #000 !important;
            background: #ffff99;
            border-radius: 5px !important;
        }

        .panel-heading .nav-tabs > li > a {
            color: #000000 !important;
        }

        .panel-heading .nav-tabs > li > a:hover, .panel-heading .nav-tabs > li.active > a {
            color: #ffffff !important;
        }

        .nav-tabs {
            padding: 12px;
            border: 1px solid #3598dc !important;
        }

        .btnaddmore {
            background: #337ab7;
            display: inline-block;
            margin-bottom: 5px;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .btnremove {
            background: #ff0000;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .padzero {
            padding: 0px !important;
        }

        .officermainbox {
            border: 1px solid #ccc;
            padding: 20px;
            margin-bottom: 30px;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border-color: #ccc !important;
        }

        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .btnaddrecord {
            width: 120px;
            float: right;
            cursor: pointer;
        }

        .new_images_sec img {
            height: 53px;
        }

        .card ul li {
            width: 24% !important;
        }

        .card ul.test li a {
            background: #ffcc66 !important;
        }

        .card ul.test li.active a {
            background: #00a0e3 !important;
        }

        .card ul li a {
            display: block;
            width: 100%;
            color: #333;
            text-transform: capitalize;
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);
            border: 1px solid #979800 !important;
        }

        .card ul li a:hover {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover {
            color: #fff !important;
            background: #12186b !important;
            border: 1px solid #12186b !important;
        }

        .card .nav-tabs {
            border: 0px !important;
        }

        .feeschargesbox .form-control {
            text-align: right;
        }

        .hrdivider {
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
            margin-top: 33px;
        }

        .hrdivider2 {
            margin-top: 14px;
            margin-bottom: 30px;
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
        }

        .officerchange .form-control {
            background: #fff !important;
        }

        .add-row {
            background: #007bff;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .delete-row {
            background: #dc3545;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .newcmpname {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 77px;
        }

        .clear {
            clear: both;
        }

        .mbtmzero {
            margin-bottom: 0px !important;
        }

        .nav > li > a {
            padding: 10px 8px !important;
        }

        .text-left {
            text-align: left !important;
        }

        .text-center {
            text-align: center !important;
        }

        .nav > li > a {
            position: relative;
            display: block;
            padding: 10px 5px;
        }

        input[type=checkbox]:checked + label {
            background: url(https://financialservicecenter.net/public/adminlte/icon_checkbox.png) no-repeat left -24px;
            font-style: normal;
        }

        input[type=checkbox] + label {
            background: url(https://financialservicecenter.net/public/adminlte/icon_checkbox.png) no-repeat left top;
            padding-left: 30px;
            height: 25px;
        }

        input[type="file"] {
            position: relative !important;
            margin-top: 11px;
            width: 100px !important;
            font-size: 12px !important;
            display: none;
            opacity: 1 !important;
        }

        .searchboxmain .form-control {
            height: 34px !important;
        }

        .searchboxmain .btn-action {
            background: #367fa9 !important;
            padding: 8px 15px !important;
            margin-top: 0px;
        }

        .box.box-success {
            top: 0;
            border: 0px;
            display: inline-block;
        }

        .clear {
            clear: both;
        }

        /**************** dropdown list ********************/

        #countryList {
            margin: 40px 0px 0px 0px;
            padding: 0px 0px;
            border: 0px solid #ccc;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 360px;
            z-index: 99999;
        }

        #countryList li {
            border-bottom: 1px solid #025b90;
            background: #ef7c30;
        }

        #countryList li a {
            padding: 0px !important;
            display: block;
            color: #fff !important;
            height: 40px;
            margin-left: 0px !important;
        }

        #countryList li a span.bgcolors {
            background: linear-gradient(to bottom, #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;
            padding: 6px 0px;
            text-align: center;
            display: block;
            font-size: 20px;
            font-weight: bold;
            float: left;
            width: 60px;
        }

        #countryList li a span.clientalign {
            display: flex;
            width: 75px;
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 40px;
            background: #038ee0;
            margin-left: 0;
            padding-left: 5px;
        }

        #countryList li a span.entityname {
            display: flex;
            width: 200px;
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            font-size: 13px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 38px;
            margin-left: 10px;
        }

        .modal.fade.in {
            display: block !important;
            opacity: 1 !important;
            padding-top: 50px;
        }

        .tab-pane.fade {
            position: absolute;
            width: 100%;
        }

        .tab-pane.fade.active.in {
            position: relative;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                width: 800px;
                margin: 30px auto;
            }
        }

        .btnannualreceipt {

            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            line-height: 25px;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);

        }

        .btnannualreceipt:hover, .btnannualreceipt.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            color: #000000;
            transition: 0.2s;
        }

    </style>
    <div class="content-wrapper">

        <div class="page-title">
            <div class="searchboxmain">
                <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client"> <a class="btn-action btn-view-edit btn-primary" href="https://financialservicecenter.net/fscemployee/workrecord">Reset</a>
                {{ csrf_field() }}
                <ul id="countryList"></ul>
            </div>

            <h1>Work Record</h1>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success mbtmzero">

                    <div class="box-header" style="padding-left:0px; display:none">
                        <div class="col-md-9" style="">
                            <div class="row">
                                <div class="col-md-4">
                                <!-- <div class=""><label style="margin-left:2%;margin-top: 11px;">Search : </label></div>
                           <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">
                             {{ csrf_field() }}
                                        <ul id="countryList"></ul>-->
                                </div>
                                <div class="col-md-4">
                                    <div class=""><label style="margin-left:2%;margin-top: 11px;"> &nbsp;</label></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!empty($common->filename))
                        <section class="content-header" style="margin-top:8px;">
                            <div class="new-page-title" style="padding:0px;">
                                <div class="new-page-title-tab">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <div class="new_client_id">
                                            <p>{{$common->filename}} </p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-12" style="padding-right:0px;">
                                        <div class="newcmpname">
                                            @if($common->business_id=='6')
                                                <div class="new_company_name">
                                                    <label> Name :</label> {{ucwords($common->nametype)}}   {{$common->first_name}} {{$common->middle_name}} {{$common->last_name}}  </div>
                                            @else
                                                <div class="new_company_name" style="margin-top:6px !important;">
                                                    <label>Company Name :</label>
                                                    <p>{{$common->company_name}}</p>
                                                </div>
                                                <div class="new_company_name">
                                                    <label>Business Name :</label>
                                                    <p>{{$common->business_name}}</p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <div class="new_images_sec">
                                            @if(empty($common->business_cat_id))
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    @else
                                                        @if(empty($common->business_brand_category_id))
                                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                                @else
                                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                                        @endif
                                                                        @endif
                                                                        @foreach($business as $busi)
                                                                            @if($busi->id==$common->business_id)
                                                                                <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive">
                                                                                @if($common->business_id !='6')
                                                                                    <div class="arrow" style="float: right; position: absolute;  top: 15px; right: 8px;"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                                                                                @endif
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                                @foreach($category as $cate)
                                                                    @if($common->business_cat_id==$cate->id)
                                                                        @if(empty($common->business_brand_category_id))

                                                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                                            @else
                                                                                <!-- <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>-->
                                                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                                                        @endif
                                                                                        <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive"/>
                                                                                    </div>
                                                                                @endif
                                                                                @endforeach
                                                                                @foreach($cb as $bb1)
                                                                                    @if($common->business_brand_category_id == $bb1->id)

                                                                                        <div class="col-md-4 col-sm-12 col-xs-12 lastimgbox">
                                                                                            <div class="arrow" style="position: absolute; left: -6px; top: 15px;">
                                                                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                                            </div>
                                                                                            <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive"/>
                                                                                        </div>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </section>
                        <div class="clear"></div>


                    @else

                    @endif
                    <div class="col-md-12" style="margin-top:-5px; padding:0px;">
                        @if(!empty($common->filename))
                            <div class="panel with-nav-tabs panel-primary" style="padding:0px 10px;">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" style="padding:5px !important;margin:5px;" id="myTab">
                                        @if($common->business_id !='6')
                                            <li class="active" style=" width: 15.8% !important;"><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
                                            <li style=" width: 11.8% !important;"><a href="#tab2primary" data-toggle="tab">License</a></li>
                                        @endif

                                        <li style=" width: 11.8% !important;"><a href="#tab3primary" data-toggle="tab">Taxation</a></li>
                                        <li style=" width: 11.8% !important;"><a href="#tab4primary" data-toggle="tab">Banking</a></li>
                                        <li style=" width: 11.8% !important;"><a href="#tab5primary" data-toggle="tab">Income</a></li>
                                        <li style=" width: 11.8% !important;"><a href="#tab6primary" data-toggle="tab">Expense </a></li>
                                        <li <?php if($common->business_id == '6') { ?>class="active" <?php } ?>style=" width: 11.8% !important;"><a href="#tab8primary" data-toggle="tab">Others</a></li>
                                    </ul>
                                </div>

                                <div class="tab-content">
                                    @if($common->business_id !='6')
                                        <div class="tab-pane fade in active  newcheckbox" id="tab1primary">
                                            <div style="padding:10px;padding-left: 0px;padding-top: 5px;">
                                                <!-- <div class="col-md-4">Client ID &nbsp;&nbsp;&nbsp;&nbsp;      Client Name</div>
               <div class="col-md-4">Icone of Business</div>!-->
                                                <div class="clear"></div>
                                                <form method="post" action="https://financialservicecenter.net/fscemployee/workrecord/fetch1" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                    <input type="hidden" name="clientsid" id="clientsid" value="<?php echo $common->id;?>">

                                                    <div class="headerbox">
                                                        <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                                                            <div class="col-md-4" style="padding-top:10px; padding-right:0px; width:37%;padding-left:11px;">

                                                                <?php $dbcolors = $common->type_of_entity_answer; $str = explode(",", $dbcolors);
                                                                //print_r($str = explode(",", $dbcolors));die;
                                                                ?>

                                                                <label class="col-md-1 text-right padtop7" style="width: 60px;padding-left: 0px;">State :</label>
                                                                <div class="col-md-3" style="width: 95px;padding-left: 0px;padding-right: 0px;">
                                                                <!--<input type="text" value="@if(empty($str[1])) @else {{$str[1]}} @endif" style="width:86px;" class="form-control fsc-input"/>-->
                                                                    <input type="text" value="{{$common->formation_register_entity}}" style="width:86px;" class="form-control fsc-input"/>
                                                                </div>
                                                                <label class="col-md-2 text-right padtop7" style="width: 85px;padding-left: 0px;">Control #:</label>
                                                                <div class="col-md-3" style="width: 100px;padding-left: 0px;padding-right: 0px;"><input type="text" value="{{$common->contact_number}}" class="form-control fsc-input" style="width:100px;"/></div>
                                                            </div>
                                                            <div class="col-md-8" style="width:63%; padding-right:0px;">
                                                                <div class="panel-heading">
                                                                    <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                                        <li style=" width:30.2% !important;" class="active"><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>
                                                                        <li style=" width:20.2% !important;"><a href="#subtab2primary" class="" data-toggle="tab">Share Transfer</a></li>
                                                                        <li style=" width:17.2% !important;"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>
                                                                        <li style=" width:13.2% !important;"><a href="#subtab4primary" class="" data-toggle="tab">Minutes</a></li>
                                                                        <li style=" width:11.2% !important;"><a href="#subtab5primary" class="" data-toggle="tab">Docs</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-content" style="padding-top:5px; padding-left:5px;">
                                                            <div class="tab-pane fade in active  newcheckbox " id="subtab1primary">
                                                                <div class="col-md-12 padzero">
                                                                    <div class="col-md-3 padzero" style="width:175px;">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 padtop7" style="padding-left: 14px;width:60px;padding-right: 0px;">Year :</label>
                                                                            <div class="col-md-6" style="/* width: 64%; position: absolute;padding-left:13px;margin-left: 100px;*/">
                                                                                <div class="dropdown" style="width:85px">
                                                                                    <div class="dropdown" style="width:85px">
                                                                                        <?php
                                                                                        if(isset($formation->formation_yearvalue) != '')
                                                                                        {
                                                                                        ?>
                                                                                        <input type="text" class="form-control" value="{{$formation->formation_yearvalue}}" readonly/>
                                                                                        <?php
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                        $now = date('Y') - 1;
                                                                                        $cnow = date('Y');
                                                                                        ?>
                                                                                        <input type="text" name="common_year" value="{{$cnow}}" class="form-control" readonly/>
                                                                                        <?php
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group row">
                                                                            <label class="col-md-6 padtop7 mt8 text-right" style="width:120px; padding-right:0px;">Due Date <?php //echo $common->formation_yearvalue;?></label>
                                                                            <div class="col-md-6">
                                                                                <?php
                                                                                $cnows = date('Y');

                                                                                $tdatess1 = date('Y-m-d');
                                                                                $todaydates1 = date('Y-m-d', strtotime($tdatess1));
                                                                                $cyears1 = $cnows . '-04-01';
                                                                                $stryear1 = date('Y-m-d', strtotime($cyears1));


                                                                                ?>
                                                                                <input type="hidden" class="todaydates" value="<?php echo $todaydates1;?>">
                                                                                <input type="hidden" class="strdates" value="<?php echo $stryear1;?>">

                                                                                <?php
                                                                                if(isset($formation->formation_yearvalue) != '' && isset($formation->formation_yearvalue) != null)
                                                                                {
                                                                                // $int = (int)$formation->formation_yearvalue
                                                                                $int = (int)substr($formation->formation_yearvalue, -4);
                                                                                $aa = $int + 1;
                                                                                $cyears = $aa . '-04-01';
                                                                                $tdatess = date('Y-m-d');
                                                                                $stryear = date('Y-m-d', strtotime($cyears));
                                                                                $todaydates = date('Y-m-d', strtotime($tdatess));


                                                                                ?>
                                                                                <input type="text" name="common_year" class="form-control" readonly value="Apr-01-<?php echo $aa; ?>"/>
                                                                                <input type="hidden" name="cyear" value="<?php echo $cyears; ?>"/>

                                                                                <?php
                                                                                }
                                                                                else
                                                                                {
                                                                                $now = date('Y');
                                                                                $cyears = $cnow . '-04-01';
                                                                                $tdatess = date('Y-m-d');

                                                                                $stryear = date('Y-m-d', strtotime($cyears));
                                                                                $todaydates = date('Y-m-d', strtotime($tdatess));


                                                                                ?>
                                                                                <input type="text" name="common_year" class="form-control" value="Apr-01-{{$cnow}}" readonly/>
                                                                                <input type="hidden" name="cyear" value="<?php echo $cnow . '-04-01'; ?>"/>

                                                                                <?php
                                                                                }
                                                                                ?></div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 padzero" style="width:27% !important;">
                                                                        <div class="form-group">
                                                                            <label class="col-md-6 padtop7" style="padding-left:0px;width: 58% !important;text-align: right;">Any Changes? :</label>
                                                                            <div class="col-md-5 padzero">
                                                                                <div class="dropdown" style=" padding-top:6px;">

                                                                                    <?php
                                                                                    if(isset($common->work_changes) != '')
                                                                                    {
                                                                                    ?>
                                                                                    <input type="radio" name="work_changes" class="checkradio" value="Yes" <?php if (isset($common->work_changes) && $common->work_changes == 'Yes') {
                                                                                        echo 'checked';
                                                                                    }?>/> Yes &nbsp;&nbsp;&nbsp;
                                                                                    <input class="checkradio" name="work_changes" type="radio" value="No" <?php if (isset($common->work_changes) && $common->work_changes == 'No') {
                                                                                        echo 'checked';
                                                                                    }?>/> No <br/>
                                                                                    <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    ?>
                                                                                    <input type="radio" name="work_changes" class="checkradio" value="Yes"/> Yes &nbsp;&nbsp;&nbsp;
                                                                                    <input class="checkradio" name="work_changes" type="radio" value="No"/> No <br/>
                                                                                    <?php
                                                                                    }
                                                                                    ?>

                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-2" style="padding-left:0px; padding-top:3px;">

                                                                        <?php if(isset($formationsetup->state) && $formationsetup->state != '')
                                                                        {
                                                                        if($formationsetup->state == $common->formation_state)
                                                                        {
                                                                        ?>
                                                                        <a class="btn_new btn-renew nn" href="<?php echo $formationsetup->renewalwebsite;?>">Renewal</a>
                                                                        <?php
                                                                        }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="clear"></div>

                                                                </div>

                                                                <div class="hideradio" <?php if(isset($common->work_changes) == 'No' && $common->work_changes == '') { ?> style="display:none;" <?php } ?>>
                                                                    <div class="col-md-12">
                                                                        <div class="officermainbox" style="background:#f2f2f2; margin-top:0px;">
                                                                            <div class="col-md-12 text-center Branch">
                                                                                <strong style="font-size:16px">On File (On Record)</strong>
                                                                            </div>
                                                                            <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                                                                <div class="col-md-6">
                                                                                    <h4>Address On File</h4>
                                                                                    <div class="form-group" id="addresschangeto" style="border:1px solid #ccc; padding:10px;">

                                                                                        <div class="form-group row" style="margin-bottom: 5px">
                                                                                            <label class="col-md-12 padtop7">Address </label>
                                                                                            <div class="col-md-12">
                                                                                                <input type="text" value="<?php echo $common->formation_address;?>" class="form-control" readonly/>
                                                                                            </div>
                                                                                            <div class="col-md-4 padtop7">

                                                                                                <input type="text" value="<?php echo $common->formation_city;?>" class="form-control" placeholder="City" readonly/>
                                                                                            </div>
                                                                                            <div class="col-md-4 padtop7">
                                                                                                <input type="text" class="form-control" value="<?php echo $common->formation_state;?>" placeholder="State" readonly/>
                                                                                            </div>
                                                                                            <div class="col-md-4 padtop7">
                                                                                                <input type="text" class="form-control" placeholder="Zip" value="<?php echo $common->formation_zip;?>" readonly/>
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="hrdivider"></div>


                                                                                    <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                                                                        <h4 style="margin-top:30px;">Address Change To</h4>
                                                                                        <div class="form-group" id="addresschangeto" style="border:1px solid #ccc; padding:10px;">

                                                                                            <div class="form-group row" style="margin-bottom: 5px">
                                                                                                <label class="col-md-12 padtop7">Address </label>
                                                                                                <div class="col-md-12">
                                                                                                    <input type="text" class="form-control" name="common_address" id="common_address" value="<?php echo $common->work_address;?>"/>
                                                                                                </div>
                                                                                                <div class="col-md-4 padtop7">
                                                                                                    <input type="text" class="form-control" name="common_city" id="common_city" value="<?php echo $common->work_city;?>" placeholder="City"/>
                                                                                                </div>
                                                                                                <div class="col-md-4 padtop7">
                                                                                                    <select name="common_state" id="common_state" class="form-control fsc-input">
                                                                                                        <option value="">State</option>
                                                                                                        <option value='AK' <?php if ($common->work_state == 'AK') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>AK
                                                                                                        </option>
                                                                                                        <option value='AS' <?php if ($common->work_state == 'AS') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>AS
                                                                                                        </option>
                                                                                                        <option value='AZ' <?php if ($common->work_state == 'AZ') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>AZ
                                                                                                        </option>
                                                                                                        <option value='AR' <?php if ($common->work_state == 'AR') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>AR
                                                                                                        </option>
                                                                                                        <option value='CA' <?php if ($common->work_state == 'CA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>CA
                                                                                                        </option>
                                                                                                        <option value='CO' <?php if ($common->work_state == 'CO') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>CO
                                                                                                        </option>
                                                                                                        <option value='CT' <?php if ($common->work_state == 'CT') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>CT
                                                                                                        </option>
                                                                                                        <option value='DE' <?php if ($common->work_state == 'DE') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>DE
                                                                                                        </option>
                                                                                                        <option value='DC' <?php if ($common->work_state == 'DC') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>DC
                                                                                                        </option>
                                                                                                        <option value='FM' <?php if ($common->work_state == 'FM') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>FM
                                                                                                        </option>
                                                                                                        <option value='FL' <?php if ($common->work_state == 'FL') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>FL
                                                                                                        </option>
                                                                                                        <option value='GA' <?php if ($common->work_state == 'GA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>GA
                                                                                                        </option>
                                                                                                        <option value='GU' <?php if ($common->work_state == 'GU') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>GU
                                                                                                        </option>
                                                                                                        <option value='HI' <?php if ($common->work_state == 'HI') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>HI
                                                                                                        </option>
                                                                                                        <option value='ID' <?php if ($common->work_state == 'ID') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>ID
                                                                                                        </option>
                                                                                                        <option value='IL' <?php if ($common->work_state == 'IL') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>IL
                                                                                                        </option>
                                                                                                        <option value='IN' <?php if ($common->work_state == 'IN') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>IN
                                                                                                        </option>
                                                                                                        <option value='IA' <?php if ($common->work_state == 'IA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>IA
                                                                                                        </option>
                                                                                                        <option value='KS' <?php if ($common->work_state == 'KS') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>KS
                                                                                                        </option>
                                                                                                        <option value='KY' <?php if ($common->work_state == 'KY') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>KY
                                                                                                        </option>
                                                                                                        <option value='LA' <?php if ($common->work_state == 'LA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>LA
                                                                                                        </option>
                                                                                                        <option value='ME' <?php if ($common->work_state == 'ME') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>ME
                                                                                                        </option>
                                                                                                        <option value='MH' <?php if ($common->work_state == 'MH') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MH
                                                                                                        </option>
                                                                                                        <option value='MD' <?php if ($common->work_state == 'MD') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MD
                                                                                                        </option>
                                                                                                        <option value='MA' <?php if ($common->work_state == 'MA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MA
                                                                                                        </option>
                                                                                                        <option value='MI' <?php if ($common->work_state == 'MI') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MI
                                                                                                        </option>
                                                                                                        <option value='MN' <?php if ($common->work_state == 'MN') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MN
                                                                                                        </option>
                                                                                                        <option value='MS' <?php if ($common->work_state == 'MS') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MS
                                                                                                        </option>
                                                                                                        <option value='MO' <?php if ($common->work_state == 'MO') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MO
                                                                                                        </option>
                                                                                                        <option value='MT' <?php if ($common->work_state == 'MT') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MT
                                                                                                        </option>
                                                                                                        <option value='NE' <?php if ($common->work_state == 'NE') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NE
                                                                                                        </option>
                                                                                                        <option value='NV' <?php if ($common->work_state == 'NV') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NV
                                                                                                        </option>
                                                                                                        <option value='NH' <?php if ($common->work_state == 'NH') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NH
                                                                                                        </option>
                                                                                                        <option value='NJ' <?php if ($common->work_state == 'NJ') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NJ
                                                                                                        </option>
                                                                                                        <option value='NM' <?php if ($common->work_state == 'NM') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NM
                                                                                                        </option>
                                                                                                        <option value='NY' <?php if ($common->work_state == 'NY') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NY
                                                                                                        </option>
                                                                                                        <option value='NC' <?php if ($common->work_state == 'NC') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>NC
                                                                                                        </option>
                                                                                                        <option value='ND' <?php if ($common->work_state == 'ND') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>ND
                                                                                                        </option>
                                                                                                        <option value='MP' <?php if ($common->work_state == 'MP') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>MP
                                                                                                        </option>
                                                                                                        <option value='OH' <?php if ($common->work_state == 'OH') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>OH
                                                                                                        </option>
                                                                                                        <option value='OK' <?php if ($common->work_state == 'OK') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>OK
                                                                                                        </option>
                                                                                                        <option value='OR' <?php if ($common->work_state == 'OR') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>OR
                                                                                                        </option>
                                                                                                        <option value='PW' <?php if ($common->work_state == 'PW') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>PW
                                                                                                        </option>
                                                                                                        <option value='PA' <?php if ($common->work_state == 'PA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>PA
                                                                                                        </option>
                                                                                                        <option value='PR' <?php if ($common->work_state == 'PR') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>PR
                                                                                                        </option>
                                                                                                        <option value='RI' <?php if ($common->work_state == 'RI') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>RI
                                                                                                        </option>
                                                                                                        <option value='SC' <?php if ($common->work_state == 'SC') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>SC
                                                                                                        </option>
                                                                                                        <option value='SD' <?php if ($common->work_state == 'SD') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>SD
                                                                                                        </option>
                                                                                                        <option value='TN' <?php if ($common->work_state == 'TN') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>TN
                                                                                                        </option>
                                                                                                        <option value='TX' <?php if ($common->work_state == 'TX') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>TX
                                                                                                        </option>
                                                                                                        <option value='UT' <?php if ($common->work_state == 'UT') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>UT
                                                                                                        </option>
                                                                                                        <option value='VT' <?php if ($common->work_state == 'VT') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>VT
                                                                                                        </option>
                                                                                                        <option value='VI' <?php if ($common->work_state == 'VI') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>VI
                                                                                                        </option>
                                                                                                        <option value='VA' <?php if ($common->work_state == 'VA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>VA
                                                                                                        </option>
                                                                                                        <option value='WA' <?php if ($common->work_state == 'WA') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>WA
                                                                                                        </option>
                                                                                                        <option value='WV' <?php if ($common->work_state == 'WV') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>WV
                                                                                                        </option>
                                                                                                        <option value='WI' <?php if ($common->work_state == 'WI') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>WI
                                                                                                        </option>
                                                                                                        <option value='WY' <?php if ($common->work_state == 'WY') {
                                                                                                            echo 'Selected';
                                                                                                        } ?>>WY
                                                                                                        </option>
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-md-4 padtop7">
                                                                                                    <input type="text" class="form-control" name="common_zip" id="common_zip" value="<?php echo $common->work_zip;?>" placeholder="Zip"/>
                                                                                                </div>
                                                                                                <div class="clear"></div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                            <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                                                                <div class="col-md-6">
                                                                                    <h4>Officer On File</h4>
                                                                                    <div class="form-group">
                                                                                        <div class="col-md-12 padzero">
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <th>First Name</th>
                                                                                                    <th>Last Name</th>
                                                                                                    <th>Position</th>
                                                                                                </tr>
                                                                                                <?php //print_r($shareholders);exit;?>
                                                                                                @foreach($shareholders as $ak)

                                                                                                    @if($ak->agent_fname1==NULL)
                                                                                                    @else
                                                                                                        <style>
                                                                                                            .input_fields_wrap_shareholder {
                                                                                                                display: none
                                                                                                            }
                                                                                                        </style>
                                                                                                        <tr>
                                                                                                            <td><input class="form-control" name="agent_fname1[]" value="{{ $ak->agent_fname1}}" readonly type="text" id="agent_fname1" placeholder="First name" class="textonly form-control"/></td>
                                                                                                            <td><input class="form-control" name="agent_lname1[]" value="{{ $ak->agent_lname1}}" readonly type="text" placeholder="Last Name" id="agent_lname1" class="textonly form-control"/></td>
                                                                                                            <td><input class="form-control" name="agent_lname1[]" value="{{ $ak->agent_position}}" readonly type="text"></td>
                                                                                                        <!-- <td> <select class="form-control" name="agent_position[]" id="agent_position11" disabled="disabled" >
                                                          <option value="">Position</option>
                                                          <option @if($ak->agent_position=='Agent') Selected hidden @else @if(empty($agent->id)) @else  hidden @endif @endif value="Agent">Agent</option>
                                                          <option @if($ak->agent_position=='Agent') disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='CEO') selected hidden  @else @if(empty($ceo->id))   @else  hidden @endif  @endif value="CEO">CEO</option>
                                                          <option @if(($ak->agent_position=='Secretary' && $ak->agent_position=='CEO' && $ak->agent_position=='CFO')) hidden  @endif  @if($ak->agent_position=='Sec') Selected hidden @else @if(empty($sec->id))  @else  hidden  @endif @endif value="Sec">CEO / CFO / Sec.</option>
                                                          <option @if($ak->agent_position=='Agent') disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='CFO') Selected hidden @else @if(empty($cfo->id))  @else  hidden  @endif @endif value="CFO">CFO</option>
                                                          <option  @if($ak->agent_position=='Agent') disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='Secretary') Selected hidden @else @if(empty($sece->id))  @else  hidden  @endif  @endif value="Secretary" >Secretary</option>
                                                       </select>
                                                       </td>!-->
                                                                                                        </tr>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            </table>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                    </div>
                                                                                    <div class="hrdivider2"></div>
                                                                                    <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                                                                        <h4 style="margin-top:10px;">Officer Change To</h4>
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12 padzero">
                                                                                                <table class="table table-bordered" id="officerchange">
                                                                                                    <tr>
                                                                                                        <th>First Name</th>
                                                                                                        <th>Last Name</th>
                                                                                                        <th>Position</th>
                                                                                                        <th><a class="add-row"><i class="fa fa-plus"></i></a></th>
                                                                                                    </tr>
                                                                                                    <style>
                                                                                                        .input_fields_wrap_shareholder {
                                                                                                            display: none
                                                                                                        }
                                                                                                    </style>
                                                                                                    @foreach($clientposition as $clientpos)
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <input class="form-control" name="idss[]" value="{{$clientpos->id}}" type="hidden" id="id"/>
                                                                                                                <input class="form-control" name="first_name[]" value="{{$clientpos->first_name}}" type="text" placeholder="First Name" id="first_name" class="textonly form-control"/></td>
                                                                                                            <td><input class="form-control" name="last_name[]" value="{{$clientpos->last_name}}" type="text" placeholder="Last Name" id="last_name" class="textonly form-control"/></td>
                                                                                                            <td>
                                                                                                                <select class="form-control" name="position[]" id="position_1">
                                                                                                                    @if($clientpos->position)
                                                                                                                        <option value="{{$clientpos->position}}">{{$clientpos->position}}</option>
                                                                                                                    @else
                                                                                                                        <option>--Select Position--</option>
                                                                                                                    @endif
                                                                                                                    <option value="Agent">Agent</option>
                                                                                                                    <option value="CEO">CEO</option>
                                                                                                                    <option value="Sec">CEO / CFO / Sec.</option>
                                                                                                                    <option value="CFO">CFO</option>
                                                                                                                    <option value="Secretary">Secretary</option>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <td style='vertical-align:middle;'></td>
                                                                                                        </tr>
                                                                                                    @endforeach
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="clear"></div>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="col-md-7 mt8">Renew For the period : </label>
                                                                                <div class="col-md-5" style="padding-left:0px !important;">
                                                                                    <input type="hidden" name="formation_client_id" value="{{$common->id}}">
                                                                                    <?php //echo $formation->formation_yearbox;//print_r($formation);?>
                                                                                    @if(isset($formation->formation_yearbox)!='' && isset($formations->client_id)!='')

                                                                                        <input type="hidden" name="formation_yearbox11" id="formation_yearbox11" value="{{$formation->formation_yearbox}}">
                                                                                        <input type="hidden" name="formation_yearvalue_already" id="formation_yearvalue_already" value="{{$formation->formation_yearvalue}}">
                                                                                        <input type="hidden" name="formation_id" value="{{$formation->id}}">

                                                                                        <!--<select style="padding:0px !important;" class="form-control" id="formation_yearbox" name="formation_yearbox" required>-->
                                                                                        <!--    <option value="">Select </option>-->
                                                                                    <!--    <option value="1" @if($formation->formation_yearbox=='1') selected @endif>1 Year</option>-->
                                                                                    <!--    <option value="2" @if($formation->formation_yearbox=='2') selected @endif>2 Year</option>-->
                                                                                    <!--    <option value="3" @if($formation->formation_yearbox=='3') selected @endif>3 Year</option>-->
                                                                                    <!--    <option value="4" @if($formation->formation_yearbox=='4') selected @endif>4 Year</option>-->
                                                                                        <!--</select>-->

                                                                                        <input class="form-control" type="textbox" name="formation_yearbox" id="formation_yearbox" value="{{$formation->formation_yearbox}} Year" readonly>
                                                                                    @elseif(isset($formations->client_id) && $formations->client_id !='')
                                                                                        <input type="hidden" name="formation_id" value="{{$formations->id}}">

                                                                                        <select style="padding:0px !important;" class="form-control ss" id="formation_yearbox22" name="formation_yearbox" required>
                                                                                            <option value="">Select</option>
                                                                                            <option value="1">1 Year</option>
                                                                                            <option value="2">2 Year</option>
                                                                                            <option value="3">3 Year</option>
                                                                                        </select>

                                                                                    @else

                                                                                        <select style="padding:0px !important;" class="form-control" id="formation_yearbox22" name="formation_yearbox" required>
                                                                                            <option value="">Select</option>
                                                                                            <option value="1">1 Year</option>
                                                                                            <option value="2">2 Year</option>
                                                                                            <option value="3">3 Year</option>
                                                                                        </select>

                                                                                    @endif

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label class="col-md-3 padtop7">Year : </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue" value="{{ $common->formation_yearvalue}}" required readonly/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">

                                                                                <div class="feeschargesbox">
                                                                                    <div class="form-group" style="margin-top:10px;">
                                                                                        <label class="col-md-4 padtop7" style="padding-right:0px;">Paid By :</label>
                                                                                        <div class="col-md-5">
                                                                                            <div class="dropdown">
                                                                                                <select class="form-control fsc-input paidby" name="record_paid_by" style="font-size:14px!important; height:30px;">
                                                                                                    <option value="">Select</option>
                                                                                                    <option value="Client" <?php if ($common->work_paidby == 'Client') {
                                                                                                        echo 'Selected';
                                                                                                    } ?>>Client
                                                                                                    </option>
                                                                                                    <option value="FSC" <?php if ($common->work_paidby == 'FSC') {
                                                                                                        echo 'Selected';
                                                                                                    } ?>>FSC
                                                                                                    </option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                    </div>
                                                                                    <div class="hidepaid">
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-7 padtop7">Annual Fees :</label>
                                                                                            <div class="col-md-5">
                                                                                                <input class="form-control annualfees" style="height:30px;" type="textbox" readonly name="formation_amount" id="formation_amount" value="{{$common->formation_amount}}" required>
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-7 padtop7">Penalty Charges:</label>
                                                                                            <div class="col-md-5">
                                                                                                @if(isset($formation->formation_penalty)!='')
                                                                                                    <input class="form-control penaltycharges" style="height:30px;" type="textbox" name="formation_penalty" id="formation_penalty" value="@if($formation->formation_penalty !='') ${{$formation->formation_penalty}} @else @endif">
                                                                                                @else
                                                                                                    <input class="form-control penaltycharges" style="height:30px;" type="textbox" name="formation_penalty" id="formation_penalty">
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>

                                                                                        <div class="form-group" style="margin-top:10px;">
                                                                                            <label class="col-md-7 padtop7">Processing Charges :</label>
                                                                                            <div class="col-md-5"><input type="text" class="form-control processingfees" name="work_processingfees" value="{{$common->work_processingfees}}" style="height:30px;"/></div>
                                                                                            <div class="clear"></div>
                                                                                        </div>

                                                                                        <div class="form-group" style="margin-top:10px;">
                                                                                            <label class="col-md-7 padtop7">Total Amount :</label>
                                                                                            <div class="col-md-5">

                                                                                                @if(isset($formation->formation_amount)!='')
                                                                                                    <input type="text" readonly class="form-control totalamt" name="record_totalamt" style="height:30px;" value="@if($formation->record_totalamt !='') ${{$formation->record_totalamt}} @else @endif"/>
                                                                                                @else

                                                                                                    <input type="text" readonly class="form-control totalamt" name="record_totalamt" style="height:30px;"/>
                                                                                                @endif


                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>

                                                                                        <div class="form-group" style="margin-top:10px;">
                                                                                            <label class="col-md-7 padtop7">Payment Method :</label>
                                                                                            <div class="col-md-5"><input type="text" name="formation_payment" class="form-control" value="<?php echo $common->formation_payment;?>" style="height:30px;"/></div>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                        <div class="form-group" style="margin-top:10px;">
                                                                                            <label class="col-md-7 padtop7">Paid Date :</label>
                                                                                            <div class="col-md-5">
                                                                                                @if(isset($formation->paiddate)!='')
                                                                                                    <input type="text" name="paiddate" class="form-control" value="<?php if ($formation->paiddate != '') {
                                                                                                        echo date('M-d Y', strtotime($formation->paiddate));
                                                                                                    }?>" id="paiddate" style="height:30px;"/>
                                                                                                @else
                                                                                                    <input type="text" name="paiddate" class="form-control" id="paiddate" style="height:30px;"/>
                                                                                                @endif
                                                                                            </div>
                                                                                            <?php //echo $common->paiddate;?>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group" style="margin-top:25px;">
                                                                                    <label class="col-md-2 padtop7">Annual Receipt :</label>
                                                                                    <div class="col-md-3">

                                                                                        <label class="file-upload btn btn-primary">
                                                                                            <input type="file" class="form-control" name="annualreceipt" value="{{ $common->annualreceipt}}" id="annualreceipt"/>
                                                                                            Browse for file ... </label>
                                                                                        <input type="hidden" name="annualreceipt_1" value="{{ $common->annualreceipt}}"/><?php //echo $common->annualreceipt?>

                                                                                    </div>
                                                                                    @if(isset($formation->annualreceipt)!='' && $formation->annualreceipt!=null)
                                                                                        <div class="col-md-3">
                                                                                            <a data-toggle="modal" num="Ann" class="btn btn-info btnannualreceipt btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnannual Pro-btn"
                                                                                               style="padding: 0px;height: 34px;padding-top: 3px;">Annual Receipt</a>
                                                                                        </div>
                                                                                    @endif

                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                                <div class="clear"></div>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="form-group" style="margin-top:5px;">
                                                                                    <label class="col-md-2 padtop7">Officer :</label>
                                                                                    <div class="col-md-3">

                                                                                        <label class="file-upload btn btn-primary">
                                                                                            <input type="file" class="form-control" name="formation_work_officer" value="{{ $common->formation_work_officer}}" id="formation_work_officer"/>
                                                                                            Browse for file ... </label>
                                                                                        <input type="hidden" class="form-control" name="officers" value="{{$common->formation_work_officer}}"><?php //echo $common->formation_work_officer?>

                                                                                    </div>
                                                                                    @if(isset($formation->formation_work_officer)!='' && $formation->formation_work_officer!=null)
                                                                                        <div class="col-md-3">
                                                                                            <a data-toggle="modal" num="off" class="btn btnannualreceipt btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnofficer Pro-btn"
                                                                                               style="padding: 0px;height: 34px; padding-top: 3px;">Officer</a>

                                                                                        </div>
                                                                                    @endif

                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                                <div class="clear"></div>
                                                                            </div>

                                                                        <!-- <div class="col-md-12">
                                       <div class="form-group" style="margin-top:5px; padding-left:0px;">
                                           <label class="col-md-2 padtop7">Status : {{$lastformation1->record_status}}</label>
                                           <div class="col-md-6">
                                                <?php
                                                                        if(empty($lastformation))
                                                                        {
                                                                        ?>

                                                                                <select class="form-control" name="record_status">
                                                 <option value="Active Owes Curr. Yr. AR" <?php if (!empty($lastformation1) && $lastformation1->record_status == 'Active Owes Curr. Yr. AR') {
                                                                            echo 'Selected';
                                                                        }?>>Active Owes Curr. Yr. AR</option>
                                                 <option value="Active Compliance" <?php if (!empty($lastformation1) && $lastformation1->record_status == 'Active Compliance') {
                                                                            echo 'Selected';
                                                                        }?>>Active Compliance</option>
                                                 <option value="Active Noncompliance" <?php if (!empty($lastformation1) && $lastformation1->record_status == 'Active Noncompliance') {
                                                                            echo 'Selected';
                                                                        }?>>Active Noncompliance</option>
                                                 <option value="Admin. Dissolved" <?php if (!empty($lastformation1) && $lastformation1->record_status == 'Admin. Dissolved') {
                                                                            echo 'Selected';
                                                                        }?>>Admin. Dissolved</option>
                                                 <option value="Dis-Cancel-Termin" <?php if (!empty($lastformation1) && $lastformation1->record_status == 'Dis-Cancel-Termin') {
                                                                            echo 'Selected';
                                                                        }?>>Dis-Cancel-Termin</option>

                                             </select>
                                             <?php
                                                                        }
                                                                        ?>

                                                                                </div>

                                           <div class="clear"></div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>!-->


                                                                        </div>
                                                                    </div>


                                                                    <div class="clear"></div>
                                                                </div>

                                                                <div class="clear"></div>

                                                                <div class="row" style="margin-top:12px;">
                                                                    <div class="form-group col-md-12">
                                                                        <label class="col-md-2 padtop7">Note : </label>
                                                                        <div class="col-md-6">
                                                                            <textarea class="form-control" name="record_note" style="height:70px;"><?php echo $common->work_note;?> </textarea>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group" style="margin-top: 20px;">
                                                                        <div class="card-footer">
                                                                            <div class="row">
                                                                                <div class="col-md-2 col-md-offset-4">
                                                                                    <input class="btn_new_save btn-primary1" type="submit" value="Save" style="disabled:true">
                                                                                </div>
                                                                                <div class="col-md-2 row">
                                                                                    <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord">Cancel</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="col-md-6">


                                                                </div>

                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="tab-pane fade newcheckbox" id="subtab2primary">
                                                                Share Transfer
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="tab-pane fade newcheckbox" id="subtab3primary">
                                                                Amendment
                                                            </div>

                                                            <div class="clear"></div>
                                                            <div class="tab-pane fade  newcheckbox" id="subtab4primary">
                                                                Minutes
                                                            </div>

                                                </form>
                                                <div class="clear"></div>
                                                <div class="tab-pane fade newcheckbox" id="subtab5primary">
                                                    <div class="col-md-12">
                                                        <div class="row form-group">

                                                            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="https://financialservicecenter.net/fscemployee/workrecord/updatedocument">
                                                                {{csrf_field()}}

                                                                <div class="col-md-12">
                                                                    <div class="row form-group">
                                                                        <label class="col-md-2" style="padding-top:6px;">Document Name</label>
                                                                        <div class="col-md-3">
                                                                            <input type="hidden" name="filename_id1" value="{{$common->filename}}">
                                                                            <input type="hidden" name="client_id1" value="{{$common->id}}">
                                                                            <select class="js-example-tags form-control" style="width:85%;" name="documentsname" id="vendor_product" required>
                                                                                @foreach($document as $cur)
                                                                                    <option value="{{$cur->documentname}}">{{$cur->documentname}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('documentsname'))
                                                                                <span class="help-block">
                                                           <strong>{{ $errors->first('documentsname') }}</strong>
                                                           </span>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-2" style="padding-top:6px;"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius">Add</a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius">Remove</a></div>

                                                                        <div class="col-md-4">
                                                                            <label class="col-md-3" style="padding-top:6px;">Upload</label>
                                                                            <div class="col-md-9">

                                                                                <label class="file-upload btn btn-primary">

                                                                                    <input type="file" class="form-control" name="clientdocument" required/>
                                                                                    Browse for file ... </label>
                                                                            </div>
                                                                        </div>

                                                                        </br>   </br>   </br>

                                                                        <div class="col-md-2" style=" padding-right:3px;">
                                                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                        </div>
                                                                        <div class="col-md-2" style="padding-left:3px;">
                                                                            <a class="btn_new_cancel" href="#">Cancel</a>
                                                                        </div>


                                                                    </div>
                                                                </div>

                                                            </form>


                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                </div>
                            </div>
                        @endif

                        <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Document Name</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="" method="post" id="ajax">
                                        {{csrf_field()}}
                                        <div class="modal-body">
                                            <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Document Name"/>
                                            <input type="hidden" id="filename_id" name="filename_id" class="form-control" value="{{$common->filename}}"/>
                                            <input type="hidden" id="client_ids" name="client_ids" class="form-control" value="{{$common->id}}"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="addopt" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="background:#038ee0;">
                                        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Documents
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </h4>
                                    </div>


                                    <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                                        <div class="curency curency_ref" id="div">
                                            @foreach($document as $cur)
                                                <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                                                    <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                                        <a class="delete" style="color:#000;" onclick="return confirm('Are you sure to remove this record?')" href="{{route('workrecord.destroydocumenttitle',$cur->id)}}" id="{{$cur->id}}">{{$cur->documentname}}
                                                            <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="modal-footer" style="text-align:center;">
                                        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- <div class="tab-pane fade" id="tab2primary">-->

                        <!--                                     <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">-->

                        <!--                                       <div class="col-md-12" style="width: 63%; padding-right:0px;">-->
                        <!--                                               <div class="panel-heading">-->
                        <!--                                                   <ul class="nav nav-tabs" id="myTab2"  style="padding:5px !important;">-->
                        <!--                                                       <li style=" width: 30.2% !important;" class="active"><a href="#subtab8primary" class="" data-toggle="tab">Business License</a></li>-->
                        <!--                                                       <li style=" width: 33.2% !important;"><a href="#subtab9primary" class="" data-toggle="tab">Professional License</a></li>-->

                        <!--                                                   </ul>-->
                        <!--                                                   </div>-->
                        <!--                                         </div>-->
                        <!--                                     </div>-->

                        <!--                                     <div class="tab-pane fade in active  newcheckbox" id="subtab8primary">-->
                        <!--	    <div class="">-->
                        <!--						<div class="Branch" style="text-align:left;padding-left: 15px;">-->
                        <!--							<h1>Business License</h1>-->
                        <!--						</div>-->
                        <!--					</div>-->

                        <!--	    <div class="col-md-12 col-sm-12 col-xs-12">-->
                    <!--		<div class="form-group {{ $errors->has('business_license_jurisdiction') ? ' has-error' : '' }}" >-->
                        <!--			<div class="col-md-12 col-sm-12 col-xs-12">-->
                        <!--				<div class="col-md-3">-->
                        <!--					<label class="control-label" style="font-size:15px;"> Jurisdiction :</label>-->
                        <!--					<select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">-->
                        <!--						<option value="">Select</option>-->
                    <!--						<option value="City" @if(Auth::user()->business_license_jurisdiction=='City') selected @endif>City</option>-->
                    <!--						<option value="County" @if(Auth::user()->business_license_jurisdiction=='County') selected @endif>County</option>-->
                        <!--					</select>-->
                    <!--					@if ($errors->has('business_license_jurisdiction'))-->
                        <!--					<span class="help-block">-->
                    <!--					<strong>{{ $errors->first('business_license_jurisdiction') }}</strong>-->
                        <!--					</span>-->
                        <!--					@endif-->
                        <!--				</div>-->
                        <!--				<div class="col-md-3">-->
                        <!--					<label id="city-change" class="control-label" style="display:none;">City :</label> -->
                        <!--					<label id="county-change" class="control-label">County :</label>-->
                    <!--					@if(Auth::user()->business_license_jurisdiction=='County')-->
                        <!--					<select type="text" class="form-control" id="business_license2" name="business_license2">-->
                        <!--						<option value="">Select</option>-->


                        <!--					</select>-->
                    <!--					@elseif(Auth::user()->business_license_jurisdiction=='City')-->
                    <!--					<input type="text" class="form-control" id="business_license3"  name="business_license2" value="{{Auth::user()->business_license2}}">-->
                        <!--					@else-->
                    <!--						<select type="text" class="form-control" id="business_license2" name="business_license2" @if(Auth::user()->business_license_jurisdiction=='City') style="display:none;" @endif>-->
                        <!--						<option value="">Select</option>-->
                    <!--						@foreach($taxstate as $v)-->
                    <!--						<option value="{{$v->county}}" @if($v->county==Auth::user()->business_license2) selected @endif>{{$v->county}}</option>-->
                        <!--						@endforeach-->
                        <!--					</select>-->
                        <!--					@endif-->
                        <!--						<div id="business_license4"></div>-->
                        <!--						<div id="business_license5"></div>-->
                        <!--				</div>-->
                        <!--				<div class="col-md-2">-->
                        <!--					<label id="city-change1" class="control-label" style="display:none;">City # :</label> -->
                        <!--					<label id="county-change1" class="control-label">County # :</label>-->
                    <!--					<input name="business_license1" placeholder="" value="{{ Auth::user()->business_license1}}" type="text" id="business_license1" class="form-control" />-->
                    <!--					@if ($errors->has('business_license1'))-->
                        <!--					<span class="help-block">-->
                    <!--						<strong>{{ $errors->first('business_license1') }}</strong>-->
                        <!--					</span>-->
                        <!--					@endif-->
                        <!--				</div>-->
                        <!--				<div class="col-md-2">-->
                        <!--					<label id="county-change1" class="control-label">License # :</label>-->
                    <!--					<input name="business_license3" placeholder=""  value="{{ Auth::user()->business_license3}}" type="text" id="business_license1" class="form-control" />-->
                    <!--					@if ($errors->has('business_license3'))-->
                        <!--					<span class="help-block">-->
                    <!--						<strong>{{ $errors->first('business_license3') }}</strong>-->
                        <!--					</span>-->
                        <!--					@endif-->
                        <!--				</div>-->
                        <!--				<div class="col-md-2">-->
                        <!--					<label class="control-label">Expire Date :</label>-->
                    <!--					<input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="{{ Auth::user()->due_date2}}">-->
                        <!--				</div>-->
                        <!--			</div>-->
                        <!--		</div>-->
                        <!--		<div class="form-group">-->
                        <!--			<div class="col-md-12 col-sm-12 col-xs-12">-->
                        <!--				<div class="col-md-6">-->
                        <!--					<label class="control-label" style="font-size:15px;padding:0;">Note :</label>-->
                    <!--					<input name="notes" placeholder="Note" value="{{ Auth::user()->notes}}" type="text" id="" class="form-control">-->
                    <!--				</div>	<?php $id1 = Auth::user()->business_license_jurisdiction;?>-->
                        <!--<div class="col-md-2">-->
                        <!--	<label></label>-->
                        <!--	<a class="btn_new btn-view-license">License History</a>-->
                        <!--</div>-->

                        <!--				<div class="col-md-2">-->
                        <!--					<label></label>-->
                    <!--					<a style="display:block;" data-toggle="modal" num="{{$id1}}" class="btn_new openBtn btn-view-license">View License</a>-->
                        <!--				</div>-->
                        <!--				<div class="col-md-2">-->
                        <!--																					<style>.nn{ display:none !important}</style>-->
                        <!--						<label></label>-->
                        <!--				<a style="display:block;" href="" target="_blank" class="btn_new btn-renew">Renew Now</a>-->

                        <!--					<label></label>-->
                        <!--				<a href="#" class="btn_new btn-renew nn">Renew Now</a>-->
                        <!--				</div>-->
                        <!--			</div>-->
                        <!--		</div>-->
                        <!--	</div>-->

                        <!--    </div>-->


                        <!--	<div class="tab-pane fade  newcheckbox" id="subtab9primary">-->
                        <!--					<div class="">-->
                        <!--						<div class="Branch" style="text-align:left; padding-left:15px;">-->
                        <!--							<h1>Professional License</h1>-->
                        <!--						</div>-->
                        <!--					</div>-->
                        <!--					<br/>-->
                        <!--					<div class="">-->
                        <!--						<div class="field_wrapper">-->
                        <!--							<div id="field0">-->
                        <!--								<div class="professional_tabs_main">-->
                        <!--									<div class="professional_tabs">-->
                        <!--										<div class="professional_tab professional_profession">-->
                        <!--											<label>Profession :</label>-->
                        <!--											<select type="text" class="form-control-insu" id="" name="profession[]">-->
                        <!--												<option value="">Select</option>-->

                        <!--											</select>-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_state">-->
                        <!--											<label>State :</label>-->
                        <!--											<select name="profession_state[]" id="profession_state" class="form-control-insu">-->
                        <!--											<option value="AK">AK</option>-->
                        <!--											<option value="AS">AS</option>-->
                        <!--											<option value="AZ">AZ</option>-->
                        <!--											<option value="AR">AR</option>-->
                        <!--											<option value="CA">CA</option>-->
                        <!--											<option value="CO">CO</option>-->
                        <!--											<option value="CT">CT</option>-->
                        <!--											<option value="DE">DE</option>-->
                        <!--											<option value="DC">DC</option>-->
                        <!--											<option value="FM">FM</option>-->
                        <!--											<option value="FL">FL</option>-->
                        <!--											<option value="GA">GA</option>-->
                        <!--											<option value="GU">GU</option>-->
                        <!--											<option value="HI">HI</option>-->
                        <!--											<option value="ID">ID</option>-->
                        <!--											<option value="IL">IL</option>-->
                        <!--											<option value="IN">IN</option>-->
                        <!--											<option value="IA">IA</option>-->
                        <!--											<option value="KS">KS</option>-->
                        <!--											<option value="KY">KY</option>-->
                        <!--											<option value="LA">LA</option>-->
                        <!--											<option value="ME">ME</option>-->
                        <!--											<option value="MH">MH</option>-->
                        <!--											<option value="MD">MD</option>-->
                        <!--											<option value="MA">MA</option>-->
                        <!--											<option value="MI">MI</option>-->
                        <!--											<option value="MN">MN</option>-->
                        <!--											<option value="MS">MS</option>-->
                        <!--											<option value="MO">MO</option>-->
                        <!--											<option value="MT">MT</option>-->
                        <!--											<option value="NE">NE</option>-->
                        <!--											<option value="NV">NV</option>-->
                        <!--											<option value="NH">NH</option>-->
                        <!--											<option value="NJ">NJ</option>-->
                        <!--											<option value="NM">NM</option>-->
                        <!--											<option value="NY">NY</option>-->
                        <!--											<option value="NC">NC</option>-->
                        <!--											<option value="ND">ND</option>-->
                        <!--											<option value="MP">MP</option>-->
                        <!--											<option value="OH">OH</option>-->
                        <!--											<option value="OK">OK</option>-->
                        <!--											<option value="OR">OR</option>-->
                        <!--											<option value="PW">PW</option>-->
                        <!--											<option value="PA">PA</option>-->
                        <!--											<option value="PR">PR</option>-->
                        <!--											<option value="RI">RI</option>-->
                        <!--											<option value="SC">SC</option>-->
                        <!--											<option value="SD">SD</option>-->
                        <!--											<option value="TN">TN</option>-->
                        <!--											<option value="TX">TX</option>-->
                        <!--											<option value="UT">UT</option>-->
                        <!--											<option value="VT">VT</option>-->
                        <!--											<option value="VI">VI</option>-->
                        <!--											<option value="VA">VA</option>-->
                        <!--											<option value="WA">WA</option>-->
                        <!--											<option value="WV">WV</option>-->
                        <!--											<option value="WI">WI</option>-->
                        <!--											<option value="WY">WY</option>-->
                        <!--											</select>-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_effective">-->
                        <!--											<label>Effective Date :</label>-->
                        <!--											<input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_license">-->
                        <!--											<label>License # :</label>-->
                        <!--											<input name="profession_license[]" placeholder="License"  value="" type="text" id="profession_license" class="form-control-insu">-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_expire">-->
                        <!--											<label>Expire Date :</label>-->

                        <!--									<label></label>-->
                        <!--							  <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>-->


                        <!--							<input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success" readonly>-->
                        <!--										</div>-->



                        <!--										<div class="professional_tab professional_note">-->
                        <!--											<label>Note :</label>-->
                        <!--											<input name="profession_note[]" placeholder="Note"  value="" type="text" id="profession_note" class="form-control-insu">-->
                        <!--											<input name="profession_id[]" placeholder="Expire Date" value="" type="hidden"  class="form-control-insu">-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_state">-->
                        <!--											<label>CE :</label>-->

                        <!--											<input name="ce[]"  type="hidden" />-->
                        <!--											<input name="ce1[]"   type="checkbox"><label for="ce1" class="form-control-insu" style="background-color: transparent;background-image: none;border:transparent;"> Check-->
                        <!--											</label>-->
                        <!--										</div>-->

                        <!--										<div class="professional_tab  professional_effective_1">-->
                        <!--											<label></label>-->
                        <!--										    	<a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>-->
                        <!--										</div>-->
                        <!--										<div class="professional_tab professional_license_1">-->

                        <!--									<label></label>-->
                        <!--							    <a data-toggle="modal" num="" class="btn_new btn-view-license openBtn Pro-btn">View License</a>-->

                        <!--								<label class="btn-success"></label>-->
                        <!--								 <a class="btn_new btn-view-license Pro-btn  btn-success">File Not Upload</a>-->
                        <!--							</div>-->
                        <!--										<div class="professional_tab  professional_expire_1">-->



                        <!--									<label></label>-->
                        <!--							    <a href="" class="btn_new btn-renew" target='_blank'>Renew Now</a>-->
                        <!--							  															<label class="btn-success"></label>-->
                        <!--							<a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a> -->
                        <!--										</div>-->
                        <!--									</div>-->
                        <!--								</div>-->
                        <!--							</div>-->
                        <!--						</div>-->

                        <!--				</div>-->

                        <!--	</div>-->









                        <!--</div>-->


                        <div class="tab-pane fade" id="tab2primary">

                            <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">

                                <div class="col-md-12" style="width: 63%; padding-right:0px;">
                                    <div class="panel-heading">
                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                            <li style=" width: 30.2% !important;" class="active"><a href="#subtab8primary" class="" data-toggle="tab">Business License</a></li>
                                            <li style=" width: 33.2% !important;"><a href="#subtab9primary" class="" data-toggle="tab">Professional License</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="tab-pane fade in active  newcheckbox" id="subtab8primary">
                                <div class="col-md-12">
                                    <div class="Branch" style="text-align:left;padding-left: 15px;">
                                        <h1 class="text-center">Business License History</h1>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton pull-right" data-toggle="modal" data-target="#myModalbusinesspopup" style="width:10% !important;">Add Record</a>

                                    <div class="clear"></div>
                                    <table class="table table-hover table-bordered dataTable no-footer">
                                        <thead>
                                        <tr>
                                            <th style="width:70px;">Year</th>

                                            <th style="width:130px;">License Gross</th>
                                            <th style="width:130px;">License Fee</th>

                                            <th style="width:110px;">License #</th>
                                            <th style="width:130px;">License Issue Date</th>
                                            <th style="width:100px;">License Copy</th>
                                            <th style="width:80px">Status</th>
                                            <th style="width:100px">Action</th>
                                            <th style="width:80px">Form</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @foreach($buslicense as $bus_lic)

                                                <td class="text-center"> {{ $bus_lic->license_year}}</td>

                                                <td class="text-center">{{ $bus_lic->license_gross}}</td>
                                                <td class="text-center">{{ $bus_lic->license_fee}}</td>

                                                <td> {{ $bus_lic->license_no}}</td>
                                                <td class="text-center"><?php echo date('m/d/Y', strtotime($bus_lic->license_renew_date))?></td>
                                                <td class="text-center"><a href="{{url('public/adminupload')}}/{{$bus_lic->license_copy}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
                                                <td class="text-center">{{$bus_lic->license_status}}</td>
                                                <td class="text-center">
                                                    <a class="btn-action btn-view-edit" style="background:#367fa9 !important"><i class="fa fa-edit"></i></a>
                                                    <a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>

                                        </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>


                            </div>

                            <div class="clear"></div>
                            <div class="tab-pane fade  newcheckbox" id="subtab9primary">
                                <div class="col-md-12">
                                    <div class="Branch" style="text-align:left; padding-left:15px;">
                                        <h1>Professional License</h1>
                                    </div>
                                </div>
                                <br/>
                                <div class="">
                                    <div class="field_wrapper">
                                        <div id="field0">
                                            <div class="professional_tabs_main">
                                                <div class="professional_tabs">
                                                    <div class="professional_tab professional_profession">
                                                        <label>Profession :</label>
                                                        <select type="text" class="form-control-insu" id="" name="profession[]">
                                                            <option value="">Select</option>

                                                        </select>
                                                    </div>
                                                    <div class="professional_tab professional_state">
                                                        <label>State :</label>
                                                        <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                            <option value="AK">AK</option>
                                                            <option value="AS">AS</option>
                                                            <option value="AZ">AZ</option>
                                                            <option value="AR">AR</option>
                                                            <option value="CA">CA</option>
                                                            <option value="CO">CO</option>
                                                            <option value="CT">CT</option>
                                                            <option value="DE">DE</option>
                                                            <option value="DC">DC</option>
                                                            <option value="FM">FM</option>
                                                            <option value="FL">FL</option>
                                                            <option value="GA">GA</option>
                                                            <option value="GU">GU</option>
                                                            <option value="HI">HI</option>
                                                            <option value="ID">ID</option>
                                                            <option value="IL">IL</option>
                                                            <option value="IN">IN</option>
                                                            <option value="IA">IA</option>
                                                            <option value="KS">KS</option>
                                                            <option value="KY">KY</option>
                                                            <option value="LA">LA</option>
                                                            <option value="ME">ME</option>
                                                            <option value="MH">MH</option>
                                                            <option value="MD">MD</option>
                                                            <option value="MA">MA</option>
                                                            <option value="MI">MI</option>
                                                            <option value="MN">MN</option>
                                                            <option value="MS">MS</option>
                                                            <option value="MO">MO</option>
                                                            <option value="MT">MT</option>
                                                            <option value="NE">NE</option>
                                                            <option value="NV">NV</option>
                                                            <option value="NH">NH</option>
                                                            <option value="NJ">NJ</option>
                                                            <option value="NM">NM</option>
                                                            <option value="NY">NY</option>
                                                            <option value="NC">NC</option>
                                                            <option value="ND">ND</option>
                                                            <option value="MP">MP</option>
                                                            <option value="OH">OH</option>
                                                            <option value="OK">OK</option>
                                                            <option value="OR">OR</option>
                                                            <option value="PW">PW</option>
                                                            <option value="PA">PA</option>
                                                            <option value="PR">PR</option>
                                                            <option value="RI">RI</option>
                                                            <option value="SC">SC</option>
                                                            <option value="SD">SD</option>
                                                            <option value="TN">TN</option>
                                                            <option value="TX">TX</option>
                                                            <option value="UT">UT</option>
                                                            <option value="VT">VT</option>
                                                            <option value="VI">VI</option>
                                                            <option value="VA">VA</option>
                                                            <option value="WA">WA</option>
                                                            <option value="WV">WV</option>
                                                            <option value="WI">WI</option>
                                                            <option value="WY">WY</option>
                                                        </select>
                                                    </div>
                                                    <div class="professional_tab professional_effective">
                                                        <label>Effective Date :</label>
                                                        <input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                    </div>
                                                    <div class="professional_tab professional_license">
                                                        <label>License # :</label>
                                                        <input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu">
                                                    </div>
                                                    <div class="professional_tab professional_expire">
                                                        <label>Expire Date :</label>

                                                        <label></label>
                                                        <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>


                                                        <!--<input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success" readonly>-->
                                                    </div>


                                                    <div class="professional_tab professional_note">
                                                        <label>Note :</label>
                                                        <input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu">
                                                        <input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" class="form-control-insu">
                                                    </div>
                                                    <div class="professional_tab professional_state">
                                                        <label>CE :</label>

                                                        <input name="ce[]" type="hidden"/>
                                                        <input name="ce1[]" id="ce1" type="checkbox"><label for="ce1" class="form-control-insu" style="border:none;box-shadow:none;line-height: 0.6;margin-top: 9px;"> Check
                                                        </label>
                                                    </div>

                                                    <div class="professional_tab  professional_effective_1">
                                                        <label></label>
                                                        <a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
                                                    </div>
                                                    <div class="professional_tab professional_license_1">

                                                        <label></label>
                                                        <a data-toggle="modal" num="" class="btn_new btn-view-license openBtn Pro-btn">View License</a>

                                                        <label class="btn-success"></label>
                                                        <a class="btn_new btn-view-license Pro-btn  btn-success">File Not Upload</a>
                                                    </div>
                                                    <div class="professional_tab  professional_expire_1">


                                                        <label></label>
                                                        <a href="" class="btn_new btn-renew" target='_blank'>Renew Now</a>
                                                        <label class="btn-success"></label>
                                                        <a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="clear"></div>


                        </div>
                        <div class="clear"></div>

                        <div class="tab-pane fade" id="tab3primary">

                            <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">

                                <div class="col-md-12" style="width: 63%; padding-right:0px; margin-top:5px;">
                                    <div class="panel-heading">
                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                            <!--<li style=" width: 30.2% !important;" class="active"><a href="#subtab5primary" class="" data-toggle="tab">Income Tax</a></li>-->


                                            <?php
                                            if($common->business_id != '6')
                                            {?>
                                            <li style=" width: auto !important;" class="active"><a href="#subtab55primary" class="" data-toggle="tab">Income Tax</a></li>
                                            <li style=" width: auto !important;"><a href="#subtab6primary" class="" data-toggle="tab">Payroll Tax</a></li>
                                            <li style=" width: auto !important;"><a href="#subtab7primary" class="" data-toggle="tab">Personal Property Tax</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="clear"></div>
                            <div class="tab-pane fade newcheckbox active in" id="subtab55primary">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <a class="btn_new btn-renew btnaddrecord" style="margin:10px 0px;" data-toggle="modal" data-target="#myModalAddrecord">Add Record</a>
                                    <div class="Branch">
                                        <div class="col-md-12">
                                            <h1 class="text-center">Income Tax </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade newcheckbox " id="subtab6primary">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="Branch">
                                            <div class="col-md-3" style="text-align:left;">
                                                <h1>Federal / State</h1>
                                            </div>
                                            <div class="col-md-6">
                                                <h1>Payroll Tax </h1>
                                            </div>
                                        </div>
                                    </div>


                                    Payroll Tax
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="tab-pane fade newcheckbox" id="subtab7primary">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="Branch">
                                            <div class="col-md-3" style="text-align:left;">
                                                <h1>Federal / State</h1>
                                            </div>
                                            <div class="col-md-6">
                                                <h1>Personal Property Tax </h1>
                                            </div>
                                        </div>
                                    </div>


                                    Personal Property Tax
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="tab-pane fade" id="tab4primary">
                            <div class="card">
                                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">

                                    <div class="col-md-12" style="width: 63%; padding-right:0px;">
                                        <div class="panel-heading">
                                            <ul class="nav nav-tabs" id="myTab02" style="padding:5px !important;">
                                                <li style=" width: 30.2% !important;" class="active"><a href="#subtab5primaryi02" class="" data-toggle="tab" aria-expanded="true">Banking</a></li>
                                                <li style=" width: 22.2% !important;"><a href="#subtab6primary02" class="" data-toggle="tab" aria-expanded="true">Credit Card</a></li>
                                                <li style=" width:20% !important;"><a href="#subtab7primary02" class="" data-toggle="tab" aria-expanded="true">Loan</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="subtab5primaryi02">
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a style="display:block; float:right; cursor:pointer;width:120px; margin:10px 0px;" href="#myModalbankingpopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbankingpopup">Add Record</a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="subtab6primary02">
                                        Credit Card
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="subtab7primary02">
                                        Loan
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="tab-pane <?php if($common->business_id == '6') { ?>fade in active<?php } ?>" id="tab8primary">

                            <div class="card-body col-md-offset-1">
                                <div class="row">
                                    <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord/updatetaxation" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                            <input type="hidden" name="client_id" value="{{$common->id}}">
                                            <input type="hidden" name="personal_taxid" value="{{$common->personal_taxid}}">

                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right" style="padding-left: 72px;padding-right: 7px;margin-top: 7px;">Filing Type: <span class="star-required">*</span></label>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select class="form-control fsc-input" id="filing_type" name="filing_type" onchange="myFunction(this)" required>
                                                            <option value="">Select</option>
                                                            <option value="Regular Filing">Regular Filing</option>
                                                            <option value="Extension Filing">Extension Filing</option>
                                                            <option value="Amendment Filing">Amendment Filing</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right">Filing Year :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">

                                                        <input type="text" class="form-control regular2" id="filing_year" name="filing_year" readonly/>

                                                        <select class="form-control fsc-input amendment2" id="filing_year" name="filing_year2" style="display:none">
                                                            <option value="">Select</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2017">2017</option>
                                                            <option value="2018">2018</option>
                                                            <option value="2019">2019</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Filing Method :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select class="form-control fsc-input regular" id="filing_method" name="filing_method">
                                                            <option value="">Select</option>
                                                            <option value="Online"> Online</option>
                                                            <option value="Paperfile">Paperfile</option>

                                                        </select>

                                                        <select class="form-control fsc-input amendment" id="filing_method" name="filing_method2" style="display:none;">
                                                            <option value="">Select</option>
                                                            <option value="Paperfile">Paperfile</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Date :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <!--<input type="text" class="form-control" id="datepicker" name="filing_date" value="{{$common->filing_date}}"/>-->
                                                        <input type="text" class="form-control" id="datepicker" name="filing_date" value=""/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right">Software use of File :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select class="form-control fsc-input" id="filing_software" name="filing_software">
                                                            <option value="">Select</option>
                                                            <option value="Drake Software">Drake Software</option>
                                                            <option value="ATX">ATX</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">State :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control" id="filing_state" name="filing_state" value="{{$common->formation_register_entity}}" readonly/>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Date of Filing :</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <!--<input type="text" class="form-control" id="datepicker" name="filing_date" value="{{$common->filing_date}}"/>-->
                                                        <input type="text" class="form-control" id="datepicker2" name="date_of_filing" value=""/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="card-footer" STYLE="margin-left:10px;">
                                            <div class="col-md-1 col-md-offset-2" style="padding-left:0px; margin-left: 17.3%;">
                                                <input class="btn_new_save" type="submit" value="Create">
                                            </div>
                                            <div class="col-md-1" style="padding-left:0px;">
                                                <a class="btn_new_cancel" href="{{url('fscemployee/workrecord')}}">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                @endif
            </div>
            <div class="clear"></div>
        </div>
    </div>
    </div>
    <div class="clear"></div>
    </div>
    </div>



    <!-- Modal Start Income Tax -->
    <div id="myModalAddrecord" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="pull-left">@if(!empty($common->filename)) {{$common->filename}} @endif</span>Client - Income Tax Filing - (Form-1040) <span style="float:right;">Add Record</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="{{route('workrecord.storefederal')}}" enctype="multipart/form-data">
                        {{csrf_field()}}

                        @if(!empty($common->id))
                            <input type="hidden" name="client_id" value="<?php echo $common->id;?>">
                        @endif
                        <div class="recordform">

                            <table style="width:100%; max-width:520px; margin:0px auto;" class="taxtable">
                                <tr>
                                    <td>
                                        <label class="control-label labels">Filling Year</label>
                                        @if($Incometaxfederal>0)
                                            <select class="form-control federalyear federalyear3" name="federalsyear" required>
                                                @else
                                                    <select class="form-control federalyear" name="federalsyear" required>
                                                        @endif
                                                        <option value="">Select</option>
                                                        <?php

                                                        $now = date('M-d-Y');
                                                        if('Jul-25-2021' == $now)
                                                        {
                                                        $aa = date('Y') - 2;
                                                        $bb = date('Y') - 1;
                                                        ?>
                                                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                        <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        $aa = date('Y') - 1;
                                                        ?>
                                                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                        <?php
                                                        }

                                                        ?>


                                                    </select>

                                    </td>
                                    <td>
                                        <label class="control-label labels">Tax Return</label>

                                        <select class="form-control taxation" name="federalstax" required>
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </td>
                                    <td>
                                        <label class="control-label labels">Due Date</label>
                                        <input type="text" class="form-control duedate" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                                    </td>
                                </tr>
                            </table>

                            <table class="taxtable table table-bordered" style="margin-top:20px;">
                                <tr>
                                    <td>
                                        <label class="form-label">Federal</label>

                                    </td>
                                    <td>
                                        <label class="form-label">Form No.</label>
                                        <input type="text" readonly class="form-control formno" name="federalsform" style="width:100px"/ required>

                                    </td>
                                    <td>
                                        <label class="form-label">Filling Method</label>
                                        <select class="form-control filingmethod" name="federalsmethod" required>
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>

                                    </td>
                                    <td>
                                        <label class="form-label">Filling Date</label>
                                        <input type="text" class="form-control fdate" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required readonly/>

                                    </td>
                                    <td>
                                        <label class="form-label">Status of Filling</label>
                                        <select class="form-control" name="federalsstatus" required>
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="EF Processing">EF Processing</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Rejected">Rejected</option>
                                        </select>


                                    </td>
                                    <td style="width:100px">
                                        <label class="form-label">File</label>
                                        <input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>
                                    </td>
                                    <td>
                                        <label class="form-label">Note</label>
                                        <textarea class="form-control" name="federalsnote"></textarea>

                                    </td>
                                </tr>
                            </table>

                            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                                <tr>
                                    <th><label class="form-label">State</label></th>
                                    <th><label class="form-label">Form No.</label></th>
                                    <th><label class="form-label">Filling Method</label></th>
                                    <th><label class="form-label">Filling Date</label></th>
                                    <th><label class="form-label">Status of Filling</label></th>
                                    <th><label class="form-label">File</label></th>
                                    <th><label class="form-label">Note</label></th>
                                    <th></th>
                                </tr>


                                <tr>
                                    <td>
                                        @if(isset($_REQUEST['id'])&& $_REQUEST['id']!='')
                                            <select class="form-control" name="statetax[]" id="statetax">

                                                <option>Select</option>
                                                @foreach($datastate as $datastate)
                                                    <option value="{{$datastate->state}}">{{$datastate->state}}</option>
                                                @endforeach
                                            </select>


                                        @endif

                                    </td>
                                    <td>

                                        <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno" style="width:100px"/>

                                    </td>
                                    <td>

                                        <select class="form-control filingmethod" name="statemethod[]">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>

                                    </td>
                                    <td>

                                        <input type="text" class="form-control fdate" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY" required readonly/>

                                    </td>
                                    <td>

                                        <select class="form-control" name="statestatus[]">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="EF Processing">EF Processing</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Rejected">Rejected</option>
                                        </select>


                                    </td>
                                    <td style="width:100px">

                                        <input type="file" class="form-control" name="statefile[]" style="display:block; margin-top:0px;"/>
                                    </td>
                                    <td>

                                        <textarea class="form-control" name="statenote[]"></textarea>

                                    </td>
                                    <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
                                </tr>
                            </table>

                            <div class="row form-group saves">
                                <label class="col-md-4 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary savebutton" value="Save">
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>

                            </div>


                            <input type="hidden" name="client_taxation_id" value="<?php if (isset($common->id) != '') {
                                echo $common->id;
                            }?>">
                            <div class="row form-group">

                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control statesyear" name="statesyear">
                                        <option value="">Select</option>
                                        <?php
                                        $aa = date('Y') - 1;

                                        ?>
                                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>
                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control taxation2" name="statestax">
                                        <option value="">Select</option>
                                        <option value="Extension">Extension</option>
                                        <option value="Original">Original</option>
                                        <option value="Amendment">Amendment</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Form No.</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control filingmethod2" name="statesmethod">
                                        <option value="">Select</option>
                                        <option value="E-File">E-File</option>
                                        <option value="Paperfile">Paperfile</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                                </div>
                            </div>
                            <div class="row form-group statusof">
                                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control" name="statesstatus">
                                        <option value="">Select</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Pending">Pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="file" class="form-control" name="statesfile"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <textarea class="form-control" name="statesnote"></textarea>
                                </div>
                            </div>
                            <div class="row form-group saves" style="display:none;">
                                <label class="col-md-5 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->

    <!-- The Modal -->
    <div class="modal" id="myModalbusinesspopup">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="{{route('workrecord.store')}}" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <?php
                        if(isset($common->id) != '')
                        {
                        ?>
                        <input type="hidden" name="client_id" value="{{$common->id}}">
                        <?php
                        }
                        ?>


                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Year</label>
                            <div class="col-md-6">
                                <select class="form-control" name="license_year" required>
                                    <option disabled>Select</option>
                                    <option>2020</option>
                                    <option>2021</option>
                                    <option>2022</option>
                                    <option>2023</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Gross Revenue</label>
                            <div class="col-md-6"><input type="text" class="form-control" name="license_gross" id="license_gross" required/></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">License Fee </label>
                            <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="license_fee" id="license_fee" required/></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">License Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="license_status" required>
                                    <option>Active</option>
                                    <option>Inactive</option>
                                    <option>Hold</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">License Copy</label>
                            <!--<div class="col-md-6">-->
                            <!--    <input type="file" name="license_copy" required/>-->
                            <!--</div>-->

                            <div class="col-md-8">
                                <label class="file-upload btn btn-primary">
                                    <input type="file" class="form-control" type="file" name="license_copy" required/>
                                    Browse for file ... </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Certificate No</label>
                            <div class="col-md-6"><input type="text" class="form-control" name="license_no" required/></div>
                        </div>

                        <!--    <div class="form-group row">-->
                        <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
                        <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
                        <!--</div>-->

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Note</label>
                            <div class="col-md-6"><input type="text" class="form-control" name="license_note" required/></div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">License Issue Date </label>
                            <div class="col-md-6"><input type="date" class="form-control" name="license_renew_date" required/></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right"></label>
                            <div class="col-md-3">
                                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                            </div>
                            <div class="col-md-3">
                                <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee">Cancel</a>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


    <div class="modal" id="myModalbankingpopup">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Record</h4>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right padleftzero">Bank Name</label>
                            <div class="col-md-3">
                                <!--<input type="text" class="form-control" name="license_year" required/>-->
                                <select class="form-control" name="license_year" required>
                                    <option disabled>Select</option>
                                    <option>SBI</option>
                                    <option>BOB</option>
                                    <option>BOI</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right">Last Four Digit A/C #</label>
                            <div class="col-md-3">
                                <select class="form-control" name="license_year" required>
                                    <option disabled>Select</option>
                                    <option>0022</option>
                                    <option>0033</option>
                                    <option>0044</option>
                                </select>
                            </div>
                            <label class="col-md-3 control-label text-right">Bank Nick name</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right">St. Start Date: </label>
                            <div class="col-md-3"><input type="text" class="form-control txtinput_1 opendate" name="startdate"/></div>
                            <label class="col-md-3 control-label text-right">St. Beginning Balance: </label>
                            <div class="col-md-3"><input type="text" class="form-control txtinput_1 closedate" name="startdate"/></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right">St. End Date:</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control"/>
                            </div>
                            <label class="col-md-3 control-label text-right">St. Ending Balance:</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right">Note:</label>

                            <div class="col-md-9">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label text-right">Account Done By:</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control"/>
                            </div>
                            <label class="col-md-3 control-label text-right">Accounts Done Date:</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control opendate"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right"></label>
                            <div class="col-md-3">
                                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                            </div>
                            <div class="col-md-3">
                                <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee">Cancel</a>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


    </div>
    </div>


    <div id="myModalviewannual" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">View Annual Receipt</h4>
                </div>
                <div class="modal-body">
                    <div id="viewmodelviewannual"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="myModalviewofficer" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">View Officer</h4>
                </div>
                <div class="modal-body">
                    <div id="viewmodelviewofficer"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('.fdate').datepicker({
                autoclose: true
            });
        });

        $(document).ready(function () {

            $('.openBtnannual').on('click', function () {

                var num = $('#clientsid').val();
                $.get('{!!URL::to('getWorkannual')!!}?id=' + num, function (data) {
                    if (data == "") {
                        $('#myModal2').modal({show: true});
                    } else {
                        $.each(data, function (index, subcatobj) {
                            // alert(subcatobj.upload_name);

                            $('#myModalviewannual').modal({show: true});
                            $('#viewmodelviewannual').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.annualreceipt + '" width="100%" height="300px">');
                        });
                    }
                });
            });

            $('.openBtnofficer').on('click', function () {
                var num = $('#clientsid').val();
                $.get('{!!URL::to('getWorkofficer')!!}?id=' + num, function (data) {
                    if (data == "") {
                        $('#myModal3').modal({show: true});
                    } else {
                        $.each(data, function (index, subcatobj) {
                            // alert(subcatobj.upload_name);

                            $('#myModalviewofficer').modal({show: true});
                            $('#viewmodelviewofficer').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.formation_work_officer + '" width="100%" height="300px">');
                        });
                    }
                });
            });

            $('#statetax').on('change', function () {
                var statetaxval = $("#statetax").val();
                $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval=' + statetaxval, function (data) {
                    //console.log(data);exit;
                    if (data == '') {
                        $("#statefederalformno").val();
                    } else {
                        $("#statefederalformno").val(data.taxform);
                    }

                });

            });

            $('.federalyear3').change(function () {
                var this1 = $(this).val();
                var aa = '<?php echo $aa = date('Y') - 1;?>';
                if (this1 == aa) {
                    $(".savebutton").hide();
                    alert('This year already exists!');
                } else {
                    $(".savebutton").show();
                }
            });

            $('.federalyear').change(function () {

                var this1 = $(this).val();
                var aa = '<?php echo $aa = date('Y') - 1;?>';

                if (this1 == aa) {
                    $(".duedate").val('Jul-15-<?php echo $aa + 1;?>');

                }


                //  if(this1 == 'Extension')
                //  {
                //      $("select option[value='Extension']").hide();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');

                //  }
                // else if(this1 == bb)
                //  {
                //      $("select option[value='Extension']").hide();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');
                //  }
                //  else if(this1 == cc)
                //  {
                //      $("select option[value='Extension']").show();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');
                //  }

            });


            $('.statesyear').change(function () {

                var this1 = $(this).val();
                //alert(this1);
                var aa = '<?php echo $aa = date('Y');?>';
                var bb = '<?php echo $bb = date('Y') + 1;?>';
                var cc = '<?php echo $cc = date('Y') + 2;?>';
                if (this1 == aa) {
                    $("select option[value='Extension']").hide();
                    $(".duedate2").val('Jul-15-<?php echo $aa + 1;?>');

                } else if (this1 == bb) {
                    $("select option[value='Extension']").hide();
                    $(".duedate2").val('Jul-15-<?php echo $bb + 1;?>');
                } else if (this1 == cc) {
                    $("select option[value='Extension']").show();
                    $(".duedate2").val('Jul-15-<?php echo $cc + 1;?>');
                }

            });


            $('.taxation').change(function () {
                var businessid = '<?php if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                    echo $common->business_id;
                }?>';

                var thiss = $(this).val();
                if (thiss == 'Extension') {
                    if (businessid == '6') {
                        $('.formno').val('Form-4868');
                    } else {
                        $('.formno').val('Form-7004');
                    }

                    $("select option[value='E-File']").show();
                } else if (thiss == 'Original') {
                    $('.formno').val('Form-1040');
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Amendment') {
                    $('.formno').val('Form-1040X');
                    $("select option[value='E-File']").hide();

                }
            });

            $('.taxation2').change(function () {
                var businessid = '<?php if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                    echo $common->business_id;
                }?>';
                var thiss = $(this).val();
                if (thiss == 'Extension') {
                    if (businessid == '6') {
                        $('.formno').val('Form-4868');
                    } else {
                        $('.formno').val('Form-7004');
                    }
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Original') {
                    $('.formno2').val('Form-1040');
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Amendment') {
                    $('.formno2').val('Form-1040X');
                    $("select option[value='E-File']").hide();

                }
            });


            $('.filingmethod').change(function () {
                var thiss = $(this).val();
                if (thiss == 'Paperfile') {
                    $('.statusof').hide();
                } else {
                    $('.statusof').show();
                }
            });

            $('.filingmethod2').change(function () {
                var thiss = $(this).val();
                if (thiss == 'Paperfile') {
                    $('.statusof2').hide();
                } else {
                    $('.statusof2').show();
                }
            });

        });

        $(document).ready(function () {

            var cnt = 1;

            $(".add-rowform").click(function () {
                cnt++;
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax' + cnt + '" name="statetax[]" id="statetax' + cnt + '"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->state}}">{{$datastate3->state}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno' + cnt + '" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdate' + cnt + '" id="fillingdate" name="statedate[]" id="federaldate' + cnt + '" placeholder="MM/DD/YYYY" required readonly/></td><td><select class="form-control" name="statestatus[]" id="federalstatus' + cnt + '"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td style="width:100px"><input type="file" class="form-control" name="statefile[]" id="federalfile' + cnt + '" style="display:block; margin-top:0px;"/></td> <td><textarea class="form-control" name="statenote[]" id="federalnote' + cnt + '"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);

                $(function () {
                    $('.fdate' + cnt).datepicker({
                        autoclose: true
                    });
                });

                $('.statetax' + cnt).on('change', function () {
                    var statetaxval = $('.statetax' + cnt).val();
                    $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval=' + statetaxval, function (data) {
                        if (data == '') {
                            $('#statefederalformno' + cnt).val();
                        } else {
                            $('#statefederalformno' + cnt).val(data.taxform);
                        }

                    });
                });

            });
            $("body").on("click", ".removetrrow", function () {
                $(this).parent().parent().remove();
            });

        });
    </script>

    <script>
        $(document).ready(function () {

            $('#formation_yearbox').on('change', function () {
                var ids = $(this).val();
                if (ids == '1') {
                    $('#formation_yearvalue').val('2019');
                } else if (ids == '2') {
                    $('#formation_yearvalue').val('2020');
                } else if (ids == '3') {
                    $('#formation_yearvalue').val('2021');
                } else if (ids == '4') {
                    $('#formation_yearvalue').val('2022');
                } else if (ids == '') {
                    $('#formation_yearvalue').val('');
                }
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                    //check if the option value is already in the select box
                    $('#vendor_product option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
                        type: "post",
                        url: "{!!route('workrecord.documents')!!}",
                        dataType: "json",
                        data: $('#ajax').serialize(),
                        success: function (data) {
                            alert('Successfully Added');
                            $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                            $("#div").load(" #div > *");
                            $("#newopt").val('');
                        },
                        error: function (data) {
                            alert("Error")
                        }
                    });

                    $('#basicExampleModal').modal('hide');
                });
            });


        });


    </script>


    <script type="text/javascript">
        $(document).ready(function () {


            $("#license_gross").on("input", function (evt) {
                var self = $(this);
                self.val(self.val().replace(/[^\d].+/, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });

            $("#license_fee").on("input", function (evt) {
                var self = $(this);
                self.val(self.val().replace(/[^\d].+/, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });


            (function ($) {
                var minNumber = -100;
                var maxNumber = 100;
                $('.spinner .btn:first-of-type').on('click', function () {
                    if ($('.spinner input').val() == maxNumber) {
                        return false;
                    } else {
                        $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
                    }
                });

                $('.txtinput_1').on("blur", function () {
                    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                    if (minNumber > inputVal) {
                        //inputVal = -100;
                    } else if (maxNumber < inputVal) {
                        //inputVal = 100;
                    }
                    $(this).val(inputVal + '.00');
                });

                $('.spinner .btn:last-of-type').on('click', function () {
                    if ($('.spinner input').val() == minNumber) {
                        return false;
                    } else {
                        $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
                    }
                });
            })(jQuery);

        });

    </script>


    <script>

        function myFunction() {

            var ftype = document.getElementById('filing_type').value;
            //alert(ftype);
            if (ftype == 'Regular Filing') {
                $('#filing_year').val('2019');
                $('.regular').show();
                $('.regular2').show();
                $('.amendment2').hide();
                $('.amendment').hide();
            } else if (ftype == 'Extension Filing') {
                $('#filing_year').val('2019');
                $('.regular').show();
                $('.regular2').show();
                $('.amendment2').hide();
                $('.amendment').hide();
            } else if (ftype == 'Amendment Filing') {
                $('#filing_year').val('2016');
                $('.regular').hide();
                $('.amendment').show();
                $('.regular2').hide();
                $('.amendment2').show();
            }


        }

        $(document).ready(function () {

            $('#formation_yearbox22').on('change', function () {
                var ids = document.getElementById("formation_yearbox22").value;
                <?php
                if(isset($common->id) != '')
                {
                ?>
                var clientid = <?php echo $common->id;?>
                <?php
                }
                ?>

                //alert(ids);
                var yearvaluesub1 = '<?php $dd = date('Y'); echo $dd - 1;?>';
                //var yearvaluesub1 = '2018';

                if (ids != '') {
                    for (var n = 1; n <= ids; ++n) {
                        var total = parseFloat(yearvaluesub1) + parseFloat(n);
                        $("#formation_yearvalue22").val(total);
                        $.get('{!!URL::to('getFormationexistdata')!!}?totalid=' + total, 'clientid=' + clientid, function (data) {
                            //console.log(data);exit;
                            if (data > 0) {
                                $("#yeardalreadyExist").show();
                                $(".yeardalreadyExist1").show();
                                $(".yeardalreadyExist2").hide();
                            } else {
                                //console.log(data.id);exit;
                                $("#yeardalreadyExist").hide();
                                $(".yeardalreadyExist1").hide();
                                $(".yeardalreadyExist2").show();
                            }

                        });


                    }


                    if (ids == '') {

                        $("#formation_amount").val('');
                    } else if (ids == 1) {
                        // alert(ids);
                        var todaydate = $('.todaydates').val();
                        var strdate = $('.strdates').val();

                        yearss1 = '<?php $dd = date('Y'); echo $dd;?>';
                        $("#formation_yearvalue").val(yearss1);
                        //alert(yearss1);
                        var aa = $(".annualfees").val('$50.00');

                        var bb = $('.processingfees').val('$10.00');

                        var aa1 = '50';
                        var bb1 = '10';
                        var pc1 = '10';

                        if (todaydate > strdate) {
                            var penalty = $('.penaltycharges').val('$10.00');
                            var totalamts = parseFloat(aa1) + parseFloat(pc1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        } else {
                            var penalty = $('.penaltycharges').val('$0.00');
                            var totalamts = parseFloat(aa1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        }
                        $('.totalamt').val(finalamts);


                    } else if (ids == 2) {
                        var todaydate = $('.todaydates').val();
                        var strdate = $('.strdates').val();

                        yearss1 = '<?php $dd = date('Y'); echo $dd;?>';
                        yearss2 = '<?php $dd = date('Y'); echo $dd + 1;?>';
                        yearss4 = ',';
                        var finalyyear2 = yearss1.concat(yearss4).concat(yearss2);
                        $("#formation_yearvalue").val(finalyyear2);
                        //  $("#formation_amount").val('$100.00');

                        var aa = $(".annualfees").val('$100.00');

                        var bb = $('.processingfees').val('$10.00');

                        var aa1 = '100';
                        var bb1 = '10';
                        var pc1 = '10';

                        if (todaydate > strdate) {
                            var penalty = $('.penaltycharges').val('$10.00');
                            var totalamts = parseFloat(aa1) + parseFloat(pc1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        } else {
                            var penalty = $('.penaltycharges').val('$0.00');
                            var totalamts = parseFloat(aa1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        }
                        $('.totalamt').val(finalamts);


                    } else if (ids == 3) {
                        var todaydate = $('.todaydates').val();
                        var strdate = $('.strdates').val();

                        yearss1 = '<?php $dd = date('Y'); echo $dd;?>';
                        yearss2 = '<?php $dd = date('Y'); echo $dd + 1;?>';
                        yearss3 = '<?php $dd = date('Y'); echo $dd + 2;?>';
                        yearss4 = ',';
                        var finalyyear3 = yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
                        $("#formation_yearvalue").val(finalyyear3);
                        // $("#formation_amount").val('$150.00');

                        var aa = $(".annualfees").val('$150.00');

                        var bb = $('.processingfees').val('$10.00');

                        var aa1 = '150';
                        var bb1 = '10';
                        var pc1 = '10';

                        if (todaydate > strdate) {
                            var penalty = $('.penaltycharges').val('$10.00');
                            var totalamts = parseFloat(aa1) + parseFloat(pc1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        } else {
                            var penalty = $('.penaltycharges').val('$0.00');
                            var totalamts = parseFloat(aa1) + parseFloat(bb1);
                            var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                            var dollar = '$';
                            var finalamts = dollar.concat(sign53);

                        }
                        $('.totalamt').val(finalamts);

                    }

                }


            });

            var ftype = document.getElementById('filing_type').value;
            //alert(ftype);
            if (ftype == 'Regular Filing') {
                $('#filing_year').val('2019');
                $('.regular').show();
                $('.regular2').show();
                $('.amendment2').hide();
                $('.amendment').hide();
            } else if (ftype == 'Extension Filing') {
                $('#filing_year').val('2019');
                $('.regular').show();
                $('.regular2').show();
                $('.amendment2').hide();
                $('.amendment').hide();

            } else if (ftype == 'Amendment Filing') {
                $('#filing_year').val('2016');
                $('.regular').hide();
                $('.amendment').show();
                $('.regular2').hide();
                $('.amendment2').show();
            }

        });


        function ShowHideDiv(address) {
            var dvPassport = document.getElementById("addresschangeto");
            addresschangeto.style.display = address.checked ? "block" : "none";
        }

        function DivShowHideDiv(officerchange) {
            var dvPassport = document.getElementById("OfficerChange");
            OfficerChange.style.display = officerchange.checked ? "block" : "none";
        }

        $(document).ready(function () {
            var paidby = $('.paidby').val();
            if (paidby == 'Client') {
                $('.hidepaid').hide();

            } else if (paidby == 'FSC') {
                $('.hidepaid').show();
            } else {
                $('.hidepaid').hide();
            }
            $('.paidby').on('change', function () {
                var thiss = $(this).val();
                // alert(thiss);
                if (thiss == 'Client') {
                    $('.hidepaid').hide();
                } else if (thiss == 'FSC') {
                    $('.hidepaid').show();
                }
            });
            //$('.hidepaid').hide();
            $('.annualfees').on('blur', function () {
                var annualfees = $('.annualfees').val();
                var sign53 = parseFloat(Math.round(annualfees * 100) / 100).toFixed(2);
                var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $(".annualfees").val(annual);
            });


            $('.processingfees').on('blur', function () {
                var processingfees = $('.processingfees').val();
                var sign3 = parseFloat(Math.round(processingfees * 100) / 100).toFixed(2);
                $(".processingfees").val(sign3);
                var annualfees = $('.annualfees').val();
                var totalamts = parseFloat(annualfees) + parseFloat(processingfees);
                // alert(totalamts);
                var sign53 = parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
                // var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $(".totalamt").val(sign53);
            });


            $('.checkradio').click(function () {

                var inputValue = $(this).attr("value");
//alert(inputValue);
                if (inputValue == 'Yes') {
                    $(".hideradio").show();
                } else {
                    $(".hideradio").hide();
                }
                //   var targetBox = $("." + inputValue);

                // $(".box").not(targetBox).hide();

                //$(targetBox).show();

            });

            $('#country_name').keyup(function () {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('workrecord.fetchrec') }}",
                        method: "POST",
                        data: {query: query, _token: _token},
                        success: function (data) {
                            $('#countryList').fadeIn();
                            $('#countryList').html(data);
                        }
                    });
                }
            });


            $('#frms').submit(function () {
                var data = $("#frms").serialize();
                $.ajax({
                    url: "https://financialservicecenter.net/fac-Bhavesh-0554/workrecord/fetchupdate",
                    method: "POST",
                    data: data,
                    success: function (data) {
                        // $('#countryList').fadeIn();
                        //        $('#countryList').html(data);
                    }
                });

            });

            $(document).on('click', 'tr', function () {
                $('#country_name').val($(this).text());
                $('#countryList').fadeOut();

                $('#file-upload').change(function () {
                    var i = $(this).prev('label').clone();
                    var file = $('#file-upload')[0].files[0].name;
                    $(this).prev('label').text(file);
                });
            });

        });
    </script>

    <script>
        $(function () {
            $("#datepicker").datepicker();
            $("#datepicker2").datepicker();

        });


        $(document).ready(function () {
            $(".add-row").click(function () {
                var markup = "<tr><td><input type='text' name='first_name[]' placeholder='First Name' class='form-control'/></td><td><input type='text' placeholder='Last Name' name='last_name[]' class='form-control'/></td><td><select class='form-control' name='position[]' id='agent_position11'><option>--Select Position--</option><option value='Agent'>Agent</option><option  value='CEO'>CEO</option><option value='Sec'>CEO / CFO / Sec.</option><option value='CFO'>CFO</option><option  value='Secretary'>Secretary</option></select></td><td style='vertical-align:middle;'><a class='delete-row removetr'><i class='fa fa-minus'></i></a></td></tr>";
                $("#officerchange tbody").append(markup);
            });

            $("body").on("click", ".removetr", function () {
                $(this).parent().parent().remove();
            });

        });


        $('#filing_date').datepicker({
            format: 'M-d-Y',
            ignoreReadonly: true,
        });

        $(document).ready(function () {
            var dateInput = $('input[name="paiddate"]');
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
                format: 'M-d-Y',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: false,
            });
            $('#paiddate').datepicker();

            $(".opendate").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
            });


            $(".closedate").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
            });

        });


        function truncateDate(date) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }


    </script>
@endsection