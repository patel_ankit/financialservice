@extends('front-section.app')
@section('main-content')
    <style>
        .star-required1 {
            color: #e9f7ff;
        }
    </style>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
                <h4>Inquiry / Application Form
                    <span class="clearfix"></span>
                    <span style="line-height: 33px;">{{Request::segment(4)}} </span>
                </h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
                <div class="alert alert-danger" style="margin-top:10px;display:none" id="dangerError">ad</div>
                <div class="alert alert-success" style="margin-top:10px;display:none" id="successError">ad</div>
                <form id="applyService" name="applyService" method="post" novalidate="novalidate" action="{{route('apply-service.store',Request::segment(3))}}">
                    {{csrf_field()}}

                    <input type="hidden" name="serviceid" id="serviceid" value="{{Request::segment(3)}}">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 4%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Name :
                                <span class="star-required">*</span>
                            </label>
                        </div>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="width: 105px;">
                                    <select class="form-control fsc-input" id="nametype" name="nametype">
                                        <option value="mr">Mr.</option>
                                        <option value="mrs">Mrs.</option>
                                        <option value="miss">Miss.</option>
                                    </select>
                                </div>

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" name="firstName" class="form-control textonly fsc-input" id="firstName" placeholder="First">
                                </div>
                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <div class="row">
                                        <input type="text" class="form-control fsc-input textonly" name="middleName" id="middleName" maxlength="1" placeholder="M">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" name="lastName" class="form-control fsc-input textonly" id="lastName" placeholder="Last">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Address1 :
                                <span class="star-required">*</span>
                            </label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="address" name="address" placeholder="Address">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Address2 : <span class="star-required1">*</span>
                            </label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="address2" name="address2" placeholder="Address">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="dropdown">
                                <select name="countryId" id="countries_states1" class="form-control bfh-countries fsc-input" data-country="USA">
                                    <option value=''>---Select---</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">City / State / Zip :
                                <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City">
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown" style="margin-top: 1%;">
                                <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1">
                                    <option value="">State</option>
                                </select>
                                <select class="form-control bfh-timezones" style="display:none" name="timezone" data-country="countries_states1"></select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input zip" id="zip" name="zip" placeholder="Zip">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Telephone 1 :
                                <span class="star-required">*</span>
                            </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder=" (999) 999-9999" id="telephoneNo1" name="telephoneNo1">
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                    <option value="">Type</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Home">Home</option>
                                    <option value="Work">Work</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input zip" id="ext1" name="ext1" readonly placeholder="Ext.">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Telephone 2 : <span class="star-required1">*</span> </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="  (999) 999-9999" id="telephoneNo2" name="telephoneNo2">
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                    <option value="">Type</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Home">Home</option>
                                    <option value="Work">Work</option>
                                    <option value="Work">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input zip" id="ext2" name="ext2" readonly placeholder="Ext.">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Fax :
                                <span class="star-required1">*</span>
                            </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="tel" name="fax" id="fax" class="form-control fsc-input  bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder=" (999) 999-9999">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email :
                                <span class="star-required">*</span>
                            </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="email" class="form-control fsc-input" id="email" name="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Inquiry For :
                                <span class="star-required">*</span>
                            </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="dropdown">
                                <select class="form-control fsc-input" id="serviceTypeId" name="serviceTypeId">
                                    <option value="">Please select Inquiry For</option>
                                    @foreach($service as $ser)
                                        <option value="{{$ser->servicename}}">{{$ser->servicename}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Note : <span class="star-required1">*</span> </label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <textarea class="form-control fsc-input" rows="3" id="note" name="note" placeholder="Note"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                        <center>
                            <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="float:none">SUBMIT</button>
                            &nbsp&nbsp<a href="http://financialservicecenter.net/service" class="btn btn-primary btn-lg fsc-form-submit" style="float:none">Cancel</a>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {
            $('#applyService').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                fields: {
                    firstName: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            notEmpty: {
                                message: 'Please Enter Your First Name'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The First Name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },

                    lastName: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Last Name'
                            },

                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The Last name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Email Address'
                            },
                            emailAddress: {
                                message: 'Please Enter Your Valid Email Address'
                            },

                            remote: {
                                url: "{{ URL::to('/emp_id1') }}",
                                data: function (validator) {
                                    return {
                                        email: validator.getFieldElements('email').val()
                                    }
                                },
                                message: 'This Email Id ALready exit.'
                            }

                        }
                    },
                    address: {
                        validators: {
                            stringLength: {
                                min: 8,
                                message: 'Please Enter Your 8 Charactor'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Address'
                            }
                        }
                    },
                    /*address2: {
                        validators: {
                             stringLength: {
                                min: 8,
                                message: 'Please Enter Your 8 Charactor'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Address'
                            }
                        }
                    },*/
                    city: {
                        validators: {
                            stringLength: {
                                min: 2,

                            },
                            notEmpty: {
                                message: 'Please Enter Your City'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The City can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    telephoneNo1Type: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your Telephone Type'
                            }
                        }
                    },
                    /*telephoneNo2Type: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your  Telephone Type'
                            }
                        }
                    },*/

                    stateId: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your State'
                            }
                        }
                    },
                    serviceTypeId: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your Service Type'
                            }
                        }
                    },
                    countryId: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your Country'
                            }
                        }
                    },
                    zip: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Zip Code'
                            }
                        }
                    },
                    telephoneNo1: {
                        validators: {

                            notEmpty: {
                                message: 'Please Enter Your Telephone Number'
                            }, mobile_no: {
                                country: 'USA',
                                message: 'Please supply a vaild Telephone number with area code'
                            }
                        }
                    },


                    /*telephoneNo2: {
                        validators: {
                            stringLength: {
                                min: 15,
                                message:'Please enter at least 10 characters and no more than 10'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Telephone Number'
                            }, mobile_no: {
                                country: 'USA',
                                message: 'Please supply a vaild telephone number with area code'
                            }
                        }
                    },
        fax: {
                        validators: {
                            stringLength: {
                                min: 15,
                                message:'Please enter at least 10 characters and no more than 10'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Fax Number'
                            }, mobile_no: {
                                country: 'USA',
                                message: 'Please supply a vaild Fax number with area code'
                            }
                        }
                    },		*/


                }
            })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#applyService').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();
                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        // console.log(result);
                    }, 'json');
                });
        });


    </script>
    <script>
        var dat1 = $('#ext1').val();
        $('#telephoneNo1Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>
    <script>
        var dat1 = $('#ext2').val();
        $('#telephoneNo2Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })
    </script>
    <!--<script>
      $(document).ready(function(){
      /***phone number format***/
      $(".phone").keypress(function (e) {
       if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
       }
       var curchr = this.value.length;
       var curval = $(this).val();
       if (curchr == 3 && curval.indexOf("(") <= -1) {
         $(this).val("(" + curval + ")" + " ");
       } else if (curchr == 4 && curval.indexOf("(") > -1) {
         $(this).val(curval + ")-");
       } else if (curchr == 5 && curval.indexOf(")") > -1) {
         $(this).val(curval + "-");
       } else if (curchr == 9) {
         $(this).val(curval + "-");
         $(this).attr('maxlength', '14');
       }
      });
      });
   </script>-->
@endsection()