<div class="proposal" style="width: 850px;padding: 10px;margin: auto;border: 1px solid #000000;">
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px" style="margin:10px auto;display:block;" class="img-responsive">
    <p style="font-size: 16px;font-weight: 600;margin-top: 10px;text-align:center;">
        <span>4550 Jimmy Carter Blvd.</span>
        <span style="padding: 0px 4px;"> | </span> Norcroos
        <span style="padding: 0px 4px;"> | </span> GA
        <span style="padding: 0px 4px;"> | </span> FAX : +1 (770) 414-5351
    </p>
    <hr style="border-top: 1px solid #8e8e8e;margin-bottom:10px;">
    <h3 class="text-center" style="margin-bottom:25px;text-transform: uppercase;text-align:center;font-size: 24px;font-weight: 100;">Agreement For</h3>
    <p>
        I / we the undersigned assign our Accounting and Tax matters to FINANCIAL SERVICE CENTER for a term of one ( 1 ) year from the date of this
        agreement. In return for the above consideration, I / we shall receive the following services :
    </p>
    <h4>BOOKKEEPING</h4>
    <ol style="margin: 0px 0px 5px 0px;">
        <li></li>
    </ol>
    <h4>FEES ARE AS FOLLOWS:</h4>
    <table style="width:90%;margin:auto;">
        <tr>
            <td width="200px">Installation Fee</td>
            <td>..................................................................................................................</td>
            <td style="text-align:right;width:100px;">$ 0.00</td>
        </tr>
        <tr>
            <td width="200px">Balance Due</td>
            <td>..................................................................................................................</td>
            <td style="text-align:right;width:100px;">$ 0.00</td>
        </tr>
    </table>
    <p style="width:90%;margin:15px auto;">Any incident occurring prior to the date of this agreement will not covered by our service</p>

    <p>This agreement between the subscriber and FINANCIAL SERVICE CENTER shall be automatically renewed for an additional year from date of agreement unless written notice of cancellation is given at least 30 days before the expiration of the year's service. Business will be reviewed in order to establish any necessary adjustment in fee.</p>

    <p>Client is responsible for submitting complete and accurate information to FINANCIAL CERVICE CENTER.</p>

    <div style="width:50%;float:left;">
        <h4>FINANCIAL SERVICE CENTER</h4>
        <p>Sign by :</p>
        <p>_____________________</p>
    </div>
    <div style="width:50%;float:left;">
        <p>Address : </p>
        <p>Name : </p>
        <p>03/25/2021 </p>
    </div>
    <div style="clear: both;"></div>
</div>