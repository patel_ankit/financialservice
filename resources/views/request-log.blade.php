@extends('front-section.app')
@section('main-content')

    <style>
        table tr th {
            font-size: 16px;
        }

        table tr td {
            font-size: 16px;
        }

        p {
            font-size: 16px;
        }

        span {
            font-size: 16px;
        }

        label.file-upload {
            position: relative;
            overflow: hidden;
            float: left;
        }

        input[type="file"] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
            font-size: 16px;
        }

        .table-title {
            margin-bottom: 15px;
            float: right;
        }

        .table-title a {
            display: inline-block;
            float: right;
            font-size: 14px;
            color: #FFF;
            background: #2196f3;
            padding: 4px 15px 6px;
            border: 1px solid #333;
        }
    </style>

    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-1 col-sm-1 col-xs-1"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 fsc-content-box">
                    <form class="form-horizontal" method="POST" id="client-login" action="https://financialservicecenter.net/login">
                        <div id="smartwizard register_container">
                            <input type="hidden" name="_token" value="7rtpYUefqrKz8893EzTiHpZV2IdxfzVCpsc4S30I">
                            <div class="tab-content">
                                <div id="step-1" role="form">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="form-group">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Client ID:<span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="hidden" placeholder="Enter Your Email Id" class="form-control fsc-input" name="type" id="type" value="1">
                                                <input type="hidden" placeholder="Enter Your Email Id" class="form-control fsc-input" name="type1" id="type1" value="0">
                                                <input id="clientid" type="text" placeholder="Client ID:" class="form-control fsc-input" name="clientid" autofocus="">
                                                <span id="err_client"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="form-group">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Client Name: <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input id="" type="text" placeholder="Client Name:" class="form-control fsc-input" name="email" value="" autofocus="">
                                                <span id="err_user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="form-group">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Business Name: <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input id="" type="text" placeholder="Business Name:" class="form-control fsc-input" name="email" value="" autofocus="">
                                                <span id="err_user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="form-group">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Physical Address: <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input id="" type="text" placeholder="Physical Address:" class="form-control fsc-input" name="email" value="" autofocus="">
                                                <span id="err_user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                        <div class="form-group">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">City/State/Zip: <span class="star-required">*</span></label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input id="" type="text" placeholder="City/State/Zip" class="form-control fsc-input" name="email" value="" autofocus="">
                                                <span id="err_user"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">

                                        <div style=" background:#993366; height:30px; width:100%; "></div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="row">

            <div class="col-sm-12">
                <div class="box-tools pull-right">
                    <div class="table-title">
                        <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminupload/create"><i class="fa fa-plus"></i>&nbsp; Add New</a>
                    </div>
                </div>
            </div>


            <button onclick="myCreateFunction()">Create row</button>
            <button onclick="myDeleteFunction()">Delete row</button>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable">
                        <tr>
                            <th>No</th>
                            <th>Vendor Name</th>
                            <th>Invoice #</th>
                            <th>Invoice Dt</th>
                            <th>Amount</th>
                            <th>Amount</th>
                            <th>Amount</th>
                            <th>Shipping Charges</th>
                            <th>Total</th>
                            <th>Upload</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><select name="upload_name" type="text" id="upload_name" class="form-control" value="">
                                    <option value=""> Select</option>
                                    <option value="SOS Certificate"> SOS Certificate</option>
                                </select></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><label class="file-upload btn btn-primary">
                                    <input type="file" name="upload" id="upload" class="form-control">

                                    Upload
                                </label></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


        <div style=" text-align:center; width:100%; margin:50px 0; ">------------------------------------------------------</div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <h2>GEORGIA CIGAR, LITTLE CIGAR, AND LOOSE AND SMOKELESS TOBACCO</h2>
                <h3>EXCISE TAX MONTHLY RETURN</h3>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <p style="width:50%;">Georgia Department of Revenue
                    Alcohol and Tobacco Division
                    P.O. Box 49728
                    Atlanta, GA 30359</p>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <p class="pull-left">Georgia Retailer</p>

                <p style=" width:50%; float:right; padding:10px; border:solid 2px #000; ">Return to be filed
                    on or before the 10th
                    day of the following
                    month for which the
                    report is filed</p>

            </div>

        </div>


        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <td colspan="2">
                                <div class="pull-left">
                                    <p>Legal Business Name</p>
                                    <p>Pramukh Swmi, LLC</p>
                                </div>
                                <h4 class="pull-right">DBA Tobacco Palace</h4>
                            </td>
                            <td colspan="2">
                                <div class="pull-left">
                                    <p>STI#</p>
                                </div>
                                <h4 class="pull-right">20114914861</h4>
                            </td>
                            <td>
                                <p>State License No. <b>0043050</b></p>
                            </td>
                            <td>
                                <p>Jun-14</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="pull-left">
                                    <p>Address</p>
                                    <h4>250</h4>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="pull-left">
                                    <p>(Street)</p>
                                    <h4>Plaza Suite 121</h4>
                                </div>
                                <h4 class="pull-right">Golden Isles</h4>
                            </td>
                            <td>
                                <div class="pull-left">
                                    <p>Zip Code</p>
                                </div>
                                <h4 class="pull-right">31520</h4>
                            </td>
                        </tr>
                        <tr>
                            <th>Line #</th>
                            <th>SUMMARY OF TRANSACTIONS DURING MONTH</th>
                            <th>Loose Tobacco <br/> (Whole Cost Price)</th>
                            <th>Smokeless<br/> (Whole Cost Price)</th>
                            <th>Large Cigars<br/> (Whole Cost Price)</th>
                            <th>Lttle Cigars<br/> (Whole Cost Price)</th>
                        </tr>
                        <tr>
                            <td>1
                            </th>
                            <td>Total Purchases and Tranfers From All Distributors/Importers (Schedule A)</td>
                            <td bgcolor="#ccffff">0.00</td>
                            <td>-</td>
                            <td bgcolor="#ccffff">10,688.01</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>2
                            </th>
                            <td>Georgia Tax Paid Purchases and Tranfers From All Distributors/Importers</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>3
                            </th>
                            <td>Non-Georgia Tax Purchases and Tranfers From All Distributors/Importers</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>4
                            </th>
                            <td>GA. Ending Taxable Purchases (Lines 1-2 = Ending Taxable Sales)</td>
                            <td>0.00</td>
                            <td>-</td>
                            <td>10,688.01</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>5
                            </th>
                            <td>Tax Rate</td>
                            <td>10%</td>
                            <td>10%</td>
                            <td>23%</td>
                            <td>0.0025</td>
                        </tr>

                        <tr>
                            <td>6
                            </th>
                            <td>Gross Tax Due (Line 4 x Tax Rate on Line 5)</td>
                            <td>$0.00</td>
                            <td>-</td>
                            <td>$2,458.24</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>7
                            </th>
                            <td>Previous Overpayment of Tax Paid on Product</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>

                        <tr>
                            <td>8
                            </th>
                            <td>Net Tax Due (Line 6 Less Line 7)</td>
                            <td>$0.00</td>
                            <td>-</td>
                            <td>$2,458.24</td>
                            <td>-</td>
                        </tr>


                        <tr>
                            <td>9
                            </th>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>TOTAL</td>
                            <td>$2,458.24</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="col-sm-12 text-right">
                <h4> $2,458.24</h4>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class=" text-center">AFFIDAVIT</h2>


                <p>I certify, under the penalties for filing false returns, that I have personal knowledge and understanding of statements made in this return and that the figures presented </p>

                <p>herein, including accompanying schedules, are true, correct and complete to the best of my knowledge and belief, and are filed in accordance with the law.</p>

            </div>

        </div>

    </div>




    <script type="text/javascript">
        function displayBox() {
            var HowYouNeed = $("#HowYouNeed").val();
            if (HowYouNeed == 'By Email') {
                $("#byemail").show();
                $("#byfax").hide();
            } else if (HowYouNeed == 'By Fax') {
                $("#byfax").show();
                $("#byemail").hide();
            } else {
                $("#byfax").hide();
                $("#byemail").hide();
            }
        }

        $(document).ready(function () {
            $("#RequestLogForm").validate({
                rules: {
                    clientFileNo: "required",
                    clientId: "required",
                    ClientName: "required",
                    ContactName: "required",
                    TelephoneNo: "required",
                    Email: {
                        required: true,
                        email: true
                    },
                    InformationRequest: "required"
                },
                messages: {
                    email: "Please enter a valid email address"
                },
                submitHandler: function () {
                    SendRequestLog()
                }
            });
        });

        function SendRequestLog() {
            var request = $.ajax({
                url: "http://166.62.89.209/financialservicecenter/index.php/Submission/SendRequestLog",
                method: "POST",
                data: $("#RequestLogForm").serialize(),
                dataType: "json"
            });
            request.done(function (data, textStatus, jqXHR) {
                if (data.success == "true") {
                    $("#successError").html(data.message).show();
                    $("#dangerError").hide();

                    $("#RequestLogForm")[0].reset();
                } else {
                    $("#dangerError").html(data.message).show();
                    $("#successError").hide();
                }
            });
            request.fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
            });
        }

        $(document).ready(function () {
            $("#TelephoneNo,#Requestfax").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });

        function getClientDetails() {
            var clientFileNo = $("#clientFileNo").val();
            var request = $.ajax({
                url: "index.php/Submission/getClientDetails",
                method: "POST",
                data: {clientFileNo: clientFileNo},
                dataType: "json"
            });
            request.done(function (data, textStatus, jqXHR) {
                $("#ClientName").val(data.ClientName);
                $("#ContactName").val(data.ContactName);
                $("#TelephoneNo").val(data.TelephoneNo);
                $("#Email").val(data.Email);
                $("#InformationRequest").val(data.InformationRequest);
                //$("#HowYouNeed").val(data.HowYouNeed);
                $('#HowYouNeed option[value="' + data.HowYouNeed + '"]').attr("selected", "selected");
                if (data.HowYouNeed == 'By Email') {
                    $("#byemail").show();
                    $("#Requestemail").val(data.Requestemail);

                } else if (data.HowYouNeed == 'By Fax') {
                    $("#byfax").show();
                    $("#Requestfax").val(data.Requestfax);
                }
                $("#Comments").val(data.Comments);
            });
            request.fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
            });
        }
    </script>

    <script>
        function myCreateFunction() {
            var table = document.getElementById("myTable");
            var row = table.insertRow(0);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "NEW CELL1";
            cell2.innerHTML = "NEW CELL2";
        }

        function myDeleteFunction() {
            document.getElementById("myTable").deleteRow(0);
        }
    </script>


@endsection()