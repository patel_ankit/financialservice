@extends('clientemployee.layouts.app')
@section('main-content')
    <style>.bg-color {
            background: #286db5;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        .input_fields_wrap_1 {
            display: inline-table;
            width: 100%;
        }

        .btn-add {
            z-index: 9999;
            position: absolute;
            left: 70%;
            right: 0;
            bottom: -65px;
            width: 150px;
        }

        .nav-tabs > li {
            width: 19.2% !important;
        }

        .nav-tabs > li > a {
            height: 40px;
            font-size: 16px !important;
            color: #000 !important;
        }

        .tab-content > .tab-pane {
            padding: 20px 0;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .cc {
            text-align: right;
            position: absolute;
            display: none;
            top: 4px;
            right: 26px;
        }

        .second-pro {
            margin-bottom: 10px;
            border: 2px solid #286db5;
        }

        .Pro-btn {
            background-color: #103b68;
            border-color: #103b68;
            width: 100%;
        }

        .Certificate-btn {
            font-size: 10.5px !important;
        }

        .clients-name-r {
            font-size: 16px;
            margin: 0;
            background-color: #286db5;
            color: #fff;
            padding: 8px;
        }

        .page-title {
            height: 80px;
        }

        .glyphicon-chevron-right:before {
            content: "\e080";
            color: #000;
        }

        .non-profit-img {
            max-width: 70px;
        }

        .pc-row {
            display: none;
        }

        .cc:after {
            content: "\f295";
            font-family: FontAwesome;
        }

        .Branch {
            display: inline-block;
            margin-bottom: 28px;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .glyphicon-chevron-right {
            top: 9px;
            text-align: center;
            font-size: 2rem;
            right: 9px;
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn3d.btn-info:hover, .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .field_wrapper {
            float: left;
            width: 100%;
        }

        #field0 {
            margin-bottom: 10px;
            float: left;
            width: 100%;
        }

        .dd {
            border: 1px solid #286db5;
            padding: 10px 0;
            margin-bottom: 10px;
            float: left;
            width: 100%;
        }

        .dd:nth-child(odd) {
            background: #e8f7fd;
        }

        .dd:nth-child(even) {
            background: #fff5dd;
        }

        .form-control-insu {
            border: 2px solid #286db5;
            border-radius: 3px;
            font-weight: normal;
        }

        .share_tabs_main {
            float: left;
            width: 100%;
            margin-bottom: 0px;
        }

        .share_tabs {
            float: left;
            width: 100%;
        }

        .share_tabs_other {
            float: left;
            width: 100%;
        }

        .share_tab {
            float: left;
            margin: 10px 1%;
        }

        .share_firstn {
            width: 14%;
        }

        .share_m {
            width: 5%;
        }

        .share_lastn {
            width: 14%;
        }

        .share_position {
            width: 15%;
        }

        .share_persentage {
            width: 14%;
        }

        .share_date {
            width: 15%;
        }

        .share_add {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_remove {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_total {
            text-align: right;
            width: 100%;
        }

        .addbtn {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #359e0b;
            border-radius: 2px;
            vertical-align: top;
        }

        .removebtn, .remove {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #d9534f;
            border-radius: 2px;
            vertical-align: top;
        }

        .arrow {
            width: 0px;
            position: relative;
            float: left;
        }

        input, select {
            font-weight: initial !important;
        }
    </style>
    <div class="content-wrapper">
        <div class="new-page-title">
            <div class="new-page-title-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="new_client_id">
                        <p>{{$common->filename}}</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="new_company_name">
                        <label>Company Name :</label>
                        <p>{{$common->company_name}}</p>
                    </div>
                    <div class="new_company_name">
                        <label>Business Name :</label>
                        <p>{{$common->business_name}}</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="new_images_sec">
                        @if(empty($common->business_cat_id))
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                @else
                                    @if(empty($common->business_brand_category_id))
                                        <div class="col-md-offset-2 col-md-4 col-sm-12 col-xs-12">
                                            @else
                                                <div class="col-md-4 col-sm-12 col-xs-12">
                                                    @endif
                                                    @endif
                                                    @foreach($business as $busi)
                                                        @if($busi->id==$common->business_id)
                                                            <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive">
                                                </div>
                                            @endif
                                            @endforeach
                                            @foreach($category as $cate)
                                                @if($common->business_cat_id==$cate->id)
                                                    @if(empty($common->business_brand_category_id))
                                                        <div class="arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                                            @else
                                                                <div class="arrow">
                                                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                </div>

                                                                <div class="col-md-4 col-sm-12 col-xs-12">
                                                                    @endif
                                                                    <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive"/>
                                                                </div>
                                                            @endif
                                                            @endforeach
                                                            @foreach($cb as $bb1)
                                                                @if($common->business_brand_category_id == $bb1->id)
                                                                    <div class="arrow">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                                        <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive"/>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                        </div>
                            </div>
                    </div>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-success">
                                    <div class="box-header">

                                        <div class="box-tools pull-right">

                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        @if(count($errors) > 0)
                                            @foreach($errors->all() as $error)
                                                <p class="alert alert-danger">{{ $error }}</p>
                                            @endforeach
                                        @endif
                                        <div class="panel with-nav-tabs panel-primary">

                                            <div class="panel-heading">
                                                <ul class="nav nav-tabs" id="myTab">
                                                    <li class="active"><a href="#tab1primary" data-toggle="tab">Basic Information</a></li>
                                                    <li><a href="#tab2primary" data-toggle="tab">Contact Information</a></li>
                                                    <li><a href="#tab6primary" data-toggle="tab">Security Information</a></li>
                                                    <li><a href="#tab14primary" data-toggle="tab">Login Information</a></li>
                                                    <li><a href="#tab11primary" data-toggle="tab">Service Information</a></li>
                                                    <li><a href="#tab4primary" data-toggle="tab">Interview</a></li>
                                                    <li><a href="#tab12primary" data-toggle="tab">Location Information</a></li>
                                                    <li><a href="#tab3primary" data-toggle="tab">Formation Information</a></li>
                                                    <li><a href="#tab8primary" data-toggle="tab">License Information</a></li>
                                                    <li><a href="#tab5primary" data-toggle="tab">Taxation Information</a></li>
                                                    <li><a href="#tab9primary" data-toggle="tab">Revenue Information</a></li>
                                                    <li><a href="#tab10primary" data-toggle="tab">Expense Information</a></li>
                                                    <li><a href="#tab15primary" data-toggle="tab"></a></li>
                                                    <li><a href="#tab16primary" data-toggle="tab">Other Information</a></li>
                                                    <li><a href="#tab7primary" data-toggle="tab"> Note</a></li>
                                                </ul>
                                            </div>
                                            <form method="post" action="{{ route('clientsetup.update', $common->cid)}}" class="form-horizontal" enctype="multipart/form-data">
                                                {{csrf_field()}}{{method_field('PATCH')}}
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane fade" id="tab9primary">
                                                            <div class="col-md-12">
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab10primary">
                                                            <div class="col-md-12">
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab11primary">
                                                            <div class="">
                                                                <div class="form-group bg-color">
                                                                    <label class="control-label col-md-3" style="width: 20%;text-align:right;">Price Selection :</label>
                                                                    <div class="col-md-2">
                                                                        <input name="pricetype" type="text" id="pricetype" class="form-control" style="pointer-events:none; background:#ccc !important;" value="@if(empty($clientser5->pricetype)) @else {{$clientser5->pricetype}} @endif">

                                                                    </div>

                                                                    <label class="control-label col-md-1" style="width: 106px;">Currency :</label>
                                                                    <div class="col-md-2">
                                                                        <input @foreach($currency as $cur) @if(empty($clientser5->currency)) @else @if($clientser5->currency==$cur->id) value="{{$cur->currency.' '.$cur->sign}}" @endif @endif   @endforeach name="currency" type="text" id="currency" class="form-control" style="pointer-events:none; background:#ccc !important;">

                                                                        <input name="ckq" type="hidden" id="ckq" class="ckq" value="@if(empty($clientser5->sign)) @else {{$clientser5->sign}} @endif">
                                                                    </div>
                                                                    <label class="control-label col-md-1" style="width: 142px;">Service Period :</label>
                                                                    <div class="col-md-2">
                                                                        <input @foreach($period as $period1) @if(empty($clientser5->serviceperiod)) @else @if($clientser5->serviceperiod==$period1->id) value="{{$period1->period}}" @endif @endif  @endforeach name="serviceperiod" type="text" id="serviceperiod" class="form-control" style="pointer-events:none; background:#ccc !important;">

                                                                    </div>
                                                                </div>
                                                                <?php $clientser2 = count($clientser);?>
                                                                @if($clientser2 !=NULL)
                                                                    @foreach($clientser as $clientser1)
                                                                        <div class="form-group ser" id="ser_{{$clientser1->id}}">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-3">
                                                                                    <label class="control-label">Type of Service :</label>
                                                                                    <input name="typeofservice[]" type="text" @foreach($typeofser as $typeofser1) @if($clientser1->typeofservice==$typeofser1->id) value="{{$typeofser1->typeofservice}}" @endif  @endforeach class="form-control" style="pointer-events:none; background:#ccc !important;">
                                                                                </div>
                                                                                <div id="regular" @if(!empty($clientsertitle1->id) && ($clientsertitle1->servicetype==$clientser1->typeofservice)) style="display:none" @endif>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label">Service Includes :</label>
                                                                                        <input name="serviceincludes[]" type="text" id="serviceincludes_{{$clientser1->id}}" class="form-control" value="{{$clientser1->serviceincludes}}" style="pointer-events:none; background:#ccc !important;">
                                                                                        <input name="serviceid[]" type="hidden" id="serviceid" class="form-control" value="{{$clientser1->id}}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @foreach($clientsertitle as $clientsertitle1)
                                                                                <div class="input_fields_wrap_notes" @if(!empty($clientsertitle1->id) && ($clientsertitle1->servicetype==$clientser1->typeofservice)) style="margin-top: 20px;float: left;width: 100%;" @else style="display:none" @endif >
                                                                                    <table class="table" id="table">
                                                                                        <thead>
                                                                                        <tr style="background:#00a0e3;color:#fff">
                                                                                            <th scope="col">Title</th>
                                                                                            <th scope="col">Note</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td><input type="hidden" name="taxid[]" value="{{$clientsertitle1->id}}"><select class="form-control pricetype" name="titles[]" id="titles3_{{$clientsertitle1->id}}">
                                                                                                    <option value="">---Select Service---</option>@foreach($taxtitle as $ti)
                                                                                                        <option value="{{$ti->title}}" @if($clientsertitle1->titles==$ti->title) selected @endif>{{$ti->title}}</option>@endforeach</select></td>
                                                                                            <td><input type="text" placeholder="Note" class="form-control" value="{{$clientsertitle1->taxnote}}" name="taxnote[]"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <div class="form-group" style="background-color:#fff5dd;padding:15px 0 23px 0;;width: 100%;margin: auto;">
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Type of Service :</label>
                                                                            <select name="typeofservice[]" type="text" id="typeofservice" class="form-control">
                                                                                <option value="">Type of Service</option>
                                                                                @foreach($typeofser as $typeofser1)
                                                                                    <option value="{{$typeofser1->id}}">{{$typeofser1->typeofservice}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div id="regular">

                                                                            <div class="col-md-4">
                                                                                <label class="control-label">Service Includes :</label>
                                                                                <input name="serviceincludes[]" type="text" id="serviceincludes" class="form-control">
                                                                                <input name="serviceid[]" type="hidden" id="serviceid" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <div class="input_fields_wrap_notes" style="display:none">
                                                                            <br>
                                                                            <br>
                                                                            <table class="table" id="table">
                                                                                <thead>
                                                                                <tr style="background:#00a0e3;color:#fff">
                                                                                    <th scope="col" style="width: 262px;">Title</th>
                                                                                    <th scope="col">Note</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="width: 262px;"><select class="form-control pricetype" name="titles[]" id="titles">
                                                                                            <option value="">---Select Service---</option>
                                                                                        </select></td>
                                                                                    <td><input type="text" placeholder="Note" class="form-control" name="taxnote[]"><input type="hidden" placeholder="Note" class="form-control" name="taxid[]"></td>

                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="education_service" id="education_service"></div>
                                                        </div>
                                                        <div class="tab-pane fade in active" id="tab1primary" style="pointer-events: none;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <input name="user_type" value="{{ $user->user_type }}" type="hidden" id="user_type" class="">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">File No : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <input type="text" class="form-control fsc-input" readonly maxlength="10" onkeyup="checkemail();" id="fileno" name="fileno" value="{{$common->filename}}" placeholder="">
                                                                                <div id="email_status"></div>
                                                                            </div>
                                                                            <label class="col-md-4 control-label">Status :</label>
                                                                            <div class="col-md-4">
                                                                                <input name="status" @if($common->status=='Approval') style=" background:#00ef00 !important" @elseif($common->status=='Active') style=" background:#00ef00 !important" @endif value="@if($common->status == 'Approval') Active @elseif($common->status == 'Active') {{$common->status}}  @else {{$common->status}} @endif" id="status" class="form-control fsc-input">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                                                    <label class="control-label col-md-3">Company Name : <span class="star-required">*</span><span class="tess">(Legal Name)</span></label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <input type="text" class="form-control fsc-input" name="company_name" id="company_name" value="{{$common->company_name}}">
                                                                                @if ($errors->has('company_name'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('company_name') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('business_name') ? ' has-error' : '' }}">
                                                                    <label class="control-label col-md-3">Business Name : <span class="star-required">*</span><span class="tess">(DBA Name)</span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="business_name" id="business_name" value="{{$common->business_name}}">
                                                                        @if ($errors->has('business_name'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('business_name') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Business Address : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="address" id="address" value="{{$common->address}}">
                                                                        @if($errors->has('address'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('address') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('address1') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Business Address 1: </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="address1" id="address1" value="{{$common->address1}}">
                                                                        @if($errors->has('address1'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('address1') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Country: </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="dropdown">
                                                                                    <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="{{$common->countryId}}">

                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">City / State / Zip : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">

                                                                            <div class="col-md-5">
                                                                                <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$common->city}}">
                                                                                @if($errors->has('city'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('city') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$common->stateId}}">

                                                                                    </select>
                                                                                    @if($errors->has('stateId'))
                                                                                        <span class="help-block">
																<strong>{{ $errors->first('stateId') }}</strong>
																</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <input type="text" class="form-control fsc-input" id="zip" name="zip" value="{{$common->zip}}" placeholder="Zip">
                                                                                @if($errors->has('zip'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('zip') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"></label>
                                                                    <div class="col-md-6">
                                                                        <input type="checkbox" id="billingtoo333" @if($common->mailing_address) checked @endif name="billingtoo333" onclick="FillBilling333(this.form)"><label for="billingtoo333" class="fsc-form-label"> Same As Above Business Address </label>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{$errors->has('mailing_address') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Mailing Address: </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" id="mailing_address" name="mailing_address" value="{{$common->mailing_address}}" placeholder="Mailing Address">
                                                                        @if($errors->has('mailing_address'))
                                                                            <span class="help-block">
													<strong>{{$errors->first('mailing_address')}}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('mailing_address1') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Mailing Address 1: </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" id="mailing_address1" name="mailing_address1" value="{{$common->mailing_address1}}" placeholder="Mailing Address 1">
                                                                        @if($errors->has('mailing_address1'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('mailing_address1') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('mailing_city') ? 'has-error' : ''}} {{ $errors->has('mailing_state') ? 'has-error' : ''}}{{ $errors->has('mailing_zip') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">City / State / Zip : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">

                                                                            <div class="col-md-5">
                                                                                <input type="text" class="form-control fsc-input" id="mailing_city" name="mailing_city" placeholder="City" value="{{$common->mailing_city}}">
                                                                                @if($errors->has('mailing_city'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('mailing_city') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <select name="mailing_state" id="mailing_state" class="form-control fsc-input">
                                                                                        @if($common->mailing_state==NULL)
                                                                                            <option value="">State</option>
                                                                                        @else
                                                                                            <option value="{{$common->mailing_state}}">{{$common->mailing_state}}</option>
                                                                                        @endif
                                                                                        <option value='AK'>AK</option>
                                                                                        <option value='AS'>AS</option>
                                                                                        <option value='AZ'>AZ</option>
                                                                                        <option value='AR'>AR</option>
                                                                                        <option value='CA'>CA</option>
                                                                                        <option value='CO'>CO</option>
                                                                                        <option value='CT'>CT</option>
                                                                                        <option value='DE'>DE</option>
                                                                                        <option value='DC'>DC</option>
                                                                                        <option value='FM'>FM</option>
                                                                                        <option value='FL'>FL</option>
                                                                                        <option value='GA'>GA</option>
                                                                                        <option value='GU'>GU</option>
                                                                                        <option value='HI'>HI</option>
                                                                                        <option value='ID'>ID</option>
                                                                                        <option value='IL'>IL</option>
                                                                                        <option value='IN'>IN</option>
                                                                                        <option value='IA'>IA</option>
                                                                                        <option value='KS'>KS</option>
                                                                                        <option value='KY'>KY</option>
                                                                                        <option value='LA'>LA</option>
                                                                                        <option value='ME'>ME</option>
                                                                                        <option value='MH'>MH</option>
                                                                                        <option value='MD'>MD</option>
                                                                                        <option value='MA'>MA</option>
                                                                                        <option value='MI'>MI</option>
                                                                                        <option value='MN'>MN</option>
                                                                                        <option value='MS'>MS</option>
                                                                                        <option value='MO'>MO</option>
                                                                                        <option value='MT'>MT</option>
                                                                                        <option value='NE'>NE</option>
                                                                                        <option value='NV'>NV</option>
                                                                                        <option value='NH'>NH</option>
                                                                                        <option value='NJ'>NJ</option>
                                                                                        <option value='NM'>NM</option>
                                                                                        <option value='NY'>NY</option>
                                                                                        <option value='NC'>NC</option>
                                                                                        <option value='ND'>ND</option>
                                                                                        <option value='MP'>MP</option>
                                                                                        <option value='OH'>OH</option>
                                                                                        <option value='OK'>OK</option>
                                                                                        <option value='OR'>OR</option>
                                                                                        <option value='PW'>PW</option>
                                                                                        <option value='PA'>PA</option>
                                                                                        <option value='PR'>PR</option>
                                                                                        <option value='RI'>RI</option>
                                                                                        <option value='SC'>SC</option>
                                                                                        <option value='SD'>SD</option>
                                                                                        <option value='TN'>TN</option>
                                                                                        <option value='TX'>TX</option>
                                                                                        <option value='UT'>UT</option>
                                                                                        <option value='VT'>VT</option>
                                                                                        <option value='VI'>VI</option>
                                                                                        <option value='VA'>VA</option>
                                                                                        <option value='WA'>WA</option>
                                                                                        <option value='WV'>WV</option>
                                                                                        <option value='WI'>WI</option>
                                                                                        <option value='WY'>WY</option>
                                                                                    </select>
                                                                                    @if($errors->has('mailing_state'))
                                                                                        <span class="help-block">
																<strong>{{ $errors->first('mailing_state') }}</strong>
																</span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <input type="text" class="form-control fsc-input" id="mailing_zip" name="mailing_zip" value="{{$common->mailing_zip}}" placeholder="Zip">
                                                                                @if($errors->has('mailing_zip'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('mailing_zip') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Business Email : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="abc@abc.com" value="{{$common->email}}">
                                                                        @if($errors->has('email'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('email') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('busimobile_noness_no') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Mobile / Cell No. : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" id="mobile_no" value="{{$common->mobile_no}}" name="mobile_no" placeholder="Business Telephone" onkeypress="PutNo()">
                                                                        @if($errors->has('mobile_no'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('mobile_no') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('business_no') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Business Telephone # : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" id="business_no" value="{{$common->business_no}}" name="business_no" placeholder="Business Telephone" onkeypress="PutNo()">
                                                                        @if($errors->has('business_no'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('business_no') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('business_fax') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Fax # :</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input business_fax1" value="{{$common->business_fax}}" id="business_fax" name="business_fax" placeholder="Business Fax">
                                                                        @if($errors->has('business_fax'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('business_fax') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Website : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" id="website" value="{{$common->website}}" name="website" placeholder="Website address">
                                                                        @if($errors->has('website'))
                                                                            <span class="help-block">
													<strong>{{ $errors->first('website') }}</strong>
													</span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab2primary">

                                                            <div class="input_fields_wrap_text">
                                                                <div class="Branch">
                                                                    <h1>Primary Contact Information</h1>
                                                                </div>

                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Name :</label>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <select type="text" class="form-control txtOnly" id="nametype" name="nametype">
                                                                                        <option value="mr" @if($common->nametype=='mr') selected="" @endif>Mr.</option>
                                                                                        <option value="mrs" @if($common->nametype=='mrs') selected="" @endif>Mrs.</option>
                                                                                        <option value="miss" @if($common->nametype=='miss') selected="" @endif>Miss.</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control textonly" id="firstname" name="firstname" value="{{$common->firstname}}">
                                                                                </div>
                                                                                <div class="col-md-1">
                                                                                    <div class="row">
                                                                                        <input type="text" class="form-control textonly" id="middlename" name="middlename" maxlength="1" value="{{$common->middlename}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="">
                                                                                    <div class="col-md-4">
                                                                                        <input type="text" class="form-control textonly" id="lastname" name="lastname" value="{{$common->lastname}}"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="btn btn-success addmore"><span style="margin-right: 0px;" class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                                                    </div>

                                                                    <div class="form-group" style="margin-bottom:0px;">
                                                                        <label class="control-label col-md-3"></label>
                                                                        <div class="col-md-3">
                                                                            <input type="checkbox" class="check" value="23" id="faxbli3" name="faxbli3" onclick="faxbli233(this.form)" @if($common->contact_address1==$common->address) checked @endif><label for="faxbli3"> Same As Business Address</label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group ">
                                                                        <label class="control-label col-md-3">Address 1 :</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" id="contact_address1" name="contact_address1" value="{{$common->contact_address1}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group ">
                                                                        <label class="control-label col-md-3">Address 2 :</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" id="contact_address2" name="contact_address2" value="{{$common->contact_address2}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group ">
                                                                        <label class="control-label col-md-3">City / State / Zip :</label>
                                                                        <div class="col-md-2">
                                                                            <input name="city_1" value="{{$common->city_1}}" type="text" id="city_1" class="textonly form-control">
                                                                        </div>
                                                                        <div class="">
                                                                            <div class="col-md-2">
                                                                                <select name="state_1" id="stateId1" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$common->state_1}}">

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="">
                                                                            <div class="col-md-2">
                                                                                <input name="zip_1" value="{{$common->zip_1}}" type="text" id="zip_1" class="form-control zip">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group ">
                                                                        <label class="control-label col-md-3">Telephone No. 1:</label>
                                                                        <div class="col-md-2">
                                                                            <input name="mobile_1" placeholder="(999) 999-9999" value="{{$common->etelephone1}}" type="tel" id="mobile_1" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999">
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                            <select name="mobiletype_1" id="mobiletype_1" class="form-control fsc-input" style="height:auto">
                                                                                <option value="Home" @if($common->eteletype1=='Home') selected @endif>Office</option>
                                                                                <option value="Mobile" @if($common->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                                <option value="Other" @if($common->eteletype1=='Other') selected @endif>Other</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                            <input class="form-control fsc-input" id="ext2_1" maxlength="5" name="ext2_1" readonly value="{{$common->eext1}}" placeholder="Ext" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Telephone No. 2:</label>
                                                                        <div class="col-md-2">
                                                                            <input name="mobile_2" placeholder="(999) 999-9999" value="{{$common->etelephone2}}" type="tel" id="mobile_2" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999">
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                            <select name="mobiletype_2" id="mobiletype_2" class="form-control fsc-input" style="height:auto">
                                                                                <option value="Home" @if($common->eteletype2	=='Home') selected @endif>Office</option>
                                                                                <option value="Mobile" @if($common->eteletype2	=='Mobile') selected @endif>Mobile</option>
                                                                                <option value="Other" @if($common->eteletype2 =='Other') selected @endif>Other</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                            <input class="form-control fsc-input" readonly id="ext2_2" maxlength="5" name="ext2_2" value="{{$common->eext2}}" placeholder="Ext" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-bottom:0px;">
                                                                        <label class="control-label col-md-3"></label>
                                                                        <div class="col-md-3">
                                                                            <input type="checkbox" class="check" value="23" name="faxbli1" id="faxbli1" onclick="faxbli(this.form)" @if($common->contact_fax_1!=NULL) checked @endif><label for="faxbli1"> Same As Business Fax</label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group ">
                                                                        <label class="control-label col-md-3">Fax No. :</label>
                                                                        <div class="col-md-2">
                                                                            <input name="contact_fax_1" placeholder="(999) 999-9999" value="{{$common->contact_fax_1}}" type="tel" id="contact_fax_1" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="form-group" style="margin-bottom:0px;">
                                                                            <label class="control-label col-md-3"></label>
                                                                            <div class="col-md-4">
                                                                                <input type="checkbox" class="check" value="23" id="emailbli1" name="emailbli1" onclick="emailbli(this.form)" @if($common->email_1!=NULL) checked @endif><label for="emailbli1" style="margin-left: 10px;"> Same As Primary Email</label>
                                                                            </div>
                                                                        </div>
                                                                        <label class="control-label col-md-3">Email :</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" id="email_1" name="email_1" value="{{$common->email_1}}">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @foreach($info as $in)
                                                                    <div class="con" id="con_{{$in}}">
                                                                        <div class="Branch">
                                                                            <h1>Secondery Contact Information</h1>
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Name :</label>
                                                                                <div class="col-md-6">
                                                                                    <div class="row">
                                                                                        <div class="col-md-3">
                                                                                            <select type="text" class="form-control txtOnly" id="nametype_sec" name="nametype_sec[]">
                                                                                                <option value="mr" @if($in->nametype_sec=='mr') selected @endif>Mr.</option>
                                                                                                <option value="mrs" @if($in->nametype_sec=='mrs') selected @endif>Mrs.</option>
                                                                                                <option value="miss" @if($in->nametype_sec=='miss') selected @endif>Miss.</option>
                                                                                            </select>
                                                                                        </div>

                                                                                        <div class="col-md-4">
                                                                                            <input type="text" class="form-control textonly" id="firstname_sec" name="firstname_sec[]" value="{{$in->firstname_sec}}" placeholder="First Name">
                                                                                        </div>

                                                                                        <div class="col-md-1">
                                                                                            <div class="row">
                                                                                                <input type="text" class="form-control textonly" id="middlename_sec" value="{{$in->middlename_sec}}" name="middlename_sec[]" maxlength="1" placeholder="M">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <div class="col-md-4">
                                                                                                <input type="text" class="form-control textonly" id="lastname_sec" value="{{$in->lastname_sec}}" name="lastname_sec[]" value="" placeholder="Last Name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <a id="{{$in->id}}" class="btn btn-danger deletecontact"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <div class="form-group ">
                                                                                <label class="control-label col-md-3">Address 1 :</label>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" class="form-control" id="contact_address_sec_1" placeholder="Address 1" name="contact_address_sec_1[]" value="{{$in->contact_address_sec_1}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group ">
                                                                                <label class="control-label col-md-3">Address 2 :</label>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" class="form-control" id="contact_address_sec_2" value="{{$in->contact_address_sec_2}}" name="contact_address_sec_2[]" placeholder="Address 2">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group ">
                                                                                <label class="control-label col-md-3">City / State / Zip :</label>
                                                                                <div class="col-md-2">
                                                                                    <input name="city_sec" value="{{$in->city_sec}}" type="text" id="city_sec" placeholder="City" class="textonly form-control">
                                                                                </div>
                                                                                <div class="">
                                                                                    <div class="col-md-2">
                                                                                        <select name="state_sec[]" id="stateId_sec" class="form-control fsc-input bfh-states" data-country="countries_states1">

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="">
                                                                                    <div class="col-md-2">
                                                                                        <input name="zip_sec[]" placeholder="zip" type="text" id="zip_sec" class="form-control zip" value="{{$in->zip_sec}}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group ">
                                                                                <label class="control-label col-md-3">Telephone No. 1:</label>
                                                                                <div class="col-md-2">
                                                                                    <input name="mobile_sec_1[]" placeholder="(999) 999-9999" value="" type="tel" id="mobile_sec" class="form-control phone" value="{{$in->mobile_sec_1}}">
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                                    <select name="mobiletype_sec_1[]" id="mobiletype_sec_{{$in->id}}" class="form-control fsc-input" style="height:auto">
                                                                                        <option value="Home" @if($in->mobiletype_sec_1=='Home') selected @endif>Office</option>
                                                                                        <option value="Mobile" @if($in->mobiletype_sec_1=='Mobile') selected @endif>Mobile</option>
                                                                                        <option value="Other" @if($in->mobiletype_sec_1=='Other') selected @endif>Other</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                                    <input class="form-control fsc-input" id="ext_sec_{{$in->id}}" maxlength="5" name="ext_sec_1[]" readonly placeholder="Ext" type="text" value="{{$in->ext_sec_1}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Telephone No. 2:</label>
                                                                                <div class="col-md-2">
                                                                                    <input name="mobile_sec_2[]" placeholder="(999) 999-9999" value="{{$in->mobile_sec_2}}" type="tel" id="mobile_sec1_{{$in->id}}" class="form-control phone">
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                                    <select name="mobiletype_sec2[]" id="mobiletype_sec2_{{$in->id}}" class="form-control fsc-input" style="height:auto">
                                                                                        <option value="Home" @if($in->mobiletype_sec2	=='Home') selected @endif>Office</option>
                                                                                        <option value="Mobile" @if($in->mobiletype_sec2	=='Mobile') selected @endif>Mobile</option>
                                                                                        <option value="Other" @if($in->mobiletype_sec2 =='Other') selected @endif>Other</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                                    <input class="form-control fsc-input" readonly id="ext_sec_2_{{$in->id}}" maxlength="5" name="ext_sec_2[]" value="{{$in->ext_sec_2}}" placeholder="Ext" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                                <label class="control-label col-md-3"></label>
                                                                                <div class="col-md-3">
                                                                                    <label><input type="checkbox" class="" name="faxbli2_{{$in->id}}" onclick="faxbli_{{$in->id}}(this.form)"> Same As Above Business Fax</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group ">
                                                                                <label class="control-label col-md-3">Fax No. :</label>
                                                                                <div class="col-md-2">
                                                                                    <input name="contact_fax_sec[]" placeholder="(999) 999-9999" value="{{$in->contact_fax_sec}}" type="tel" id="contact_fax_sec" class="phone form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="form-group" style="margin-bottom:0px;">
                                                                                    <label class="control-label col-md-3"></label>
                                                                                    <div class="col-md-4">
                                                                                        <input type="checkbox" id="aboveemail" class="" name="emailbli2_{{$in->id}}" onclick="emailbl_{{$in->id}}(this.form)"> <label for="aboveemail" style="margin-left: 10px;"> Same As Above Email</label>
                                                                                    </div>

                                                                                </div>
                                                                                <label class="control-label col-md-3">Email :</label>
                                                                                <div class="col-md-4">

                                                                                    <input type="text" class="form-control" id="email_sec" name="email_sec[]" value="{{$in->email_sec}}" placeholer="Email">
                                                                                    <input type="hidden" class="form-control" id="secid" name="secid[]" value="{{$in->id}}" placeholer="Email">

                                                                                </div>
                                                                            </div>

                                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <script>
                                                                        function emailbl_{{$in->id}}(f) {// alert();
                                                                            if (f.emailbli2_{{$in->id}}.checked == true) {// alert();
                                                                                f.email_sec.value = f.email.value;
                                                                            } else {
                                                                                f.email_sec.value = '';
                                                                            }

                                                                        }

                                                                        function faxbli_{{$in->id}}(f) { //alert();
                                                                            if (f.faxbli2_{{$in->id}}.checked == true) {
                                                                                f.contact_fax_sec.value = f.business_fax.value;
                                                                            } else {
                                                                                f.contact_fax_sec.value = '';
                                                                            }

                                                                        }</script>
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            var date = $('#ext_sec_{{$in->id}}').val();
                                                                            $('#mobiletype_sec_{{$in->id}}').on('change', function () {
                                                                                if (this.value == 'Home') {
                                                                                    //alert();
//document.getElementById('#ext_sec_35').removeAttribute('readonly');
                                                                                    $("#ext_sec_{{$in->id}}").removeAttr("readonly");
                                                                                    $('#ext_sec_{{$in->id}}').val();
                                                                                } else if (this.value == 'Mobile') {
                                                                                    $("#ext_sec_{{$in->id}}").removeAttr("readonly");
                                                                                    $('#ext_sec_{{$in->id}}').val();
                                                                                } else {
                                                                                    $("#ext_sec_{{$in->id}}").attr("readonly", true); //$("#ext_sec_{{$in->id}}").addAttr("readonly");
//document.getElementById('#ext_sec_{{$in->id}}').readOnly =true;
                                                                                    $('#ext_sec_{{$in->id}}').val('');
                                                                                }
                                                                            });
                                                                        });
                                                                    </script>

                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            var date = $('#ext_sec_2_{{$in->id}}').val();
                                                                            $('#mobiletype_sec2_{{$in->id}}').on('change', function () {
                                                                                if (this.value == 'Home') {
                                                                                    //alert();
//document.getElementById('#ext_sec_35').removeAttribute('readonly');
                                                                                    $("#ext_sec_2_{{$in->id}}").removeAttr("readonly");
                                                                                    $('#ext_sec_2_{{$in->id}}').val();
                                                                                } else if (this.value == 'Mobile') {
                                                                                    $("#ext_sec_2_{{$in->id}}").removeAttr("readonly");
                                                                                    $('#ext_sec_2_{{$in->id}}').val();
                                                                                } else {
                                                                                    $("#ext_sec_2_{{$in->id}}").attr("readonly", true); //$("#ext_sec_{{$in->id}}").addAttr("readonly");
//document.getElementById('#ext_sec_{{$in->id}}').readOnly =true;
                                                                                    $('#ext_sec_2_{{$in->id}}').val('');
                                                                                }
                                                                            });
                                                                        });
                                                                    </script>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade in active" id="tab13primary" style="pointer-events: none;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <input name="user_type" value="{{ $user->user_type }}" type="hidden" id="user_type" class="">

                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade in active" id="tab14primary" style="pointer-events: none;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <input name="user_type" value="{{ $user->user_type }}" type="hidden" id="user_type" class="">

                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade in active" id="tab15primary" style="pointer-events: none;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <input name="user_type" value="{{ $user->user_type }}" type="hidden" id="user_type" class="">

                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade in active" id="tab16primary" style="pointer-events: none;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <input name="user_type" value="{{ $user->user_type }}" type="hidden" id="user_type" class="">

                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab4primary">
                                                            <div class="col-md-12">

                                                                <div class="Branch">
                                                                    <h1>Interview &nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp; Client</h1>
                                                                </div>
                                                                <div class="panel-group" id="accordion" role="tablist">

                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title">
                                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                                                    <span class="ac_name_first">Type of Entity</span>
                                                                                    <span class="ac_name_middel">Formation Information</span>
                                                                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body accordion-body">
                                                                                <table class="table table-bordered table-hover" id="tab_logic">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th class="text-center" style="width:5%;">No</th>
                                                                                        <th class="text-center" style="width:60%;">Question</th>
                                                                                        <th class="text-center">Answer</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <?php $dbcolors = explode(',', $common->type_of_entity_answer);?>

                                                                                    @foreach($question as $ques)
                                                                                        @if($ques->type=='Type of Entity')
                                                                                            <tr class='ccd' data-patientType="No" id="{{$loop->index+1}}">
                                                                                                <td class="text-center">{{$loop->index+1}}</td>
                                                                                                <td>
                                                                                                    <input type="text" name='type_of_entity[]' readonly placeholder='Name' value="{{ucwords($ques->question)}}" class="form-control-accordion"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    @if($ques->question=='Date of Incorporation/Organization')
                                                                                                        <input type="text" name='type_of_entity_answer3' readonly placeholder='' value="{{$common->type_of_entity_answer3}}" class="date form-control-accordion"/>
                                                                                                    @else
                                                                                                        <select name='type_of_entity_answer[]' id="patient_type_{{$loop->index+1}}" class="form-control-accordion">
                                                                                                            <option value="">---Select Answer---</option>
                                                                                                            @foreach($answer as $ans)
                                                                                                                @if($ans->q_id==$ques->id)
                                                                                                                    <option value="{{$ans->answer}}" @if(in_array($ans->answer,$dbcolors)) Selected @endif >{{$ans->answer}}</option>
                                                                                                                @endif
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                    @endif
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endif
                                                                                    @endforeach
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab" id="headingtwo">
                                                                            <h4 class="panel-title">
                                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
                                                                                    <span class="ac_name_first">Type of Business</span>
                                                                                    <span class="ac_name_middel">Business Information</span>
                                                                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                            <div class="panel-body accordion-body">
                                                                                <table class="table table-bordered table-hover" id="tab_logic">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th class="text-center" style="width:5%;">No</th>
                                                                                        <th class="text-center" style="width:60%;">Question</th>
                                                                                        <th class="text-center">Answer</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @foreach($question as $ques)
                                                                                        @if($ques->type=='Type of Business')
                                                                                            <tr>
                                                                                                <td class="text-center">{{$loop->index+1}}</td>
                                                                                                <td><input type="text" name='type_of_entity[]' readonly placeholder='Name' value="{{ucwords($ques->question)}}" class="form-control-accordion"/></td>
                                                                                                <td>
                                                                                                    @if($ques->question=='What is your business start date ?')

                                                                                                        <input type="text" name="type_of_entity_answer1" readonly placeholder='' value="{{$common->type_of_entity_answer1}}" class="date form-control-accordion"/>

                                                                                                    @elseif($ques->question=='Exactly what you do ? Explain in details')

                                                                                                        <input type="text" name="type_of_entity_answer2" value="{{$common->type_of_entity_answer2}}" class="form-control-accordion"/>

                                                                                                    @else
                                                                                                        <select name="type_of_entity_answer[]" class="form-control-accordion">
                                                                                                            <option value="">---Select Answer---</option>
                                                                                                            @foreach($answer as $ans)
                                                                                                                @if($ans->q_id==$ques->id)
                                                                                                                    <option value="{{$ans->answer}}" @if(in_array($ans->answer,$dbcolors)) Selected @endif>{{$ans->answer}}</option>
                                                                                                                @endif
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                    @endif
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endif
                                                                                    @endforeach
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab2primary">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"></label>
                                                                    <div class="col-md-6">
                                                                        <label class="fsc-form-label"><input type="checkbox" checked="" name="billingtoo1" onclick="FillBilling1(this.form)">&nbsp; Same As Company Address</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group {{ $errors->has('dbaname') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Store Name : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="business_store_name" id="business_store_name" value="{{$common->business_name}}" placeholder="Business Name">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Physical Location Address 1 : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="business_address" id="business_address" value="{{$common->address}}" placeholder="Address">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Physical Location Address 2 : </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control fsc-input" name="business_address_1" id="business_address_1" value="{{$common->address}}" placeholder="Address">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} ">
                                                                    <label class="control-label col-md-3">City / State / Zip : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">

                                                                            <div class="col-md-5">
                                                                                <input type="text" class="form-control fsc-input" id="business_city" name="business_city" placeholder="business_city" value="{{$common->business_city}}">
                                                                                @if($errors->has('city'))
                                                                                    <span class="help-block">
															<strong>{{ $errors->first('city') }}</strong>
															</span>
                                                                                @endif
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <select name="business_state" id="business_state" class="form-control fsc-input">
                                                                                        @if(empty($common->business_state))
                                                                                            <option value="">State</option>
                                                                                        @else
                                                                                            <option value="{{$common->business_state}}">{{$common->business_state}}</option>
                                                                                        @endif
                                                                                        <option value='AK'>AK</option>
                                                                                        <option value='AS'>AS</option>
                                                                                        <option value='AZ'>AZ</option>
                                                                                        <option value='AR'>AR</option>
                                                                                        <option value='CA'>CA</option>
                                                                                        <option value='CO'>CO</option>
                                                                                        <option value='CT'>CT</option>
                                                                                        <option value='DE'>DE</option>
                                                                                        <option value='DC'>DC</option>
                                                                                        <option value='FM'>FM</option>
                                                                                        <option value='FL'>FL</option>
                                                                                        <option value='GA'>GA</option>
                                                                                        <option value='GU'>GU</option>
                                                                                        <option value='HI'>HI</option>
                                                                                        <option value='ID'>ID</option>
                                                                                        <option value='IL'>IL</option>
                                                                                        <option value='IN'>IN</option>
                                                                                        <option value='IA'>IA</option>
                                                                                        <option value='KS'>KS</option>
                                                                                        <option value='KY'>KY</option>
                                                                                        <option value='LA'>LA</option>
                                                                                        <option value='ME'>ME</option>
                                                                                        <option value='MH'>MH</option>
                                                                                        <option value='MD'>MD</option>
                                                                                        <option value='MA'>MA</option>
                                                                                        <option value='MI'>MI</option>
                                                                                        <option value='MN'>MN</option>
                                                                                        <option value='MS'>MS</option>
                                                                                        <option value='MO'>MO</option>
                                                                                        <option value='MT'>MT</option>
                                                                                        <option value='NE'>NE</option>
                                                                                        <option value='NV'>NV</option>
                                                                                        <option value='NH'>NH</option>
                                                                                        <option value='NJ'>NJ</option>
                                                                                        <option value='NM'>NM</option>
                                                                                        <option value='NY'>NY</option>
                                                                                        <option value='NC'>NC</option>
                                                                                        <option value='ND'>ND</option>
                                                                                        <option value='MP'>MP</option>
                                                                                        <option value='OH'>OH</option>
                                                                                        <option value='OK'>OK</option>
                                                                                        <option value='OR'>OR</option>
                                                                                        <option value='PW'>PW</option>
                                                                                        <option value='PA'>PA</option>
                                                                                        <option value='PR'>PR</option>
                                                                                        <option value='RI'>RI</option>
                                                                                        <option value='SC'>SC</option>
                                                                                        <option value='SD'>SD</option>
                                                                                        <option value='TN'>TN</option>
                                                                                        <option value='TX'>TX</option>
                                                                                        <option value='UT'>UT</option>
                                                                                        <option value='VT'>VT</option>
                                                                                        <option value='VI'>VI</option>
                                                                                        <option value='VA'>VA</option>
                                                                                        <option value='WA'>WA</option>
                                                                                        <option value='WV'>WV</option>
                                                                                        <option value='WI'>WI</option>
                                                                                        <option value='WY'>WY</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <input type="text" class="form-control fsc-input" id="bussiness_zip" name="bussiness_zip" value="{{$common->bussiness_zip}}" placeholder="Zip">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                                    <label class="control-label col-md-3">Country : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="dropdown">
                                                                                    <select name="business_country" id="business_country" class="form-control fsc-input">
                                                                                        <option value='INDIA' @if($common->business_country=='INDIA') selected @endif>INDIA</option>
                                                                                        <option value='USA' @if($common->business_country=='USA') selected @endif>USA</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">County / Code : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="dropdown">
                                                                                    <select name="location_county" id="location_county" class="form-control fsc-input">
                                                                                        @foreach($taxstate as $v)
                                                                                            <option value="{{$v->county}}" @if($v->county==Auth::user()->physical_county) selected @endif>{{$v->county}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 ">
                                                                                <input name="physical_county_no" value="{{ Auth::user()->physical_county_no}}" type="text" id="physical_county_no" readonly class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab3primary">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Type of Entity / Form :</label>
                                                                    <div class="col-md-6">
                                                                        <select type="text" class="form-control fsc-input" name="typeofservice" id="typeofservice" placeholder="Enter Your Company Name" data-bv-field="typeofservice">
                                                                            <option value=""> ---Select---</option>
                                                                            <option value="C Corporation" @if($common->typeofservice=='C Corporation') selected @endif>C Corporation</option>
                                                                            <option value="S Corporation" @if($common->typeofservice=='S Corporation') selected @endif>S Corporation</option>
                                                                            <option value="Double Member LLC" @if($common->typeofservice=='Double Member LLC') selected @endif>Double Member LLC</option>
                                                                            <option value="Single Member LLC" @if($common->typeofservice=='Single Member LLC') selected @endif>Single Member LLC</option>
                                                                            <option value="Payroll" @if($common->typeofservice=='Payroll') selected @endif>Payroll</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="Branch">
                                                                    <h1>Company Formation Information</h1>
                                                                </div>
                                                                <br/>

                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">State of Formation :</label>
                                                                        <div class="col-md-2">
                                                                            <select name="state_of_formation" id="state_of_formation" class="form-control fsc-input state_of_formation">
                                                                                <option value="GA">GA</option>
                                                                                <option value="AK">AK</option>
                                                                                <option value="AS">AS</option>
                                                                                <option value="AZ">AZ</option>
                                                                                <option value="AR">AR</option>
                                                                                <option value="CA">CA</option>
                                                                                <option value="CO">CO</option>
                                                                                <option value="CT">CT</option>
                                                                                <option value="DE">DE</option>
                                                                                <option value="DC">DC</option>
                                                                                <option value="FM">FM</option>
                                                                                <option value="FL">FL</option>
                                                                                <option value="GA">GA</option>
                                                                                <option value="GU">GU</option>
                                                                                <option value="HI">HI</option>
                                                                                <option value="ID">ID</option>
                                                                                <option value="IL">IL</option>
                                                                                <option value="IN">IN</option>
                                                                                <option value="IA">IA</option>
                                                                                <option value="KS">KS</option>
                                                                                <option value="KY">KY</option>
                                                                                <option value="LA">LA</option>
                                                                                <option value="ME">ME</option>
                                                                                <option value="MH">MH</option>
                                                                                <option value="MD">MD</option>
                                                                                <option value="MA">MA</option>
                                                                                <option value="MI">MI</option>
                                                                                <option value="MN">MN</option>
                                                                                <option value="MS">MS</option>
                                                                                <option value="MO">MO</option>
                                                                                <option value="MT">MT</option>
                                                                                <option value="NE">NE</option>
                                                                                <option value="NV">NV</option>
                                                                                <option value="NH">NH</option>
                                                                                <option value="NJ">NJ</option>
                                                                                <option value="NM">NM</option>
                                                                                <option value="NY">NY</option>
                                                                                <option value="NC">NC</option>
                                                                                <option value="ND">ND</option>
                                                                                <option value="MP">MP</option>
                                                                                <option value="OH">OH</option>
                                                                                <option value="OK">OK</option>
                                                                                <option value="OR">OR</option>
                                                                                <option value="PW">PW</option>
                                                                                <option value="PA">PA</option>
                                                                                <option value="PR">PR</option>
                                                                                <option value="RI">RI</option>
                                                                                <option value="SC">SC</option>
                                                                                <option value="SD">SD</option>
                                                                                <option value="TN">TN</option>
                                                                                <option value="TX">TX</option>
                                                                                <option value="UT">UT</option>
                                                                                <option value="VT">VT</option>
                                                                                <option value="VI">VI</option>
                                                                                <option value="VA">VA</option>
                                                                                <option value="WA">WA</option>
                                                                                <option value="WV">WV</option>
                                                                                <option value="WI">WI</option>
                                                                                <option value="WY">WY</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Legal Name :</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" id="legal_name1" readonly="" name="legal_name1" value="{{$common->company_name}}"><input type="text" style="display:none" class="form-control" id="legal_name" readonly="" name="legal_name" value="FINANCIAL SERVICE CENTER INC">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Control Number :</label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" class="form-control" maxlength="7" id="contact_number1" name="contact_number" value="{{$common->contact_number}}" style="text-transform: capitalize;">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Secretary of State :</label>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <a data-toggle="modal" num="SOS Certificate" class="btn btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtn Pro-btn">Certificate of Corporation</a>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <a data-toggle="modal" num="SOS-AOI" class="btn btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtn Pro-btn">Articles of Incorporation</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Agent Name:</label>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-5">
                                                                                    <input name="agent_fname" value="{{$common->agent_fname}}" type="text" id="agent_fname" placeholder="First name" class="textonly form-control">
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <input name="agent_mname" value="{{$common->agent_mname}}" maxlength="1" type="text" placeholder="M" id="agent_mname" class="textonly form-control">
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <input name="agent_lname" value="{{$common->agent_lname}}" type="text" placeholder="Last Name" id="agent_lname" class="textonly form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="Branch">
                                                                    <h1>Shareholder / Officer Information</h1>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="fsc-form-label"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)" @if(empty($agent)) @else checked @endif>&nbsp; Same As Agent</label>
                                                                </div>
                                                                <div class="">
                                                                    <input class="form-control" name="agent1" type="hidden" id="agent1" placeholder="First name" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent2" type="hidden" id="agent2" placeholder="First name" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent3" type="hidden" id="agent3" placeholder="First name" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent4" type="hidden" id="agent4" placeholder="First name" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent5" type="hidden" id="agent5" placeholder="First name" class="textonly form-control"/>
                                                                    <input class="form-control" name="agent6" type="hidden" id="agent6" placeholder="First name" class="textonly form-control"/>
                                                                    <div class="">
                                                                        <div class="share_tabs_main">
                                                                            <div class="share_tabs">
                                                                                <div class="share_tab share_firstn" style="margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">First Name</label>
                                                                                </div>
                                                                                <div class="share_tab share_m" style="margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">M</label>
                                                                                </div>
                                                                                <div class="share_tab share_lastn" style="margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">Last Name</label>
                                                                                </div>
                                                                                <div class="share_tab share_position" style="margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">Position</label>
                                                                                </div>
                                                                                <div class="share_tab share_persentage" style="width: 9%; margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">Percentage</label>
                                                                                </div>
                                                                                <div class="share_tab share_date" style="margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">Effective Date</label>
                                                                                </div>
                                                                                <div class="share_tab share_date" style="width: 9%;margin: 10px 1% -10px 1%;">
                                                                                    <label style="font-size: 14px;color:#404040">Status</label>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    @foreach($admin_shareholder as $ak)
                                                                        @if($ak->agent_fname1==NULL)
                                                                        @else
                                                                            <style>
                                                                                .input_fields_wrap_shareholder {
                                                                                    display: none
                                                                                }
                                                                            </style>
                                                                            <div class="">
                                                                                <div class="share_tabs_main" id="input_fields_wrap_2">
                                                                                    <div class="share_tabs {{ $errors->has('agent_fname1') ? ' has-error' : '' }}">
                                                                                        <div class="share_tab share_firstn">
                                                                                            <input name="conid[]" value="{{$ak->id}}" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                                                            <input class="form-control" name="agent_fname1[]" value="{{ $ak->agent_fname1}}" type="text" id="agent_fname1" placeholder="First name" class="textonly form-control"/>
                                                                                        </div>
                                                                                        <div class="share_tab share_m">
                                                                                            <input class="form-control" name="agent_mname1[]" value="{{ $ak->agent_mname1}}" type="text" placeholder="M" id="agent_mname1" class="textonly form-control"/>
                                                                                        </div>
                                                                                        <div class="share_tab share_lastn">
                                                                                            <input class="form-control" name="agent_lname1[]" value="{{ $ak->agent_lname1}}" type="text" placeholder="Last Name" id="agent_lname1" class="textonly form-control"/>
                                                                                        </div>
                                                                                        <div class="share_tab share_position">
                                                                                            <select class="form-control" name="agent_position[]" id="agent_position11">
                                                                                                <option value="">Position</option>
                                                                                                <option @if($ak->agent_position=='Agent')  Selected hidden @else  @if(empty($agent->id)) @else  hidden @endif @endif value="Agent">Agent</option>
                                                                                                <option @if($ak->agent_position=='Agent')  disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='CEO')  selected hidden @else @if(empty($ceo->id))   @else  hidden @endif  @endif  value="CEO">CEO</option>
                                                                                                <option @if($ak->agent_position=='Agent')  disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='CFO')  Selected hidden @else @if(empty($cfo->id))  @else  hidden @endif @endif value="CFO">CFO</option>
                                                                                                <option @if($ak->agent_position=='Agent')  disabled @else @if($ak->agent_position=='Sec') disabled @ednif @endif @endif @if($ak->agent_position=='Secretary') Selected hidden @else  @if(empty($sece->id))  @else  hidden @endif  @endif  value="Secretary">Secretary</option>
                                                                                                <option @if(($ak->agent_position=='Secretary' && $ak->agent_position=='CEO' && $ak->agent_position=='CFO')) hidden @endif  @if($ak->agent_position=='Sec') Selected hidden @else @if(empty($sec->id))  @else  hidden @endif @endif value="Sec">CEO / CFO / Sec.</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="share_tab share_persentage" style="width: 9%;">
                                                                                            <input name="agent_per[]" value="@if($ak->agent_per >0)  {{$ak->agent_per}} @endif" type="text" placeholder="" id="agent_per11" class="txtOnly form-control num numeric1"/>
                                                                                            <div class="cc"></div>
                                                                                        </div>
                                                                                        <div class="share_tab share_date" style="width: 11%;">
                                                                                            <?php $date = $ak->effective_date;?>
                                                                                            <input name="effective_date[]" value="{{$date}}" placeholder="Effective Date" id="effective_date" class="form-control effective_date2"/>
                                                                                        </div>
                                                                                        <div class="share_tab share_date" style="width: 9%;">
                                                                                            <select class="form-control greenText" onchange="this.className=this.options[this.selectedIndex].className"
                                                                                                    class="greenText" name="agentstatus[]" id="agentstatus">
                                                                                                <option value="">Status</option>
                                                                                                <option value="Active" @if($ak->agentstatus=='Active')  Selected @endif class="greenText">Active</option>
                                                                                                <option value="In-Active" @if($ak->agentstatus=='In-Active')  Selected @endif class="redText">In-Active</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="share_add">
                                                                                            <a href="{{ route('admind3.admindelete3',$ak->id) }}" id="add_row1" role="button" class="btn btn-danger removebtn" title="Add field" data-toggle="modal">Remove</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        @endif
                                                                    @endforeach

                                                                    <div class="col-md-12">
                                                                        <div class="mainfile">
                                                                            <div class="input_fields_wrap input_fields_wrap_shareholder">
                                                                                <div class="input_fields_wrap_1">
                                                                                    <input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                                                    <div class="share_tabs_main">
                                                                                        <div class="share_tabs ">
                                                                                            <div class="share_tab share_firstn">
                                                                                                <input name="agent_fname1[]" value="" type="text" id="agent_fname2" placeholder="First name" class="textonly form-control"/>
                                                                                            </div>
                                                                                            <div class="share_tab share_m">
                                                                                                <input name="agent_mname1[]" value="" type="text" placeholder="M" id="agent_mname2" class="textonly form-control"/>
                                                                                            </div>
                                                                                            <div class="share_tab share_lastn">
                                                                                                <input name="agent_lname1[]" value="" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control"/>
                                                                                            </div>
                                                                                            <div class="share_tab share_position">
                                                                                                <select name="agent_position[]" id="agent_position" class="form-control agent_position">
                                                                                                    <option value="">Position</option>
                                                                                                    <option value="Agent" @if(empty($agent->agent_position)) @else disabled @endif>Agent</option>
                                                                                                    <option value="CEO" @if(empty($sec->agent_position))  @else disabled @endif @if(empty($ceo->agent_position)) @else disabled @endif>CEO</option>
                                                                                                    <option value="CFO" @if(empty($sec->agent_position))  @else disabled @endif @if(empty($cfo->agent_position))  @else disabled @endif>CFO</option>
                                                                                                    <option value="Secretary" @if(empty($sec->agent_position))  @else disabled @endif @if(empty($sece->agent_position))  @else disabled @endif>Secretary</option>
                                                                                                    <option value="Sec" @if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))) @else disabled @endif @if(empty($sec->agent_position))  @else disabled @endif >CEO / CFO / Sec.</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="share_tab share_persentage">
                                                                                                <input name="agent_per[]" value="" type="text" placeholder="" id="agent_per" placeholder="Feb-07-1997" class="txtOnly form-control numeric1  num"/>
                                                                                                <div class="cc"></div>
                                                                                            </div>
                                                                                            <div class="share_tab share_date">
                                                                                                <input name="effective_date[]" value="" maxlength="10" type="text" placeholder="Effective Date" id="effective_date" class="txtOnly form-control effective_date2"/>
                                                                                            </div>
                                                                                            <div class="share_add">
                                                                                                <a href="javascript:void(0)" id="add_row1" class="btn btn-danger remove" title="Add field">Remove</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="share_tabs_main">
                                                                        <div class="share_tabs_other">
                                                                            <div class="share_tab share_firstn">&nbsp;</div>
                                                                            <div class="share_tab share_m">&nbsp;</div>
                                                                            <div class="share_tab share_lastn">&nbsp;</div>
                                                                            <div class="share_tab share_position">
                                                                                <label class="share_total">Total :</label>
                                                                            </div>
                                                                            <div class="share_tab share_persentage">
                                                                                <input name="total" style="width:67%;margin-left:1.3%; !important" value="{{ number_format($total,2)}}%" type="text" placeholder="" id="total" class="txtOnly form-control total" readonly/>
                                                                                <p style="display:none;color:red;float: left;" id="t1">This should be not more then 100.00%</p>
                                                                            </div>
                                                                            <div class="share_tab share_remove">
                                                                                <button type="button" id="add_row1" class="btn btn-success addbtn">ADD</button>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fscemployee/clients')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane fade" id="tab5primary">

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch">
                                                                    <h1>For Income-Tax Purpose, Select Type of Corporation :</h1>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="">
                                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 fsc-form-row">
                                                                            <label class="col-md-12 fsc-form-label" for="">Type of Corp : <span class="not-reqire">*</span></label>
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input name="typeofcorp" value="{{$common->typeofservice}}" type="text" placeholder="" id="typeofcorp" class="form-control" readonly/>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 fsc-form-row">
                                                                            <div class="row">
                                                                                <label class="col-md-12 fsc-form-label" for="">Type of Form : <span class="not-reqire">*</span></label>
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                    <select type="text" class="form-control fsc-input" name="typeofcorp1" id="typeofcorp1" placeholder="Enter Your Company Name" onchange="showDiv(this)">
                                                                                        <option value="{{$common->typeofcorp1}}">{{$common->typeofcorp1}}</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-row">
                                                                            <label class="col-md-12 fsc-form-label" for="">Effective Date : <span class="not-reqire">*</span></label>
                                                                            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                <input name="typeofcorp_effect" value="{{$common->typeofcorp_effect}}" maxlength="12" type="text" placeholder="" id="typeofcorp_effect" class="form-control  effective_date1"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch">
                                                                    <div class="col-md-3" style="text-align:left;">
                                                                        <h1>Federal / State</h1>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <h1>Income Tax </h1>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group {{ $errors->has('federal_id') ? ' has-error' : '' }}">
                                                                        <div class="col-md-2">
                                                                            <label class="control-label">Federal ID :</label>
                                                                            <input type="text" class="form-control txtOnly federal_id" id="federal_id" maxlength="9" name="federal_id" value="{{ $common->federal_id}}">
                                                                            @if ($errors->has('federal_id'))
                                                                                <span class="help-block">
															<strong>{{ $errors->first('federal_id') }}</strong>
															</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Type of Form To File :</label>
                                                                            <input type="text" class="form-control" id="type_form" name="type_form" value="{{$common->type_form}}" readonly>
                                                                            @if ($errors->has('type_form'))
                                                                                <span class="help-block">
															<strong>{{ $errors->first('type_form') }}</strong>
															</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label class="control-label">Due Date :</label>
                                                                            <input type="text" class="form-control" id="due_date" name="due_date" readonly value="{{$common->typeofcorp_effect_2}}">
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Extension Due Date :</label>
                                                                            <input type="text" class="form-control" id="extension_due_date" readonly name="extension_due_date" value="{{$common->typeofcorp_effect_2}}">
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-2">
                                                                            <label class="control-label">State ID :</label>
                                                                            <input type="text" class="form-control txtOnly" id="state_id" maxlength="9" name="state_id" value="{{$common->state_id}}">
                                                                            @if ($errors->has('state_id'))
                                                                                <span class="help-block">
															<strong>{{ $errors->first('state_id') }}</strong>
															</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Type of Form To File :</label>
                                                                            <input type="text" class="form-control" id="form_authority_1" name="form_authority_1" value="{{Auth::user()->form_authority_1}}" readonly>
                                                                            @if ($errors->has('type_form'))
                                                                                <span class="help-block">
															<strong>{{ $errors->first('type_form') }}</strong>
															</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label class="control-label">Due Date :</label>
                                                                            <input type="text" class="form-control" readonly id="due_datem" name="due_datem" value="{{$common->typeofcorp_effect_2}}">
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Extension Due Date :</label>
                                                                            <input type="text" class="form-control" readonly id="extension_due_date" name="extension_due_date" value="{{$common->typeofcorp_effect_2}}">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch">
                                                                    <div class="col-md-3" style="text-align:left;">
                                                                        <h1>Federal / State</h1>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <h1>Payroll Tax</h1>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group {{ $errors->has('type_form') ? ' has-error' : '' }}">

                                                                    <div class="form-group {{ $errors->has('fedral_state') ? ' has-error' : '' }}">

                                                                        <div class="col-md-12">

                                                                            <div class="fe_state_tab_main">
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-12">
                                                                                        <h3>Federal Withholding & FICA Tax</h3>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <label class="control-label-insu"> Name :</label>
                                                                                        <input type="text" class="form-control-insu" id="federal_name" name="federal_name" value="" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="{{$common->federal_frequency_due_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="{{$common->federal_payment_frequency_year}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="{{$common->federal_payment_frequency_month}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="{{$common->federal_payment_frequency_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="{{$common->payment_frequency_year}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="{{$common->payment_frequency_monthly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="{{$common->payment_frequency_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="{{$common->frequency_due_date_year}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="{{$common->frequency_due_date_monthly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="{{$common->frequency_due_date_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="{{$common->frequency_due_date_year_1}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="{{$common->frequency_due_date_monthly_1}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="{{$common->frequency_due_date_quaterly_1}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="{{$common->frequency_due_date_monthly_2}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="{{$common->frequency_due_date_quaterly_2}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="{{$common->federal_frequency_due_year}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="{{$common->federal_frequency_due_monthly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="{{$common->quaterly_monthly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="{{$common->quaterly_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="{{$common->payment_quaterly_quaterly}}" readonly="">
                                                                                        <input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="{{$common->payment_quaterly_monthly}}" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Department Authority :</label>
                                                                                        <input type="text" class="form-control-insu" id="federal_ga_dept" name="federal_ga_dept" value="" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Type of Tax :</label>
                                                                                        <input type="text" class="form-control-insu" id="federal_form_1" name="federal_form_1" value="" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Filing Frquency :</label>
                                                                                        <select type="text" class="form-control-insu" id="federal_frequency" name="federal_frequency">
                                                                                            <option value="Quaterly" @if($common->federal_frequency=='Quaterly') selected @endif>Quaterly</option>
                                                                                        <!--<option value="Monthly" @if($fill->filing_frequency=='Monthly') selected @endif>Monthly</option>-->
                                                                                            <option value="Yearly" @if($common->federal_frequency=='Yearly') selected @endif>Annually</option>
                                                                                        </select>
                                                                                        @if ($errors->has('federal_frequency'))
                                                                                            <span class="help-block">
																			<strong>{{ $errors->first('federal_frequency') }}</strong>
																			</span>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Due Date :</label>
                                                                                        <input readonly type="text" class="form-control-insu form-control" id="federal_frequency_due_date" name="federal_frequency_due_date" value="" readonly>
                                                                                        @if ($errors->has('federal_frequency_due_date'))
                                                                                            <span class="help-block">
																			<strong>{{ $errors->first('federal_frequency_due_date') }}</strong>
																			</span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-1">&nbsp;</div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Federal ID :</label>
                                                                                        <input type="text" class="form-control-insu" readonly id="number_1" name="number_1" value="{{$common->number_1}}">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Form No. :</label>
                                                                                        <select type="text" class="form-control-insu" id="form_number_1" name="form_number_1">
                                                                                            <option value="Form-941" @if($common->federal_frequency=='Quaterly') selected @endif>Form-941</option>
                                                                                            <option value="'Form-944" @if($common->federal_frequency=='Yearly') selected @endif>Form-944</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Frequency </label>
                                                                                        <select class="form-control-insu" id="federal_payment_frequency" name="federal_payment_frequency">
                                                                                            <option value="Quaterly" @if($common->federal_payment_frequency=='Quaterly') selected @endif>Quaterly</option>
                                                                                            <option value="Monthly" @if($common->federal_payment_frequency=='Monthly') selected @endif>Monthly</option>
                                                                                            <option value="Yearly" @if($common->federal_payment_frequency=='Yearly') selected @endif>Annually</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Due Date </label>
                                                                                        <input type="text" readonly class="form-control-insu form-control" name="federal_payment_frequency_date" value="<?php if ($common->federal_payment_frequency == 'Monthly') {
                                                                                            if (date('M-d-Y') <= $common->federal_payment_frequency_date) {
                                                                                                echo $common->federal_payment_frequency_date;
                                                                                            } else {
                                                                                                echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");
                                                                                            }
                                                                                        }?>" placeholder="" id="federal_payment_frequency_date">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="fe_state_tab_main">
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-12">
                                                                                        <h3>Federal Unemployment Tax (FUTA)</h3>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <label class="control-label-insu">Name :</label>
                                                                                        <input type="text" class="form-control-insu" id="" name="" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Department Authority :</label>
                                                                                        <input type="text" class="form-control-insu" id="GA Dept. of Rev." name="ga_dept" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Type of Tax :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_1" name="form_1" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Filing Frquency :</label>
                                                                                        <input type="text" class="form-control-insu" id="frequency" name="frequency" value="Annually" readonly>

                                                                                        @if ($errors->has('frequency'))
                                                                                            <span class="help-block">
																			<strong>{{ $errors->first('frequency') }}</strong>
																			</span>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Due Date :</label>
                                                                                        <input type="text" class="form-control-insu form-control" id="frequency_due_date" readonly name="frequency_due_date" value="<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>" readonly>
                                                                                        @if ($errors->has('frequency_due_date'))
                                                                                            <span class="help-block">
																			<strong>{{ $errors->first('frequency_due_date') }}</strong>
																			</span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-1">&nbsp;</div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Federal ID :</label>
                                                                                        <input type="text" class="form-control-insu" id="number_2" readonly name="number_2" value="{{$common->number_2}}">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Form No. :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_number_2" readonly name="form_number_2" value="Form-940">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Frequency </label>
                                                                                        <select type="text" class="form-control-insu" id="payment_frequency" name="payment_frequency">
                                                                                            <option value="Quaterly" @if($pay->filing_frequency1=='Quaterly') selected @endif>Quaterly</option>
                                                                                        <!--<option value="Monthly" @if($pay->filing_frequency1=='Monthly') selected @endif>Monthly</option>-->
                                                                                            <option value="Yearly" @if($pay->filing_frequency1=='Annually') selected @endif>Yearly</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Due Date </label>
                                                                                        <input type="text" class="form-control-insu form-control" name="payment_frequency_date" readonly value="<?php if ($common->payment_frequency == 'Yearly') {
                                                                                            echo "Jan" . '-"31".-' . date("Y", strtotime("1 year", strtotime(date("Y"))));
                                                                                        } else if ($common->payment_frequency == 'Quaterly') {
                                                                                            if (date('M-d-Y') <= $common->payment_frequency_date) {
                                                                                                echo $common->payment_frequency_date;
                                                                                            } else {
                                                                                                echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");
                                                                                            }
                                                                                        }?>" placeholder="" id="payment_frequency_date">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="fe_state_tab_main">
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-12">
                                                                                        <h3>State Withholding</h3>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <label class="control-label-insu">Name :</label>
                                                                                        <input type="text" class="form-control-insu" id="fedral_state_1" name="fedral_state_1" value="{{$common->fedral_state_1}}">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Department Authority :</label>
                                                                                        <input type="text" class="form-control-insu" id="ga_dept_1" name="ga_dept_1" value="GA Dept of Revenue" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Type of Tax :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_2" name="form_2" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Filing Frquency :</label>
                                                                                        <input type="text" class="form-control-insu" id="frequency_type_form_1" name="frequency_type_form_1" value="Quaterly" readonly>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Due Date :</label>
                                                                                        <input type="text" class="form-control-insu form-control" id="frequency_due_date_1" name="frequency_due_date_1" value="<?php  echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>" readonly="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-1">&nbsp;</div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">State Withholding No. :</label>
                                                                                        <input type="text" class="form-control-insu" id="number_3" name="number_3" value="{{$common->number_3}}">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Form No. :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_number_3" readonly name="form_number_3" value="Form-G-7">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Frequency </label>
                                                                                        <select type="text" class="form-control-insu" id="payment_frequency_1" name="payment_frequency_1">
                                                                                            <option value="Quaterly" @if($common->payment_frequency_1 =='Quaterly') selected @endif>Quaterly</option>
                                                                                            <option value="Monthly" @if($common->payment_frequency_1 =='Monthly') selected @endif>Monthly</option>
                                                                                        <!--<option value="Yearly"  @if(Auth::user()->payment_frequency_1=='Yearly') selected @endif>Yearly</option>-->
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Due Date </label>
                                                                                        <input type="text" class="form-control-insu form-control" readonly name="payment_frequency_date_1" value="<?php if ($common->payment_frequency_1 == 'Yearly') {
                                                                                            echo "Jan" . '-"31"-' . date("Y", strtotime("1 year", strtotime(date("Y"))));
                                                                                        } else if ($common->payment_frequency_1 == 'Monthly') {
                                                                                            if (date('M-d-Y') <= $common->frequency_due_date_monthly_2) {
                                                                                                echo $common->frequency_due_date_monthly_2;
                                                                                            } else {
                                                                                                echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");
                                                                                            }
                                                                                        } else if ($common->payment_frequency_1 == 'Quaterly') {
                                                                                            if (date('M-d-Y') <= $common->frequency_due_date_quaterly_2) {
                                                                                                echo $common->frequency_due_date_quaterly_2;
                                                                                            } else {
                                                                                                echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");
                                                                                            }
                                                                                        }?>" placeholder="" id="payment_frequency_date_1">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="fe_state_tab_main">
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-12">
                                                                                        <h3>State Unemployment (SUTA)</h3>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <label class="control-label-insu">Name :</label>
                                                                                        <input type="text" class="form-control-insu" id="fedral_state_2" name="fedral_state_2" value="{{$common->fedral_state_2}}">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">Department Authority :</label>
                                                                                        <input type="text" class="form-control-insu" id="ga_dept_labour" name="ga_dept_labour" value="GA Dept of Labor" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Type of Tax :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_3" name="form_3" readonly="">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Filing Frquency :</label>
                                                                                        <input type="text" class="form-control-insu" id="quaterly" name="quaterly" value="Quaterly" readonly>
                                                                                    <!--<option value="Quaterly" @if(Auth::user()->quaterly=='Quaterly') selected @endif>Quaterly</option>
																	<option value="Monthly" @if(Auth::user()->quaterly=='Monthly') selected @endif>Monthly</option>
																	</select>				-->
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Due Date :</label>
                                                                                        <input type="text" class="form-control-insu form-control" id="quaterly_due_date" name="quaterly_due_date" value="<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y"); ?>" placeholder="" readonly="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fe_state_tab">
                                                                                    <div class="col-md-1">&nbsp;</div>
                                                                                    <div class="col-md-4">
                                                                                        <label class="control-label-insu">State Unemployment No. :</label>
                                                                                        <input type="text" class="form-control-insu" id="number_4" name="number_4" value="{{$common->number_4}}">
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <label class="control-label-insu">Form No. :</label>
                                                                                        <input type="text" class="form-control-insu" id="form_number_4" readonly name="form_number_4" value="Form-DOL-4N">
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Frequency </label>

                                                                                        <input type="text" class="form-control-insu" id="payment_frequency_2" readonly name="payment_frequency_2" value='Quaterly'>
                                                                                    <!--<option value="Quaterly" @if(Auth::user()->payment_frequency_2=='Quaterly') selected @endif>Quaterly</option>
																	<option value="Monthly" @if(Auth::user()->payment_frequency_2=='Monthly') selected @endif>Monthly</option>
																	</select>-->
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <label class="control-label-insu">Payment Due Date </label>
                                                                                        <input type="text" class="form-control-insu form-control" name="payment_frequency_date_2" readonly value="<?php  echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>" placeholder="" id="payment_frequency_date_2">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane fade" id="tab6primary">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">UserName :</label>
                                                                        <div class="col-md-8">
                                                                            @if(isset($user->name))
                                                                                <input type="text" readonly class="form-control" id="" value="{{ $user->name }}" name="name">
                                                                            @else
                                                                                <input type="text" class="form-control" id="" value="" name="name" readonly>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Question 1 :</label>
                                                                        <div class="col-md-8">
                                                                            <select name="question1" id="question1" class="form-control">
                                                                                @if (isset($user->question1))
                                                                                    <option value="{{ Auth::user()->question1 }}">{{ $user->question1 }}</option>
                                                                                @else
                                                                                    <option value="">---Select---</option>
                                                                                    @endelse
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Question 2 :</label>
                                                                        <div class="col-md-8">
                                                                            <select name="question2" id="question2" class="form-control">
                                                                                @if (isset($user->question2))
                                                                                    <option value="{{ Auth::user()->question2 }}">{{ $user->question2 }}</option>
                                                                                @else
                                                                                    <option value="">---Select---</option>
                                                                                    @endelse
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Question 3 :</label>
                                                                        <div class="col-md-8">
                                                                            <select name="question3" id="question3" class="form-control">
                                                                                @if (isset($user->question3))
                                                                                    <option value="{{ Auth::user()->question3 }}">{{ $user->question3 }}</option>
                                                                                @else
                                                                                    <option value="">---Select---</option>
                                                                                    @endelse
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Reset Days:</label>
                                                                        <div class="col-md-4">
                                                                            <select name="resetdays" id="resetdays" class="form-control">
                                                                                <option value="">---Select---</option>
                                                                                @if (isset($user->resetdays))
                                                                                    <option value="30" @if($user->resetdays=='30') selected @endif>30</option>
                                                                                    <option value="45" @if($user->resetdays=='45') selected @endif>45</option>
                                                                                    <option value="90" @if($user->resetdays=='90') selected @endif>90</option>
                                                                                    <option value="120" @if($user->resetdays=='120') selected @endif>120</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-4"><input name="resetdate" value="{{$user->enddate}}" type="text" id="reset_date" class="form-control"/></div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Answer 1:</label>
                                                                        <div class="col-md-8">
                                                                            @if(isset($user->answer1))
                                                                                <input readonly name="answer1" value="{{ $user->answer1 }}" type="text" id="answer1" class="form-control"/>
                                                                            @else
                                                                                <input type="text" class="form-control" id="" value="" name="answer2" readonly>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Answer 2:</label>
                                                                        <div class="col-md-8">
                                                                            @if(isset($user->answer2))
                                                                                <input name="answer2" readonly value="{{ $user->answer2 }}" type="text" id="answer2" class="form-control"/>
                                                                            @else
                                                                                <input type="text" class="form-control" id="" value="" name="name" readonly>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Answer 3:</label>
                                                                        <div class="col-md-8">
                                                                            @if(isset($user->answer3))
                                                                                <input name="answer3" readonly value="{{ $user->answer3 }}" type="text" id="answer3"
                                                                                       class="form-control"/>
                                                                            @else
                                                                                <input type="text" class="form-control" id="" value="" name="answer3" readonly>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane fade" id="tab8primary">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch" style="text-align:left; padding-left:15px;">
                                                                    <h1>Business License</h1>
                                                                </div>
                                                            </div>

                                                            <br/>

                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group {{ $errors->has('business_license_jurisdiction') ? ' has-error' : '' }}" style="margin-bottom:5px;">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                                        <div class="col-md-3">
                                                                            <label class="control-label" style="font-size:15px;">Jurisdiction :</label>
                                                                            <select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">
                                                                                <option value="">Select</option>
                                                                                <option value="City" @if(Auth::user()->business_license_jurisdiction=='City') selected @endif>City</option>
                                                                                <option value="County" @if(Auth::user()->business_license_jurisdiction=='County') selected @endif>County</option>
                                                                            </select>
                                                                            @if ($errors->has('business_license_jurisdiction'))
                                                                                <span class="help-block">
																<strong>{{ $errors->first('business_license_jurisdiction') }}</strong>
																</span>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-3">
                                                                            <label id="city-change" class="control-label" style="display:none;">City :</label>
                                                                            <label id="county-change" class="control-label">County :</label>
                                                                            @if($common->business_license_jurisdiction=='County')
                                                                                <select type="text" class="form-control" id="business_license2" name="business_license2">
                                                                                    <option value="">Select</option>
                                                                                    <option value="{{ $common->business_license2}}" @if($common->business_license2) selected @endif>{{ $common->business_license2}}</option>
                                                                                    @foreach($taxstate as $v)
                                                                                        <option value="{{$v->county}}" @if($v->county==$common->business_license2) selected @endif>{{$v->county}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            @elseif($common->business_license_jurisdiction=='City')
                                                                                <input type="text" class="form-control" id="business_license3" name="business_license2" value="{{$common->business_license2}}">
                                                                            @else
                                                                                <select type="text" class="form-control" id="business_license2" name="business_license2" @if($common->business_license_jurisdiction=='City') style="display:none;" @endif>
                                                                                    <option value="">Select</option>
                                                                                    @foreach($taxstate as $v)
                                                                                        <option value="{{$v->county}}" @if($v->county==$common->business_license2) selected @endif>{{$v->county}}</option>
                                                                                    @endforeach
                                                                                </select>

                                                                            @endif
                                                                            <div id="business_license4"></div>
                                                                            <div id="business_license5"></div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label id="city-change1" class="control-label" style="display:none;">City # :</label>
                                                                            <label id="county-change1" class="control-label">County # :</label>
                                                                            <input name="business_license1" placeholder="" value="{{ $common->business_license1}}" type="text" id="business_license1" class="form-control"/>
                                                                            @if ($errors->has('business_license1'))
                                                                                <span class="help-block">
																	<strong>{{ $errors->first('business_license1') }}</strong>
																</span>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label id="county-change1" class="control-label">License # :</label>
                                                                            <input name="business_license3" placeholder="" value="{{$common->business_license3}}" type="text" id="business_license1" class="form-control"/>
                                                                            @if ($errors->has('business_license3'))
                                                                                <span class="help-block">
																	<strong>{{ $errors->first('business_license3') }}</strong>
																</span>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <label class="control-label">Expire Date :</label>
                                                                            <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="{{ $common->due_date2}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="col-md-6">
                                                                            <label class="control-label" style="font-size:15px;padding:0;">Note :</label>
                                                                            <input name="notes" placeholder="Note" value="{{$common->notes}}" type="text" id="" class="form-control">
                                                                        </div> <?php $id1 = $common->business_license_jurisdiction;?>
                                                                        <div class="col-md-2">
                                                                            <label></label>
                                                                            <a class="btn_new btn-view-license">License History</a>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <label></label>
                                                                            <a style="display:block;" data-toggle="modal" num="{{$id1}}" class="btn_new openBtn btn-view-license">View License</a>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            @foreach($upload as $up)
                                                                                @if($up->upload_name==$id1)
                                                                                    <style>.nn {
                                                                                            display: none !important
                                                                                        }</style>
                                                                                    <label></label>
                                                                                    <a style="display:block;" href="{{$up->website_link}}" target="_blank" class="btn_new btn-renew">Renew Now</a>
                                                                                @endif
                                                                            @endforeach
                                                                            <label></label>
                                                                            <a href="#" class="btn_new btn-renew nn">Renew Now</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch" style="text-align:left; padding-left:15px;">
                                                                    <h1>Professional License</h1>
                                                                </div>
                                                            </div>

                                                            <?php $i = 0; $pro = count($admin_professional);?>
                                                            @if($pro != NULL)
                                                                @foreach($admin_professional as $ak1)
                                                                    <?php $i; $i++;?>
                                                                    @if($ak1->profession==NULL)
                                                                    @else
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="field_wrapper">
                                                                                <div id="field0">
                                                                                    <div class="professional_tabs_main">
                                                                                        <div class="professional_tabs">
                                                                                            <div class="professional_tab professional_profession">
                                                                                                <label>Profession :</label>
                                                                                                <select type="text" class="form-control-insu" id="" name="profession[]">
                                                                                                    <option value="">Select</option>
                                                                                                    <option value="ERO" @if($ak1->profession=='ERO') selected @endif>ERO</option>
                                                                                                    <option value="CPA" @if($ak1->profession=='CPA') selected @endif>CPA</option>
                                                                                                    <option value="Mortgage Broker" @if($ak1->profession=='Mortgage Broker') selected @endif>Mortgage Broker</option>
                                                                                                    <option value="PTIN" @if($ak1->profession=='PTIN') selected @endif>PTIN</option>
                                                                                                    <option value="Insurance Agent" @if($ak1->profession=='Insurance Agent') selected @endif>Insurance Agent</option>
                                                                                                    <option value="Insurance Broker" @if($ak1->profession=='Insurance Broker') selected @endif>Insurance Broker</option>
                                                                                                    <option value="MLO" @if($ak1->profession=='MLO') selected @endif>MLO</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="professional_tab professional_state">
                                                                                                <label>State :</label>
                                                                                                <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                                                                    @if(empty($ak1->profession_state)) @else
                                                                                                        <option value="{{$ak1->profession_state}}">{{$ak1->profession_state}}</option> @endif
                                                                                                    <option value="AK">AK</option>
                                                                                                    <option value="AS">AS</option>
                                                                                                    <option value="AZ">AZ</option>
                                                                                                    <option value="AR">AR</option>
                                                                                                    <option value="CA">CA</option>
                                                                                                    <option value="CO">CO</option>
                                                                                                    <option value="CT">CT</option>
                                                                                                    <option value="DE">DE</option>
                                                                                                    <option value="DC">DC</option>
                                                                                                    <option value="FM">FM</option>
                                                                                                    <option value="FL">FL</option>
                                                                                                    <option value="GA">GA</option>
                                                                                                    <option value="GU">GU</option>
                                                                                                    <option value="HI">HI</option>
                                                                                                    <option value="ID">ID</option>
                                                                                                    <option value="IL">IL</option>
                                                                                                    <option value="IN">IN</option>
                                                                                                    <option value="IA">IA</option>
                                                                                                    <option value="KS">KS</option>
                                                                                                    <option value="KY">KY</option>
                                                                                                    <option value="LA">LA</option>
                                                                                                    <option value="ME">ME</option>
                                                                                                    <option value="MH">MH</option>
                                                                                                    <option value="MD">MD</option>
                                                                                                    <option value="MA">MA</option>
                                                                                                    <option value="MI">MI</option>
                                                                                                    <option value="MN">MN</option>
                                                                                                    <option value="MS">MS</option>
                                                                                                    <option value="MO">MO</option>
                                                                                                    <option value="MT">MT</option>
                                                                                                    <option value="NE">NE</option>
                                                                                                    <option value="NV">NV</option>
                                                                                                    <option value="NH">NH</option>
                                                                                                    <option value="NJ">NJ</option>
                                                                                                    <option value="NM">NM</option>
                                                                                                    <option value="NY">NY</option>
                                                                                                    <option value="NC">NC</option>
                                                                                                    <option value="ND">ND</option>
                                                                                                    <option value="MP">MP</option>
                                                                                                    <option value="OH">OH</option>
                                                                                                    <option value="OK">OK</option>
                                                                                                    <option value="OR">OR</option>
                                                                                                    <option value="PW">PW</option>
                                                                                                    <option value="PA">PA</option>
                                                                                                    <option value="PR">PR</option>
                                                                                                    <option value="RI">RI</option>
                                                                                                    <option value="SC">SC</option>
                                                                                                    <option value="SD">SD</option>
                                                                                                    <option value="TN">TN</option>
                                                                                                    <option value="TX">TX</option>
                                                                                                    <option value="UT">UT</option>
                                                                                                    <option value="VT">VT</option>
                                                                                                    <option value="VI">VI</option>
                                                                                                    <option value="VA">VA</option>
                                                                                                    <option value="WA">WA</option>
                                                                                                    <option value="WV">WV</option>
                                                                                                    <option value="WI">WI</option>
                                                                                                    <option value="WY">WY</option>
                                                                                                </select>
                                                                                            </div>

                                                                                            <div class="professional_tab professional_effective">
                                                                                                <label>Effective Date :</label>
                                                                                                <input name="profession_effective_date[]" placeholder="Effective Date" value="{{$ak1->profession_epr_date1}}" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                                                            </div>

                                                                                            <div class="professional_tab professional_license">
                                                                                                <label>License # :</label>
                                                                                                <input name="profession_license[]" placeholder="License" value="{{$ak1->profession_license}}" type="text" id="profession_license" class="form-control-insu">
                                                                                            </div>

                                                                                            <div class="professional_tab professional_expire">
                                                                                                <label>Expire Date :</label>
                                                                                                @foreach($upload as $up)
                                                                                                    @if($up->upload_name==$ak1->pro_id)
                                                                                                        <label></label>
                                                                                                        <input name="profession_epr_date[]" placeholder="Expire Date" value="{{$up->expired_date}}" type="text" id="profession_epr_date" class="form-control-insu" readonly>
                                                                                                        <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                                                display: none
                                                                                                            }</style>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                                <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}" readonly>
                                                                                            </div>
                                                                                            <div class="professional_add">
                                                                                                @if($i==2)
                                                                                                @else
                                                                                                    <a href="#myModalm99_{{$ak1->id}}" id="remove_button_pro" role="button" class="btn_new btn-remove" data-toggle="modal"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="professional_tab professional_note">
                                                                                                <label>Note :</label>
                                                                                                <input name="profession_note[]" placeholder="Note" value="{{$ak1->profession_note}}" type="text" id="profession_note" class="form-control-insu">
                                                                                                <input name="profession_id[]" placeholder="Expire Date" value="{{$ak1->id}}" type="hidden" class="form-control-insu">
                                                                                            </div>
                                                                                            <div class="professional_tab professional_effective">
                                                                                                <label></label>
                                                                                                <a data-toggle="modal-history" num="{{$ak1->profession.'-'.$ak1->profession_state.'-'.$ak1->profession_license}}" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
                                                                                            </div>
                                                                                            <div class="professional_tab professional_license">
                                                                                                @foreach($upload as $up)
                                                                                                    @if($up->upload_name==$ak1->pro_id)
                                                                                                        <label></label>
                                                                                                        <a data-toggle="modal" num="{{$ak1->profession.'-'.$ak1->profession_state}}" class="btn_new btn-view-license openBtn Pro-btn">View License</a>
                                                                                                        <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                                                display: none
                                                                                                            }</style>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                                <label class="btn-success-{{$ak1->pro_id}}"></label>
                                                                                                <a class="btn_new btn-view-license Pro-btn  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">File Not Upload</a>
                                                                                            </div>
                                                                                            <div class="professional_tab professional_expire">
                                                                                                @foreach($upload as $up)
                                                                                                    @if($up->upload_name==$ak1->pro_id)
                                                                                                        <label></label>
                                                                                                        <a href="{{$up->website_link}}" class="btn_new btn-renew" target='_blank'>Renew Now</a>
                                                                                                        <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}} {
                                                                                                                display: none
                                                                                                            }</style>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                                <label class="btn-success-{{$ak1->pro_id}}"></label>
                                                                                                <a href="javascript:void(0);" class="btn_new btn-renew btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">Renew Now</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="field_wrapper">
                                                                        <div id="field0">
                                                                            <div class="professional_tabs_main">
                                                                                <div class="professional_tabs">
                                                                                    <div class="professional_tab professional_profession">
                                                                                        <label>Profession :</label>
                                                                                        <select type="text" class="form-control-insu" id="" name="profession[]">
                                                                                            <option value="">Select</option>
                                                                                            <option value="ERO">ERO</option>
                                                                                            <option value="CPA">CPA</option>
                                                                                            <option value="Mortgage Broker">Mortgage Broker</option>
                                                                                            <option value="PTIN">PTIN</option>
                                                                                            <option value="Insurance Agent">Insurance Agent</option>
                                                                                            <option value="Insurance Broker">Insurance Broker</option>
                                                                                            <option value="MLO">MLO</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="professional_tab professional_state">
                                                                                        <label>State :</label>
                                                                                        <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                                                            <option value="AK">AK</option>
                                                                                            <option value="AS">AS</option>
                                                                                            <option value="AZ">AZ</option>
                                                                                            <option value="AR">AR</option>
                                                                                            <option value="CA">CA</option>
                                                                                            <option value="CO">CO</option>
                                                                                            <option value="CT">CT</option>
                                                                                            <option value="DE">DE</option>
                                                                                            <option value="DC">DC</option>
                                                                                            <option value="FM">FM</option>
                                                                                            <option value="FL">FL</option>
                                                                                            <option value="GA">GA</option>
                                                                                            <option value="GU">GU</option>
                                                                                            <option value="HI">HI</option>
                                                                                            <option value="ID">ID</option>
                                                                                            <option value="IL">IL</option>
                                                                                            <option value="IN">IN</option>
                                                                                            <option value="IA">IA</option>
                                                                                            <option value="KS">KS</option>
                                                                                            <option value="KY">KY</option>
                                                                                            <option value="LA">LA</option>
                                                                                            <option value="ME">ME</option>
                                                                                            <option value="MH">MH</option>
                                                                                            <option value="MD">MD</option>
                                                                                            <option value="MA">MA</option>
                                                                                            <option value="MI">MI</option>
                                                                                            <option value="MN">MN</option>
                                                                                            <option value="MS">MS</option>
                                                                                            <option value="MO">MO</option>
                                                                                            <option value="MT">MT</option>
                                                                                            <option value="NE">NE</option>
                                                                                            <option value="NV">NV</option>
                                                                                            <option value="NH">NH</option>
                                                                                            <option value="NJ">NJ</option>
                                                                                            <option value="NM">NM</option>
                                                                                            <option value="NY">NY</option>
                                                                                            <option value="NC">NC</option>
                                                                                            <option value="ND">ND</option>
                                                                                            <option value="MP">MP</option>
                                                                                            <option value="OH">OH</option>
                                                                                            <option value="OK">OK</option>
                                                                                            <option value="OR">OR</option>
                                                                                            <option value="PW">PW</option>
                                                                                            <option value="PA">PA</option>
                                                                                            <option value="PR">PR</option>
                                                                                            <option value="RI">RI</option>
                                                                                            <option value="SC">SC</option>
                                                                                            <option value="SD">SD</option>
                                                                                            <option value="TN">TN</option>
                                                                                            <option value="TX">TX</option>
                                                                                            <option value="UT">UT</option>
                                                                                            <option value="VT">VT</option>
                                                                                            <option value="VI">VI</option>
                                                                                            <option value="VA">VA</option>
                                                                                            <option value="WA">WA</option>
                                                                                            <option value="WV">WV</option>
                                                                                            <option value="WI">WI</option>
                                                                                            <option value="WY">WY</option>
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="professional_tab professional_effective">
                                                                                        <label>Effective Date :</label>
                                                                                        <input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                                                    </div>

                                                                                    <div class="professional_tab professional_license">
                                                                                        <label>License # :</label>
                                                                                        <input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu">
                                                                                    </div>

                                                                                    <div class="professional_tab professional_expire">
                                                                                        <label>Expire Date :</label>

                                                                                        <label></label>
                                                                                        <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>


                                                                                    </div>

                                                                                    <div class="professional_add">

                                                                                    </div>

                                                                                    <div class="professional_tab professional_note">
                                                                                        <label>Note :</label>
                                                                                        <input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu">
                                                                                        <input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" class="form-control-insu">
                                                                                    </div>

                                                                                    <div class="professional_tab professional_effective">
                                                                                        <label></label>
                                                                                        <a data-toggle="modal-history" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
                                                                                    </div>
                                                                                    <div class="professional_tab professional_license">

                                                                                        <label class="btn-success"></label>
                                                                                        <a class="btn_new btn-view-license Pro-btn">File Not Upload</a>
                                                                                    </div>
                                                                                    <div class="professional_tab professional_expire">


                                                                                        <label class="btn-success"></label>
                                                                                        <a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            @endif

                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="field_wrapper">
                                                                    <div id="field0">
                                                                        <div class="professional_tabs_main" id="professional_tabs_main">
                                                                            <div class="professional_tabs" style="background:transparent;border: transparent;">

                                                                                <div class="professional_add">
                                                                                    <a href="javascript:void(0);" id="add_button_pro" class="btn_new btn-add"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="col-md-7 col-md-offset-3">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <button type="submit" class="btn_new_save">Save</button>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <a href="{{url('/fac-Bhavesh-0554/clientsetup')}}" class="btn_new_cancel">Cancel</a>
                                                                        </div>
                                                                        <div class="col-md-3">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane fade" id="tab7primary">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <!-- Nav tabs -->
                                                                    <div class="card">
                                                                        <ul class="nav nav-tabs" role="tablist">

                                                                            <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"> <span>For FSC Only</span></a></li>
                                                                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span>For Client Only</span></a></li>
                                                                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><span>For Everybody</span></a></li>

                                                                        </ul>

                                                                        <!-- Tab panes -->
                                                                        <div class="tab-content">

                                                                            <div role="tabpanel" class="tab-pane active" id="profile">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                                                    <div class="input_fields_wrap">
                                                                                        <?php $l = 1; $notecon2 = count($fsc);?>
                                                                                        @if($notecon2!=NULL)
                                                                                            @foreach($fsc as $notes)
                                                                                                <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                                <input name="usid[]" value="{{$notes->admin_id}}" type="hidden" placeholder="" id="usid" class=" form-control">
                                                                                                <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Subject <?php echo $l; ?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminsubject[]" value="{{$notes->subject}}" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Note <?php echo $l;?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                    </div>
                                                                                                    @if($l==1)
                                                                                                        <button class="btn btn-success" type="button" onclick="education_field();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                                                    @else
                                                                                                        <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                                                    @endif
                                                                                                </div>
                                                                                                <?php  $l++;?>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class=" form-control">
                                                                                            <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                            <input name="notetype[]" value="fsc" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Subject 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminsubject[]" value="" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Note 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                </div>
                                                                                                <div class="">

                                                                                                </div>
                                                                                                @if($l==1)

                                                                                                @else
                                                                                                    <div class="col-md-1">
                                                                                                    </div>
                                                                                                @endif
                                                                                            </div>

                                                                                        @endif
                                                                                    </div>
                                                                                    <div id="education_field"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                                                    <div class="input_fields_wrap">
                                                                                        <?php $l = 1; $notecon3 = count($client);?>
                                                                                        @if($notecon3!=NULL)
                                                                                            @foreach($client as $notes)

                                                                                                <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                                <input name="usid[]" value="{{$notes->admin_id}}" type="hidden" placeholder="" id="usid" class=" form-control">
                                                                                                <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Subject <?php echo $l;?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminsubject[]" value="{{$notes->subject}}" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Note <?php echo $l;?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                    </div>
                                                                                                    @if($l==1)

                                                                                                    @else

                                                                                                        <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>

                                                                                                    @endif
                                                                                                </div>
                                                                                                <?php  $l++;?>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <input name="usid[]" value="{{$common->cid}}" type="hidden" placeholder="Last Name" id="usid" class=" form-control">
                                                                                            <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                            <input name="notetype[]" value="client" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Subject 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminsubject[]" value="" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Note 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                </div>
                                                                                                <div class="col-md-1">

                                                                                                </div>
                                                                                                @if($l==1)

                                                                                                @else
                                                                                                    <div class="col-md-1">

                                                                                                    </div>
                                                                                                @endif
                                                                                            </div>

                                                                                        @endif
                                                                                    </div>

                                                                                    <div id="education_field1"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                                                                    <div class="input_fields_wrap">
                                                                                        <?php $l = 1; $notecon = count($every);?>
                                                                                        @if($notecon!=NULL)
                                                                                            @foreach($every as $notes)

                                                                                                <input name="noteid[]" value="{{$notes->id}}" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                                <input name="usid[]" value="" type="hidden" placeholder="" id="usid" class=" form-control">
                                                                                                <input name="notetype[]" value="{{$notes->type}}" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Subject <?php echo $l;?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminsubject[]" value="{{$notes->subject}}" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label col-md-3">Note <?php echo $l;?>:</label>
                                                                                                    <div class="col-md-6">
                                                                                                        <input name="adminnotes[]" value="{{$notes->notes}}" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                    </div>
                                                                                                    @if($l==1)

                                                                                                    @else


                                                                                                        <a href="#myModalnote_{{$notes->id}}" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>

                                                                                                    @endif
                                                                                                </div>
                                                                                                <?php $l++;?>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class=" form-control">
                                                                                            <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=" form-control">
                                                                                            <input name="notetype[]" value="Everybody" type="hidden" placeholder="Last Name" id="notetype" class=" form-control">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Subject 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminsubject[]" value="" type="text" placeholder="Create Subject" id="adminsubject" class=" form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label col-md-3">Note 1 :</label>
                                                                                                <div class="col-md-6">
                                                                                                    <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class=" form-control">
                                                                                                </div>
                                                                                                <div class="col-md-1">

                                                                                                </div>
                                                                                                @if($l==1)

                                                                                                @else
                                                                                                    <div class="col-md-1">

                                                                                                    </div>
                                                                                                @endif
                                                                                            </div>

                                                                                        @endif
                                                                                    </div>

                                                                                    <div id="education_field2"></div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>


                <script>
                    $("#federal_id").mask("99-9999999");
                    $("#contact_number11").mask("a999999");
                    $("#business_no").mask("(999) 999-9999");
                    $(".ext").mask("999");
                    $("#business_fax").mask("(999) 999-9999");
                    $(".business_fax1").mask("(999) 999-9999");
                    $(".residence_fax").mask("(999) 999-9999");
                    $("#mobile_no").mask("(999) 999-9999");
                    $(".cell").mask("(999) 999-9999");
                    $(".telephone").mask("(999) 999-9999");
                    $(".usapfax").mask("(999) 999-9999");
                    $("#zip").mask("99999");
                    $("#mailing_zip").mask("99999");
                    $("#bussiness_zip").mask("99999");
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $(".effective_date2").datepicker({
                            autoclose: true,
                            format: "mm/dd/yyyy",
                            //endDate: "today"
                        });

                        $(".effective_date1").datepicker({
                            autoclose: true,
                            format: "mm/dd/yyyy",
                            //endDate: "today"
                        });
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button'); //Add button selector
                        var wrapper = $('#professional_tabs_main'); //Input field wrapper
                        var fieldHTML = '<div class="professional_tabs professional_tabs"><div class="professional_tab professional_profession"><label>Profession :</label><select type="text" class=" form-control-insu" id="" name="profession[]"><option value="">Select</option><option value="ERO">ERO</option><option value="CPA">CPA</option><option value="Mortgage Broker">Mortgage Broker</option><option value="Mortgage Broker">Mortgage Broker</option><option value="PTIN">PTIN</option><option value="Insurance Agent">Insurance Agent</option><option value="Insurance Broker">Insurance Broker</option><option value="MLO">MLO</option></select></div><div class="professional_tab professional_state"><label>State :</label><select name="profession_state[]" id="profession_state" class="form-control-insu"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="professional_tab professional_effective"><label>Effective Date :</label><input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1"></div><div class="professional_tab professional_license"><label>License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu"></div><div class="professional_tab professional_expire"><label>Expire Date :</label><input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu"></div><div class="professional_add"><a href="javascript:void(0);" id="remove_button_pro" class="btn_new btn-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div><div class="professional_tab professional_note"><label>Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu"><input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" id="" class="form-control-insu"></div><div class="professional_tab professional_effective"><label></label><a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a></div><div class="professional_tab professional_license"><label></label><a data-toggle="modal" num="1" class="btn_new btn-view-license">View License</a></div><div class="professional_tab professional_expire"><label></label><a href="javascript:void(0);" class="btn_new btn-renew">Renew Now</a></div></div>'; //New input field html
                        var x = 1; //Initial field counter is 1
                        $(addButton).click(function () { //Once add button is clicked
                            if (x < maxField) { //Check maximum number of input fields
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); // Add field html
                            }
                        });
                        $(wrapper).on('click', '#add_button_pro', function (e) { //Once remove button is clicked
                            e.preventDefault();
                            $(wrapper).append(fieldHTML); //Remove field html
                            x++;
                            $(".effective_date1").datepicker({
                                autoclose: true,
                                format: "mm/dd/yyyy",
                                endDate: "today"
                            });
                        });
                        $(wrapper).on('click', '#remove_button_pro', function (e) { //Once remove button is clicked
                            e.preventDefault();
                            $(this).parent().parent('.professional_tabs').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>

                <script>
                    var $select = $(".agent_position");
                    $select.on("change", function () {
                        var selected = [];
                        $.each($select, function (index, select) {
                            if (select.value !== "") {
                                selected.push(select.value);
                            }
                            // alert(select.value);
                        });
                        $("option").prop("hidden", false);
                        for (var index in selected) {
                            $('option[value="' + selected[index] + '"]').prop("hidden", true);

                        }
                    });

                    $(document).ready(function () {
                        var maxGroup = 120;
                        count = 0;
                        $(".btn-success").click(function () {
                            count += 1; // alert( count);
                            var aa = $('body').find('.input_fields_wrap_shareholder').length;
                            if (count > aa) { //alert();
                                if ($('body').find('.input_fields_wrap_shareholder').length < maxGroup) {
                                    var fieldHTML = '<div class="input_fields_wrap_shareholder" style="display:block">' + $(".input_fields_wrap_1").html() + '</div>';
                                    $('body').find('.input_fields_wrap_shareholder:last').after(fieldHTML);
                                    var $select = $(".agent_position");
                                    $select.on("change", function () {
                                        var selected = [];
                                        $.each($select, function (index, select) {
                                            if (select.value !== "") {
                                                var dd = selected.push(select.value); //alert(dd);
                                                if (dd == '2' && dd == '3' && dd == '4') {

                                                }
                                            }
                                        });
                                        $("option").prop("hidden", false);
                                        for (var index in selected) {
                                            $('option[value="' + selected[index] + '"]').prop("hidden", true);
                                        }
                                    });
                                } else {
                                    alert('Maximum ' + maxGroup + ' Persons are allowed.');
                                }
                            } else {
                                $('.input_fields_wrap_shareholder').css('display', 'block');

                            }
                        });

                        //remove fields group
                        $("body").on("click", ".remove", function () {
                            $(this).parents(".input_fields_wrap_shareholder").remove();
                            //var dd =  $("#agent_position").val(); alert(dd);
                            // alert($('select option:hidden').val();

                        });
                    });
                </script>


                <script>
                    function showDiv(elem) {
                        if (elem.value == 'Federal') {
                            document.getElementById('hidden_div').style.display = "none";
                        } else {
                            document.getElementById('hidden_div').style.display = "block";
                        }
                    }
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '.category', function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('getRequest')!!}?id=' + id, function (data) {
                                $('#business_cat_id').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                                })

                            });

                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '.category', function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('getcat')!!}?id=' + id, function (data) {
                                $('#user_type').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#user_type').val(subcatobj.bussiness_name);
                                })

                            });

                        });
                    });
                </script>

                <script type="text/javascript">
                    function checkemail() {
                        var client_id = document.getElementById("fileno").value;
                        //alert(client_id);
                        if (client_id) {
                            $.ajax({
                                type: 'get',
                                url: '{!!URL::to('/getclick')!!}',
                                data: {
                                    client_id: client_id,
                                },
                                success: function (response) {
                                    $('#email_status').html(response);
                                    if (response == "OK") {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            });
                        } else {
                            $('#email_status').html("");
                            return false;
                        }
                    }

                    function checkall() {
                        var emailhtml = document.getElementById("email_status").innerHTML;
                        if ((emailhtml) == "OK") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                </script>
                <script>
                    $(document).ready(function () {
                        $("#mailing_zip").keyup(function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('/getzip')!!}?zip=' + id, function (data) {
                                $('#mailing_state').empty();
                                $('#mailing_city').empty();//$('#countryId').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#city').removeAttr("disabled");
                                    $('#stateId').removeAttr("disabled");
                                    $('#mailing_city').val(subcatobj.city);
                                    $('#mailing_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                                    //$('#countryId').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
                                })
                            });
                        });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        $("#bussiness_zip").keyup(function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('/getzip')!!}?zip=' + id, function (data) {
                                $('#business_state').empty();
                                $('business_city').empty();
                                $('#business_country').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#city').removeAttr("disabled");
                                    $('#stateId').removeAttr("disabled");
                                    $('#business_city').val(subcatobj.city);
                                    $('#business_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                                    $('#business_country').append('<option value="' + subcatobj.country + '">' + subcatobj.country + '</option>');
                                })
                            });
                        });
                    });
                </script>

                <script>
                    $(document).on("change", ".numeric1", function () {
                        var sum = 0;
                        $(".numeric1").each(function () {
                            sum += +$(this).val().replace("%", "");
                            var num = parseFloat($(this).val());
                            $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                            //$(".numeric2").val(num.toFixed(2));
                        });
                        if (sum > 100) {
                            $(".total").val(sum.toFixed(2) + "%");
                            $('.btn-primary1').addClass("disabled");
                        } else if (sum < 100) {
                            $(".total").val(sum.toFixed(2) + "%");
                            $('.btn-primary1').addClass("disabled");
                        } else if (sum = 100) {
                            $(".total").val(sum.toFixed(2) + "%");
                            $('.btn-primary1').removeClass("disabled");
                        } else {
                            $(".total").val(sum.toFixed(2) + "%");
                            $('.btn-primary1').removeClass("disabled");
                        }
                    });
                </script>


                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#type_form3', function () {
                            var selectedCountry = $("#fedral_state option:selected").val(); //alert(selectedCountry);
                            var id = $(this).val();
                            $.get('{!!URL::to('getcount')!!}?id=' + id + '&state=' + selectedCountry, function (data) {
                                $('#business_license').empty();
                                $('#business_license').append('<option value="">Select</option>');
                                $.each(data, function (index, subcatobj) {
                                    $('#business_license').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                                })
                            });
                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#business_license', function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                                $('#business_license1').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#business_license1').val(subcatobj.countycode);
                                })
                            });
                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#typeofservice', function () {
                            //console.log('htm');
                            var id = $(this).val();
                            $.get('{!!URL::to('/faderal')!!}?id=' + id, function (data) {
                                $('#typeofcorp').empty();
                                $('#typeofcorp1').empty();
                                $('#due_date').empty();
                                $('#type_form').empty();
                                $('#type_formm').empty();
                                $('#due_date1').empty();
                                $('#type_form1').empty();
                                $('#fedral_state').empty();
                                $('#typeofcorp_effect_2').empty();
                                $('#frequency_due_date').empty();
                                $('#quaterly_due_date').empty();
                                $('#frequency_due_date_1').empty();
                                $('#form_authority_1').empty();
                                $('#formauthority').empty();
                                $('#form_1').empty();
                                $('#form_2').empty();
                                $('#form_3').empty();
                                //$('#type_form').append('<option value="">Select</option>');
                                //$('#type_form1').append('<option value="">Select</option>');
                                //$('#fedral_state').append('<option value="">Select</option>');
                                $.each(data, function (index, subcatobj) {
                                    $('#typeofcorp').val(subcatobj.authority_name);
                                    $('#typeofcorp1').append('<option value="' + subcatobj.formname + '">' + subcatobj.formname + '</option>');
                                    $('#type_form').val(subcatobj.formname);
                                    $('#type_formm').val(subcatobj.formname);
                                    // $('#type_form').val(subcatobj.telephone);
                                    $('#due_date').val(subcatobj.due_date);
                                    $('#due_datem').val(subcatobj.due_date);
                                    $('#due_date1').val(subcatobj.due_date);
                                    $('#frequency_due_date_1').val(subcatobj.due_date);
                                    $('#quaterly_due_date').val(subcatobj.due_date);
                                    $('#frequency_due_date').val(subcatobj.due_date);
                                    $('#form_authority').val(subcatobj.zip);
                                    $('#formauthority').val(subcatobj.zip);
                                    $('#form_authority_1').val(subcatobj.zip);
                                    $('#typeofcorp_effect_2').val(subcatobj.due_date);
                                    $('#form_1').val(subcatobj.typeofform);
                                    $('#form_2').val(subcatobj.typeofform);
                                    $('#form_3').val(subcatobj.typeofform);
                                    $('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                                    $('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                                })

                            });

                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#typeofcorp1', function () {
                            //console.log('htm');
                            var id = $(this).val(); //alert(id);
                            $.get('{!!URL::to('/getforms')!!}?id=' + id, function (data) {
                                $('#due_date_1').empty();
                                $('#type_form').empty();
                                $('#due_date_2').empty();
                                $('#type_form1').empty();
                                $('#fedral_state').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#type_form').append('<option value="' + subcatobj.telephone + '">' + subcatobj.telephone + '</option>');
                                    $('#typeofcorp').val(subcatobj.authority_name);
                                    $('#due_date_1').val(subcatobj.address);
                                    $('#due_date_2').val(subcatobj.address);
                                    $('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                                    $('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                                })
                            });
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#type_form3").change(function () {
                            var selectedText = $(this).find("option:selected").text();
                            var selectedValue = $(this).val();
                            if (selectedValue == 'County') {
                                $("#county-change").show();
                                $("#business_license").show();
                                $("#business_license2").hide();
                                $("#county-change1").show();
                                $("#city-change").hide();
                                $("#city-change1").hide();
                            } else if (selectedValue == 'City') {
                                $("#county-change").hide();
                                $("#county-change1").hide();
                                $("#business_license").hide();
                                $("#business_license2").show();
                                $("#city-change").show();
                                $("#city-change1").show();
                            } else {
                                $("#county-change").show();
                                $("#county-change1").show();
                            }
                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(document).on('change', '.state_of_formation', function () {
                            var id = $(this).val();
                            $.get('{!!URL::to('getcontrol')!!}?id=' + id, function (data) {
                                $('#contact_number1').empty();
                                $.each(data, function (index, subcatobj) {
                                    $('#contact_number1').val(subcatobj.controlname);
                                })
                            });
                        });
                    });
                </script>

                <div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">View License</h4>
                            </div>
                            <div class="modal-body">
                                <div id="viewmodel"></div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

                <div id="myModal2" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">View License</h4>
                            </div>
                            <div class="modal-body">
                                <h2>File Not Uploaded</h2>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function () {
                        $('.openBtn').on('click', function () {
                            //console.log('htm');
                            var num = $(this).attr("num"); //alert(num);
                            //alert(num);
                            $.get('{!!URL::to('getimagess')!!}?id=' + num, function (data) {// alert();
                                if (data == "") {
                                    $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                                } else {
                                    $.each(data, function (index, subcatobj) { //alert();
                                        if (num === subcatobj.upload_name) {
                                            $('#myModal').modal({show: true});
                                            $('#viewmodel').html('<embed src="http://financialservicecenter.net/public/clientupload/' + subcatobj.upload + '" width="100%" height="300px">');
                                        } else {
//	alert();
                                        }
                                    });
                                }
                            });
                        });
                    });
                </script>

                @foreach($admin_professional as $ak1)
                    @if($ak1->profession==NULL)
                    @else

                        <div id="myModalm99_{{$ak1->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to this record</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('clientd.clientdelete',$ak1->id) }}" class="btn btn-danger">Delete</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif
                @endforeach

                @foreach($shareholder as $ak)
                    @if($ak->agent_fname1==NULL)
                    @else

                        <div id="myModalk_{{$ak->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to this record</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('admind2.admindelete2',$ak->id) }}" class="btn btn-danger">Delete</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif
                @endforeach
                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#location_county', function () {         //alert(selectedCountry);
                            var id = $(this).val();// alert(id);
                            //	alert(id);
                            $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                                $('#physical_county_no').empty();

                                $.each(data, function (index, subcatobj) {
                                    $('#physical_county_no').val(subcatobj.countycode);

                                })

                            });

                        });
                    });
                </script>

                <script>
                    function FillBilling(f) { //alert();
                        var vv = $("#agent_fname1").val();
                        var vv1 = $("#agent_mname1").val();
                        var vv2 = $("#agent_lname1").val();
                        var vv3 = $("#agent_position11").val();
                        var vv4 = $("#agent_per11").val();
                        var vv5 = $("#effective_date").val(); //alert(vv4)
                        var k = $("#agent1").val();
                        var k1 = $("#agent2").val();
                        var k2 = $("#agent3").val();
                        var k3 = $("#agent4").val();
                        var k4 = $("#agent5").val();
                        var k5 = $("#agent6").val();
                        //alert(vv);
                        if (f.billingtoo.checked == true) { //alert();
                            f.agent_fname2.value = f.agent_fname.value;
                            f.agent_mname2.value = f.agent_mname.value;
                            f.agent_lname2.value = f.agent_lname.value;
                            $('.btn-primary1').attr('disabled', 'disabled');
                            if (vv3 == 'Agent' || k3 == 'Agent') {
                                $("#agent_fname1").val(k);
                                $("#agent_mname1").val(k1);
                                $("#agent_lname1").val(k2);
                                $("#agent_position11").val(k3);
                                $("#agent_per11").val(k4);
                                $("#effective_date").val(k5);
                                $("#agent_position11").append("<option selected value='Agent'>Agent</option>");
                                $('#input_fields_wrap_2').css('background', '#438bd6').css('padding', '10px 0 1px 0');
                            } else {
                                // $("#agent_position").empty();
                                $("#agent_position").find('option[value="Agent"]:first').prop('selected', true);
                                $("#agent_position").find('option[value="Agent"]').prop('hidden', true);
                                //$("#agent_position").append("<option  @if(empty($agent->agent_position))  @else disabled  @endif value='Agent'>Agent</option><option value='CEO' @if(empty($ceo->agent_position)) @else  disabled @endif  @if(empty($sec->agent_position)) @else  disabled @endif>CEO</option><option value='CFO' @if(empty($cfo->agent_position)) @else   disabled @endif  @if(empty($sec->agent_position)) @else  disabled @endif>CFO</option><option value='Secretary' @if(empty($sece->agent_position)) @else  disabled @endif  @if(empty($sec->agent_position)) @else  disabled @endif>Secretary</option><option value='Sec' @if(empty($sec->agent_position)) @else  disabled @endif @if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))) @else disabled @endif>CEO / CFO / Sec.</option>");
                                $('.input_fields_wrap_1').css('background', '#438bd6').css('padding', '10px 0 1px 0');
                            }
                        } else {
                            f.agent_fname2.value = '';
                            f.agent_mname2.value = '';
                            f.agent_lname2.value = '';
                            if (vv3 == 'Agent') {
                                $("#agent1").val(vv);
                                $("#agent2").val(vv1);
                                $("#agent3").val(vv2);
                                $("#agent4").val(vv3);
                                $("#agent5").val(vv4);
                                $("#agent6").val(vv5);
                                $("#agent_position11").empty();
                                $("#agent_fname1").val('');
                                $("#agent_per11").val('');
                                $("#agent_mname1").val('');
                                $("#agent_lname1").val('');
                                $("#effective_date").val('');
                                $('#input_fields_wrap_2').css('background', 'transparent').css('padding', '0');
                            }
                            $("#agent_position").find('option[value="Agent"]:first').prop('selected', false);
                            $("#agent_position").find('option[value="Agent"]').prop('hidden', false);
                            //$("#agent_position").empty();
                            //  $("#agent_position").append("<option value=''>Position</option><option value='Agent'  @if(empty($agent->agent_position))  @else disabled   @endif>Agent</option><option value='CEO' @if(empty($sec->agent_position))  @else disabled @endif @if(empty($ceo->agent_position))  @else disabled   @endif >CEO</option><option value='CFO' @if(empty($sec->agent_position))  @else disabled @endif @if(empty($cfo->agent_position))  @else disabled   @endif>CFO</option><option value='Secretary' @if(empty($secetary->agent_position))  @else disabled   @endif @if(empty($sec->agent_position))  @else disabled @endif>Secretary</option><option @if(empty($sec->agent_position))  @else disabled @endif value='Sec'   @if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))) @else disabled @endif>CEO / CFO / Sec.</option>");
                            $('.input_fields_wrap_1').css('background', 'transparent').css('padding', '0');
                        }
                    }
                </script>

                <script>


                    function FillBilling1(f) {
                        if (f.billingtoo1.checked == true) { //alert();
                            f.business_store_name.value = f.business_name.value;
                            f.business_address.value = f.address.value;
                            f.business_address_1.value = f.address1.value;
                            f.business_city.value = f.city.value;
                            f.business_state.value = f.stateId.value;
                            f.bussiness_zip.value = f.zip.value;
                            f.business_country.value = f.countryId.value;


                        } else {
                            f.business_store_name.value = '';
                            f.business_address.value = '';
                            f.business_address_1.value = '';
                            f.business_city.value = '';
                            f.business_state.value = '';
                            f.bussiness_zip.value = '';
                            f.business_state.value = '';
                            f.business_country.value = '';
                        }
                    }
                </script>

                <script>
                    var room1 = 0;
                    var coun = <?php echo $notecon;?>;
                    var z = room1 + coun;

                    function education_field_note() {
                        room1++;
                        z++;
                        var objTo = document.getElementById('input_fields_wrap_notes')
                        var divtest = document.createElement("div");
                        divtest.setAttribute("class", "form-group removeclass" + z);
                        divtest.innerHTML = '<label class="control-label col-md-3">Note ' + z + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                        var rdiv = 'removeclass' + z;
                        var rdiv1 = 'Schoolname' + z;
                        objTo.appendChild(divtest)
                    }

                    function remove_education_fields(rid) {
                        $('.removeclass' + rid).remove();
                        room1--;
                    }
                </script>


                <script>
                    $(document).ready(function () {
                        $('#patient_type_2').change(function () {
                            if ($(this).val() == "No") {
                                $(".ccd").hide();
                                $('.ccd:first').show();
                                $('.ccd:nth-child(2)').show();
                            } else {
                                $(".ccd").show();
                            }

                        });
                        $('#patient_type_1').change(function () {
                            if ($(this).val() == "") {
                                $("#typeofservice").val('');
                            } else {
                                $("#typeofservice").val($(this).val());
                            }

                        });


                    });
                </script>
                <script type="text/javascript">
                    $(function () {
                        $(document).on("click", ".date", function () {
                            $(this).datepicker({
                                changeMonth: true,
                                changeYear: true,
                                format: 'M-dd-yyyy'
                            }).datepicker("show");
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function () {
                        $("#federal_payment_frequency").change(function () {
                            var selectedText = $('#federal_payment_frequency_month').val();
                            var selectedText1 = $('#federal_payment_frequency_quaterly').val();
                            var selectedValue = $(this).val(); //alert(selectedValue);
//alert(selectedValue );
                            if (selectedValue == 'Yearly') {

                                $("#federal_payment_frequency_date").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                if (selectedText == '') {
                                    $("#federal_payment_frequency_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText < '<?php echo date('M-d-Y');?>') {
                                        $("#federal_payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');

                                    } else {
                                        $("#federal_payment_frequency_date").val('<?php echo $common->federal_payment_frequency_month;?>');
                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {

                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#federal_payment_frequency_date").val('<?php echo $common->federal_payment_frequency_quaterly;?>');
                                } else {
                                    $("#federal_payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {

                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#federal_frequency").change(function () {
                            var selectedText = $('#federal_frequency_due_monthly').val();
                            var selectedText1 = $('#federal_frequency_due_quaterly').val();
                            $(this).find("#fedral_state_1").text();
//  alert(selectedText);
                            var selectedValue = $(this).val(); ///alert(selectedValue);
                            $("#form_number_1").empty();
                            if (selectedValue == 'Yearly') {
                                $("#form_number_1").append('<option value="Form-944" selected>Form-944</option>');
                                $("#federal_frequency_due_date").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Quaterly') {

                                $("#form_number_1").append('<option value="Form-941" selected>Form-941</option>');

                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#federal_frequency_due_date").val('<?php echo $common->federal_frequency_due_quaterly;?>');
                                } else {
                                    $("#federal_frequency_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {

                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#payment_frequency").change(function () {
                            var selectedText = $('#federal_payment_frequency_month').val();
                            var selectedText1 = $('#federal_payment_frequency_quaterly').val();
                            var selectedValue = $(this).val(); //alert(selectedValue);
                            $("#form_number_2").empty();
                            if (selectedValue == 'Yearly') {
                                $("#form_number_2").append('<option value="Form-944" selected>Form-944</option>');
                                $("#payment_frequency_date").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                $("#form_number_2").append('<option value="Form-940" selected>Form-940</option>');
                                if (selectedText == '') {
                                    $("#payment_frequency_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText > '<?php echo date('M-d-Y');?>') {
                                        $("#payment_frequency_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');

                                    } else {
                                        $("#payment_frequency_date").val('<?php echo $common->federal_payment_frequency_month;?>');

                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {
                                $("#form_number_2").append('<option value="Form-941" selected>Form-941</option>');
                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#payment_frequency_date").val('<?php echo Auth::user()->federal_payment_frequency_quaterly;?>');
                                } else {
                                    $("#payment_frequency_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {

                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#frequency_type_form_1").change(function () {
                            var selectedText = $('#payment_frequency_monthly').val();
                            var selectedText1 = $('#payment_frequency_quaterly').val();

                            var selectedValue = $(this).val(); //alert(selectedValue);
                            if (selectedValue == 'Yearly') {
                                $("#frequency_due_date_1").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                if (selectedText == '') {
                                    $("#frequency_due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText > '<?php echo date('M-d-Y');?>') {
                                        $("#frequency_due_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');

                                    } else {
                                        $("#frequency_due_date_1").val('<?php echo $common->payment_frequency_monthly;?>');
                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {
                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#frequency_due_date_1").val('<?php echo $common->payment_frequency_quaterly;?>');
                                } else {
                                    $("#frequency_due_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {

                            }
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function () {
                        $("#quaterly").change(function () {
                            var selectedText = $('#quaterly_monthly').val();
                            var selectedText1 = $('#quaterly_quaterly').val();
                            var selectedValue = $(this).val(); //alert(selectedValue);
                            if (selectedValue == 'Yearly') {
                                $("#quaterly_due_date").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                if (selectedText == '') {
                                    $("#quaterly_due_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText > '<?php echo date('M-d-Y');?>') {
                                        $("#quaterly_due_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                    } else {
                                        $("#quaterly_due_date").val('<?php echo $common->quaterly_monthly;?>');
                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {
                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#quaterly_due_date").val('<?php echo $common->quaterly_quaterly;?>');
                                } else {
                                    $("#quaterly_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {
                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#payment_frequency_2").change(function () {
                            var selectedText = $('#payment_quaterly_monthly').val();
                            var selectedText1 = $('#payment_quaterly_quaterly').val();
                            var selectedValue = $(this).val(); //alert(selectedValue);
                            if (selectedValue == 'Yearly') {
                                $("#payment_frequency_date_2").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                if (selectedText == '') {
                                    $("#payment_frequency_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText > '<?php echo date('M-d-Y');?>') {
                                        $("#payment_frequency_date_2").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                    } else {
                                        $("#payment_frequency_date_2").val('<?php echo $common->payment_quaterly_monthly;?>');

                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {
                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#payment_frequency_date_2").val('<?php echo $common->payment_quaterly_quaterly;?>');
                                } else {
                                    $("#payment_frequency_date_2").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {
                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(function () {
                        $("#payment_frequency_1").change(function () {
                            var selectedText = $('#frequency_due_date_monthly_2').val();
                            var selectedText1 = $('#frequency_due_date_quaterly_2').val();
                            var selectedValue = $(this).val(); //alert(selectedValue);
                            if (selectedValue == 'Yearly') {
                                $("#payment_frequency_date_1").val('<?php echo "Jan" . '-31-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                            } else if (selectedValue == 'Monthly') {
                                if (selectedText == '') {
                                    $("#payment_frequency_date_1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                } else {
                                    if (selectedText > '<?php echo date('M-d-Y');?>') {
                                        $("#payment_frequency_date_1").val('<?php echo $common->frequency_due_date_monthly_2;?>');
                                    } else {
                                        $("#payment_frequency_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                                    }
                                }
                            } else if (selectedValue == 'Quaterly') {
                                if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                                    $("#payment_frequency_date_1").val('<?php echo $common->frequency_due_date_quaterly_2;?>');
                                } else {
                                    $("#payment_frequency_date_1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . date("t", strtotime("1 month", strtotime(date("M")))) . '-' . date("Y");?>');
                                }
                            } else {

                            }
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function () {
                        $("#type_form3").change(function () {
                            var selectedText = $('#fedral_state_1').val() //$(this).find("#fedral_state_1").text();
//alert(selectedText);
                            var selectedValue = $(this).val();
//alert(selectedValue);
                            if (selectedValue == 'County') {
                                var val = $('#physical_county_no').val();
                                var val1 = $('#location_county').val();
                                $('#business_license1').val(val);
                                $('#business_license4').hide();
                                $('#business_license3').hide();
                                $('#business_license5').empty();
                                $('#business_license5').show();
                                $('#business_license5').append('<select type="text" class="form-control" id="business_license2" name="business_license2" @if($common->business_license_jurisdiction=='City') style="display:none;" @endif><option value="">Select</option>@foreach($taxstate as $v)<option value="{{$v->county}}" @if($v->county==Auth::user()->business_license2) selected @endif>{{$v->county}}</option>@endforeach</select>');
                                $("#county-change").show();
                                $("#county-change1").show();
                                $("#city-change").hide();
                                $("#city-change1").hide();
                            } else if (selectedValue == 'City') {
                                $('#business_license4').empty();
                                var val2 = $('#city').val();
                                $('#business_license1').val('');
                                $('#business_license2').val('');
                                $('#business_license5').hide();
                                $('#business_license4').append('<input tpye="text" name="business_license2" id="ccc" value="" class="form-control">');
                                $("#county-change").hide();
                                $("#county-change1").hide();
                                $("#city-change").show();
                                $("#city-change1").show();
                            } else {
                                $("#county-change").show();
                                $('#business_license4').hide();
                            }
                        });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        $(document).on('change', '#type_form3', function () {
                            var selectedCountry = $('#stateId').val();
                            var physical_county = $('#location_county').val();
                            var physical_city = $('#city').val();//alert(physical_county);
// alert(selectedCountry);
                            var id = $(this).val();
                            $.get('{!!URL::to('getcount')!!}?id=' + id + '&state=' + selectedCountry, function (data) {
                                $('#business_license2').empty();
                                $('#business_license2').append('<option value="">Select</option>');
                                if (id == 'City') {
                                    $('#ccc').val(physical_city);
                                    $('#business_license2').hide();
                                    $('#business_license4').show();
                                }

                                $.each(data, function (index, subcatobj) {

                                    if (subcatobj.county == physical_county) {

                                        $('#business_license2').show();
                                        $('#business_license2').append('<option value="' + physical_county + '" selected>' + physical_county + '</option>');
                                        $('#business_license4').hide();
                                    } else {
                                        $('#business_license4').hide();
                                        $('#business_license2').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                                    }


                                })

                            });

                        });
                    });
                </script>
                <script>
                    $(document).on('keyup', '.federal_id', function () {
                        $('#number_1').val(this.value);
                        $('#number_2').val(this.value);

                    })
                </script>
@endsection()