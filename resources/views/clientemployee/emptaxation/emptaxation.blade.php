@extends('clientemployee.layouts.app')
@section('main-content')
    <style>
        .table > tbody > tr > td {
            padding: 6px 8px 6px 8px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of License</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">
                                <div class="table-title">

                                <!--	<a href="{{route('taxation.create')}}">Add New</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>

                                        <th>No</th>
                                        <th>Type of License</th>
                                        <th>Renewal</th>
                                        <th>Expire Date</th>
                                        <th style="width:145px !important;">Renewal Website</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($position as $pos)
                                        <tr>
                                            <td style="text-transform: capitalize; text-align:center;">{{$loop->index + 1}}</td>

                                            <td>{{$pos->question}}</td>
                                            <td>{{$pos->renewal}}</td>
                                            <td style="text-align:center;"> @if($pos->renewal=='Universal') {{$pos->date.'-'.$pos->duedate1}} @else {{$pos->date.'-'.$pos->duedate1}} @endif</td>
                                            <td style="text-align:center;">@if(!empty($pos->renewalwebsite))<a href="{{$pos->renewalwebsite}}" style="color:#222;width:140px;" target="_black" class="btn_new btn-renew col-md-2">Renewal Now</a>@endif</td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('emptaxation.edit', $pos->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('emptaxation.destroy',$pos->id) }}" method="post" style="display:none" id="delete-id-{{$pos->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$pos->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()