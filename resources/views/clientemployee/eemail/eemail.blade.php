@extends('fscemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>List of FSC Email / Telephone Extension</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                                <div class="table-title">
                                    <a href="https://www.financialservicecenter.net/webmail" style="margin-right:5px;background:linear-gradient(to bottom, #ffb963 0%, #ff9966 100%) !important;" target="_blank">Access</a>
                                    <a data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;margin-right:8px;background:linear-gradient(to bottom, #ffb963 0%, #ff9966 100%) !important;" target="_blank">Preview</a></div>
                            </div>
                        </div>
                        <div class="box-header">
                            <div class="box-tools pull-right">
                                <div class="table-title">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                            @endif
                            @if ( session()->has('error') )
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable1">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">No.</th>
                                        <th style="text-align:center">Name</th>
                                        <th style="text-align:center">Email</th>
                                        <th style="text-align:center">Ext.</th>
                                        <th style="text-align:center">Location</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($email as $a)
                                        <tr @if($a->for_whom==Auth::user()->user_id) style="background:#ccc;" @endif>
                                            <td style="text-align:center;">
                                                <center>{{$loop->index+1}}</center>
                                            </td>
                                            <td>
                                                @foreach($emp as $e)
                                                    @if($e->id == $a->for_whom)
                                                        {{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} @endif
                                                @endforeach
                                            </td>
                                            <td>{{$a->email}}</td>
                                            <td>{{$a->ext}}</center></td>
                                            <td>{{$a->location}}</td>
                                            <td>@if($a->for_whom==Auth::user()->user_id)<a class="btn-action btn-view-edit" href="{{route('eemail.edit', $a->id)}}"><i class="fa fa-edit"></i></a>@endif</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#sampleTable1').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                    //titleAttr: 'Copy',
                    title: $('h1').text(),
                },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h1').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="A"]', sheet).attr('s', '51');

                            $('c[r^="E"]', sheet).attr('s', '50');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 100, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'center',
                                        image: logo,
                                        height: 50,

                                        margin: [10, 10, -140, 10]
                                    }, {
                                        alignment: 'center',
                                        text: 'List of FSC Email / Telephone Extension',
                                        fontSize: 12,
                                        margin: [-280, 70, 10, 10],
                                    },],
                                    margin: [20, 0, 0, 22], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h6').text(),
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br style="text-align:center;">List of Email / Telephone Extension</center>'
                                );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": true,
                    "targets": 0
                }],
                "order": [[2, 'asc']]
            });

            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });
        $(document).ready(function () {
            var table = $('#sampleTable3').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                        //titleAttr: 'Copy',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h1').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 60, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'left',
                                        image: logo,
                                        width: 50, margin: [200, 5]
                                    }, {
                                        alignment: 'CENTER',
                                        text: 'List of FSC Email / Telephone Extension',
                                        fontSize: 20,
                                        margin: [10, 35],
                                    },],
                                    margin: [20, 0, 0, 12], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h1').text(),
                        exportOptions: {
                            columns: ':not(.no-print)'
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": true,
                    "targets": 0
                }],
                "order": [[1, 'asc']]
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });
    </script>
    <script src="https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2"></script>
    <script>
        function generate() {
            var doc = new jsPDF('p', 'pt');
            var res = doc.autoTableHtmlToJson(document.getElementById("sampleTable1"));
            doc.autoTable(res.columns, res.data, {margin: {top: 80}});
            var header = function (data) {
                doc.setFontSize(20);
                doc.setTextColor(10);
                doc.setFontStyle('normal');
                // doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
                doc.text("List of Email / Telephone Extension", data.settings.margin.left, 50);
            };
            var options = {
                beforePageContent: header,
                margin: {
                    top: 80
                },
                startY: doc.autoTableEndPosY() + 20
            };
            doc.autoTable(res.columns, res.data, {margin: {top: 80}}, options);
            doc.save("table.pdf");
        }
    </script>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:650px !important">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" margin-top: -95px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" id="printThis">
                        <div style=" text-align: center; "><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}" alt=""></div>
                        <h5 class="modal-title" id="exampleModalLabel"><p style="text-align:center; margin: 5px; font-size: 16px;">List of FSC Email / Telephone Extension</p></h5>
                        <table class="table table-hover table-bordered" id="sampleTable2">
                            <thead>
                            <tr>
                                <th style="text-align:center">No.</th>
                                <th style="text-align:center">EE / User</th>
                                <th style="text-align:center">Name</th>
                                <th style="text-align:center;width:200px !important;">Email</th>
                                <th style="text-align:center">Ext.</th>
                                <th style="text-align:center">Location</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($email as $a)
                                <tr>
                                    <td>
                                        <center>{{$loop->index+1}}</center>
                                    </td>
                                    <td>
                                        @foreach($emp as $e)
                                            @if($e->id == $a->for_whom)
                                                {{ucwords($e->type)}}@endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($emp as $e)
                                            @if($e->id == $a->for_whom)
                                                {{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} @endif
                                        @endforeach
                                    </td>
                                    <td>{{$a->email}}</td>
                                    <td>
                                        <center>{{$a->ext}}</center>
                                    </td>
                                    <td>{{$a->location}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="btnPrint" type="button" class="btn btn-default">Print</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementById("btnPrint").addEventListener("click", function () {
            var printContents = document.getElementById('printThis').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        });
    </script>
@endsection()