@extends('clientemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Search</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form enctype='multipart/form-data' class="form-horizontal changepassword" action="http://financialservicecenter.net/user/userchangepassword/3" id="changepassword" method="post">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group ">
                                        <label for="focusedinput" class="col-md-3 control-label">Search By Message: <span class="required"> * </span></label>
                                        <div class="col-md-4">
                                            <input name="searchtext" value="" placeholder="" class="form-control" id="searchtext" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <button class="btn-success btn" type="submit" name="search"> Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <br>
                </form>
            </div>
        </div>
        <br>
        <div class="page-title">
            <h1>Task Details</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-cogs"></i>Task Details</div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <div id="errorlist"></div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Assign To</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Creation Date</th>
                                    <th>Due Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection