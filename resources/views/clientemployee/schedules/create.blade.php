@extends('clientemployee.layouts.app')
@section('main-content')
    <style>
        label {
            float: right
        }

        .ui-timepicker-container {
            z-index: 999999 !important
        }

        #sch_start_date, #sch_end_date, #schedule_in_time, #schedule_out_time {
            text-align: center;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Add Schedule</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{route('schedules.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('emp_city') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Employer City : <span class="star-required">*</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select class="form-control category" name="emp_city" id="emp_city">
                                                        <option value=" ">Select Employer City</option>
                                                        @foreach($branch as $bran)
                                                            <option value="{{$bran->city}}">{{$bran->city}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('emp_city'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('emp_city') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('emp_name') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Employee Name : <span class="star-required">*</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select class="form-control emp" name="emp_name" id="emp_name">
                                                        <option value="" selected>---Select Employee---</option>
                                                    </select>

                                                    @if ($errors->has('emp_name'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('emp_name') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('duration') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Duration : <span class="star-required">*</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control txtOnly fsc-input" readonly id="duration" name='duration' placeholder="Duration">@if ($errors->has('duration'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('duration') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('sch_start_date') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Schedule Start Date / Day: <span class="star-required">*</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <input type="text" class="form-control txtOnly fsc-input startdate" readonly style="text-align:left" id='sch_start_date' name='sch_start_date' placeholder="Start Date">
                                                    @if ($errors->has('sch_start_date'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('sch_start_date') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <select class="ttt form-control fsc-input" name='sch_start_day' id="sch_start_day">
                                                        <option value="">---Select---</option>
                                                        <option value="Sunday">Sunday</option>
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('sch_end_date') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Schedule End Date / Day : <span class="star-required">&nbsp;&nbsp;</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <input type="text" class="form-control txtOnly fsc-input" readonly id="sch_end_date" style="text-align:left" name='sch_end_date' placeholder="End Date">
                                                    @if ($errors->has('sch_end_date'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('sch_end_date') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                    <select class="ttt form-control fsc-input" name='sch_end_day' id="sch_end_day">
                                                        <option value="">---Select---</option>
                                                        <option value="Sunday">Sunday</option>
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('schedule_in_time') ? ' has-error' : '' }} {{ $errors->has('schedule_out_time') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Time In / Time Out : <span class="star-required">&nbsp;&nbsp;</span></label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control txtOnly fsc-input ttt" id="schedule_in_time" name='schedule_in_time' placeholder="Time In">
                                                    @if ($errors->has('schedule_in_time'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('schedule_in_time') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control txtOnly fsc-input" id="schedule_out_time" name='schedule_out_time' placeholder="Time Out">
                                                    @if ($errors->has('schedule_out_time'))
                                                        <span class="help-block">
                        <strong>{{ $errors->first('schedule_out_time') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-2">
                                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                            </div>
                                            <div class="col-md-2 row">
                                                <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/schedule')}}">Cancel</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="" id="Register"></div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
    </div>
    </div>

    <script type="text/javascript">
        $("#schedule_in_time").timepicker();
        $("#schedule_out_time").timepicker();
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                var id = $(this).val();
                $.get('{!!URL::to('fscgetschedule')!!}?id=' + id, function (data) {            // $('#emp_name').append('<option value="">---Select Employee---</option>');

                    $('#emp_name').empty();
                    $('#emp_name').append('<option value="">---Select Employee---</option>');
                    $.each(data, function (index, subcatobj) {
                        $('#emp_name').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');
                    })
                });
            });
        });
    </script>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sch_start_date").change(function () {
                var startdate = $("#sch_start_date").val();
                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var durtion = $('#duration').val();
                var date = new Date(startdate);
                var day = weekday[date.getDay()];
                var monthly = 30;
                var weekly = 7;
                var bimonthly = 15;
                var biweekly = 14;
                var monthss = monthNames[(date.getMonth())];
                var yearss = date.getFullYear();
                var yyy = yearss % 4;
                //if(yyy)
                //{
                //alert('true');
                //}
                //else
                //{
                //alert('false');
                //}
                if (durtion == "Weekly") {
                    var totaldays = 6;
                } else if (durtion == "Monthly") {
                    if (monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec') {
                        var totaldays = 30;
                    } else if (monthss == 'Feb') {
                        //if(years / 4 = 0)
                        if (yyy == 0) {
                            var totaldays = 28;
                        } else if (yyy == 1) {
                            var totaldays = 27;
                        }
                    } else if (monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov') {
                        var totaldays = 29;
                    }
                } else if (durtion == "Bi-Weekly") {
                    var totaldays = 13;
                } else if (durtion == "Bi-Monthly") {
                    var totaldays = 14;
                }
                // var vv = day + totaldays;

                date.setDate(date.getDate() + totaldays);// alert(vv);
                var date1 = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
                // alert(date1);
                var newdate = new Date(date1);
                var day1 = weekday[newdate.getDay()];
                //alert(newdate);
                var date2 = monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear();
                $('#sch_end_date').val(date1);
                $('#sch_start_day').val(day);
                $('#sch_end_day').val(day1);
                //document.write(date2);
            });
            $("#duration").change(function () {
                $('#sch_end_date').val('');
                $('#sch_start_date').val('');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.emp', function () {
                //console.log('htm');
                var id = $(this).val();//alert(id);

                $.get('{!!URL::to('fscgetduration')!!}?id=' + id, function (data) {
                    $('#duration').empty();

                    $.each(data, function (index, subcatobj) {
                        $('#duration').val(subcatobj.duration);
                        $('#sch_start_date').val(subcatobj.sch_start_date);
                        $('#sch_start_day').val(subcatobj.sch_start_day);
                        $('#sch_end_date').val(subcatobj.sch_end_date);
                        $('#sch_end_day').val(subcatobj.sch_end_day);

                    })

                });

            });
        });
    </script>

    <script>
        $(document).ready(function () {

            $("#sch_start_date").datepicker({
                format: "mm/dd/yyyy"
            });
        });

    </script>



@endsection()