@extends('clientemployee.layouts.app')
@section('title', 'Profile')
@section('main-content')
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            margin-right: 0;
        }

        .nav-tabs > li > a {
            color: #444;
            border-radius: 5px;
            margin-right: 0;
        }

        .nav-tabs > li {
            width: 210px;
            margin: auto;
        }

        .rules-one {
            font-size: 16px;
            line-height: 30px;
        }

        .nav-tabs > li > a {
            height: 45px;
        }

        .btn-center {
            margin: auto;
            width: 250px;
        }

        .Branch {
            width: 100%;
            margin: 20px 0 20px 0;
            text-align: center;
            background: #428bca;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            padding: 5px;
        }

        .btn3d.btn-info:hover, .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn-info:hover {
            color: #fff;
            background-color: #0e8df2;
        }

        .Branch h1 {
            padding: 0px;
            margin: 0px 0 0 0;
            color: #fff;
            font-size: 24px;
        }

        label {
            float: right
        }

        .im {
            text-decoration: none;
            position: absolute;
            top: 21px;
            margin: -15px auto auto auto;
            right: 0;
            color: #fff;
            background: #183b687d;
            padding: 4px;
        }

        .remove {
            display: block;
            padding: 6px 16px;
            margin: -48px 0 0 0;
            position: relative;
            right: 110px;
            top: 0;
        }

        .addMore {
            position: absolute;
            right: 151px;
            top: 80px;
        }

        label.file-upload {
            position: relative;
            overflow: hidden;
            float: left;
        }

        input[type="file"] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555 !important;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 3px 3px 7px 8px !important;
        }

        .Red {
            background-color: red;
            color: #fff
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff
        }

        .Yellow {
            background-color: Yellow !important;
        }

        .Orange {
            background-color: Orange !important;
            color: #fff
        }

        .model-width {
            margin: auto;
            width: 485px;
            border: #3668f6 1px solid;
            border-radius: 0;
        }

        .model-width .modal-header {
            padding: 4px 10px;
            background: #fff;
        }

        .model-width .modal-header h4 {
            font-size: 16px;
        }

        .model-width .modal-footer {
            text-align: center;
        }

        th, td {
            text-align: center !important;
        }

        .primary {
            background-color: #d0d0d0 !important;
            border-radius: 0px;
            color: #000;
            padding: 5px 10px;
            width: 93px;
            font-weight: 200px;
        }

        .form-control {
            font-size: 16px !important;
            font-weight: 500;
        }

        .nav-tabs > li {
            width: 18.9% !important;
            margin: 2px 0 0 8px;
            border: 1px solid #ccc;
            background: #fff;
            border-radius: 8px;
            margin: 5px 5px 5px 6px !important;
        }
    </style>
    <div class="content-wrapper">
        <div class="page-title" style="height: 58px;">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h1>{{ucfirst($emp->firstName)}} {{ucfirst($emp->middleName)}} {{ucfirst($emp->lastName)}}</h1>
            </div>
            <div class="col-md-4">
                <h1 class="pull-right">FSC Client Employee</h1>
            </div>
        </div>
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">

                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active rules1"><a href="#tab1primary" data-toggle="tab">General Info </a></li>
                                        <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Hiring Info</a></li>
                                        <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Pay Info</a></li>
                                        <li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Personal Info</a></li>
                                        <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Info</a></li>
                                        <li><a href="#tab7primary" data-toggle="tab" class="hvr-shutter-in-horizontal">User Rights Info</a></li>
                                        <li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Other Info</a></li>
                                        <li class="rules"><a href="#tab8primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Rule</a></li>
                                        <li><a href="#tab9primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Responsibility</a></li>
                                        <li><a href="#tab10primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Benefits</a></li>
                                    </ul>
                                </div>
                                <form method="post" action="{{route('clientprofile.update',Auth::user()->id)}}" id="registrationForm" class="form-horizontal" enctype="multipart/form-data">
                                    {{csrf_field()}}{{method_field('PATCH')}}
                                    <input type="hidden" class="form-control" id="text1" name="text1" value="">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tab1primary">
                                                <div class="col-md-12">
                                                    <div class="Branch">
                                                        <h1>General Information</h1>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Employee ID :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" name="employee_id" id="employee_id" value="{{$emp->employee_id}}"> @if ($errors->has('employee_id'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('employee_id') }}</strong>
                                             </span>
                                                                    @endif
                                                                    @if($emp->check=='0')
                                                                        <input name="password1" value="<?php echo mt_rand();?>" class="form-control fsc-input" id="password1" readonly="" type="hidden">
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <label class="fsc-form-label">Status : </label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input style="pointer-events:none" type="text" @if($emp->check=='1') class="form-control fsc-input Green" @endif
                                                                    @if($emp->check=='0') class="form-control fsc-input Blue" @endif  id="check" name="check" id="check" value="@if($emp->check=='1') Active @endif @if($emp->check=='0') In-Active @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Name :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                    <select class="form-control fsc-input" id="nametype" name="nametype">
                                                                        <option value="mr" @if($emp->nametype=='mr') selected @endif>Mr.</option>
                                                                        <option value="mrs" @if($emp->nametype=='mrs') selected @endif>Mrs.</option>
                                                                        <option value="miss" @if($emp->nametype=='miss') selected @endif>Miss.</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="firstName" name="firstName" placeholder="First" value="{{$emp->firstName}}">@if ($errors->has('firstName'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('firstName') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="row">
                                                                        <input type="text" maxlength="1" class="form-control fsc-input " id="middleName" name="middleName" placeholder="M" value="{{$emp->middleName}}">
                                                                    </div>

                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input " id="lastName" name="lastName" value="{{$emp->lastName}}" placeholder="Last">
                                                                    <input type="hidden" class="form-control fsc-input  " id="type" name="type" value="{{$emp->type}}" placeholder="Last">
                                                                    @if ($errors->has('lastName'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('lastName') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('address1') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Address 1 : </label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="{{$emp->address1}}">@if ($errors->has('address1'))
                                                                <span class="help-block">
                                       <strong>{{ $errors->first('address1') }}</strong>
                                       </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Address 2 :</label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="{{$emp->address2}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('countryId') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Country :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown">
                                                                        <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="{{$emp->countryId}}" style='height:auto'></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}{{ $errors->has('stateId') ? ' has-error' : '' }}{{ $errors->has('zip') ? ' has-error' : '' }} ">
                                                        <label class="control-label col-md-3">City/State/Zip :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City" value="{{$emp->city}}">
                                                                    @if ($errors->has('city'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('city') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$emp->stateId}}">

                                                                        </select>
                                                                        @if ($errors->has('stateId'))
                                                                            <span class="help-block">
                                                <strong>{{ $errors->first('stateId') }}</strong>
                                                </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input zip" id="zip" name="zip" maxlength="6" value="{{$emp->zip}}" placeholder="Zip">
                                                                    @if ($errors->has('zip'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('zip') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group {{ $errors->has('telephoneNo1') ? ' has-error' : '' }} {{ $errors->has('telephoneNo1Type') ? ' has-error' : '' }} ">
                                                        <label class="control-label col-md-3">Telephone 1 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo1" name="telephoneNo1" placeholder="(999) 999-9999" value="{{$emp->telephoneNo1}}"> @if ($errors->has('telephoneNo1'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('telephoneNo1') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                            <option value='Mobile' @if($emp->telephoneNo1Type=='Mobile') selected @endif>Mobile</option>
                                                                            <option value='Home' @if($emp->telephoneNo1Type=='Home') selected @endif>Home</option>
                                                                            <option value='Work' @if($emp->telephoneNo1Type=='Work') selected @endif>Work</option>
                                                                            <option value='Office' @if($emp->telephoneNo1Type=='Office') selected @endif>Office</option>
                                                                            <option value='Other' @if($emp->telephoneNo1Type=='Other') selected @endif>Other</option>
                                                                        <!--
                                                <option value="Mobile" @if($emp->telephoneNo1Type=='Mobile') selected @endif>Work</option>
                                                <option value="Resident" @if($emp->telephoneNo1Type=='Resident') selected @endif>Resident</option>
                                                <option value="Office"  @if($emp->telephoneNo1Type=='Office') selected @endif>Cell</option>
                                                <option value="Other" @if($emp->telephoneNo1Type=='Other') selected @endif>Other</option>-->
                                                                        </select>
                                                                        @if ($errors->has('telephoneNo1Type'))
                                                                            <span class="help-block">
                                                <strong>{{ $errors->first('telephoneNo1Type') }}</strong>
                                                </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" readonly id="ext1" maxlength="3" name="ext1" value="{{$emp->ext1}}" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('telephoneNo2') ? ' has-error' : '' }} {{ $errors->has('telephoneNo2Type') ? ' has-error' : '' }} ">
                                                        <label class="control-label col-md-3">Telephone 2 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo2" name="telephoneNo2" placeholder="(9999) 999-9999" value="{{$emp->telephoneNo2}}">
                                                                    @if ($errors->has('telephoneNo2'))
                                                                        <span class="help-block">
                                             <strong>{{ $errors->first('telephoneNo2') }}</strong>
                                             </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                                        <!--<option value="Mobile" @if($emp->telephoneNo2Type=='Mobile') selected @endif>Work</option>
                                                <option value="Resident" @if($emp->telephoneNo2Type=='Resident') selected @endif>Resident</option>
                                                <option value="Office"  @if($emp->telephoneNo2Type=='Office') selected @endif>Cell</option>
                                                <option value="Other" @if($emp->telephoneNo2Type=='Other') selected @endif>Other</option>-->
                                                                            <option value='Mobile' @if($emp->telephoneNo2Type=='Mobile') selected @endif>Mobile</option>
                                                                            <option value='Home' @if($emp->telephoneNo2Type=='Home') selected @endif>Home</option>
                                                                            <option value='Work' @if($emp->telephoneNo2Type=='Work') selected @endif>Work</option>
                                                                            <option value='Office' @if($emp->telephoneNo2Type=='Office') selected @endif>Office</option>
                                                                            <option value='Other' @if($emp->telephoneNo2Type=='Other') selected @endif>Other</option>
                                                                        </select>
                                                                        @if ($errors->has('telephoneNo2Type'))
                                                                            <span class="help-block">
                                                <strong>{{ $errors->first('telephoneNo2Type') }}</strong>
                                                </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" maxlength="3" readonly id="ext2" name="ext2" value="{{$emp->ext2}}" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('fax') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Fax :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown">
                                                                        <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="fax" name="fax" value="{{$emp->fax}}" placeholder="(9999) 999-9999">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Email :</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control fsc-input" id="email" readonly name="email" value="{{$emp->email}}" placeholder="Email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('photo') ? ' has-error' : '' }}">
                                                        <label class="control-label col-md-3">Picture :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div style="width:100px;height:100px; border-radius: 50%; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                        <img width="100%" style="border-radius: 50%;" height="100%" id="preview_image_5" src="@if(empty($emp->photo)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/employeeimage/')}}/{{$emp->photo}}@endif"/>
                                                                        <i id="loading5" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                    </div>
                                                                    <a href="javascript:photo_upload()" style="text-decoration: none;" class="im"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                    <input type="file" id="photo" style="display: none"/>
                                                                    <input type="hidden" id="photo_name"/>
                                                                    <input type="hidden" id="user_id11" value="{{$emp->id}}"/>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                    <div class="Branch">
                                                        <h1>Hiring Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Hire Date :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="hiremonth" type="text" value="{{$emp->hiremonth}}" id="hiremonth" class="form-control date1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <label class="control-label">Termination Date : </label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <input name="termimonth" type="text" value="{{$emp->termimonth}}" id="termimonth" class="form-control date1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Note :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <textarea name="tnote" id="tnote" class="form-control fsc-input">{{$emp->tnote}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Re-Hire Date :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="rehiremonth" type="text" value="{{$emp->rehiremonth}}" id="rehiremonth" class="form-control date1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <label class="control-label">Termination Date :</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="rehireyear" type="text" value="{{$emp->rehireyear}}" id="rehireyear" class="form-control date1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Note :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <textarea name="tnote" id="tnote" class="form-control fsc-input">{{$emp->tnote}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Branch">
                                                        <h1>Branch / Department Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Branch City:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="branch_city" id="branch_city" class="form-control fsc-input category">
                                                                            <option value="">---Select---</option>
                                                                            @foreach($branch as $pos)
                                                                                <option value="{{$pos->city}}" @if($emp->branch_city ==$pos->city) selected @endif>{{$pos->city}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Branch Name:</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <input type="text" readonly class="form-control fsc-input" id="branch_name" name="branch_name" placeholder="" value="{{$emp->branch_name}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Position :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="position" id="position" class="form-control fsc-input">
                                                                            <option value="">---Select Position---</option>
                                                                            @foreach($position as $pos)
                                                                                <option value="{{$pos->id}}" @if($emp->position ==$pos->id) selected @endif>{{$pos->position}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2"><input type="checkbox" id="super" name="super" value="1" @if($emp->super =='1') checked @endif><label for="super"> Supervisor</label></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Note :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <textarea name="note" id="note" class="form-control fsc-input">{{$emp->note}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="Branch">
                                                        <h1>Review Information</h1>
                                                    </div>

                                                    <div class="review">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <a href="javascript:void(0)" class="ad pull-right btn btn-primary" style="margin:10px 0;position: absolute;right: 113px;top: 0px;"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a>

                                                            </div>
                                                        </div>
                                                        <?php $i = 1; $j = 1; $count = count($review1);?>

                                                        @if($count!=null)
                                                            @foreach($review1 as $re)
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $('#first_rev_day{{$re->id}}').on('keyup', function () {
                                                                            var hiremonth = $('#hiremonth').val();
                                                                            var reset1 = parseInt($('#first_rev_day{{$re->id}}').val()); //alert(reset);
                                                                            var tt = hiremonth.split("-").reverse().join(" ");
                                                                            var t = new Date(tt);

                                                                            t.setDate(t.getDate() + reset1);
                                                                            var month = "0" + (t.getMonth() + 1);
                                                                            var date = "0" + t.getDate();
                                                                            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                                                                            const d = new Date();
                                                                            month = month.slice(-2);
                                                                            date = date.slice(-2);
                                                                            var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                                                                            if (reset1 == '') {
                                                                                $('#reviewmonth{{$re->id}}').empty();
                                                                            } else {
                                                                                $('#reviewmonth{{$re->id}}').val(date);
                                                                            }
                                                                        });
                                                                    });
                                                                </script>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"><?php echo $i; $i++;?> Review Days :</label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <input type="text" class="form-control fsc-input first_rev_day" maxlength="3" onkeypress="return isNumberKey(event)" name="first_rev_day[]" id="first_rev_day{{$re->id}}" value="{{$re->first_rev_day}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <label class="control-label"><?php echo $j; $j++;?> Review Date :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <input name="reviewmonth[]" type="text" value="{{$re->reviewmonth}}" id="reviewmonth{{$re->id}}" class="form-control reviewmonth" readonly="readonly">
                                                                                    <input name="ree[]" type="hidden" value="{{$re->id}}" id="ree" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Comments :</label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <textarea name="hiring_comments[]" id="hiring_comments" class="form-control fsc-input">{{$re->hiring_comments}}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if($i == 2)
                                                                @else
                                                                    <a href="#myModal{{$re->id}}" role="button" class="btn btn-danger remove pull-right" title="Add field" data-toggle="modal"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Review Days :</label>
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                                <input type="text" class="form-control fsc-input first_rev_day" onkeypress="return isNumberKey(event)" maxlength="3" name="first_rev_day[]" id="first_rev_day">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                                <label class="control-label">Review Date :</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                                <input name="reviewmonth[]" type="text" id="reviewmonth" class="form-control reviewmonth" readonly="readonly">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input name="ree[]" type="hidden" value="" id="ree" class="form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comments :</label>
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="review-1">
                                                            <div class="review-2">
                                                                <div class="input_fields_wrap_1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab3primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Salary Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Pay Method : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="pay_method" id="pay_method" class="form-control">
                                                                            <option value="Salary" @if($emp->pay_method=='Salary') selected @endif>Salary</option>
                                                                            <option value="Hourly" @if($emp->pay_method=='Hourly') selected @endif>Hourly</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="fsc-form-label">Pay Duration : </label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <select name="pay_frequency" id="pay_frequency" class="form-control">
                                                                        <option value="Weekly" @if($emp->pay_frequency=='Weekly') selected @endif>Weekly</option>
                                                                        <option value="Bi-Weekly" @if($emp->pay_frequency=='Bi-Weekly') selected @endif>Bi-Weekly</option>
                                                                        <option value="Bi-Monthly" @if($emp->pay_frequency=='Semi-Monthly') selected @endif>Semi-Monthly</option>
                                                                        <option value="Monthly" @if($emp->pay_frequency=='Monthly') selected @endif>Monthly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fieldGroup">
                                                        <a href="javascript:void(0)" class="addMore pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Pay</a>
                                                        <?php $k = 1; $in = count($info);?>
                                                        @if($in!=null)
                                                            @foreach($info as $in)
                                                                <div id="field{{$in->id}}">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Pay Rate : </label>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                                        <input name="pay_scale[]" value="{{$in->pay_scale}}" type="text" maxlength="10" id="pay_scale" class="form-control pay_scale"/>
                                                                                        <input name="employee[]" value="{{$in->id}}" type="hidden" id="employee" class="form-control"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                    <label class="fsc-form-label">Effective Date : </label>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                    <input name="effective_date[]" value="{{$in->effective_date}}" type="text" id="effective_date{{$in->id}}" class="form-control date1"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Note :</label>
                                                                        <div class="col-md-6">
                                                                            <textarea id="fields" name="fields[]" class="form-control fsc-input">{{$in->fields}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                    <?php $k; $k++;?>
                                                                    @if($k == 2)
                                                                    @else
                                                                        <div class="form-group">
                                                                            <div class="col-md-12"><a href="#myModal1{{$in->id}}" role="button" class="btn btn-danger remove11 pull-right" data-toggle="modal"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div id="Pay Method">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Pay Rate : </label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                                    <input name="pay_scale[]" value="" type="text" id="pay_scale" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control pay_scale"/>
                                                                                    <input name="employee[]" value="" type="hidden" id="employee" class="form-control"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <label class="fsc-form-label">Effective Date : </label>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                                <input name="effective_date[]" value="" type="text" id="effective_date" class="form-control date1"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Note :</label>
                                                                    <div class="col-md-6">
                                                                        <textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Taxes Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Filling Status :</label>
                                                        <div class="col-md-6">
                                                            <select name="filling_status" id="filling_status" class="form-control">
                                                                <option value="Single" @if($emp->filling_status=='Single') selected @endif>Single</option>
                                                                <option value="MFJ" @if($emp->filling_status=='Married File Jointly') selected @endif>Married File Jointly</option>
                                                                <option value="MFJ" @if($emp->filling_status=='Married File Separately') selected @endif>Married File Separately</option>
                                                                <option value="MFJ" @if($emp->filling_status=='Head of Household') selected @endif>Head of Household</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Fedral Claim : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="fedral_claim" value="{{$emp->fedral_claim}}" maxlength="3" onkeypress="return isNumberKey(event)" type="text" id="fedral_claim" onkeypress="return isNumberKey(event)" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="fsc-form-label">Additional Withholding : </label>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                                    <input name="additional_withholding" value="{{$emp->additional_withholding}}" type="text" id="additional_withholding" class="form-control additional_withholding"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <center>
                                                                        <div style="width:50px;height:50px;border-radius:50%;border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                            <img width="100%" height="100%" style="border-radius:50%;" id="preview_image_2" src="@if(empty($emp->additional_attach)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/uploads/')}}/{{$emp->additional_attach}}@endif"/>
                                                                            <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                        </div>
                                                                        <a href="javascript:additional_attach()" style="text-decoration: none;" class="im"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                        <input type="file" id="file_2" style="display: none"/>
                                                                        <input type="hidden" id="file_name_2"/>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">State Claim : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="state_claim" value="{{$emp->state_claim}}" maxlength="3" type="text" id="state_claim" onkeypress="return isNumberKey(event)" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="fsc-form-label">Additional Withholding : </label>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                                    <input name="additional_withholding_1" value="{{$emp->additional_withholding_1}}" type="text" id="additional_withholding_1" class="form-control additional_withholding"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <center>
                                                                        <div style="width:50px;height:50px; border-radius:50%; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                            <img width="100%" style=" border-radius:50%;" height="100%" id="preview_image_1" src="@if(empty($emp->additional_attach_1)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/uploads/')}}/{{$emp->additional_attach_1}}@endif"/>
                                                                            <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                        </div>
                                                                        <a class="im" href="javascript:additional_attach_update()" style="text-decoration: none;"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                        <input type="file" id="file_1" style="display: none"/>
                                                                        <input type="hidden" id="file_name_1"/>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Local Claim : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input name="local_claim" value="{{$emp->local_claim}}" maxlength="3" type="text" id="local_claim" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="fsc-form-label">Additional Withholding : </label>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                                    <input name="additional_withholding_2" value="{{$emp->additional_withholding_2}}" type="text" id="additional_withholding_2" class="form-control additional_withholding"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                                                    <center>
                                                                        <div style=" border-radius:50%;width:50px;height:50px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                            <img width="100%" style=" border-radius:50%;" height="100%" id="preview_image" src="@if(empty($emp->additional_attach_2)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/uploads/')}}/{{$emp->additional_attach_2}}@endif"/>
                                                                            <i id="loading2" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                        </div>
                                                                        <a class="im" href="javascript:changeProfile()" style="text-decoration: none;"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                        <input type="file" id="file" style="display: none"/>
                                                                        <input type="hidden" id="file_name"/>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab4primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Personal Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Gender :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="gender" id="gender" class="form-control fsc-input">
                                                                            <option value="Male" @if($emp->gender=='Male') selected @endif>Male</option>
                                                                            <option value="Female" @if($emp->gender=='Female') selected @endif>Female</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Marital Status :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="marital" id="marital" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Married" @if($emp->marital=='Married') selected @endif>Married</option>
                                                                            <option value="UnMarried" @if($emp->marital=='UnMarried') selected @endif>UnMarried</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date of Birth : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <input type="text" class="form-control date1 fsc-input" name="month" id="month" value="{{$emp->month}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">ID Proof 1 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="pf1" class="form-control fsc-input" id="pf1">
                                                                            <option value="">Select</option>
                                                                            <option value="State ID" @if($emp->pf1=='State ID') selected @endif>State ID</option>
                                                                            <option value="Voter Id" @if($emp->pf1=='Voter Id') selected @endif>Voter ID</option>
                                                                            <option value="Driving Licence" @if($emp->pf1=='Driving Licence') selected @endif>Driving Licence</option>
                                                                            <option value="Pan Card" @if($emp->pf1=='DPan Card') selected @endif>Pan Card</option>
                                                                            <option value="Pass Port" @if($emp->pf1=='Pass Port') selected @endif>Pass Port</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('pfid1') ? ' has-error' : '' }}">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <center>
                                                                            <div style="width:50px;height:50px;border-radius:50%; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                                <img width="100%" height="100%" style="border-radius:50%;" id="preview_image_3" src="@if(empty($emp->pfid1)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/employeeProof1/')}}/{{$emp->pfid1}}@endif"/>
                                                                                <i id="loading3" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                            </div>
                                                                            <a class="im" href="javascript:pfid_upload()" style="text-decoration: none;"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                            <input type="file" id="pfid1" style="display: none"/>
                                                                            <input type="hidden" id="pfid1_name"/>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">ID Proof 2 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="pf2" class="form-control fsc-input" id="pf2">
                                                                            <option value="">Select</option>
                                                                            <option value="State ID" @if($emp->pf2=='State ID') selected @endif>State ID</option>
                                                                            <option value="Voter Id" @if($emp->pf2=='Voter Id') selected @endif>Voter ID</option>
                                                                            <option value="Driving Licence" @if($emp->pf2=='Driving Licence') selected @endif>Driving Licence</option>
                                                                            <option value="Pan Card" @if($emp->pf2=='DPan Card') selected @endif>Pan Card</option>
                                                                            <option value="Pass Port" @if($emp->pf2=='Pass Port') selected @endif>Pass Port</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('pfid2') ? ' has-error' : '' }}">
                                                                    <center>
                                                                        <div style="width:50px;height:50px; border-radius:50%;; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                                            <img width="100%" height="100%" style="border-radius:50%;" id="preview_image_4" src="@if(empty($emp->pfid2)) {{asset('public/images/noimage.jpg')}} @else {{asset('public/employeeProof2/')}}/{{$emp->pfid2}}@endif"/>
                                                                            <i id="loading4" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                        </div>
                                                                        <a href="javascript:pfid2_upload()" style="text-decoration: none;" class="im"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                        <input type="file" id="pfid2" style="display: none"/>
                                                                        <input type="hidden" id="pfid2_name"/>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Resume :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('resume') ? ' has-error' : '' }}">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <label class="file-upload btn btn-primary">
                                                                            Browse for file ... <input name="resume" style="opecity:0" placeholder="Upload Service Image" id="resume" type="file">
                                                                        </label>
                                                                        <input name="resume_1" placeholder="Upload Service Image" value="{{$emp->resume}}" class="form-control fsc-input" id="resume_1" type="hidden">
                                                                        @if ($errors->has('resume'))
                                                                            <span class="help-block">
															<strong>{{ $errors->first('resume') }}</strong>
															</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Type of Agreement :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="type_agreement" class="form-control fsc-input" id="type_agreement">
                                                                            <option value="">Select</option>
                                                                            <option value="Hiring Letter" @if($emp->type_agreement=='Hiring Letter') selected @endif>Hiring Letter</option>
                                                                            <option value="Employment Agreement" @if($emp->type_agreement=='Employment Agreement') selected @endif>Employment Agreement</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('agreement') ? ' has-error' : '' }}">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <label class="file-upload btn btn-primary">Browse for file ...
                                                                            <input name="agreement" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement" type="file">
                                                                        </label>
                                                                        <input name="agreement_1" value="{{$emp->agreement}}" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement_1" type="hidden">
                                                                        <img src="{{asset('public/agreement')}}/{{$emp->agreement}}" alt="" id="blah-3" alt="your image" style="margin-top:10px;width:69px;">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Branch">
                                                        <h1>Emergency Contact Info</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Contact Person Name : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input " id="firstName_1" name="firstName_1" placeholder="First" value="{{$emp->firstName_1}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" maxlength="1" class="form-control  fsc-input" id="middleName_1" name="middleName_1" placeholder="Middle" value="{{$emp->middleName_1}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control  fsc-input" id="lastName_1" name="lastName_1" value="{{$emp->lastName_1}}" placeholder="Last">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Address 1 : </label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="{{$emp->address11}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Address 2:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="{{$emp->eaddress1}}"><input type="hidden" class="form-control fsc-input" name="status" id="status" value="{{$emp->status}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">City/State/Zip : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control textonly fsc-input" id="ecity" name="ecity" placeholder="City" value="{{$emp->ecity}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="estate" id="estate" class="form-control fsc-input">
                                                                            <option value="ID">Select</option>
                                                                            <option value="ID">ID</option>
                                                                            <option value="AK">AK</option>
                                                                            <option value="AS">AS</option>
                                                                            <option value="AZ">AZ</option>
                                                                            <option value="AR">AR</option>
                                                                            <option value="CA">CA</option>
                                                                            <option value="CO">CO</option>
                                                                            <option value="CT">CT</option>
                                                                            <option value="DE">DE</option>
                                                                            <option value="DC">DC</option>
                                                                            <option value="FM">FM</option>
                                                                            <option value="FL">FL</option>
                                                                            <option value="GA">GA</option>
                                                                            <option value="GU">GU</option>
                                                                            <option value="HI">HI</option>
                                                                            <option value="ID">ID</option>
                                                                            <option value="IL">IL</option>
                                                                            <option value="IN">IN</option>
                                                                            <option value="IA">IA</option>
                                                                            <option value="KS">KS</option>
                                                                            <option value="KY">KY</option>
                                                                            <option value="LA">LA</option>
                                                                            <option value="ME">ME</option>
                                                                            <option value="MH">MH</option>
                                                                            <option value="MD">MD</option>
                                                                            <option value="MA">MA</option>
                                                                            <option value="MI">MI</option>
                                                                            <option value="MN">MN</option>
                                                                            <option value="MS">MS</option>
                                                                            <option value="MO">MO</option>
                                                                            <option value="MT">MT</option>
                                                                            <option value="NE">NE</option>
                                                                            <option value="NV">NV</option>
                                                                            <option value="NH">NH</option>
                                                                            <option value="NJ">NJ</option>
                                                                            <option value="NM">NM</option>
                                                                            <option value="NY">NY</option>
                                                                            <option value="NC">NC</option>
                                                                            <option value="ND">ND</option>
                                                                            <option value="MP">MP</option>
                                                                            <option value="OH">OH</option>
                                                                            <option value="OK">OK</option>
                                                                            <option value="OR">OR</option>
                                                                            <option value="PW">PW</option>
                                                                            <option value="PA">PA</option>
                                                                            <option value="PR">PR</option>
                                                                            <option value="RI">RI</option>
                                                                            <option value="SC">SC</option>
                                                                            <option value="SD">SD</option>
                                                                            <option value="TN">TN</option>
                                                                            <option value="TX">TX</option>
                                                                            <option value="UT">UT</option>
                                                                            <option value="VT">VT</option>
                                                                            <option value="VI">VI</option>
                                                                            <option value="VA">VA</option>
                                                                            <option value="WA">WA</option>
                                                                            <option value="WV">WV</option>
                                                                            <option value="WI">WI</option>
                                                                            <option value="WY">WY</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control zip fsc-input" id="ezipcode" maxlength="5" name="ezipcode" value="{{$emp->ezipcode}}" placeholder="Zip">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Telephone 1 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="etelephone1" name="etelephone1" placeholder="(000) 000-0000" value="{{$emp->etelephone1}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="eteletype1" id="eteletype1" class="form-control fsc-input">
                                                                            <option value='Mobile' @if($emp->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                            <option value='Home' @if($emp->eteletype1=='Home') selected @endif>Home</option>
                                                                            <option value='Work' @if($emp->eteletype1=='Work') selected @endif>Work</option>
                                                                            <option value='Office' @if($emp->eteletype1=='Office') selected @endif>Office</option>
                                                                            <option value='Other' @if($emp->eteletype1=='Other') selected @endif>Other</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" maxlength="5" value="{{$emp->eext1}}" readonly id="eext1" name="eext1" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Telephone 2 :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="{{$emp->etelephone2}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="eteletype2" id="eteletype2" class="form-control fsc-input">
                                                                            <option value='Mobile' @if($emp->eteletype2=='Mobile') selected @endif>Mobile</option>
                                                                            <option value='Home' @if($emp->eteletype2=='Home') selected @endif>Home</option>
                                                                            <option value='Work' @if($emp->eteletype2=='Work') selected @endif>Work</option>
                                                                            <option value='Office' @if($emp->eteletype2=='Office') selected @endif>Office</option>
                                                                            <option value='Other' @if($emp->eteletype2=='Other') selected @endif>Other</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" maxlength="5" value="{{$emp->eext2}}" readonly id="eext2" name="eext2" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Fax :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="text" class="form-control fsc-input" placeholder="(000) 000 0000" name="efax" id="efax" value="{{$emp->efax}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">E-mail :</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control fsc-input" name="eemail" id="eemail" value="{{$emp->eemail}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Relationship :</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control fsc-input" name="relation" id="relation" value="{{$emp->relation}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Note For Emergency :</label>
                                                        <div class="col-md-6">
                                                            <textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input">{{$emp->comments1}}</textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab5primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Security Information</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">User Name :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input id="uname" class="form-control fsc-input" placeholder="User Name" value="{{$emp->email}}" readonly="" name="uname" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Password :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input placeholder="Password" class="form-control fsc-input" id="password" name="newpassword" value="{{Auth::user()->newpassword}}" readonly="" type="password">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Reset Days : </label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <select name="reset" id="reset" class="form-control fsc-input" readonly="">
                                                                        <option value="30" @if($emp->reset=='30') selected @endif>30</option>
                                                                        <option value="60" @if($emp->reset=='60') selected @endif>60</option>
                                                                        <option value="90" @if($emp->reset=='90') selected @endif>90</option>
                                                                        <option value="120" @if($emp->reset=='120') selected @endif>120</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <input name="reset_date" id="reset_date" class="form-control fsc-input" readonly="" value="{{Auth::user()->enddate}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 1 : </label>
                                                        <div class="col-md-6">
                                                            <select name="question1" id="question1" class="form-control fsc-input" readonly="">
                                                                <option value="">Select</option>
                                                                <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                                <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                                <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                                                <option value="In what city were you born?" selected="selected">In what city were you born?</option>
                                                                <option value="What is the name of your first school?">What is the name of your first school?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 1 :</label>
                                                        <div class="col-md-6">
                                                            <input name="answer1" value="{{$emp->answer1}}" placeholder="" class="form-control fsc-input" id="answer1" readonly="" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 2 :</label>
                                                        <div class="col-md-6">
                                                            <select name="question2" id="question2" class="form-control fsc-input" readonly="">
                                                                <option value="">Select</option>
                                                                <option value="What is your favorite movie?">What is your favorite movie?</option>
                                                                <option value="What was the make of your first car?">What was the make of your first car?</option>
                                                                <option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
                                                                <option value="What is your father's middle name?">What is your fathers middle name?</option>
                                                                <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 2 :</label>
                                                        <div class="col-md-6">
                                                            <input name="answer2" value="{{$emp->answer2}}" placeholder="" class="form-control fsc-input" id="answer2" readonly="" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 3 :</label>
                                                        <div class="col-md-6">
                                                            <select name="question3" id="question3" class="form-control fsc-input" readonly="">
                                                                <option value="">Select</option>
                                                                <option value="What was your high school mascot?">What was your high school mascot?</option>
                                                                <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                                                <option value="In what year was your father born?">In what year was your father born?</option>
                                                                <option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
                                                                <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 3 :</label>
                                                        <div class="col-md-6">
                                                            <input name="answer3" value="{{$emp->answer3}}" placeholder="" class="form-control fsc-input" id="answer3" readonly="" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab6primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Coming Soon...</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab8primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Employee Rules</h1>
                                                    </div>
                                                    <div class="responsibility-8">
                                                        <div id="responsibility">
                                                            <ul>
                                                                @foreach($rules as $rule)
                                                                    @if($rule->type=='Rules')
                                                                        <li><b>{{$rule->title}} : </b><br>{!!$rule->rules!!}</li>
                                                                    @endif
                                                                @endforeach
                                                                <li>
                                                                    <input @if($emp->read=='2') checked @else  @endif type="checkbox" id="terms" name="terms" value="2">
                                                                    <label for="terms" style="float:left"> I read an acknowledge the rules of Financial Service Center and I will follow as per company's rule. </label>
                                                                    <div class="row">
                                                                        <div class="col-md-2">@if($emp->rulesdate)<input name="rulesdate" value="{{date("F-d-Y",strtotime($emp->rulesdate))}}" placeholder="" class="form-control fsc-input" id="rulesdate" readonly="" type="text"> @else    <input name="rulesdate" value="{{date("F-d-Y",strtotime(date('y-m-d')))}}" placeholder="" class="form-control fsc-input" id="rulesdate" readonly="" type="text"> @endif</div>
                                                                        <div class="col-md-2"><input name="signature" value="" placeholder="" class="form-control fsc-input" id="signature" type="text"></div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab9primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Work Responsibility</h1>
                                                    </div>
                                                    <div class="responsibility">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <table id="sampleTable3_wrapper" class="table table-striped table-bordered customers" style="width:100%">
                                                                    <thead>
                                                                    <tr style="text-align:center">
                                                                        <th>No.</th>
                                                                        <th>Creation Date</th>
                                                                        <th>Responsibility From</th>
                                                                        <th>Responsibility Date</th>
                                                                        <th>Frequency</th>
                                                                        <th>Responsibility Subject</th>
                                                                        <th>Checked</th>
                                                                        <th>Checked Date</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($resposibilty as $resposibilty1 )
                                                                        @if($resposibilty1->type=='Resposibilty' && $resposibilty1->employee_id==$emp->id)
                                                                            <tr style="text-align:center">
                                                                                <td>{{$loop->index+1}}</td>
                                                                                <td>{!!$resposibilty1->date!!}</td>
                                                                                <td>Mr. Vijay Bombaywala</td>
                                                                                <td>{!!$resposibilty1->date!!}</td>
                                                                                <td>{!!$resposibilty1->respon_type!!}</td>
                                                                                <td>{!!$resposibilty1->title!!}</td>
                                                                                <td>
                                                                                    <center>@if($resposibilty1->status=='2') <img src="{{URL::asset('public/img/green.jpg')}}" alt="" width="40"> @else  <img src="{{URL::asset('public/img/red.jpg')}}" alt="" width="40"> @endif </center>
                                                                                </td>
                                                                                <td>{!!$resposibilty1-> rulesdate !!}</td>
                                                                                <td><a href="{{route('resposibilty.edit',$resposibilty1->id)}}" class="btn btn-primary">Edit Resposibilty</a></td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab7primary">
                                                <div class="col-md-12">
                                                    <div class="Branch">
                                                        <h1>User Rights</h1>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Supervisor Name :</label>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-7">
                                                                    <select name="question3" id="question3" class="form-control fsc-input" readonly="">
                                                                        <option value="">Select</option>
                                                                        @foreach($super1 as $sup1)
                                                                            @foreach($super as $sup)
                                                                                <option value="{{ $sup1->id}}" @if($sup->username==$sup1->id) selected @endif>{{ $sup1->firstName.' '.$sup1->middleName.' '.$sup1->lastName}}</option>
                                                                            @endforeach
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-md-offset-3 col-md-7">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input class="btn_new_save btn-primary1 primary1" style="margin-left:6%" type="button" value="Save">
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="btn_new_cancel" style="margin-left:6%" href="https://financialservicecenter.net/clientemployee">Cancel</a>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    </div>
    </div>
    </div>
    <!-- copy of input fields group -->
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>-->
    <!-- copy of input fields group -->
    <div class="form-group fieldGroupCopy" style="display: none;">
        <div><input type="hidden" class="form-control fsc-input" name="employee[]" id="employee" value="">
            <div class="form-group"><label class="control-label col-md-3">Pay Rate :</label>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="dropdown" style="margin-top: 1%;"><input name="pay_scale[]" value="" type="text" id="pay_scale" maxlength="10" class="form-control pay_scale"/></div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><label class="fsc-form-label">Effective Date :</label></div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><input name="effective_date[]" type="text" id="effective_date" class="form-control date1"/></div>
                    </div>
                </div>
            </div>
            <div class="form-group"><label class="control-label col-md-3">Note :</label>
                <div class="col-md-6"><textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea></div>
            </div>
            <div class="form-group">
                <div class="col-md-12"><a href="javascript:void(0)" class="btn btn-danger remove pull-right"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
            </div>
        </div>
        <script>
            $(document).on('blur', '.pay_scale', function () {
                var sum = 0;
                ckq = "$"
                //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
                // var n1 = n.toLocaleString();
                var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var x2 = ckq + ' ' + n1;
                $(this).val(x2);

                $('.price').each(function () {
                    sum += parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                });

            });
            $(document).on('blur', '.additional_withholding', function () {
                var sum = 0;
                ckq = "$"
                //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
                // var n1 = n.toLocaleString();
                var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var x2 = ckq + ' ' + n1;
                $(this).val(x2);

                $('.price').each(function () {
                    sum += parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                });

            });

        </script>
    </div>
    </div>
    <script>
        function changeProfile() {
            $('#file').click();
        }

        $('#file').change(function () {
            if ($(this).val() != '') {
                upload(this);

            }
        });

        function upload(img) {
            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading2').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/additional')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#file_name').val(data);
                        $('#preview_image').attr('src', '{{asset('public/uploads')}}/' + data);
                    }
                    $('#loading2').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }
    </script>
    <script>
        function photo_upload() {
            $('#photo').click();
        }

        $('#photo').change(function () {
            if ($(this).val() != '') {
                upload5(this);
            }
        });

        function upload5(img) {
            var form_data = new FormData(); //alert(form_data);
            form_data.append('photo', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading5').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/profilephoto1')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image_5').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#photo_name').val(data);//

                        // alert();
                        $('#preview_image_5').attr('src', '{{asset('public/employeeimage')}}/' + data);
                    }
                    $('#loading5').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image_5').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var maxField = 10;
            var addButton = $('.ad');
            var x = 1;
            var y = <?php echo $count;?>;
            var z = x + y;
            var wrapper = $('.review-1');
            $(addButton).click(function () {
                if (z < maxField) {
                    $(wrapper).append('<div class="review-2"><div class="input_fields_wrap_1"><div class="form-group"><label class="control-label col-md-3">' + z + ' Review Days :</label><div class="col-md-6"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="ree[]" type="hidden" value="" id="ree" class="form-control"><input type="text"  maxlength="3" class="form-control fsc-input first_rev_day" name="first_rev_day[]" id="first_rev_day' + z + '" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><label class="control-label">' + z + ' Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth' + z + '" class="form-control reviewmonth" readonly="readonly"></div></div></div></div></div><div class="form-group"><label class="control-label col-md-3">Comments :</label><div class="col-md-6"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><textarea name="hiring_comments[]" id="hiring_comments' + z + '" rows="1" class="form-control fsc-input"></textarea></div></div></div></div><a href="javascript:void(0)" class="btn btn-danger remove pull-right "><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>'); // Add field html
                    var rdiv1 = '#first_rev_day' + z;
                    var rdiv2 = '#reviewmonth' + z;//alert(rdiv2);
                    $(rdiv1).on('keyup', function () {
                        var hiremonth = $('#hiremonth').val();
                        var gg = $(rdiv1).val();
                        var reset1 = parseInt(gg); //alert(gg);
                        var tt = hiremonth.split("-").reverse().join(" ");
                        var t = new Date(tt);

                        t.setDate(t.getDate() + reset1);
                        var month = "0" + (t.getMonth() + 1);
                        var date = "0" + t.getDate();
                        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                        const d = new Date();
                        month = month.slice(-2);
                        date = date.slice(-2);
                        var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                        if (reset1 == '') {
                            $(rdiv2).empty();
                        } else {
                            $(rdiv2).val(date);
                        }
                    });
                    x++;
                    z++;
                }
            });
            $(wrapper).on('click', '.ad', function (e) { //Once remove button is clicked
                e.preventDefault();
                $(wrapper).append(fieldHTML); //Remove field html
                x++;
                z++;
            });
            $(wrapper).on('click', '.remove', function (e) { //Once remove button is clicked
                e.preventDefault();
                $(this).parent().parent('.review-2').remove(); //Remove field html
                x--;
                z--; //Decrement field counter
            });
        });
    </script>

    <script>
        function additional_attach_update() {
            $('#file_1').click();
        }

        $('#file_1').change(function () {
            if ($(this).val() != '') {
                upload1(this);
            }
        });

        function upload1(img) {
            var form_data = new FormData();
            form_data.append('file_1', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading1').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/additional2')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image_1').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#file_name_1').val(data);
                        $('#preview_image_1').attr('src', '{{asset('public/uploads')}}/' + data);
                    }
                    $('#loading1').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image_1').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }

        function additional_attach_remove() {
            if ($('#file_name_1').val() != '')
                if (confirm('Are you sure want to remove profile picture?')) {
                    $('#loading1').css('display', 'block');
                    var form_data = new FormData();
                    form_data.append('_method', 'DELETE');
                    form_data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        url: "/clientemployee/additional3/" + $('#file_name_1').val(),
                        data: form_data,
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            $('#preview_image_1').attr('src', '{{asset('public/images/noimage.jpg')}}');
                            $('#file_name_1').val('');
                            $('#loading1').css('display', 'none');
                        },
                        error: function (xhr, status, error) {
                            alert(xhr.responseText);
                        }
                    });
                }
        }
    </script>
    <script>
        function additional_attach() {
            $('#file_2').click();
        }

        $('#file_2').change(function () {
            if ($(this).val() != '') {
                upload2(this);
            }
        });

        function upload2(img) {
            var form_data = new FormData();
            form_data.append('file_2', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/additional3')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image_2').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#file_name_2').val(data);
                        $('#preview_image_2').attr('src', '{{asset('public/uploads')}}/' + data);
                    }
                    $('#loading').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image_2').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }
    </script>

    <script>
        function pfid_upload() {
            $('#pfid1').click();
        }

        $('#pfid1').change(function () {
            if ($(this).val() != '') {
                upload3(this);
            }
        });

        function upload3(img) {
            var form_data = new FormData();
            form_data.append('pfid1', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading3').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/pfidd1')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image_3').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#pfid1_name').val(data);
                        $('#preview_image_3').attr('src', '{{asset('public/employeeProof1')}}/' + data);
                    }
                    $('#loading3').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image_3').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }

    </script>

    <script>
        function pfid2_upload() {
            $('#pfid2').click();
        }

        $('#pfid2').change(function () {
            if ($(this).val() != '') {
                upload4(this);
            }
        });

        function upload4(img) {
            var form_data = new FormData();
            form_data.append('pfid2', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#loading4').css('display', 'block');
            $.ajax({
                url: "{{url('/clientemployee/pfidd')}}/" + $('#user_id11').val(),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#preview_image_4').attr('src', '{{asset('public/images/noimage.jpg')}}');
                        alert(data.errors['file']);
                    } else {
                        $('#pfid2_name').val(data);
                        $('#preview_image_4').attr('src', '{{asset('public/employeeProof2')}}/' + data);
                    }
                    $('#loading4').css('display', 'none');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#preview_image_4').attr('src', '{{asset('public/images/noimage.jpg')}}');
                }
            });
        }
    </script>
    <script>
        $(document).ready(function () {
            var maxGroup = 120;
            $(".addMore").click(function () {
                if ($('body').find('.fieldGroup').length < maxGroup) {
                    var fieldHTML = '<div class="fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maximum ' + maxGroup + ' Persons are allowed.');
                }
            });
            $("body").on("click", ".remove", function () {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>

    <!----script---->
    @foreach($review1 as $re)
        <div id="myModal{{$re->id}}" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you want to delete this record ?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('review1.reviewdelete1',$re->id) }}" class="btn btn-danger">Delete</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @foreach($info as $in)
        <div id="myModal1{{$in->id}}" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you want to delete this record ?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('pay1.paydelete1',$in->id) }}" class="btn btn-danger">Delete</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <script>
        $("#efax").mask("(999) 999-9999");
        $(".ext").mask("99999");
        $("#ext1").mask("99999");
        $("#ext2").mask("99999");
        $("#eext1").mask("99999");
        $("#eext2").mask("99999");
        $("#mobile_no").mask("(999) 999-9999");
        $("#etelephone2").mask("(999) 999-9999");
        $("#etelephone1").mask("(999) 999-9999");
        $("#computer_ip").mask("999.999.999.999");
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                var id = $(this).val();
                $.get('{!!URL::to('/getbranchs')!!}?id=' + id, function (data) {
                    $('#branch_name').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#branch_name').val(subcatobj.branchname);
                    })
                });
            });
        });
    </script>
    <script>
        $('.date1').mask("99/99/9999", {placeholder: 'mm/dd/yyyy'});
    </script>
    <script>
        $(document).ready(function () {
            $('#reset').on('change', function () {
                var reset = parseInt($('#reset').val());
                var date = new Date();
                var t = new Date();
                t.setDate(t.getDate() + reset);
                var month = "0" + (t.getMonth() + 1);
                var date = "0" + t.getDate();
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                const d = new Date();
                month = month.slice(-2);
                date = date.slice(-2);
                var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                $('#reset_date').val(date);
            });
        });
    </script>
    <script>
        var date = $('#ext1').val({{$emp->ext1}});
        $('#telephoneNo1Type').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val({{$emp->ext1}});
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>

    <script>
        var dat1 = $('#ext2').val({{$emp->ext2}});
        $('#telephoneNo2Type').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val({{$emp->ext2}});
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })
    </script>

    <script>
        $('#eteletype2').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                $('#eext2').val({{$emp->eext2}});
                document.getElementById('eext2').removeAttribute('readonly');
            } else {
                document.getElementById('eext2').readOnly = true;
                $('#eext2').val('');
            }
        })
    </script>
    <script>
        var dat2 = $('#eext1').val({{$emp->eext1}});
        $('#eteletype1').on('change', function () {
            if (this.value == 'Office' || this.value == 'Work') {
                document.getElementById('eext1').removeAttribute('readonly');
                $('#eext1').val({{$emp->eext1}});
            } else {
                document.getElementById('eext1').readOnly = true;
                $('#eext1').val('');
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#first_rev_day').on('keyup', function () {
                var hiremonth = $('#hiremonth').val();
                var reset1 = parseInt($('#first_rev_day').val());
                var tt = hiremonth.split("-").reverse().join(" ");
                var t = new Date(tt);
                t.setDate(t.getDate() + reset1);
                var month = "0" + (t.getMonth() + 1);
                var date = "0" + t.getDate();
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                const d = new Date();
                month = month.slice(-2);
                date = date.slice(-2);
                var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                if (reset1 == '') {
                    $('#reviewmonth').empty();
                } else {
                    $('#reviewmonth').val(date);
                }
            });
        });

        $(document).ready(function () {
            $("select#check").change(function () {
                var selectedCountry = $(this).children("option:selected").val();
                var color = $("option:selected", this).attr("class");
                $("#check").attr("class", color).addClass("form-control1 fsc-input");
            });
        });
    </script>
    <div id="alertss" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="background-color: #ededed;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Save</h4>
                </div>
                <div class="modal-body">
                    <p>Employee Profile Successfully Saved.</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="alertss1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="background-color: #ededed;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Save</h4>
                </div>
                <div class="modal-body">
                    <p>Employee Profile not Successfully Saved.</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="alerts" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content model-width" style="background-color: #f3f3f3;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Save changes</h4>
                </div>
                <div class="modal-body">
                    <center><p>Do you want to save the changes ?</p></center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default primary1 primary" style="">Yes</button>
                    <button type="button" id="button" class="btn btn-default primary">No</button>
                    <a class="btn btn-default primary no-button">Cancel</a>
                </div>
            </div>

        </div>
    </div>
    <script>
        $('select').on('change', function () {
            $('#text1').val(this.value);
        });
        jQuery(function ($) {
            var input = $('input,textarea,select');
            input.on('keydown', function () {
                var key = event.keyCode || event.charCode || event.which;
                if (key == 8 || key == 46) {
                    $('#text1').val(key);
                } else {
                    $('#text1').val(key);
                }
            });
        });
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var currentTab = $(e.target).text(); // get current tab
                var vf = $('#text1').val();//alert(vf);
                var current_tab = e.target;
                var previousTab = $(e.relatedTarget).text();
                var target = $(e.relatedTarget).attr("href");
                $('.no-button').eq(0).attr('href', target);
                var href = $('.no-button').attr('href');
                if (target == href) {
                    if (vf) {// alert('2');
                        $("#alerts").modal({
                            show: true,
                        });
                    } else {
                    }
                } else {
                }
            });
        });

        $(function () {
            $('#login-form-link').click(function (e) {
                $("#login-form").delay(100).fadeIn(100);
                $("#register-form").fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
            $('#register-form-link').click(function (e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
        });

        $(document).on('click', '.no-button', function () {
            var href = $(this).attr('href');
            var $link = $('li.active a[data-toggle="tab"]');
            $link.parent().removeClass('active');
            var tabLink = $link.attr('href');//alert(tabLink);
            $('#alerts').modal('hide');
            $('#myTab a[href="' + href + '"]').tab('show');
        });
    </script>
    <script>
        $(function () {
            $('.primary1').click(function () {  //alert(newopt);
                $.ajax({
                    type: "post",
                    url: "{{ route('clientprofile.update',Auth::user()->id)}}",
                    dataType: "json",
                    data: $('#registrationForm').serialize(),
                    success: function (data) {
                        $('#alerts').modal('hide');
                        $('#alertss').modal('show');
                        $('#text1').val('');
                    },
                    error: function (data) {
                        $('#alerts').modal('hide');
                        $('#alertss1').modal('show');
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#button").click(function () {
                location.reload(true);
            });
        });
        $(".date1").datepicker({
            autoclose: true,
            format: "mm/dd/yyyy",
            //endDate: "today"
        });
    </script>
@endsection()