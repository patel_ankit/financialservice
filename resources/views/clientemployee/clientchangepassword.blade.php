@extends('clientemployee.layouts.app')
@section('title', 'Change Password')
@section('main-content')
    @if(Auth::user()->flag >='3')
        <style>
            .card {
                pointer-events: none;
            }

            .field-icon {
                margin-top: 2px;
            }
        </style>
    @endif
    <style>
        .field-icon {
            margin-top: 2px;
        }
    </style>
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Change Password</h1>
        </div>
        <section class="content" style="background-color: #fff;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        @if ( session()->has('success') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                        @endif
                        @if ( session()->has('error') )
                            <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                        @endif
                        @if(Auth::user()->flag >='3')
                            <div class="alert alert-danger">Change Password After 30 Minutes</div>
                        @endif
                        <div class="card-body">
                            <form enctype='multipart/form-data' class="form-horizontal changepassword" action="{{route('clientchangepassword.update',Auth::user()->id)}}" id="changepassword" method="post">
                                {{csrf_field()}}  {{method_field('PATCH')}}
                                <div class="">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                            <label class="control-label col-md-3">Old Password :</label>
                                            <div class="col-md-4">
                                                <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                                <input type="hidden" class="form-control" id="flag" value="@if(empty(Auth::user()->flag)) 1 @else {{Auth::user()->flag+1}} @endif" name="flag">
                                                @if ($errors->has('oldpassword'))
                                                    <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control" id="" value="{{ Auth::user()->email}}" name="email">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">New Password :</label>
                                            <div class="col-md-4"><span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                <input name="newpassword" type="password" id="newpassword" class="form-control"/>

                                                <div id="messages"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Confirm Password :</label>
                                            <div class="col-md-4">
                                                <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                                            <label class="control-label col-md-3">Reset Days :</label>
                                            <div class="col-md-2">
                                                <select name="resetdays" value="" id="resetdays" class="form-control">
                                                    @if(empty(Auth::user()->resetdays))
                                                        <option>---Reset Days---</option>
                                                    @endif
                                                    <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                                    <option value="45" @if(Auth::user()->resetdays=='45') selected @endif>45</option>
                                                    <option value="60" @if(Auth::user()->resetdays=='60') selected @endif>60</option>
                                                    <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                                    <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                                                </select>
                                                @if ($errors->has('resetdays'))
                                                    <span class="help-block">
                              <strong>{{ $errors->first('resetdays') }}</strong>
                              </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2">
                                                <input name="resetdate" type="text" id="reset_date" value="{{Auth::user()->enddate}}" readonly class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-offset-3 col-md-7">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                            </div>
                                            <div class="col-md-3">
                                                <a class="btn_new_cancel" href="{{url('clientemployee')}}">Cancel</a>
                                            </div>
                                            <div class="col-md-3">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {

            $('.changepassword').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    newpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            regexp:
                                {

                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                    message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                },

                            different: {
                                field: 'oldpassword',
                                message: 'The password cannot be the same as Current Password'
                            }
                        }
                    },
                    oldpassword: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Current Password'
                            },
                            remote: {
                                message: 'The Password is not available',
                                url: '{{ URL::to('clientemployee/clientchangepass') }}',
                                data: {
                                    type: 'oldpassword'
                                },
                                type: 'POST'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and can\'t be empty'
                            },
                            identical: {
                                field: 'newpassword',
                                message: 'The password and its confirm are not the same'
                            },
                            different: {
                                field: 'oldpassword',
                                message: 'The password can\'t be the same as Old Password'
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                $('.changepassword').data('bootstrapValidator').resetForm();
                // Prevent form submission
                e.preventDefault();
                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    // console.log(result);
                }, 'json');
            });
        });


        $(document).ready(function () {
            $('#resetdays').on('change', function () {
                var reset = parseInt($('#resetdays').val());
                var date = new Date();
                var t = new Date();
                t.setDate(t.getDate() + reset);
                var month = "0" + (t.getMonth() + 1);
                var date = "0" + t.getDate();
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                const d = new Date();
                month = month.slice(-2);
                date = date.slice(-2);
                //var date = date +"/"+month+"/"+t.getFullYear();
                var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                $('#reset_date').val(date);
            });
        });
    </script>
@endsection()