@extends('clientemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of County</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                                <div class="table-title">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>State</th>
                                        <th>County</th>
                                        <th>County Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($price as $bus)
                                        <tr>
                                            <td style="text-align:center;">{{$loop->index + 1}}</td>
                                            <td style="text-align:center;">{{$bus->state}}</td>
                                            <td style="text-align:center;">{{$bus->county}}</td>
                                            <td style="text-align:center;">{{$bus->countycode}}</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('empsstates.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('empsstates.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">{{csrf_field()}} {{method_field('DELETE')}}</form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()