@extends('clientemployee.layouts.app')
@section('main-content')
    <style>
        .gif {
            width: 50px;
            position: absolute;
            top: -14px;
            left: 64px;
        }

        .modal-body {
            width: 100%;
        }

        @charset "utf-8";
        .lunch-clock-time-head {
            width: 100%;
            float: left;
            background: #286DB5;
        }

        .btn.focus, .btn:focus, .btn:hover {
            color: #1a1a1a;
            text-decoration: none;
            transition: 0.3s;
        }

        .lunch-clock-time {
            width: 100%;
            float: left;
            background: #4a95e4;
            padding: 10px 10px;
            text-align: center;
        }

        .btn-danger {
            border: 1px solid #222;
        }

        .btn-success {
            border: 1px solid #222;
        }

        .input-group {
            position: relative;
            display: inline;
            border-collapse: separate;
        }

        .lunch-clock-time-head h2 {
            font-size: 18px;
            color: #fff;
            box-shadow: -1px 4px 8px rgba(0, 0, 0, 0.1);
            margin: 0;
            padding: 12px 0;
            text-align: center;
        }

        .lunch-clock-time-text {
            width: 100%;
            float: left;
        }

        .lunch-clock-schedule {
            width: 100%;
            float: left;
            background: #FFF;
            padding: 6px 10px;
            margin: 14px 0;
        }

        .lunch-clock-schedule h3 {
            font-size: 16px;
            color: #4b77be;
            line-height: 24px;
            margin: 5px 0 10px 0;
            text-align: center;
        }

        .lunch-clock-schedule-left {
            width: 50%;
            float: left;
        }

        .info-box {
            border-radius: 10px;
        }

        .lunch-clock-time p {
            font-size: 15px;
            line-height: 28px;
            text-align: center;
            margin: 4px 0;
        }

        .lunch-clock-schedule-right {
            width: 50%;
            float: left;
        }

        .lunch-clock-time-buttons {
            width: 100%;
            float: left;
        }

        .lunch-clock-time-buttons {
            width: 100%;
            float: left;
        }

        .lunch-clock-time-buttons button:disabled {
            display: none;
        }

        .lunch-clock-time-buttons button {
            width: 150px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button.lun-btn-out {
            background: #e61212;
        }

        .lunch-clock-time-buttons button.note-btn {
            background: #e4914a;
            margin: 10px 0;
        }

        .lunch-clock-time-buttons button {
            width: 150px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button {
            width: 132px;
            margin: 0 10px 0 0;
            padding: 6px 0 !important;
            font-size: 15px;
            color: #fff;
            text-transform: uppercase;
        }

        .lunch-clock-time-buttons button.lun-btn-in {
            background: #0ba20b;
        }

        .icon-btn {
            width: 100%;
        }

        .week {
            display: none
        }

        .info-box-content {
            color: #fff !important;
        }
    </style>


    <div class="content-wrapper">
        <div class="page-title">
            <h1>Dashboard </h1>
            @if(empty($timer->week_code))
                <?php
                $date = date('Y-m-d');
                $weekendDay = false;
                $day = date("D", strtotime($date));
                if ($day == 'Sat' || $day == 'Sun') {
                    $weekendDay = true;
                }
                if($weekendDay){?>
                <style>
                    .timing {
                        pointer-events: none;
                    }

                    .week {
                        display: block !important;
                        background: #103B68;
                        color: #fff;
                        padding: 10px 0;
                        font-size: 13px;
                        font-weight: bold;
                    }
                </style>
                <?php } else {
                    // echo $date . ' falls on a weekday.';
                }
                ?>
            @endif
        </div>
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#00c1ec !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>
                                <div class="info-box-content"> Task <span class="info-box-number">
@if(empty($tk->checked))
                                        @else
                                            @if($tk->checked=='1')
                                                <div class="gif">
<img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
</div>
                                            @else
                                            @endif
                                        @endif
                                        @if(empty($tk->emptotal))
                                            0
                                        @else
                                            <a href="{{url('fscemployee/newtask')}}"> {{$tk->emptotal}}</a>
                                        @endif
               
                  <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
                                <div class="info-box-content"> Message <span class="info-box-number">
@foreach($msg as $ss)
                                            @if(empty($ss->recieve))
                                            @else
                                                @if($ss->recieve=='1')
                                                    <div class="gif">
<img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
</div>
                                                @else
                                                @endif
                                            @endif
                                            @if(empty($ss->msgtotal))
                                                <h2>0</h2>
                                            @else
                                                <a href="{{url('fscemployee/sendmessage')}}"> {{$ss->msgtotal}}</a>
                                                <style>.dd {
                                                        display: none
                                                    }</style>
                                            @endif
                                        @endforeach
                 <a href="{{url('fscemployee/sendmessage')}}" class="dd"> 0</a>
                  <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>


                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#71af8a !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-file-o"></i></span>
                                <div class="info-box-content">Responsiblity / Rules <span class="info-box-number">
@if(empty($rules))
                                        @else
                                            @if($rules)
                                                <div class="gif">
<img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
</div>
                                            @else
                                            @endif
                                        @endif
                                        @if(empty($rules))
                                            <a>0</a>
                                        @else
                                            <a href="{{url('fscemployee/employeeprofile')}}?tab=1" style="color:#fff !important;"> {{$rules}}</a>
                                        @endif
                  <small></small>
                  </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#71af8a !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>
                                <div class="info-box-content"> Sample-1<span class="info-box-number">
<div class="gif">
<img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
</div>
                                   <a href="https://financialservicecenter.net/fscemployee/newtask"> 1</a>
                              
                  <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box" style="background:#00c1ec !important;">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
                                <div class="info-box-content"> Sample-2 <span class="info-box-number">
                
           <div class="gif">
<img src="https://financialservicecenter.net/public/dashboard/images/newimage.gif" alt="" class="img-responsive">
</div>
                 <a href="https://financialservicecenter.net/fscemployee/sendmessage"> 3</a>
                 <style>.dd {
                         display: none
                     }</style>
                         <a href="https://financialservicecenter.net/fscemployee/sendmessage" class="dd"> 0</a>
                  <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>


                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-file-o"></i></span>
                                <div class="info-box-content"> Sample-3<span class="info-box-number">
                   <a>0</a> <small></small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-12 col-sm-6 col-xs-12 employee" style="text-align:right;">
                    <div class="col-md-6"></div>
                    <div class="timing col-md-6" style="padding-right:0px;">
                        <div class="col-md-12 lunch-clock-time" style="border: 5px solid #fff;border-radius: 20px;">
                            <div class="lunch-clock-time-head">
                                <h2>Time Log
                                    <?php
                                    function time_to_decimal($time)
                                    {
                                        $timeArr = explode(':', $time);
                                        $decTime = (($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60)) / 60;
                                        return $decTime;
                                    }
                                    $sum = 0;
                                    foreach ($decimal1 as $del) {
                                        $total = date("H:i:s", strtotime($del->total));
                                        if ($del->breaktime == NULL) {
                                            $breaktime = '00:00:00';
                                        } else {
                                            $breaktime = date("H:i:s", strtotime($del->breaktime));
                                        }
                                        if ($del->breaktime1 == NULL) {
                                            $breaktime1 = '00:00:00';
                                        } else {
                                            $breaktime1 = date("H:i:s", strtotime($del->breaktime1));
                                        }
                                        $all = time_to_decimal($total) - (time_to_decimal($breaktime) + time_to_decimal($breaktime1));
                                        $all;
                                        $sum += $all;
                                    }
                                    if (empty($emp_schedule->emp_out)) {
                                        $sum1 = $sum + $currenttime;
                                    } else {
                                        $sum1 = $sum;
                                    }
                                    ?>
                                    <span style="float:right;padding-right:10px">Total Hours <?php echo round($sum1, 2);?></span></h2>
                            </div>
                            <div class="lunch-clock-time-text">
                                <div class="lunch-clock-schedule">
                                    <h3><span>Schedule Time :</span> @if(empty($empschedule->clockin))<span>00:00 PM To 00:00 PM</span>@else <span>{{$empschedule->clockin}} To {{$empschedule->clockout}}</span> @endif</h3>
                                    <div class="week">Today is Week day. If you Time start then Contact Your Administration</div>
                                    <div class="lunch-clock-schedule-left">
                                        <p><span>Time In : @if(empty($emp_schedule->emp_in)) 00:00 @else {{date("h:i A",strtotime($emp_schedule->emp_in))}} @endif</span></p>
                                        @if(empty($emp_schedule->launch_in)) @else
                                            <p><span>Breack In : {{date("h:i A",strtotime($emp_schedule->launch_in))}} </span></p>
                                        @endif
                                        @if(empty($emp_schedule->launch_in_second)) @else
                                            <p><span>Breack In Second: {{date("h:i A",strtotime($emp_schedule->launch_in_second))}} </span></p>
                                        @endif
                                    </div>
                                    <div class="lunch-clock-schedule-right">
                                        <p><span>Time Out : @if(empty($emp_schedule->emp_out)) 00:00 @else {{date("h:i A",strtotime($emp_schedule->emp_out))}} @endif</span></p>
                                        @if(empty($emp_schedule->launch_out)) @else
                                            <p><span>Break Out : {{date("h:i A",strtotime($emp_schedule->launch_out))}} </span></p>
                                        @endif
                                        @if(empty($emp_schedule->launch_out_second)) @else
                                            <p><span>Breack Out Second: {{date("h:i A",strtotime($emp_schedule->launch_out_second))}} </span></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="lunch-clock-time-buttons">
                                <div class="lunch-clock-time-buttons">
                                    @if(empty($emp_schedule->emp_in))
                                        @if(empty($empschedule->clockin))
                                        @else
                                            <?php //echo date('h:i A');
                                            $newtime = strtotime(date('h:i A'));
                                            $starttime = strtotime($empschedule->clockin) - (15 * 60);
                                            $date = date('h:i A', $starttime);
                                            $st = strtotime($date);
                                            $starttime1 = strtotime($empschedule->clockin) + (15 * 60);
                                            $date1 = date('h:i A', $starttime1);
                                            $st1 = strtotime($date1);
                                            ?>
                                        @endif
                                        <div class="col-md-6">
                                            <form method="post" action="clientemployee" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="emp_in_date" value="{{date('Y-m-d')}}">
                                                <input type="hidden" name="emp_in" value="{{date('Y-m-d H:i:m')}}">
                                                <input type="hidden" name="employee_id" value="{{Auth::user()->user_id}}">
                                                @if(empty($empschedule->clockin))
                                                    <a class="btn lun-btn-out btn-success icon-btn" href="#" data-toggle="modal" data-target="#myModal4">Start</a>
                                                @else
                                                    @if($st < $newtime)
                                                        @if($st1 < $newtime)
                                                            @if(! empty($timer->after_code))
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                            @else
                                                                <a class="btn lun-btn-out btn-success icon-btn" href="#" data-toggle="modal" data-target="#myModal2">Start</a>
                                                                <!--<input class="btn btn-success icon-btn" type="submit" name="submit" value="Start"> -->
                                                            @endif
                                                        @else
                                                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                        @endif
                                                    @else
                                                        @if(! empty($timer->after_code))
                                                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
                                                        @else
                                                            <a class="btn lun-btn-out btn-success icon-btn" href="#" data-toggle="modal" data-target="#myModal2">Start</a>
                                                            <!--<input class="btn btn-success icon-btn" type="submit" name="submit" value="Start2"> -->
                                                        @endif
                                                    @endif
                                                @endif
                                            </form>
                                        </div>
                                    @else
                                        @if(empty($emp_schedule->launch_in))
                                            <div class="col-md-6">
                                                <form method="post" action="{{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}}" class="gg" enctype="multipart/form-data">
                                                    {{csrf_field()}}{{method_field('PATCH')}}
                                                    <input type="hidden" name="launch_in" value="{{date('Y-m-d H:i:s')}}">
                                                    <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH IN">
                                                </form>
                                            </div>
                                        @else
                                            @if($emp_schedule->launch_out)
                                                @if($emp_schedule->launch_in_second)
                                                    @if($emp_schedule->launch_out_second)
                                                    @else
                                                        <div class="col-md-6">
                                                            <form method="post" action="{{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}}" class="" enctype="multipart/form-data">
                                                                {{csrf_field()}}{{method_field('PATCH')}}
                                                                <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                                <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                                                                <input type="hidden" name="launch_in_second" value="{{$emp_schedule->launch_in_second}}">
                                                                <input type="hidden" name="launch_out_second" value="{{date('Y-m-d H:i:s')}}">
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH OUT SECOND">
                                                            </form>
                                                        </div>
                                                    @endif
                                                @else
                                                    @if(empty($emp_schedule->emp_out))
                                                        <div class="col-md-6">
                                                            <form method="post" action="{{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}}" class="" enctype="multipart/form-data">
                                                                {{csrf_field()}}{{method_field('PATCH')}}
                                                                <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                                <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                                                                <input type="hidden" name="launch_in_second" value="{{date('Y-m-d H:i:s')}}">
                                                                <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH IN SECOND">
                                                            </form>
                                                        </div>
                                                    @endif @endif

                                            @else
                                                <div class="col-md-6">
                                                    <form method="post" action="{{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}}" class="" enctype="multipart/form-data">
                                                        {{csrf_field()}}{{method_field('PATCH')}}
                                                        <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                                                        <input type="hidden" name="launch_out" value="{{date('Y-m-d H:i:s')}}">
                                                        <input class="btn btn-success icon-btn" type="submit" name="submit" value="LUNCH OUT">
                                                    </form>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                    @if(empty($emp_schedule->emp_out))
                                        <div class="col-md-6">
                                            @if(empty($emp_schedule->launch_in))
                                                @if(empty($emp_schedule->emp_in))
                                                    <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                @else
                                                    <?php
                                                    $newtime = strtotime(date('h:i A'));
                                                    $starttime2 = strtotime($empschedule->clockout) - (15 * 60);
                                                    $date2 = date('h:i A', $starttime2);
                                                    $st2 = strtotime($date2);
                                                    $starttime3 = strtotime($empschedule->clockout) + (15 * 60);
                                                    $date3 = date('h:i A', $starttime3);
                                                    $st3 = strtotime($date3);
                                                    ?>
                                                    @if($st2 < $newtime)
                                                        @if($st3 < $newtime)
                                                            @if(! empty($timer->before_code))
                                                                <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                            @else
                                                                <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">End</a>
                                                            @endif
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                        @endif
                                                    @else
                                                        @if(! empty($timer->before_code))
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">End</a>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else
                                                @if($emp_schedule->launch_out)
                                                    <?php
                                                    $newtime = strtotime(date('h:i A'));
                                                    $starttime2 = strtotime($empschedule->clockout) - (15 * 60);
                                                    $date2 = date('h:i A', $starttime2);
                                                    $st2 = strtotime($date2);
                                                    $starttime3 = strtotime($empschedule->clockout) + (15 * 60);
                                                    $date3 = date('h:i A', $starttime3);
                                                    $st3 = strtotime($date3);
                                                    ?>
                                                    @if($st2 < $newtime)
                                                        @if($st3 < $newtime)
                                                            @if(! empty($timer->before_code))
                                                                <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                            @else
                                                                <a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">End</a>
                                                            @endif
                                                        @else
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                        @endif
                                                    @else
                                                        @if(! empty($timer->before_code))
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                        @else
                                                        <!--<a class="btn lun-btn-out btn-danger icon-btn" href="#" data-toggle="modal" data-target="#myModalbefore">End</a>-->
                                                            <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal">End</a>
                                                        @endif
                                                    @endif
                                                @else
                                                    <a class="btn lun-btn-out btn-danger icon-btn " href="#" data-toggle="modal" data-target="#myModal3">End</a>
                                                @endif
                                            @endif
                                        </div>
                                    @else
                                        <style>
                                            .gg {
                                                display: none;
                                            }
                                        </style>
                                    @endif
                                    @if(empty($emp_schedule->launch_out))
                                        <div class="col-md-12" style="padding:15px;">
                                            @else
                                                <div class="col-md-6">
                                                    @endif
                                                    <a class="btn note-btn btn-primary" href="#" data-toggle="modal" style="width: 100%;border: 1px solid #222;background:#ffcc00;color:#222;" data-target="#myModal1">Note</a>
                                                </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.info-box -->
                    </div>
                </div>
        </section>
    </div>
    @if(empty($emp_schedule->emp_in))
    @else
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Clock Out</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}}" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                            <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                            <input type="hidden" name="emp_out" value="{{date('Y-m-d H:i:s')}}">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Employee Code :</label>
                                <input type="password" class="form-control" value="{{$employee->employee_id}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Work :</label>
                                <textarea class="form-control" required name="work"></textarea>
                            </div>
                            <input class="btn btn-success icon-btn" type="submit" name="submit" value="End">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Note</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="@if(empty($emp_schedule->emp_in)) clientemployee @else {{ URL::to('clientemployee/home') }}/{{$emp_schedule->id}} @endif" class="" id="businessname" name="businessname" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if(empty($emp_schedule->emp_in))
                            <input type="hidden" name="emp_in_date" value="{{date('m-d-Y')}}">
                            <input type="hidden" class="form-control" value="{{Auth::user()->user_id}}" name="employee_id">
                        @else
                            {{method_field('PATCH')}}
                            <input type="hidden" name="launch_in" value="{{$emp_schedule->launch_in}}">
                            <input type="hidden" name="launch_out" value="{{$emp_schedule->launch_out}}">
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Employee Code :</label>
                            @if(!empty($employee->employee_id))
                                <input type="password" class="form-control" value="{{$employee->employee_id}}">
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Note :</label>
                            <textarea class="form-control" id="txtEditor" cols="55" name="note"></textarea>
                            <input type="hidden" class="form-control" value="note1" name="note1">
                        </div>
                        <input class="btn btn-success icon-btn" type="submit" name="submit" value="Note">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Start Timer</h4>
                </div>
                <form action="@if(empty($timer->id)) clientemployee @else {{ URL::to('clientemployee/home') }}/{{$timer->id}} @endif" method="post" id="superid">
                    <div class="modal-body">
                        <p id="sk" style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">You can clock in 15 minute before your schedule time. You are early .You can log in your schedule time</p>
                        <div class="col-md-12" style="display:none" id="rk">
                            <div class="col-md-12">
                                {{ csrf_field() }} @if(empty($timer->id)) @else {{method_field('PATCH')}} @endif
                                <div class="form-group has-feedback">
                                    <label for="password" class="col-md-4 control-label">Supervisor Name :</label>
                                    <div class="col-md-8  inputGroupContainer">

                                        @if(isset($emp22->id)!='' && isset($emp22->id)!=null)
                                            <div class="input-group">

                                                <input type="hidden" class="form-control" name="supervisorname" id="supervisorname" value="{{$emp22->id}}" readonly>
                                                <input type="text" class="form-control" value="{{$emp22->first_name.' '.$emp22->last_name}}" readonly>


                                                <span class="glyphicon form-control-feedback"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-4 control-label">Supervisor Code :</label>
                                    <div class="col-md-8  inputGroupContainer">
                                        <div class="input-group">
                                            <input type="password" class="form-control" required name="code" placeholder="Please Enter Your Code">
                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" id="sp">Contact Supervisor</a>
                        <input type="submit" value="Supervisor" class="btn btn-primary pull-right" style="display:none" id="rk1">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalbefore" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Start Timer</h4>
                </div>
                <form action="@if(empty($timer->id))  clientemployee  @else {{ URL::to('clientemployee/home') }}/{{$timer->id}}  @endif" method="post" id="superid1">
                    <div class="modal-body">
                        <p id="skbefore" style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">You can clock in 15 minute before your schedule time. You are early .You can log in your schedule time</p>


                        <div class="col-md-12" style="display:none" id="rk1before">
                            <div class="col-md-12">

                                {{ csrf_field() }} @if(empty($timer->id)) @else {{method_field('PATCH')}} @endif
                                <div class="form-group has-feedback">
                                    <label for="password" class="col-md-4 control-label">
                                        Supervisor Name :
                                    </label>
                                    <div class="col-md-8  inputGroupContainer">
                                        @if(isset($emp22->id)!='' && isset($emp22->id)!=null)
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" name="supervisorname" id="supervisorname" value="{{$emp22->id}}" readonly>
                                                <input type="text" class="form-control" value="{{$emp22->first_name.' '.$emp22->last_name}}" readonly>
                                                <span class="glyphicon form-control-feedback"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group has-feedback">
                                    <label for="confirmPassword" class="col-md-4 control-label">Supervisor Code :</label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <input type="password" class="form-control" required name="code1" placeholder="Please Enter Your Code">
                                            <span class="glyphicon form-control-feedback"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" id="before">Contact Supervisor</a>
                        <input type="submit" value="Supervisor" class="btn btn-primary pull-right" style="display:none" id="rkbefore">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lunch Out</h4>
                </div>
                <div class="modal-body">
                    <p style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">Before end you must have to Lunch out</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal4" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lunch Out</h4>
                </div>
                <div class="modal-body">
                    <p style="background-color: #F36A5A !important; padding:10px; width:100%;color:#fff">Please Schedule Set</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#sp").click(function () {
                $("#sp").hide();
                $("#sk").hide();
                $("#rk").show();
                $("#rk1").show();
            });


            $("#before").click(function () {
                $("#before").hide();
                $("#skbefore").hide();
                $("#rk1before").show();
                $("#rkbefore").show();
            });
        });
    </script>
    @if(Auth::user()->active==1 )
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
    @endif
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });

        $(document).ready(function () {
            $('#superid').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    code: {
                        validators: {

                            notEmpty: {
                                message: 'Please Enter Supervisor Code'
                            },
                            remote: {
                                type: 'POST',
                                url: '{{ URL::to('clientemployee/superstart2') }}',
                                message: 'The  Supervisor Code not available',
                                delay: 2000
                            }

                        }
                    },


                }
            })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#superid').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        console.log(result);
                    }, 'json');
                });
        });


        $(document).ready(function () {
            $('#superid1').bootstrapValidator({

                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    code1: {
                        validators: {

                            notEmpty: {
                                message: 'Please Enter Supervisor Code'
                            },
                            remote: {
                                type: 'POST',
                                url: '{{ URL::to('clientemployee/superend2') }}',
                                message: 'The  Supervisor Code not available',
                                delay: 2000
                            }

                        }
                    },


                }
            })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#superid1').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        console.log(result);
                    }, 'json');
                });
        });


    </script>

    <?php
    if(empty($remainder->emp_out))
    {
    if(isset($remainder->remainder_date))
    {
    $remday = $remainder->remainder_date;
    $rem3 = date('Y-m-d', strtotime('-3 days', strtotime($remday)));
    $rem2 = date('Y-m-d', strtotime('-2 days', strtotime($remday)));
    $rem1 = date('Y-m-d', strtotime('-1 days', strtotime($remday)));
    $rem4 = date('Y-m-d', strtotime('-0 days', strtotime($remday)));
    $beforethreeday = strtotime($rem3);
    $enddate = strtotime($remday);
    $current = date('Y-m-d');
    if($current == $rem3 || $current == $rem2 || $current == $rem1)
    {
    ?>
    @if(session()->has('success'))
        <script>
            $(window).load(function () {
                $('#myModalremainder').modal('show');
            });
        </script>
    @endif
    <div id="myModalremainder" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Clock Out Remainder</h4>
                </div>
                <div class="modal-body">
                    <p>You did not clock out previously <br> Please contact your Admin to clock out then after you can clock in.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php
    }
    else
    {
    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#myModalremainder").modal({
                backdrop: 'static',
                keyword: 'false',
                show: true,
            });
        });
    </script>
    <div id="myModalremainder" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Clock Out Remainder</h4>
                </div>
                <div class="modal-body">
                    <p>You did not clock out previously <br> Please contact your Admin to clock out then after you can clock in.</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('clientemployee.logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" class="btn btn-default"><i class="fa fa-sign-out sidebar-icon"></i><span>Log Out</span></a>
                    <form id="logout-form" action="{{ route('clientemployee.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>

        </div>
    </div>
    <?php }}}
    ?>
@endsection