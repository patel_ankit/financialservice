@extends('clientemployee.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Add New Account Code </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('accountcode.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group {{ $errors->has('accountcode') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Account Code : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <select class="js-example-tags form-control" id="accountcode" name="accountcode">
                                            <option value="">Select</option>
                                            @foreach($accountcode as $cur)
                                                <option value="{{$cur->accountcode}}">{{$cur->accountcode}}</option>
                                            @endforeach
                                        </select>


                                        @if ($errors->has('accountcode'))
                                            <span class="help-block">
											<strong>{{ $errors->first('accountcode') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('account_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Account Name : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="account_name" type="text" id="account_name" class="form-control" value=""/>
                                        @if ($errors->has('account_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('account_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('account_belongs_to') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Account Belongs To: <span class="star-required">*</span></label>
                                    <div class="col-md-4">

                                        <select class="form-control" name="account_belongs_to" id="account_belongs_to">
                                            <option value="">Select</option>
                                            <option value="Cost of Sales">Cost of Sales</option>
                                            <option value="Expenses">Expenses</option>
                                            <option value="Cash and Bank Balance">Cash and Bank Balance</option>
                                            <option value="Accounts Payable">Accounts Payable</option>
                                            <option value="Account Receivable">Account Receivable</option>
                                            <option value="Other Assets">Other Assets</option>
                                            <option value="Income">Income</option>
                                            <option value="Inventory">Inventory</option>
                                            <option value="Long Term Liabilities">Long Term Liabilities</option>
                                            <option value="Other Current Assets">Other Current Assets</option>
                                            <option value="Other Current Liabilities">Other Current Liabilities</option>
                                            <option value="Fixed Assets">Fixed Assets</option>
                                        </select>

                                        @if ($errors->has('account_belongs_to'))
                                            <span class="help-block">
											<strong>{{ $errors->first('account_belongs_to') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/accountcode')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 16px !important;
            color: #000;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-color: #000 transparent transparent transparent;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 6px;
            right: 4px;
        }

        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            padding-left: 0px !important;
            padding-top: 2px !important;

        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            /* border: 1px solid #aaa; */
            border-radius: 4px;
            border: 2px solid #2fa6f2;
            height: 40px;
        }</style>
    <script>
        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });

    </script>
@endsection()