@extends('clientemployee.layouts.app')
@section('title', 'Edit Leave')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Edit Leave</h1>
        </div>
        <section class="content" style="background-color: #fff;">
            <div class="">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('leave.update',$leave->id)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Leave Date :</label>
                                    <div class="col-md-3">
                                        <input type="text" name="start_date" id="start_date" value="{{$leave->start_date}}" class="form-control fsc-input">
                                        @if ($errors->has('start_date'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('total_days') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Total Days :</label>
                                    <div class="col-md-3">
                                        <input type="text" name="total_days" id="total_days" value="{{$leave->total_days}}" class="form-control fsc-input">
                                        <input type="hidden" value="{{ Auth::user()->id}}" name="employee_id" id="employee_id" class="form-control fsc-input">
                                        @if ($errors->has('total_days'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('total_days') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Leave End Date :</label>
                                    <div class="col-md-3">
                                        <input type="text" name="end_date" id="end_date" value="{{$leave->end_date}}" readonly class="form-control fsc-input">
                                        @if ($errors->has('end_date'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('leave_reason') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Leave Reason :</label>
                                    <div class="col-md-8">
                                        <textarea id="editor1" name="leave_reason" rows="10" cols="80">{!!$leave->leave_reason!!}</textarea>
                                        @if ($errors->has('leave_reason'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('leave_reason') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="col-md-offset-3 col-md-7">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input class="btn_new_save btn-primary1 primary1" style="margin-left:-6%" type="submit" name="submit" value="Save">
                                            </div>
                                            <div class="col-md-3">
                                                <a class="btn_new_cancel" style="margin-left:-6%" href="{{url('fscemployee/leave')}}">Cancel</a>
                                            </div>
                                            <div class="col-md-3">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function () {
            var dateInput = $('input[name="start_date"]');
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
                format: 'M-dd-yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: truncateDate(new Date())
            });

            $('#start_date').datepicker('setStartDate', truncateDate(new Date()));
        });

        function truncateDate(date) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }

        $(document).ready(function () {
            $("#total_days").on("keyup", function () {
                var dateInput = $('#start_date').val();
                // var day = ;
                var dd22 = $('#total_days').val();
                var dd221 = dd22 - 1; //alert(dd221);
                var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var daym = ['01', '02', '03', '04', '05', '06', '07', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

                var dd1 = dateInput.split('-');
                dateMonth = dd1[0],
                    dateDay = dd1[1],
                    dateYear = dd1[2];
                var dateInput1 = dateDay + '/' + dateMonth + '/' + dateYear;
                var date = new Date(dateInput1);
                var newdate = new Date(date);//alert(days);
                newdate.setDate(newdate.getDate() + parseInt(dd221));
                var dd = newdate.getDate();
                var d1 = parseInt(dd);
                var mm = newdate.getMonth();
                var mo = month[mm];
                var y = newdate.getFullYear();
                var someFormattedDate = mo + '-' + dd + '-' + y;
                document.getElementById('end_date').value = someFormattedDate;
            });
        });


    </script>
@endsection()