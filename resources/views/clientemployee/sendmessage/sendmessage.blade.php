@extends('clientemployee.layouts.app')
@section('title', 'Message')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>Message Logsheet (Inbox)</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <section class="content" style="background-color: #fff;">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-title" style="width: 100%;display: inline-block;">
                                <a href="{{url('fscemployee/sendmessage/create')}}">Add New Message</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Priority</th>
                                        <th>Date</th>
                                        <th>Message From</th>
                                        <th>Call Purpose</th>
                                        <th>For Whom?</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($task as $com)
                                        @if($com->status1=='Done') @else
                                            <tr>
                                                <td style="text-align:center">{{$loop->index+1}}</td>
                                                <td> {{$com->call_back}}@if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
                                                <td style="text-align:center">{{$com->date}}<br> {{$com->day}} <br>{{$com->time}}</td>
                                                <td>@foreach($admin1 as $com2) @if($com2->id==$com->employeeid) {{ucwords($com2->fname)}} {{ucwords($com2->mname)}} {{ucwords($com2->lname)}} @endif @endforeach @foreach($emp as $com2) @if($com2->id==$com->employeeid) {{ucwords($com2->firstName)}} {{ucwords($com2->middleName)}} {{ucwords($com2->lastName)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif</td>
                                                <td>{!!$com->purpose!!}</td>
                                                <td>@foreach($emp as $com2) @if($com2->id==$com->admin_id) {{ucwords($com2->firstName)}} {{ucwords($com2->middleName)}} {{ucwords($com2->lastName)}} @endif @endforeach</td>
                                                <td style="text-align:center"><a class="btn-action btn-view-edit" href="{{route('sendmessage.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
                                                    <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                            {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('sendmessage.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                                    <form action="{{ route('sendmessage.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                    </form>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>.table > thead > tr > th {
            background: #ffff99;
            border-bottom: 8px solid #993366;
            text-align: center;
        }</style>
@endsection()