@extends('clientemployee.layouts.app')
@section('title', 'Resposibilty')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Resposibilty</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form method="post" action="{{route('responsibility.respon',$position->id)}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
                            {{csrf_field()}} {{method_field('PATCH')}}
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Title :</label>
                                <div class="col-md-8">
                                    <select name="type" type="type" id="type" class="form-control">
                                        <option value="Resposibilty">Resposibilty</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} emp">
                                <label class="control-label col-md-3">Employee :</label>
                                <div class="col-md-8">
                                    <select name="employee_id" id="employee_id" class="form-control">
                                        <option value="{!!$position->employee_id!!}">{{Auth::user()->name}}</option>

                                    </select>

                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Title :</label>
                                <div class="col-md-8">
                                    <input name="title" type="text" id="title" value="{{$position->title}}" class="form-control">

                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Rules / Resposibilty :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <textarea id="editor1" name="rules" rows="10" cols="80">{!!$position->rules!!}</textarea>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-8">
                                    <div class="">
                                        <label><input id="checked" type="checkbox" name="checked" value="2" @if($position->status=='2') checked @endif> Click Here</label>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection()