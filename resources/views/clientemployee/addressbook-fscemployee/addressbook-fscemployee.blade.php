@extends('clientemployee.layouts.app')
@section('title', 'AddressBook')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of Address Book</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1> View / Edit</h1>
                </div>
            </div>
        </section>
        <section class="content" style="background-color: #fff;">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-title">
                                <h3></h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Name</th>
                                        <th>Country</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($gold as $go)
                                        <tr>
                                            <td>@if($go->type=='Active') Client @else{{ucwords($go->type)}}@endif</td>
                                            <td>{{$go->firstName}} {{$go->middleName}} {{$go->lastName}}</td>
                                            <td>{{$go->countryId}}</td>
                                            <td>{{$go->email}} </td>
                                            <td>{{$go->telephoneNo1}}</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{url('fscemployee/addressbook-fscemployee',[$go->id,$go->type])}}/edit"><i class="fa fa-edit"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()