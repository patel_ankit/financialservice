@extends('clientemployee.layouts.app')
@section('title', 'Task')
@section('main-content')
    <div class="content-wrapper">

        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>List of Task</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <section class="content" style="background-color: #fff;">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="table-title">
                            <a href="{{url('fscemployee/newtask/create')}}">Add New Task</a><br>
                            <br>
                        </div>
                        <div class="card-body">

                            @if(session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">

                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Create Date</th>
                                        <th>Send By</th>

                                        <th>Subject</th>
                                        <th>Description</th>
                                        <th>Priority</th>
                                        <th>Due Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($task as $com)
                                        <tr>
                                            <td style="text-align:center;">{{$loop->index+1}}</td>
                                            <td style="text-align:center;">{{$com->creattiondate}}</td>
                                            <td>@foreach($emp as $com1) @if($com->admin_id==$com1->id) {{ucwords($com1->name)}} @endif @endforeach @foreach($admin as $com2) @if($com2->id==$com->admin_id) {{ucwords($com2->fname).' '.ucwords($com2->lname)}} @endif @endforeach </td>

                                            <td>{{$com->title}}</td>
                                            <td>{!! $com->content !!}</td>
                                            <td>{!! $com->priority !!}</td>
                                            <td style="text-align:center;">{!! $com->duedate !!}</td>
                                            <td style="text-align:center;">

                                                <a class="btn-action btn-view-edit" href="{{route('newtask.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
                                            <!--	<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
<form action="{{ route('newtask.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                    </form>-->
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection