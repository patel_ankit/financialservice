<footer>
    <div class="container-fluid show1">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <a href="https://vimbo.com/" target="_blank"> <img src="https://financialservicecenter.net/public/frontcss/images/footer-logo-left.png" alt="logo" class="img-responsive"> </a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <a href="#"><span class="first-letter">F</span>inancial
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a> &nbsp; All rights reserved. </p></div>
        <div class="col-md-3 col-sm-3 col-xs-12 ">
            <a href="https://vimbo.com/" target="_blank"><img src="https://financialservicecenter.net/public/frontcss/images/footer-logo-right.png" alt="logo" class="img-responsive pull-right"> </a>
        </div>
    </div>
    <div class="container-fluid hide1">
        <div class=" col-xs-12">
            <p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <br><br><a href="#"><span class="first-letter">F</span>inancial
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a><br><br> All rights reserved. </p></div>
        <div style="background: #fff;width: 100%;
    display: inline-block;">
            <center><img src="{{url('public/dashboard/images/footer-logo.png')}}" alt="" class="img-responsive"></center>
        </div>
    </div>
</footer>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-success').fadeIn('slow', function () {
            $('.alert-success').delay(5000).fadeOut();
        });
    });
</script>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-danger').fadeIn('slow', function () {
            $('.alert-danger').delay(5000).fadeOut();
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sampleTable3').DataTable({
            "order": [[0, 'asc']],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    //$('.alert-success').fadeIn('slow').delay(4000).fadeOut('slow');
</script>

<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    $(document).ready(function () {
        $(".textonly").keypress(function (event) {
            var inputValue = event.charCode;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
                event.preventDefault();
            }
        });
    });
    $(document).ready(function () {
        //called when key is pressed in textbox
        $(".zip").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });

</script>