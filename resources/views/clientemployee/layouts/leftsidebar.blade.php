<style>
    .sidebar-menu > li > a {
        line-height: 42px;
    }

    .aligntext {
        margin-left: 1%;
    }
</style>
<aside class="main-sidebar">
    <section class="sidebar">

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="{{url('clientemployee')}}"><i class="fa fa-tachometer sidebar-icon"></i><span> Dashboard </span></a></li>
            <li class="treeview"><a href="#"><i class="fa fa-user-circle-o sidebar-icon"></i><span> Profile </span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('clientemployee/clientprofile')}}?tab=0" class="fsemp"><i class="fa fa-circle-o sidebar-icon"></i><span> Profile</span></a></li>
                    <li><a href="{{url('clientemployee/clientchangepassword')}}"><i class="fa fa-circle-o"></i><span> Login Info</span></a></li>
                </ul>
            </li>

            <li class="treeview"><a href="#"><i class="fa fa-user-circle-o sidebar-icon"></i><span> Report </span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('clientemployee/employeereport')}}" class="fsemp"><i class="fa fa-circle-o sidebar-icon"></i><span> Timesheet</span></a></li>

                </ul>
            </li>

            <li><a href="{{ route('clientemployee.logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><i class="fa fa-sign-out sidebar-icon"></i><span class="aligntext">Log Out</span></a>
                <form id="logout-form" action="{{ route('clientemployee.logout') }}" method="POST" style="display: none;">{{ csrf_field() }} </form>
            </li>

        </ul>
    </section>
</aside>