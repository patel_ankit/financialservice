@extends('clientemployee.layouts.app')
@section('title', 'Message')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title" style="height:50px;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h1>Message Logsheet (Outbox)</h1>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h1>Add / View / Edit</h1>
                </div>
            </div>
        </section>
        <section class="content" style="background-color: #fff;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-title" style="width: 100%;display: inline-block;">
                                <a href="{{url('fscemployee/sendmessage/create')}}">Add New Message</a>
                            </div>
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>S. No</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Contact Name</th>
                                        <th>Contact No.</th>
                                        <th>Call Purpose</th>
                                        <th>For Whom?</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($task as $com)
                                        <tr>
                                            <td></td>
                                            <td style="text-align:center;">@if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif{{$com->date}}<br> {{$com->day}}</td>
                                            <td style="text-align:center;">{{$com->time}}</td>
                                            <td>@if($com->type=='Other Person') {{$com->clientname}} @endif @foreach($admin as $com2) @if($com2->id==$com->employeeid) {{ucwords($com2->fname)}} {{ucwords($com2->mname)}} {{ucwords($com2->lname)}} @endif @endforeach @foreach($emp as $com2) @if($com2->id==$com->employeeid) {{ucwords($com2->firstName)}} {{ucwords($com2->middleName)}} {{ucwords($com2->lastName)}} @endif @endforeach</td>
                                            <td>@foreach($admin as $com2) @if($com2->id==$com->employeeid) {{ucwords($com2->telephone)}}  @endif @endforeach @foreach($admin as $com2) @if($com2->id==$com->admin_id) {{ucwords($com2->telephone)}} @endif @endforeach</td>
                                            <td>{{$com->title}}</td>
                                            <td>@foreach($emp as $com2) @if($com2->id==$com->admin_id) {{ucwords($com2->firstName)}} {{ucwords($com2->middleName)}} {{ucwords($com2->lastName)}} @endif @endforeach</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('getmsg.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('getmsg.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('getmsg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>.table > thead > tr > th {
            background: #ffff99;
            border-bottom: 8px solid #993366;
        }</style>
    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                "ordering": true,
                dom: 'Bfrtip',
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
                "order": [[1, 'asc']],

                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                        //titleAttr: 'Copy',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h2').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            $('row c[r^="A"]', sheet).attr('s', '51');
                            // $('c[r^="E"]', sheet).attr('s','50');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h1').text(),
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 60, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'left',
                                        image: logo,
                                        width: 50, margin: [230, 5]
                                    }, {
                                        alignment: 'right',
                                        text: 'Telephone Message (In) Logsheet',
                                        fontSize: 14,
                                        margin: [50, 35, 190, 40],
                                    },],
                                    margin: [50, 0, 0, 12], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h6').text(),
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br>Telephone Message (In) Logsheet</center>'
                                );
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6],
                        },
                        footer: true,
                        autoPrint: true
                    },],
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
            table.columns(9).search('^(?:(?!Done).)*$\r?\n?', true, false).draw();
            $("#choice").on("change", function () {
                var _val = $(this).val();//alert(_val);

                if (_val == 'Urgent') {
                    table.columns(1).search(_val).draw();
                } else if (_val == 'Regular') {  //alert();
                    table.columns(1).search(_val).draw();
                } else if (_val == 'Important') {  //alert();
                    table.columns(1).search(_val).draw();
                } else if (_val == 'On Hold') {  //alert();
                    table.columns(9).search(_val).draw();
                } else if (_val == 'Under Progress') {  //alert();
                    table.columns(9).search(_val).draw();
                } else if (_val == 'Done') {  //alert();
                    table.columns(9).search(_val).draw();
                } else {
                    table.columns().search('').draw();
                }
            })
        });
    </script>
@endsection()