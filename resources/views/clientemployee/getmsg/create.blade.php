@extends('clientemployee.layouts.app')
@section('title', 'Create Message')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>New Message </h1>
        </div>
        <section class="content" style="background-color: #fff;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('getmsg.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date / Day / Time :</label>
                                    <div class="col-md-2" style="width: 130px;">
                                        <div class="">
                                            <input type="text" name="date" id="date" value="{{date('m/d/Y')}}" style="font-weight: normal;pointer-events:none; background:#ccc !important;" class="form-control" placeholder="Date">
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="width: 130px;">
                                        <div class="">
                                            <input type="text" name="day" id="day" class="form-control" style="font-weight: normal;pointer-events:none; background:#ccc !important;" placeholder="Day" value="{{date('l')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="width: 115px;">
                                        <div class="">
                                            <input type="text" name="time" id="time" class="form-control" style="font-weight: normal;pointer-events:none; background:#ccc !important;" value="{{date("H:i a")}}" placeholder="Time">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Massage From :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="type" id="type" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="employee">FSC-EE / User</option>
                                                <option value="Active">Client</option>
                                                <option value="Other Person">Other Person</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Massage To :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="employee" id="employee" class="form-control selectpicker fsc-input">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('employee'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group client" style="display:none">
                                    <label class="control-label col-md-3">Client Business Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="busname" readonly id="busname" class="form-control">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group client" style="display:none">
                                    <label class="control-label col-md-3">Client Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientname" readonly id="clientname" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client1" style="display:none">
                                    <label class="control-label col-md-3">Client File # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientfile" readonly id="clientfile" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client1" style="display:none">
                                    <label class="control-label col-md-3">Client Telephone # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientno" readonly id="clientno" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Call Purpose :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="purpose" id="purpose" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="FSC-EE">FSC-EE</option>
                                                <option value="FSC-User">FSC-User</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add Call Purpose</button>
                                    </div>
                                </div>

                                <div class="form-group client">
                                    <label class="control-label col-md-3">Other :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="other" id="other" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client">
                                    <label class="control-label col-md-3">What is the Message :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="othermsg" id="othermsg" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Message :</label>
                                    <div class="col-md-4">
                                        <div class="">

                                            <select name="purpose" id="purpose" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="Regular">Regular</option>
                                                <option value="Important">Important</option>
                                                <option value="Urgent">Urgent</option>

                                            </select>
                                            <input type="hidden" value="" name="other" id="other" class="form-control fsc-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-3">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input class="btn_new_save  btn-primary1 primary1" type="submit" name="submit" value="save">
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="btn_new_cancel" href="{{url('/fscemployee/getmsg')}}">Cancel</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
    <script>


        $(".selectpicker").select2({})


    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#type', function () {
                $('#clientname').empty();
                $('#clientname').empty();
                $('#clientno').empty();
                $('#busname').empty();
                $('#clientfile').empty();
                var id = $(this).val();
                $('#employee').empty();
                if (id == 'Other Person') {
                    $('.client').show();
                    $('.client1').show();
                    document.getElementById('busname').removeAttribute('readonly');
                    document.getElementById('clientname').removeAttribute('readonly');
                    document.getElementById('clientfile').removeAttribute('readonly');
                    document.getElementById('clientno').removeAttribute('readonly');
                }

                $.get('{!!URL::to('/fscemployee/getmessage')!!}?id=' + id, function (data) {
                    $('#employee').empty();
                    $('#employee').append('<option value="">---Select---</option>');
                    $.each(data, function (index, subcatobj) {
//$('#employee1').val(subcatobj.firstName);
                        if (id == 'employee') {
                            $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');
                        }
                        if (id == 'Active') {
                            $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + '  ' + subcatobj.last_name + '</option>');
                        }
                    })

                });

            });
        });

        /*$( "#date" ).datepicker({
            'dateFormat':'yy-mm-dd',
            onSelect: function(dateText){ alert();
                var seldate = $(this).datepicker('getDate');
                seldate = seldate.toDateString();
                seldate = seldate.split(' ');
                var weekday=new Array();
                    weekday['Mon']="Monday";
                    weekday['Tue']="Tuesday";
                    weekday['Wed']="Wednesday";
                    weekday['Thu']="Thursday";
                    weekday['Fri']="Friday";
                    weekday['Sat']="Saturday";
                    weekday['Sun']="Sunday";
                var dayOfWeek = weekday[seldate[0]];
                $('#day').val(dayOfWeek);
            }
        });*/

    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#employee', function () {
                var selectedCountry = $("#type option:selected").val();
                var id = $(this).val(); //alert(selectedCountry);
                $.get('{!!URL::to('/fscemployee/getmessage')!!}?id=' + id + '&state=' + selectedCountry, function (data) {
                    $('#clientname').empty();
                    $('#clientname').empty();
                    $('#clientno').empty();
                    $('#busname').empty();
                    $('#clientfile').empty();
                    $.each(data, function (index, subcatobj) {
                        document.getElementById('busname').readOnly = true;
                        document.getElementById('clientname').readOnly = true;
                        document.getElementById('clientfile').readOnly = true;
                        document.getElementById('clientno').readOnly = true;
                        //   alert(subcatobj.type);
                        if ('employee' == subcatobj.type) {
                            $('.client').show();
                            $('.client1').show();
                            $('#clientname').val(subcatobj.firstName);
                            $('#clientno').val(subcatobj.telephoneNo1);
                            $('#busname').val(subcatobj.business_name);
                            $('#clientfile').val(subcatobj.employee_id);
                        }
                        if ('Active' == subcatobj.status) {
                            $('.client').show();
                            $('.client1').show();
                            $('#clientname').val(subcatobj.first_name);
                            $('#clientno').val(subcatobj.business_no);
                            $('#busname').val(subcatobj.business_name);
                            $('#clientfile').val(subcatobj.filename);
                        }
                    })
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#date").change(function () {
                var startdate = $("#date").val();
                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var durtion = $('#duration').val();
                var date = new Date(startdate);
                var day = weekday[date.getDay()];
                var monthly = 30;
                var weekly = 7;
                var bimonthly = 15;
                var biweekly = 14;
                var monthss = monthNames[(date.getMonth())];
                var yearss = date.getFullYear();
                var yyy = yearss % 4;
                //if(yyy)
                //{
                //alert('true');
                //}
                //else
                //{
                //alert('false');
                //}
                if (durtion == "Weekly") {
                    var totaldays = 6;
                } else if (durtion == "Monthly") {
                    if (monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec') {
                        var totaldays = 30;
                    } else if (monthss == 'Feb') {
                        //if(years / 4 = 0)
                        if (yyy == 0) {
                            var totaldays = 28;
                        } else if (yyy == 1) {
                            var totaldays = 27;
                        }
                    } else if (monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov') {
                        var totaldays = 29;
                    }
                } else if (durtion == "Bi-Weekly") {
                    var totaldays = 13;
                } else if (durtion == "Bi-Monthly") {
                    var totaldays = 14;
                }
                // var vv = day + totaldays;

                date.setDate(date.getDate() + totaldays);// alert(vv);
                var date1 = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
                // alert(date1);
                var newdate = new Date(date1);
                var day1 = weekday[newdate.getDay()];
                //alert(newdate);
                var date2 = monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear();
                // $('#sch_end_date').val(date1);
                $('#day').val(day);
                $('#sch_end_day').val(day1);
                //document.write(date2);
            });
            $("#duration").change(function () {
                $('#sch_end_date').val('');
                $('#sch_start_date').val('');
            });
        });
    </script>
    <style>
        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 39px;
            border-redius: 4px;
            user-select: none;
            -webkit-user-select: none;
        }

    </style>

    <style>
        .select2 {
            width: 100% !important;
        }

        .select2-container .select2-selection--single {

            border: 2px solid #00468F;
        }
    </style>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Call Purpose</h4>
                </div>
                <div class="modal-body" style="display: inline-table;">
                    <div class="form-group">
                        <label class="control-label col-md-3">Call Purpose :</label>
                        <div class="col-md-6">
                            <div class="">
                                <input type="text" name="Purpose" id="Purpose" class="form-control" placeholder="Call Purpose">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="">
                                <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Add Call Purpose">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection()