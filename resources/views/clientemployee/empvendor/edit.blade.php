@extends('clientemployee.layouts.app')
@section('title', 'Vendor')
@section('main-content')
    <style>
        .select2-container .select2-selection--single .select2-selection__rendered {
            padding-left: 0px !important;
        }

        .select2-container--default .select2-selection--single {
            background: #ffe066 !important;
        }

        .select2-selection select2-selection--single {
            background: #ffe066 !important;
        }

        .form-control {
            font-weight: 100 !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #4a9ef3;
            border: 1px solid #aaa;
            border-radius: 4px;
            cursor: default;
            float: left;
            margin-right: 5px;
            margin-top: 5px;
            padding: 0 5px;
            color: #fff !important;
            font-size: 17px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 14px !important;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field {
            border-color: #3c8dbc !important;
            border: 2px solid #2fa6f2;

            border-radius: 4px;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            /* border: 1px solid #aaa; */
            border-radius: 4px;
            border: 2px solid #2fa6f2;
        }

        .select2-container--default .select2-selection--multiple {
            border: 2px solid #2fa6f2;
        }

        .nex1:hover, .nex1:focus {
            background-color: transparent;
        }

        .ag {
            position: relative;
            top: -25px;
            font-size: 13px;
            left: -100px;
        }

        .ac {
            margin-left: 0.7%;
        }

        .ps_mr {
            width: 13% !important;
        }

        .ps_firstname {
            width: 28% !important;
        }

        .ps_middlename {
            WIDTH: 8% !IMPORTANT;
        }

        .ps_lastname {
            width: 31% !important;
        }

        .green-border {
            background-color: #003b6d !important;
        }

        .breadcrumb > li.ddd.active1 a {
            background: green !important;
        }

        .title-form-group p {
            font-size: 16px;
            line-height: 30px;
        }

        .tess {
            text-align: left;
            float: right;
            font-size: 11px;
            margin-right: 20px;
        }

        .star-required1 {
            color: transparent;
        }

        .fsc-reg-sub-header {
            padding-left: 0;
        }

        .pager li > a, .pager li > span {
            width: 93px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            color: #fff;
            background-color: green;
        }

        .bread
        .title-form-group p {
            font-size: 12px
        }

        li.next {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .nav-pills > li > a {
            border-radius: 0 !important;
        }

        li.previous {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .fsc-reg-sub-header-div {
            border-radius: 0;
            padding: 6px 4%;
            border-top: 2px solid #fff;
            border-bottom: 2px solid #fff;
        }

        .breadcrumb {
            padding: 0px;
            background: #D4D4D4;
            list-style: none;
            overflow: hidden;
            margin-top: -20px;
        }

        .breadcrumb > li + li:before {
            padding: 0;
        }

        .breadcrumb li {
            float: left;
            width: 24%;
            text-align: center;
            font-size: 15px;
        }

        }
        .breadcrumb li.active a {
            background: brown;
            background: green;
        }

        .breadcrumb li.completed a {
            background: brown;
            background: hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li.active a:after {
            border-left: 30px solid green;
        }

        .breadcrumb li.completed a:after {
            border-left: 30px solid hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li a {
            width: 95%;
            color: white;
            text-decoration: none;
            padding: 10px 0 10px 45px;
            position: relative;
            display: block;
            float: left;
        }

        .breadcrumb li a:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            border-bottom: 50px solid transparent;
            border-left: 30px solid hsla(0, 0%, 83%, 1);
            position: absolute;
            top: 50%;
            margin-top: -50px;
            left: 100%;
            z-index: 2;
        }

        .breadcrumb li a:before {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            /* Go big on the size, and let overflow hide */
            border-bottom: 50px solid transparent;
            border-left: 30px solid white;
            position: absolute;
            top: 50%;
            margin-top: -50px;
            margin-left: 1px;
            left: 100%;
            z-index: 1;
        }

        .breadcrumb li:first-child a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .content-wrapper {

            height: 100%;
        }

        .breadcrumb li:nth-o a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .pager .disabled > a, .pager .disabled > a:focus, .pager .disabled > a:hover, .pager .disabled > span {
            color: #fff;
            cursor: not-allowed;
            background-color: #428bca;
            font-size: 14px;
        }

        .pager li > a, .pager li > span {
            display: inline-block;
            padding: 5px 14px;
            background-color: #0472d0;
            border: 1px solid #ddd;
            border-radius: 15px;
            font-size: 14px;
            color: #fff;
        }

        .error {
            font-size: 16px;
            color: #ff0000;
            font-weight: normal;
        }

        .breadcrumb li a:hover {
            background: green !important;
        }

        .breadcrumb li a:hover:after {
            border-left-color: green !important;
        }

        .nav-tabs > li {
            width: 25%;
        }

        .nav-tabs > li > a {
            height: 40px;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .Red {
            background-color: red !important;
            color: #fff !important
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff !important
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff !important
        }

        .Yellow {
            background-color: Yellow !important;
            color: #555 !important
        }

        .Orange {
            background-color: Orange !important;
            color: #fff !important
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn3d.btn-info:hover, .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .fsc-form-label {
            display: block;
            text-align: right;
        }

        .tab-content > .tab-pane {
            padding: 20px 0;
        }

        .btnNext {
            float: right;
        }

        .btnPrevious {
            float: left;
        }

        .ps_main {
            float: left;
        }

        .ps_mr {
            float: left;
            width: 18%;
            margin-right: 1%;
        }

        .ps_firstname {
            float: left;
            width: 33%;
            margin: 0 1%;
        }

        .ps_middlename {
            float: left;
            width: 10%;
            margin: 0 1%;
        }

        .ps_lastname {
            float: left;
            width: 33%;
            margin-left: 1%;
        }

        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 8px 3px 8px 8px !important;
        }

        .center_main {
            width: 100%;
            text-align: center;
        }

        .center_tab {
            display: inline-block;
            width: 170px;
            margin: 0 10px;
        }

        .ser:nth-of-type(odd) {
            padding: 15px 0;
            width: 100%;
            background-color: #e8f7fd;
            margin: auto;
        }

        .ser:nth-of-type(even) {
            background-color: #fff5dd;
            padding: 15px 0;
            width: 100%;
            margin: auto;
        }

        .bg-color {
            background: #bdbdbd;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        .select2-container--default .select2-selection--single {
            height: 40px;
            line-height: 40px;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Vendor </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                </div>
                                <form method="post" id="registrationForm" action="{{route('empvendor.update',$emp->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                    {{csrf_field()}}{{method_field('PATCH')}}
                                    <div id="smartwizard">
                                        <div class="panel-body">
                                            <div class="tab-content">

                                                <div class="tab-pane fade in active" data-step="0" id="tab1primary">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <br/>
                                                        <div class="form-group {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" id="business_catagory_name_2">
                                                            <label class="control-label col-md-3">Type of Business : <span class="star-required">*</span></label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <div class="row">
                                                                    <div class="col-md-12"> <?php $str1 = $emp->business_catagory_name; $splittedstring1 = explode(",", $str1);?>
                                                                        <select name="business_catagory_name[]" multiple id="business_catagory_name" class="js-example-tags form-control fsc-input category1">
                                                                            <option value=''>---Select Business Category---</option>
                                                                            @foreach($category as $cate)
                                                                                <option value='{{$cate->id}}' @foreach($splittedstring1 as $key => $value) @if($value ==$cate->id) selected @endif @endforeach >{{$cate->business_cat_name}}</option>
                                                                            @endforeach
                                                                        </select>

                                                                    </div>

                                                                </div>
                                                                @if ($errors->has('business_catagory_name'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('business_catagory_name') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="form-group {{ $errors->has('business_name') ? ' has-error' : '' }}">
                                                            <label class="control-label col-md-3">Vendor Name : <span class="star-required">*</span></label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <div class="">
                                                                    <div class="business_name">
                                                                        <input type="text" class="form-control" placeholder="Vendor Name" value="{{$emp->business_name}}" id="business_name" name="business_name">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @if ($errors->has('business_name'))
                                                                <span class="help-block">
											<strong>{{ $errors->first('business_name') }}</strong>
										</span>
                                                            @endif
                                                        </div>

                                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Address 1 :</label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" value="{{$emp->address1}}" placeholder="Address 1" name="address" id="address">
                                                                @if ($errors->has('address'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('address') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="form-group {{ $errors->has('address1') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Address 2 : </label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" value="{{$emp->address2}}" placeholder="Address 2" name="address1" id="address1">

                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Country : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <div class="dropdown">
                                                                            <select name="countryId" style="width:77%;" id="countries_states1" data-country="{{$emp->countryId}}" class="form-control bfh-countries fsc-input">

                                                                            </select>

                                                                        </div>
                                                                        @if ($errors->has('countryId'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('countryId') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">City / State / Zip : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$emp->city}}">
                                                                        @if ($errors->has('city'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-3" style="width:24%">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-state="{{$emp->stateId}}" data-country="countries_states1">
                                                                            </select>
                                                                            @if ($errors->has('stateId'))
                                                                                <span class="help-block">
											<strong>{{ $errors->first('stateId') }}</strong>
										</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3" style="width:27%">
                                                                        <input type="text" class="form-control fsc-input zip" id="zip" name="zip" value="{{$emp->zip}}" maxlength="6" placeholder="Zip">
                                                                        @if ($errors->has('zip'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('zip') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Email :</label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="Email ID" value="{{$emp->email}}">
                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                                                @endif   </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone # : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="telephone" value="{{$emp->telephoneNo1}}" type="tel" id="motelephoneile" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999">
                                                                        @if ($errors->has('telephone'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('telephone') }}</strong>
										</span>
                                                                        @endif

                                                                    </div>
                                                                    <div class="col-md-4" style="width:24%">
                                                                        <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Resid">Res</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4" style="width:27%">
                                                                        <input class="form-control fsc-input" id="ext1" maxlength="5" readonly name="ext1" value="{{$emp->ext1}}" placeholder="Ext" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone # :</label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="motelephoneile1" value="{{$emp->telephoneNo2}}" type="tel" id="motelephoneile1" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-4" style="width:24%">
                                                                        <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Resid">Res</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4" style="width:27%">
                                                                        <input class="form-control fsc-input" id="ext2" maxlength="5" value="{{$emp->ext2}}" readonly name="ext2" placeholder="Ext" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('business_fax') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Fax # : </label>
                                                            <div class="col-md-2 ac">
                                                                <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" value="{{$emp->fax}}" id="business_fax" name="business_fax" placeholder="(999) 999-9999">
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Website : </label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" id="website" value="{{$emp->website}}" name="website" placeholder="Website address">
                                                            </div>
                                                        </div>
                                                        <div class="Branch">
                                                            <h1>Contact Information</h1>
                                                        </div>
                                                        <br/>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Name :</label>
                                                                <div class="col-md-6">
                                                                    <div class="ps_main">
                                                                        <div class="ps_mr {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                                                            <select type="text" class="form-control txtOnly" id="minss" name="minss">

                                                                                <option Value="Mr.">Mr.</option>
                                                                                <option Value="Mrs.">Mrs.</option>
                                                                                <option Value="Miss.">Miss.</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="ps_firstname">
                                                                            <input type="text" class="form-control" placeholder="First Name" value="{{$emp->firstName}}" id="firstname" name="firstname">
                                                                        </div>
                                                                        <div class="ps_middlename {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                                                            <input type="text" class="form-control" id="middlename" placeholder="M" name="middlename" maxlength="1" value="{{$emp->middleName}}">
                                                                        </div>
                                                                        <div class="ps_lastname {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                                                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{{$emp->lastName}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-3">
                                                                    <input type="checkbox" class="" id="billingtoo123" name="billingtoo123" onclick="FillBilling123(this.form)"> <label for="billingtoo123"> Same As Business Telephone</label>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Telephone # :</label>
                                                                <div class="col-md-2" style="width:17% !Important;">
                                                                    <input name="mobile_1" placeholder="(999) 999-9999" value="{{$emp->etelephone1}}" type="tel" id="mobile_1" class="form-control phone"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="width:13.2%">
                                                                    <select name="mobiletype_1" id="mobiletype_1" class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        <option value="Home">Office</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                        <option value="Other">Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="width:13.2%">
                                                                    <input class="form-control fsc-input" id="ext2_1" maxlength="5" readonly value="{{$emp->eext1}}" name="ext2_1" placeholder="Ext" type="text">
                                                                </div>
                                                            </div>

                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-3">
                                                                    <input type="checkbox" class="" name="billingtoo" id="billingtoo" onclick="FillBilling(this.form)"><label for="billingtoo"> Same As Above Business Fax</label>
                                                                </div>

                                                            </div>
                                                            <div class="form-group {{ $errors->has('contact_fax') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Fax No. :</label>
                                                                <div class="col-md-2" style="width:17% !Important;">
                                                                    <input name="contact_fax_1" placeholder="(999) 999-9999" type="tel" value="{{$emp->efax}}" id="contact_fax" class="form-control bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999"/>

                                                                </div>
                                                            </div>
                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-4">
                                                                    <input type="checkbox" class="" id="emailbli2" name="emailbli2" onclick="emailbl12(this.form)"> <label for="emailbli2">Same As Above Email</label>
                                                                </div>

                                                            </div>
                                                            <div class="form-group  {{ $errors->has('email_1') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-5" style="width:43.45%">
                                                                    <input type="text" class="form-control" id="email_1" name="email_1" value="{{$emp->email}}" placeholder="Email ID">

                                                                </div>
                                                            </div>

                                                            <div class="Branch">
                                                                <h1>Product Information</h1>
                                                            </div>
                                                            <br/>
                                                            <div class="after-add-more">
                                                                <div class="form-group {{ $errors->has('productid') ? ' has-error' : '' }}">
                                                                    <label class="control-label col-md-3">Type of Product : <span class="star-required">*</span></label>
                                                                    <div class="col-md-6">
                                                                        <?php $str = $emp->product; $splittedstring = explode(",", $str);?>
                                                                        <select class="js-example-tags form-control" style="width: 86.5%;" id="vendor_product" name="productid[]" multiple="multiple">
                                                                            @foreach($products1 as $cur)
                                                                                <option value="{{$cur->id}}" @foreach($splittedstring as $key => $value) @if($value ==$cur->id) selected @endif @endforeach >{{$cur->productname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if ($errors->has('productid'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('productid') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <br/>
                                                            <div class="Branch" style=" background:skyblue; ">
                                                                <h1>Accounting Use Only</h1>
                                                            </div>
                                                            <br/>
                                                            <div class="after-add-more" style="pointer-events: none;">
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Status</label>
                                                                    <div class="col-md-5" style="width:14%">
                                                                        <select name="status" id="status" class="form-control fsc-input" style="background:#ffe066 !important;" onchange="this.className=this.options[this.selectedIndex].className"
                                                                                name="agentstatus[]" id="agentstatus">
                                                                            <option value="">Select</option>
                                                                            <option class="greenText" value="1" @if($emp->check =='1') selected @endif>Active</option>
                                                                            <option class="redText" value="0" @if($emp->check =='0') selected @endif>Inactive</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Account Code</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <select class="js-example-tags form-control" style="font-size:14px !important;background:#ffe066 !important;" id="accountcode" name="accountcode">
                                                                            <option style="font-size:14px !important;" style="background:#ffe066 !important;" value="">Select</option>
                                                                            @foreach($accountcode as $cur)
                                                                                <option value="{{$cur->accountcode}}" style="font-size:14px !important;" @if($emp->accountcode ==$cur->accountcode) selected @endif>{{$cur->accountcode}} ({{$cur->account_name}})</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Account Name</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" style="background:#ffe066 !important;" id="account_name" name="account_name" value="{{$emp->account_name}}" placeholder="Account Name">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Account Belong To</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" style="background:#ffe066 !important;" id="account_belongs_to" name="account_belongs_to" value="{{$emp->account_belongs_to}}" placeholder="Account Belong To">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Note</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" style="background:#ffe066 !important;" id="note" name="note" value="{{$emp->notes}}" placeholder="Note">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="col-md-7 col-md-offset-3">
                                                        <div class="row">
                                                            <div class="col-md-3" style="margin-left:7px !important;">
                                                                <button type="submit" class="btn_new_save">Save</button>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="{{url('/fac-Bhavesh-0554/vendor')}}" class="btn_new_cancel">Cancel</a>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
        </section>
    </div>
    <script>
        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });


        $(".ext").mask("99999");
        $("#federal_id").mask("99-9999999");
        $("#contact_number11").mask("a999999");
        $(".ext").mask("99339");
        $("#zip").mask("99999");
        $("#company_zip").mask("99999");
        $("#second_zip").mask("99999");
    </script>

    <script>
        function FillBilling(f) {
            if (f.billingtoo.checked == true) {
                f.contact_fax_1.value = f.business_fax.value;
            } else {
                $('#contact_fax').val('');
            }

        }

        function emailbl12(f) {
            if (f.emailbli2.checked == true) {
                f.email_1.value = f.email.value;
            } else {
                $('#email_1').val('');
            }

        }
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#business_catagory_name', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getRequest_1')!!}?id=' + id, function (data) {
                    if (data == "") {
//	$('#business_catagory_name_4').hide();
                    } else {
//	$('#business_catagory_name_4').show();
                    }
                    $('#image1').empty();

                    $.each(data, function (index, subcatobj) {
                        $('#image1').append('<img src="/public/category/' + subcatobj.business_cat_image + '" alt="" class="img-responsive">');
                    })
                });
            });
        });


        $(document).ready(function () {
            $(document).on('change', '#accountcode', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getAccountcode')!!}?id=' + id, function (data) {
                    $('#account_name').val('');
                    $('#account_belongs_to').val('');
                    $.each(data, function (index, subcatobj) {
                        $('#account_name').val(subcatobj.account_name);
                        $('#account_belongs_to').val(subcatobj.account_belongs_to);
                    })
                });
            });
        });

        $(document).ready(function () {

            $(".phone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("(" + curval + ")" + " ");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                    $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 9) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });

    </script>

    <script type="text/javascript">

    </script>

    <script>

        $(document).ready(function () {
            $("select#status").change(function () {
                var selectedCountry = $(this).children("option:selected").val();
                var color = $("option:selected", this).attr("class");
                $("#status").attr("class", color).addClass("form-control1 fsc-input");
            });
        });

    </script>
    <script>
        $(document).on("change", ".numeric1", function () {
            var sum = 0;
            $(".numeric1").each(function () {
                sum += +$(this).val().replace("%", "");
                var num = parseFloat($(this).val());
                $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
//$(".numeric2").val(num.toFixed(2)); 
            });
            if (sum > 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').addClass("disabled");
                $('#t1').show();
            } else if (sum < 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').addClass("disabled");
                $('#t1').show();
            } else if (sum = 100) {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').removeClass("disabled");
                $('#t1').hide();


            } else {
                $(".total").val(sum.toFixed(2) + "%");
                $('.btn-primary1').removeClass("disabled");
            }
        });
    </script>
    <script>
        function FillBilling123(f) {
            if (f.billingtoo123.checked == true) {
                f.mobile_1.value = f.telephone.value;
                f.mobiletype_1.value = f.telephoneNo1Type.value;
                f.ext2_1.value = f.ext1.value;
            } else {
                f.mobile_1.value = '';
                f.mobiletype_1.value = '';
                f.ext2_1.value = '';
            }
        }
    </script>


    <script>
        var date = $('#ext2_1').val();
        $('#mobiletype_1').on('change', function () {

            if (this.value == 'Home') {
                document.getElementById('ext2_1').removeAttribute('readonly');
                $('#ext2_1').val();
            } else {
                document.getElementById('ext2_1').readOnly = true;
                $('#ext2_1').val('');
            }
        })
    </script>


    <script>
        var date = $('#ext1').val();
        $('#telephoneNo1Type').on('change', function () {

            if (this.value == 'Office') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else if (this.value == 'Business') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>
    <script>
        var date = $('#ext2').val();
        $('#telephoneNo2Type').on('change', function () {

            if (this.value == 'Office') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else if (this.value == 'Business') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })
    </script>


    <script>
        var date = $('#ext2_2').val();
        $('#mobiletype_2').on('change', function () {

            if (this.value == 'Home') {
                document.getElementById('ext2_2').removeAttribute('readonly');
                $('#ext2_2').val();
            } else {
                document.getElementById('ext2_2').readOnly = true;
                $('#ext2_2').val('');
            }
        })
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(function () {
            $('#addopt').click(function () { //alert();
                var newopt = $('#newopt').val();
                if (newopt == '') {
                    alert('Please enter something!');
                    return;
                }

                //check if the option value is already in the select box
                $('#vendor_product option').each(function (index) {
                    if ($(this).val() == newopt) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "{!!route('product.productss')!!}",
                    dataType: "json",
                    data: $('#ajax').serialize(),
                    success: function (data) {
                        alert('Successfully Added');
                        $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                        $("#div").load(" #div > *");
                        $("#newopt").val('');
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                $('#basicExampleModal').modal('hide');
            });
        });


        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removeproduct.removeproducts')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            // alert(data);
                            $('#cur_' + id).remove();
                            $("#vendor_product").load(" #vendor_product > *");
                        }
                    })
                } else {
                    return false;
                }
            });
        });
    </script>
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="ajax">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Product Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="addopt" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="curency curency_ref" id="div">
                        @foreach($products1 as $cur)
                            <li id="cur_{{$cur->id}}"><a class="delete" id="{{$cur->id}}">{{$cur->productname}} <span><i class="fa fa-trash"></i></span></a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection()