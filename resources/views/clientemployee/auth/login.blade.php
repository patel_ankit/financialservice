@extends('fscemployee.app')
@section('main-content')
    <style>
        .star-required {
            color: red
        }
    </style>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 fsc-content-head">
                <h4>LOGIN</h4>
            </div>
        </div>

        <h2 style="margin-top:3%; margin-bottom:2%;">Choose any login type :</h2>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs">
                <li><img data-toggle="tab" href="#home" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/login-client.png')}}"/></li>

                <li><img data-toggle="tab" href="#menu1" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/client-employee.png')}}"/></li>

                <li><img data-toggle="tab" href="#menu2" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/login-user.png')}}"/></li>

                <li><img data-toggle="tab" href="#menu3" style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/login-vendor.png')}}"/></li>
            </ul>
        </div>

        <div class="tab-content">

            <div id="home" role="tabpanel" class="tab-pane fade in">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>CLIENT LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 fsc-content-box">

                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}


                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Client ID : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="password" type="password" class="form-control fsc-input" name="password" required placeholder="Enter Your Password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                                </div>

                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                </div>

                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>

                            </div>


                        </form>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

            </div>

            <div id="menu1" role="tabpanel" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>CLIENT EMPLOYEE LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <form class="form-horizontal" method="POST" action="{{route('fscemployee.login')}}">
                        {{ csrf_field() }}
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="col-md-1 col-sm-1 col-xs-1"></div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                            <label class="fsc-form-label">User ID : <span class="star-required">*</span></label>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                            <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <input id="password" type="password" class="form-control fsc-input" name="password" required placeholder="Enter Your Password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                                    </div>

                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                        <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                    </div>

                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>

                                </div>

                            </div>

                            <div class="col-md-1 col-sm-1 col-xs-1"></div>

                        </div>
                    </form>
                </div>

            </div>

            <div id="menu2" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>USER LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>
                <form class="form-horizontal" method="POST" action="">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-1 col-sm-1 col-xs-1"></div>

                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">User ID : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="email" type="email" placeholder="Enter Your Email Id" class="form-control fsc-input" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input id="password" type="password" class="form-control fsc-input" name="password" required placeholder="Enter Your Password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <a href="{{ route('password.request') }}" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                                </div>

                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                    <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                                </div>

                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>

                            </div>

                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    </div>
                </form>
            </div>

            <div id="menu3" class="tab-pane fade">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>VENDOR LOGIN</h4>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Vendor : <span class="star-required">*</span></label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="email" class="form-control fsc-input" id="" placeholder="Vendor ID">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="email" class="form-control fsc-input" id="" placeholder="Password">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <a href="#" class="Libre" style="font-size: 1.4em;">Forgot Password?</a>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="">LOGIN</button>
                            </div>

                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-default btn-lg fsc-form-submit" style="">CANCEL</button>
                            </div>

                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>

                        </div>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1"></div>

                </div>

            </div>

        </div>

    </div>

    <script type="text/javascript">
        function ClientLogin() {
            $("#clientSuccessError").hide();
            $("#clientSuccessError").hide();
            var email = $("#email").val();
            var password = $("#password").val();
            if (email == "") {
                $("#clientSuccessError").html("Please enter email address").show();
            } else if (password == "") {
                $("#clientDangerError").html("Please enter password").show();
            } else {
                $.post("", {email: email, password: password}, function (data) {
                    if (data.success == "true") {
                        location.reload()
                    } else {
                        $("#clientSuccessError").html(data.message).show();
                    }
                }, "json");
            }
        }
    </script>




@endsection