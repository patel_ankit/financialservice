@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Personal Registration</h4>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="text-align:center;">
                <img src="{{URL::asset('frontcss/images/registration/Personal.png')}}" alt=""/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
                <h4>Registration</h4>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form id="registrationForm" name="registrationForm" method="post">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">

                    <div class='alert alert-danger' style='margin-top:10px; display:none; font-size:15px;' id='dangerError'>ad</div>

                    <div class='alert alert-success' style='margin-top:10px; display:none; font-size:15px;' id='successError'>ad</div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <input type="hidden" id="bussiness_id" name="bussiness_id" value="6"/>
                        <input type="hidden" id="sub_bussiness_id" name="sub_bussiness_id" value=""/>
                        <input type="hidden" id="bussiness_brand_id" name="bussiness_brand_id" value=""/>
                        <input type="hidden" id="brand_details_id" name="brand_details_id" value=""/>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Company Name : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="company_name" name="company_name" placeholder="Company Name">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Business Name : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="business_name" name="business_name" placeholder="Business Name">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
                        <h3 class="Libre fsc-reg-sub-header">Business Location :</h3>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Address 1 : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="address1" name="address1" placeholder="">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Address 2 : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="address2" name="address2" placeholder="">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City">
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown" style="margin-top: 1%;">
                                <select name="stateId" id="stateId" class="form-control fsc-input">
                                    <option value="">Please Select State</option>
                                    <option value='2'>AK</option>
                                    <option value='3'>AS</option>
                                    <option value='4'>AZ</option>
                                    <option value='5'>AR</option>
                                    <option value='6'>CA</option>
                                    <option value='7'>CO</option>
                                    <option value='8'>CT</option>
                                    <option value='9'>DE</option>
                                    <option value='10'>DC</option>
                                    <option value='11'>FM</option>
                                    <option value='12'>FL</option>
                                    <option value='13'>GA</option>
                                    <option value='14'>GU</option>
                                    <option value='15'>HI</option>
                                    <option value='16'>ID</option>
                                    <option value='17'>IL</option>
                                    <option value='18'>IN</option>
                                    <option value='19'>IA</option>
                                    <option value='20'>KS</option>
                                    <option value='21'>KY</option>
                                    <option value='22'>LA</option>
                                    <option value='23'>ME</option>
                                    <option value='24'>MH</option>
                                    <option value='25'>MD</option>
                                    <option value='26'>MA</option>
                                    <option value='27'>MI</option>
                                    <option value='28'>MN</option>
                                    <option value='29'>MS</option>
                                    <option value='30'>MO</option>
                                    <option value='31'>MT</option>
                                    <option value='32'>NE</option>
                                    <option value='33'>NV</option>
                                    <option value='34'>NH</option>
                                    <option value='35'>NJ</option>
                                    <option value='36'>NM</option>
                                    <option value='37'>NY</option>
                                    <option value='38'>NC</option>
                                    <option value='39'>ND</option>
                                    <option value='40'>MP</option>
                                    <option value='41'>OH</option>
                                    <option value='42'>OK</option>
                                    <option value='43'>OR</option>
                                    <option value='44'>PW</option>
                                    <option value='45'>PA</option>
                                    <option value='46'>PR</option>
                                    <option value='47'>RI</option>
                                    <option value='48'>SC</option>
                                    <option value='49'>SD</option>
                                    <option value='50'>TN</option>
                                    <option value='51'>TX</option>
                                    <option value='52'>UT</option>
                                    <option value='53'>VT</option>
                                    <option value='54'>VI</option>
                                    <option value='55'>VA</option>
                                    <option value='56'>WA</option>
                                    <option value='57'>WV</option>
                                    <option value='58'>WI</option>
                                    <option value='59'>WY</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="zip" name="zip" placeholder="Zip">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="countryId" id="countryId" class="form-control fsc-input">
                                    <option value="">Please Select Country</option>
                                    <option value='1'>USA</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Business Tele. : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="business_no" name="business_no" placeholder="Business Telephone" onkeypress="PutNo()">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Business Fax : </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="business_fax" name="business_fax" placeholder="Business Fax">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Web Address : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="website" name="website" placeholder="Website address">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
                        <h3 class="Libre fsc-reg-sub-header">Contact Person Information :</h3>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Name : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="first_name" name="first_name" placeholder="First Name" onkeypress="putaboveData()" onblur="putaboveData()">
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="middle_name" name="middle_name" placeholder="Middle Name" onkeypress="putaboveData()" onblur="putaboveData()">
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="last_name" name="last_name" placeholder="Last Name" onkeypress="putaboveData()" onblur="putaboveData()">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Mobile / Cell No. : </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="mobile_no" name="mobile_no" placeholder="Mobile No" maxlength="10">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Office Telephone : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="office_no" name="office_no" placeholder="Office Telephone">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                            <input type="checkbox" name="Same as business" id="sameasbusiness" onclick="sameasbussinessNo()" style="float: left; width: 15px; height: 15px;"><h4 style="margin-top: 5px; margin-left: 20px; font-size: 1.2em;" class="Libre">SAME AS BUSINESS TELEPHONE</h4>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email Address : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="email" class="form-control fsc-input" id="email" name="email" placeholder="abc@abc.com" onkeypress="putaboveData()" onblur="putaboveData()">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
                        <h3 class="Libre fsc-reg-sub-header">Login Information :</h3>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                            <input type="checkbox" name="Same as business" id="sameasabove" style="float: left; width: 15px; height: 15px;" onclick="AboveInformation()"><h4 style="margin-top: 5px; margin-left: 20px; font-size: 1.2em;" class="Libre">SAME AS ABOVE INFORMATION</h4>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Name : </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="first_name1" name="first_name1" placeholder="First Name">
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="middle_name1" name="middle_name1" placeholder="Middle Name">
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="last_name1" name="last_name1" placeholder="Last Name">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Choose Password : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="password" name="password" class="form-control fsc-input" id="password" placeholder="*********">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Confirm Password : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="password" class="form-control fsc-input" id="cpassword" name="cpassword" placeholder="**********">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="email" class="form-control fsc-input" id="email1" name='email1' placeholder="abc@abc.com">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
                        <h3 class="Libre fsc-reg-sub-header">Security Question :</h3>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Question 1 : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="question1" id="question1" class="form-control fsc-input">
                                    <option value=''>Select Security Question</option>
                                    <option value='What is your pet name?'>What is your pet name?</option>
                                    <option value='What was the name of your first school?'>What was the name of your first school?</option>
                                    <option value='Who was your childhood hero?'>Who was your childhood hero?</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Answer 1 : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="answer1" name="answer1" placeholder="Answer">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Question 2 :</label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="question2" id="question2" class="form-control fsc-input">
                                    <option value=''>Select Security Question</option>
                                    <option value='What is your pet name?'>What is your pet name?</option>
                                    <option value='What was the name of your first school?'>What was the name of your first school?</option>
                                    <option value='Who was your childhood hero?'>Who was your childhood hero?</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Answer 2 : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="answer2" name='answer2' placeholder="Answer">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Comments : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <textarea class="form-control fsc-input" rows="3" id="comment" name='comment' placeholder="Comments"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="g-recaptcha" data-sitekey="6LcQbiAUAAAAABPESGB-JkGkGyQIHNCj7Xql012x"></div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-lg fsc-form-submit">SUBMIT</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="reset" class="btn btn-default btn-lg fsc-form-submit">RESET</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>

                </div>
            </form>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            //called when key is pressed in textbox
            $("#business_no,#office_no,#zip,#mobile_no,#business_fax,#office_no").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });

        function formSubmit() {
            var isValid = $("#registrationForm").validate();
            if (!isValid) return false;
            $("#dangerError").hide();
            $("#successError").hide();
            var request = $.ajax({
                url: "http://166.62.89.209/financialservicecenter/index.php/registration/formSubmit",
                method: "POST",
                data: $("#registrationForm").serialize(),
                dataType: "json"
            });
            request.done(function (data, textStatus, jqXHR) {
                if (data.success == "true") {
                    $("#successError").html(data.message).show();
                    $("#dangerError").hide();
                    $('#registrationForm')[0].reset();
                } else {
                    $("#dangerError").html(data.message).show();
                    $("#successError").hide();
                }
            });
            request.fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
            });
        }

        $("#registrationForm").validate({
            rules: {
                company_name: "required",
                business_name: "required",
                address1: "required",
                city: "required",
                zip: "required",
                stateId: "required",
                countryId: "required",
                first_name: "required",
                middle_name: "required",

                last_name: "required",
                business_no: "required",
                password: "required",
                cpassword: "required",
                question1: "required",
                answer1: "required",
                cpassword: {
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                emailId: "Please enter a valid email address"
            },
            submitHandler: formSubmit
        });

        function sameasbussinessNo() {
            var val = $("#sameasbusiness").prop('checked');
            if (val == true) {
                var business_no = $("#business_no").val();
                $("#office_no").val(business_no);
                $("#office_no").attr("readonly", "readonly");
            } else {
                $("#office_no").removeAttr("readonly", "readonly");
            }
        }

        function PutNo() {
            var val = $("#sameasbusiness").prop('checked');
            if (val == true) {
                var business_no = $("#business_no").val();
                $("#office_no").val(business_no);
            }
        }

        function AboveInformation() {
            var val = $("#sameasabove").prop('checked');
            if (val == true) {
                var first_name = $("#first_name").val();
                var middle_name = $("#middle_name").val();
                var last_name = $("#last_name").val();
                var email = $("#email").val();
                $("#first_name1").val(first_name);
                $("#first_name1").attr("readonly", "readonly");
                $("#middle_name1").val(middle_name);
                $("#middle_name1").attr("readonly", "readonly");
                $("#last_name1").val(last_name);
                $("#last_name1").attr("readonly", "readonly");
                $("#email1").val(email);
                $("#email1").attr("readonly", "readonly");
            } else {
                $("#first_name1").removeAttr("readonly", "readonly");
                $("#middle_name1").removeAttr("readonly", "readonly");
                $("#last_name1").removeAttr("readonly", "readonly");
                $("#email1").removeAttr("readonly", "readonly");
            }
        }

        function putaboveData() {
            var val = $("#sameasabove").prop('checked');
            if (val == true) {
                var first_name = $("#first_name").val();
                var middle_name = $("#middle_name").val();
                var last_name = $("#last_name").val();
                var email = $("#email").val();
                $("#first_name1").val(first_name);
                $("#middle_name1").val(middle_name);
                $("#email1").val(email);
                $("#last_name1").val(last_name);
            }
        }
    </script>

@endsection()