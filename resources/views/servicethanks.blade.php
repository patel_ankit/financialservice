@extends('front-section.app')
@section('main-content')

    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <div class="jumbotron text-xs-center">

                    <p class="lead">Your Inquiry successfully sent.</p>

                    <p class="lead">We will get back to you soon</p>
                    <hr>
                    <h2 class="display-3">Thanks! <i class="fa fa-smile-o" aria-hidden="true" style="color: #ffbe00;"></i>
                    </h2>

                </div>

            </div>
        </div>

    </div>
@endsection()