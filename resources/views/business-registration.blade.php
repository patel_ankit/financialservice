@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>BUSINESS REGISTRATION</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div">
            @foreach($category as $cat)
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
                    <a href="@if($cat->link == 'comman-registration/create') {{url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])}}@else {{url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])}} @endif "><img class="img-responsive" src="{{URL::asset('public/category/')}}/{{$cat->business_cat_image}}"/></a>
                    <center>
                        <div class="services-tab">
                            <a href="@if($cat->link == 'comman-registration/create'){{url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])}} @else{{url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])}} @endif">
                  <span>
                     <div class="st_title">{{$cat->business_cat_name}}</div>
                  </span>
                            </a>
                        </div>
                    </center>
                </div>
            @endforeach()
        </div>
    </div>
@endsection()