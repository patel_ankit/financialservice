@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="padding:0px;">
        @foreach($homecontent as $home)
            <div class="well well-lg" style="margin-top: 1%;">
                <h4 class="fsc-home-text-head" style='text-align:left'>{{$home->title}}</h4>
                <h4 class="fsc-home-text">{!!$home->content!!}</h4>
            </div>
        @endforeach
        <div class="fsc-well" style="margin-top:3%; width:100%;">
            <a href="https://financialservicecenter.net/what-we-do" class="fsc-home-wedo-head">This is what we do.</a>
        </div>
    </div>
@endsection