<!--footer-->
<div class="footer">
    <div class="container">
        <div class="col-md-6 bottom-head">
            <h2><a href="">Anchor</a></h2>
            <span class="cap">Lorem Ipsum</span>
        </div>
        <div class="col-md-6 copyright">
            <p>&copy; 2017 Anchor . All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank"> W3layouts </a></p>
            <div class="icons">
                <ul>
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-rss"></span></a></li>
                    <li><a href="#"><span class="fa fa-vk"></span></a></li>
                </ul>
            </div>
        </div>
        <!-- //Copyright -->
        <div class="clearfix"></div>
    </div>
</div>
<!--//footer-->
<!-- js -->
<script type='text/javascript' src='{{URL :: asset('js/jquery-2.2.3.min.js')}}'></script>
<!-- //js -->

<!--menu script-->
<script type="text/javascript" src="{{URL :: asset('js/modernizr-2.6.2.min.js')}}"></script>
<script src="{{URL :: asset('js/bootstrap.min.js')}}"></script>
<!--navbar script-->
<script src="{{URL :: asset('js/jquery.fatNav.js')}}"></script>
<script>
    (function () {

        $.fatNav();

    }());
</script>
<!--banner slider script-->
<script src="{{URL :: asset('js/responsiveslides.min.js')}}"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>

<!--client carousel -->
<script src="{{URL :: asset('js/owl.carousel.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            items: 1,
            itemsDesktop: [768, 1],
            itemsDesktopSmall: [414, 1],
            lazyLoad: true,
            autoPlay: true,
            navigation: true,

            navigationText: false,
            pagination: true,

        });

    });
</script>
<!-- //carousel -->
<!-- gallery-pop-up -->
<script src="{{URL :: asset('js/jquery.chocolat.js')}}"></script>
<link rel="stylesheet" href="{{URL :: asset('css/chocolat.css')}}" type="text/css" media="screen">
<!--light-box-files -->
<script type="text/javascript">
    $(function () {
        $('.agileinfo_portfolio_grid a').Chocolat();
    });
</script>
<!-- //gallery-pop-up -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{URL :: asset('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{URL :: asset('js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->

<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function () {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>
<!-- //here ends scrolling icon -->
</body>
</html>