<title>Home</title>
<!--meta tags -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Anchor Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
<script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
<!--//meta tags ends here-->
<!--booststrap-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!--//booststrap end-->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- menu -->
<link type="text/css" rel="stylesheet" href="css/cm-overlay.css"/>
<!-- //menu -->
<link href="{{URL :: asset('css/jquery.fatNav.css')}}" rel="stylesheet" type="text/css">

<!--stylesheets-->
<link href="{{URL :: asset('css/style.css')}}" rel='stylesheet' type='text/css' media="all">
<link rel="stylesheet" href="{{URL :: asset('css/owl.carousel.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{URL :: asset('css/lightbox.css')}}">

<!--//style sheet end here-->
<link href="//fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">