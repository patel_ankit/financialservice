@foreach($gallery as $gal)
    @if($gal->status=='0')
        <div class="col-md-4 agileinfo_portfolio_grid project">
            <a href="galleries/{{$gal->image}}" class="lsb-preview" data-lsb-group="header">
                <div class="agileits_portfolio_grid project_icon">
                    <img src="galleries/{{$gal->image}}" alt="{{$gal->title}}" title="{{$gal->title}}" class="img-responsive"/>
                </div>
            </a>
        </div>
    @endif
@endforeach