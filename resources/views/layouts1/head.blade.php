<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{URL::asset('public/dashboard/images/favicon.png')}}" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/bootstrap-combined.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/custom.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/responsive.css')}}">
<title>FSC - User</title>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/pace.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/main.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/clientcss/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/clientcss/css/ionicons.min.css')}}">
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/password.validation.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('public/dashboard/js/jquery.maskedinput.js')}}"></script>
<script src="{{URL::asset('public/dashboard/js/jquery.maskedinput.min.js')}}"></script>
<style>.modal-dialog {
        width: 60%;
        margin: 30px auto;
    }

    .sw-main .sw-container {
        z-index: 9
    }

    footer p {
        text-align: center;
        margin-top: 20px;
        color: #054b94;
        font-size: 17px;
    }</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location;

// for sidebar menu but not for treeview submenu
        $('ul.sidebar-menu a').filter(function () {
            return this.href == url;
        }).parent().siblings().removeClass('active').end().addClass('active');

// for treeview which is like a submenu
        $('ul.treeview-menu a').filter(function () {
            return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
    });
</script>

<script>
    $("#business_no").mask("(999) 999-9999");
    $(".ext").mask("999");
    $("#business_fax").mask("(999) 999-9999");
    $(".business_fax1").mask("(999) 999-9999");
    $(".residence_fax").mask("(999) 999-9999");
    $("#mobile_no").mask("(999) 999-9999");
    $(".cell").mask("(999) 999-9999");
    $(".telephone").mask("(999) 999-9999");
    $(".usapfax").mask("(999) 999-9999");
    $("#zip").mask("9999");
    $("#mailing_zip").mask("9999");
    $("#bussiness_zip").mask("9999");
</script>

@guest
@else
    @if(Auth::user()->active==0 )
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap.validation.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/jquery.bootstrap.wizard.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap-formhelpers.min.js')}}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#myModal").modal({
                    backdrop: 'static',
                    keyword: 'false',
                    show: true,
                });
            });
        </script>
        <style type="text/css">
            #registrationForm .tab-content {
                margin-top: 20px;
            }
        </style>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        @if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                        <form enctype='multipart/form-data' action="{{url('home',Auth::user()->id)}}" id="registrationForm" method="post">
                            <!-- SmartWizard html -->
                            {{csrf_field()}} {{method_field('PATCH')}}


                            <div id="smartwizard">
                                <ul class="nav nav-pills">
                                    <li class="active"><a href="#step-1" data-toggle="tab">Login Information</a></li>
                                    <li><a href="#step-2" data-toggle="tab">General Information</a></li>
                                    <li><a href="#step-3" data-toggle="tab">Personal Information</a></li>
                                </ul>
                                <br>
                                <div class="tab-content">
                                    <div class="tab-pane active" data-step="0" id="step-1">
                                        <div id="form-step-0" role="form" data-toggle="validator">
                                            @if ( session()->has('success') )
                                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                                            @endif
                                            @if ( session()->has('error') )
                                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="password">Current Password : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="password" class="form-control fsc-input" name="oldpassword" id="oldpassword" placeholder="Enter Your Current Password" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="newpassword">New Password : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="password" class="form-control fsc-input" name="newpassword" id="newpassword" placeholder="Enter Your New Password" required>
                                                        <div class="help-block with-errors"></div>
                                                        <div id="messages"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="newpassword">Confirm Password : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="password" class="form-control fsc-input" name="cpassword" id="cpassword" placeholder="Enter Your Confirm Password" required>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="newpassword">Reset Days : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="resetdays" value="" id="resetdays" class="form-control">
                                                            @if(empty(Auth::user()->resetdays))
                                                                <option>---Select Reset Days---</option> @endif
                                                            <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                                            <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                                            <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question1">Question 1 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question1" id="question1" class="form-control fsc-input" required>
                                                            <option value="">---Select---</option>
                                                            <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                            <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                            <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                                            <option value="In what city were you born?">In what city were you born?</option>
                                                            <option value="What is the name of your first school?">What is the name of your first school?</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer1">Answer 1:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer1" value="" required type="text" id="answer1" class="form-control fsc-input">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question2">Question 2:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question2" id="question2" required class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            <option value="What is your favorite movie?">What is your favorite movie?</option>
                                                            <option value="What was the make of your first car?">What was the make of your first car?</option>
                                                            <option value="What is your favorite color?">What is your favorite color?</option>
                                                            <option value="What is your father's middle name?">What is your father's middle name?</option>
                                                            <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer2">Answer 2:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer2" value="" required type="text" id="answer2" class="form-control fsc-input">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="question3">Question 3: </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="question3" id="question3" required class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            <option value="What was your high school mascot?">What was your high school mascot?</option>
                                                            <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                                            <option value="In what year was your father born?">In what year was your father born?</option>
                                                            <option value="What is the name of your favorite childhood friend?">What is the name of your favorite childhood friend?</option>
                                                            <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label" for="answer3">Answer 3:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input name="answer3" value="" required type="text" id="answer3" class="form-control fsc-input">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="step-2" data-step="2">
                                        <div id="form-step-2" role="form" data-toggle="validator">
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="business_id" id="business_id" class="form-control fsc-input category" required>
                                                            @foreach($business as $bus)
                                                                @if($common->business_id==$bus->id)
                                                                    <option value="{{$common->business_id}}" selected="">{{$common->bussiness_name}}</option>
                                                                @else
                                                                    <option value="{{$bus->id}}">{{$bus->bussiness_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Category Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="business_cat_id" id="business_cat_id" class="form-control fsc-input category1" required>
                                                            @foreach($category as $cat)
                                                                @if($common->business_cat_id==$cat->id)
                                                                    <option value="{{$common->business_cat_id}}" selected="">{{$common->business_cat_name}}</option>
                                                                @else
                                                                    <option value="{{$cat->id}}">{{$cat->business_cat_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Brand Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="business_brand_id" id="business_brand_id" class="form-control fsc-input" required>
                                                            @foreach($businessbrand as $brand)
                                                                @if($common->business_cat_id==$brand->id)
                                                                    <option value="{{$common->business_brand_id}}" selected="">{{$common->business_brand_name}}</option>
                                                                @else
                                                                    <option value="{{$brand->id}}">{{$brand->business_brand_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Brand Category Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="business_brand_category_id" id="business_brand_category_id" required class="form-control fsc-input">
                                                            @foreach($cd as $cd1)
                                                                @if($common->business_brand_category_id==$cd1->id)
                                                                    <option value="{{$common->business_brand_category_id}}" selected="">{{$common->business_brand_category_name}}</option>
                                                                @else
                                                                    <option value="{{$cd1->id}}">{{$cd1->business_brand_category_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Company Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="company_name" name="company_name" value="{{$common->company_name}}" required placeholder="Company Name">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="business_name" name="business_name" value="{{$common->business_name}}" placeholder="Business Name" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;height: 51px;margin: 20px 0;">
                                                <h3 class="Libre fsc-reg-sub-header">Business Location :</h3>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Name : </label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="first_name1" name="first_name" placeholder="First Name" value="{{$common->first_name}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="middle_name1" name="middle_name" placeholder="Middle Name" value="{{$common->middle_name}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="last_name1" name="last_name" placeholder="Last Name" value="{{$common->last_name}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Email : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="email" readonly class="form-control fsc-input" id="email1" name="email" placeholder="abc@abc.com" value="{{$common->email}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address 1 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="address" name="address" placeholder="Enter Your Address" value="{{$common->address}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address 2 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="address1" name="address1" placeholder="Enter Your Address" value="{{$common->address1}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$common->city}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="stateId" id="stateId" class="form-control fsc-input" required>
                                                            <option value="{{$common->stateId}}" selected>{{$common->stateId}}</option>
                                                            <option value="ID">ID</option>
                                                            <option value="AK">AK</option>
                                                            <option value="AS">AS</option>
                                                            <option value="AZ">AZ</option>
                                                            <option value="AR">AR</option>
                                                            <option value="CA">CA</option>
                                                            <option value="CO">CO</option>
                                                            <option value="CT">CT</option>
                                                            <option value="DE">DE</option>
                                                            <option value="DC">DC</option>
                                                            <option value="FM">FM</option>
                                                            <option value="FL">FL</option>
                                                            <option value="GA">GA</option>
                                                            <option value="GU">GU</option>
                                                            <option value="HI">HI</option>
                                                            <option value="ID">ID</option>
                                                            <option value="IL">IL</option>
                                                            <option value="IN">IN</option>
                                                            <option value="IA">IA</option>
                                                            <option value="KS">KS</option>
                                                            <option value="KY">KY</option>
                                                            <option value="LA">LA</option>
                                                            <option value="ME">ME</option>
                                                            <option value="MH">MH</option>
                                                            <option value="MD">MD</option>
                                                            <option value="MA">MA</option>
                                                            <option value="MI">MI</option>
                                                            <option value="MN">MN</option>
                                                            <option value="MS">MS</option>
                                                            <option value="MO">MO</option>
                                                            <option value="MT">MT</option>
                                                            <option value="NE">NE</option>
                                                            <option value="NV">NV</option>
                                                            <option value="NH">NH</option>
                                                            <option value="NJ">NJ</option>
                                                            <option value="NM">NM</option>
                                                            <option value="NY">NY</option>
                                                            <option value="NC">NC</option>
                                                            <option value="ND">ND</option>
                                                            <option value="MP">MP</option>
                                                            <option value="OH">OH</option>
                                                            <option value="OK">OK</option>
                                                            <option value="OR">OR</option>
                                                            <option value="PW">PW</option>
                                                            <option value="PA">PA</option>
                                                            <option value="PR">PR</option>
                                                            <option value="RI">RI</option>
                                                            <option value="SC">SC</option>
                                                            <option value="SD">SD</option>
                                                            <option value="TN">TN</option>
                                                            <option value="TX">TX</option>
                                                            <option value="UT">UT</option>
                                                            <option value="VT">VT</option>
                                                            <option value="VI">VI</option>
                                                            <option value="VA">VA</option>
                                                            <option value="WA">WA</option>
                                                            <option value="WV">WV</option>
                                                            <option value="WI">WI</option>
                                                            <option value="WY">WY</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" maxlength="4" id="zip" name="zip" placeholder="Postel Code" value="{{$common->zip}}" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="countryId" id="countryId" class="form-control fsc-input" required>
                                                            <option value="{{$common->countryId}}">{{$common->countryId}}</option>
                                                            <option value="USA">USA</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Mobile / Cell No. : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input bfh-phone" data-format="+1 (ddd) ddd-dddd" id="mobile_no" value="{{$common->mobile_no}}" name="mobile_no" placeholder="Mobile No">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Tele. : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input bfh-phone" id="business_no" data-format="+1 (ddd) ddd-dddd" value="{{$common->business_no}}" name="business_no" placeholder="Business Telephone" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Fax : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input bfh-phone" value="{{$common->business_fax}}" id="business_fax" data-format="+1 (ddd) ddd-dddd" name="business_fax" placeholder="Business Fax" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Web Address : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="website" value="{{$common->website}}" name="website" placeholder="Website address" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <select style="visibility:hidden;" name="user_type" id="user_type" class="form-control fsc-input category1">
                                                <option value='{{$common->user_type}}' selected>{{$common->user_type}}</option>
                                            </select>
                                            <input type="hidden" class="form-control fsc-input" readonly="" id="active" value="1" name="active" placeholder="">
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="step-3" data-step="3">
                                        <div id="form-step-3" role="form" data-toggle="validator">
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Gender : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">

                                                        <div class="col-md-4 radio-inline">
                                                            <label class="control-label-popup"><input type="radio" name="gender" value="Male" id="gneder" checked="checked"> Male</label>
                                                        </div>
                                                        <div class="col-md-4 radio-inline">
                                                            <label class="control-label-popup"><input type="radio" name="gender" value="Female" id="gender"> Female</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Marital Status : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="marital" id="marital" class="form-control fsc-input" required>
                                                            <option value="">Select Marital Status</option>
                                                            <option value="married">Married</option>
                                                            <option value="unmarried">UnMarried</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Date Of Birth : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select class="form-control fsc-input" name="month" id="month">
                                                            <option value="">Month</option>
                                                            <option value="Jan">Jan</option>
                                                            <option value="Feb">Feb</option>
                                                            <option value="Mar">Mar</option>
                                                            <option value="Apr">Apr</option>
                                                            <option value="May">May</option>
                                                            <option value="Jun">Jun</option>
                                                            <option value="Jul">Jul</option>
                                                            <option value="Aug">Aug</option>
                                                            <option value="Sep">Sep</option>
                                                            <option value="Oct">Oct</option>
                                                            <option value="Nov">Nov</option>
                                                            <option value="Dec">Dec</option>
                                                        </select>

                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="day" id="day" class="form-control fsc-input" required>
                                                            <?php
                                                            for ($i = 1; $i <= 31; $i++) {
                                                                $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                                                echo "<option value='$i'>$i</option>";
                                                            }
                                                            ?>
                                                        </select>

                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <?php
                                                        // Sets the top option to be the current year. (IE. the option that is chosen by default).
                                                        $currently_selected = date('Y');
                                                        // Year to start available options at
                                                        $earliest_year = 1950;
                                                        // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
                                                        $latest_year = date('Y');

                                                        print '<select class="form-control fsc-input"  id="year" name="year">';
                                                        // Loops over each int[year] from current year, back to the $earliest_year [1950]
                                                        foreach (range($latest_year, $earliest_year) as $i) {
                                                            // Prints the option with the next year in range.
                                                            print '<option value="' . $i . '"' . ($i === $currently_selected ? ' selected="selected"' : '') . '>' . $i . '</option>';
                                                        }
                                                        print '</select>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">ID Proof 1 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="eteletype3" id="eteletype3" class="form-control fsc-input" required>
                                                            <option value="">Select Proof</option>
                                                            <option value="Voter Id">Voter Id</option>
                                                            <option value="Driving Licence">Driving Licence</option>
                                                            <option value="Pan Card">Pan Card</option>
                                                            <option value="Pass Port">Pass Port</option>
                                                        </select>

                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="file" class="form-control fsc-input" id="pf1" name="pf1" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">ID Proof 2 : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="eteletype4" id="eteletype4" class="form-control fsc-input" required>
                                                            <option value="">Select Proof</option>
                                                            <option value="Voter Id">Voter Id</option>
                                                            <option value="Driving Licence">Driving Licence</option>
                                                            <option value="Pan Card">Pan Card</option>
                                                            <option value="Pass Port">Pass Port</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="file" class="form-control fsc-input" id="pf2" name="pf2">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;height: 51px;margin: 20px 0;">
                                                <h3 class="Libre fsc-reg-sub-header">Emergency Contact Info</h3>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Person Name : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="personname" name="personname" value="" required placeholder="Please Enter Your Person Name">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Relationship : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="relation" name="relation" value="" placeholder="Please Enter Relation.." required>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Address : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="eaddress1" name="eaddress1" placeholder="Please Enter Your Address" value="" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="per_city" name="per_city" placeholder="City" value="" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <select name="per_stateId" id="per_stateId" class="form-control fsc-input" required>

                                                            <option value="ID">ID</option>
                                                            <option value="AK">AK</option>
                                                            <option value="AS">AS</option>
                                                            <option value="AZ">AZ</option>
                                                            <option value="AR">AR</option>
                                                            <option value="CA">CA</option>
                                                            <option value="CO">CO</option>
                                                            <option value="CT">CT</option>
                                                            <option value="DE">DE</option>
                                                            <option value="DC">DC</option>
                                                            <option value="FM">FM</option>
                                                            <option value="FL">FL</option>
                                                            <option value="GA">GA</option>
                                                            <option value="GU">GU</option>
                                                            <option value="HI">HI</option>
                                                            <option value="ID">ID</option>
                                                            <option value="IL">IL</option>
                                                            <option value="IN">IN</option>
                                                            <option value="IA">IA</option>
                                                            <option value="KS">KS</option>
                                                            <option value="KY">KY</option>
                                                            <option value="LA">LA</option>
                                                            <option value="ME">ME</option>
                                                            <option value="MH">MH</option>
                                                            <option value="MD">MD</option>
                                                            <option value="MA">MA</option>
                                                            <option value="MI">MI</option>
                                                            <option value="MN">MN</option>
                                                            <option value="MS">MS</option>
                                                            <option value="MO">MO</option>
                                                            <option value="MT">MT</option>
                                                            <option value="NE">NE</option>
                                                            <option value="NV">NV</option>
                                                            <option value="NH">NH</option>
                                                            <option value="NJ">NJ</option>
                                                            <option value="NM">NM</option>
                                                            <option value="NY">NY</option>
                                                            <option value="NC">NC</option>
                                                            <option value="ND">ND</option>
                                                            <option value="MP">MP</option>
                                                            <option value="OH">OH</option>
                                                            <option value="OK">OK</option>
                                                            <option value="OR">OR</option>
                                                            <option value="PW">PW</option>
                                                            <option value="PA">PA</option>
                                                            <option value="PR">PR</option>
                                                            <option value="RI">RI</option>
                                                            <option value="SC">SC</option>
                                                            <option value="SD">SD</option>
                                                            <option value="TN">TN</option>
                                                            <option value="TX">TX</option>
                                                            <option value="UT">UT</option>
                                                            <option value="VT">VT</option>
                                                            <option value="VI">VI</option>
                                                            <option value="VA">VA</option>
                                                            <option value="WA">WA</option>
                                                            <option value="WV">WV</option>
                                                            <option value="WI">WI</option>
                                                            <option value="WY">WY</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" maxlength="4" id="per_zip" name="per_zip" placeholder="Postel Code" value="" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group" id="test">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 1 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text" name="etelephone1" placeholder="+1(000) 000 0000" value="" class="form-control fsc-input bfh-phone" data-format="+1 (ddd) ddd-dddd" id="etelephone1">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="eteletype1" class="form-control fsc-input" id="eteletype1">
                                                                    <option value="Mobile">Mobile</option>
                                                                    <option value="Resident">Resident</option>
                                                                    <option value="Office">Office</option>
                                                                    <option value="Other">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" id="eext5" name="eext5" class="form-control fsc-input" disabled="disabled" placeholder="Ext" value="">
                                                                <input type="text" id="eext1" name="eext1" class="form-control fsc-input" placeholder="Ext" value="031">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone 2 : </label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text" name="etelephone2" placeholder="+1(000) 000 0000" value="" class="form-control fsc-input bfh-phone" data-format="+1 (ddd) ddd-dddd" id="etelephone2">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="eteletype2" class="form-control fsc-input" id="eteletype2">
                                                                    <option value="Mobile">Mobile</option>
                                                                    <option value="Resident">Resident</option>
                                                                    <option value="Office">Office</option>
                                                                    <option value="Other">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" id="eext2" name="eext2" class="form-control fsc-input" placeholder="Ext" value="031">
                                                                <input type="text" id="eext6" name="eext6" class="form-control fsc-input" disabled="disabled" placeholder="Ext" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Note For Emergency : <span class="star-required">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="comments" name="comments" placeholder="Enter Your Address" value="" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                                    <input type="submit" value="Save" class="pull-right btn btn-success">
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first" style="display:none;"><a href="#">First</a></li>
                                <li class="previous"><a href="#">Previous</a></li>
                                <li class="next last" style="display:none;"><a href="#">Last</a></li>
                                <li class="next"><a href="#">Next</a></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $.ajaxSetup({
                headers:
                    {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    }
            });
            $(document).ready(function () {
                function adjustIframeHeight() {
                    var $body = $('body'),
                        $iframe = $body.data('iframe.fv');
                    if ($iframe) {
                        // Adjust the height of iframe
                        $iframe.height($body.height());
                    }
                }

                $('#registrationForm').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    excluded: ':disabled',
                    fields: {


                        personname: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Person Name'
                                },
                            }
                        },
                        relation: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Relation'
                                },
                            }
                        },
                        eaddress1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Address'
                                },
                            }
                        },
                        per_city: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter City'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The City can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        per_stateId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select State'
                                },
                            }
                        },
                        per_zip: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Country'
                                },
                            }
                        },
                        comments: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Comments'
                                },
                            }
                        },
                        etelephone1: {
                            validators: {
                                stringLength: {
                                    min: 15,

                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Phone No.'
                                },

                                etelephone1: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                },
                            }
                        },
                        eteletype1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Phone Type'
                                },
                            }
                        },
                        etelephone2: {
                            validators: {
                                stringLength: {
                                    min: 15,

                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Phone No.'
                                },
                                etelephone2: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }
                            }
                        },
                        eteletype2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Phone Type'
                                },
                            }
                        },


                        newpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and cannot be empty'
                                },
                                regexp:
                                    {

                                        regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

                                        message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                    },
                                different: {
                                    field: 'oldpassword',
                                    message: 'The password cannot be the same as Current Password'
                                }
                            }
                        },
                        oldpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Current Password'
                                },
                                remote: {
                                    message: 'The Password is not available',
                                    url: '{{ URL::to('/checkpassword') }}',
                                    data: {
                                        type: 'oldpassword'
                                    },
                                    type: 'POST'
                                }
                            }
                        },
                        cpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The confirm password is required and can\'t be empty'
                                },
                                identical: {
                                    field: 'newpassword',
                                    message: 'The password and its confirm are not the same'
                                },
                                different: {
                                    field: 'oldpassword',
                                    message: 'The password can\'t be the same as Old Password'
                                }
                            }
                        },
                        pf1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please upload Your Proof'
                                },
                                pf1: {
                                    extension: 'doc,docx,pdf,zip,rtf',
                                    type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/zip',
                                    maxSize: 5 * 1024 * 1024,
                                    message: 'The selected file is not valid, it should be (doc,docx,pdf,zip,rtf) and 5 MB at maximum.'
                                }


                            }
                        },
                        pf2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please upload Your Proof'
                                },
                                pf2: {
                                    extension: 'doc,docx,pdf,zip,rtf',
                                    type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/zip',
                                    maxSize: 5 * 1024 * 1024,
                                    message: 'The selected file is not valid, it should be (doc,docx,pdf,zip,rtf) and 5 MB at maximum.'
                                }


                            }
                        },
                        question1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 1'
                                }
                            }
                        },
                        question2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 2'
                                }
                            }
                        },
                        question3: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Question 3'
                                }
                            }
                        },
                        answer1: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 1'
                                }
                            }
                        },
                        answer2: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 2'
                                }
                            }
                        },
                        answer3: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Answer 3'
                                }
                            }
                        },
                        business_id: {
                            validators: {
                                message: 'Please Select Your Business Name',
                            }
                        },
                        business_cat_id: {
                            validators: {
                                message: 'Please Select Your Business Category Name',
                            }
                        },
                        business_brand_id: {
                            validators: {
                                message: 'Please Select Your Business Brand Name',
                            }
                        },
                        business_brand_category_id: {
                            validators: {
                                message: 'Please Select Your Business Brand Category Name',
                            }
                        },
                        company_name: {
                            validators: {
                                message: 'Please Select Your Company Name',
                            }
                        },
                        business_name: {
                            validators: {
                                message: 'Please Select Your Business Name',
                            }
                        },
                        first_name1: {
                            validators: {
                                stringLength: {
                                    min: 2,
                                },
                                notEmpty: {
                                    message: 'Please Enter Your First Name'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The First Name can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        middle_name1: {
                            validators: {
                                stringLength: {
                                    min: 2,
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Middle Name'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The Middle name can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        last_name1: {
                            validators: {
                                stringLength: {
                                    min: 2,
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Last Name'
                                },

                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The Last name can consist of alphabetical characters and spaces only'
                                }
                            }
                        },

                        address: {
                            validators: {
                                stringLength: {
                                    min: 8,
                                    message: 'Please Enter Your 8 Charactor'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Address'
                                }
                            }
                        },
                        address1: {
                            validators: {
                                stringLength: {
                                    min: 8,
                                    message: 'Please Enter Your 8 Charactor'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Address'
                                }
                            }
                        },
                        city: {
                            validators: {
                                stringLength: {
                                    min: 2,

                                },
                                notEmpty: {
                                    message: 'Please Enter Your City'
                                },
                                regexp: {
                                    regexp: /^[a-z\s]+$/i,
                                    message: 'The City can consist of alphabetical characters and spaces only'
                                }
                            }
                        },
                        stateId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your State'
                                }
                            }
                        },
                        countryId: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Select Your Country'
                                }
                            }
                        },
                        zip: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Zip Code'
                                }
                            }
                        },
                        business_no: {
                            validators: {
                                stringLength: {
                                    min: 15,
                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Business Phone Number'
                                },
                                business_no: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }
                            }
                        },
                        mobile_no: {
                            validators: {
                                stringLength: {
                                    min: 15,
                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Mobile Number'
                                }, mobile_no: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }
                            }
                        },
                        business_fax: {
                            validators: {
                                stringLength: {
                                    min: 15,
                                    message: 'Please enter at least 10 characters and no more than 10'
                                },
                                notEmpty: {
                                    message: 'Please Enter Your Business Fax'
                                }, business_fax: {
                                    country: 'USA',
                                    message: 'Please supply a vaild phone number with area code'
                                }

                            }
                        },
                        website: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Website Address'
                                },
                                uri: {
                                    message: 'The  Website Address Is Not Valid'
                                }
                            }
                        },
                    }
                }).bootstrapWizard({
                    tabClass: 'nav nav-pills',
                    onTabClick: function (tab, navigation, index) {
                        return validateTab(index);
                    },
                    onNext: function (tab, navigation, index) {
                        var numTabs = $('#registrationForm').find('.tab-pane').length,
                            isValidTab = validateTab(index - 1);
                        if (!isValidTab) {
                            return false;
                        }

                        if (index === numTabs) {
                            $('#completeModal').modal();
                        }

                        return true;
                    },
                    onPrevious: function (tab, navigation, index) {
                        return validateTab(index + 1);
                    },
                    onTabShow: function (tab, navigation, index) {
                        // Update the label of Next button when we are at the last tab
                        var numTabs = $('#registrationForm').find('.tab-pane').length;
                        $('#registrationForm')
                            .find('.next')
                            .removeClass('disabled')    // Enable the Next button
                            .find('a')
                            .html(index === numTabs - 1 ? 'End' : 'Next');

                        // You don't need to care about it
                        // It is for the specific demo
                        adjustIframeHeight();
                    }
                });

                function validateTab(index) {
                    var fv = $('#registrationForm').data('formValidation'), // FormValidation instance
                        // The current tab
                        $tab = $('#registrationForm').find('.tab-pane').eq(index);

                    // Validate the container
                    fv.validateContainer($tab);

                    var isValidStep = fv.isValidContainer($tab);
                    if (isValidStep === false || isValidStep === null) {
                        // Do not jump to the target tab
                        return false;
                    }

                    return true;
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getcat1')!!}?id=' + id, function (data) {
                        $('#user_type').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#user_type').append('<option value="' + subcatobj.bussiness_name + '">' + subcatobj.bussiness_name + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();//alert(id);
                    $.get('{!!URL::to('getRequest2')!!}?id=' + id, function (data) {
                        $('#business_cat_id').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            $(function () {
                $('#eext1').hide();
                $('#eteletype1').change(function () {
                    if ($('#eteletype1').val() == 'Office') {
                        $('#eext1').show();
                        $('#eext5').hide();
                    } else {
                        $('#eext1').hide();
                        $('#eext5').show();
                    }
                });
            });
        </script>
        <script>
            $(function () {
                $('#eext2').hide();
                $('#eteletype2').change(function () {
                    if ($('#eteletype2').val() == 'Office') {
                        $('#eext2').show();
                        $('#eext6').hide();
                    } else {
                        $('#eext2').hide();
                        $('#eext6').show();
                    }
                });
            });
        </script>
    @endif
@endguest