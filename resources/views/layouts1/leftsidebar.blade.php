<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="active"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i><span>Client-Dashboard</span></a></li>

            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Profile</span><i class="fa fa-caret-right"></i></a>
                <ul class="treeview-menu">
                    <li class=""><a href="{{route('profile.index')}}"><i class="fa fa-user"></i><span>Edit Profile</span></a></li>
                    <li class=""><a href="{{route('userchangepassword.index')}}"><i class="fa fa-lock"></i><span>Change PW</span></a></li>
                </ul>
            </li>

            <li class="">
                <a href="#"><i class="fa fa-cog"></i><span>Project</span></a>

            </li>


            <li class="">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i><span>Log Out</span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>

        <!-- <li class="treeview">
						<a href="#"><i class="fa fa-user"></i><span>Client</span><i class="fa fa-angle-right"></i></a>
						<ul class="treeview-menu">
							<li><a href="{{url('/admin/customer')}}"><i class="fa fa-circle-o"></i> Service Inquiry</a></li>
						</ul>
					</li>-->

            <!--<li><a href="#"><i class="fa fa-file-text"></i><span>Forms</span></a></li>-->

            <!--<li class="treeview">
                <a href="#"><i class="fa fa-bullseye"></i><span>Submission</span><i class="fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Request Log</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Tax Issue Log</a></li>
                </ul>
            </li>-->

        <!--<li class="treeview">
						<a href="#"><i class="fa fa-th-large"></i><span>Business</span><i class="fa fa-angle-right"></i></a>
						<ul class="treeview-menu">
							<li><a href="{{url('/admin/business')}}"><i class="fa fa-circle-o"></i> Business</a></li>
							<li><a href="{{url('/admin/businesscategory')}}"><i class="fa fa-circle-o"></i> Business Category</a></li>
							<li><a href="{{url('/admin/business-brand')}}"><i class="fa fa-circle-o"></i> Business Brand</a></li>
							<li><a href="{{url('/admin/business-brand-category')}}"><i class="fa fa-circle-o"></i> Business Brand Category</a></li>
							<li><a href="{{url('/admin/company-register')}}"><i class="fa fa-circle-o"></i> Company Register</a></li>
						</ul>
					</li>-->

            <!--<li class="treeview">
                <a href="#"><i class="fa fa-cogs"></i><span>Setting</span><i class="fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Position</a></li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-link"></i><span>Links</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Federal</a></li>
                            <li><a href="#"><i class="fa fa-circle-o"></i> State</a></li>
                        </ul>
                    </li>
                </ul>
            </li>-->

            <!--	<li><a href="#"><i class="fa fa-pie-chart"></i><span>Presentation</span></a></li>-->
        </ul>
    </section>
</aside>