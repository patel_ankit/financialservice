@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Submission</h4>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                <h4>Tax Issue Log</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form id='TaxIssueLogForm' name="TaxIssueLogForm" method="post" enctype="multipart/form-data">

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                    <div class="alert alert-danger" style="display:none;margin-top: 10px;font-size: 15px" id="dangerError"></div>

                    <div class="alert alert-success" style="display:none;margin-top: 10px;font-size: 15px" id="successError"></div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Client No : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="clientno" name='clientno' placeholder="Client No">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Client Name : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="clientname" name='clientname' placeholder="Client Name">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Contact Name : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="contactname" name='contactname' placeholder="Contact Name">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Telephone Number : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" name='telephoneno' class="form-control fsc-input  bfh-phone" data-format="+1 (ddd) ddd-dddd" id="telephoneno" placeholder="TelephoneNo">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" class="form-control fsc-input" id="email" name='email' placeholder="Email Address">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Tax Issue For ? <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <select class="form-control fsc-input" id="taxissue" name="taxissue" onchange="taxissuedata()">
                                <option value=''>Please select</option>
                                <option value='Federal'>Federal</option>
                                <option value='StateDOR'>State DOR</option>
                                <option value='StateDOL'>State DOL</option>
                                <option value='County'>County</option>
                                <option value='Other'>Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Form : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <select class="form-control fsc-input" id="form" name="form">
                                <option value=''>Please select</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Which Time Period : </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <select class="form-control fsc-input" id="time" name="time">
                                <option value='JAN'>JAN</option>
                                <option value='FEB'>FEB</option>
                                <option value='MAR'>MAR</option>
                                <option value='APR'>APR</option>
                                <option value='MAY'>MAY</option>
                                <option value='JUN'>JUN</option>
                                <option value='JUL'>JUL</option>
                                <option value='AUG'>AUG</option>
                                <option value='SEP'>SEP</option>
                                <option value='OCT'>OCT</option>
                                <option value='NOV'>NOV</option>
                                <option value='DEC'>DEC</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <select class="form-control fsc-input" id="year" name="year">
                                <option value='2017'>2017</option>
                                <option value='2016'>2016</option>
                                <option value='2015'>2015</option>
                                <option value='2014'>2014</option>
                                <option value='2013'>2013</option>
                                <option value='2012'>2012</option>
                                <option value='2011'>2011</option>
                                <option value='2010'>2010</option>
                                <option value='2009'>2009</option>
                                <option value='2008'>2008</option>
                                <option value='2007'>2007</option>
                                <option value='2006'>2006</option>
                                <option value='2005'>2005</option>
                                <option value='2004'>2004</option>
                                <option value='2003'>2003</option>
                                <option value='2002'>2002</option>
                                <option value='2001'>2001</option>
                                <option value='2000'>2000</option>
                                <option value='1999'>1999</option>
                                <option value='1998'>1998</option>
                                <option value='1997'>1997</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">How you need ? <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                            <div class="dropdown">
                                <select name="HowYouNeed" id="HowYouNeed" class="form-control fsc-input" onchange='displayBox()'>
                                    <option value=''>Please select</option>
                                    <option value='By Email'>By Email</option>
                                    <option value='By Fax'>By Fax</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;display:none" id="byemail">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" name='Requestemail' class="form-control fsc-input" id="Requestemail" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;display:none" id="byfax">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Fax : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" name='Requestfax' class="form-control fsc-input" id="Requestfax" placeholder="Fax">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Comments : </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <textarea class="form-control fsc-input" rows="3" id="Comments" name='Comments' placeholder="Comments"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">File Upload : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="file" class="form-control fsc-input" id="file" name='file' placeholder="Select Document">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-lg fsc-form-submit">SUBMIT</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">RESET</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>

                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

            </form>
        </div>

    </div>
    <script>
        $.ajaxSetup({
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
        });
        $(document).ready(function () {
            $('#TaxIssueLogForm').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                fields: {
                    clientno: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Client NO.'
                            }
                        }
                    },
                    clientname: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Client Name'
                            }
                        }
                    },
                    contactname: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Contact Name'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The Legal Name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    dbaname: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your DBA Name'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The DBA Name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },

                    first_name: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your First Name'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The First Name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    middle_name: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Middle Name'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The Middle name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            stringLength: {
                                min: 1,
                            },
                            notEmpty: {
                                message: 'Please Enter Your Last Name'
                            },

                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The Last name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Email Address'
                            },
                            emailAddress: {
                                message: 'Please Enter Your Valid Email Address'
                            },

                            remote: {
                                url: "{{ URL::to('/check_unique') }}",
                                data: function (validator) {
                                    return {
                                        email: validator.getFieldElements('email').val()
                                    }
                                },
                                message: 'This Email Id ALready exit.'
                            }

                        }
                    },
                    address: {
                        validators: {
                            stringLength: {
                                min: 1,
                                message: 'Please Enter Your 8 Charactor'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Address'
                            }
                        }
                    },
                    /*address1: {
                        validators: {
                             stringLength: {
                                min: 1,
                                message: 'Please Enter Your 8 Charactor'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Address'
                            }
                        }
                    },*/
                    city: {
                        validators: {
                            stringLength: {
                                min: 1,

                            },
                            notEmpty: {
                                message: 'Please Enter Your City'
                            },
                            regexp: {
                                regexp: /^[a-z\s]+$/i,
                                message: 'The City can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    stateId: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your State'
                            }
                        }
                    },
                    countryId: {
                        validators: {
                            notEmpty: {
                                message: 'Please Select Your Country'
                            }
                        }
                    },
                    zip: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Zip Code'
                            }
                        }
                    },
                    telephoneno: {
                        validators: {
                            stringLength: {
                                min: 15,
                                message: 'Please enter at least 10 characters and no more than 10'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Business Phone Number'
                            },
                            telephoneno: {
                                country: 'USA',
                                message: 'Please supply a vaild phone number with area code'
                            }
                        }
                    },

                    /*business_fax: {
                        validators: {
                            stringLength: {
                                min: 15,
                                message:'Please enter at least 10 characters and no more than 10'
                            },
                            notEmpty: {
                                message: 'Please Enter Your Business Fax'
                            },business_fax: {
                                country: 'USA',
                                message: 'Please supply a vaild phone number with area code'
                            }

                        }
                    },
                    website: {
                        validators: {
                            notEmpty: {
                                message: 'Please Enter Your Website Address'
                            },
                            uri: {
                                message: 'The  Website Address Is Not Valid'
                            }
                        }
                    },*/

                }
            })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#TaxIssueLogForm').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();
                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        // console.log(result);
                    }, 'json');
                });
        });


    </script>



    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#TaxIssueLogForm").validate({
                rules: {
                    ClientNo: "required",
                    ClientName: "required",
                    ContactName: "required",
                    TelephoneNo: "required",
                    taxissue: "required",
                    form: "required",
                    HowYouNeed: "required",
                    file: "required",
                    Email: {
                        required: true,
                        email: true
                    },
                    InformationRequest: "required"
                },
                messages: {
                    email: "Please enter a valid email address"
                },
                submitHandler: function () {
                    SendTaxLog()
                }
            });
            $("#TelephoneNo").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });

        function SendTaxLog() {
            var formData = new FormData($('#TaxIssueLogForm')[0]);
            formData.append('file', $('input[type=file]')[0].files[0]);

            $("#successError").hide();
            $("#dangerError").hide();
            $.ajax({
                url: "http://166.62.89.209/financialservicecenter/index.php/Submission/Sendtaxissuelog",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    var response = jQuery.parseJSON(data);
                    if (response.success == "true") {
                        $("#successError").html(response.message).show();
                        $("#dangerError").hide();
                        $("#TaxIssueLogForm")[0].reset();
                        $("#form").html("<option value=''>Please select</option>");
                    } else if (response.success == "false") {
                        $("#dangerError").html(response.message).show();
                        $("#successError").hide();
                    }
                }
            });
        }

        function displayBox() {
            var HowYouNeed = $("#HowYouNeed").val();
            if (HowYouNeed == 'By Email') {
                $("#byemail").show();
                $("#byfax").hide();
            } else if (HowYouNeed == 'By Fax') {
                $("#byfax").show();
                $("#byemail").hide();
            } else {
                $("#byfax").hide();
                $("#byemail").hide();
            }
        }

        function taxissuedata() {
            var taxissue = $("#taxissue").val();
            if (taxissue != "") {
                var request = $.ajax({
                    url: "http://166.62.89.209/financialservicecenter/index.php/Submission/taxtissueData",
                    method: "POST",
                    data: {taxissue: taxissue},
                    dataType: "html"
                });
                request.done(function (data, textStatus, jqXHR) {
                    $("#form").html(data);
                });
                request.fail(function (jqXHR, textStatus) {
                    console.log("Request failed: " + textStatus);
                });
            } else {
                $("#form").html("<option value=''>Please select</option>");
            }
        }
    </script>
@endsection()