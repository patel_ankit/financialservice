<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>FSC</title>
</head>
<body>

<img src="https://financialservicecenter.net/public/frontcss/images/fsc_logo.png" alt="img">
<br><br>
<table border='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;'>
    <tr style='background:#535da6;'>
        <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Congratulations, <br>Your account is approved by FSC</td>
    </tr>

    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Client ID :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'>

            <p>
                {{ $client }}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Fullname :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'>

            <p>
                {{ $first_name}}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Email :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>

            <p>
                {{ $email }}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Username :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>

            <p>
                {{ $email }}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Password :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>

            <p>
                {{ $password }}
            </p>
        </td>
    </tr>


    <tr style='background:#535da6;'>
        <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Please click this link to update your profile and create your password<br>
            Login Link: <a href="http://financialservicecenter.net/login" target="_blank">Click Here</a>
        </td>
    </tr>
</table>


</body>
</html>