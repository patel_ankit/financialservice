@extends('front-section.app')
@section('main-content')
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>Submission</h4>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                <h4>SALE TAX REPORT</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form id='saletaxReporting' name="saletaxReporting" method="post" enctype="multipart/form-data">

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                    <div class="alert alert-danger" style="display:none;margin-top: 10px;font-size: 15px" id="dangerError"></div>

                    <div class="alert alert-success" style="display:none;margin-top: 10px;font-size: 15px" id="successError"></div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Email : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="text" name='email' class="form-control fsc-input" id="email" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <input type="password" class="form-control fsc-input" id="password" name='password' placeholder="Password">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-lg fsc-form-submit">LOGIN</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <button type="reset" class="btn btn-default btn-lg fsc-form-submit" style="">RESET</button>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>

                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

            </form>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#saletaxReporting").validate({
                rules: {
                    password: "required",
                    email: {
                        required: true,
                        email: true
                    },
                },
                messages: {
                    email: "Please enter a valid email address"
                },
                submitHandler: function () {
                    SaletaxLogin()
                }
            });
        });

        function SaletaxLogin() {
            $("#successError").hide();
            $("#dangerError").hide();
            var request = $.ajax({
                url: "http://166.62.89.209/financialservicecenter/index.php/Submission/SaletaxLogin",
                method: "POST",
                data: $("#saletaxReporting").serialize(),
                dataType: "html"
            });
            request.done(function (data, textStatus, jqXHR) {
                var response = jQuery.parseJSON(data);
                if (response.success == 'true') {
                    location.reload();
                } else {
                    $("#dangerError").html(response.message).show();
                }
            });
            request.fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
            });
        }
    </script>
@endsection()