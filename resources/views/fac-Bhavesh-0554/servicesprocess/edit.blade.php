@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .fsc-reg-sub-header-div h3 {
            font-size: 19px;
        }

        .check-box-fsc {
            margin: 10px 20px;
        }

        .fsc-reg-sub-header-div {
            border-radius: 0;
            padding: 6px 4%;
            border-top: 2px solid #fff;
            border-bottom: 2px solid #fff;
            float: left;
            width: 100%;
            text-align: center;
            margin-top: 20px;
        }

        a.disabled {
            pointer-events: none;
            cursor: default;
            opacity: 0.5;
        }

        .form-control-insu {
            border: 2px solid #286db5;
            border-radius: 3px;
            font-weight: bold;
        }

        .share_tabs_main {
            float: left;
            width: 100%;
            margin-bottom: 0px;
        }

        .share_tabs {
            float: left;
            width: 100%;
        }

        .share_tabs_other {
            float: left;
            width: 100%;
        }

        .share_tab {
            float: left;
            margin: 10px 0.8%;
        }

        .share_firstn {
            width: 14%;
        }

        .share_m {
            width: 5%;
        }

        .share_lastn {
            width: 14%;
        }

        .share_position {
            width: 15%;
        }

        .share_persentage {
            width: 14%;
        }

        .share_date {
            width: 15%;
        }

        .share_add {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_remove {
            float: left;
            width: 7%;
            margin: 15px 1% 0 1%;
        }

        .share_total {
            font-size: 12px;
            line-height: 30px;
            text-align: right;
            width: 100%;
        }

        .addbtn {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #359e0b;
            border-radius: 2px;
            vertical-align: top;
        }

        .removebtn, .remove {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #d9534f;
            border-radius: 2px;
            vertical-align: top;
        }

        .share_tab input {
            font-size: 14px;
        }

        .tabs-lists {
            width: 100%;
        }

        .tabs-lists > li {

            width: 50%;
            text-align: center;
        }

        .tabs-lists > li > a {
            position: relative;
            display: block;
            padding: 10px 0;
            font-size: 18px;
        }

        /*select {font-family: 'FontAwesome', 'Second Font name'}*/
        .pricetype {
            background: #fff;
            border-radius: 4px;
        }

        glyphicon-chevron-right:before {
            content: "\e080";
            color: #000;
        }

        .nav-tabs > li > a {
            height: 40px;
        }

        .clients-name-r {
            font-size: 14px;
            margin: 6px 0;
            background-color: #286db5;
            color: #fff;
            padding: 8px;
        }

        .b-i-top {
            max-width: 90px;
            margin-top: -12px;
        }

        .page-title {
            height: 75px;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .form-control {
            height: 35px !important;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .glyphicon-chevron-right {
            top: 5px;
            text-align: center;
            font-size: 2rem;
        }

        .fsc-form-label {
            display: block;
            text-align: right;
        }

        .Red {
            background-color: red !important;
            color: #fff !important
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff !important
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff !important
        }

        .Yellow {
            background-color: Yellow !important;
            color: #555 !important
        }

        .Orange {
            background-color: Orange !important;
            color: #fff !important
        }

        .nav-tabs > li {
            width: 32% !important;
        }

        .arrow {
            width: 0px;
            position: relative;
            float: left;
            right: 12px;
            top: 7px;
        }

        .nav-tabs {
            padding: 3px;
        }

        .form-horizontal .form-group {
            margin-left: 0px;
        }

        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 3px 3px 7px 8px !important;
        }

        .ser:nth-of-type(odd) {
            padding: 15px 0;
            width: 100%;
            background-color: #e8f7fd;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .ser:nth-of-type(even) {
            background-color: #fff5dd;
            padding: 15px 0;
            width: 100%;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .bg-color {
            background: #286db5;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        #tab3primary label {
            text-align: left;
        }

        .model-width {
            width: 84%;
            border: #3668f6 1px solid;
            border-radius: 0;
        }

        .model-width .modal-header {
            padding: 4px 10px;
            background: #fff;
        }

        .model-width .modal-header h4 {
            font-size: 16px;
        }

        .model-width .modal-footer {
            text-align: center;
        }

        .primary {
            background-color: #d0d0d0 !important;
            border-radius: 0px;
            color: #000;
            padding: 5px 10px;
            width: 93px;
            font-weight: 200px;
        }

        .fa-trash {
            font-size: 18px;
        }

        .input_fields_wrap {
            margin-top: 0px;
        }

        .card ul li {
            width: 24% !important;
        }

        .card ul li a {
            display: block;
            width: 100%;
            color: #333;
            text-transform: capitalize;
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);
            border: 1px solid #979800 !important;
        }

        .card ul li a:hover {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li a.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card-footer {
            padding: 7px;
        }

        .active-width {
            width: 100px;
            float: right;
            margin-bottom: 0;
            margin-right: 10px !important;
        }

        .Branch h1 {
            line-height: 35px;
        }

        .education_service .customer-service-information-bg {
            padding: 0 0px;
        }

        .customer-service-information-bg .form-control-1 {
            width: 100%;
            padding: 10px;
        }

        .customer-service-information-bg .form-control {
            font-size: small !important;
            font-size: small !important;
        }

        .new_images_sec {
            margin: 12px 0 0 0;
        }

        .new_images_sec .img-responsive {
            height: 50px;
        }

        .new_images_sec .col-md-4:first-child {
            margin-left: -20px;
        }

        .education_service .form-group .control-label {
            font-size: 13px;
        }

        .star-reqire {
            color: red;
        }

        .service {
            display: none;
        }

        /* This is to remove the arrow of select element in IE */
        .typeofservice select::-ms-expand {
            display: none;
        }

        .typeofservice select {
            -webkit-appearance: none;
            appearance: none;
        }

        @-moz-document url-prefix() {
            .typeofservice {
                border-radius: 4px;
                box-sizing: border-box;
                position: relative;
                overflow: hidden;
            }
            .typeofservice select {
                background-position: right 30px center !important;
                border: none !important;
            }
        }

        .fsc-form-label {
            margin-bottom: 0px !important;
            margin-top: 7px;
        }

        input[type="radio"] {
            margin-top: 3px !important;
            float: left !important;
            margin-right: 6px !important;
        }

        .form-control.Yellow {
            background: #ffff00 !important;
        }

        .form-control.red {
            background: #ff0000 !important;
        }

        .form-control.Green {
            background: #00ef00 !important;
        }

        .form-control.Blue {
            background: #7c7cff !important;
        }

        .form-control.Orange {
            background: #ffa500 !important;
        }

        .form-control.textarea {
            height: auto !important;
            min-height: 100px !important;
        }

        @media (max-width: 991px) {
            .clear_991 {
                clear: both;
            }
        }

        @media (max-width: 830px) {
            .nav-tabs > li {
                width: 31.5% !important;
            }
        }

        @media (max-width: 500px) {
            .nav-tabs > li {
                width: 30.5% !important;
            }
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service Apply</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div style="padding-top:3px;">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form enctype='multipart/form-data' action="{{route('servicesprocess.update',$servicetype->id)}}" id="registrationForm" name="clientname" method="post">
                                <!-- SmartWizard html -->
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div id="smartwizard">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active"><a href="#step-1" data-toggle="tab">Step-1</a></li>
                                        <li><a href="#step-2" data-toggle="tab">Step-2</a></li>
                                        <li><a href="#step-3" data-toggle="tab">Step-3</a></li>

                                    </ul>

                                    <div class="tab-content row">
                                        <div class="tab-pane active" data-step="0" id="step-1">
                                            <div id="form-step-0" role="form" data-toggle="validator">
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-3">
                                                            <label class="fsc-form-label" for="typeofservice">Type of Service : <span class="star-reqire">*</span></label>
                                                        </div>
                                                        <input value="{{$servicetype->serviceid}}" type="hidden" class="form-control fsc-input" id="serviceid" name="serviceid" placeholder="Rreservation">
                                                        <input value="{{$servicetype->unique}}" type="hidden" class="form-control fsc-input" id="unique" name="unique" placeholder="Rreservation">
                                                        <input value="{{$servicetype->typeofcorp}}" type="hidden" class="form-control fsc-input" id="typeofcorp" name="typeofcorp" placeholder="Rreservation">


                                                        <div class="col-lg-6 col-md-9">
                                                            <div class="typeofservice">
                                                                <select type="text" class="form-control fsc-input" name="typeofservice" id="typeofservice" placeholder="Enter Your Company Name">
                                                                    <option value="">---Select---</option>

                                                                    @foreach($service as $ser)
                                                                        @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Accounting Bookkeeping and Taxation Service'))


                                                                            <option value="Bookkeeping Service" @if($servicetype->typeofservice=='Bookkeeping Service') selected @endif>Bookkeeping Service</option>
                                                                            <option value="Payroll Service" @if($servicetype->typeofservice=='Payroll Service') selected @endif>Payroll Service</option>
                                                                            <option value="Taxation Service" @if($servicetype->typeofservice=='Taxation Service') selected @endif>Taxation Service</option>
                                                                            <option value="I Do Not Know" @if($servicetype->typeofservice=='I Do Not Know') selected @endif>I Do Not Know</option>
                                                                            <option value="Please Recommnet" @if($servicetype->typeofservice=='Please Recommend') selected @endif>Please Recommend</option>
                                                                        @else
                                                                        @endif
                                                                        @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Residential Mortgage Services'))
                                                                            <option value="Home Purchase Loan" @if($servicetype->typeofservice=='Home Purchase Loan') selected @endif>Home Purchase Loan</option>
                                                                            <option value="Refinance" @if($servicetype->typeofservice=='Refinance') selected @endif>Refinance</option>
                                                                            <option value="Home Equity Line" @if($servicetype->typeofservice=='Home Equity Line') selected @endif>Home Equity Line</option>

                                                                        @endif
                                                                        @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Commercial Mortgage Services'))

                                                                            <option @if($servicetype->typeofservice=='Business Purchase') selected @endif value="Business Purchase">Business Purchase</option>
                                                                            <option @if($servicetype->typeofservice=='Refinance Business Loan') selected @endif value="Refinance Business Loan">Refinance Business Loan</option>
                                                                            <option @if($servicetype->typeofservice=='SBA Loan') selected @endif value="SBA Loan">SBA Loan</option>
                                                                            <option value="Convential Loan" @if($servicetype->typeofservice=='Convential Loan') selected @endif>Convential Loan</option>
                                                                            <option value="Hard Money Loan" @if($servicetype->typeofservice=='Hard Money Loan') selected @endif>Hard Money Loan</option>
                                                                            <option value="Business Credit Line" @if($servicetype->typeofservice=='Business Credit Line') selected @endif>Business Credit Line</option>
                                                                            <option value="Construction Loan" @if($servicetype->typeofservice=='Construction Loan') selected @endif>Construction Loan</option>
                                                                            <option value="Hotel / Motel Loan" @if($servicetype->typeofservice=='Hotel / Motel Loan') selected @endif>Hotel / Motel Loan</option>
                                                                            <option value="Franchise Business Loan" @if($servicetype->typeofservice=='Franchise Business Loan') selected @endif>Franchise Business Loan</option>
                                                                            <option value="Restaurant Loan" @if($servicetype->typeofservice=='Restaurant Loan') selected @endif>Restaurant Loan</option>
                                                                            <option value="Land Loan" @if($servicetype->typeofservice=='Land Loan') selected @endif>Land Loan</option>
                                                                            <option value="Equipment Lease / Loan" @if($servicetype->typeofservice=='Equipment Lease / Loan') selected @endif>Equipment Lease / Loan</option>
                                                                            <option value="Church Loan" @if($servicetype->typeofservice=='Church Loan') selected @endif>Church Loan</option>
                                                                        @endif
                                                                        @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Financial Services'))
                                                                            <option value="Form a New Corporation / LLC" @if($servicetype->typeofservice=='Form a New Corporation / LLC') selected @endif>Form a New Corporation / LLC</option>
                                                                            <option value="Licenses" @if($servicetype->typeofservice=='Licenses') selected @endif>Licenses</option>
                                                                        @endif
                                                                        @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Insurance Service'))
                                                                            <option value="Health Insurance-Individual" @if($servicetype->typeofservice=='Health Insurance-Individual') selected @endif>Health Insurance-Individual</option>
                                                                            <option value="Health Insurance-Group" @if($servicetype->typeofservice=='Health Insurance-Group') selected @endif>Health Insurance-Group</option>
                                                                            <option value="Visitor Medical Insurance" @if($servicetype->typeofservice=='Visitor Medical Insurance') selected @endif>Visitor Medical Insurance</option>
                                                                            <option value="Life Insurance" @if($servicetype->typeofservice=='Life Insurance') selected @endif>Life Insurance</option>
                                                                            <option value="Annuity" @if($servicetype->typeofservice=='Annuity') selected @endif>Annuity</option>
                                                                            <option value="Other products of Medical & Life Insurance" @if($servicetype->typeofservice=='Other products of Medical & Life Insurance') selected @endif>Other products of Medical & Life Insurance</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if($servicetype->typeofservice)

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  checks" @if($servicetype->foreign_business) @else style="display:none" @endif>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 checks" @if($servicetype->foreign_business) @else style="display:none" @endif>
                                                            <div class="row">
                                                                <?php
                                                                $aColors = array("Bear/Wine Licenses", "EBT Licenses", "Lotto Licenses", "Tobacco Licenses", "COAM(Amusement Machine)", "Other Licences");
                                                                $dbcolors = explode(',', $servicetype->foreign_business);
                                                                ?>
                                                                <div class="chck-box-fscc">
                                                                    @foreach($aColors as $info)
                                                                        @if(in_array($info,$dbcolors))
                                                                            <input id="checkBox_{{$info}}" type="checkbox" value="{{$info}}" class="form-check-input" name="linense_type[]" checked>    <label class="form-check-label check-box-fsc" for="checkBox_{{$info}}">{{$info}}</label>
                                                                        @else
                                                                            <input id="checkBox_{{$info}}" type="checkbox" value="{{$info}}" class="form-check-input" name="linense_type[]"><label class="form-check-label check-box-fsc" for="checkBox_{{$info}}">{{$info}}</label><br>
                                                                        @endif
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group checks1" @if($servicetype->domestic_business) @else style="display:none" @endif>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-row">
                                                                &nbsp;
                                                            </div>


                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin typeofservice">
                                                                <select type="text" class="form-control fsc-input" name="typeofservice1" id="typeofservice1" placeholder="Enter Your Company Name">
                                                                    <option value="">---Select---</option>
                                                                    <option value="Create New Corporation / LLC" @if($servicetype->typeofservice1=='Create New Corporation / LLC') selected @endif> Create New Corporation / LLC</option>
                                                                    <option value="Transfer of Share / Membership" @if($servicetype->typeofservice1=='Transfer of Share / Membership') selected @endif>Transfer of Share / Membership</option>
                                                                    <option value="New License Service" @if($servicetype->typeofservice1=='New License Service') selected @endif>New License Service</option>
                                                                    <option value="Renew Licenser Service" @if($servicetype->typeofservice1=='Renew Licenser Service') selected @endif>Renew Licenser Service</option>
                                                                </select>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label" for="">New Business : <span STYLE="color:red;" class="not-reqire">*</span> </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    @if($servicetype->newbusiness1 == 'New Business' or $servicetype->newbusiness1 == 'Already Existance')
                                                                        <label class="fsc-form-label" for="newbusiness" style="text-align:left;"><input type="radio" class="fsc-input" value="New Business" id="newbusiness" @if($servicetype->newbusiness1=='New Business') checked @endif  name="newbusiness1"> New Business </label>
                                                                    @else
                                                                        <label class="fsc-form-label" for="newbusiness" style="text-align:left;"><input type="radio" class="fsc-input" onclick="show1();" value="Domestic" id="newbusiness" @if($servicetype->newbusiness1=='Domestic') checked @endif  name="newbusiness1"> Domestic </label>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    @if($servicetype->newbusiness1 == 'New Business' or $servicetype->newbusiness1 == 'Already Existance')
                                                                        <label class="fsc-form-label" for="newbusiness1" style="text-align:left;"><input type="radio" class="fsc-input" value="Already Existance" id="newbusiness1" @if($servicetype->newbusiness1=='Already ExistanceNew Business') checked @endif  name="newbusiness1"> Already Existance </label>
                                                                    @else
                                                                        <label class="fsc-form-label" for="newbusiness1" style="text-align:left;"><input type="radio" class="fsc-input" onclick="show2();" value="Foreign Company" id="newbusiness1" @if($servicetype->newbusiness1=='Foreign Company') checked @endif  name="newbusiness1"> Foreign Compnay</label>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="ddd" @if($servicetype->newbusiness1=='Domestic') style="display:block" @endif>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label" for="">Domestic Business : <span style="color:red;" class="not-reqire">*</span> </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin typeofservice" style="margin-bottom:1%;">
                                                            <select type="text" class="form-control fsc-input" onchange="GetSelectedTextValue(this)" name="domestic_business" id="domestic_business" placeholder="Enter Your Company Name">
                                                                <option value="">---Select Domestic---</option>
                                                                <option @if($servicetype->domestic_business=='Domestic Profit Corporation') selected @endif value="Domestic Profit Corporation">Domestic Profit Corporation</option>
                                                                <option value="Domestic Nonprofit Corporation" @if($servicetype->domestic_business=='Domestic Nonprofit Corporation') selected @endif>Domestic Nonprofit Corporation</option>
                                                                <option value="Domestic Limited Liability Corporation" @if($servicetype->domestic_business=='Domestic Limited Liability Corporation') selected @endif>Domestic Limited Liability Corporation</option>
                                                                <option value="Domestic Limited Liability Partenership" @if($servicetype->domestic_business=='Domestic Limited Liability Partenership') selected @endif>Domestic Limited Liability Partenership</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="dddd" @if($servicetype->newbusiness1=='Foreign Company') style="display:block" @endif>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label" for="">Foreign Business : <span class="not-reqire">*</span> </label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="margin-bottom:1%;">
                                                            <select type="text" class="form-control fsc-input" name="foreign_business" id="foreign_business" placeholder="Enter Your Company Name">
                                                                <option value="">---Select Foreign Compnay---</option>
                                                                <option @if($servicetype->foreign_business=='Foreign Profit Corporation') selected @endif value="Foreign Profit Corporation">Foreign Profit Corporation</option>
                                                                <option @if($servicetype->foreign_business=='Foreign Non-Profit Corporation') selected @endif value="Foreign Non-Profit Corporation">Foreign Non-Profit Corporation</option>
                                                                <option @if($servicetype->foreign_business=='Foreign Limited Liability Corporation') selected @endif value="Foreign Limited Liability Corporation">Foreign Limited Liability Corporation</option>
                                                                <option @if($servicetype->foreign_business=='Foreign Limited Liability Partenership') selected @endif value="Foreign Limited Liability Partenership">Foreign Limited Liability Partenership</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                @foreach($service as $ser)
                                                    @if(($ser->id==$servicetype->serviceid) and ($ser->service_name!='Residential Mortgage Services') and ($ser->service_name!='Financial Services'))
                                                    <!--     <div class="form-group" >
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label" for="">New Business : <span class="not-reqire">*</span> </label>
                              </div>
                              <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <div class="row">
                                    <div class="col-lg-6">
                                       <label class="fsc-form-label" for="newbusiness"><input type="radio" class="fsc-input"  onclick="show1();" value="Domestic" id="newbusiness" @if($servicetype->newbusiness=='Domestic') checked @endif  name="newbusiness"> Domestic </label>
                                    </div>
                                    <div class="col-lg-6">
                                       <label class="fsc-form-label" for="newbusiness1"><input type="radio" class="fsc-input"  onclick="show2();" value="Foreign Compnay" id="newbusiness1" @if($servicetype->newbusiness=='Foreign Compnay') checked @endif  name="newbusiness"> Foreign Compnay</label>
                                    </div>
                                 </div>
                                 <div class="help-block with-errors"></div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group" id="ddd" @if($servicetype->newbusiness=='Domestic') style="display:block" @endif>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              </div>
                              <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="margin-bottom:2%;">
                                 <select type="text" class="form-control fsc-input" onchange="GetSelectedTextValue(this)" name="domestic_business" id="domestic_business" placeholder="Enter Your Company Name">
                                    <option value="">---Select Domestic---</option>
                                    <option @if($servicetype->domestic_business=='Domestic Profit Corporation') selected @endif value="Domestic Profit Corporation">Domestic Profit Corporation</option>
                                    <option value="Domestic Nonprofit Corporation"  @if($servicetype->domestic_business=='Domestic Nonprofit Corporation') selected @endif>Domestic Nonprofit Corporation</option>
                                    <option value="Domestic Limited Liability Corporation" @if($servicetype->domestic_business=='Domestic Limited Liability Corporation') selected @endif>Domestic Limited Liability Corporation</option>
                                    <option value="Domestic Limited Liability Partenership" @if($servicetype->domestic_business=='Domestic Limited Liability Partenership') selected @endif>Domestic Limited Liability Partenership</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group" id="dddd" @if($servicetype->newbusiness=='Foreign Compnay') style="display:block" @endif>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              </div>
                              <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="margin-bottom:2%;">
                                 <select type="text" class="form-control fsc-input" name="foreign_business" id="foreign_business" placeholder="Enter Your Company Name">
                                    <option value="">---Select Foreign Compnay---</option>
                                    <option @if($servicetype->foreign_business=='Foreign Profit Corporation') selected @endif value="Foreign Profit Corporation">Foreign Profit Corporation</option>
                                    <option @if($servicetype->foreign_business=='Foreign Non-Profit Corporation') selected @endif value="Foreign Non-Profit Corporation">Foreign Non-Profit Corporation</option>
                                    <option @if($servicetype->foreign_business=='Foreign Limited Liability Corporation') selected @endif value="Foreign Limited Liability Corporation">Foreign Limited Liability Corporation</option>
                                    <option @if($servicetype->foreign_business=='Foreign Limited Liability Partenership') selected @endif value="Foreign Limited Liability Partenership">Foreign Limited Liability Partenership</option>
                                 </select>
                              </div>
                           </div>
                        </div>-->
                                                    @endif
                                                @endforeach



                                                @foreach($service as $ser)
                                                    @if(($ser->id==$servicetype->serviceid) and ($ser->service_name=='Residential Mortgage Services'))
                                                        <div class="row Branch">
                                                            <h1>Basic Information :</h1>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Person Name : <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" value="{{$servicetype->agent_fname}}" id="c_person_name" name="c_person_name" placeholder="Person Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Address : <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="business_address" name="address" value="{{$servicetype->address}}" placeholder="Address">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">City / State / Zip : <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>

                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control  fsc-input textonly" id="business_city" value="{{$servicetype->business_city}}" name="business_city" placeholder="City">
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <select name="business_state" id="business_state" class="form-control fsc-input">
                                                                    <option @if($servicetype->business_state) selected @endif value="{{$servicetype->business_state}}">{{$servicetype->business_state}}</option>
                                                                    <option value="GA">GA</option>
                                                                    <option value="AK">AK</option>
                                                                    <option value="AS">AS</option>
                                                                    <option value="AZ">AZ</option>
                                                                    <option value="AR">AR</option>
                                                                    <option value="CA">CA</option>
                                                                    <option value="CO">CO</option>
                                                                    <option value="CT">CT</option>
                                                                    <option value="DE">DE</option>
                                                                    <option value="DC">DC</option>
                                                                    <option value="FM">FM</option>
                                                                    <option value="FL">FL</option>
                                                                    <option value="GU">GU</option>
                                                                    <option value="HI">HI</option>
                                                                    <option value="ID">ID</option>
                                                                    <option value="IL">IL</option>
                                                                    <option value="IN">IN</option>
                                                                    <option value="IA">IA</option>
                                                                    <option value="KS">KS</option>
                                                                    <option value="KY">KY</option>
                                                                    <option value="LA">LA</option>
                                                                    <option value="ME">ME</option>
                                                                    <option value="MH">MH</option>
                                                                    <option value="MD">MD</option>
                                                                    <option value="MA">MA</option>
                                                                    <option value="MI">MI</option>
                                                                    <option value="MN">MN</option>
                                                                    <option value="MS">MS</option>
                                                                    <option value="MO">MO</option>
                                                                    <option value="MT">MT</option>
                                                                    <option value="NE">NE</option>
                                                                    <option value="NV">NV</option>
                                                                    <option value="NH">NH</option>
                                                                    <option value="NJ">NJ</option>
                                                                    <option value="NM">NM</option>
                                                                    <option value="NY">NY</option>
                                                                    <option value="NC">NC</option>
                                                                    <option value="ND">ND</option>
                                                                    <option value="MP">MP</option>
                                                                    <option value="OH">OH</option>
                                                                    <option value="OK">OK</option>
                                                                    <option value="OR">OR</option>
                                                                    <option value="PW">PW</option>
                                                                    <option value="PA">PA</option>
                                                                    <option value="PR">PR</option>
                                                                    <option value="RI">RI</option>
                                                                    <option value="SC">SC</option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="TN">TN</option>
                                                                    <option value="TX">TX</option>
                                                                    <option value="UT">UT</option>
                                                                    <option value="VT">VT</option>
                                                                    <option value="VI">VI</option>
                                                                    <option value="VA">VA</option>
                                                                    <option value="WA">WA</option>
                                                                    <option value="WV">WV</option>
                                                                    <option value="WI">WI</option>
                                                                    <option value="WY">WY</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input " value="{{$servicetype->business_zip}}" id="business_zip" name="business_zip" placeholder="Zipcode">

                                                            </div>
                                                        </div>


                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Telephone # : <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="phonenumber" value="{{$servicetype->address2}}" name="phonenumber" placeholder="Telephone Number">
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <select name="c_type" id="c_type" class="form-control fsc-input">
                                                                    <option value="Office" @if($servicetype->c_type=='Office') selected @endif >Office</option>
                                                                    <option value="Mobile" @if($servicetype->c_type=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Residential" @if($servicetype->c_type=='Residential') selected @endif>Residential</option>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Email 11: <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" readonly id="c_email" name="basic_email" placeholder="Email" value="{{$servicetype->email}}">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>




                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Telephone : <span class="not-reqire" style="color:red;">*</span></label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="business_telephone" name="business_telephone" value="{{$servicetype->address2}}" placeholder="Business Telephone">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                    @endif
                                                @endforeach

                                                @foreach($service as $ser)
                                                    @if(($ser->id==$servicetype->serviceid) and ($ser->service_name!='Residential Mortgage Services'))
                                                        <div class="row Branch">
                                                            <h1>Company Information :</h1>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Company Legal Name 1: <span style="color:red;" class="not-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="company_name" value="{{$servicetype->company_name}}" name="company_name" placeholder="Company Legal Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Company Legal Name 2: </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="company_name_1" value="{{$servicetype->company_name_1}}" name="company_name_1" placeholder="Company Legal Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Company Legal Name 3: </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="company_name_2" value="{{$servicetype->company_name_2}}" name="company_name_2" placeholder="Company Legal Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Name (DBA) : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" value="{{$servicetype->business_name}}" class="form-control fsc-input" id="business_name" name="business_name" placeholder="Business Name (DBA)" data-bv-field="business_name"><i class="form-control-feedback" data-bv-icon-for="business_name" style="display: none;"></i>
                                                            </div>
                                                        </div>

                                                        <div class="row Branch">
                                                            <h1>Business Information :</h1>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Type of Business : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="type_of_business" name="type_of_business" value="{{$servicetype->type_of_business}}" placeholder="Type of Business">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Address : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" value="{{$servicetype->address}}" id="business_address" name="business_address" placeholder="Business Address">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">City / State / Zip : <span class="star-reqire">*</span></label>
                                                            </div>

                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control  fsc-input textonly" value="{{$servicetype->city}}" id="city" name="city" placeholder="City">
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin typeofservice">
                                                                <select name="state" id="state" class="form-control fsc-input">
                                                                    <option @if($servicetype->state) selected @endif value="{{$servicetype->state}}">{{$servicetype->state}}</option>
                                                                    <option value="GA">GA</option>
                                                                    <option value="AK">AK</option>
                                                                    <option value="AS">AS</option>
                                                                    <option value="AZ">AZ</option>
                                                                    <option value="AR">AR</option>
                                                                    <option value="CA">CA</option>
                                                                    <option value="CO">CO</option>
                                                                    <option value="CT">CT</option>
                                                                    <option value="DE">DE</option>
                                                                    <option value="DC">DC</option>
                                                                    <option value="FM">FM</option>
                                                                    <option value="FL">FL</option>
                                                                    <option value="GU">GU</option>
                                                                    <option value="HI">HI</option>
                                                                    <option value="ID">ID</option>
                                                                    <option value="IL">IL</option>
                                                                    <option value="IN">IN</option>
                                                                    <option value="IA">IA</option>
                                                                    <option value="KS">KS</option>
                                                                    <option value="KY">KY</option>
                                                                    <option value="LA">LA</option>
                                                                    <option value="ME">ME</option>
                                                                    <option value="MH">MH</option>
                                                                    <option value="MD">MD</option>
                                                                    <option value="MA">MA</option>
                                                                    <option value="MI">MI</option>
                                                                    <option value="MN">MN</option>
                                                                    <option value="MS">MS</option>
                                                                    <option value="MO">MO</option>
                                                                    <option value="MT">MT</option>
                                                                    <option value="NE">NE</option>
                                                                    <option value="NV">NV</option>
                                                                    <option value="NH">NH</option>
                                                                    <option value="NJ">NJ</option>
                                                                    <option value="NM">NM</option>
                                                                    <option value="NY">NY</option>
                                                                    <option value="NC">NC</option>
                                                                    <option value="ND">ND</option>
                                                                    <option value="MP">MP</option>
                                                                    <option value="OH">OH</option>
                                                                    <option value="OK">OK</option>
                                                                    <option value="OR">OR</option>
                                                                    <option value="PW">PW</option>
                                                                    <option value="PA">PA</option>
                                                                    <option value="PR">PR</option>
                                                                    <option value="RI">RI</option>
                                                                    <option value="SC">SC</option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="TN">TN</option>
                                                                    <option value="TX">TX</option>
                                                                    <option value="UT">UT</option>
                                                                    <option value="VT">VT</option>
                                                                    <option value="VI">VI</option>
                                                                    <option value="VA">VA</option>
                                                                    <option value="WA">WA</option>
                                                                    <option value="WV">WV</option>
                                                                    <option value="WI">WI</option>
                                                                    <option value="WY">WY</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input " value="{{$servicetype->zip}}" id="business_zip" name="zip" placeholder="Zipcode">
                                                            </div>

                                                        </div>
                                                        <div class="">
                                                            <div class="form-group">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                        <label class="fsc-form-label">County / Code : <span class="star-reqire">*</span></label>
                                                                    </div>
                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin typeofservice">
                                                                        <select name="physical_county" id="physical_county" class="form-control fsc-input">
                                                                            @foreach($taxstate as $v)
                                                                                <option value="{{$v->county}}" @if($v->county==$servicetype->physical_county) selected @endif>{{$v->county}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text" class="form-control fsc-input" id="physical_county_no" value="{{$servicetype->physical_county_no}}" readonly name="physical_county_no" placeholder="Code">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Telephone : <span class="star-reqire">*</span></label>
                                                            </div>

                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input class="form-control fsc-input" placeholder="(999)-999-9999" value="{{$servicetype->business_telephone}}" id="business_telephone" name="business_telephone" maxlength="15" type="text">
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin typeofservice">
                                                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                    <option value="">Type</option>
                                                                    <option value="Home" @if($servicetype->telephoneNo1Type=='Home') selected @endif>Home</option>
                                                                    <option value="Mobile" @if($servicetype->telephoneNo1Type=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Other" @if($servicetype->telephoneNo1Type=='Other') selected @endif>Other</option>
                                                                    <option value="Work" @if($servicetype->telephoneNo1Type=='Work') selected @endif>Work</option>

                                                                </select>
                                                            </div>


                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input class="form-control fsc-input" id="ext1" name="ext1" readonly="" value="{{$servicetype->ext1}}" placeholder="Ext." type="text">
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Website :</label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="website" name="website" value="{{$servicetype->website}}" placeholder="Website">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row Branch">
                                                            <h1>Contact Person Information :</h1>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">

                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Contact Person Name : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin typeofservice" style="width: 108px;">
                                                                <select class="form-control fsc-input" id="nametype" name="nametype">

                                                                    <option value="mr" @if($servicetype->nametype=='mr') selected @endif>Mr.</option>
                                                                    <option value="mrs" @if($servicetype->nametype=='mrs') selected @endif>Mrs.</option>
                                                                    <option value="miss" @if($servicetype->nametype=='miss') selected @endif>Miss</option>
                                                                </select>

                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" value="{{$servicetype->fname}}" id="fname" name="fname" placeholder="First Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" STYLE="width:6.8% !important;">
                                                                <input type="text" class="form-control fsc-input" id="mname" value="{{$servicetype->mname}}" name="mname" placeholder="M">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="lname" value="{{$servicetype->lname}}" name="lname" placeholder="Last Name">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Telephone # : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="phonenumber" value="{{$servicetype->phonenumber}}" name="phonenumber" placeholder="Telephone Number">
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin typeofservice">
                                                                <select name="phonetype" id="phonetype" class="form-control fsc-input">
                                                                    <option value="Mobile" @if($servicetype->agent_mname=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Office" @if($servicetype->agent_mname=='Office') selected @endif >Office</option>

                                                                    <option value="Residential" @if($servicetype->agent_mname=='Residential') selected @endif>Residential</option>

                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Email : <span class="star-reqire">*</span></label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" value="{{$servicetype->email}}" readonly id="email" name="email" placeholder="Email">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach


                                                <div class="row Branch">
                                                    <h1>Shareholder / Officer Information :</h1>
                                                </div>
                                                <div class="">
                                                    <div class="share_tabs_main" style="margin-bottom:10px;">
                                                        <div class="share_tabs">
                                                            <div class="share_tab share_firstn" style="margin: 10px 1% -10px 1%;">
                                                                <label style="font-size: 14px;color:#404040">First Name</label>
                                                            </div>
                                                            <div class="share_tab share_m" style="margin: 10px 1% -10px 1%;">
                                                                <label style="font-size: 14px;color:#404040">M</label>
                                                            </div>
                                                            <div class="share_tab share_lastn" style="margin: 10px 1% -10px 1%;">
                                                                <label style="font-size: 14px;color:#404040">Last Name</label>
                                                            </div>
                                                            <div class="share_tab share_position" style="margin: 10px 1% -10px 0%;">
                                                                <label style="font-size: 14px;color:#404040">Position</label>
                                                            </div>
                                                            <div class="share_tab share_persentage" style="width: 9%; margin: 10px 1% -10px 0%;">
                                                                <label style="font-size: 14px;color:#404040">Percentage</label>
                                                            </div>
                                                            <div class="share_tab share_date" style="margin: 10px 1% -10px 1%;width: 11.5%;">
                                                                <label style="font-size: 14px;color:#404040">Effective Date</label>
                                                            </div>
                                                            <div class="share_tab share_date" style="width: 11.5%;margin: 10px 1% -10px 0%;">
                                                                <label style="font-size: 14px;color:#404040">Status</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $sumplus = 0;?>
                                                @if(empty($user1))

                                                    <div class="col-md-12 clear_991" style="padding:0px;">
                                                        <div class="mainfile clear_991">
                                                            <div class="input_fields_wrap input_fields_wrap_shareholder clear_991">
                                                                <div class="input_fields_wrap_1">
                                                                    <input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                                    <div class="share_tabs_main">
                                                                        <div class="share_tabs ">
                                                                            <!--<div class="share_tab share_position" style="width: 10%;">
                                                                                <select class="form-control" style="font-size: 14px;" id="agentnametype" name="agentnametype[]">
                                                                                    <option value="mr" >Mr.</option>
                                                                                    <option value="mrs" >Mrs.</option>
                                                                                    <option value="miss" >Miss</option>
                                                                                </select>

                                                                            </div>-->

                                                                            <div class="share_tab share_firstn">
                                                                                <input name="agent_fname1[]" type="text" id="agent_fname2" placeholder="First name" class="textonly form-control"/>
                                                                            </div>
                                                                            <div class="share_tab share_m">
                                                                                <input name="agent_mname1[]" type="text" placeholder="M" id="agent_mname2" class="textonly form-control"/>
                                                                            </div>
                                                                            <div class="share_tab share_lastn">
                                                                                <input name="agent_lname1[]" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control"/>
                                                                            </div>
                                                                            <div class="share_tab share_position">
                                                                                <select name="agent_position[]" style="font-size: 14px;" id="agent_position" class="form-control agent_position">
                                                                                    <option value="">Position</option>
                                                                                    <option value="Agent">Agent</option>
                                                                                    <option value="CEO">CEO</option>
                                                                                    <option value="CFO">CFO</option>
                                                                                    <option value="Secretary">Secretary</option>
                                                                                    <option value="Sec">CEO / CFO / Sec.</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="share_tab share_persentage" style="width:9%;">
                                                                                <input name="agent_per[]" type="text" placeholder="" id="agent_per" placeholder="Feb-07-1997" class="txtOnly form-control numeric1  num"/>
                                                                                <div class="cc"></div>
                                                                            </div>
                                                                            <div class="share_tab share_date" style="width: 11%;">
                                                                                <input name="effective_date[]" maxlength="10" type="text" placeholder="Effective Date" id="effective_date" class="txtOnly datepicker form-control effective_date2"/>
                                                                            </div>
                                                                            <div class="share_tab share_date statusselectbox" style="width: 9%;">
                                                                                <select class="form-control greenText" onchange="" name="agentstatus[]" id="agentstatus">
                                                                                    <option value="">Status</option>
                                                                                    <option value="Active" selected="" class="greenText">Active</option>
                                                                                    <option value="In-Active" class="redText">In-Active</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="share_add">
                                                                                <a href="javascript:void(0)" id="add_row1" class="btn btn-danger remove" title="Add field">Remove</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else

                                                    @foreach($user1 as $us)

                                                        <div class="col-md-12 clear_991" style="padding:0px">
                                                            <div class="mainfile clear_991">
                                                                <div class="input_fields_wrap input_fields_wrap_shareholder clear_991">
                                                                    <div class="input_fields_wrap_1">
                                                                        <input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control"/>
                                                                        <div class="share_tabs_main">
                                                                            <div class="share_tabs ">
                                                                            <!--<div class="share_tab share_position" style="width: 10%;">
                    														<select class="form-control" style="font-size: 14px;" id="agentnametype" name="agentnametype[]">
                    														    <option value="mr" @if($us->nametype=='mr') selected @endif>Mr.</option>
                    														    <option value="mrs" @if($us->nametype=='mrs') selected @endif>Mrs.</option>
                    														    <option value="miss" @if($us->nametype=='miss') selected @endif>Miss</option>
                    														</select>
                    														
                    													</div>-->

                                                                                <div class="share_tab share_firstn">
                                                                                    <input name="agent_fname1[]" value="{{$us->agent_fname1}}" type="text" id="agent_fname2" placeholder="First name" class="textonly form-control"/>
                                                                                </div>
                                                                                <div class="share_tab share_m">
                                                                                    <input name="agent_mname1[]" value="{{$us->agent_mname1}}" type="text" placeholder="M" id="agent_mname2" class="textonly form-control"/>
                                                                                </div>
                                                                                <div class="share_tab share_lastn">
                                                                                    <input name="agent_lname1[]" value="{{$us->agent_lname1}}" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control"/>
                                                                                </div>
                                                                                <div class="share_tab share_position">
                                                                                    <select name="agent_position[]" style="font-size: 14px;" id="agent_position" class="form-control agent_position">
                                                                                        <option value="">Position</option>
                                                                                        <option value="Agent" @if($us->agent_position=='Agent') selected @endif>Agent</option>
                                                                                        <option value="CEO" @if($us->agent_position=='CEO') selected @endif>CEO</option>
                                                                                        <option value="CFO" @if($us->agent_position=='CFO') selected @endif>CFO</option>
                                                                                        <option value="Secretary" @if($us->agent_position=='Secretary') selected @endif>Secretary</option>
                                                                                        <option value="Sec" @if($us->agent_position=='Sec') selected @endif>CEO / CFO / Sec.</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="share_tab share_persentage" style="width:9%;">
                                                                                    <input name="agent_per[]" value="{{$us->agent_per}}" type="text" placeholder="" id="agent_per" placeholder="Feb-07-1997" class="txtOnly form-control numeric1  num"/>
                                                                                    <div class="cc"></div>
                                                                                </div>
                                                                                <div class="share_tab share_date" style="width: 11%;">
                                                                                    <input name="effective_date[]" value="{{$us->effective_date}}" maxlength="10" type="text" placeholder="Effective Date" id="effective_date" class="txtOnly form-control datepicker effective_date2"/>
                                                                                </div>
                                                                                <div class="share_tab share_date statusselectbox" style="width: 9%;">
                                                                                    <select class="form-control greenText" onchange="" name="agentstatus[]" id="agentstatus">
                                                                                        <option value="">Status</option>
                                                                                        <option value="Active" selected="" class="greenText">Active</option>
                                                                                        <option value="In-Active" class="redText">In-Active</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="share_add">
                                                                                    <a href="javascript:void(0)" id="add_row1" class="btn btn-danger remove" title="Add field">Remove</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach

                                                @endif

                                                <div class="col-md-12">
                                                    <div class="share_tabs_main">
                                                        <div class="share_tabs_other">
                                                            <div class="share_tab share_firstn">&nbsp;</div>
                                                            <div class="share_tab share_m">&nbsp;</div>
                                                            <div class="share_tab share_lastn">&nbsp;</div>
                                                            <div class="share_tab share_position">
                                                                <label class="share_total">Total :</label>
                                                            </div>
                                                            <div class="share_tab share_persentage">
                                                                <input name="total" style="width:67%;margin-left:1.3%; !important" type="text" placeholder="" id="total" class="txtOnly form-control total" readonly/>
                                                                <p style="display:none;color:red;float: left;" id="t1">This should be not more then 100.00%</p>
                                                            </div>

                                                            <div class="share_tab share_remove">
                                                                <button type="button" id="add_row1" class="btn btn-success addbtn">ADD</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                                    <!--  <ul class="nav nav-tabs" id="myTab">
                                                         <li><a href="#step-2" data-toggle="tab">Next</a></li>
                                                     </ul>-->
                                                    <a class="btn btn-primary btnNext pull-right">Next <i class="fa fa-chevron-right" style="font-size: 13px !important;margin-left:5px;"></i></a>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-2" data-step="2">
                                            <div id="form-step-2" role="form" data-toggle="validator">
                                                <div class="form-section">
                                                    <div class="">
                                                        <div class="">
                                                            <div class="row Branch">
                                                                <h1>Payment Information :</h1>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <!-- Nav tabs -->
                                                                <div class="">
                                                                    <ul class="nav nav-tabs tabs-lists" role="tablist">
                                                                        <li role="presentation" @if($servicetype->directpay=='Credit Card') class="active" @else  @endif><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Credit Card</a></li>
                                                                        <li role="presentation" @if($servicetype->directpay=='AHC') class="active" @else  @endif><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">ACH</a></li>
                                                                        <li role="presentation" @if($servicetype->directpay=='Direct Pay') class="active" @else @endif><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Direct Pay</a></li>

                                                                    </ul>

                                                                    <!-- Tab panes -->
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane @if($servicetype->directpay=='Credit Card') active @else  @endif " id="home">

                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <img src="https://financialservicecenter.net/public/frontcss/images/cards.png" alt="payment-cards" style="width:250px;margin:10px auto" class="img-responsive">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Card No. : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" id="" name="" placeholder="A/c No.">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Expiry Date : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <div class="col-md-3" style="padding-left:0px;">
                                                                                                <input type="date" class="form-control fsc-input" id="" name="" placeholder="">
                                                                                                <div class="help-block with-errors"></div>
                                                                                            </div>
                                                                                            <div class="col-md-3" style="padding-right:0px;display: flex;">
                                                                                                <label class="fsc-form-label" style="padding-right:15px">CVV : <span class="star-reqire">*</span></label>
                                                                                                <input type="text" maxlength="3" class="form-control fsc-input" style="width:100px;" id="" name="" placeholder="CVV">
                                                                                                <div class="help-block with-errors"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Name : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" id="" name="" placeholder="Card Holder Name">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane @if($servicetype->directpay=='AHC') active @else  @endif" id="profile">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Account No. : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" value="{{$servicetype->acno}}" id="acno" name="acno" placeholder="A/c No.">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Confirm Account No. : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" value="{{$servicetype->acno}}" id="cacno" name="cacno" placeholder="Confirm A/C No.">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Routing Number : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" id="routing" value="{{$servicetype->routing}}" name="routing" placeholder="Routing Number">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                                            <label class="fsc-form-label">Bank Name : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            <input type="text" class="form-control fsc-input" id="bankname" name="bankname" value="{{$servicetype->bankname}}" placeholder="Bank Name">
                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div role="tabpanel" class="tab-pane @if($servicetype->directpay=='Direct Pay')active @else  @endif" id="settings">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row text-right">
                                                                                            <label class="form-label">Direct Pay at Office : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                                                            @if($servicetype->directpay=='Direct Pay')
                                                                                                <input type="checkbox" id="directpay" name="directpay" value="Direct Pay" checked style="width:15px;height:20px;margin-top:20px;">
                                                                                                <label for="directpay"> </label>
                                                                                            @else
                                                                                                <input type="checkbox" id="directpay" name="directpay" value="Direct Pay" style="width:15px;height:20px;margin-top:20px;">
                                                                                                <label for="directpay"> </label>
                                                                                            @endif


                                                                                            <div class="help-block with-errors"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row  text-right">
                                                                                            <label class="form-label">Note : <span class="star-reqire">*</span></label>
                                                                                        </div>
                                                                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">


                                                                                            <textarea class="form-control textarea" id="availabilitynote" name="availabilitynote" value="{{$servicetype->availabilitynote}}" placeholder="Enter Note" style="height:auto!important"></textarea>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                                                <div class="title-form-group">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                                                <!--   <ul class="nav nav-tabs" id="myTab">
                            <li><a href="#step-1" data-toggle="tab">Privious</a></li>
                            <li><a href="#step-3" data-toggle="tab">Next</a></li>


                            </ul> -->

                                                <a class="btn btn-primary btnNext pull-right">Next <i class="fa fa-chevron-right" style="font-size: 13px !important;margin-left:5px;"></i></a>
                                                <a class="btn btn-primary btnPrevious pull-left"><i class="fa fa-chevron-left" style="font-size: 13px !important;margin-right:5px;"></i> Previous </a>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-3" data-step="3">
                                            <div id="form-step-3" role="form" data-toggle="validator">
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Username (Email) : <span class="star-required">*</span></label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input type="text" class="form-control fsc-input" value="{{$servicetype->username}}" id="cemail" readonly name="cemail" placeholder="Username/Email">
                                                                    <!--<label class="fsc-form-label"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)">  &nbsp; Same As Email</label>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Password : <span class="star-required">*</span></label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                            <input type="password" class="form-control fsc-input textonly" id="password" value="{{$servicetype->newpassword}}" name="password" placeholder="Password" value="">
                                                            <div id="messages"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">

                                                        </div>
                                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <h5 style="text-align: left;"><input type="checkbox" style="padding-top:4px;" id="terms" @if($servicetype->term==1) checked @endif name="terms" value="1"> <label for="terms">I agree to FSC <a href="#" data-toggle="modal" data-target="#termspopup">Terms Of Use</a> and acknowledge I have read the <a href="#" data-toggle="modal" data-target="#privacypopup">Privacy Policy</a>.</label></h5>
                                                        </div>

                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1%;">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Created Date and Time: </label>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input textonly" value="{{date('M-d Y - g:i A',strtotime($servicetype->created_at))}}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                </br>

                                                <div class="row Branch">
                                                    <h1>For Office Use Only</h1>
                                                </div>
                                                <?php
                                                //	echo "<pre>";print_r($servicetype);
                                                ?>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">

                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="padding-right:10px !important;">
                                                        <label class="fsc-form-label">Submitted Date:</label>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" style="background-color: transparent !important;pointer-events: inherit;"
                                                               value="{{date('M-d Y',strtotime($servicetype->created_at))}}" readonly>


                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('client_id') ? ' has-error' : '' }}" style="margin-top:1%;">

                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Client Id : <span class="star-required">*</span></label>
                                                    </div>
                                                    <!--<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">-->
                                                    <!--   <input type="text" class="form-control fsc-input" style="background-color: transparent !important;pointer-events: inherit;" placeholder="Please Enter Client Id" name="client_id" id="client_id">-->

                                                    <!--<em style="display:none;" class="existmessage"><span style="forn-size:13px;color:red;">ID already Exist!</span></em>-->
                                                    <!--</div>-->


                                                    <div class="col-md-7">
                                                        <input type="text" placeholder="AB" onkeypress="autotab(this, document.clientname.client_id2)" maxlength="2"
                                                               class="form-control fsc-input inputs" style="width: 50px;float: left;text-transform:uppercase;"
                                                               id="client_id1" name="client_id1">

                                                        <input type="text" placeholder="123" onkeypress="autotab1(this, document.clientname.client_id3)" maxlength="3" class="form-control fsc-input"
                                                               id="client_id2" name="client_id2"
                                                               style="width: 50px;float: left;border-left: 2px solid  #2fa6f2;margin-left:10px;">

                                                        <input type="text" placeholder="A" class="form-control fsc-input inputs" maxlength="12" id="client_id3" name="client_id3"
                                                               style="width: 110px;float: left;border-left: 2px solid  #2fa6f2;margin-left:10px;text-transform:uppercase;">

                                                        <em style="display:none;" class="existmessage"><span style="forn-size:13px;color:red;">ID already Exist!</span></em>
                                                    </div>


                                                </div>


                                                <div class="form-group checkedclient_id" style="display:none">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">

                                                        </div>
                                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <h5 style="text-align: left;">
                                                                <input type="checkbox" id="checkedclient_id" name="checkedclient_id" value="1">
                                                                <label for="checkedclient_id">I agree to FSC Convert to Client</label></h5>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('status') ? ' has-error' : '' }}" style="margin-top:1%;">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Status : <span class="star-required">*</span></label>
                                                    </div>

                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <select name="status" onchange="showDiv1(this)" id="status" class="form-control fsc-input">
                                                                    <option value="">Status</option>
                                                                    <option value="Hold" class="Red">Hold</option>
                                                                    <option class="Orange" value="Pending">Pending</option>
                                                                    <option value="Approval" class="Yellow">Approve</option>
                                                                    <option value="Active" class="Green">Active</option>
                                                                    <option value="Inactive" class="Blue">Inactive</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                                                    <!--<ul class="nav nav-tabs" id="myTab">
              <li><a href="#step-2" data-toggle="tab">Privious</a></li>
              </ul> -->

                                                    <a class="btn btn-primary btnPrevious pull-left"><i class="fa fa-chevron-left" style="font-size: 13px !important;margin-right:5px;"></i> Previous</a>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="col-md-7 col-md-offset-4">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <button type="submit" id="nextssss" class="btn_new_save primary1" name="submit">Save</button>
                                                                <button type="submit" id="existmessage" disabled style="display:none;" class="btn_new_save primary1" name="submit">Save</button>

                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="https://financialservicecenter.net/fac-Bhavesh-0554/servicesprocess" class="btn_new_cancel">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
    <script>
        function autotab(current, to) {
            //alert(current.getAttribute("maxlength"));
            if (current.getAttribute && current.value.length == current.getAttribute("maxlength")) {
                to.focus()
            }
        }

        function autotab1(current, to) {
            //alert(current.getAttribute("maxlength"));
            if (current.getAttribute && current.value.length == '2') {
                //alert();
                to.focus()
            }
        }


        // function autotab2(current,to)
        // {

        //     if (current.value.length==current.getAttribute("maxlength"))
        //     {
        //         to.focus()
        //     }
        // }

    </script>


    <script>

        $('document').ready(function () {
            $('#client_id3').on('blur', function () {
                var id1 = $("#client_id1").val();
                var id2 = $("#client_id2").val();
                var id3 = $("#client_id3").val();
                var id4 = '-';

                if (id3 == '') {
                    var fullid = id1.concat(id4).concat(id2);
                } else {
                    var fullid = id1.concat(id4).concat(id2).concat(id4).concat(id3);
                }

                //console.log(fullid);
                $.get('{!!URL::to('getClientfilename')!!}?filename=' + fullid, function (data) {
                    if (data == 1) {
                        $('.existmessage').show();
                        $(".nextssss").hide();
                        $('#existmessage').show();
                        $("#nextssss").hide();
                        $(".checkedclient_id").hide();

                    } else {
                        $('.existmessage').hide();
                        $(".nextssss").show();
                        $('#existmessage').hide();
                        $(".checkedclient_id").show();
                    }
                });

            });
        });
    </script>



    <script>$("#phonenumber").mask("(999) 999-9999");
        $(".ext").mask("99999");
        $("#fax").mask("(999) 999-9999");
        $("#business_telephone").mask("(999) 999-9999");
        $(".usapfax").mask("(999) 999-9999");
        //$("#business_zip").mask("99999");
    </script>
    <script type="text/javascript">
        function OnChangeCheckbox(checkbox) {
            if (checkbox.checked) {
                $('.nex11').addClass('green-border');
            } else {
                $('.nex11').removeClass('green-border');
            }
        }
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#typeofservice").change(function () { //alert();
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                if (selectedValue == 'Form a New Corporation / LLC') {
                    $(".checks1").show();
                    $(".checks").hide();

                } else if (selectedValue == 'Licenses') {
                    $(".checks").show();
                    $(".checks1").hide();

                } else {

                }

            });
        });
    </script>

    <script>
        $(document).ready(function () {
            var dateInput = $('input[name="expire"]'); // Our date input has the name "date"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
                format: 'M/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: truncateDate(new Date()) // <-- THIS WORKS
            });

            $('#expire').datepicker('setStartDate', truncateDate(new Date()));
            $('#effective_date').datepicker('setStartDate', truncateDate(new Date()));
            $('.datepicker').datepicker();

            // <-- SO DOES THIS
        });

        function truncateDate(date) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }


        function show1() {
            document.getElementById('ddd').style.display = 'block';
            document.getElementById('dddd').style.display = 'none';
        }

        function show2() {
            document.getElementById('ddd').style.display = 'none';
            document.getElementById('dddd').style.display = 'block';
        }
    </script>
    <script>
        function FillBilling1(f) {
            if (f.billingtoo.checked == true) {// alert();
                f.address3.value = f.address1.value;
                f.business_city.value = f.city.value;
                f.business_state.value = f.state.value;
                f.business_zip.value = f.zip.value;
                f.county2.value = f.county.value;
                f.address4.value = f.address2.value;
            }
        }
    </script>
    <script type="text/javascript">
        // function checkemail()
        // {
        //  var client_id=document.getElementById("client_id").value;
        // 	//alert(client_id);
        //  if(client_id)
        //  {
        //   $.ajax({
        //   type: 'get',
        //   url: '{!!URL::to('/getclick')!!}',
        //   data: {
        //   client_id:client_id,
        //   },
        //   success: function (response) {
        //   $( '#email_status').html(response);
        //   if(response=="OK")
        //   {
        //     return true;
        //   }
        //   else
        //   {
        //     return false;
        //   }
        //   }
        //   });
        //  }
        //  else
        //  {
        //   $( '#email_status' ).html("");
        //   return false;
        //  }
        // }

        function checkall() {
            var emailhtml = document.getElementById("email_status").innerHTML;

            if ((emailhtml) == "OK") {
                return true;
            } else {
                return false;
            }
        }

    </script>
    <style>
        #ddd {
            display: none
        }

        #dddd {
            display: none
        }
    </style>

    <script>
        $(document).ready(function () {
            var max_fields = 5; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID

            var x = 3; //initlal text box count
            $(add_button).click(function (e) { //on add input button click


                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    //text box increment
                    $(wrapper).append('<div><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Member ' + x + ' :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><input type="text" class="form-control fsc-input" id="scheduler_fname" name="scheduler_fname[]" placeholder="First Name"></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><input type="text" class="form-control fsc-input" id="scheduler_mname" name="scheduler_mname[]" placeholder="Middle Name"></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><input type="text" class="form-control fsc-input" id="scheduler_lname" name="scheduler_lname[]" placeholder="Last Name"></div></div></div><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label"></label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><input type="text" class="form-control fsc-input number" id="scheduler_ssn_' + x + '" onblur="formatValue_' + x + '()" name="scheduler_ssn[]" placeholder="%"></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><select name="scheduler_position[]" id="scheduler_position" class="form-control fsc-input agent_position"><option value="">Position</option><option value="CEO">CEO</option><option value="CFO">CFO</option><option value="Secretary">Secretary</option><option value="Sec">CEO / CFO / Sec.</option></select></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"></div></div></div></div><a href="" class="btn btn-danger remove_field remove  col-md-offset-10">Remove</a></div>'); //add input box
                    x++;
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


    </script>



    <script type="text/javascript">
        $(document).ready(function () {

            $("#common").change(
                function () {

                    // alert("changing");
                    var num = parseFloat($("#common").val());
                    $("#common").val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    //$("#loanAmount").val(num.toFixed(2));
                }
            );

            $("#scheduler_ssn").change(
                function () {
                    // alert("changing");
                    var num2 = parseFloat($("#scheduler_ssn").val());
                    $("#scheduler_ssn").val(num2.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                    //$("#scheduler_ssn").val(num.toFixed(2));
                }
            );

            $("#scheduler_ssn_1").change(
                function () {
                    //  alert("changing");
                    var num1 = parseFloat($("#scheduler_ssn_1").val());//alert(num1);
                    $("#scheduler_ssn_1").val(num1.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
//$("#rateofIntrest").val(num1.toFixed(2)); 
                });

            $("#scheduler_ssn_3").change(
                function () {
                    //alert("changing");
                    var num3 = parseFloat($("#scheduler_ssn_3").val());
                    $("#scheduler_ssn_3").val(num3.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                });
            $("#scheduler_ssn_4").change(
                function () {
                    //  alert("changing");
                    var num4 = parseFloat($("#scheduler_ssn_4").val());
                    $("#scheduler_ssn_4").val(num4.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                });
        });


        function formatValue_3() {
            var x = parseFloat($("#scheduler_ssn_3").val());
            $("#scheduler_ssn_3").val(x.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
        }

        function formatValue_4() {
            var y = parseFloat($("#scheduler_ssn_4").val());
            $("#scheduler_ssn_4").val(y.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
        }

    </script>
    <script>
        $(document).ready(function () {
            $("#zip").keyup(function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('/getzip')!!}?zip=' + id, function (data) {
                    $('#city').empty();
                    $('#state').empty();
                    $('#county').empty();

                    $.each(data, function (index, subcatobj) {
                        $('#city').removeAttr("disabled");
                        $('#state').removeAttr("disabled");

                        $('#county').removeAttr("disabled");
                        $('#city').val(subcatobj.city);
                        $('#county').val(subcatobj.county);
                        $('#state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                        //$('#county').append('<option value="'+subcatobj.country+'">'+subcatobj.county+'</option>');
                    })

                });

            });
        });

        function show1() {
            document.getElementById('ddd').style.display = 'block';
            document.getElementById('dddd').style.display = 'none';
        }

        function show2() {
            document.getElementById('ddd').style.display = 'none';
            document.getElementById('dddd').style.display = 'block';
        }
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#physical_county', function () {
                var selectedCountry = $("#fedral_state option:selected").val(); //alert(selectedCountry);
                var id = $(this).val();// alert(id);
                $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                    $('#physical_county_no').empty();

                    $.each(data, function (index, subcatobj) {
                        $('#physical_county_no').val(subcatobj.countycode);

                    })

                });

            });
        });
    </script>
    <script>
        var $select = $(".agent_position");
        $select.on("change", function () {
            var selected = [];
            $.each($select, function (index, select) {
                if (select.value !== "") {
                    selected.push(select.value);
                }
                // alert(select.value);
            });
            $("option").prop("hidden", false);
            for (var index in selected) {
                $('option[value="' + selected[index] + '"]').prop("hidden", true);

            }
        });

        $(document).ready(function () {
            var maxGroup = 120;
            count = 1;
            $(".btn-success").click(function () {
                count += 1; // alert( count);
                var aa = $('body').find('.input_fields_wrap_shareholder').length;
                if (count > aa) { //alert();
                    if ($('body').find('.input_fields_wrap_shareholder').length < maxGroup) {
                        var fieldHTML = '<div class="input_fields_wrap_shareholder" style="display:block">' + $(".input_fields_wrap_1").html() + '</div>';
                        $('body').find('.input_fields_wrap_shareholder:last').after(fieldHTML);
                        var $select = $(".agent_position");
                        $select.on("change", function () {
                            var selected = [];
                            $.each($select, function (index, select) {
                                if (select.value !== "") {
                                    var dd = selected.push(select.value); //alert(dd);
                                    if (dd == '2' && dd == '3' && dd == '4') {

                                    }
                                }
                            });
                            $("option").prop("hidden", false);
                            for (var index in selected) {
                                $('option[value="' + selected[index] + '"]').prop("hidden", true);
                            }
                        });
                    } else {
                        alert('Maximum ' + maxGroup + ' Persons are allowed.');
                    }
                } else {
                    $('.input_fields_wrap_shareholder').css('display', 'block');

                }
            });

            //remove fields group
            $("body").on("click", ".remove", function () {
                $(this).parents(".input_fields_wrap_shareholder").remove();
                //var dd =  $("#agent_position").val(); alert(dd);
                // alert($('select option:hidden').val();

            });
        });

        $('.btnNext').click(function () {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        });

        $('.btnPrevious').click(function () {
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        });


        var selval = $('#status').val();
        // alert(selval);

        if (selval == 'Hold') {
            $('#status').addClass('red');
            $('#status').removeClass('Orange');
            $('#status').removeClass('Yellow');
            $('#status').removeClass('Green');
            $('#status').removeClass('Blue');

        } else if (selval == 'Pending') {
            $('#status').addClass('Orange');
            $('#status').removeClass('red');
            $('#status').removeClass('Yellow');
            $('#status').removeClass('Green');
            $('#status').removeClass('Blue');


        } else if (selval == 'Approval') {

            $('#status').addClass('Yellow');
            $('#status').removeClass('Orange');
            $('#status').removeClass('red');
            $('#status').removeClass('Green');
            $('#status').removeClass('Blue');


        } else if (selval == 'Active') {
            $('#status').addClass('Green');
            $('#status').removeClass('Orange');
            $('#status').removeClass('red');
            $('#status').removeClass('Yellow');
            $('#status').removeClass('Blue');


        } else if (selval == 'Inactive') {
            $('#status').addClass('Blue');
            $('#status').removeClass('Orange');
            $('#status').removeClass('red');
            $('#status').removeClass('Yellow');
            $('#status').removeClass('Green');
        } else {

        }


        function showDiv1() {
            var selval = $('#status').val();
            // alert(selval);

            if (selval == 'Hold') {
                $('#status').addClass('red');
                $('#status').removeClass('Orange');
                $('#status').removeClass('Yellow');
                $('#status').removeClass('Green');
                $('#status').removeClass('Blue');

            } else if (selval == 'Pending') {
                $('#status').addClass('Orange');
                $('#status').removeClass('red');
                $('#status').removeClass('Yellow');
                $('#status').removeClass('Green');
                $('#status').removeClass('Blue');


            } else if (selval == 'Approval') {

                $('#status').addClass('Yellow');
                $('#status').removeClass('Orange');
                $('#status').removeClass('red');
                $('#status').removeClass('Green');
                $('#status').removeClass('Blue');


            } else if (selval == 'Active') {
                $('#status').addClass('Green');
                $('#status').removeClass('Orange');
                $('#status').removeClass('red');
                $('#status').removeClass('Yellow');
                $('#status').removeClass('Blue');


            } else if (selval == 'Inactive') {
                $('#status').addClass('Blue');
                $('#status').removeClass('Orange');
                $('#status').removeClass('red');
                $('#status').removeClass('Yellow');
                $('#status').removeClass('Green');
            } else {

            }


        }
    </script>



    <script>
        $('#client_id1').inputmask({mask: 'AA'});
        // $("#client_id2").inputmask({mask:''});
        $("#client_id2").mask("999");
    </script>
    <style>
        .form-control, .form-control-insu {
            background-color: #f5f5f5 !important;
        }
    </style>

    <div id="termspopup" class="modal fade " role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <p>Terms & Condition</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="privacypopup" class="modal fade " role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        privacy popup
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection()