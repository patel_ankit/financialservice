@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Zip COde</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form method="post" action="{{route('zipcode.update',$city->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Zip Code :</label>
                                <div class="col-md-4">
                                    <input type="text" name="zipcode" id="zipcode" value="{{$city->zipcode}}" class="form-control" placeholder="zip code"/>
                                    @if ($errors->has('zipcode'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('zipcode') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection()