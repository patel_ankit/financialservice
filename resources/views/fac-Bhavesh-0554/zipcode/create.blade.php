@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>File to Import:</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('zipcode.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('import_file') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Select File to Import :</label>
                                <div class="col-md-4">
                                    <input type="file" name="import_file" id="import_file" class="form-control" placeholder="zip code"/>
                                    @if ($errors->has('import_file'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('import_file') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection()