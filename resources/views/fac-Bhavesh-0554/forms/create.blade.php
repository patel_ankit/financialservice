@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Forms</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">
                                <div class="table-title">

                                    <a href="{{route('formcategory.create')}}">Add New Category</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">


                            <form method="post" class="form-horizontal" id="" action="{{route('forms.store')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('form_department') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Department Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control category1" id="form_department" name="form_department">
                                            <option value=''>Please select Department Name</option>
                                            <option value="Federal">Federal</option>
                                            <option value="State">State</option>
                                            <option value="County">County</option>
                                            <option value="Local (City)">Local (City)</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        @if ($errors->has('form_department'))
                                            <span class="help-block">
											<strong>{{ $errors->first('form_department') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Category :</label>
                                    <div class="col-md-4">
                                        <select name="category" type="text" id="category" class="form-control">
                                            <option value="">Select</option>
                                        </select>
                                        <span id="loader" style="display:none"><i class="fa fa-spinner fa-3x fa-spin"></i></span>
                                        @if ($errors->has('category'))
                                            <span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('form_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Form Name :</label>
                                    <div class="col-md-4">
                                        <input name="form_name" type="text" id="form_name" class="form-control" value=""/>
                                        @if ($errors->has('form_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('form_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('form_no') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Form No :</label>
                                    <div class="col-md-4">
                                        <input name="form_no" type="text" id="form_no" class="form-control" value=""/>
                                        @if ($errors->has('form_no'))
                                            <span class="help-block">
											<strong>{{ $errors->first('form_no') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div id="weblink" class="form-group {{ $errors->has('link') ? ' has-error' : '' }}" style="display:none">
                                    <label class="control-label col-md-3">Website Link :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" id="link" class="form-control" value=""/>
                                        @if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('form_upload') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Upload Form :</label>
                                    <div class="col-md-4">


                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="form_upload" style="opecity:0" placeholder="Upload Service Image" id="form_upload" type="file">
                                        </label>
                                        @if ($errors->has('form_upload'))
                                            <span class="help-block">
											<strong>{{ $errors->first('form_upload') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="submit" id="primary1" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/forms')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <script>
        $(document).ready(function () {
            $(document).on('change', '.category1', function () {
                var id = $(this).val();
                if (id == 'Other') {
                    $('#weblink').hide();
                } else {
                    $('#weblink').show();
                }

                $.get('{!!URL::to('/getform')!!}?id=' + id, function (data) { //alert(data);
                    $('#category').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#category').append('<option value="' + subcatobj.category + '">' + subcatobj.category + '</option>');
                    })

                });

            });
        });
    </script>

@endsection()