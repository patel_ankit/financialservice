@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>List of upload</h1>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-title">
                            <h3></h3>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="sampleTable3">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Documant Name</th>
                                    <th>Upload</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($upload as $bus)
                                    @if($bus->upload_name == Request::segment(3))

                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$bus->upload_name}}</td>
                                            <td><a target="_blank" href="{{url('public/adminupload')}}/{{$bus->upload}}"><i class="fa fa-file-pdf-o"></i> Downlaod </a>
                                                <iframe src="{{url('public/adminupload')}}/{{$bus->upload}}" width="100%" style="height:100%"></iframe>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection()