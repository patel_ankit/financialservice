@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }

        td h3 {
            margin: 0px;
        }

        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Submission - Request Form</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">


                        </div>
                        <div class="col-md-12">

                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>Submission</th>
                                        <th>Name</th>
                                        <th width="10%">Telephone</th>
                                        <th>Email</th>
                                        <th>Request Form</th>
                                        <th>Note</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($common as $com)

                                        <tr>
                                            <td><?php echo $com->submission_name;?></td>
                                            <Td><?php echo $com->name?></Td>
                                            <Td><?php echo $com->telephone?></Td>
                                            <Td><?php echo $com->email?></Td>
                                            <Td><?php echo $com->requestform?></Td>
                                            <Td><?php echo $com->note?></Td>
                                            <td>
                                                <a class="btn-action btn-view-edit" href="{{url('fac-Bhavesh-0554/submissionrequest/'.$com->id.'/edit1')}}"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

@endsection()