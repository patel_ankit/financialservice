@extends('fscemployee.layouts.app')
@section('title', 'Technical Support')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Technical Support</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form method="post" action="{{route('technicalsupport.store')}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div
                            <div class="form-group">
                                <label class="control-label col-md-3">Date / Day / Time:</label>
                                <div class="col-md-2" style="width: 130px;">
                                    <div class="">
                                        <input type="text" name="date" id="date" class="form-control" value="" placeholder="Date">
                                    </div>
                                </div>

                                <div class="col-md-2" style="width: 130px;">
                                    <div class="">
                                        <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="">
                                    </div>
                                </div>

                                <div class="col-md-2" style="width: 115px;">
                                    <div class="">
                                        <input type="text" name="time" id="time" class="form-control" value="" placeholder="Time">
                                    </div>

                                </div>

                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">To :</label>
                                    <div class="col-md-8">
                                        <input name="to" type="text" id="to" value="Mahendra Singh Baghel" readonly class="form-control">

                                        @if ($errors->has('to'))
                                            <span class="help-block">
											<strong>{{ $errors->first('to') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Subject :</label>
                                    <div class="col-md-8">
                                        <input name="subject" type="text" id="subject" class="form-control">

                                        @if ($errors->has('subject'))
                                            <span class="help-block">
											<strong>{{ $errors->first('subject') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Details :</label>
                                    <div class="col-md-8">
                                        <div class="box-body pad">
                                            <textarea id="editor1" name="details" rows="10" cols="80"></textarea>
                                        </div>
                                        @if ($errors->has('details'))
                                            <span class="help-block">
										<strong>{{ $errors->first('details') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Attachment :</label>
                                    <div class="col-md-8">
                                        <div class="box-body pad">
                                            <label class="file-upload btn btn-primary">
                                                Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
                                            </label>
                                        </div>

                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-3">
                                            <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                        </div>
                                    </div>
                                </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('#type').on('change', function () {
                if ($('#type').val() == 'Resposibilty') {
                    $('.emp').show();
                } else {
                    $('.emp').hide();
                }

            });
        });

    </script>
    <style>
        input[type="file"] {
            display: block;
            position: absolute;
        }
    </style>
@endsection()