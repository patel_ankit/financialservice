@extends('fac-Bhavesh-0554.layouts.app')
@section('title', 'Edit Technical Support')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Technical Support</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="card-body col-md-12">

                            <form method="post" action="#" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date / Day / Time:</label>
                                    <div class="col-md-2" style="width: 130px;">
                                        <div class="">
                                            <input type="text" name="date" id="date" class="form-control" value="{{$homecontent->date}}" placeholder="Date">
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="width: 130px;">
                                        <div class="">
                                            <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="{{$homecontent->day}}">
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="width: 115px;">
                                        <div class="">
                                            <input type="text" name="time" id="time" class="form-control" value="{{$homecontent->time}}" placeholder="Time">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">To :</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="to" name="to">
                                            <option value="">Select</option>
                                            @foreach($employee1 as $employ)
                                                @if(!empty($employ->technical_support))
                                                    <option value="{{$employ->id}}" @if($employ->id==$homecontent->to_supporter) selected @endif>{{$employ->technical_support}} ({{$employ->firstName.' '.$employ->middleName.' '.$employ->lastName}})</option>
                                                @endif
                                                @if(!empty($employ->timing_support))
                                                    <option value="{{$employ->id}}" @if($employ->id==$homecontent->to_supporter) selected @endif>{{$employ->timing_support}} ({{$employ->firstName.' '.$employ->middleName.' '.$employ->lastName}})</option>
                                                @endif
                                                @if(!empty($employ->system_support))
                                                    <option value="{{$employ->id}}" @if($employ->id==$homecontent->to_supporter) selected @endif>{{$employ->system_support}} ({{$employ->firstName.' '.$employ->middleName.' '.$employ->lastName}})</option>
                                                @endif
                                                @if(!empty($employ->other_support))
                                                    <option value="{{$employ->id}}" @if($employ->id==$homecontent->to_supporter) selected @endif>{{$employ->other_support}} ({{$employ->firstName.' '.$employ->middleName.' '.$employ->lastName}})</option>
                                                @endif
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Subject :</label>
                                    <div class="col-md-8">
                                        <input name="subject" type="text" id="subject" value="{{$homecontent->subject}}" class="form-control">

                                        @if ($errors->has('subject'))
                                            <span class="help-block">
											<strong>{{ $errors->first('subject') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Details :</label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <textarea id="editor1" name="details" rows="10" cols="80">{{$homecontent->details}}</textarea>
                                        </div>
                                        @if ($errors->has('details'))
                                            <span class="help-block">
										<strong>{{ $errors->first('details') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Technical Answer :</label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <textarea id="editor2" name="answer" rows="10" cols="80">{{$homecontent->answer}}</textarea>
                                        </div>
                                        @if ($errors->has('details'))
                                            <span class="help-block">
										<strong>{{ $errors->first('details') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Attachment :</label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <!--<label class="file-upload btn btn-primary">
                               Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
                           </label>-->
                                            <img src="{{asset('public/attachment','')}}/{{$homecontent->attachment}}" title="{{$homecontent->subject}}" alt="{{$homecontent->subject}}" width="100px">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"> </label>
                                    <div class="col-md-8">
                                        <div class="">

                                            <input type="checkbox" id="click" name="click" value="1" placeholder="Select Document">
                                            <label for="click">Click</label>

                                        </div>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-3">
                                            <!--	<input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">-->
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $('#type').on('change', function () {
                    if ($('#type').val() == 'Resposibilty') {
                        $('.emp').show();
                    } else {
                        $('.emp').hide();
                    }

                });
            });

        </script>
        <style>
            input[type="file"] {
                display: block;
                position: absolute;
            }
        </style>
@endsection()