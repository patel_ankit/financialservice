@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <style>
        td {
            text-align: center
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Link</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('link.update',$link->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}

                                <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type :</label>
                                    <div class="col-md-4">


                                        <select name="type" type="text" id="type" class="form-control category1" value="">
                                            <option value="">Select</option>
                                            <option value="Federal" @if($link->type=='Federal') selected @endif>Federal</option>
                                            <option value="State" @if($link->type=='State') selected @endif>State</option>
                                            <option value="County" @if($link->type=='County') selected @endif>County</option>
                                            <option value="Local City" @if($link->type=='Local City') selected @endif>Local City</option>
                                            <option value="Community Link" @if($link->type=='Community Link') selected @endif>Community Link</option>
                                            <option value="Other Link" @if($link->type=='Other Link') selected @endif>Other Link</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Name :</label>
                                    <div class="col-md-4">
                                        <input name="name" type="text" id="name" class="form-control" value="{{$link->name}}"/> @if ($errors->has('name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('subtype') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Category :</label>
                                    <div class="col-md-4">
                                        <select name="category" type="text" id="category" class="form-control" value="">
                                            <option value="{{$link->category}}">{{$link->category}}</option>

                                        </select>
                                        @if ($errors->has('category'))
                                            <span class="help-block">
											<strong>{{ $errors->first('') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group dis {{ $errors->has('link') ? ' has-error' : '' }}" @if($link->link) @else style="display:none" @endif>
                                    <label class="control-label col-md-3">Link :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" id="link" class="form-control" value="{{$link->link}}"/> @if ($errors->has('post_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="divhide" @if($link->type=='Federal' || $link->type=='State') style="display:none" @endif>
                                    <div class="form-group {{ $errors->has('post_id') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Post Id :</label>
                                        <div class="col-md-4">
                                            <input name="post_id" type="text" readonly id="post_id" class="form-control" value="{{$link->post_id}}"/> @if ($errors->has('post_id'))
                                                <span class="help-block">
											<strong>{{ $errors->first('post_id') }}</strong>
										</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('post_date') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Post Date :</label>
                                        <div class="col-md-4">
                                            <input name="post_date" type="text" id="post_date" class="form-control" value="<?php echo date('M-d-Y');?>" readonly/> @if ($errors->has('post_date'))
                                                <span class="help-block">
											<strong>{{ $errors->first('post_date') }}</strong>
										</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('linkimage') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Image :</label>
                                        <div class="col-md-4">
                                            <!--<input type="file" name="linkimage[]" multiple id="linkimage" class="form-control"  />  -->
                                            <label class="file-upload btn btn-primary">
                                                Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" name="linkimage[]" multiple id="linkimage" type="file">
                                            </label>
                                            @if ($errors->has('linkimage'))
                                                <span class="help-block">
											<strong>{{ $errors->first('linkimage') }}</strong>
										</span>
                                            @endif
                                            <input type="hidden" name="linkimage1" id="linkimage1" class="form-control" value="{{$link->linkimage}}"/>

                                            @if(empty($link->linkimage))
                                                <img src="{{url('public/images/')}}/noimage.svg" style="width:35%"> @else
                                                <?php $images = explode('|', $link->linkimage); ?>
                                                @foreach($images as $image)
                                                    <img src="{{asset('public/link')}}/{{$image}}" alt="" style="width:35%"/>
                                                @endforeach @endif


                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Description :</label>
                                        <div class="col-md-8">
                                            <textarea id="editor1" name="description">{{$link->content}}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
										</span>
                                            @endif
                                        </div>
                                    </div>


                                    <br>
                                    <br><br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Client No. :</label>
                                        <div class="col-md-4">
                                            <select name="clientname" class="form-control client_id">
                                                <option value="">Select</option>
                                                @foreach($client as $client1)
                                                    @if(empty($client1->filename))
                                                    @else
                                                        <option value="{{$client1->filename}}">{{$client1->filename}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div id="email_status"></div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="submit" id="primary1" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/link')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category1', function () {

                    var id = $(this).val(); //alert(id);

                    if (id == 'Federal' || id == 'State') {
                        $('.divhide').hide();
                        $('.dis').hide();
                    } else if (id == 'Other Link') {
                        $('.divhide').hide();
                        $('.dis').show();
                    } else {
                        $('.dis').hide();
                        $('.divhide').show();
                    }
                    $.get('{!!URL::to('/getlink')!!}?id=' + id, function (data) { //alert(data);
                        $('#category').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#category').append('<option value="' + subcatobj.category + '">' + subcatobj.category + '</option>');
                        })

                    });

                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on('change', '.client_id', function () {

                    var id = $(this).val(); //alert(id);
                    $.get('{!!URL::to('/clientno1')!!}?id=' + id, function (data) { //alert(data);
                        $('#email_status').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#email_status').append('<div class="table-responsive"><table class="table"><thead><tr><th>Client No.</th><th>Client Name</th><th>Client Phone No.</th><th>Client Email</th></tr></thead><tbody><tr><td>' + subcatobj.filename + '</td><td>' + subcatobj.first_name + '</td><td>' + subcatobj.business_no + '</td><td>' + subcatobj.email + '</td></tr></tbody></table></div>');
                        })

                    });

                });
            });
        </script>

@endsection()