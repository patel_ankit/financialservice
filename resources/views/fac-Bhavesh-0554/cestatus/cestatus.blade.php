@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .year {
            padding: 0px 0px 0 0px;
            line-height: 19px;
            /* border: 2px solid #286db5; */
            height: 34px;
            /* float: left; */
            border-radius: 0 3px 3px 0;
            /* width: 16%; */
            text-align: right;
            border-left: transparent;
            position: relative;
            right: -109%;
            top: -30px;
        }

        .width {
            width: 19.7%;
        }

        .width1 {
            width: 71%;
        }

        td {
            text-align: center;
        }

        .content-wrapper {

            height: 100%;
        }

        .nav-tabs {
            padding: 5px !important;
        }

        .box-header {
            color: #444;
            display: block;
            padding: 3px;
            position: relative;
        }

        .form-horizontal .control-label {
            padding-top: 11px !important;
            margin-bottom: 0;
            text-align: right;
        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #00a65a !important;
            border-color: #008d4c !important;
        }

        .buttons-excel:hover {
            background: #008d4c !important;
        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }

        .fa {
            font-size: 16px !important;
        }

        .table-responsive table.table {
            display: revert;
        }

        .box-tools {
            position: absolute !important;
            margin-top: -3px !important;
            margin-right: 120px !important;
        }

        @media only screen and (max-width: 500px) {
            .year_cust {
                width: 95px;
            }

            .total_cust {
                width: 95px;
            }

            .title_cust {
                width: 120px;
                padding-right: 0px;
            }
        }

        @media only screen and (max-width: 878px) {
            .table-responsive table.table.table1 {
                display: block;
                overflow-x: auto;
                white-space: nowrap;
            }
        }

        @media only screen and (max-width: 1218px) {
            .table-responsive table.table.table2 {
                display: block;
                overflow-x: auto;
                white-space: nowrap;
            }
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h2>CE Status</h2>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-primary">
                                @if ( session()->has('success') )
                                    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                                @endif
                                <?php $i = 1;?>
                                @foreach($professional as $prof)
                                    <div class="panel-heading">
                                        <ul class="nav nav-tabs" id="myTab">

                                            <li @if($i=='0') class="active" @endif><a href="{{ucwords($prof->profession)}}" id="{{ucwords($prof->id)}}" data-toggle="tab">CE-{{ucwords($prof->profession)}}</a></li>
                                            <?php $i++;?>

                                        </ul>
                                    </div>
                                @endforeach
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="col-md-12">
                                            <div class="Branch">
                                                <h1>CE Rules</h1>
                                            </div>
                                        </div>
                                        <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <br>
                                            <div class="col-lg-7">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-md-4"></label>
                                                            <label class="control-label col-md-3">&nbsp;&nbsp;</label>
                                                            <label class="control-label col-md-3">&nbsp;&nbsp;</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-md-4">Profession :</label>
                                                            <div class="col-md-8">
                                                                <input type="text" name="profession" id="profession" class="form-control profession">
                                                                <input type="hidden" name="professionid" id="professionid" class="form-control professionid">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-md-4">Reporting Period :</label>
                                                            <div class="col-md-2">
                                                                <input type="text" name="reporting_period" id="reporting_period" class="form-control"><span class="year">Yr</span>
                                                            </div>
                                                            <label class="control-label col-md-3">Reporting Yr :</label>
                                                            <div class="col-md-3">
                                                                <div class="">
                                                                    <input type="text" name="reporting_year" id="reporting_year" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-lg-5 col-xs-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-xs-4"></label>
                                                            <label class="control-label col-xs-3" style="text-align: center;">1 year</label>
                                                            <label class="control-label col-xs-3" style="text-align: center;">Total</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-xs-4 title_cust">General Hrs.:</label>
                                                            <div class="col-xs-3">
                                                                <input type="text" name="gen_hour" id="gen_hour" style="text-align: center;" class="form-control txt year_cust">
                                                            </div>
                                                            <div class="col-xs-3 row"><input type="text" name="gen_hour_1" id="gen_hour_1" style="text-align: center;" class="form-control txt1 total_cust">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="control-label col-xs-4 title_cust">Special Hrs.:</label>
                                                            <div class="col-xs-3">
                                                                <input type="text" class="form-control txt year_cust" name="spe_hour" style="text-align: center;" id="spe_hour">
                                                            </div>
                                                            <div class="col-xs-3 row"><input type="text" name="spe_hour_1" style="text-align: center;" id="spe_hour_1" class="form-control txt1 total_cust">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                                    <label class="control-label col-xs-4 title_cust">Total Hrs. Req.:</label>
                                                    <div class="">
                                                        <div class="col-xs-3">
                                                            <input type="text" name="sum" id="sum" style="text-align: center;" class="form-control year_cust" readonly>
                                                        </div>
                                                        <div class="col-xs-3 row"><input type="text" style="text-align: center;" name="sum1" id="sum1" class="form-control total_cust" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 width">Rules :</label>
                                                    <div class="col-md-9 width1" style="padding-right:0px;">
                                                        <input type="text" name="rules" id="rules" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input_fields_wrap_notes">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 width">Note :</label>
                                                        <div class="col-md-9 width1" style="padding-right:0px;">
                                                            <input name="notes[]" type="text" placeholder="Create Note" id="notes" class="textonly form-control">
                                                            <input name="noteid[]" type="hidden" placeholder="" id="noteid" class="textonly form-control">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a class="btn btn-primary" onclick="education_field_note();">Add</a>
                                                        </div>
                                                    </div>
                                                    <div id="input_fields_wrap_notes"></div>
                                                </div>
                                            </div>
                                            <div class="card-footer">

                                                <div class="col-md-2 col-md-offset-2" style="margin-left: 20%;">
                                                    <input class="btn_new_save btn-primary1" style="margin-left:-2% !important;" type="submit" name="submit" value="Save">
                                                </div>
                                                <div class="col-md-2 row">
                                                    <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/cestatus')}}">Cancel</a>
                                                </div>

                                            </div>
                                        </form>
                                    </div>


                                </div>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="Branch">
                                    <h1>Current CE Status</h1>
                                </div>
                            </div>

                            <form enctype='multipart/form-data' class="form-horizontal changepassword" action="" id="changepassword" method="GET">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group ">
                                            <label for="focusedinput" class="col-md-offset-3 col-md-2 control-label col-md-2 control-label">Year :</label>
                                            <div class="col-sm-2">

                                                <select class="form-control" name="year" id="year" type="text">
                                                    <option value="">---Select Year---</option>
                                                    <?php
                                                    $year = 30;
                                                    for($i = 0;$i <= 0;$i++){
                                                    $year = date('Y', strtotime("last day of -$i year"));
                                                    ?>
                                                    <option value="<?php echo $year;?>"><?php echo $year;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table1" id="">
                                    <thead>
                                    <tr>
                                        <th scope="col">Year
                                            <table style="visibility: hidden; margin:0" class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col">&nbsp;</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </th>
                                        <th scope="col">Complete Hours
                                            <table style="margin:0" class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="col">General</th>
                                                    <th scope="col">Special</th>
                                                    <th scope="col">Total</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </th>
                                        <th scope="col">Required Hours
                                            <table style="margin:0" class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="col">General</th>
                                                    <th scope="col">Special</th>
                                                    <th scope="col">Total</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </th>
                                        <th scope="col">Due Date
                                            <table style="visibility: hidden;  margin:0" class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col">&nbsp;</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbody">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>
                            <div class="box-header" style="padding:0px;">

                                <div class="box-tools pull-right" data-toggle="tooltip" title="Status" style="z-index:9999;">
                                    <div class="table-title">

                                        <a href="{{url('fac-Bhavesh-0554/insurance/create')}}">Add New</a>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table2" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Date Attend</th>
                                        <th scope="col">Course Name</th>
                                        <th scope="col">Sponser / School Name</th>
                                        <th scope="col">Field of Study</th>
                                        <th scope="col">General Hrs.</th>
                                        <th scope="col">Ethics Hrs.</th>
                                        <th scope="col">Total CE Hrs</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody><?php $sum = 0;$sum1 = 0;$sum2 = 0;?>
                                    @foreach($insurance as $insu)
                                        <?php $sum += $insu->general_hrs; $sum1 += $insu->ethics_hrs;$sum2 += $insu->total_ce_hrs;?>
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$insu->date_attend}}</td>
                                            <td>{{$insu->course_name}}</td>
                                            <td>{{$insu->school_name}}</td>
                                            <td>{{$insu->field_of_study}}</td>
                                            <td>{{$insu->general_hrs}}</td>
                                            <td>{{$insu->ethics_hrs}}</td>
                                            <td>{{$insu->total_ce_hrs}}</td>
                                            <td><a class="btn-action btn-view-edit" href="{{route('insurance.edit', $insu->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('insurance.destroy',$insu->id) }}" method="post" style="display:none" id="delete-id-{{$insu->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>

                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$insu->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>{{$sum}}</b></td>
                                        <td><b>{{$sum1}}</b></td>
                                        <td><b>{{$sum2}}</b></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <!--</div>-->
        <script>
            var room1 = 0;
            var z = room1;

            function education_field_note() {
                room1++;
                z++;
                var objTo = document.getElementById('input_fields_wrap_notes')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<label class="control-label col-md-3 width">Note ' + z + ' :</label><div class="col-md-9 width1"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="notes[]" value="" type="text" id="notes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                objTo.appendChild(divtest)
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
                z--;
                room1--;
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                //iterate through each textboxes and add keyup
                //handler to trigger sum event
                $(".txt").each(function () {

                    $(this).keyup(function () {
                        calculateSum();
                    });
                });

            });

            function calculateSum() {

                var sum = 0;
                //iterate through each textboxes and add the values
                $(".txt").each(function () {

                    //add only if the value is number
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sum += parseFloat(this.value);
                    }

                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                //	$("#sum").html(sum.toFixed(2));
                document.getElementById('sum').value = sum;
            }</script>

        <script type="text/javascript">
            $(document).ready(function () {

                //iterate through each textboxes and add keyup
                //handler to trigger sum event
                $(".txt1").each(function () {
                    $(this).keyup(function () {
                        calculateSum1();
                    });
                });

            });

            function calculateSum1() {

                var sum1 = 0;
                //iterate through each textboxes and add the values
                $(".txt1").each(function () {

                    //add only if the value is number
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sum1 += parseFloat(this.value);
                    }

                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                //	$("#sum").html(sum.toFixed(2));
                document.getElementById('sum1').value = sum1;
            }
        </script>
        <script>
            $(document).ready(function () {
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).attr("href"); // get current tab
                    var LastTab = $(e.relatedTarget).text(); // get last tab
                    var currId = $(e.target).attr("id");
                    $(".profession").val(currentTab);
                    $(".professionid").val(currId);
                    var id = $(this).val();
                    $.get('{!!URL::to('cestatus1')!!}?id=' + currId,
                        function (data) {
                            $('#reporting_period').empty();
                            $('#reporting_year').empty();
                            $('#gen_hour').empty();
                            $('#gen_hour_1').empty();
                            $('#rules').empty();
                            $('#spe_hour').empty();
                            $('#spe_hour_1').empty();
                            $('#notes').empty();
                            $('#sum1').empty();
                            $('#sum').empty();
                            $.each(data, function (index, subcatobj) {
                                $('#reporting_period').val(subcatobj.reporting_period);
                                $('#reporting_year').val(subcatobj.reporting_year);
                                $('#gen_hour').val(subcatobj.gen_hour);
                                $('#gen_hour_1').val(subcatobj.gen_hour_1);
                                $('#rules').val(subcatobj.rules);
                                $('#spe_hour').val(subcatobj.spe_hour);
                                $('#spe_hour_1').val(subcatobj.spe_hour_1);
                                $('#sum1').val(subcatobj.spe_total_hr);
                                $('#sum').val(subcatobj.gen_total_hr);
                                $('#notes').val(subcatobj.notes);
                            })
                        });
                });


            });
        </script>
        <script type="text/javascript">
            $('#year').on('change', function () {
                $value = $(this).val();
                $.ajax({
                    type: 'get',
                    url: '{!!URL::to('getdata')!!}',
                    data: {'id': $value},
                    success: function (data) {
                        $('.tbody').html(data);
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $.ajaxSetup({headers: {'csrftoken': '{{ csrf_token() }}'}});

            $('#reporting_year').mask("99/99/9999", {placeholder: 'mm/dd/yyyy'});
        </script>
@endsection()