@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Add Insurance </h1>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('insurance.store')}}" class="form-horizontal" id="contact" name="content" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group{{ $errors->has('date_attend') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Date Attend :</label>
                                <div class="col-md-5">
                                    <input name="date_attend" type="text" id="date_attend" class="form-control">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('course_name') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Course Name :</label>
                                <div class="col-md-5">
                                    <input name="course_name" type="text" id="course_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('school_name') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">School Name :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="school_name" type="text" id="school_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('field_of_study') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Field of Study :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="field_of_study" type="text" id="field_of_study" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ethics_hrs') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Ethics Hrs. :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="ethics_hrs" type="text" id="ethics_hrs" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('general_hrs') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">General Hrs. :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="general_hrs" type="text" id="general_hrs" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('total_ce_hrs') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Total CE Hrs :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="total_ce_hrs" type="text" id="total_ce_hrs" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn upload-image" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection()