@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .dataTables_filter {
            margin-top: -47px;
            position: absolute;
        }

        .dt-buttons {
            position: absolute;
            margin-top: -40px;
            margin-left: 86.6%;
        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header" style="height:50px !important;">
            <h1 class="col-sm-12">
                <div class="col-sm-7" style="text-align:right;">@if(empty($_REQUEST['online'])) List of Service @else Online Access @endif Fees</div>
                <div class="col-sm-5" style="text-align:right;">View / Edit</div>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right" style="margin-right: 150px;">
                                <div class="table-title">
                                    <a href="{{route('price.create')}}?online={{$_REQUEST['online']}}">Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Currency</th>
                                        <th>Type of Service</th>
                                        <th>Service Period</th>
                                        <th style="width: 353px;">Note</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($price as $bus)

                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$bus->currency.' '.$bus->sign}}</td>
                                            <td>{{$bus->typeofservice}}</td>
                                            <td>@if($bus->typeofservice =='Payroll Service') {{$bus->servicename}} @else {{$bus->period}} @endif</td>
                                            <td>{{$bus->serviceincludes}}</td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('price.edit', $bus->id)}}?online={{$_REQUEST['online']}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('price.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()