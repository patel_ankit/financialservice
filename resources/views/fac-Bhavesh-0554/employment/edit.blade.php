@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title"><h1>Hiring </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                        </div>
                        <div class="card-body">
                            <form method="post" action="{{route('employment.update',$employment->id)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3 col-xs-12 left_991">Posting Date :</label>
                                        <div class="col-md-2 col-xs-6">

                                            <input name="date" type="text" value="<?php echo date('M-d-Y', strtotime($employment->created_at));?>" id="date1" class="form-control">

                                            @if ($errors->has('date'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Position Name :</label>
                                        <div class="col-md-4">
                                            <input name="position_name" type="text" id="position_name" class="form-control" value="{{$employment->position_name}}"/>
                                            @if ($errors->has('position_name'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('position_name') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} {{ $errors->has('city') ? ' has-error' : '' }} {{ $errors->has('state') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">City/State/Country:</label>
                                        <div class="col-lg-2 col-md-3">
                                            <input name="city" value="{{$employment->city}}" type="text" id="city" class="form-control">
                                            @if ($errors->has('city'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <select name="country" id="countryId" class="form-control fsc-input">
                                                <option value=''>---Select---</option>
                                                <option value='USA' @if($employment->country=='USA') selected @endif>USA</option>
                                                <option value='IND' @if($employment->country=='INDIA') selected @endif>IND</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <select name="state" id="stateId" class="form-control fsc-input">
                                                <option value="{{$employment->state}}">{{$employment->state}}</option>
                                            </select>
                                            @if ($errors->has('state'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('state') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Job Type :</label>
                                        <div class="col-md-2">
                                            <select name="type" id="type" class="form-control fsc-input">
                                                <option value="{!!$employment->type!!}">{!!$employment->type!!}</option>
                                                <option value='Full Time'>Full Time</option>
                                                <option value='Part Time'>Part Time</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Description :</label>
                                        <div class="col-md-8">
                                            <textarea id="editor1" name="description" rows="10" cols="80">{!!$employment->description!!}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                                        <!--<label class="control-label col-md-3">Job Url :</label>-->
                                        <div class="col-md-8">

                                            <input name="link" type="hidden" id="link" class="form-control" value="{!!$employment->link!!}">

                                            @if ($errors->has('link'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('link') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-xs-2" style="width:auto;">
                                        <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save" style="padding:8px 25px;">
                                    </div>
                                    <div class="col-xs-2" style="width:auto;">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/employment')}}" style="padding:8px 25px;">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var dateInput = $('input[name="date"]'); // Our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
                dateInput.datepicker({
                    format: 'M-dd-yyyy',
                    container: container,
                    todayHighlight: true,
                    //autoclose: true,
                    // startDate: truncateDate(new Date()) // <-- THIS WORKS
                });

                $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
            });

            function truncateDate(date) {
                return new Date(date.getFullYear(), date.getMonth(), date.getDate());
            }
        </script>
@endsection()