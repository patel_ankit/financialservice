@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <section class="content-header page-title">
            <?php
            // print_r($employment);
            if ($employment->countryId == 'IND') {
                $cnts = 'IND';
            } else if ($employment->countryId == 'USA') {
                $cnts = 'USA';
            }?>
            <h1><span class="pull-left">{{$employment->firstName}} {{$employment->middleName}} {{$employment->lastName}}</span> Application Receive</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                        </div>
                        <div class="">
                            <?php //echo "<pre>";print_r($employment);?>
                            <form method="post" action="{{route('employeapplication.update',$employment->cid)}}" class="form-horizontal" id="" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px !important;">
                                                    <select class="form-control fsc-input" id="nametype" name="nametype">
                                                        <option value="mr" @if($employment->nametype=='mr') selected @endif>Mr.</option>
                                                        <option value="mrs" @if($employment->nametype=='mrs') selected @endif>Mrs.</option>
                                                        <option value="miss" @if($employment->nametype=='miss') selected @endif>Miss.</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                    <input type="hidden" class="form-control fsc-input" name='employment_id' id="employment_id" placeholder="First Name" value="{{$employment->employment_id}}">
                                                    <input type="text" class="form-control fsc-input textonly" name='firstName' id="firstName" placeholder="First Name" value="{{$employment->firstName}}">
                                                </div>

                                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <input type="text" class="form-control textonly  fsc-input" maxlength="" name='middleName' id="middleName" placeholder="M" value="{{$employment->middleName}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                                    <input type="text" class="form-control textonly  fsc-input" name="lastName" value="{{$employment->lastName}}" id="lastName" placeholder="Last Name">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Address 1 : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" id="address1" value="{{$employment->address1}}" name='address1' placeholder="Address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Address 2 : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" id="address2" name='address2' placeholder="Address" value="{{$employment->address2}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Country : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="{{$cnts}}">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">City / State / Zip : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" id="city" value="{{$employment->city}}" name='city' placeholder="City">
                                                </div>
                                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                                    <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="{{$employment->stateId}}">

                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" id="zip" value="{{$employment->zip}}" name='zip' placeholder="Zip" maxlength='6'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Telephone 1 : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                                    <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo1" value='{{$employment->telephoneNo1}}' name='telephoneNo1' placeholder="(999) 999-9999" data-inputmask="'alias': 'phone'">
                                                </div>
                                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                        <option value='Mobile' @if($employment->telephoneNo1Type=='Mobile') selected @endif>Mobile</option>
                                                        <option value='Home' @if($employment->telephoneNo1Type=='Home') selected @endif>Home</option>
                                                        <option value='Work' @if($employment->telephoneNo1Type=='Work') selected @endif>Work</option>
                                                        <option value='Other' @if($employment->telephoneNo1Type=='Other') selected @endif>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Telephone 2 : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                                    <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" value='{{$employment->telephoneNo2}}' name='telephoneNo2' id="telephoneNo2" placeholder="(999) 999-9999" data-inputmask="'alias': 'phone'">
                                                </div>
                                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                                    <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                        <option value='Mobile' @if($employment->telephoneNo2Type=='Mobile') selected @endif>Mobile</option>
                                                        <option value='Home' @if($employment->telephoneNo2Type=='Home') selected @endif>Home</option>
                                                        <option value='Work' @if($employment->telephoneNo2Type=='Work') selected @endif>Work</option>
                                                        <option value='Other' @if($employment->telephoneNo2Type=='Other') selected @endif>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" value='{{$employment->email}}' readonly id="email" name='email' placeholder="Email Address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Position : <span class="star-required">*</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="dropdown">
                                                        <select id='requiremnetId' name='requiremnetId' class='form-control  fsc-input' style='height:auto'>
                                                            @foreach($employment1 as $emp)
                                                                <option value='{{$emp->id}}' @if($employment->requiremnetId == $emp->id) selected @else @endif>{{$emp->position_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Resume :<span class="star-required">&nbsp;&nbsp;</span> </label>

                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

                                        <!--<label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" id="resume" name='resume' placeholder="Select Document">
            </label>
            			
&nbsp;&nbsp;

@if(empty($employment->resume))
                                            <img src="{{asset('public/images/file-not-found.png')}}" width="60" alt="">
@else
                                            <a href="{{asset('public/resumes/')}}/{{$employment->resume}}" target="_blank" class="btn btn-success">

@if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'doc')
                                                <i class="fa fa-file" aria-hidden="true"></i>
@endif
                                            @if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'pdf')
                                                <i class="fa fa-file-pdf-o"></i>
@endif
                                            @if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'docx')
                                                <i class="fa fa-file-pdf-o"></i>
@endif Download</a>
@endif

                                                <input type="hidden" class="form-control fsc-input" id="resume1" name='resume1' value="{{$employment->resume}}" placeholder="Select Document">
&nbsp;&nbsp;
!-->
                                            <a href="{{ route('send.sendmail',[$employment->firstName,$employment->email]) }}" id="{{$employment->cid}}" class="btn btn-success">Send Question</a>
                                            <a data-toggle="modal" data-target="#myModalResume" class="btn btn-primary">Show Resume</a>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Telephone 2 : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-7 col-md-9">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                                    <input type="text" class="form-control" value="<?php echo date('M-d Y', strtotime($employment->created_at));?>" readonly placeholder="Date">&nbsp;&nbsp;
                                                </div>
                                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                                    <input type="text" class="form-control" value="<?php echo date('D', strtotime($employment->created_at));?>" readonly placeholder="Day">&nbsp;&nbsp;
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                                    <input type="text" class="form-control" value="<?php echo date('g:i A', strtotime($employment->created_at));?>" readonly placeholder="Time">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div style="margin-bottom:20px">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-primary fsc-form-submit">Save</button>
                                            &nbsp&nbsp <a href="https://financialservicecenter.net/fac-Bhavesh-0554/employeapplication" class="btn btn-primary fsc-form-submit">Cancel</a>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-md-12">
                                        <center><p>Accept will convert into prospect <a href="{{ route('co.update1',$employment->cid) }}" id="{{$employment->cid}}" class="btn btn-success userStatus">Accept</a> &nbsp; <a class="btn btn-danger userStatus" href="{{ route('co.deleteto',$employment->cid) }}" id="{{$employment->cid}}">Reject</a> Reject will be deleted</p></center>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="" id="Register"></div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <!-- Modal -->
    <div id="myModalResume" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <iframe src="{{asset('public/resumes/')}}/{{$employment->resume}}" width="100%" height="500"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        //$("#telephoneNo1").mask("(999) 999-9999");
        $(".ext").mask("999");
        //$("#telephoneNo2").mask("(999) 999-9999");
        //$("#mobile_no").mask("(999) 999-9999");
        //$(".usapfax").mask("(999) 999-9999");
        $("#zip").mask("9999");
        $(document).ready(function () {
            /***phone number format***/
            $(".phone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("(" + curval + ")" + " ");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                    $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 9) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });
    </script>

@endsection()