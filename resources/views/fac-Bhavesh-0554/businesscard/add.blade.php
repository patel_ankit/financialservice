@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Add Business Card</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Business Card Name :</label>
                                    <div class="col-md-4">
                                        <input name="cardname" type="text" id="cardname" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-3">Business Card Image :</label>
                                    <div class="col-md-4">
                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="logo" style="opecity:0" placeholder="Upload Service Image" id="logo" type="file" value=""/>
                                        </label>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

@endsection