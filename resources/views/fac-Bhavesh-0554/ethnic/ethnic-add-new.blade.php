@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Ethnic</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('ethnic-add-new.store')}}" class="form-horizontal" id="ethnicname" name="ethnicname">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('ethnic_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Ethnic Name :</label>
                                    <div class="col-md-4">
                                        <input name="ethnic_name" type="text" id="ethnic_name" class="form-control" value=""/> @if ($errors->has('ethnic_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('ethnic_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/ethnic')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()