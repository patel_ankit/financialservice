@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .table > tbody > tr > td:nth-child(4) {
            text-transform: lowercase;
        }

        .modal-title {
            text-align: center;
        }

        #sampleTable2_filter {
            display: none;
        }

        .page-title {
            padding: 8px 15px;
        }

        .dt-buttons {
            margin-bottom: 5px;
        }

        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }

        .Branch {
            width: 82%;
            margin: 0 auto 12px auto;
            float: initial;
        }

        .fa {
            font-size: 16px !important;
        }

        .print_btn1 {
            position: absolute;
            margin-right: 80px;
            bottom: 52px;
            right: 0px;
        }

        .print_btn1 .btn {
            position: absolute;
            right: 0px;
            border: 1px solid #333;
        }

        .modal button.dt-button.buttons-print {
            padding: 0px !important;
            background: transparent !important;
            border: none;
            margin-top: 20px;
        }

        @media (max-width: 1370px) {
            .hide_991 {
                width: 0% !important;
            }

            .telephone1 {
                width: 30%;
            }

            .telephone2 {

            }

            .telephone3 {

            }

            .telephone4 {
                width: 15%;
            }

            .telephone5 {
                width: 18%;
            }
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title">
            <div class="" style="padding-right:0px !important;">
                <div style="text-align:center;">
                    <h2>FSC Employee / User <span>&nbsp;&nbsp;&nbsp;</span>Email - Telephone - Fax - Extension Information</h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">

                        <div class="box-header">
                            <div class="Branch" style="margin-bottom:0px;">
                                <h1>FSC Main / &nbsp;&nbsp; Email - Telephone - Fax Information</h1>
                            </div>
                            <div class="col-md-12" style="padding: 10px 0px;">
                                <div class="box-tools">
                                    <form method="post" action="{{route('email.mainstore')}}">
                                        {{csrf_field()}}

                                        <div class="col-md-1 hide_991" style="width:6%;"></div>
                                        <div class="col-md-3 telephone1">
                                            <label>Email</label>
                                            <input type="email" name="main_email" id="" class="form-control" required value="{{$mainemail->mainemail}}" placeholder="Main Email">
                                        </div>
                                        <div class="col-md-2 telephone2" style="padding-left:0px;">
                                            <label>Telephone</label>
                                            <input type="text" name="main_telephone" id="main_telephone" required class="form-control" value="{{$mainemail->maintelephone}}" placeholder="Main Telephone">
                                        </div>
                                        <div class="col-md-2 telephone3" style="padding-left:0px;">
                                            <label>Fax</label>
                                            <input type="text" name="main_fax" id="main_fax" required class="form-control" value="{{$mainemail->mainfax}}" placeholder="Main Fax">
                                        </div>
                                        <div class="col-md-2 telephone4" style="padding-left:0px;">
                                            <label>Password</label>
                                            <label onclick="myFunctions()" style="position: absolute;margin-top: 35px;right: 25px;"><i class="fa fa-eye toggle-password1" style="margin-left:0px;cursor:pointer;color: #337ab7;"></i></label>
                                            <input type="password" name="password" id="myInput" required class="form-control" <?php if(isset($mainemail->password)) { ?> value="<?php echo $mainemail->password;?>" <?php } ?> placeholder="Password">
                                        </div>
                                        <div class="col-md-2 telephone5" style="padding-left:0px;">
                                            <label>Get Access </label><br>
                                            <a class="btn btn-warning" href="https://www.financialservicecenter.net:2096/" style="margin-right:8px;padding: 6px 15px;margin-top: 3px;" target="_blank">Access</a>
                                            <input type="submit" class="btn btn-primary" style="margin-top: 4px;" name="submit" value="Save">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <hr style="border-top: 2px solid #575757;">
                        <div class="col-md-12">
                            <div class="Branch" style="margin-bottom:0px;">
                                <h1>List of FSC EE / User Email - Telephone Extension Information</h1>
                            </div>
                        </div>
                        <div class="clear clearfix"></div>
                        <div class="col-md-12" style="position: absolute;width: auto;margin-top: 52px;margin-left: 35%;z-index:100;">
                            <div class="">
                                <div class="">
                                    <a data-toggle="modal" data-target="#exampleModal" style="color:#FFF;background:#e67300  !important;cursor:pointer; margin-right:10px;padding: 9px 15px;" target="_blank">Preview</a>
                                    <a href="https://www.financialservicecenter.net:2096/" style="color:#FFF;background:#e67300  !important; margin-right:10px;padding: 9px 15px;" target="_blank">Access</a>
                                    <a href="{{route('email.create')}}" style="color:#FFF;background:#e67300  !important; margin-right:10px;padding: 9px 15px;">Add New Email / Telephone Extension</a>
                                    <!--<a  data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;margin-right:8px;background:linear-gradient(to bottom, #ffb963 0%, #ff9966 100%) !important;" target="_blank">Preview</a>-->
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                            @endif
                            @if ( session()->has('error') )
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                            @endif
                            <div class="table-responsive" style="padding: 10px;">
                                <p class="text-center"> We consider <strong style="font-size: 16px;">E</strong> - Employee &nbsp;&nbsp; | &nbsp;&nbsp;<strong style="font-size: 16px;">U</strong> - User &nbsp;&nbsp; | &nbsp;&nbsp;<strong style="font-size: 16px;">Z</strong> - Others In Below Table.</p>
                                <table class="table table-hover table-bordered" id="sampleTable1">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">No.</th>
                                        <th style="text-align:center;">E / U</th>
                                        <th style="text-align:center">Name</th>
                                        <th style="text-align:center;width:240px !important;">Email</th>
                                        <th style="text-align:center;">Telephone</th>
                                        <th style="text-align:center">Ext.</th>
                                        <th style="text-align:center">Location</th>

                                        <th style="text-align:center;width:136px !important;">Password</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody><?php  $count = count($email);?>
                                    <?php //print_r($email);exit;?>

                                    @foreach($email as $a)
                                        <tr>
                                            <td style="text-align:center;">{{$loop->index+1}}</td>
                                            <td style="text-align:center;">@if($a->type =='employee') E @elseif($a->type =='user') U @elseif($a->for_whom =="Other") Z @endif </td>
                                            <td>@if($a->for_whom !="Other"){{ucwords($a->firstName.' '.$a->middleName.' '.$a->lastName)}} @else {{'Other'}} @endif</td>
                                            <td>{{$a->email}}</td>
                                            <td>{{$a->telephone}}</td>
                                            <td>
                                                <center>{{$a->ext}}</center>
                                            </td>
                                            <td>@if($a->for_whom !="Other") {{$a->location}} @else {{'---'}} @endif</td>

                                            <td style=""><input type="password" style="width:88px !important;" value="{{$a->password}}" disabled id="myInput_{{$a->ids}}">&nbsp;<lable onclick="myFunction{{$a->ids}}()"><i class="fa fa-eye toggle-password1" style="margin-left:10px;cursor:pointer;color: #337ab7;"></i></label>
                                            </td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('email.edit', $a->ids)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('email.destroy',$a->ids) }}" method="post" style="display:none" id="delete-id-{{$a->ids}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')) {event.preventDefault();document.getElementById('delete-id-{{$a->ids}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <script>function myFunction{{$a->ids}}() {
                                                var x = document.getElementById("myInput_{{$a->ids}}");
                                                if (x.type === "password") {
                                                    x.type = "text";
                                                } else {
                                                    x.type = "password";
                                                }
                                            }</script>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <!--</div>-->
    <script>
        $(".toggle-password1").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");

        });
    </script>
    <script>
        function myFunctions() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }</script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#main_telephone").mask("(999) 999-9999");
            $("#main_fax").mask("(999) 999-9999");

            var table = $('#sampleTable1').DataTable({
                dom: 'Bfrtlip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        messageTop: 'List of FSC Email / Telephone Extension',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        title: $('h4').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            $('row c[r^="A"]', sheet).attr('s', '51');
                            $('c[r^="E"]', sheet).attr('s', '50');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5],
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            doc.styles['td:nth-child(2)'] = {
                                width: '300px',
                                'max-width': '300px'
                            };
                            doc.content.splice(0, 1);
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 100, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 11;
                            doc.styles.tableHeader.fillColor = '#ffff99';
                            doc.styles.tableHeader.color = '#000000';
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'center',
                                        image: logo,
                                        height: 50,

                                        margin: [10, 10, -140, 10]
                                    }, {
                                        alignment: 'center',
                                        text: 'List of FSC Email / Telephone Extension',
                                        fontSize: 12,
                                        margin: [-280, 70, 10, 10],
                                    },],
                                    margin: [20, 0, 0, 22], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 1;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 1;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#000';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#000';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 12;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 12;
                            };
                            objLayout['paddingTop'] = function (i) {
                                return 8;
                            };
                            objLayout['paddingBottom'] = function (i) {
                                return 8;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        },

                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        title: $('h6').text(),
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br style="text-align:center;">List of Email / Telephone Extension</center>'
                                );
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": true,
                    "targets": 2,
                    "orderData": [0, 1]
                }],
                "order": [[1, 'asc']]
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });

    </script>
    <script src="https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2"></script>
    <script>
        function generate() {
            var doc = new jsPDF('p', 'pt');
            var res = doc.autoTableHtmlToJson(document.getElementById("sampleTable1"));
            doc.autoTable(res.columns, res.data, {margin: {top: 80}});
            var header = function (data) {
                doc.setFontSize(20);
                doc.setTextColor(10);
                doc.setFontStyle('normal');
                // doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
                doc.text("List of Email / Telephone Extension", data.settings.margin.left, 50);
            };
            var options = {
                beforePageContent: header,
                margin: {
                    top: 80
                },
                startY: doc.autoTableEndPosY() + 20
            };
            doc.autoTable(res.columns, res.data, {margin: {top: 80}}, options);
            doc.save("table.pdf");
        }
    </script>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!--<div class="modal-header">

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" margin-top: -95px;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>-->
                <div class="modal-body">
                    <div class="table-responsive" id="printThis">
                        <div style=" text-align: center; "><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}" alt=""></div>
                        <h5 class="modal-title" id="exampleModalLabel"><p style="text-align:center;margin: 10px 0px;font-size: 16px;padding-top: 7px;border-top: 1px solid;font-weight:bold;">List of EE / User &nbsp; Email - Telephone Extension</p></h5>
                        <table class="table table-hover table-bordered" id="sampleTable2">
                            <thead>
                            <tr>
                                <th style="text-align:center">No.</th>
                                <th style="text-align:center" width="80px;">E / U</th>
                                <th style="text-align:center">Name</th>
                                <th style="text-align:center;width:200px !important;">Email</th>
                                <th style="text-align:center">Ext.</th>
                                <th style="text-align:center">Location</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($email as $a)
                                <tr>
                                    <td style="text-align:center;">{{$loop->index+1}}</td>
                                    <td style="text-align:center;">@if($a->type =='employee') E @elseif($a->type =='user') U @elseif($a->for_whom =="Other") Z @endif </td>
                                    <td>@if($a->for_whom !="Other"){{ucwords($a->firstName.' '.$a->middleName.' '.$a->lastName)}} @else {{'Other'}} @endif</td>
                                    <td>{{$a->email}}</td>
                                    <td>
                                        <center>{{$a->ext}}</center>
                                    </td>
                                    <td>@if($a->for_whom !="Other") {{$a->location}} @else {{'---'}} @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row print_btn1">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button id="btnPrint" type="button" class="btn btn-success" style="visibility:hidden;">Print</button>
                        </div>
                    </div>
                </div>
                <!-- Modal Footer -->
                <!--<div class="modal-footer">-->

                <!--</div>-->
            </div>
        </div>
    </div>
    <script>
        document.getElementById("btnPrint").addEventListener("click", function () {
            var printContents = document.getElementById('printThis').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        });

        // document.getElementById("btnPrint").onclick = function () {
        //     printElement(document.getElementById("printThis"));
        // }

        // function printElement(elem) {
        //     var domClone = elem.cloneNode(true);

        //     var $printSection = document.getElementById("printSection");

        //     if (!$printSection) {
        //         var $printSection = document.createElement("div");
        //         $printSection.id = "printSection";
        //         document.body.appendChild($printSection);
        //     }

        //     $printSection.innerHTML = "";
        //     $printSection.appendChild(domClone);
        //     window.print();
        // }

        $(document).ready(function () {
            var table = $('#sampleTable2').DataTable({
                dom: 'frtlipB',
                buttons: [

                    {
                        extend: 'print',
                        text: '<h4 style="margin:0px;" class="btn btn-success"> Print</h4>',
                        title: $('h6').text(),
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br style="text-align:center;">List of Email / Telephone Extension</center>'
                                );
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },
                        footer: true,
                        autoPrint: true
                    },
                ],

                "order": [[1, 'asc']]
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
        });


    </script>
@endsection()