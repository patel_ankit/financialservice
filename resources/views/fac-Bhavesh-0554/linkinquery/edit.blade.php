@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Link Inquery</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">


                                <div class="form-group">
                                    <label class="control-label col-md-3"> Name :</label>
                                    <div class="col-md-4">
                                        <input name="branch" type="text" id="branch" class="form-control" value="{{$employ->name}}"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Email :</label>
                                    <div class="col-md-4">
                                        <input name="branch" type="text" id="branch" class="form-control" value="{{$employ->email}}">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Telephone No. :</label>
                                    <div class="col-md-4">
                                        <input name="branch" type="text" id="branch" class="form-control" value="{{$employ->telephone}}"/>

                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-3">
                                            <a class="btn btn-primary icon-btn" href="{{url('fac-Bhavesh-0554/linkinquery')}}"><< Back</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()