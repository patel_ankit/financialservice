@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Formation Setup</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('formationsetup.store')}}" class="form-horizontal" id="positionname" name="positionname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type of Entity / Form :</label>
                                    <div class="col-md-4">
                                        <select class="form-control fsc-input" name="typeofservice" id="typeofservice" placeholder="Enter Your Company Name">
                                            <option value=""> ---Select---</option>

                                            <option value="C Corporation">C Corporation</option>
                                            <option value="S Corporation" selected="">S Corporation</option>
                                            <option value="Single Member LLC">Single Member LLC</option>
                                            <option value="Double Member LLC">Double Member LLC</option>

                                        </select>

                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type of Form :</label>
                                    <div class="col-md-4">
                                        <input name="question" type="text" id="question" class="form-control" value=""/>

                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('expiredate') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Expire Date :</label>
                                    <div class="col-md-4">
                                        <input name="expiredate" type="text" id="expiredate" class="form-control" value=""/>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/question')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()