@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <section class="page-title content-header">
            <h1>Add New Bank</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('statetax.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}


                                <div class="form-group {{ $errors->has('statename') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">State Name : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <!--<input name="statename" type="text" id="statename" class="form-control p-l-10" />                                                            -->
                                        <select name="statename" type="text" id="statename" class="form-control p-l-10"/>
                                        <option>Select</option>
                                        @foreach($datastate as $datastate)

                                            <option value="{{$datastate->state}}">{{$datastate->state}}</option>
                                            @endforeach
                                            </select>
                                            @if ($errors->has('statename'))
                                                <span class="help-block">
											<strong>{{ $errors->first('statename') }}</strong>
										</span>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('taxform') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3"> Form : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="taxform" type="text" id="taxform" class="form-control p-l-10"/>
                                        @if ($errors->has('taxform'))
                                            <span class="help-block">
											<strong>{{ $errors->first('taxform') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                            <!--   <div class="form-group {{ $errors->has('routingnumber') ? ' has-error' : '' }}">-->
                                <!--	<label class="control-label col-md-3">Routing Number : <span class="star-required">*</span></label>-->
                                <!--	<div class="col-md-4">-->
                                <!--		<input name="routingnumber" type="text" id="routingnumber" class="form-control p-l-10" />                                                            -->
                            <!--		@if ($errors->has('routingnumber'))-->
                                <!--				<span class="help-block">-->
                            <!--					<strong>{{ $errors->first('routingnumber') }}</strong>-->
                                <!--				</span>-->
                                <!--		@endif-->
                                <!--	</div>-->
                                <!--</div>-->


                            <!--   <div class="form-group {{ $errors->has('contactnumber') ? ' has-error' : '' }}">-->
                                <!--	<label class="control-label col-md-3">Contact Number : <span class="star-required">*</span></label>-->
                                <!--	<div class="col-md-4">-->
                                <!--		<input name="contactnumber" type="text" id="contactnumber" class="form-control p-l-10" />                                                            -->
                            <!--		@if ($errors->has('contactnumber'))-->
                                <!--				<span class="help-block">-->
                            <!--					<strong>{{ $errors->first('contactnumber') }}</strong>-->
                                <!--				</span>-->
                                <!--		@endif-->
                                <!--	</div>-->
                                <!--</div>     -->

                            <!--   <div class="form-group {{ $errors->has('bankwebsite') ? ' has-error' : '' }}">-->
                                <!--	<label class="control-label col-md-3">Website : <span class="star-required">*</span></label>-->
                                <!--	<div class="col-md-4">-->
                                <!--		<input name="bankwebsite" type="text" id="bankwebsite" class="form-control p-l-10" />                                                            -->
                            <!--		@if ($errors->has('bankwebsite'))-->
                                <!--				<span class="help-block">-->
                            <!--					<strong>{{ $errors->first('bankwebsite') }}</strong>-->
                                <!--				</span>-->
                                <!--		@endif-->
                                <!--	</div>-->
                                <!--</div>     -->

                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/statetax')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 16px !important;
            padding: 0;
            color: #000;
        }

        .p-l-10 {
            padding-left: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-color: #000 transparent transparent transparent;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 6px;
            right: 4px;
        }

        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            /* border: 1px solid #aaa; */
            border-radius: 4px;
            border: 2px solid #2fa6f2;
            height: 40px;
            padding: 8px;
        }</style>

    <script>
        $("#contactnumber").mask("(999) 999-9999");


    </script>


    <script>
        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });

    </script>
@endsection()