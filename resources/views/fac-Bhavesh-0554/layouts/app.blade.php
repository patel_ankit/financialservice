<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('fac-Bhavesh-0554.layouts.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    @include('fac-Bhavesh-0554.layouts.header')
    @include('fac-Bhavesh-0554.layouts.leftsidebar')
    @section('main-content')
        @include('fac-Bhavesh-0554.layouts.Headermenu')
    @show

</div>
@include('fac-Bhavesh-0554.layouts.footer')
</body>
</html>