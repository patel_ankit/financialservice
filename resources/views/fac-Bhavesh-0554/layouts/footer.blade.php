<footer style="bottom:0px;">
    <div class="container-fluid show1">
        <div class="col-md-3 col-sm-1 col-xs-12">

        </div>
        <div class="col-md-6 col-sm-7 col-xs-12 foot_copy_resp">
            <p><strong>Copyright © 2019 - 2021 &nbsp;<br class="show_resp"> <a href="#"><span class="first-letter">F</span>inancial
                    <!--<p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <a href="#"><span class="first-letter">F</span>inancial -->
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a> &nbsp; All rights reserved. </p></div>
        <div class="col-md-3 col-sm-4 col-xs-12 foot_logo_resp">

            <a href="http://vimbo.com/" target="_blank"><img src="https://financialservicecenter.net/public/frontcss/images/f-f-logo.png" alt="logo" class="img-responsive pull-right footer-logo-admin" style="margin-top:12px;"> </a>
        </div>
    </div>
    <div class="container-fluid hide1">
        <div class=" col-xs-12">
            <p><strong>Copyright © 2019 - 2021 &nbsp; <br><br><a href="#"><span class="first-letter">F</span>inancial
                    <!--<p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <br><br><a href="#"><span class="first-letter">F</span>inancial -->
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a><br><br> All rights reserved. </p></div>
        <div style="background: #fff;width: 100%;
    display: inline-block;">
            <center><img src="{{url('public/dashboard/images/footer-logo.png')}}" alt="" class="img-responsive"></center>
        </div>
    </div>
</footer>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-success').fadeIn('slow', function () {
            $('.alert-success').delay(5000).fadeOut();
        });
    });
</script>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-danger').fadeIn('slow', function () {
            $('.alert-danger').delay(5000).fadeOut();
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var groupColumn = 1;
        var response;
        jsonObj = [];


        var url = '{!!URL::to('fac-Bhavesh-0554/mywork/getJsonOfWork/0')!!}';
        var url2 = '{!!URL::to('fac-Bhavesh-0554/mywork/getJsonOfWork/UP')!!}';
        var url3 = '{!!URL::to('fac-Bhavesh-0554/mywork/getJsonOfWork/W')!!}';
        //alert(url);
        $.ajax({
            url: url,
            method: "get",
            success: function (datas) {
                response = datas//$.parseJSON(datas);
                //console.log(response.data[0]);
            },
            async: false
        });
        //debugger;
        //console.log(response);
        var table = $('#sampleTable3').DataTable({

            dom: 'Bfrtlip',
            rowReorder: true,
            ajax: {
                url: url,
                method: "get"
            },
            "columns": [
                {data: 'No'},
                {data: 'Priority'},
                {data: 'Priority_ID'},
                {data: 'Date'},
                {data: 'Time'},
                {data: 'Client ID'},
                {data: 'Client Name'},
                {data: 'Type of Work'},
                {data: 'EstTime'},
                {data: 'Status'},
                {data: 'Action'}
            ],

            buttons: [

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ],
            "order": [[2, 'asc']],
            "columnDefs": [
                {"visible": true, "targets": groupColumn},
                {"visible": true, "targets": 2},
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        if (group == "Time Sensitive") {
                            $(rows).eq(i).before('<tr style="background-color:red; border: 1px solid black;"><td colspan="3" style="text-align: left; color:white;"><strong>' + group + '<strong></td></tr>');
                        } else if (group == "Urgent") {
                            $(rows).eq(i).before('<tr style=" border: 1px solid black;" class="group"><td colspan="3" style="text-align: center;"><strong>' + group + '<strong></td></tr>');
                        } else if (group == "Regular") {
                            $(rows).eq(i).before('<tr style=" border: 1px solid black;" class="group"><td colspan="3" style="text-align: center;"><strong>' + group + '<strong></td></tr>');
                        }
                        last = group;
                    }
                });
            }

        });

        $('#sampleTable3 tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
                table.order([groupColumn, 'desc']).draw();
            } else {
                table.order([groupColumn, 'asc']).draw();
            }
        });

        $('#sampleTable3 tbody').on('click', 'tr', function () {
            //console.log( table.row( this ).data().No);
        });

        // $('#sampleTable3 tr.group').each(function(){
        //     $(this).nextUntil( "tr.group", "tr" ).last().css( "border-bottom", "1px solid black !important;" );
        // });


        table.on('row-reorder', function (e, diff, edit) {

            // var first = diff[0].newPosition;

            var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '<br>';
            console.log(result);
            var row_drag_count = 0;
            var row_drag_one = 0;
            for (var i = 0, ien = diff.length; i < ien; i++) {
                if (i == 0) {
                    row_drag_one = 0;
                }
                var rowData = table.row(diff[i].node).data();
                var lastData = diff[i].oldData;
                var newdata = table.row(diff[i].newData).data();
                var oldData = table.row(diff[i].oldData).data();

                //console.log("New Data : "+newdata);
                row_drag_count = i;
                result += rowData[1] + ' updated to be in position ' +
                    diff[i].newData + ' (was ' + diff[i].oldData + ')<br>';
            }

            //console.log(diff);
            var old = 0;

            var newpos0 = diff[0].oldPosition;
            var newpos1 = diff[1].oldPosition;

            var startPosition = 0;
            var endPosition = 0;

            if (diff.length != 2) {
                if (newpos0 > newpos1) {
                    console.log("You Drag Up");
                    console.log("you move ID : " + table.row(diff[row_drag_count].newPosition).data().No);
                    console.log("To : " + table.row(diff[0].newPosition).data().No);

                    startPosition = table.row(diff[row_drag_count].newPosition).data().No;
                    endPosition = table.row(diff[0].newPosition).data().No;

                } else if (newpos0 < newpos1) {
                    console.log("You Drag Down");
                    console.log("you move ID : " + table.row(diff[0].newPosition).data().No);
                    console.log("To : " + table.row(diff[row_drag_count].newPosition).data().No);

                    startPosition = table.row(diff[0].newPosition).data().No;
                    endPosition = table.row(diff[row_drag_count].newPosition).data().No;

                }
            } else {

            }
            // console.log(old);
            // console.log(table.row(old).data().No);
            // console.log(table.row(diff[1].oldPosition).data().No);


            var url = '{!!URL::to('fac-Bhavesh-0554/mywork/changeOrder')!!}/' + startPosition + '/' + endPosition;
            console.log(url);
            $.ajax({
                url: url,
                method: "get",
                success: function (data) {
                    console.log(data);
                    table.ajax.reload();
                    //$("#sampleTable3").ajax.reload();
                },
                async: false
            });

        });


        $('#sampleTable345').DataTable({
            dom: 'Bfrtlip',
            buttons: [

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ],
        });

        $('#sampleTableForUnderProgress').DataTable({

            dom: 'Bfrtlip',
            rowReorder: true,
            ajax: {
                url: url2,
                method: "get"
            },
            "columns": [
                {data: 'No'},
                {data: 'Priority'},
                {data: 'Priority_ID'},
                {data: 'Date'},
                {data: 'Time'},
                {data: 'Client ID'},
                {data: 'Client Name'},
                {data: 'Type of Work'},
                {data: 'EstTime'},
                {data: 'Status'},
                {data: 'Action'}
            ],

            buttons: [

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ],
            "order": [[2, 'asc']],
            "columnDefs": [
                {"visible": true, "targets": groupColumn},
                {"visible": true, "targets": 2},
            ]

        });

        $('#sampleTableForWating').DataTable({

            dom: 'Bfrtlip',
            rowReorder: true,
            ajax: {
                url: url3,
                method: "get"
            },
            "columns": [
                {data: 'No'},
                {data: 'Priority'},
                {data: 'Priority_ID'},
                {data: 'Date'},
                {data: 'Time'},
                {data: 'Client ID'},
                {data: 'Client Name'},
                {data: 'Type of Work'},
                {data: 'EstTime'},
                {data: 'Status'},
                {data: 'Action'}
            ],

            buttons: [

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ],
            "order": [[2, 'asc']],
            "columnDefs": [
                {"visible": true, "targets": groupColumn},
                {"visible": true, "targets": 2},
            ]

        });
        var lengths = $(".myworkstatus").length;
        console.log("myworkclass : " + lengths);
        $(".myworkstatus").each(function (index) {
            $(this).on("click", function () {
                // For the boolean value
                var values = $(this).find(':selected').val();
                // For the mammal value
                var ids = $(this).attr('id');
                console.log(ids + " : " + values);
                //var url = '{!!URL::to('fac-Bhavesh-0554/mywork/changeOrder')!!}/'+mammalKey+'/'+boolKey+';
                console.log("URl  : " + url);
                updateStatus(ids, values);
                // $.get(url, function(data){ 
                //     console.log(data);
                // });
            });
        });

    });

    $(document).ready(function () {
        $('#sampleTable7').DataTable({

            dom: 'Bfrtlip',
            buttons: [

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp; ',
                    //   titleAttr: 'PDF',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp; Print',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ]
        });
    });
</script>
<!--<script>
   $(document).ready(function(){
   $("#zip").keyup(function() {
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('{!!URL::to('/getzip')!!}?zip='+id, function(data)
   		{ 
  $('#city').empty();
 $('#stateId').empty();$('#countryId').empty();
              $.each(data, function(index, subcatobj)
   		  {$('#city').removeAttr("disabled"); $('#stateId').removeAttr("disabled"); 
$('#city').val(subcatobj.city);
$('#stateId').append('<option value="'+subcatobj.state+'">'+subcatobj.state+'</option>');
 $('#countryId').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
   		   })
   
   		});
   			
   	});
   });
</script>-->
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    $(document).ready(function () {
        $(document).on('change', '#countryId', function () {
            //console.log('htm');
            var id = $(this).val();
            $.get('{!!URL::to('/getstateList')!!}?id=' + id, function (data) {
                $('#stateId').empty();
                $.each(data, function (index, subcatobj) {
                    $('#stateId').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                })

            });

        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".textonly").keypress(function (event) {
            var inputValue = event.charCode;
            // alert(inputValue);
            if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
                event.preventDefault();
            }
        });
    });
    $(document).ready(function () {
        //called when key is pressed in textbox
        $(".zip").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });

    $(document).ready(function () {
        //called when key is pressed in textbox
        $(".zip1").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });

    $('.only_num').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

</script>