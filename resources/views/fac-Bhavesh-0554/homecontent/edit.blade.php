@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Home Content</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">

                            <form method="post" action="{{route('homecontent.update',$homecontent->id)}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Title :</label>
                                    <div class="col-md-8">
                                        <input name="title" type="text" style="margin-left:1.4%" value="{{$homecontent->title}}" id="title" class="form-control"/>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Content :</label>
                                    <div class="col-md-8">
                                        <div class="box-body pad">
                                            <textarea id="editor1" name="content" rows="10" cols="80">{!!$homecontent->content!!}</textarea>
                                        </div>
                                        @if ($errors->has('content'))
                                            <span class="help-block">
										<strong>{{ $errors->first('content') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1 primary1" type="button" style="margin-left:1.5%" id="primary1" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/homecontent')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

@endsection()