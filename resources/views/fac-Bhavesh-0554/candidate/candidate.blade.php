@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .page-title {
            display: -ms-flexbox;
            -ms-flex-align: center;
            align-items: center;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -ms-flex-direction: row;
            flex-direction: row;
            padding: 8px 18px !important;
            box-shadow: 0 1px 2px rgba(0, 0, 0, .1);
            background-color: #D6EBFA !important;
            text-align: center;
        }

        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }

        @media only screen and (max-width: 490px) {
            div.dataTables_wrapper div.dataTables_filter {
                width: 100%;
                display: flex;
            }

            .dt-buttons {
                margin-top: 10px !important;
            }
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="page-title content-header">
            <div class="">
                <div class="">
                    <h2>List of Candidate Data
                        <span class="right_title" style="text-align:right;padding-right: 20px;position: absolute;right: 0;">View / Edit</span></h2>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">

                        <div class="col-md-12">

                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                            @endif
                            @if ( session()->has('error') )
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Date Rec'd</th>
                                        <th>Position</th>
                                        <th>Candidate Name</th>
                                        <th>Email</th>


                                        <th>Resume</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $cnt = 0;?>
                                    @foreach($application as $app)
                                        <?php $cnt++;?>
                                        @if($app->status==1)
                                            <tr>
                                                <td>{{$cnt}}</td>
                                                <td style="text-align:center;">{{date('M-d Y',strtotime($app->candidate_date))}}</td>
                                                <td>{{$app->position_name}}</td>
                                                <td>{{$app->firstName}} {{$app->middleName}} {{$app->lastName}}</td>

                                                <td>{{$app->email}}</td>


                                                <td style="text-align:center;">
                                                    @if(empty($app->resume))
                                                        <img src="{{asset('public/images/file-not-found.png')}}" width="60" alt="">
                                                    @else<a href="{{asset('public/resumes/')}}/{{$app->resume}}" target="_blank">
                                                        @if(pathinfo($app->resume, PATHINFO_EXTENSION) == 'doc')
                                                            <i class="fa fa-file" aria-hidden="true"></i>
                                                        @endif
                                                        @if(pathinfo($app->resume, PATHINFO_EXTENSION) == 'pdf')
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        @endif
                                                        @if(pathinfo($app->resume, PATHINFO_EXTENSION) == 'docx')
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        @endif Download</a>@endif</td>
                                                <td style="text-align:center;">
                                                    @if($app->newapp==2)<a class="" href="{{route('candidate.edit',$app->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif
                                                    <a class="btn-action btn-view-edit" href="{{route('candidate.edit', $app->cid)}}"><i class="fa fa-edit"></i></a>
                                                    <form action="{{ route('candidate.destroy',$app->cid) }}" method="post" style="display:none" id="delete-id-{{$app->cid}}">
                                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                    </form>
                                                    <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                            {event.preventDefault();document.getElementById('delete-id-{{$app->cid}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();
                table.columns(6)
                    .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'Inactive') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'New') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'Active') {  //alert();
                        table.columns(6).search(_val).draw();
                        table.columns(6)
                            .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                            .draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>
@endsection()