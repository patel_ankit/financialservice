@extends('fac-Bhavesh-0554.layouts.app')
<style>
    .main-header {
        display: none !important;
    }

    .main-header .navbar .sidebar-toggle {
        color: #fff;
        display: none;
    }

    footer {
        display: none !important;
    }

</style>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
    <title>FSC - Forgot Passowrd</title>
    <style>
        .help-block {
            color: red
        }

        .semibold-text a {
            color: red;
        }

        .login-head {
            font-size: 24px;
            color: #103b68;
            margin-top: 50px;
            margin-bottom: 10px;
            padding-bottom: 10px;
            border-bottom: 1px solid #bfbfbf;
            text-align: center;
        }

        .forgot {
            width: 400px;
            margin: 220px auto;
            background: #fff;
            padding: 1px 20px 20px 20px !important;
        }

        .employee-logo {
            text-align: center;
            position: absolute;
            margin-top: -155px;
            margin-left: 63px;
        }
    </style>
</head>

<body>
<div class="employee-logo-bg" style="display:none;">

    <section class="employee-login-content">

        <div class="employee-logo">
            <img src="{{asset('public/dashboard/images/logo_employee.png')}}" alt=""/>
        </div>

        <div class="employee-login-box">

            <form class="login-form" method="post" action="{{ route('adminpassword.store') }}" id="admin-login">
                {{ csrf_field() }}
                <h3 class="login-head">Admin Forgot Password</h3>

                <div class="form-group">
                    <label class="employee-control-label">Email</label>
                    <input class="employee-form-control" type="text" name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
                    @endif
                </div>


                <div class="form-group btn-container">
                    <button type="submit" name="submit" class="btn_submit">Send Link</button>
                </div>


                <center>@if ( session()->has('success') )
                        <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                    @endif
                    @if ( session()->has('error') )
                        <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                    @endif
                    <div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div>
                </center>

            </form>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>

    </section>

</div>
</body>

<script src="{{asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/plugins/pace.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/main.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>


</html>
@section('main-content')




    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 employee-logo-bg" style="height:100% !important;">

        <div class="forgot">


            <form class="form-horizontal" method="POST" action="{{ route('forgotusername.store') }}">
                <div class="employee-logo">
                    <img src="{{asset('public/dashboard/images/logo_employee.png')}}" alt=""/>
                </div>
                <h3 class="login-head" style="text-align: center;">Admin Forgot Username</h4>
                    {{ csrf_field() }}

                    <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class=" control-label">E-Mail Address</label>

                        <input id="email" type="email" class="employee-form-control" name="email" value="{{ old('email') }}" required>


                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn_submit">
                                Send Link
                            </button>
                        </div>
                    </div>


            </form>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissable">{{session()->get('error') }}</div>
            @endif
        </div>
    </div>


@endsection