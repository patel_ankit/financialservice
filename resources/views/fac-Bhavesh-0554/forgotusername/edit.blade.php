@extends('fac-Bhavesh-0554.layouts.app')
<style>
    .main-header .navbar .sidebar-toggle {
        color: #fff;
        display: none;
    }

</style>
@section('main-content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
            <div class="forgot">
                <h4>Admin Reset Username</h4>
                <form class="form-horizontal" method="POST" action="{{ route('forgotusername.update',$business->id) }}">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}

                    <div class="col-md-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class=" control-label">E-Mail Address</label>


                        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                        @endif

                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">
                                Reset
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>







@endsection