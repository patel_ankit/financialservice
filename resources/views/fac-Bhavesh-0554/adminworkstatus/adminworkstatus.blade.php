@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .search-btn {
            position: absolute;
            top: 46px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        .panel-primary {
            /*border-color:#ffffff !important;*/
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 16px !important;
            padding: 2px 0px 0px 0px !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border-radius: 4px;
            border: 2px solid #2fa6f2;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field {
            border-color: #3c8dbc !important;
            border: 2px solid #2fa6f2;
            border-radius: 4px;
        }

        .select2-container--default .select2-selection--multiple {
            border: 2px solid #2fa6f2;
        }

        .select2-container--default .select2-selection--single {
            height: 40px;
            line-height: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 5px;
            right: 3px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-color: #000 transparent transparent transparent;
        }


        #countryList {
            margin: 40px 0px 0px 0px;
            padding: 0px 0px;
            border: 0px solid #ccc;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 245px;
            z-index: 99999;
        }

        #countryList li {
            border-bottom: 1px solid #dcdcdc;
            background: #f4f4f4;
        }

        #countryList li a {
            padding: 10px 10px;
            display: block;
        }

        #countryList li:hover {
            background: #e7e5e5;
        }

        .new_images_sec .col-md-4 {
            position: relative;
        }

        .new_images_sec .col-md-4 .arrow {
            position: absolute;
            top: 10px;
            right: -6px;
        }

        .new_images_sec .col-md-4 {
            position: relative;
        }

        .new_images_sec .col-md-4 img {
            height: 42px !important;
        }

        .new_images_sec .col-md-4 .arrow {
            position: absolute;
            top: 10px;
            right: -6px;
        }

        .new_images_sec .col-md-4.lastimgbox .arrow {
            position: absolute;
            top: 10px;
            left: -6px;
        }

        .searchboxmain {
            float: left;
            display: FLEX;
            margin-top: -7px;
            justify-content: space-between;
        }

        .searchboxmain .form-control {
            margin-right: 10px !important;
        }

        .clear {
            clear: both;
        }

        /*.page-title h1{float:left; margin-left:15%;}*/
        .page-title {
            padding: 10px 30px 10px !important;
        }

        #countryList {
            margin: 40px 0px 0px 0px;
            padding: 0px 0px;
            border: 0px solid #ccc;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 320px;
            z-index: 99999;
        }

        #countryList li {
            border-bottom: 1px solid #0070b1;
            background: #038bd9;
        }

        #countryList li a {
            padding: 10px 10px;
            display: block;
            color: #fff !important;
            text-align: left;
        }

        #countryList li:hover {
            background: #27a1e7;
        }

        .nav.nav-tabs li {
            width: 12.8% !important;
        }

        .clear {
            clear: both;
        }

        .mt25 {
            margin-top: 25px;
        }

        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .pointernone {
            pointer-events: none
        }

        .pad20 {
            padding: 20px;
        }

        .nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
            cursor: default;
        }

        .nav-tabs > li {
            margin: 0px 0 0 8px;
        }

        .nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
            border-color: #000 !important;
            color: #000 !important;
            background: #ffff99;
            border-radius: 5px !important;
        }

        .nav-tabs {
            padding: 12px;
            border: 1px solid #3598dc !important;
        }

        .btnaddmore {
            background: #337ab7;
            display: inline-block;
            margin-bottom: 5px;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .btnremove {
            background: #ff0000;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .padzero {
            padding: 0px !important;
        }

        .officermainbox {
            border: 1px solid #ccc;
            padding: 20px;
            margin-bottom: 30px;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .card ul li {
            width: 24% !important;
        }

        .card ul.test li a {
            background: #ffcc66 !important;
        }

        .card ul.test li.active a {
            background: #00a0e3 !important;
        }

        .card ul li a {
            display: block;
            width: 100%;
            color: #333;
            text-transform: capitalize;
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);
            border: 1px solid #979800 !important;
        }

        .card ul li a:hover {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li.active a {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover {
            color: #fff !important;
            background: #12186b !important;
            border: 1px solid #12186b !important;
        }

        .card ul.submaintab li a {
            width: 18.5% !important;
            float: left;
            margin: 0px 4px !important;
            cursor: pointer;
        }

        .card ul.submaintab li.active a {
            cursor: pointer;
        }

        .card ul.submaintab li {
            margin-bottom: 5px !important;
        }

        .card ul.submaintab li, .card ul.submaintab li.active {
            background: none !important;
            border: 0px !important;
            width: 100% !important;
        }

        .card ul.submaintab li.active a {
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%) !important;
            color: #333 !important;
            border: 1px solid #979800 !important;
        }

        .card ul.submaintab li.active a:hover, .card ul.submaintab li.active a.active {
            color: #fff !important;
            background: #12186b !important;
            border: 1px solid #12186b !important;
        }


        .card .nav-tabs {
            border: 0px !important;
        }

        .feeschargesbox .form-control {
            text-align: right;
        }

        .hrdivider {
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
            margin-top: 33px;
        }

        .hrdivider2 {
            margin-top: 14px;
            margin-bottom: 30px;
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
        }

        .officerchange .form-control {
            background: #fff !important;
        }

        .add-row {
            background: #007bff;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .delete-row {
            background: #dc3545;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #FFF !important;
            border: 1px solid #12186b !important;
            background: #12186b !important;
            cursor: default;
            border-radius: 8px;
        }

        #renewRecord .form-control {
            margin-bottom: 15px;
        }

        #renewRecord label.text-right {
            text-align: right !important;
            width: 100%;
        }

        .clear {
            clear: both;
        }

        .inlinebutton {
            display: inline-block;
            width: auto;
            padding: 5px 10px !important;
        }

        .fade {
            display: none;
        }

        .fade.in {
            display: block;
        }

        input[type="file"] {
            opacity: 1 !important;
            position: relative;
            font-size: 14px !important;
        }

        .nav > li > a {
            position: relative;
            display: block;
            padding: 10px 5px;

        }

        a.disabled {
            pointer-events: none;
            cursor: default;
            opacity: 0.5;

            /*color: currentColor;*/
            /*cursor: not-allowed;*/
            /*opacity: 0.5;*/
            /*text-decoration: none;*/
        }

        .clear {
            clear: both;
        }

        .btnaddrecord {
            width: auto;
            float: right;
            cursor: pointer;
            padding: 5px 10px !important;
            margin-bottom: 10px;
        }

        .modal-header {
            background: #f5efa8 !important;
            border-bottom: 2px solid #1b5bab;
        }

        .panel.panel-default .panel-heading h4 {
            font-size: 20px !important;
            padding: 10px !important;
            background: #b3e4a6 !important;
        }

        .panel.panel-default .panel-heading h4 a:hover, .panel.panel-default .panel-heading h4 a {
            color: #103b68 !important;
        }

        .ac_name_first {
            width: 97% !important;
        }

        .panel-title a .glyphicon-plus {
            display: none;
        }

        .panel-title a .glyphicon-minus {
            display: block;
        }

        .panel-title a.collapsed .glyphicon-plus {
            display: block;
        }

        .panel-title a.collapsed .glyphicon-minus {
            display: none;
        }

        .recordform label.control-label {
            text-align: right;
            padding-top: 7px;
        }

        #renewRecord .form-control {
            margin-bottom: 15px;
        }

        #renewRecord label.text-right {
            text-align: right !important;
            width: 100%;
        }

        .newcmpname {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 77px;
        }

        .Branch .titleleft {
            width: 45%;
            float: left;
            text-align: left;
            padding-left: 15px;
            padding-top: 5px;
        }

        .togglebox {
            float: right;
            margin-right: 10px;
        }

        .togglebox label {
            padding-right: 10px;
        }

        .customfieldsbox {
            border: 1px solid #ccc;
            padding: 15px;
            margin-bottom: 15px;
        }

        .mt30 {
            margin-top: 30px;
        }

        .btn-toggle a {
            margin: 0px 5px;
            border-radius: 4px !important;
        }

        .nav-tabs > li {
            width: 19.6% !important;
        }

        .btnyes.active {
            background-color: #00a65a !important;
            border-color: #008d4c !important;
            color: #fff !important;
        }

        .btnno.active {
            background-color: #ac2925 !important;
            border-color: #ac2925 !important;
            color: #fff !important;
        }

        .card .nav-tabs li.subtab1 a, .card .nav-tabs li.subtab2 a, .card .nav-tabs li.subtab3 a, .card .nav-tabs li.subtab4 a, .card .nav-tabs li.subtab5 a {
            padding: 10px;
        }

        .card .nav-tabs li.subtab1 {
            width: auto !important;
        }

        .card .nav-tabs li.subtab2 {
            width: auto !important;
        }

        .card .nav-tabs li.subtab3 {
            width: auto !important;
        }

        .card .nav-tabs li.subtab4 {
            width: auto !important;
        }

        .card .nav-tabs li.subtab5 {
            width: auto !important;
        }

        .sel_banking {
            display: inline-flex;
        }

        .sel_banking select {
            width: 220px;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                width: 800px;
                margin: 30px auto;
            }
        }

        @media only screen and (max-width: 1050px) {
            .nav.nav-tabs li {
                width: 23.4% !important;
                margin: 3px 4px;
            }
        }

        @media only screen and (max-width: 991px) {
            .ac_name_first {
                width: 95% !important;
            }
        }

        @media only screen and (max-width: 915px) {
            .sel_banking {
                width: 100%;
            }

            .sel_banking select {
                width: 50%;
                float: left;
            }
        }

        @media only screen and (max-width: 525px) {
            .nav.nav-tabs li {
                width: 31.3% !important;
            }

            .ac_name_first {
                width: 92% !important;
            }

            .sel_banking {
                display: block;
            }

            .sel_banking select {
                width: 98%;
            }
        }

        @media only screen and (max-width: 418px) {
            .nav.nav-tabs li {
                width: 47.5% !important;
            }
        }

        .clear {
            clear: both;
        }

        .mt10 {
            margin-top: 10px !important;
        }

        #myModalShareledger label {
            margin-top: 7px;
        }

        #myModalShareledger .form-control {
            margin-bottom: 15px;
        }

        .btn-view-edit {
            border: 0px;
        }

        .starred {
            color: red;
        }
    </style>
    <div class="content-wrapper">

        <!--<div class="page-title">-->
        <!-- <div class="searchboxmain">-->
        <!--      <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">	 -->
    <!--             {{ csrf_field() }}-->
        <!--             <ul id="countryList"></ul>-->
        <!--              <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important; padding:10px 20px;" href="https://financialservicecenter.net/fac-Bhavesh-0554/workstatus">Reset</a>-->

        <!--</div>-->
        <section class="content-header page-title"><h1>Admin Work Status</h1></section>
        <!--<h1 class="text-center">Admin Work Status</h1>-->
        <div class="clear"></div>
        <!--</div>-->

        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
        @endif

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div class="">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" id="myTab" style="padding:3px;">
                                        <li class="active"><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
                                        <li><a href="#tab2primary" data-toggle="tab">License</a></li>
                                        <li><a href="#tab3primary" data-toggle="tab">Taxation</a></li>
                                        <li><a href="#tab4primary" data-toggle="tab">Banking</a></li>
                                        <li><a href="#tab5primary" data-toggle="tab">Income</a></li>
                                        <li><a href="#tab6primary" data-toggle="tab">Expense </a></li>
                                        <li><a href="#tab7primary" data-toggle="tab">Others </a></li>

                                    </ul>
                                </div>

                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active disablebox newcheckbox" id="tab1primary">
                                            <div class="col-md-12" style="padding-left:5px; margin-top:5px;">

                                                <div class="form-group row card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                                                    <div class="col-lg-4 col-md-12" style="padding-top:10px; padding-right:0px;display: inline-block;">
                                                        <label class="col-lg-1 col-xs-4 text-right padtop7 wstate1" style="padding-left: 0px; margin-top:10px;">State :</label>
                                                        <div class="col-md-3 wstate2 col-xs-3" style="padding-left: 0px;padding-right: 0px;"><input type="text" value="{{$common->state}}" style="" class="form-control fsc-input" readonly/></div>
                                                        <label class="col-lg-2 col-xs-4 text-right padtop7 wcontrol1" style="padding-left: 0px;margin-top:10px;">Control #:</label>
                                                        <div class="col-md-3 wcontrol2 col-xs-3" style="padding-left: 0px;padding-right: 0px;"><input type="text" value="{{$common->contact_number}}" class="form-control fsc-input" readonly/></div>
                                                    </div>

                                                    <div class="col-lg-8 col-md-12" style="padding-right:0px;">
                                                        <div class="panel-heading">
                                                            <ul class="nav nav-tabs" id="myTab2" style="padding:5px 0px !important;float: right;margin-right: 5px !important;">
                                                                <li class="active subtab1"><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>
                                                                <li class="subtab2"><a href="#subtab2primary" class="" data-toggle="tab">Share Ledger</a></li>
                                                                <li class="subtab3"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>
                                                                <li class="subtab4"><a href="#subtab4primary" class="" data-toggle="tab">Minutes</a></li>
                                                                <li class="subtab5"><a href="#subtab5primary" class="" data-toggle="tab">Docs</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-content" style="padding-top:5px; ">
                                                    <div class="tab-pane fade in active  newcheckbox" id="subtab1primary">


                                                        <?php
                                                        if(isset($formation->id) != '')
                                                        {
                                                        $formationdate = 'Apr-1-2021';
                                                        if('Apr-1-2021' >= $formationdate)
                                                        {

                                                        ?>
                                                        <a style="display:block; cursor:pointer;" data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew btnaddrecord disabled">Add Corporation Renewal Record</a>
                                                        <?php
                                                        }
                                                        else if('Apr-1-2022' >= $formationdate)
                                                        {
                                                        ?>
                                                        <a style="display:block; cursor:pointer;" data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew btnaddrecord disabled">Add Corporation Renewal Record</a>
                                                        <?php

                                                        }
                                                        else if('Apr-1-2023' >= $formationdate)
                                                        {
                                                        ?>
                                                        <a style="display:block; cursor:pointer;" data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew btnaddrecord disabled">Add Corporation Renewal Record</a>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <a style="display:block; cursor:pointer;" data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew btnaddrecord">Add Corporation Renewal Record</a>
                                                        <?php

                                                        }
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <a style="display:block; cursor:pointer;" data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew btnaddrecord">Add Corporation Renewal Record</a>
                                                        <?php

                                                        }
                                                        ?>


                                                        <div class="clear"></div>

                                                        <div class="table-responsive">
                                                            <table class="table table-bordered tablestriped">
                                                                <thead>
                                                                <tr>
                                                                    <th>Renew Year</th>
                                                                    <th>Renew For</th>
                                                                    <th>Paid Amount</th>
                                                                    <th>Payment Method</th>
                                                                    <th>Corporation Status</th>
                                                                    <th>SOS Reciept</th>
                                                                    <th>Annual Officer</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>


                                                                <tbody>
                                                                <tr>
                                                                    @foreach($formations as $formation)
                                                                        <td class="text-center">{{$formation->formation_yearvalue}}</td>
                                                                        <td class="text-center">{{$formation->formation_yearbox}} <?php if ($formation->formation_yearbox != '' && $formation->formation_yearbox != null) {
                                                                                echo "Year";
                                                                            } else {
                                                                            } ?></td>
                                                                        <td class="text-center">{{$formation->formation_amount}}</td>
                                                                        <td class="text-center">{{$formation->formation_payment}}</td>
                                                                        <td class="text-center">{{$formation->record_status}}</td>

                                                                        <td class="text-center">
                                                                        <!--<a href="{{url('public/adminupload')}}/{{$formation->annualreceipt}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>-->
                                                                            <a data-toggle="modal" num="{{ $formation->id}}" class="btn  btn-sm openBtnadminformation"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        </td>

                                                                        <td class="text-center">
                                                                        <!--<a href="{{url('public/adminupload')}}/{{$formation->formation_work_officer}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>-->
                                                                            <a data-toggle="modal" num="{{ $formation->id}}" class="btn  btn-sm openBtnadminformation2"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        </td>


                                                                        <td class="text-center">

                                                                            <button type="button" class="btn-action btn-view-edit passingFormation" data-id="{{$formation->id}}"><i class="fa fa-edit"></i></button>
                                                                            <form action="{{ route('adminworkstatus.destroy',$formation->id) }}" method="post" style="display:none" id="delete-id-{{$formation->id}}">
                                                                                {{csrf_field()}} {{method_field('DELETE')}}
                                                                                <input type="hidden" name="id" value="{{$formation->id}}">
                                                                            </form>

                                                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                    {event.preventDefault();document.getElementById('delete-id-{{$formation->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>

                                                                        </td>
                                                                </tr>
                                                                @endforeach
                                                                </tbody>

                                                            </table>


                                                            <div id="myModals_<?php //echo $formation->cid;?>" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            <h4 class="modal-title">Annual Receipt </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                <iframe height="450" width="530" src="<?php echo url('/');?>/public/adminupload/<?php //echo $formation->work_annualreceipt;?>"></iframe>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div id="myModals1_<?php //echo $formation->cid;?>" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            <h4 class="modal-title">Work Officer </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                <iframe height="450" width="530" src="<?php echo url('/');?>/public/adminupload/<?php //echo $formation->work_officer;?>"></iframe>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="clear"></div>

                                                    <div class="tab-pane fade  newcheckbox" id="subtab2primary">
                                                        <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalShareledger">Add Record</a>
                                                        <div class="clear"></div>

                                                        <div class="table-responsive mt10">
                                                            <table class="table table-bordered table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>NAME OF CERTIFICATE HOLDER</th>
                                                                    <th>CERTIFICATES ISSUED NO. SHARES*</th>
                                                                    <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                                                                    <th>AMOUNT PAID THEREON</th>
                                                                    <th>DATE OF TRANSFER OF SHARES*</th>
                                                                    <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                                                                    <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>

                                                                    @foreach($sharledger as $ledger)
                                                                        <td>{{$loop->index+1}}</td>
                                                                        <td>{{$ledger->holder_name}}</td>
                                                                        <td>{{$ledger->issue_certificate}}</td>
                                                                        <td>{{$ledger->origine_issue_from}}</td>
                                                                        <td>{{$ledger->paid_amount}}</td>
                                                                        <td>{{$ledger->transfer_date}}</td>
                                                                        <td>{{$ledger->surrender_certificate}}</td>
                                                                        <td>{{$ledger->surrender_certificate_no}}</td>
                                                                        <td class="text-center">
                                                                            <!--<button type="button" class="btn-action btn-view-edit passingIDS" data-id="8"><i class="fa fa-edit"></i></button>-->
                                                                            <button type="button" class="btn-action btn-view-edit ledgerID" data-id="{{$ledger->id}}"><i class="fa fa-edit"></i></button>
                                                                            <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroyledger',$ledger->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                        </td>
                                                                </tr>

                                                                @endforeach

                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane fade  newcheckbox" id="subtab3primary">
                                                        Amendment
                                                    </div>
                                                    <div class="tab-pane fade newcheckbox" id="subtab4primary">
                                                        Minutes
                                                    </div>


                                                    <div class="tab-pane fade newcheckbox" id="subtab5primary">

                                                        <form method="post" class="form-horizontal" enctype="multipart/form-data" id="myform" action="{{ route('adminworkstatus.updatedocument')}}">
                                                            {{csrf_field()}}

                                                            <div class="col-md-12">
                                                                <div class="row form-group">
                                                                    <label class="col-md-2" style="padding-top:6px;width:13% !important;">Document Name</label>
                                                                    <div class="col-md-3">
                                                                        <input type="hidden" name="admin_id" value="1">
                                                                        <select class="js-example-tags form-control" style="width:100%;" name="documentsname" id="vendor_product" multiple="multiple" required>
                                                                            @foreach($document as $cur)
                                                                                <option value="{{$cur->documentname}}">{{$cur->documentname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if ($errors->has('documentsname'))
                                                                            <span class="help-block">
                                                           <strong>{{ $errors->first('documentsname') }}</strong>
                                                           </span>
                                                                        @endif
                                                                    </div>

                                                                    <div class="col-md-1" style="margin-top:1%;"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a></div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3" style="padding-top:9px;">Upload</label>
                                                                        <div class="col-md-9">

                                                                            <input type="file" class="form-control" name="admindocument" required/>

                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-2" style=" padding-right:3px;width:10% !important;">
                                                                        <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                    </div>
                                                                    <div class="col-md-2" style="padding-left:3px;width:10% !important;">
                                                                        <a class="btn_new_cancel" href="#">Cancel</a>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                        </form>

                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:50px">No.</th>
                                                                <th class="text-left">Document Name</th>
                                                                <th style="width:80px;">View</th>
                                                                <th style="width:80px;">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                @foreach($admindoc as $admindoc1)
                                                                    <td class="text-center">{{$loop->index+1}}</td>
                                                                    <td class="text-left">{{$admindoc1->documentsname}}</td>
                                                                    <td class="text-center"><a href="{{url('public/adminupload')}}/{{$admindoc1->admindocument}}" target="_blank" class="btn-action btn-view-edit"><i class="fa fa-eye"></i></a></td>
                                                                    <td class="text-center">
                                                                    <!--<a data-toggle="modal" data-id="{{$admindoc1->id}}" data-target="#renewDocument" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>-->
                                                                        <button type="button" class="btn-action btn-view-edit passingID" data-id="{{$admindoc1->id}}"><i class="fa fa-edit"></i></button>
                                                                        <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroydocument',$admindoc1->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>

                                                                    </td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>

                                                <div class="clear"></div>


                                                <!-- Modal Start-->
                                                <div class="modal fade" id="myModal" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Update Dodument Record</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('adminworkstatus.updatedocuments',$admindoc1->id)}}">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" class="form-control" name="id" id="idkl">
                                                                    <input type="hidden" class="form-control" name="admin_id" id="admin_id">
                                                                    <!--<input type="text" class="form-control" name="documentsname" id="documentsname">-->
                                                                    <!--<input type="text" class="form-control" name="admindocument" id="admindocument">-->

                                                                    </br>
                                                                    <div class="row">
                                                                        <div class="col-md-4"><label class="control-label text-right">Document Name :</label></div>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control" style="width:85%;" name="documentsname" id="documentsname" required>
                                                                                @foreach($document as $cur)
                                                                                    <option value="{{$cur->documentname}}">{{$cur->documentname}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('documentsname'))
                                                                                <span class="help-block">
                                                                   <strong>{{ $errors->first('documentsname') }}</strong>
                                                                   </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    </br>
                                                                    <div class="row">
                                                                        <div class="col-md-4"><label class="control-label text-right">Upload Document : </label>
                                                                        </div>
                                                                        <div class="col-md-8">

                                                                            <label class="file-upload btn btn-primary">
                                                                                <input type="file" class="form-control" name="admindocument"/>
                                                                                Browse for file ... </label>
                                                                            <input type="hidden" name="admindocument_1" id="admindocument"/><span id="admindocument22"></span>
                                                                        </div>
                                                                    </div>

                                                                    </br>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                        </div>
                                                                        <div class="col-md-2" style=" padding-right:3px;">

                                                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                        </div>
                                                                        <div class="col-md-2" style="padding-left:3px;">
                                                                            <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- Modal End-->


                                            </div>
                                        </div>


                                        <script>
                                            $(".passingID").click(function () {
                                                var ids = $(this).attr('data-id');
                                                //alert(ids);
                                                $("#idkl").val(ids);
                                                $('#myModal').modal('show');
                                                //console.log(ids);
                                                $.get('{!!URL::to('getDocumentdata')!!}?documentid=' + ids, function (data) {
                                                    console.log(data);
                                                    if (data == "") {

                                                    } else {
                                                        //$('#county').html(data);
                                                        $('#id').val(data.id);
                                                        $('#admin_id').val(data.admin_id);
                                                        $('#documentsname').val(data.documentsname);
                                                        $('#admindocument').val(data.admindocument);
                                                        $('#admindocument22').html(data.admindocument);
                                                    }


                                                });
                                            });
                                        </script>

                                        <div class="tab-pane fade" id="tab2primary">

                                            <div class="form-group row  card" style="background: #e0f1fd !important;border: 1px solid #3598dc !important;margin: auto;margin-bottom: 10px;">

                                                <div class="col-xs-12" style="padding:0px;">
                                                    <div class="panel-heading">
                                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                            <li style=" width: 145px !important;" class="active"><a href="#subtab8primary" class="" data-toggle="tab">Business License</a></li>
                                                            <li style=" width: 165px !important;"><a href="#subtab9primary" class="" data-toggle="tab">Professional License</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade in active  newcheckbox" id="subtab8primary">
                                                <div class="col-md-12">
                                                    <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                        <h1 class="text-center">Business License Record</h1>
                                                    </div>
                                                </div>
                                            <?php  $buscount = count($buslicense);
                                            if($buscount > 0)
                                            {?>
                                            <!--Record Button Start	-->
                                                <div class="col-md-12 text-right">
                                                    <?php
                                                    if(isset($admin_buslicense->license_year) != '')
                                                    {
                                                    //exit('111111111111111111111111111111');
                                                    if($admin_buslicense->license_year == '2021')
                                                    {
                                                    ?>

                                                    <a style="display:block; cursor:pointer;" href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton disabled" data-toggle="modal" data-target="#myModalbusinesspopup">Add Business License Record</a>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                    <a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup">Add Business License Record</a>
                                                    <?php
                                                    }


                                                    }
                                                    else
                                                    {

                                                    ?>
                                                    <a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup">Add Business License Record</a>
                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="clear"></div>

                                                    <table class="table table-hover table-bordered dataTable no-footer">
                                                        <thead>
                                                        <tr>
                                                            <th style="width:70px;">Year</th>

                                                            <th style="width:130px;">License Gross</th>
                                                            <th style="width:130px;">License Fee</th>

                                                            <th style="width:110px;">License #</th>
                                                            <th style="width:130px;">License Issue Date</th>
                                                            <th style="width:100px;">License Copy</th>
                                                            <th style="width:80px">Status</th>
                                                            <th style="width:100px">Action</th>
                                                            <th style="width:80px">Form</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            @foreach($buslicense as $bus_lic)
                                                                <td class="text-center"> {{ $bus_lic->license_year}}</td>
                                                                <td class="text-center">{{number_format($bus_lic->license_gross,2)}}</td>
                                                                <td class="text-center">{{ $bus_lic->license_fee}}</td>

                                                                <td> {{ $bus_lic->license_no}}</td>
                                                                <td class="text-center"><?php echo date('m/d/Y', strtotime($bus_lic->license_renew_date))?></td>
                                                                <td class="text-center">
                                                                <!--<a href="{{url('public/adminupload')}}/{{$bus_lic->license_copy}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>-->
                                                                    <a data-toggle="modal" num="{{ $bus_lic->id}}" class="btn btn-primary btn-sm openBtnadminlic"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                </td>
                                                                <td class="text-center">{{$bus_lic->license_status}}</td>
                                                                <td class="text-center">
                                                                    <button type="button" class="btn-action btn-view-edit passingIDS" data-id="{{$bus_lic->id}}"><i class="fa fa-edit"></i></button>
                                                                    <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroylicense',$bus_lic->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                                <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>

                                                        </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                </div>
                                                <!--Record Button End	-->

                                                <!--Business License Start	-->
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="display:none;">
                                                    <div class="form-group {{ $errors->has('business_license_jurisdiction') ? ' has-error' : '' }}">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-md-3">
                                                                <label class="control-label" style="font-size:15px;"> Jurisdiction :</label>
                                                                <select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">
                                                                    <option value="">Select</option>
                                                                    <option value="City" @if(Auth::user()->business_license_jurisdiction=='City') selected @endif>City</option>
                                                                    <option value="County" @if(Auth::user()->business_license_jurisdiction=='County') selected @endif>County</option>
                                                                </select>
                                                                @if ($errors->has('business_license_jurisdiction'))
                                                                    <span class="help-block">
																<strong>{{ $errors->first('business_license_jurisdiction') }}</strong>
																</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label id="city-change" class="control-label" style="display:none;">City :</label>
                                                                <label id="county-change" class="control-label">County :</label>
                                                                @if(Auth::user()->business_license_jurisdiction=='County')
                                                                    <select type="text" class="form-control" id="business_license2" name="business_license2">
                                                                        <option value="">Select</option>


                                                                    </select>
                                                                @elseif(Auth::user()->business_license_jurisdiction=='City')
                                                                    <input type="text" class="form-control" id="business_license3" name="business_license2" value="{{Auth::user()->business_license2}}">
                                                                @else
                                                                    <select type="text" class="form-control" id="business_license2" name="business_license2" @if(Auth::user()->business_license_jurisdiction=='City') style="display:none;" @endif>
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                @endif
                                                                <div id="business_license4"></div>
                                                                <div id="business_license5"></div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label id="city-change1" class="control-label" style="display:none;">City # :</label>
                                                                <label id="county-change1" class="control-label">County # :</label>
                                                                <input name="business_license1" placeholder="" value="{{ Auth::user()->business_license1}}" type="text" id="business_license1" class="form-control"/>
                                                                @if ($errors->has('business_license1'))
                                                                    <span class="help-block">
																	<strong>{{ $errors->first('business_license1') }}</strong>
																</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label id="county-change1" class="control-label">License # :</label>
                                                                <input name="business_license3" placeholder="" value="{{ Auth::user()->business_license3}}" type="text" id="business_license1" class="form-control"/>
                                                                @if ($errors->has('business_license3'))
                                                                    <span class="help-block">
																	<strong>{{ $errors->first('business_license3') }}</strong>
																</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="control-label">Expire Date :</label>
                                                                <input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="{{ Auth::user()->due_date2}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-md-6">
                                                                <label class="control-label" style="font-size:15px;padding:0;">Note :</label>
                                                                <input name="notes" placeholder="Note" value="{{ Auth::user()->notes}}" type="text" id="" class="form-control">
                                                            </div> <?php $id1 = Auth::user()->business_license_jurisdiction;?>
                                                            <div class="col-md-2">
                                                                <label></label>
                                                                <a class="btn_new btn-view-license">License Record</a>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label></label>
                                                                <a style="display:block;" data-toggle="modal" num="{{$id1}}" class="btn_new openBtn btn-view-license">View License</a>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <style>.nn {
                                                                        display: none !important
                                                                    }</style>
                                                                <label></label>
                                                                <a style="display:block;" href="" target="_blank" class="btn_new btn-renew">Renew Now</a>

                                                                <label></label>
                                                                <a href="#" class="btn_new btn-renew nn">Renew Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <div class="pull-right"><a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup">Add Business License Record</a></div>
                                                <div class="col-xs-12 text-center"><h3>No License Require</h3></div>
                                            <?php
                                            }
                                            ?>
                                            <!--Business License End	-->

                                            </div>

                                            <!--Profession License Start	-->
                                            <div class="tab-pane fade  newcheckbox" id="subtab9primary">
                                                <div class="col-md-12">
                                                    <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                        <h1 class="text-center">Professional License Record</h1>
                                                    </div>
                                                </div>
                                                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">

                                                    <div class="col-md-12" style="padding-right:0px;">
                                                        <div class="panel-heading">
                                                            <ul class="nav nav-tabs submaintab" id="myTab2" style="padding:3px;">
                                                                <li>
                                                                    @foreach($admin_professional1 as $ak2)
                                                                        <?php
                                                                        if($ak2->professiontype == 'Individual')
                                                                        {
                                                                        $prftype = 'Ind';

                                                                        ?>

                                                                        <a style="font-size:14px !important;" href="#sublicensetab{{$ak2->id}}" data-toggle="tab">
                                                                            {{$ak2->profession}}-{{$ak2->profession_state}}-<?php echo $prftype;?></a>
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                    @endforeach
                                                                </li>
                                                                <li>
                                                                    @foreach($admin_professional1 as $ak2)
                                                                        <?php
                                                                        if($ak2->professiontype != 'Individual')
                                                                        {
                                                                        ?>
                                                                        <a style="font-size:14px !important;" class="" href="#sublicensetab{{$ak2->id}}" data-toggle="tab">
                                                                            {{$ak2->profession}}-{{$ak2->profession_state}}-<?php echo $ak2->professiontype;?></a>

                                                                        <?php
                                                                        }
                                                                        ?>

                                                                    @endforeach

                                                                </li>
                                                            </ul>
                                                            <ul class="nav nav-tabs" id="myTab3" style="padding:3px; display:none;">


                                                                @foreach($admin_professional1 as $ak2)
                                                                    <?php
                                                                    if($ak2->professiontype != 'Individual')
                                                                    {
                                                                    ?>

                                                                    <li><a style="font-size:14px !important;" class="" href="#sublicensetab{{$ak2->id}}" data-toggle="tab">
                                                                            {{$ak2->profession}}-{{$ak2->profession_state}}-<?php echo $ak2->professiontype;?></a></li>
                                                                    <?php
                                                                    }
                                                                    ?>

                                                                @endforeach

                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                                @foreach($admin_professional1 as $ak2)
                                                    @if($ak2->professiontype =='Individual')
                                                        <div class="tab-pane fade" id="sublicensetab{{$ak2->id}}">
                                                            <div class="col-md-12">
                                                                <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                                    <h1 class="text-center">{{$ak2->profession}}-{{$ak2->profession_state}}-{{$ak2->professiontype}}</h1>
                                                                </div>


                                                                <div class="col-md-12 text-right">

                                                                    <?php
                                                                    if(isset($ak2->pro_license_year) != '')
                                                                    {

                                                                    if($ak2->pro_license_year == '2020')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2021')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2022')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2023')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                <!--<a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup">Add Record</a>-->
                                                                    <a class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }


                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                    <a class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</a>

                                                                    <?php
                                                                    }
                                                                    ?>


                                                                <!--<button type="button" class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</button>-->

                                                                    <div class="clear"></div>

                                                                    @if(!empty($ak2->pro_license_year))
                                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:70px;">Year</th>
                                                                                <th style="width:130px;">License Fee</th>
                                                                                <th>Notes</th>
                                                                                <th style="width:100px;">License Copy</th>
                                                                                <th style="width:80px">Status</th>
                                                                                <th style="width:100px">Action</th>
                                                                                <th style="width:80px">Form</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td class="text-center">{{$ak2->pro_license_year}}</td>
                                                                                <td class="text-center">{{$ak2->pro_license_fee}}</td>
                                                                                <td>{{$ak2->pro_license_note}}</td>

                                                                                <td class="text-center"><a href="{{url('public/adminupload')}}/{{$ak2->pro_license_copy}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                                                                <td class="text-center"><a class="btn-action btn-view-edit" style="background:#689203 !important; text-align:center">@if($ak2->status=='Active') Active  @elseif($ak2->status=='Hold') Hold  @elseif($ak2->status=='Hold') @else Inactive  @endif</a></td>
                                                                                <td class="text-center">

                                                                                <!--<button type="button" class="btn-action btn-view-edit" data-id="{{$ak2->id}}"><i class="fa fa-edit"></i></button>-->
                                                                                    <button type="button" class="btn-action btn-view-edit passingIDS33" data-id="{{$ak2->ids}}"><i class="fa fa-edit"></i></button>
                                                                                    <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroyprolic',$ak2->ids)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                                </td>
                                                                                <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>

                                                                            </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    @else

                                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:70px;">Year</th>
                                                                                <th style="width:130px;">License Fee</th>
                                                                                <th>Notes</th>
                                                                                <th style="width:100px;">License Copy</th>
                                                                                <th style="width:80px">Status</th>
                                                                                <th style="width:100px">Action</th>
                                                                                <th style="width:80px">Form</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>


                                                                                <td colspan="7" class="text-center">No Record Found!</td>


                                                                            </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    @endif
                                                                </div>

                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach

                                                @foreach($admin_professional1 as $ak2)
                                                    @if($ak2->professiontype =='Firm')
                                                        <div class="tab-pane fade" id="sublicensetab{{$ak2->id}}">
                                                            <div class="col-md-12">
                                                                <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                                    <h1 class="text-center">{{$ak2->profession}}-{{$ak2->profession_state}}-{{$ak2->professiontype}}</h1>
                                                                </div>


                                                                <div class="col-md-12 text-right">

                                                                    <?php
                                                                    if(isset($ak2->pro_license_year) != '')
                                                                    {

                                                                    if($ak2->pro_license_year == '2020')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2021')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2022')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else if($ak2->pro_license_year == '2023')
                                                                    {
                                                                    ?>


                                                                    <a style="display:block; cursor:pointer;" class="btn_new btn-renew inlinebutton passingIDS22 disabled" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                <!--<a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup">Add Record</a>-->
                                                                    <a class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</a>
                                                                    <?php
                                                                    }


                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                    <a class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</a>

                                                                    <?php
                                                                    }
                                                                    ?>


                                                                <!--<button type="button" class="btn_new btn-renew inlinebutton passingIDS22" data-id="{{$ak2->id}}">Add Record</button>-->

                                                                    <div class="clear"></div>

                                                                    @if(!empty($ak2->pro_license_year))
                                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:70px;">Year</th>
                                                                                <th style="width:130px;">License Fee</th>
                                                                                <th>Notes</th>
                                                                                <th style="width:100px;">License Copy</th>
                                                                                <th style="width:80px">Status</th>
                                                                                <th style="width:100px">Action</th>
                                                                                <th style="width:80px">Form</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td class="text-center">{{$ak2->pro_license_year}}</td>
                                                                                <td class="text-center">{{$ak2->pro_license_fee}}</td>
                                                                                <td>{{$ak2->pro_license_note}}</td>

                                                                                <td class="text-center"><a href="{{url('public/adminupload')}}/{{$ak2->pro_license_copy}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                                                                <td class="text-center"><a class="btn-action btn-view-edit" style="background:#689203 !important; text-align:center">@if($ak2->status=='Active') Active  @elseif($ak2->status=='Hold') Hold  @elseif($ak2->status=='Hold') @else Inactive  @endif</a></td>
                                                                                <td class="text-center">

                                                                                <!--<button type="button" class="btn-action btn-view-edit" data-id="{{$ak2->id}}"><i class="fa fa-edit"></i></button>-->
                                                                                    <button type="button" class="btn-action btn-view-edit passingIDS33" data-id="{{$ak2->ids}}"><i class="fa fa-edit"></i></button>
                                                                                    <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroyprolic',$ak2->ids)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                                </td>
                                                                                <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>

                                                                            </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    @else

                                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:70px;">Year</th>
                                                                                <th style="width:130px;">License Fee</th>
                                                                                <th>Notes</th>
                                                                                <th style="width:100px;">License Copy</th>
                                                                                <th style="width:80px">Status</th>
                                                                                <th style="width:100px">Action</th>
                                                                                <th style="width:80px">Form</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>


                                                                                <td colspan="7" class="text-center">No Record Found!</td>


                                                                            </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    @endif
                                                                </div>

                                                            </div>
                                                        </div>
                                                @endif
                                            @endforeach

                                            <!--	<div class="col-md-12 text-right">
												        <a href="#myModalbusinesspopupprofessional" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopupprofessional">Add Record</a>
												        <div class="clear"></div>


        												@if($ak1!=NULL)





                                                <div class="">
        													<div class="field_wrapper">
        														<div id="field0">
        															<div class="professional_tabs_main">
        																<div class="professional_tabs">
        																	<div class="professional_tab professional_profession" style="width:160px;">
        																		    <label>Profession :</label>
            																		<select type="text" class="form-control-insu" id="" name="profession[]">
            																			<option value="">Select</option>
                        																<option value="ERO" @if($ak1->profession=='ERO') selected @endif>ERO</option>
                        																<option value="CPA" @if($ak1->profession=='CPA') selected @endif>CPA</option>
                        																<option value="Mortgage Broker" @if($ak1->profession=='Mortgage Broker') selected @endif>Mortgage Broker</option>
                        																<option value="PTIN" @if($ak1->profession=='PTIN') selected @endif>PTIN</option>
                        																<option value="Insurance Agent" @if($ak1->profession=='Insurance Agent') selected @endif>Insurance Agent</option>
                        																<option value="Insurance Broker" @if($ak1->profession=='Insurance Agency') selected @endif>Insurance Agency</option>
                        																<option value="MLO" @if($ak1->profession=='MLO') selected @endif>MLO</option>
            																		</select>
        																	</div>



        																	<div class="professional_tab professional_state">
        																		<label>State :</label>
        																		<select name="profession_state[]" id="profession_state" class="form-control-insu">
        																		    @if(empty($ak1->profession_state)) @else <option value="{{$ak1->profession_state}}">{{$ak1->profession_state}}</option> @endif
                                                        <option value="AK">AK</option>
        																		<option value="AS">AS</option>
        																		<option value="AZ">AZ</option>
        																		<option value="AR">AR</option>
        																		<option value="CA">CA</option>
        																		<option value="CO">CO</option>
        																		<option value="CT">CT</option>
        																		<option value="DE">DE</option>
        																		<option value="DC">DC</option>
        																		<option value="FM">FM</option>
        																		<option value="FL">FL</option>
        																		<option value="GA">GA</option>
        																		<option value="GU">GU</option>
        																		<option value="HI">HI</option>
        																		<option value="ID">ID</option>
        																		<option value="IL">IL</option>
        																		<option value="IN">IN</option>
        																		<option value="IA">IA</option>
        																		<option value="KS">KS</option>
        																		<option value="KY">KY</option>
        																		<option value="LA">LA</option>
        																		<option value="ME">ME</option>
        																		<option value="MH">MH</option>
        																		<option value="MD">MD</option>
        																		<option value="MA">MA</option>
        																		<option value="MI">MI</option>
        																		<option value="MN">MN</option>
        																		<option value="MS">MS</option>
        																		<option value="MO">MO</option>
        																		<option value="MT">MT</option>
        																		<option value="NE">NE</option>
        																		<option value="NV">NV</option>
        																		<option value="NH">NH</option>
        																		<option value="NJ">NJ</option>
        																		<option value="NM">NM</option>
        																		<option value="NY">NY</option>
        																		<option value="NC">NC</option>
        																		<option value="ND">ND</option>
        																		<option value="MP">MP</option>
        																		<option value="OH">OH</option>
        																		<option value="OK">OK</option>
        																		<option value="OR">OR</option>
        																		<option value="PW">PW</option>
        																		<option value="PA">PA</option>
        																		<option value="PR">PR</option>
        																		<option value="RI">RI</option>
        																		<option value="SC">SC</option>
        																		<option value="SD">SD</option>
        																		<option value="TN">TN</option>
        																		<option value="TX">TX</option>
        																		<option value="UT">UT</option>
        																		<option value="VT">VT</option>
        																		<option value="VI">VI</option>
        																		<option value="VA">VA</option>
        																		<option value="WA">WA</option>
        																		<option value="WV">WV</option>
        																		<option value="WI">WI</option>
        																		<option value="WY">WY</option>
        																		</select>
        																	</div>

        																	<div class="professional_tab professional_profession" style="width:100px;">
        																		    <label>Type :</label>
            											                            <select type="text" class="form-control-insu professiontypefunction{{$ak1->id}}" id="professiontype{{$ak1->id}}" name="professiontype[]">

            																			<option value="">Select</option>
                        																<option value="Firm" @if($ak1->professiontype=='Firm') selected @endif>Firm</option>
                        																<option value="Individual" @if($ak1->professiontype=='Individual') selected @endif>Individual</option>
            																		</select>
        																	</div>

        																	<div class="professional_tab professional_effective">
        																		<label>Effective Date :</label>
        																		<input name="profession_effective_date[]" placeholder="Effective Date" value="{{$ak1->profession_epr_date1}}" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
        																	</div>

        																	<div class="professional_tab professional_license">
        																		<label>License # :</label>
        																		<input name="profession_license[]" placeholder="License"  value="{{$ak1->profession_license}}" type="text" id="profession_license" class="form-control-insu">
        																	</div>

        																	<div class="professional_tab professional_expire">
        																		<label>Expire Date :</label>

                        														    @if($up->upload_name==$ak1->pro_id)
                                                    <label></label>
                            														    <input name="profession_epr_date[]" placeholder="Expire Date" value="{{$up->expired_date}}" type="text" id="profession_epr_date" class="form-control-insu" readonly>
                            														    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}}{display:none}</style>
                            														@endif

                                                        <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}" readonly>
        																	</div>

        																	<div class="professional_tab professional_note">
        																		<label>Note :</label>
        																		<input name="profession_note[]" placeholder="Note"  value="{{$ak1->profession_note}}" type="text" id="profession_note" class="form-control-insu">
        																		<input name="profession_id[]" placeholder="Expire Date" value="{{$ak1->id}}" type="hidden"  class="form-control-insu">
        																	</div>

        																<div class="professional_tab professional_state formss{{$ak1->id}} individual{{$ak1->id}}" id="individual{{$ak1->id}}" style="display:none;">
        																		<label>CE Check:</label>

        																	    <?php
                                                if($ak1->ce_check == '1')
                                                {
                                                ?>
                                                        <input name="ce[]"  type="checkbox" id="ce_{{$ak1->id}}" value="1" checked/>
        																                <label for="ce_{{$ak1->id}}"></label>
        																	        <?php
                                                }
                                                else
                                                {
                                                ?>
                                                        <input name="ce[]"  type="checkbox" id="ce_{{$ak1->id}}" value="1"/>
        																                <label for="ce_{{$ak1->id}}"></label>
        																	        <?php
                                                }
                                                ?>

                                                        </label>
        																	</div>

        																	</script>
        																	<div class="professional_tab  professional_effective_1">
        																		<label></label>
        																	    	<a data-toggle="modal-history" num="{{$ak1->profession.'-'.$ak1->profession_state.'-'.$ak1->profession_license}}" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
        																	</div>

        																	<div class="professional_tab professional_license_1">

                    														    @if($up->upload_name==$ak1->pro_id)
                                                    <label></label>
                    														    <a data-toggle="modal" num="{{$ak1->profession.'-'.$ak1->profession_state.'-'.$ak1->profession_license}}" class="btn_new btn-view-license openBtn Pro-btn">View License</a>
                    														    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}}{display:none}</style>
                        														@endif

                                                        <label class="btn-success-{{$ak1->pro_id}}"></label>
                    															<a class="btn_new btn-view-license Pro-btn  btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">File Not Upload</a>
        														            </div>

        																	<div class="professional_tab  professional_expire_1">

                        														@if($up->upload_name==$ak1->pro_id)
                                                    <label></label>
                        														    <a href="{{$up->website_link}}" class="btn_new btn-renew" target='_blank'>Renew Now</a>
                        														    <style>.btn-success-{{str_replace(' ', '',$ak1->pro_id)}}{display:none}</style>
                        														@endif

                                                        <label class="btn-success-{{$ak1->pro_id}}"></label>
                        														<a href="javascript:void(0);" class="btn_new btn-renew btn-success-{{str_replace(' ', '',$ak1->pro_id)}}">Renew Now</a>
        																	</div>

        																</div>
        															</div>
        														</div>
        													</div>
        											    </div>


        											    <script>
            										    $(document).ready(function()
                                                        {
                                                            var pro=$('#professiontype{{$ak1->id}}').val();
                                                            //	Form,Individual
                                                            //formss,individual
                                                            //alert(pro);
                                                            if(pro == 'Firm')
                                                            {
                                                                $(".formss{{$ak1->id}}").hide();
                                                            }
                                                            else if(pro == 'Individual')
                                                            {
                                                                $(".individual{{$ak1->id}}").show();
                                                            }

                                                        });

            											</script>
            											<script>
            											$(document).ready(function()
                                                        {
                										    $(".professiontypefunction{{$ak1->id}}").on("click", function()
                                                            {
                                                                //alert();
                                                                var pro=$('#professiontype{{$ak1->id}}').val();
                                                               // alert(pro);
                                                                if(pro == 'Firm')
                                                                {
                                                                    $(".formss{{$ak1->id}}").hide();
                                                                }
                                                                else if(pro == 'Individual')
                                                                {
                                                                    //$(".individual{{$ak1->id}}").hide();
                                                                    $("#individual{{$ak1->id}}").show();
                                                                }
                                                            });
                                                        });


            											</script>

        											    @endif


                                                    -->
                                            </div>
                                            <!--Profession License  End-->


                                            <div class="" style="display:none;">
                                                <div class="field_wrapper">
                                                    <div id="field0">
                                                        <div class="professional_tabs_main">
                                                            <div class="professional_tabs">
                                                                <div class="professional_tab professional_profession">
                                                                    <label>Profession :</label>
                                                                    <select type="text" class="form-control-insu" id="" name="profession[]">
                                                                        <option value="">Select</option>

                                                                    </select>
                                                                </div>
                                                                <div class="professional_tab professional_state">
                                                                    <label>State :</label>
                                                                    <select name="profession_state[]" id="profession_state" class="form-control-insu">
                                                                        <option value="AK">AK</option>
                                                                        <option value="AS">AS</option>
                                                                        <option value="AZ">AZ</option>
                                                                        <option value="AR">AR</option>
                                                                        <option value="CA">CA</option>
                                                                        <option value="CO">CO</option>
                                                                        <option value="CT">CT</option>
                                                                        <option value="DE">DE</option>
                                                                        <option value="DC">DC</option>
                                                                        <option value="FM">FM</option>
                                                                        <option value="FL">FL</option>
                                                                        <option value="GA">GA</option>
                                                                        <option value="GU">GU</option>
                                                                        <option value="HI">HI</option>
                                                                        <option value="ID">ID</option>
                                                                        <option value="IL">IL</option>
                                                                        <option value="IN">IN</option>
                                                                        <option value="IA">IA</option>
                                                                        <option value="KS">KS</option>
                                                                        <option value="KY">KY</option>
                                                                        <option value="LA">LA</option>
                                                                        <option value="ME">ME</option>
                                                                        <option value="MH">MH</option>
                                                                        <option value="MD">MD</option>
                                                                        <option value="MA">MA</option>
                                                                        <option value="MI">MI</option>
                                                                        <option value="MN">MN</option>
                                                                        <option value="MS">MS</option>
                                                                        <option value="MO">MO</option>
                                                                        <option value="MT">MT</option>
                                                                        <option value="NE">NE</option>
                                                                        <option value="NV">NV</option>
                                                                        <option value="NH">NH</option>
                                                                        <option value="NJ">NJ</option>
                                                                        <option value="NM">NM</option>
                                                                        <option value="NY">NY</option>
                                                                        <option value="NC">NC</option>
                                                                        <option value="ND">ND</option>
                                                                        <option value="MP">MP</option>
                                                                        <option value="OH">OH</option>
                                                                        <option value="OK">OK</option>
                                                                        <option value="OR">OR</option>
                                                                        <option value="PW">PW</option>
                                                                        <option value="PA">PA</option>
                                                                        <option value="PR">PR</option>
                                                                        <option value="RI">RI</option>
                                                                        <option value="SC">SC</option>
                                                                        <option value="SD">SD</option>
                                                                        <option value="TN">TN</option>
                                                                        <option value="TX">TX</option>
                                                                        <option value="UT">UT</option>
                                                                        <option value="VT">VT</option>
                                                                        <option value="VI">VI</option>
                                                                        <option value="VA">VA</option>
                                                                        <option value="WA">WA</option>
                                                                        <option value="WV">WV</option>
                                                                        <option value="WI">WI</option>
                                                                        <option value="WY">WY</option>
                                                                    </select>
                                                                </div>
                                                                <div class="professional_tab professional_effective">
                                                                    <label>Effective Date :</label>
                                                                    <input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
                                                                </div>
                                                                <div class="professional_tab professional_license">
                                                                    <label>License # :</label>
                                                                    <input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu">
                                                                </div>
                                                                <div class="professional_tab professional_expire">
                                                                    <label>Expire Date :</label>

                                                                    <label></label>
                                                                    <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>


                                                                    <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success" readonly>
                                                                </div>


                                                                <div class="professional_tab professional_note">
                                                                    <label>Note :</label>
                                                                    <input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu">
                                                                    <input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" class="form-control-insu">
                                                                </div>
                                                                <div class="professional_tab professional_state">
                                                                    <label>CE :</label>

                                                                    <input name="ce[]" type="hidden"/>
                                                                    <input name="ce1[]" type="checkbox"><label for="ce1" class="form-control-insu" style="background-color: transparent;background-image: none;border:transparent;"> Check
                                                                    </label>
                                                                </div>

                                                                <div class="professional_tab  professional_effective_1">
                                                                    <label></label>
                                                                    <a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License Record</a>
                                                                </div>
                                                                <div class="professional_tab professional_license_1">

                                                                    <label></label>
                                                                    <a data-toggle="modal" num="" class="btn_new btn-view-license openBtn Pro-btn">View License</a>

                                                                    <label class="btn-success"></label>
                                                                    <a class="btn_new btn-view-license Pro-btn  btn-success">File Not Upload</a>
                                                                </div>
                                                                <div class="professional_tab  professional_expire_1">


                                                                    <label></label>
                                                                    <a href="" class="btn_new btn-renew" target='_blank'>Renew Now</a>
                                                                    <label class="btn-success"></label>
                                                                    <a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!--Profession License End	-->
                                        <div class="tab-pane fade" id="tab3primary">

                                            <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">

                                                <div class="col-xs-12" style="padding-right:0px;">
                                                    <div class="panel-heading">
                                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                            <li style=" width: auto !important;" class="active"><a href="#subtab5primaryi" style="padding:8px 15px !important;" class="" data-toggle="tab">Income Tax</a></li>
                                                            <li style=" width: auto !important;"><a href="#subtab6primary" style="padding:8px 15px !important;" class="" data-toggle="tab">Payroll Tax</a></li>
                                                            <li style=" width: auto !important;"><a href="#subtab7primary" style="padding:8px 15px !important;" class="" data-toggle="tab">Personal Property Tax</a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="tab-pane fade in active  newcheckbox" id="subtab5primaryi">

                                                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                                                    <div class="col-md-12" style="width: 63%; padding-right:0px;">
                                                        <div class="panel-heading">
                                                            <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                                <li style=" width: auto !important;" class="active"><a href="#subtab5primaryi1" style="    padding: 10px !important;" class="" data-toggle="tab">Federal</a></li>
                                                                <li style=" width: auto !important;"><a href="#subtab5primaryi2" class="" style="    padding: 10px !important;" data-toggle="tab">State</a></li>


                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane fade in active newcheckbox" id="subtab5primaryi1">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="Branch">
                                                                <div class="col-md-4" style="text-align:left;">
                                                                    <h1>Federal - ({{Auth::user()->typeofcorp1}}) </h1>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <h1>Federal Income Tax Record </h1>
                                                                </div>

                                                                <div class="col-md-3" style="text-align:right;">
                                                                <!--<h1>State - {{Auth::user()->state_of_formation}} - ({{Auth::user()->type_form_file2}})  </h1>-->
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord">Add Federal Income Tax Record</a>
                                                        <div class="clear"></div>

                                                        <?php
                                                        foreach($Incometaxfederal as $Income)
                                                        {

                                                        ?>
                                                        <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                            <div class="panel panel-default">

                                                                <div class="panel-heading" role="tab" id="headingtwo">
                                                                    <h4 class="panel-title" style="margin-bottom:0px;">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $Income->id;?>">
                                                                            <span class="ac_name_first">@if($Income->federalyear!=''){{$Income->federalyear}} @else {{$Income->statesyear}} @endif</span>

                                                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                                                            <i class="more-less glyphicon glyphicon-minus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>

                                                                <div style="clear:both;"></div>

                                                                <div id="collapse<?php echo $Income->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                    <div class="panel-body accordion-body" style="padding-top:15px;">
                                                                        <div class="table-responsive">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered tablestriped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Year</th>
                                                                                        <th>Tax Return</th>
                                                                                        <th>Form No.</th>
                                                                                        <th>Filing Date</th>
                                                                                        <th>Filing Methods</th>
                                                                                        <th>Status</th>
                                                                                        <th>Action</th>
                                                                                    </tr>
                                                                                    </thead>


                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <?php
                                                                                        foreach($Incometax2 as $Income2)
                                                                                        {
                                                                                        if($Income2->federalyear == $Income->federalyear)
                                                                                        {
                                                                                        ?>


                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalyear}} @else {{$Income2->statesyear}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federaltax}} @else {{$Income2->statestax}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalformno}} @else {{$Income2->statesformno}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{date('M-d Y',strtotime($Income2->federaldate))}} @else {{date('M-d Y',strtotime($Income2->statesdate))}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalmethod}} @else {{$Income2->statesmethod}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalstatus}} @else {{$Income2->statesstatus}} @endif</td>
                                                                                        <td class="text-center">
                                                                                            <button type="button" class="btn-action btn-view-edit passingTaxation" data-id="{{$Income2->id}}"><i class="fa fa-edit"></i></button>
                                                                                            <form action="{{ route('adminworkstatus.destroy',$Income2->id) }}" method="post" style="display:none" id="delete-id-{{$Income2->id}}">
                                                                                                {{csrf_field()}} {{method_field('DELETE')}}
                                                                                                <input type="hidden" name="id" value="{{$Income2->id}}">
                                                                                            </form>

                                                                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                                    {event.preventDefault();document.getElementById('delete-id-{{$Income2->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>

                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    }

                                                                                    }
                                                                                    ?>
                                                                                    </tbody>

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="tab-pane fade newcheckbox" id="subtab5primaryi2">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="Branch">
                                                                <div class="col-md-4" style="text-align:left;">
                                                                    <h1>State - ({{Auth::user()->typeofcorp1}}) </h1>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h1>State Income Tax Record</h1>
                                                                </div>

                                                                <div class="col-md-2" style="text-align:right;">
                                                                <!--<h1>State - {{Auth::user()->state_of_formation}} - ({{Auth::user()->type_form_file2}})  </h1>-->
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord">Add State Income Tax Record</a>
                                                        <div class="clear"></div>

                                                        <?php
                                                        foreach($Incometaxstates as $Income)
                                                        {

                                                        ?>
                                                        <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                            <div class="panel panel-default">

                                                                <div class="panel-heading" role="tab" id="headingtwo">
                                                                    <h4 class="panel-title" style="margin-bottom:0px;">
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $Income->id;?>">
                                                                            <span class="ac_name_first">@if($Income->federalyear!=''){{$Income->federalyear}} @else {{$Income->statesyear}} @endif</span>

                                                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                                                            <i class="more-less glyphicon glyphicon-minus"></i>

                                                                        </a>
                                                                    </h4>
                                                                </div>

                                                                <div style="clear:both;"></div>

                                                                <div id="collapse<?php echo $Income->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                    <div class="panel-body accordion-body" style="padding-top:15px;">
                                                                        <div class="table-responsive">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered tablestriped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Year</th>
                                                                                        <th>Tax Return</th>
                                                                                        <th>Form No.</th>
                                                                                        <th>Filing Date</th>
                                                                                        <th>Filing Method</th>
                                                                                        <th>Status</th>
                                                                                        <th>Action</th>
                                                                                    </tr>
                                                                                    </thead>


                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <?php
                                                                                        foreach($Incometax2 as $Income2)
                                                                                        {
                                                                                        if($Income2->federalyear == $Income->federalyear)
                                                                                        {
                                                                                        ?>


                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalyear}} @else {{$Income2->statesyear}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federaltax}} @else {{$Income2->statestax}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalformno}} @else {{$Income2->statesformno}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{date('M-d Y',strtotime($Income2->federaldate))}} @else {{date('M-d Y',strtotime($Income2->statesdate))}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalmethod}} @else {{$Income2->statesmethod}} @endif</td>
                                                                                        <td class="text-center">@if($Income2->federalyear){{$Income2->federalstatus}} @else {{$Income2->statesstatus}} @endif</td>
                                                                                        <td class="text-center">
                                                                                            <button type="button" class="btn-action btn-view-edit passingTaxation" data-id="{{$Income2->id}}"><i class="fa fa-edit"></i></button>
                                                                                            <form action="{{ route('adminworkstatus.destroy',$Income2->id) }}" method="post" style="display:none" id="delete-id-{{$Income2->id}}">
                                                                                                {{csrf_field()}} {{method_field('DELETE')}}
                                                                                                <input type="hidden" name="id" value="{{$Income2->id}}">
                                                                                            </form>

                                                                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                                    {event.preventDefault();document.getElementById('delete-id-{{$Income2->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>

                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    }

                                                                                    }
                                                                                    ?>
                                                                                    </tbody>

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="tab-pane fade newcheckbox" id="subtab6primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="Branch">
                                                            <div class="col-md-3" style="text-align:left;">
                                                                <h1>Federal / State</h1>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h1>Payroll Tax Record</h1>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord">Add Payroll Tax Record</a>
                                                    <div class="clear"></div>

                                                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                        <div class="panel panel-default">

                                                            <div class="panel-heading" role="tab" id="headingtwo">
                                                                <h4 class="panel-title" style="margin-bottom:0px;">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwopt">
                                                                        <span class="ac_name_first">2012</span>

                                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                                        <i class="more-less glyphicon glyphicon-minus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div style="clear:both;"></div>

                                                            <div id="collapsetwopt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                <div class="panel-body accordion-body" style="padding-top:15px;">

                                                                    <div class="table-responsive">
                                                                        Year
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade newcheckbox" id="subtab7primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="Branch">
                                                            <div class="col-md-3" style="text-align:left;">
                                                                <h1>County - </h1>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <h1>Personal Property Tax Record </h1>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord" style="width:auto;">Add Personal Property Tax Record</a>
                                                    <div class="clear"></div>

                                                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                        <div class="panel panel-default">

                                                            <div class="panel-heading" role="tab" id="headingtwo">
                                                                <h4 class="panel-title" style="margin-bottom:0px;">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwoppt">
                                                                        <span class="ac_name_first">2012</span>

                                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                                        <i class="more-less glyphicon glyphicon-minus"></i>

                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div style="clear:both;"></div>

                                                            <div id="collapsetwoppt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                <div class="panel-body accordion-body" style="padding-top:15px;">

                                                                    <div class="table-responsive">
                                                                        Year
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane fade" id="tab4primary">
                                            <div class="card">
                                                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">

                                                    <div class="col-xs-12" style=" padding:0px;">
                                                        <div class="panel-heading">
                                                            <ul class="nav nav-tabs" id="myTab02" style="padding:5px !important;">
                                                                <li class="" style="width:auto !important;"><a href="#subtab5primaryi02" class="" style="padding:8px 12px;" data-toggle="tab" aria-expanded="true">Banking</a></li>
                                                                <li style="width:auto !important;"><a href="#subtab6primary02" class="" style="padding:8px 12px;" data-toggle="tab" aria-expanded="true">Credit Card</a></li>
                                                                <li style="width:auto !important;"><a href="#subtab7primary02" class="" style="padding:8px 12px;" data-toggle="tab" aria-expanded="true">Loan</a></li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane" id="subtab5primaryi02">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                                    <h1 class="text-center">Banking Record</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a style="display:block; float:right; cursor:pointer;" href="#myModalbankingpopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbankingpopup">Add Banking Record</a>
                                                                    <div class="sel_banking">
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select Bank</option>
                                                                            <option>Bank name 1</option>
                                                                            <option>Bank name 2</option>
                                                                        </select>
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select A/C No.</option>
                                                                            <option>All</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="clear"></div>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered dataTable no-footer">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:50px;">ID</th>
                                                                        <th style="width:130px;">Bank Name</th>
                                                                        <th style="width:130px;">Digits</th>
                                                                        <th style="width:110px;">Start Date</th>
                                                                        <th style="width:130px;">Beginnig Balance</th>
                                                                        <th style="width:100px;">End Date</th>
                                                                        <th style="width:120px">End Balance</th>
                                                                        <th style="width:80px">Account By</th>
                                                                        <th style="width:100px">Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        @foreach($newbanking as $newbank)
                                                                            <td class="text-center"> {{$loop->index+1}}</td>
                                                                            <td class="text-center"> {{ $newbank->bank_name}}</td>
                                                                            <td class="text-center">{{ $newbank->bankfourdigit}}</td>
                                                                            <td> {{ $newbank->bankstartdate}}</td>
                                                                            <td class="text-center">{{number_format($newbank->bank_beginnigbalance,2)}}</td>
                                                                            <td class="text-center">{{$newbank->bankenddate}}</td>
                                                                            <td class="text-center">{{number_format($newbank->bank_endingbalance,2)}}</td>
                                                                            <td class="text-center">{{$newbank->bank_accountdoneby}}</td>

                                                                            <td class="text-center">
                                                                                <button type="button" class="btn-action btn-view-edit newbankingID" data-id="{{$newbank->id}}"><i class="fa fa-edit"></i></button>
                                                                                <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.newbankingdelete',$newbank->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                            </td>


                                                                    </tr>
                                                                    @endforeach
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="subtab6primary02">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                                    <h1 class="text-center">Credit Card Record</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a style="display:block; float:right; cursor:pointer;" href="#myModalbankingpopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbankingpopup">Add Credit Card Record</a>
                                                                    <div class="sel_banking">
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select Bank</option>
                                                                            <option>Bank name 1</option>
                                                                            <option>Bank name 2</option>
                                                                        </select>
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select A/C No.</option>
                                                                            <option>All</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="clear"></div>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered dataTable no-footer">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:70px;">ID</th>
                                                                        <th style="width:130px;">Bank Name</th>
                                                                        <th style="width:130px;">Digits</th>
                                                                        <th style="width:110px;">Start Date</th>
                                                                        <th style="width:130px;">Beginnig Balance</th>
                                                                        <th style="width:100px;">End Date</th>
                                                                        <th style="width:80px">End Balance</th>
                                                                        <th style="width:80px">Account By</th>
                                                                        <th style="width:100px">Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>

                                                                        <td class="text-center"></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>

                                                                        <td class="text-center">
                                                                            <button type="button" class="btn-action btn-view-edit newbankingID" data-id="{{$newbank->id}}"><i class="fa fa-edit"></i></button>
                                                                            <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.newbankingdelete',$newbank->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                        </td>


                                                                    </tr>

                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="subtab7primary02">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="Branch" style="text-align:left;padding-left: 15px;">
                                                                    <h1 class="text-center">Loan Record</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a style="display:block; float:right; cursor:pointer;" href="#myModalbankingpopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbankingpopup">Add Loan Record</a>
                                                                    <div class="sel_banking">
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select Bank</option>
                                                                            <option>Bank name 1</option>
                                                                            <option>Bank name 2</option>
                                                                        </select>
                                                                        <select class="form-control" style="margin-left:10px;">
                                                                            <option>Select A/C No.</option>
                                                                            <option>All</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="clear"></div>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered dataTable no-footer">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:70px;">ID</th>
                                                                        <th style="width:130px;">Bank Name</th>
                                                                        <th style="width:130px;">Digits</th>
                                                                        <th style="width:110px;">Start Date</th>
                                                                        <th style="width:130px;">Beginnig Balance</th>
                                                                        <th style="width:100px;">End Date</th>
                                                                        <th style="width:80px">End Balance</th>
                                                                        <th style="width:80px">Account By</th>
                                                                        <th style="width:100px">Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>

                                                                        <td class="text-center"></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>

                                                                        <td class="text-center">
                                                                            <button type="button" class="btn-action btn-view-edit newbankingID" data-id="{{$newbank->id}}"><i class="fa fa-edit"></i></button>
                                                                            <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.newbankingdelete',$newbank->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                        </td>


                                                                    </tr>

                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!--Modal Add new Banking Start-->
        <div class="modal" id="myModalbankingpopup">
            <div class="modal-dialog" style="width:1000px; !important;">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span>Add Banking Record</h4>
                    </div>

                    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('adminworkstatus.addnewbanking')}}">
                    {{csrf_field()}}

                    <!-- Modal body -->
                        <div class="modal-body">


                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right padleftzero">Bank Name</label>
                                <div class="col-md-3">
                                    <!--MJMJMJMJMJ-->
                                    <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                    <select class="form-control" name="bankname_banking" id="bankname_banking" required>
                                        <option value="">Select</option>
                                        @if($admin_bank)

                                            @foreach($admin_bank as $bank)
                                                <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Last Four Digit A/C #</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="bankfourdigit" id="bankfourdigit" required readonly/>

                                </div>
                                <label class="col-md-3 control-label text-right">Bank Nick name</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="banknick_name" id="banknick_name" required readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Statement Start Date: </label>
                                <div class="col-md-3">
                                    <?php
                                    if(isset($newbankinglast->bankenddate) != '')
                                    {

                                    ?>
                                    <input type="text" class="form-control" name="bankstartdate" value="<?php echo $newbankinglast->bankenddate;?>" id="StartDate_dis" required readonly/>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <input type="text" class="form-control opendate" name="bankstartdate" id="StartDate" required readonly/>
                                    <?php
                                    }
                                    ?>


                                </div>
                                <label class="col-md-3 control-label text-right">Statement Beginning Balance: </label>
                                <div class="col-md-3">
                                    <?php
                                    if(isset($newbankinglast->bank_endingbalance) != '')
                                    {
                                    ?>
                                    <input type="text" class="form-control txtinput_1" name="bank_beginnigbalance" value="<?php echo $newbankinglast->bank_endingbalance;?>" required readonly/>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <input type="text" class="form-control txtinput_1" name="bank_beginnigbalance" required/>
                                    <?php
                                    }
                                    ?>

                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Statement End Date:</label>
                                <div class="col-md-3">
                                    <?php
                                    if(isset($newbankinglast->bankenddate) != '')
                                    {
                                    ?>
                                    <input type="text" class="form-control datepicker" name="bankenddate" id="EndDate_dis" required readonly/>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <input type="text" class="form-control datepicker" name="bankenddate" id="EndDate" required readonly/>
                                    <?php
                                    }
                                    ?>

                                </div>
                                <label class="col-md-3 control-label text-right">Statement Ending Balance:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control txtinput_1" name="bank_endingbalance" required/>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Note:</label>

                                <div class="col-md-9">
                                    <textarea class="form-control" name="bank_notes"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Account Done By:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="bank_accountdoneby" required/>
                                </div>
                                <label class="col-md-3 control-label text-right">Accounts Done Date:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control opendate" name="bank_accountdonedate" required readonly/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>


                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!--Modal Add new Banking End-->


        <script>

            // $("#StartDate_dis").click(function()
            $(document).ready(function () {

                var aa = $("#StartDate_dis").val();
                if (aa != '') {
                    console.log(aa);
                    var arr = aa.split("/");
                    var date = new Date(arr[0] + "-" + arr[1] + "-" + arr[2]);
                    var yy = date.setMonth(date.getMonth() + 1);
                    var d = date.getDate();
                    var m = date.getMonth() + 1;
                    var y = date.getFullYear();
                    //var minDate = new Date(m +1, d,y);
                    //$("#EndDate").datepicker('setDate', minDate);
                    var id4 = '/';
                    var makedate = m + id4 + d + id4 + y;
                    $("#EndDate_dis").val(makedate);

                }

                //$("#EndDate").datepicker({dateFormat: 'dd/mm/yy'});
            });


            // $("#StartDate").click(function()
            $("#StartDate").on('change', function () {

                var aa = $("#StartDate").val();
                if (aa != '') {
                    console.log(aa);
                    var arr = aa.split("/");
                    var date = new Date(arr[0] + "-" + arr[1] + "-" + arr[2]);
                    var yy = date.setMonth(date.getMonth() + 1);
                    var d = date.getDate();
                    var m = date.getMonth() + 1;
                    var y = date.getFullYear();
                    //var minDate = new Date(m +1, d,y);
                    //$("#EndDate").datepicker('setDate', minDate);
                    var id4 = '/';
                    var makedate = m + id4 + d + id4 + y;
                    $("#EndDate").val(makedate);

                }

                //$("#EndDate").datepicker({dateFormat: 'dd/mm/yy'});
            });

            $("#StartDate_2").click(function ()
                //$("#StartDate_2").on('change', function()
            {
                //autoclose: true,
                var aa = $("#StartDate_2").val();
                if (aa != '') {
                    console.log(aa);
                    var arr = aa.split("/");
                    var date = new Date(arr[0] + "-" + arr[1] + "-" + arr[2]);
                    var yy = date.setMonth(date.getMonth() + 1);
                    var d = date.getDate();
                    var m = date.getMonth() + 1;
                    var y = date.getFullYear();
                    //var minDate = new Date(m +1, d,y);
                    //$("#EndDate").datepicker('setDate', minDate);
                    var id4 = '/';
                    var makedate = m + id4 + d + id4 + y;
                    $("#EndDate_2").val(makedate);

                }

                //$("#EndDate").datepicker({dateFormat: 'dd/mm/yy'});
            });

            // $("#StartDate").datepicker
            // ({
            //     dateFormat: 'dd/mm/yy',
            //     onSelect: function (selectedDate)
            //     {
            //         alert(selectedDate)
            //         if (this.id == 'StartDate')
            //         {
            //             console.log(selectedDate);//2020-05-30
            //             var arr = selectedDate.split("/");
            //             var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
            //             var d = date.getDate();
            //             var m = date.getMonth();
            //             var y = date.getFullYear();
            //             var minDate = new Date(y, m, d + 30);
            //             $("#EndDate").datepicker('setDate', minDate);

            //         }
            //     }
            // });
            // $("#EndDate").datepicker({dateFormat: 'dd/mm/yy'});
        </script>

        <!-- The Modal -->
        <div class="modal" id="myModalbusinesspopup">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span> License Add Record</h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">

                        <form method="post" action="{{route('adminworkstatus.store')}}" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Year</label>
                                <div class="col-md-6">
                                    <!--<input type="text" class="form-control" name="license_year" required/>-->
                                    <select class="form-control" name="license_year" required>
                                        <option disabled>Select</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                        <option>2022</option>
                                        <option>2023</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Gross Revenue</label>
                                <div class="col-md-6"><input type="text" class="form-control license_gross" name="license_gross" id="license_gross" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Fee </label>
                                <div class="col-md-6"><input type="text" class="form-control txtinput_1 license_fee" name="license_fee" id="license_fee" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="license_status" required>
                                        <option>Active</option>
                                        <option>Inactive</option>
                                        <option>Hold</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Copy</label>
                                <!--<div class="col-md-6">-->
                                <!--    <input type="file" name="license_copy" required/>-->
                                <!--</div>-->

                                <div class="col-md-8" style="width:50% !important;">
                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" type="file" name="license_copy" style="display:none;" required/>
                                        Browse for file ... </label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Certificate No</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="license_no" required/></div>
                            </div>

                            <!--    <div class="form-group row">-->
                            <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
                            <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
                            <!--</div>-->
                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Website</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="website" required/>http:// or https:// required
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Note</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="license_note"/></div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Issue Date </label>
                                <div class="col-md-6"><input type="date" class="form-control" name="license_renew_date" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right"></label>
                                <div class="col-xs-3" style="width:auto;">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save" style="padding: 8px 30px;">
                                </div>
                                <div class="col-xs-3" style="width:auto;">
                                    <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee" style="padding: 8px 30px;">Cancel</a>
                                </div>
                            </div>

                        </form>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>


        <?php
        if(isset($formation->formation_yearbox) != '')
        {
        ?>

        <div id="myModal1" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">PDF File</h4>
                    </div>
                    <div class="modal-body">


                        <embed src="{{url('public/adminupload')}}/{{$formation->annualreceipt}}" frameborder="0" width="100%" height="400px">


                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>


    <?php
    }
    ?>



    <!--Modal Update new Banking Start-->
        <div class="modal" id="newbankingModal">
            <div class="modal-dialog" style="width:1000px !important">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span> Renew Record</h4>
                    </div>

                    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('adminworkstatus.updatenewbanking')}}">
                    {{csrf_field()}}

                    <!-- Modal body -->
                        <div class="modal-body">


                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right padleftzero">Bank Name</label>
                                <div class="col-md-3">
                                    <input type="hidden" name="adminnewnk_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="newbank_id" id="newbank_id">
                                    <select class="form-control" name="bankname_banking" id="bankname_banking_2" required>
                                        <option value="">Select</option>
                                        @if($admin_bank)

                                            @foreach($admin_bank as $bank)
                                                <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Last Four Digit A/C #</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="bankfourdigit" id="bankfourdigit_2" required readonly/>

                                </div>
                                <label class="col-md-3 control-label text-right">Bank Nick name</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="banknick_name" id="banknick_name_2" required readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Statement Start Date: </label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control opendate" name="bankstartdate" id="StartDate_2" required readonly/>

                                    <!--<input id="StartDate" class="opendate" type="text" name="StartDate">-->


                                </div>
                                <label class="col-md-3 control-label text-right">Statement Beginning Balance: </label>
                                <div class="col-md-3"><input type="text" class="form-control txtinput_1" name="bank_beginnigbalance" id="bank_beginnigbalance" required/></div>

                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Statement End Date:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datepicker" name="bankenddate" id="EndDate_2" required readonly/>

                                    <!--<input id="EndDate" class="datepicker" type="text" name="EndDate">-->

                                </div>
                                <label class="col-md-3 control-label text-right">Statement Ending Balance:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control txtinput_1" name="bank_endingbalance" id="bank_endingbalance" required/>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Note:</label>

                                <div class="col-md-9">
                                    <textarea class="form-control" name="bank_notes" id="bank_notes"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right">Account Done By:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="bank_accountdoneby" id="bank_accountdoneby" required/>
                                </div>
                                <label class="col-md-3 control-label text-right">Accounts Done Date:</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control opendate" name="bank_accountdonedate" id="bank_accountdonedate" required readonly/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>


                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!--Modal Update new Banking End-->


        <script>
            $(".newbankingID").click(function () {
                var nbnkid = $(this).attr('data-id');
                $("#newbank_id").val(nbnkid);
                $('#newbankingModal').modal('show');
                $.get('{!!URL::to('getAdminnewbank')!!}?newbankid=' + nbnkid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        $('#bankname_banking_2').val(data.bankname_banking);
                        $('#bankfourdigit_2').val(data.bankfourdigit);
                        $('#banknick_name_2').val(data.banknick_name);
                        $('#StartDate_2').val(data.bankstartdate);
                        $('#bank_beginnigbalance').val(data.bank_beginnigbalance);
                        $('#EndDate_2').val(data.bankenddate);
                        $('#bank_endingbalance').val(data.bank_endingbalance);
                        $('#bank_notes').val(data.bank_notes);
                        $('#bank_accountdoneby').val(data.bank_accountdoneby);
                        $('#bank_accountdonedate').val(data.bank_accountdonedate);
                    }
                });
            });

        </script>


        <script>
            $(document).ready(function () {
                $(document).on('change', '#bankname_banking', function () {
                    var id = $(this).val();
                    //alert(id);
                    $.get('{!!URL::to('getbankcode')!!}?id=' + id, function (data) {

                        //console.log(data);exit;
                        $.each(data, function (index, subcatobj) {
                            $('#bankfourdigit').val(subcatobj.fourdigit);
                            $('#banknick_name').val(subcatobj.nick_name);

                        })
                    });
                });

                $(document).on('change', '#bankname_banking_2', function () {
                    var id = $(this).val();
                    //alert(id);
                    $.get('{!!URL::to('getbankcode2')!!}?id=' + id, function (data) {

                        //console.log(data);exit;
                        $.each(data, function (index, subcatobj) {
                            $('#bankfourdigit_2').val(subcatobj.fourdigit);
                            $('#banknick_name_2').val(subcatobj.nick_name);

                        })
                    });
                });

            });


        </script>


        <!-- Modal Business License Start-->
        <div class="modal fade" id="myModals" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span>License Renew Record</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="">


                            {{csrf_field()}}
                            <input type="hidden" class="form-control" name="idlic" id="idlic">
                            <input type="hidden" class="form-control" name="admin_id" id="admin_id12">


                            </br>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Year</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="license_year" id="license_year" required>
                                        <option disabled>Select</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                        <option>2022</option>
                                        <option>2023</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Gross Revenue</label>
                                <div class="col-md-6"><input type="text" class="form-control license_gross" name="license_gross" id="license_gross1" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Fee </label>
                                <div class="col-md-6"><input type="text" class="form-control txtinput_1 license_fee" name="license_fee" id="license_fee1" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="license_status" id="license_status" required>
                                        <option>Active</option>
                                        <option>Inactive</option>
                                        <option>Hold</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Copy</label>
                                <div class="col-md-8">
                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" type="file" name="license_copy"/>
                                        Browse for file ... </label>
                                    <input type="hidden" name="license_copy_2" id="license_copy"/><span id="license_copy2"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Certificate No</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="license_no" id="license_no" required/></div>
                            </div>

                            <!--    <div class="form-group row">-->
                            <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
                            <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
                            <!--</div>-->

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Note</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="license_note" id="license_note" required/></div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Issue Date </label>
                                <div class="col-md-6"><input type="date" class="form-control" name="license_renew_date" id="license_renew_date" required/></div>
                            </div>


                            </br>
                            <div class="row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-2" style=" padding-right:3px;">

                                    <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                </div>
                                <div class="col-md-2" style="padding-left:3px;">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal End-->

        <!-- Modal Professional License Start-->
        <div class="modal fade" id="myModals22" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Professional License</h4>
                    </div>
                    <div class="modal-body">

                        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('adminworkstatus.updateprolicense')}}">
                            {{csrf_field()}}

                            <input type="hidden" name="admin_lic_id" id="id22">
                            <input type="hidden" name="admin_id" value="1">

                            <input type="hidden" name="profession72" id="profession72">
                            <input type="hidden" name="professiontype72" id="professiontype72">
                            <input type="hidden" name="profession_state72" value="profession_state72">
                            <input type="hidden" name="profession_license72" id="profession_license72">

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Year</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="pro_license_year" required>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Fee </label>
                                <div class="col-md-6"><input type="text" class="form-control txtinput_1 pro_license_fee" name="pro_license_fee" id="pro_license_fee" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="status" required>
                                        <option>Active</option>
                                        <option>Inative</option>
                                        <option>Hold</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Copy</label>
                                <div class="col-md-6"><input type="file" name="pro_license_copy" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Website</label>
                                <div class="col-md-6"><input type="text" name="pro_license_website" class="form-control" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Note</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="pro_license_note"/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/adminworkstatus">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal Second End-->


        <!-- Modal Third Start-->
        <div class="modal fade" id="myModals33" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Professional License</h4>
                    </div>
                    <div class="modal-body">

                        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('adminworkstatus.updatingeprolicense')}}">
                            {{csrf_field()}}


                            <input type="hidden" name="id" id="idlicids">
                            <input type="hidden" name="admin_lic_id" id="admin_lic_ids">
                            <input type="hidden" name="admin_id" value="1">

                            <input type="hidden" name="profession71" id="profession71">
                            <input type="hidden" name="professiontype71" id="professiontype71">
                            <input type="hidden" name="profession_state71" id="profession_state71">
                            <input type="hidden" name="profession_license71" id="profession_license71">


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Year</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="pro_license_year" id="pro_license_year" required>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Fee </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control txtinput_1 pro_license_fee" name="pro_license_fee" id="pro_license_fee2" required/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status" required>
                                        <option>Active</option>
                                        <option>Inative</option>
                                        <option>Hold</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">License Copy</label>

                                <div class="col-md-6">
                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" type="file" name="pro_license_copy"/>
                                        Browse for file ... </label>

                                    <input type="hidden" name="pro_license_copy_2" id="pro_license_copy"/><span id="pro_license_copy_2"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Website</label>
                                <div class="col-md-6"><input type="text" name="pro_license_website" id="pro_license_website" class="form-control" required/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right">Note</label>
                                <div class="col-md-6"><input type="text" class="form-control" name="pro_license_note" id="pro_license_note"/></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/adminworkstatus">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal Third End-->


        <!--Modal Formation Start-->

        <form method="post" class="form-horizontal" enctype="multipart/form-data" id="myform" action="{{route('adminworkstatus.update')}}">
            {{csrf_field()}}
            <div id="formationModals" class="modal fade" role="dialog">

                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span> Corporation Renew Record</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew For : </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="hidden" name="formationid" id="formationid">
                                    <input type="hidden" name="admin_id_formation" id="admin_id_formation">

                                    <select class="form-control" id="formation_yearbox" name="formation_yearbox" onChange="getvalYear();" required>
                                        <option value="">Select</option>
                                        <option value="1">1 Year</option>
                                        <option value="2">2 Year</option>
                                        <option value="3">3 Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew Year : </label>
                                </div>

                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue" required readonly/>
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew Amount : </label>
                                </div>

                                <div class="col-md-5">
                                    <input class="form-control" type="textbox" readonly name="formation_amount" id="formation_amount" required>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Paid Amount : </label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" type="textbox" name="formation_paid" id="formation_paid" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Method of Payment : </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="formation_payment" id="formation_payment" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label text-right">Status : </label>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="record_status" id="record_status" required>
                                        <option value="0">Select</option>
                                        <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                        <option value="Active Compliance">Active Compliance</option>
                                        <option value="Active Noncompliance">Active Noncompliance</option>
                                        <option value="Admin. Dissolved">Admin. Dissolved</option>
                                        <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label text-right">Annually Receipt : </label>
                                </div>
                                <div class="col-md-8">

                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" name="annualreceipt"/>
                                        Browse for file ... </label>
                                    <input type="hidden" name="annualreceipt_1" id="annualreceipt"/><span id="annualreceipt_1"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label text-right">Officer : </label>
                                </div>
                                <div class="col-md-8">

                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" name="formation_work_officer"/>
                                        Browse for file ... </label>
                                    <input type="hidden" class="form-control" name="officers" id="formation_work_officer"><span id="officers"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Website : </label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" type="tex" name="formation_website" id="formation_website" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-2" style=" padding-right:3px;">

                                    <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                </div>
                                <div class="col-md-2" style="padding-left:3px;">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--Modal Formation  End-->


        <!-- Modal Update Share Ledger Start -->
        <div id="shareledgerModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Modal Header</h4>
                    </div>
                    <form method="post" class="form-horizontal" enctype="multipart/form-data" id="myform" action="{{route('adminworkstatus.updateshareledger')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="ledger_id" id="ledid">
                        <input type="hidden" name="admin_ledid" id="admin_ledid">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Name of Certificate of Holder<span class="starred">*</span></label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="holder_name" id="holder_name" placeholder="Holder Name" required/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Certificates issued No. Shares<span class="starred">*</span></label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="issue_certificate" id="issue_certificate" placeholder="Certificates Issue No of Shares" required/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">From whom Transferred (If Original issue Enter as such)</label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="origine_issue_from" id="origine_issue_from" placeholder="From Transfer"/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Amount paid thereon</label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="paid_amount" id="paid_amount" placeholder="Paid Amount"/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Date of Transfer of Shares<span class="starred">*</span></label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control ledgerdate" name="transfer_date" id="transfer_date" placeholder="mm/dd/yyyy" readonly required/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label"> Certificates Surrendered certif. Nos. </label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="surrender_certificate" id="surrender_certificate" placeholder="Surrender Certificate No"/></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 text-right">
                                    <label class="control-label"> Certificates Surrendered No. Shares<span class="starred">*</span> </label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="surrender_certificate_no" id="surrender_certificate_no" placeholder="Surrender Certificate No of Shares" required/></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Update Share Ledger Start -->

        <!--Formation Renew Modal Start-->
        <div id="renewRecord" class="modal fade" role="dialog">
            <div class="modal-dialog">


                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Corporation Renew Record</h4>
                    </div>

                    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile/updateformation">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew For : </label>
                                </div>
                                <div class="col-md-5">

                                    <select class="form-control formation_yearbox" id="formation_yearbox11" name="formation_yearbox" onChange="getvalYear11();">
                                        <option value="">Select</option>
                                        <option value="1">1 Year</option>
                                        <option value="2">2 Year</option>
                                        <option value="3">3 Year</option>
                                    </select>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew Year : </label>
                                </div>
                                <div class="col-md-5">

                                    <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue11" required readonly/>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Renew Amount : </label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" type="textbox" readonly name="formation_amount" id="formation_amount11" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Paid Amount : </label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" type="textbox" name="formation_paid" id="formation_paid" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Method of Payment : </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="formation_payment" id="formation_payment" required/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4"><label class="control-label text-right">Status : </label>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="record_status" required>
                                        <option value="0">Select</option>
                                        <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                        <option value="Active Compliance">Active Compliance</option>
                                        <option value="Active Noncompliance">Active Noncompliance</option>
                                        <option value="Admin. Dissolved">Admin. Dissolved</option>
                                        <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>

                                    </select>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-md-4"><label class="control-label text-right">Annually Receipt : </label>
                                </div>
                                <div class="col-md-8">

                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" name="annualreceipt" id="annualreceipt" required/>
                                        Browse for file ... </label>

                                </div>
                            </div>

                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-md-4"><label class="control-label text-right">Officer : </label>
                                </div>
                                <div class="col-md-8">

                                    <label class="file-upload btn btn-primary">
                                        <input type="file" class="form-control" name="formation_work_officer" id="formation_work_officer" required/>
                                        Browse for file ... </label>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label text-right">Website : </label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" type="tex" name="formation_website" id="formation_website" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-2" style=" padding-right:3px;">
                                    <!--<input class="btn_new_save btn-primary1" type="submit" value="Save" style="disabled:true">!-->
                                    <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                </div>
                                <div class="col-md-2" style="padding-left:3px;">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--Formation Renew Modal End-->


        <!--Modal Formation Start-->
        <div id="myModalUpdaterecord" class="modal fade" role="dialog">
            <div class="modal-dialog">
                Modal content
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span> Income Tax Filing - (1120S) Renew Record</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="{{route('adminworkstatus.updatetaxation')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="recordform">

                                <input type="hidden" name="taxationid" id="taxationid">

                                <div class="row form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <input type="hidden" id="federalids">
                                    <input type="checkbox" id="federal222" name="federal" onclick="federalclick()">
                                    <label for="federal222" class="col-md-4 control-label" style="text-align:center;">Federal</label>

                                    <input type="hidden" id="statesids">
                                    <input type="checkbox" id="states222" name="states" name="states" onclick="stateclick(this.form)">
                                    <label for="states222" class="col-md-4 control-label" style="text-align:center;">State</label>

                                </div>

                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filling Year</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <!-- <select class="form-control federalyear" name="federalyear" id="federalyear">-->
                                        <!--    <option value="">Select</option>-->
                                        <!--    <option value="2019">2019</option>-->
                                        <!--    <option value="2018">2018</option>-->
                                        <!--    <option value="2017">2017 </option>-->

                                        <!--</select>-->
                                        <input type="text" name="federalyear" id="federalyear" class="form-control federalyear" readonly/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <!-- <select class="form-control">-->
                                        <!--    <option value="">Select</option>-->
                                        <!--     <option value="2019">2019</option>-->
                                        <!--    <option value="2018">2018</option>-->
                                        <!--    <option value="2017">2017 </option>-->

                                        <!--</select>-->

                                        <input type="text" name="statesyear" id="statesyear" class="form-control statesyear" readonly/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control taxation" name="federaltax" id="federaltax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control taxation2" name="statestax" id="statestax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Form No.</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="text" readonly class="form-control formno" name="federalformno" id="federalformno"/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="text" class="form-control formno2" name="statesformno" id="statesformno"/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="text" class="form-control duedate" name="federalduedate" id="federalduedate" placeholder="Mar-15-2020" readonly/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="text" class="form-control duedate2" name="statesduedate" id="statesduedate" placeholder="Mar-15-2020" readonly/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control filingmethod" name="federalmethod" id="federalmethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control" name="statesmethod" id="statesmethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="date" class="form-control" name="federaldate" id="federaldate"/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="date" class="form-control" name="statesdate" id="statesdate"/>
                                    </div>
                                </div>
                                <div class="row form-group statusof">
                                    <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control" name="federalstatus" id="federalstatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control" name="statesstatus" id="statesstatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Add File </label>

                                    <div class="col-md-4 federals" style="display:none;">
                                        <label class="file-upload btn btn-primary">
                                            <input type="file" class="form-control" name="federalfile"/>
                                            Browse for file ... </label>
                                        <input type="hidden" name="federalfile_1" id="federalfile"/><span id="federalfile_1"></span>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <label class="file-upload btn btn-primary">
                                            <input type="file" class="form-control" name="statesfile"/>
                                            Browse for file ... </label>
                                        <input type="hidden" name="statesfile_1" id="statesfile"/><span id="statesfile_1"></span>
                                    </div>
                                </div>
                                <div class="row form-group ">
                                    <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <textarea class="form-control" name="federalnote" id="federalnote"></textarea>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <textarea class="form-control" name="statesnote" id="statesnote"></textarea>
                                    </div>
                                </div>
                                <div class="row form-group saves" style="display:none;">
                                    <label class="col-md-5 control-label">&nbsp;</label>
                                    <div class="col-md-2">
                                        <input type="submit" class="btn_new_save primary" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal Formation  End-->


        <script>
            function getvalYear11() {

                var yearbox = $('#formation_yearbox11').val();
                //var yearbox2=$('#formation_yearbox2').val();
                //var yearbox3=$('#formation_yearbox3').val();
                //alert(yearbox2);
                if (yearbox == '') {
                    //$("#formation_yearvalue").val('');
                } else if (yearbox == 1) {
                    $("#formation_yearvalue11").val('2020');
                    $("#formation_amount11").val('$50.00');
                } else if (yearbox == 2) {
                    $("#formation_yearvalue11").val('2020,2021');
                    $("#formation_amount11").val('$100.00');
                } else if (yearbox == 3) {
                    $("#formation_yearvalue11").val('2020,2021,2022');
                    $("#formation_amount11").val('$150.00');
                }

                if (yearbox2 == '') {
                    //$("#formation_yearvalue").val('');
                } else if (yearbox2 == 1) {
                    $("#formation_yearvalue11").val('2020');
                    $("#formation_amount11").val('$50.00');
                } else if (yearbox2 == 2) {
                    $("#formation_yearvalue11").val('2020,2021');
                    $("#formation_amount11").val('$100.00');
                } else if (yearbox2 == 3) {
                    $("#formation_yearvalue11").val('2020,2021,2022');
                    $("#formation_amount11").val('$150.00');
                }

                if (yearbox3 == '') {
                    //$("#formation_yearvalue").val('');
                } else if (yearbox3 == 1) {
                    $("#formation_yearvalue11").val('2020');
                    $("#formation_amount11").val('$50.00');
                } else if (yearbox3 == 2) {
                    $("#formation_yearvalue11").val('2020,2021');
                    $("#formation_amount11").val('$100.00');
                } else if (yearbox3 == 3) {
                    $("#formation_yearvalue11").val('2020,2021,2022');
                    $("#formation_amount11").val('$150.00');
                }
            }


        </script>


        <div id="myModallic" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width:100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="text-align:center;"><span class="pull-left">{{ Auth::user()->company_name}}</span> <span class="col1"></span></h4>
                    </div>
                    <div class="modal-body" style="width:100%;">
                        <div id="viewmodellic"></div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <script>

            $('.openBtnadminformation2').on('click', function () {
                var num = $(this).attr("num"); //alert(num);
                $.get('{!!URL::to('/getadminformation2')!!}?id=' + num, function (data) {
                    if (data == "") {
                        $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                    } else {
                        $.each(data, function (index, subcatobj) {
                            $('#myModallic').modal({show: true});
                            //$('#viewmodellic').html('<embed src="https://www.domyinsurance.com/public/adminupload/" width="100%" height="450px">');
                            $('#viewmodellic').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.formation_work_officer + '" width="100%" height="450px">');
                            $('.col1').html('Articles of Organization');
                        });
                    }

                });

            });

            $('.openBtnadminformation').on('click', function () {
                var num = $(this).attr("num"); //alert(num);
                $.get('{!!URL::to('/getadminformation')!!}?id=' + num, function (data) {
                    if (data == "") {
                        $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                    } else {
                        $.each(data, function (index, subcatobj) {
                            $('#myModallic').modal({show: true});
                            //$('#viewmodellic').html('<embed src="https://www.domyinsurance.com/public/adminupload/" width="100%" height="450px">');
                            $('#viewmodellic').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.annualreceipt + '" width="100%" height="450px">');
                            $('.col1').html('Certificate of Organization');
                        });
                    }

                });

            });

            $('.openBtnadminlic').on('click', function () {
                var num = $(this).attr("num"); //alert(num);
                $.get('{!!URL::to('/getadminlic')!!}?id=' + num, function (data) {
                    if (data == "") {
                        $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                    } else {
                        $.each(data, function (index, subcatobj) {
                            $('#myModallic').modal({show: true});
                            //$('#viewmodellic').html('<embed src="https://www.domyinsurance.com/public/adminupload/" width="100%" height="450px">');
                            $('#viewmodellic').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.license_copy + '" width="100%" height="450px">');
                            $('.col1').html('License');
                        });
                    }

                });

            });


            $(".ledgerID").click(function () {
                var ledid = $(this).attr('data-id');
                //alert(ledid);
                $("#ledid").val(ledid);
                $('#shareledgerModal').modal('show');
                $.get('{!!URL::to('getAdminshareledger')!!}?ledgerid=' + ledid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {

                        //$('#id').val(data.id);
                        $('#admin_ledid').val(data.admin_id);
                        $('#holder_name').val(data.holder_name);
                        $('#issue_certificate').val(data.issue_certificate);
                        $('#origine_issue_from').val(data.origine_issue_from);
                        $('#paid_amount').val(data.paid_amount);
                        $('#transfer_date').val(data.transfer_date);
                        $('#surrender_certificate').val(data.surrender_certificate);
                        $('#surrender_certificate_no').val(data.surrender_certificate_no);


                    }
                });
            });


            $(".passingIDS22").click(function () {
                var prolicid = $(this).attr('data-id');
                //alert(ids);
                $("#id22").val(prolicid);
                $('#myModals22').modal('show');
                //console.log(prolicid);
                $.get('{!!URL::to('getAdminprolicensedata')!!}?prolicenseid=' + prolicid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {

                        // $('#id').val(data.id);
                        // $('#admin_lic_ids').val(data.admin_lic_id);
                        // $('#admin_id13').val(data.admin_id);
                        // $('#pro_license_year').val(data.pro_license_year);
                        // $('#pro_license_fee2').val(data.pro_license_fee);
                        // $('#status').val(data.status);
                        // $('#pro_license_copy').val(data.pro_license_copy);
                        // $('#pro_license_copy_2').html(data.pro_license_copy);
                        // $('#pro_license_website').val(data.pro_license_website);
                        // $('#pro_license_note').val(data.pro_license_note);

                        $('#profession72').val(data.profession);
                        $('#professiontype72').val(data.professiontype);
                        $('#profession_state72').val(data.profession_state);
                        $('#profession_license72').val(data.profession_license);
                    }


                });
            });


            $(".passingTaxation").click(function () {
                var taxationid = $(this).attr('data-id');
                $("#taxationid").val(taxationid);
                $('#myModalUpdaterecord').modal('show');
                $.get('{!!URL::to('getAdmintaxtiondata')!!}?taxationid=' + taxationid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {

                        $('#id').val(data.id);
                        $('#federalids').val(data.federal);
                        $('#statesids').val(data.states);
                        $('#federalyear').val(data.federalyear);
                        $('#federaltax').val(data.federaltax);
                        $('#federalformno').val(data.federalformno);
                        $('#federalduedate').val(data.federalduedate);
                        $('#federalmethod').val(data.federalmethod);
                        $('#federaldate').val(data.federaldate);
                        $('#federalstatus').val(data.federalstatus);
                        $('#federalfile').val(data.federalfile);
                        $('#federalfile_1').html(data.federalfile);
                        $('#federalnote').val(data.federalnote);

                        $('#statesyear').val(data.statesyear);
                        $('#statestax').val(data.statestax);
                        $('#statesformno').val(data.statesformno);
                        $('#statesduedate').val(data.statesduedate);
                        $('#statesmethod').val(data.statesmethod);
                        $('#statesdate').val(data.statesdate);
                        $('#statesstatus').val(data.statesstatus);
                        $('#statesfile').val(data.statesfile);
                        $('#statesfile_1').html(data.statesfile);
                        $('#statesnote').val(data.statesnote);

                        $(document).ready(function () {
                            var fedid = $('#federalids').val();
                            if (fedid == 1) {
                                $('.labels').show();
                                $('.saves').show();
                                $('.federals').show();

                            } else {

                                $('.federals').hide();

                            }


                            var staid = $('#statesids').val();
                            if (staid == 1) {
                                $('.labels').show();
                                $('.saves').show();
                                $('.states').show();

                            } else {

                                $('.states').hide();

                            }
                        });

                    }


                });
            });


            $(".passingFormation").click(function () {
                var formationid = $(this).attr('data-id');
                //alert(formationid);
                $("#formationid").val(formationid);
                $('#formationModals').modal('show');
                //console.log(ids);
                $.get('{!!URL::to('getAdminformationdata')!!}?formationid=' + formationid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {

                        $('#id').val(data.id);
                        $('#admin_id_formation').val(data.admin_id);
                        $('#formation_yearbox').val(data.formation_yearbox);
                        $('#formation_yearvalue').val(data.formation_yearvalue);
                        $('#formation_amount').val(data.formation_amount);
                        $('#formation_paid').val(data.formation_paid);
                        $('#formation_payment').val(data.formation_payment);
                        $('#record_status').val(data.record_status);
                        $('#annualreceipt').val(data.annualreceipt);
                        $('#annualreceipt_1').html(data.annualreceipt);
                        $('#formation_work_officer').val(data.formation_work_officer);
                        $('#officers').html(data.formation_work_officer);
                        $('#formation_website').val(data.formation_website);

                    }


                });
            });


            $(".passingIDS").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#idlic").val(ids);
                $('#myModals').modal('show');
                //console.log(ids);
                $.get('{!!URL::to('getAdminlicensedata')!!}?licenseid=' + ids, function (data) {
                    console.log(data);
                    if (data == "") {

                    } else {

                        $('#id').val(data.id);
                        $('#admin_id12').val(data.admin_id);
                        $('#license_year').val(data.license_year);
                        $('#license_gross1').val(data.license_gross);
                        $('#license_fee1').val(data.license_fee);
                        $('#license_status').val(data.license_status);
                        $('#license_copy').val(data.license_copy);
                        $('#license_copy2').html(data.license_copy);
                        $('#license_no').val(data.license_no);
                        $('#license_renew_date').val(data.license_renew_date);
                        $('#license_note').val(data.license_note);

                    }


                });
            });

            $(".IncometaxIDS").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#idlic").val(ids);
                $('#myModals').modal('show');
                //console.log(ids);
                $.get('{!!URL::to('getAdminlicensedata')!!}?licenseid=' + ids, function (data) {
                    console.log(data);
                    if (data == "") {

                    } else {

                        $('#id').val(data.id);
                        $('#admin_id12').val(data.admin_id);
                        $('#license_year').val(data.license_year);
                        $('#license_gross1').val(data.license_gross);
                        $('#license_fee1').val(data.license_fee);
                        $('#license_status').val(data.license_status);
                        $('#license_copy').val(data.license_copy);
                        $('#license_copy2').html(data.license_copy);
                        $('#license_no').val(data.license_no);
                        $('#license_renew_date').val(data.license_renew_date);
                        $('#license_note').val(data.license_note);

                    }


                });
            });

            $(".passingIDS33").click(function () {
                var idss = $(this).attr('data-id');
                //alert(idss);
                $("#idlicids").val(idss);
                $('#myModals33').modal('show');
                //console.log(idss);
                $.get('{!!URL::to('getAdminprolicensedata')!!}?prolicenseid=' + idss, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {

                        $('#id').val(data.id);
                        $('#admin_lic_ids').val(data.admin_lic_id);
                        $('#admin_id13').val(data.admin_id);
                        $('#pro_license_year').val(data.pro_license_year);
                        $('#pro_license_fee2').val(data.pro_license_fee);
                        $('#status').val(data.status);
                        $('#pro_license_copy').val(data.pro_license_copy);
                        $('#pro_license_copy_2').html(data.pro_license_copy);
                        $('#pro_license_website').val(data.pro_license_website);
                        $('#pro_license_note').val(data.pro_license_note);

                        $('#profession71').val(data.profession);
                        $('#professiontype71').val(data.professiontype);
                        $('#profession_state71').val(data.profession_state);
                        $('#profession_license71').val(data.profession_license);

                    }


                });
            });
        </script>


        <script type="text/javascript">


            $(document).ready(function () {

                $(".ledgerdate").datepicker({
                    autoclose: true,
                    format: "mm/dd/yyyy",
                });

                $('.fillingdate').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                });


                $('.js-example-tags').select2({
                    tags: true,
                    tokenSeparators: [",", " "]
                });


                $(".license_gross").on("input", function (evt) {
                    var self = $(this);
                    self.val(self.val().replace(/[^\d].+/, ""));
                    if ((evt.which < 48 || evt.which > 57)) {
                        evt.preventDefault();
                    }
                });

                $(".license_fee").on("input", function (evt) {
                    var self = $(this);
                    self.val(self.val().replace(/[^\d].+/, ""));
                    if ((evt.which < 48 || evt.which > 57)) {
                        evt.preventDefault();
                    }
                });

                $(".pro_license_fee").on("input", function (evt) {
                    var self = $(this);
                    self.val(self.val().replace(/[^\d].+/, ""));
                    if ((evt.which < 48 || evt.which > 57)) {
                        evt.preventDefault();
                    }
                });


                (function ($) {
                    var minNumber = -100;
                    var maxNumber = 100;
                    $('.spinner .btn:first-of-type').on('click', function () {
                        if ($('.spinner input').val() == maxNumber) {
                            return false;
                        } else {
                            $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
                        }
                    });

                    $('.txtinput_1').on("blur", function () {
                        var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                        if (minNumber > inputVal) {
                            //inputVal = -100;
                        } else if (maxNumber < inputVal) {
                            //inputVal = 100;
                        }
                        $(this).val(inputVal + '.00');
                    });

                    $('.spinner .btn:last-of-type').on('click', function () {
                        if ($('.spinner input').val() == minNumber) {
                            return false;
                        } else {
                            $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
                        }
                    });
                })(jQuery);

            });

        </script>


        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function () {

                $('#myform').validate({ // initialize the plugin
                    rules: {
                        formation_yearbox: {
                            required: true,
                        },
                    },
                    messages:
                        {
                            formation_yearbox: {
                                required: 'Please select type a year ',
                            },
                        }
                });

            });

        </script>

        <script>
            function getvalYear() {
                var yearboxval = document.getElementById("formation_yearbox").value;
                var yearvaluesub = document.getElementById("formation_yearvalue").value;
                // alert(yearboxval);
                if (yearboxval == '') {
                    $("#formation_yearvalue").val('');
                } else if (yearboxval == 1) {
                    $("#formation_yearvalue").val('2020');
                    $("#formation_amount").val('$50.00');
                } else if (yearboxval == 2) {
                    $("#formation_yearvalue").val('2020,2021');
                    $("#formation_amount").val('$100.00');
                } else if (yearboxval == 3) {
                    $("#formation_yearvalue").val('2020,2021,2022');
                    $("#formation_amount").val('$150.00');
                }
            }


        </script>

        <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Document Name</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="ajax">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Document Name"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="addopt" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background:#038ee0;">
                        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Documents
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h4>
                    </div>


                    <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                        <div class="curency curency_ref" id="div">
                            @foreach($document as $cur)
                                <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                                    <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                        <a class="delete" style="color:#000;" onclick="return confirm('Are you sure to remove this record?')" href="{{route('adminworkstatus.destroydocumenttitle',$cur->id)}}" id="{{$cur->id}}">{{$cur->documentname}}
                                            <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div id="myModalAddrecord" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="pull-left">{{ Auth::user()->company_name}}</span>Income Tax Filing - (1120S) Add Record</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="{{route('adminworkstatus.storefederal')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="recordform">


                                <div class="row form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <input type="checkbox" id="federal" name="federal" name="federal" value="1" onclick="federalclick()">
                                    <label for="federal" class="col-md-4 control-label" style="text-align:center;">Federal</label>

                                    <input type="checkbox" id="states" name="states" name="states" value="1" onclick="stateclick(this.form)">

                                    <label for="states" class="col-md-4 control-label" style="text-align:center;">State</label>

                                </div>

                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filling Year</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control federalyear" name="federalyear">
                                            <option value="">Select</option>
                                            <?php
                                            $aa = date('Y');
                                            $bb = date('Y') + 1;
                                            $cc = date('Y') + 2;
                                            ?>
                                            <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                            <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                            <option value="<?php echo $cc;?>"><?php echo $cc;?></option>

                                        </select>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control statesyear" name="statesyear">
                                            <option value="">Select</option>
                                            <?php
                                            $aa = date('Y');
                                            $bb = date('Y') + 1;
                                            $cc = date('Y') + 2;
                                            ?>
                                            <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                            <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                            <option value="<?php echo $cc;?>"><?php echo $cc;?></option>

                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control taxation" name="federaltax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control taxation2" name="statestax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Form No.</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="text" readonly class="form-control formno" name="federalformno"/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="text" class="form-control formno2" name="statesformno" readonly/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="text" class="form-control duedate" name="federalduedate" placeholder="Mar-15-2020" readonly/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control filingmethod" name="federalmethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control filingmethod2" name="statesmethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="date" class="form-control" id="fillingdate" name="federaldate"/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                                    </div>
                                </div>
                                <div class="row form-group statusof">
                                    <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <select class="form-control" name="federalstatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <select class="form-control" name="statesstatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <input type="file" class="form-control" name="federalfile"/>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <input type="file" class="form-control" name="statesfile"/>
                                    </div>
                                </div>
                                <div class="row form-group ">
                                    <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                                    <div class="col-md-4 federals" style="display:none;">
                                        <textarea class="form-control" name="federalnote"></textarea>
                                    </div>

                                    <div class="col-md-4 states" style="display:none;">
                                        <textarea class="form-control" name="statesnote"></textarea>
                                    </div>
                                </div>
                                <div class="row form-group saves" style="display:none;">
                                    <label class="col-md-5 control-label">&nbsp;</label>
                                    <div class="col-md-2">
                                        <input type="submit" class="btn_new_save primary" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <!-- Modal Share Ledger Start -->
        <div id="myModalShareledger" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Modal Header</h4>
                    </div>
                    <form method="post" class="form-horizontal" enctype="multipart/form-data" id="myform" action="{{route('adminworkstatus.addshareledger')}}">
                        {{csrf_field()}}

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Name of Certificate of Holder<span class="starred">*</span></label>
                                </div>
                                <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                <div class="col-md-6"><input type="text" class="form-control" name="holder_name" placeholder="Holder Name" required/></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Certificates issued No. Shares<span class="starred">*</span></label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="issue_certificate" placeholder="Certificates Issue No of Shares" required/></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">From whom Transferred (If Original issue Enter as such)</label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="origine_issue_from" placeholder="From Transfer"/></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Amount paid thereon</label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="paid_amount" placeholder="Paid Amount"/></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label">Date of Transfer of Shares<span class="starred">*</span></label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control ledgerdate" name="transfer_date" placeholder="mm/dd/yyyy"/ readonly required></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label"> Certificates Surrendered certif. Nos. </label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="surrender_certificate" placeholder="Surrender Certificate No"/></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label class="control-label"> Certificates Surrendered No. Shares<span class="starred">*</span> </label>
                                </div>
                                <div class="col-md-6"><input type="text" class="form-control" name="surrender_certificate_no" placeholder="Surrender Certificate No of Shares" required/></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 control-label text-right"></label>
                                <div class="col-md-3">
                                    <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                                </div>
                                <div class="col-md-3">
                                    <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Share Ledger Start -->


        <script>


            $('.btn-toggle').click(function () {
                $(this).find('.btn').toggleClass('active');
                /*if ($(this).find('.btn-primary').length>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').length>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').length>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').length>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }*/
                $(this).find('.btn').toggleClass('btn-default');
            });
            /*$('form').submit(function(){
  var radioValue = $("input[name='options']:checked").val();
  if(radioValue){
     alert("You selected - " + radioValue);
   };
    return false;
});
*/
            $(document).ready(function () {
                var flag = $('#flag').val();
                var flag2 = $('#flag2').val();
                // var flag = $('#flag3').val();
                var cnt = 1;
                $(".addCFnew").click(function () {
                    //alert('');
                    cnt++;
                    //  alert(cnt);
                    $("#customFieldsbo").append('<div class="customfieldsbox"> <div class="row form-group"> <div class="col-md-3"><label class="control-label">Bank Name</label> <input type="text" class="form-control"/></div> <div class="col-md-3"><label class="control-label">Account Nick Name</label> <input type="text" class="form-control"/> </div> <div class="col-md-3" style="width:20%"><label class="control-label">Last Four digit of A/C #</label> <input type="text" class="form-control fourdigit"/> </div> <div class="col-md-2" style="width:20%"> <label class="control-label">Statement</label><select class="form-control" name="statement[]"><option value="">Select</option><option value="E-Statement">E-Statement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></select><div class="col-md-1"></div> </div> </div> <div class="row form-group"> <div class="col-md-3"> <label class="control-label">Check Stubs</label><select class="form-control" name="counter[]"><option value="">Select</option><option value="Statement With Check Image">statement With Check Image</option><option value="Counter Check">Counter Check</option></select> </div> <div class="col-md-3"> <label class="control-label">Open Date</label><input type="text" class="form-control opendate"/> </div> <div class="col-md-3" style="width:20%"><label class="control-label">Status</label> <select class="form-control showns' + cnt + '"><option value="">Select</option><option value="Active">Active</option><option value="Close">Close</option></select> </div> <div class="col-md-3 show' + cnt + '" style="width:20%;display:none;"> <label class="control-label">Close Date</label> <input type="text" class="form-control closedate"/> </div> <div class="col-md-1"><a class="btn btn-danger mt30 remCFnew">Remove</a> </div></div> ');


                    $('.showns' + cnt).on('change', function () {
                        //alert();
                        var thisss = $(this).val();
                        //alert(thisss);
                        if (thisss == 'Close') {
                            $('.show' + cnt).show();
                        } else {
                            $('.show' + cnt).hide();
                        }
                    });

                    $(".opendate").datepicker({
                        autoclose: true,
                        format: "mm/dd/yyyy",
                    });
                    $(".fourdigit").mask("9999");


                    $(".closedate").datepicker({
                        autoclose: true,
                        format: "mm/dd/yyyy",
                    });

                });


                $("#customFieldsbo").on('click', '.remCFnew', function () {
                    //  flag--;
                    //alert();
                    $(this).parent().parent().parent().remove();
                });


                $(".addCFnewccd").click(function () {
                    //alert('');
                    $("#customFieldsboccard").append('<div class="customfieldsbox"> <div class="row form-group"> <div class="col-md-3"><label class="control-label">Credit Card Name</label> <input type="text" class="form-control"/></div> <div class="col-md-3"><label class="control-label">Account Nick Name</label> <input type="text" class="form-control"/> </div> <div class="col-md-3" style="width:20%"><label class="control-label">Last Four digit of A/C #</label> <input type="text" class="form-control fourdigit"/> </div> <div class="col-md-2" style="width:20%"> <label class="control-label">Statement</label> <select class="form-control" name="statement[]"><option value="">Select</option><option value="E-Statement">E-Statement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></select><div class="col-md-1"></div> </div> </div> <div class="row form-group"> <div class="col-md-3"> <label class="control-label">Note</label><input type="text" class="form-control"/> </div> <div class="col-md-3"> <label class="control-label">Open Date</label><input type="text" class="form-control"/> </div> <div class="col-md-3" style="width:20%"><label class="control-label">Status</label> <select class="form-control"><option value="">Select</option><option value="Active">Active</option><option value="Close">Close</option></select> </div> <div class="col-md-3" style="width:20%;display:none;"> <label class="control-label">Close Date</label> <input type="text" class="form-control"/> </div> <div class="col-md-1"><a class="btn btn-danger mt30 remCFccdnew">Remove</a> </div></div> ');
                });


                $("#customFieldsboccard").on('click', '.remCFccdnew', function () {
                    //  flag--;
                    //alert();
                    $(this).parent().parent().parent().remove();
                });


                $(".addCF").click(function () {
                    flag++;
                    $("#customFields").append('<tr valign="top"><td>' + flag + '</td><td><input type="hidden" class="form-control" name="flag[]" placeholder="Enter Bank Name"/><input type="text" class="form-control" name="bankname[]" placeholder="Enter Bank Name"/></td><td><input type="text" class="form-control" name="accountname[]" placeholder="Enter Account Nick Name"/></td><td><input type="text" name="lastfourname[]" class="form-control fourdigit" placeholder="Enter Last Four digit of A/C #"/></td><td><Select class="form-control" name="statement[]"><option value="">Select</option><option value="E-Staement">E-Staement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></Select></td> <td><Select class="form-control" name="counter[]"><option>Select</option><option>Statement With Check image</option> <option>Counter Check</option> </Select></td><td> <a href="javascript:void(0);" class="remCF btn btn-danger">Remove</a></td></tr>');

                    $(".fourdigit").mask("9999");

                });
                $(".addCF2").click(function () {
                    flag2++;
                    $("#customFields2").append('<tr valign="top"><td>' + flag2 + '</td><td><input type="hidden" class="form-control" name="flag2[]" placeholder="Enter Bank Name"/><input type="text" class="form-control" placeholder="Enter Credit Card Name" name="business_credit_card_number[]"/></td><td><input type="text" name="business_credit_card_digit_number[]" class="form-control" placeholder="Enter Last Four Digit Number"/></td><td><input type="text" name="business_credit_enddate[]" class="form-control effective_date1" placeholder="Enter Statement End Date"/></td><td> <a href="javascript:void(0);" class="remCF2 btn btn-danger">Remove</a></td></tr>');
                    $(".effective_date1").datepicker({
                        autoclose: true,
                        format: "mm/dd/yyyy",
                    });
                });

                $("#customFields").on('click', '.remCF', function () {
                    flag--;
                    $(this).parent().parent().remove();
                });
                $("#customFields2").on('click', '.remCF2', function () {
                    flag2--;
                    $(this).parent().parent().remove();
                });
            });
        </script>
        <script>
            /*$('#Select1').on('change', function()
{
    var thisss=$(this).val();

  if(thisss == 'NewCreated');
  {
    $("#saleofbus").show();
    $("#trfbus").hide();
  }
  else if(thisss == 'Purchase')
      {
          $("#trfbus").show();
            $("#saleofbus").hide();
      }


});
*/
        </script>

        <script>
            $(document).ready(function () {


                $(".opendate").datepicker({
                    autoclose: true,
                    format: "mm/dd/yyyy",
                });

                $(".closedate").datepicker({
                    autoclose: true,
                    format: "mm/dd/yyyy",
                });

                $('.openBtn').on('click', function () {
//console.log('htm');
                    var num = $(this).attr("num"); //alert(num);
//alert(num);
                    $.get('{!!URL::to('getimage')!!}?id=' + num, function (data) {// alert();
                        if (data == "") {
                            if (num === 'SOS Certificate') {
                                $('.col2').html('Certificate of Corporation');
                            } else if (num === 'SOS-AOI') {
                                $('.col2').html('Articles of Incorporation');
                            } else {
                                $('.col2').html(num);
                            }
                            $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                        } else {
                            $.each(data, function (index, subcatobj) { //alert();
                                if (num === subcatobj.upload_name) {
                                    if (subcatobj.upload_name === 'SOS Certificate') {
                                        $('.col1').html('Certificate of Corporation');
                                    } else if (subcatobj.upload_name === 'SOS-AOI') {
                                        $('.col1').html('Articles of Incorporation');
                                    } else {
                                        $('.col1').html(num);
                                    }
                                    $('#myModal8').modal({show: true});
                                    $('#viewmodel').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.upload + '" width="100%" height="300px">');
                                } else {
//alert();
                                }
                            });
                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function () {
                $('.openBtn1').on('click', function () {
//console.log('htm');
                    var num = $(this).attr("num"); //alert(num);
//alert(num);
                    $.get('{!!URL::to('history1')!!}?id=' + num, function (data) {// alert();
                        if (data == "") {
                            if (num === 'SOS Certificate') {
                                $('.col2').html('Certificate of Corporation');
                            } else if (num === 'SOS-AOI') {
                                $('.col2').html('Articles of Incorporation');
                            } else {
                                $('.col2').html(num);
                            }
                            $('#myModal2').modal({show: true}); // alert('File Not Uploaded');
                        } else {
                            $.each(data, function (index, subcatobj) { //alert();
                                if (num === subcatobj.upload_name) {
                                    if (subcatobj.upload_name === 'SOS Certificate') {
                                        $('.col1').html('Certificate of Corporation');
                                    } else if (subcatobj.upload_name === 'SOS-AOI') {
                                        $('.col1').html('Articles of Incorporation');
                                    } else {
                                        $('.col1').html('License');
                                    }
                                    $('#myModal-history').modal({show: true});
                                    $('#viewmodel-1').html('<embed src="https://financialservicecenter.net/public/adminupload/' + subcatobj.upload + '" width="100%" height="300px">');
                                } else {
//alert();
                                }
                            });
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#subscription_access").click(function () {
                    if ($(this).is(":checked")) {
                        $(".subscription_hidden2").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".subscription_hidden2").hide();
                        // $("#AddPassport").show();
                    }
                });

                $("#limit_access").click(function () {
                    if ($(this).is(":checked")) {
                        $(".limit_hidden").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".limit_hidden").hide();
                        // $("#AddPassport").show();
                    }
                });

                $("#user_access").click(function () {
                    if ($(this).is(":checked")) {
                        $(".user_hidden").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".user_hidden").hide();
                        // $("#AddPassport").show();
                    }
                });


                $('#businessone').on('change', function () {

                    if ($(this).val() == 'NewCreated') {
                        // alert('hide');
                        $('.purchage_business ').hide();

                    } else if ($(this).val() == 'Purchase') {
                        // alert('show');
                        $('.purchage_business ').show();
                    } else {
                        $('.purchage_business ').hide();
                    }
                });

                $('#salesbusiness').on('change', function () {
                    if ($(this).val() == 'Sale') {
                        // alert('hide');
                        $('.sale_business ').show();
                        $('.transfer_business ').hide();

                    } else if ($(this).val() == 'Transfer') {
                        // alert('show');
                        $('.transfer_business ').show();
                        $('.sale_business ').hide();
                    } else {
                        $('.transfer_business ').hide();
                        $('.sale_business ').hide();
                    }
                });


                $("#button").click(function () {
                    location.reload(true);
                });
            });

            function showbanknamevid() {


                $('#btnno').removeClass('btn-danger');
                $('#banknamediv').show();
                $('#uparrow').show();
                // $('#dwnarrow').show();

                $('#banktype').val('1');
            }

            function showbanknamevid02() {
                $('#banknamediv').show();
                $('#uparrow').show();
                $('#dwnarrow').hide();

                // $('#banktype').val('1');
            }

            function hidebanknamevid() {
                $('#btnyes').removeClass('btn-success');
                $('#btnno').addClass('btn-danger');
                $('#banknamediv').hide();
                $('#uparrow').hide();
                $('#dwnarrow').hide();
                $('#banktype').val('2');
            }

            function hidebanknamevid02() {
                $('#banknamediv').hide();
                $('#uparrow').hide();
                $('#dwnarrow').show();
                // $('#banktype').val('2');
            }

            function showcreditcardnamevid() {
                $('#creditcardnamediv').show();
                $('#uparrow2').show();
                $('#dwnarrow2').hide();
                $('#banktype1').val('1');
            }

            function showcreditcardnamevid02() {
                $('#creditcardnamediv').hide();
                $('#uparrow2').hide();
                $('#dwnarrow2').show();
            }

            function showcreditcardnamevid1() {
                $('#creditcardnamediv1').show();
                $('#businessloan').val('1');
                $('#uparrow3').show();
                $('#dwnarrow3').hide();
            }

            function showcreditcardnamevid102() {
                $('#creditcardnamediv1').hide();
                $('#uparrow3').hide();
                $('#dwnarrow3').show();
                $('#businessloan').val('1');
            }


            function showcreditcardnamevid2() {
                $('#creditcardnamediv1').hide();
                $('#uparrow3').hide();
                $('#dwnarrow3').hide();
                $('#businessloan').val('2');
            }

            function hidecreditcardnamevid102() {
                $('#creditcardnamediv1').show();
                $('#uparrow3').show();
                $('#dwnarrow3').hide();
                $('#businessloan').val('2');
            }

            function hidecreditcardnamevid() {
                $('#creditcardnamediv').hide();
                $('#banktype1').val('2');
            }


            function hidecreditcardnamevid02() {
                $('#creditcardnamediv').hide();
                $('#uparrow2').hide();
                $('#dwnarrow2').show();
                //$('#banktype1').val('2');
            }


            $('#businessone').on('change', function () {

                //alert();
                if ($(this).value == 'NewCreated') {
                    alert('NewCreated');
                    $('.purchage_business').hide();
                } else if ($(this).value == 'Purchase') {
                    alert('purchase');
                    $('.purchage_business').show();
                }
            });

            $('input[type=radio][name=saleofbusiness]').change(function () {
                if (this.value == 'Transfer of Business') {
                    $('.sale_business').hide();
                    $('.transfer_business').show();
                } else if (this.value == 'Sale of Business') {
                    $('.sale_business').show();
                    $('.transfer_business').hide();
                }

            });
        </script>


        <script>

            function federalclick() {
                var checkBox = document.getElementById("federal");
                if (checkBox.checked == true) {
                    $('.labels').show();
                    $('.saves').show();
                    $('.federals').show();

                } else {

                    $('.federals').hide();

                }
            }


            function stateclick() {
                var checkBox1 = document.getElementById("states");
                if (checkBox1.checked == true) {
                    $('.labels').show();
                    $('.saves').show();
                    $('.states').show();
                } else {

                    $('.states').hide();
                }
            }


            $(document).ready(function () {

                $('.federalyear').change(function () {

                    var this1 = $(this).val();
                    //alert(this1);
                    var aa = '<?php echo $aa = date('Y');?>';
                    var bb = '<?php echo $bb = date('Y') + 1;?>';
                    var cc = '<?php echo $cc = date('Y') + 2;?>';
                    if (this1 == aa) {
                        $("select option[value='Extension']").hide();
                        $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');

                    } else if (this1 == bb) {
                        $("select option[value='Extension']").hide();
                        $(".duedate").val('Mar-15-<?php echo $bb + 1;?>');
                    } else if (this1 == cc) {
                        $("select option[value='Extension']").show();
                        $(".duedate").val('Mar-15-<?php echo $cc + 1;?>');
                    }

                });


                $('.statesyear').change(function () {

                    var this1 = $(this).val();
                    //alert(this1);
                    var aa = '<?php echo $aa = date('Y');?>';
                    var bb = '<?php echo $bb = date('Y') + 1;?>';
                    var cc = '<?php echo $cc = date('Y') + 2;?>';
                    if (this1 == aa) {
                        $("select option[value='Extension']").hide();
                        $(".duedate2").val('Mar-15-<?php echo $aa + 1;?>');

                    } else if (this1 == bb) {
                        $("select option[value='Extension']").hide();
                        $(".duedate2").val('Mar-15-<?php echo $bb + 1;?>');
                    } else if (this1 == cc) {
                        $("select option[value='Extension']").show();
                        $(".duedate2").val('Mar-15-<?php echo $cc + 1;?>');
                    }

                });


                $('.taxation').change(function () {
                    var thiss = $(this).val();
                    if (thiss == 'Extension') {
                        $('.formno').val('Form-7004');
                        $("select option[value='E-File']").show();
                    } else if (thiss == 'Original') {
                        $('.formno').val('Form-1120S');
                        $("select option[value='E-File']").show();
                    } else if (thiss == 'Amendment') {
                        $('.formno').val('Form-1120S-X');
                        $("select option[value='E-File']").hide();

                    }
                });

                $('.taxation2').change(function () {
                    var thiss = $(this).val();
                    if (thiss == 'Extension') {
                        $('.formno2').val('Form-7004');
                        $("select option[value='E-File']").show();
                    } else if (thiss == 'Original') {
                        $('.formno2').val('Form-1120S');
                        $("select option[value='E-File']").show();
                    } else if (thiss == 'Amendment') {
                        $('.formno2').val('Form-1120S-X');
                        $("select option[value='E-File']").hide();

                    }
                });


                $('.filingmethod').change(function () {
                    var thiss = $(this).val();
                    if (thiss == 'Paperfile') {
                        $('.statusof').hide();
                    } else {
                        $('.statusof').show();
                    }
                });

                $('.filingmethod2').change(function () {
                    var thiss = $(this).val();
                    if (thiss == 'Paperfile') {
                        $('.statusof2').hide();
                    } else {
                        $('.statusof2').show();
                    }
                });


                $('.submaintab li a').click(function () {
                    //alert();
                    $('.submaintab li').removeClass('active');
                    $('.submaintab li a').removeClass('active');
                    $(this).addClass('active');
                })


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $(function () {
                    $('#addopt').click(function () { //alert();
                        var newopt = $('#newopt').val();
                        if (newopt == '') {
                            alert('Please enter something!');
                            return;
                        }

                        //check if the option value is already in the select box
                        $('#vendor_product option').each(function (index) {
                            if ($(this).val() == newopt) {
                                alert('Duplicate option, Please enter new!');
                            }
                        })
                        $.ajax({
                            type: "post",
                            url: "{!!route('admin.documents')!!}",
                            dataType: "json",
                            data: $('#ajax').serialize(),
                            success: function (data) {
                                alert('Successfully Added');
                                $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                                $("#div").load(" #div > *");
                                $("#newopt").val('');
                            },
                            error: function (data) {
                                alert("Error")
                            }
                        });

                        $('#basicExampleModal').modal('hide');
                    });
                });


                $('#country_name').keyup(function () {
                    var query = $(this).val();
                    if (query != '') {
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url: "{{ route('workstatus.fetch') }}",
                            method: "POST",
                            data: {query: query, _token: _token},
                            success: function (data) {
                                $('#countryList').fadeIn();
                                $('#countryList').html(data);
                            }
                        });
                    }
                });

                $(document).on('click', 'tr', function () {
                    $('#country_name').val($(this).text());
                    $('#countryList').fadeOut();
                });

            });


        </script>


@endsection