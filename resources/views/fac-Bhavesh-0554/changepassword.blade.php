@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    @if(Auth::user()->flag >='3')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
        <style>
            .card {
                pointer-events: none;
            }

            .passwordbox {
                margin-top: 10px !important;
            }

            .passwordbox li {
                margin-bottom: 0px !important;
                font-size: 13px !important;
                width: 100%;
            }

            .passwordbox li .fa.fa-times {
                color: red;
            }

        </style>
    @endif
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title">
            <h1>Change Password</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        @if ( session()->has('success') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                        @endif
                        @if ( session()->has('error') )
                            <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                        @endif
                        @if(Auth::user()->flag >='3')
                            <div class="" style="width: 100%;background: #ff0000b3;color: #fff;padding: 10px;">Change Password After 30 Minutes</div>
                        @endif
                        <div class="card col-md-12">
                            <form enctype='multipart/form-data' class="form-horizontal changepassword"
                                  action="{{route('changepassword.update', Auth::user()->id)}}" id="changepassword" method="post">
                                {{csrf_field()}}  {{method_field('PATCH')}}
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label col-lg-5 col-md-4">User Name :</label>
                                                <div class="col-lg-7 col-md-8">
                                                    <input type="text" class="form-control" id="" name="">
                                                    @if ($errors->has('oldpassword'))
                                                        <span class="help-block">
                                      <strong>{{ $errors->first('oldpassword') }}</strong>
                                      </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                                <label class="control-label col-lg-5 col-md-4">Old Password :</label>
                                                <div class=" col-lg-7 col-md-8">
                                                    <input type="hidden" class="form-control" id="" value="{{ Auth::user()->email}}" name="email">

                                                    <input type="password" class="form-control" id="oldpassword" name="oldpassword"> <input type="hidden" class="form-control" id="flag" value="@if(empty(Auth::user()->flag)) 1 @else {{Auth::user()->flag+1}} @endif" name="flag">
                                                    @if ($errors->has('oldpassword'))
                                                        <span class="help-block">
                                      <strong>{{ $errors->first('oldpassword') }}</strong>
                                      </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label col-lg-5 col-md-4">New Password :</label>
                                                <div class=" col-lg-7 col-md-8">
                                                    <!-- <input name="newpassword" type="password" id="newpassword" class="form-control" />
                                                     <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                     <div id="messages"></div>
                                                       <div class="pwstrength_viewport_progress"></div> -->
                                                    <input id="password" type="password" name="password" placeholder="" class="form-control input-md" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" onKeyUp="passwordStrength(this.value)">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-5 col-md-4">Confirm Password :</label>
                                                <div class=" col-lg-7 col-md-8">
                                                    <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12 show_991">
                                                <div id="popover-password">
                                                    <ul class="list-unstyled passwordbox">
                                                        <li class=""><span class="low-upper-case"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; 1 lowercase &amp; 1 uppercase</li>
                                                        <li class=""><span class="one-number"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 number (0-9)</li>
                                                        <li class=""><span class="one-special-char"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 Special Character (!@#$%^&*).</li>
                                                        <li class=""><span class="eight-character"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Atleast 8 Character</li>
                                                        <li class=""><span class="password-match"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Both Passwords are Same</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                                                <label class="control-label col-lg-5 col-md-4">Reset Days :</label>
                                                <div class="col-md-2">
                                                    <select name="resetdays" value="" id="resetdays" class="form-control">
                                                        <option>---Select Reset Days---</option>
                                                        <option value="30" @if(Auth::user()->reset_day=='30') selected @endif>30</option>
                                                        <option value="60" @if(Auth::user()->reset_day=='60') selected @endif>60</option>
                                                        <option value="90" @if(Auth::user()->reset_day=='90') selected @endif>90</option>
                                                        <option value="120" @if(Auth::user()->reset_day=='120') selected @endif>120</option>
                                                    </select>
                                                    @if ($errors->has('resetdays'))
                                                        <span class="help-block">
                                      <strong>{{ $errors->first('resetdays') }}</strong>
                                      </span>
                                                    @endif
                                                </div>
                                                <label class="control-label col-lg-2 col-md-3">Reset Date :</label>
                                                <div class="col-md-3"><input readonly name="reset_date" type="text" id="reset_date" value="{{date('M-d-Y', strtotime(Auth::user()->end_date))}}" class="form-control"/></div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div id="popover-password" class="hide_991">
                                                <ul class="list-unstyled passwordbox">
                                                    <li class=""><span class="low-upper-case"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; 1 lowercase &amp; 1 uppercase</li>
                                                    <li class=""><span class="one-number"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 number (0-9)</li>
                                                    <li class=""><span class="one-special-char"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 Special Character (!@#$%^&*).</li>
                                                    <li class=""><span class="eight-character"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Atleast 8 Character</li>
                                                    <li class=""><span class="password-match"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Both Passwords are Same</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-8">
                                    <div class="card-footer">
                                        <label class="control-label col-lg-5 col-md-4"></label>
                                        <div class="col-md-7 ">
                                            <div class="row">
                                                <div class="col-xs-3" style="padding-left:7px;width:120px;">
                                                    <button type="submit" class="btn_new_save">Save</button>
                                                </div>
                                                <div class="col-xs-3" style="width:120px;">
                                                    <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/')}}">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
        <script>
            $.ajaxSetup({
                headers:
                    {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    }
            });
            $(document).ready(function () {

                $('.changepassword').bootstrapValidator({
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and cannot be empty'
                                },
                                regexp:
                                    {

                                        regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$#!%*?&])[A-Za-z\d$@#$!%*?&]{8}/,

                                        message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
                                    },

                                different: {
                                    field: 'oldpassword',
                                    message: 'The password cannot be the same as Current Password'
                                }
                            }
                        },
                        oldpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Current Password'
                                },
                                remote: {
                                    message: 'The Password is not available',
                                    url: '{{ URL::to('admincheck') }}',
                                    data: {
                                        type: 'oldpassword'
                                    },
                                    type: 'POST'
                                }
                            }
                        },
                        cpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The confirm password is required and can\'t be empty'
                                },
                                identical: {
                                    field: 'cpassword',
                                    message: 'The password and its confirm are not the same'
                                },
                                different: {
                                    field: 'oldpassword',
                                    message: 'The password can\'t be the same as Old Password'
                                }
                            }
                        }
                    }

                }).on('success.form.bv', function (e) {
                    $('.changepassword').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('.changepassword').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();
                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        // console.log(result);
                    }, 'json');
                });
            });
        </script>

        <script>
            $(document).ready(function () {
                $('#resetdays').on('change', function () {
                    var reset = parseInt($('#resetdays').val());
                    var date = new Date();
                    var t = new Date();
                    //var n = $("#resetdays").val();
                    //alert(offset);
                    t.setDate(t.getDate() + reset);
                    var month = "0" + (t.getMonth() + 1);
                    var date = "0" + t.getDate();

                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                    const d = new Date();

                    month = month.slice(-2);
                    date = date.slice(-2);
                    //var date = date +"/"+month+"/"+t.getFullYear();
                    var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                    $('#reset_date').val(date);
                });
            });
        </script>
        <Script>

            $(document).ready(function () {


                $('#password').keyup(function () {
                    var password = $('#password').val();
                    if (checkStrength(password) == false) {
                        //  $('#sign-up').attr('disabled', true);
                    }
                });

                function checkStrength(password) {
                    var strength = 0;


                    //If password contains both lower and uppercase characters, increase strength value.
                    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                        strength += 1;
                        $('.low-upper-case').addClass('text-success');
                        $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                        $('#popover-password-top').addClass('hide');


                    } else {
                        $('.low-upper-case').removeClass('text-success');
                        $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                        $('#popover-password-top').removeClass('hide');
                    }

                    //If it has numbers and characters, increase strength value.
                    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                        strength += 1;
                        $('.one-number').addClass('text-success');
                        $('.one-number i').removeClass('fa-times').addClass('fa-check');
                        $('#popover-password-top').addClass('hide');

                    } else {
                        $('.one-number').removeClass('text-success');
                        $('.one-number i').addClass('fa-times').removeClass('fa-check');
                        $('#popover-password-top').removeClass('hide');
                    }

                    //If it has one special character, increase strength value.
                    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                        strength += 1;
                        $('.one-special-char').addClass('text-success');
                        $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                        $('#popover-password-top').addClass('hide');

                    } else {
                        $('.one-special-char').removeClass('text-success');
                        $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                        $('#popover-password-top').removeClass('hide');
                    }

                    if (password.length > 7) {
                        strength += 1;
                        $('.eight-character').addClass('text-success');
                        $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                        $('#popover-password-top').addClass('hide');

                    } else {
                        $('.eight-character').removeClass('text-success');
                        $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                        $('#popover-password-top').removeClass('hide');
                    }

                    $("#cpassword").keyup(function () {
                        if ($('#password').val() == $('#cpassword').val()) {
                            strength += 1;
                            $('.password-match').addClass('text-success');
                            $('.password-match i').removeClass('fa-times').addClass('fa-check');
                            $('#popover-password-top').addClass('hide');

                        } else {
                            $('.password-match').removeClass('text-success');
                            $('.password-match i').addClass('fa-times').removeClass('fa-check');
                            $('#popover-password-top').removeClass('hide');
                        }
                    });


                    // If value is less than 2

                    if (strength < 2) {
                        $('#result').removeClass()
                        $('#password-strength').addClass('progress-bar-danger');

                        $('#result').addClass('text-danger').text('Very Week');
                        $('#password-strength').css('width', '10%');
                    } else if (strength == 2) {
                        $('#result').addClass('good');
                        $('#password-strength').removeClass('progress-bar-danger');
                        $('#password-strength').addClass('progress-bar-warning');
                        $('#result').addClass('text-warning').text('Week')
                        $('#password-strength').css('width', '60%');
                        return 'Week'
                    } else if (strength == 4) {
                        $('#result').removeClass()
                        $('#result').addClass('strong');
                        $('#password-strength').removeClass('progress-bar-warning');
                        $('#password-strength').addClass('progress-bar-success');
                        $('#result').addClass('text-success').text('Strength');
                        $('#password-strength').css('width', '100%');

                        return 'Strong'
                    }

                }

            });

        </Script>

@endsection()