@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        #example_filter {
            margin-top: -56px;
            position: absolute;
            margin-left: 240px;
        }

        .dt-buttons {
            position: absolute;
            margin-top: 20px;
            right: 20px;
        }

        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header page-title">
            <div class="">
                <div class="" style="text-align:center;">
                    <h2>List of Rules / Responsibility <span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;">Add / View / Edit</span></h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-tools pull-right" style="position: relative;margin-top: 7px;margin-right: 20px;z-index: 9999;">
                            <div class="table-title">
                                <a href="{{route('rules.create')}}" style="position: absolute;width: auto;margin-left: -310px;margin-top: 22px;">Add New Rules / Resp.</a>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="box-header">
                                <div class="col-md-8" style="">
                                    <div class="row" style="">
                                        <div class="col-md-2" style="width:57px;padding:0px;"><label style="margin-top:10px;">Filter : </label></div>
                                        <div class="col-md-7" style="padding-left:0px;">
                                            <select name="choice" style="width: 150px;margin-left: 0px;" id="choice" class="form-control">
                                                <option value="1">All</option>
                                                <option value="Rules">Rules</option>
                                                <option value="Resposibilty">Responsibility</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                            @endif
                            @if ( session()->has('error') )
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">#</th>
                                        <th style="text-align:center;width:130px !important">Creation Date</th>
                                        <th style="text-align:center">Type</th>
                                        <th style="text-align:center">Whom</th>
                                        <th style="text-align:center;width:250px !important">Title</th>

                                        <th style="text-align:center" width="150px;">Check / Uncheck</th>
                                        <th style="text-align:center">Check Date</th>
                                        <th style="text-align:center;width:105px">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($homecontent as $sli)
                                        <tr>
                                            <td style="text-align:center;"
                                            {{$loop->index + 1}}</td>

                                            <td style="text-align:center;">{!!$sli->date!!}</td>
                                            <td>{!!$sli->type!!}</td>
                                            <td>{!!$sli->usertype!!}</td>
                                            <td>{!!$sli->title!!}</td>
                                            <td style="text-align:center;">
                                                <center>@if($sli->status=='2') <img src="{{URL::asset('public/img/green.jpg')}}" alt="" width="25"> @else  <img src="{{URL::asset('public/img/red.jpg')}}" alt="" width="25"> @endif </center>
                                            </td>
                                            <td style="text-align:center;">{!!$sli->rulesdate !!}</td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('rules.edit', $sli->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('rules.destroy',$sli->id) }}" method="post" style="display:none" id="delete-id-{{$sli->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>

                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$sli->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();
                    //alert(_val);
                    if (_val == 'Rules') {
                        table.columns(2).search(_val).draw();
                    } else if (_val == 'Resposibilty') {
                        table.columns(2).search(_val).draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>


@endsection()