@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .form-check {
            width: 50%;
            float: left;
        }

        .cke_bottom {
            display: none;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Rules / Responsibility</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('rules.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <label class="control-label col-md-3">Date / Day / Time:</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="">
                                                    <input type="text" name="date" id="date" class="form-control" value="{{date('m/d/Y')}}" placeholder="Date">
                                                </div>
                                            </div>

                                            <div class="col-md-2" style="width: 110px;">
                                                <div class="row">
                                                    <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="{{date('l')}}">
                                                </div>
                                            </div>
                                            <input type="hidden" name="state" readonly id="state" value="employee" class="form-control">
                                            <div class="col-md-2">
                                                <div class="">
                                                    <input type="text" name="time" id="time" class="form-control" value="{{date("H:i a")}}" placeholder="Time">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type :</label>
                                    <div class="col-md-8">
                                        <select name="type" type="type" id="type" class="form-control">
                                            <option value="">---Select---</option>
                                            <option value="Rules">Rules</option>
                                            <option value="Resposibilty">Responsibility</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Type :</label>
                                    <div class="col-md-8">
                                        <select name="usertype" type="type" id="usertype" class="form-control">
                                            <option value="">---Select---</option>
                                            <option value="FSC Employee">FSC Employee</option>
                                            <option value="FSC User">FSC User</option>
                                            <!--<option value="FSC Client">FSC Client</option>
                                            <option value="Client">Client</option>
                                           !-->                                                                </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group emp hideemp" style="display:none">
                                    <label class="control-label col-md-3">Employee / User :</label>
                                    <div class="col-md-8">
                                        <select name="employee_id" id="employee_id" class="form-control">
                                            <option value="">---Select Employee---</option>
                                            @foreach($employee as $as)
                                                <option value="{{$as->id}}">{{ucfirst($as->firstName.' '.$as->middleName.' '.$as->lastName)}} ({{$as->teams}})</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group emp" style="display:none">
                                    <label class="control-label col-md-3">Responsibility Type :</label>
                                    <div class="col-md-8">
                                        <select name="respon_type" id="respon_type" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Daily">Daily</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="Quarterly">Quarterly</option>
                                            <option value="Regular">Regular</option>
                                            <option value="Half-Yearly">Half-Yearly</option>
                                            <option value="Yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group resp1" style="display:none;">
                                    <label class="control-label col-md-3">HowToDo :</label>
                                    <div class="col-md-3">
                                        <select name="howtodo_id" id="howtodo_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($howtodos as $howtodo)
                                                <option value="{{$howtodo->id}}">{{$howtodo->subject}}</option>
                                            @endforeach</select>
                                    </div>
                                </div>

                                <div class="form-group choose_day" style="display:none">
                                    <label class="control-label col-md-3">Choose Day :</label>
                                    <div class="col-md-8">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="choose_day1" value="Monday">
                                            <label class="form-check-label" for="exampleCheck1">Monday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck2" name="choose_day2" value="Tuesday">
                                            <label class="form-check-label" for="exampleCheck2">Tuesday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck3" name="choose_day3" value="Wednesday">
                                            <label class="form-check-label" for="exampleCheck3">Wednesday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck4" name="choose_day4" value="Thursday">
                                            <label class="form-check-label" for="exampleCheck4">Thursday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck5" name="choose_day5" value="Friday">
                                            <label class="form-check-label" for="exampleCheck5">Friday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck6" name="choose_day6" value="Saturday">
                                            <label class="form-check-label" for="exampleCheck6">Saturday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck7" name="choose_day7" value="Sunday">
                                            <label class="form-check-label" for="exampleCheck7">Sunday</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group emp1" style="display:none">
                                    <label class="control-label col-md-3">Choose Date :</label>
                                    <div class="col-md-8">
                                        <select name="choose_date" id="choose_date" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            for( $i = 1; $i <= 31; $i++ )
                                            {?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php
                                            }?>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('typeofservice') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type of Service :</label>
                                    <div class="col-md-3">
                                        <select name="typeofservice" type="text" id="typeofservice" class="form-control">
                                            <option value="">Type of Service</option>
                                            @foreach($typeofser as $typeofser1)
                                                <option value="{{$typeofser1->id}}">{{$typeofser1->typeofservice}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('typeofservice'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('typeofservice') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Title :</label>
                                    <div class="col-md-8">
                                        <input name="title" type="text" id="title" class="form-control">

                                        @if ($errors->has('title'))
                                            <span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Rules / Resposibilty :</label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <textarea id="editor1" name="rules" rows="10" cols="80"></textarea>
                                        </div>
                                        @if ($errors->has('rules'))
                                            <span class="help-block">
											<strong>{{ $errors->first('rules') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Send">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/rules')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $('#type').on('change', function () {
                    if ($('#type').val() == 'Resposibilty') {
                        $('.emp').show();
                        $('.resp1').show();

                    } else {
                        $('.resp1').hide();

                        $('.emp').hide();
                    }

                });
            });

        </script>

        <script>
            $(document).ready(function () {
                $('#usertype').on('change', function () {
                    //    alert();
                    var id = $(this).val();
                    //  alert(id);
                    $.get('{!!URL::to('getusertype')!!}?id=' + id, function (data) {
                        if (data == "") {

                        } else {

                        }
                        $('#employee_id').empty();
                        $('.hideemp').hide();
                        $('#employee_id').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            if (subcatobj.teams) {
                                var team = '(' + subcatobj.teams + ')';
                            } else {
                                var team = '';
                            }
                            $('.hideemp').show();
                            $('#employee_id').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '  ' + team + '</option>');
                        })
                    });
                });

                $('#respon_type').on('change', function () {
                    if ($('#respon_type').val() == 'Weekly') {
                        $('.choose_day').show();
                        $('.emp1').hide();
                    } else if ($('#respon_type').val() == 'Monthly') {

                        $('.emp1').show();
                        $('.choose_day').hide();
                    } else {
                        $('.emp1').hide();
                        $('.choose_day').hide();
                    }

                });
            });

        </script>


@endsection()