@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .form-check {
            width: 50%;
            float: left;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Rules / Responsibility</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">

                            <form method="post" action="{{route('rules.update',$homecontent->id)}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date / Day / Time:</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="">
                                                    <input type="text" name="date" id="date" class="form-control" value="{!!$homecontent->date!!}" placeholder="Date">
                                                </div>
                                            </div>

                                            <div class="col-md-2" style="width: 110px;">
                                                <div class="row">
                                                    <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="{!!$homecontent->day!!}">
                                                </div>
                                            </div>
                                            <input type="hidden" name="state" readonly id="state" value="employee" class="form-control">
                                            <div class="col-md-2">
                                                <div class="">
                                                    <input type="text" name="time" id="time" class="form-control" value="{!!$homecontent->time!!}" placeholder="Time">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type :</label>
                                    <div class="col-md-8">
                                        <select name="type" type="text" id="type" class="form-control">
                                            <option value="">---Select---</option>
                                            <option value="Rules" @if($homecontent->type=='Rules') selected @endif>Rules</option>
                                            <option value="Resposibilty" @if($homecontent->type=='Resposibilty') selected @endif>Responsibility</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} emp" @if($homecontent->type=='Resposibilty') @else style="display:none" @endif >
                                    <label class="control-label col-md-3">Employee / User :</label>
                                    <div class="col-md-8">
                                        <select name="employee_id" id="employee_id" class="form-control">
                                            <option value="">---Select Employee---</option>
                                            @foreach($employee as $as)
                                                <option value="{{$as->id}}" @if($homecontent->employee_id==$as->id) selected @endif>{{ucfirst($as->firstName.' '.$as->middleName.' '.$as->lastName)}} ({{$as->teams}})</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Type :</label>
                                    <div class="col-md-8">
                                        <select name="usertype" type="text" id="usertype" class="form-control">
                                            <option value="">---Select---</option>
                                            <option value="FSC Employee" @if($homecontent->usertype=='FSC Employee') selected @endif>FSC Employee</option>
                                            <option value="FSC User" @if($homecontent->usertype=='FSC User') selected @endif>FSC User</option>
                                            <option value="FSC Client" @if($homecontent->usertype=='FSC Client') selected @endif>FSC Client</option>
                                            <option value="Client" @if($homecontent->usertype=='Client') selected @endif>Client</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group emp" @if($homecontent->type=='Resposibilty') @else style="display:none" @endif>
                                    <label class="control-label col-md-3">Responsibility Type :</label>
                                    <div class="col-md-8">
                                        <select name="respon_type" id="respon_type" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Daily" @if($homecontent->respon_type=='Daily') selected @endif>Daily</option>
                                            <option value="Weekly" @if($homecontent->respon_type=='Weekly') selected @endif>Weekly</option>
                                            <option value="Monthly" @if($homecontent->respon_type=='Monthly') selected @endif>Monthly</option>
                                            <option value="Quarterly" @if($homecontent->respon_type=='Quarterly') selected @endif>Quarterly</option>
                                            <option value="Regular" @if($homecontent->respon_type=='Regular') selected @endif>Regular</option>
                                            <option value="Half-Yearly" @if($homecontent->respon_type=='Half-Yearly') selected @endif>Half-Yearly</option>
                                            <option value="Yearly" @if($homecontent->respon_type=='Yearly') selected @endif>Yearly</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">HowToDo :</label>
                                    <div class="col-md-3">
                                        <select name="howtodo_id" id="howtodo_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($howtodos as $howtodo)
                                                <option value="{{$howtodo->id}}" @if($homecontent->howtodo_id ==$howtodo->id) selected @endif>{{$howtodo->subject}}</option>
                                            @endforeach</select>
                                    </div>
                                </div>

                                <div class="form-group choose_day" @if($homecontent->respon_type=='Weekly')  @else style="display:none" @endif >
                                    <label class="control-label col-md-3">Choose Day :</label>
                                    <div class="col-md-8">

                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" @if($homecontent->choose_day1=='Monday') checked @endif name="choose_day1" value="Monday">
                                            <label class="form-check-label" for="exampleCheck1">Monday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck2" name="choose_day2" value="Tuesday" @if($homecontent->choose_day2=='Tuesday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck2">Tuesday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck3" name="choose_day3" value="Wednesday" @if($homecontent->choose_day3=='Wednesday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck3">Wednesday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck4" name="choose_day4" value="Thursday" @if($homecontent->choose_day4=='Thursday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck4">Thursday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck5" name="choose_day5" value="Friday" @if($homecontent->choose_day5=='Friday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck5">Friday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck6" name="choose_day6" value="Saturday" @if($homecontent->choose_day6=='Saturday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck6">Saturday</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck7" name="choose_day7" value="Sunday" @if($homecontent->choose_day7=='Sunday') checked @endif>
                                            <label class="form-check-label" for="exampleCheck7">Sunday</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group emp1" @if(!empty($homecontent->choose_date)) @else style="display:none" @endif>
                                    <label class="control-label col-md-3">Choose Date :</label>
                                    <div class="col-md-8">
                                        <select name="choose_date" id="choose_date" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            for( $i = 1; $i <= 31; $i++ )
                                            {?>
                                            <option value="<?php echo $i;?>" @if($homecontent->choose_date==$i) selected @endif><?php echo $i;?></option>
                                            <?php
                                            }?>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('typeofservice') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type of Service :</label>
                                    <div class="col-md-3">
                                        <select name="typeofservice" type="text" id="typeofservice" class="form-control">
                                            <option value="">Type of Service</option>
                                            @foreach($typeofser as $typeofser1)
                                                <option value="{{$typeofser1->id}}" @if($typeofser1->id==$homecontent->typeofservice) selected @endif>{{$typeofser1->typeofservice}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('typeofservice'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('typeofservice') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Title :</label>
                                    <div class="col-md-8">
                                        <input name="title" type="text" id="title" value="{!!$homecontent->title!!}" class="form-control">

                                        @if ($errors->has('title'))
                                            <span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Rules / Resposibilty :</label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <textarea id="editor1" name="rules" rows="10" cols="80">{!!$homecontent->rules!!}</textarea>
                                        </div>
                                        @if ($errors->has('rules'))
                                            <span class="help-block">
										<strong>{{ $errors->first('rules') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-8">
                                        <div class="">
                                            <label><input id="checked" type="checkbox" name="checked" value="2" @if($homecontent->status=='2') checked @endif> Click Here</label>
                                        </div>
                                        @if ($errors->has('rules'))
                                            <span class="help-block">
										<strong>{{ $errors->first('rules') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 ">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/rules')}}">Cancel</a>
                                        </div>
                                        <div class="col-md-2 row">


                                            <a class="btn_new_cancel" style="background:red !important;" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                    {event.preventDefault();document.getElementById('delete-id-{{$homecontent->id}}').submit();} else{event.preventDefault();}" href="">Delete</a>

                                        </div>

                                    </div>
                                </div>

                            </form>
                            <form action="{{ route('rules.destroy',$homecontent->id) }}" method="post" style="display:none" id="delete-id-{{$homecontent->id}}">
                                {{csrf_field()}} {{method_field('DELETE')}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $('#type').on('change', function () {
                    if ($('#type').val() == 'Resposibilty') {
                        $('.emp').show();
                    } else {
                        $('.emp').hide();
                    }

                });
            });

        </script>
        <script>
            $(document).ready(function () {
                $('#respon_type').on('change', function () {
                    if ($('#respon_type').val() == 'Weekly') {
                        $('.choose_day').show();
                        $('.emp1').hide();
                    } else if ($('#respon_type').val() == 'Monthly') {

                        $('.emp1').show();
                        $('.choose_day').hide();
                    } else {
                        $('.emp1').hide();
                        $('.choose_day').hide();
                    }

                });
            });

        </script>
@endsection()