@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .dt-buttons {
            margin-bottom: 10px;
        }

        .search-btn {
            position: absolute;
            top: 10px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        .dt-buttons {
            margin-top: -42px;
            position: absolute;
            margin-left: 800px;
        }
    </style>t
    <div class="content-wrapper">

        <section class="content-header page-title" style="height: 39px;;margin-top:-20px !important;">
            <div class="row col-md-12">
                <div class="col-md-7" style="text-align:right;">
                    <h2>List of Employee / User</h2>
                    <h3 style="display:none">List of Employee / User</h3>
                </div>
                <div class="col-md-5" style="text-align:right;">
                    <h2>Add / View / Edit</h2>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="col-md-9" style="margin-left:-1.6%">
                                <div class="row">
                                    <div class="col-md-1" style="width: 7.333% !important;"><label style="margin-left:28%;margin-top: 11px;">Filter: </label></div>
                                    <div class="col-md-3" style="width: 135px;">
                                        <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                                            <option value="1">All</option>
                                            <option value="Active" selected>Active</option>
                                            <option value="Inactive">Inactive</option>
                                            <option value="New">New</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1"><label style="margin-left:28%;margin-top: 11px;">Search: </label></div>
                                    <div class="col-md-4" style="width: 200px;">
                                        <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
                                            <option value="All" selected>All</option>

                                            <option value="Type">Type</option>
                                            <option value="EE / User ID">EE / User ID</option>
                                            <option value="Employee Name">Employee Name</option>
                                            <option value="Email ID">Email ID</option>
                                            <option value="Tel. Number">Tel. Number</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3" style="width: 180px;">
                                        <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
                                            <tbody>
                                            <tr id="filter_global">
                                                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>

                                            <tr id="filter_col2" data-column="1" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col3" data-column="2" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="EE / User Id">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col4" data-column="3" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col5" data-column="4" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email Id">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            <tr id="filter_col6" data-column="5" style="display:none">
                                                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel. Number">
                                                    <button class="search-btn"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tools pull-right" data-toggle="tooltip" title="Status" style="margin-left:-135px !important;float:left !important;">
                                <div class="table-title">
                                    <a href="{{route('employee.create')}}">Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:-35px !important;">
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">No</th>
                                        <th style="text-align:center">Type</th>
                                        <th style="text-align:center">EE / User ID</th>
                                        <th style="text-align:center">Employee Name</th>
                                        <th style="text-align:center">Email ID</th>
                                        <th style="text-align:center">Tel. Number</th>
                                        <th style="text-align:center">Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($employee as $employ)

                                        <tr @if($employ->type=='user') style="background:#b9f0b2" @endif >
                                            <td style="text-transform: capitalize;text-align:center;">{{$loop->index+1}}</td>
                                            <td style="text-transform: capitalize;">{{$employ->type}}</td>
                                            <td>{{$employ->employee_id}}</td>
                                            <td>{{$employ->firstName}} {{$employ->middleName}} {{$employ->lastName}}</td>
                                            <td>{{$employ->email}}</td>
                                            <td>{{$employ->telephoneNo1}}</td>
                                            <td style="text-align:center"> @if($employ->check==1)<a class="btn-action btn-view-edit" href="" style="background:#689203 !important; text-align:center">Active</a> @else <a class="btn-action btn-delete" href="">Inactive</a> @endif

                                            </td>
                                            <td style="text-align:center;">
                                            <!--@if($employ->newemp==1)<a href="{{route('employee.edit',$employ->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif-->
                                                <a class="btn-action btn-view-edit" style="background:#367fa9 !important" href="{{route('employee.edit',$employ->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{route('employee.destroy',$employ->id)}}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                dom: 'Bfrtlip',
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                }],
                "order": [[0, 'asc']],
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                        //titleAttr: 'Copy',
                        title: $('h3').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                        // titleAttr: 'Excel',
                        title: $('h3').text(),
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c', sheet).attr('s', '51');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                        // titleAttr: 'CSV',
                        title: $('h3').text(),
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                        customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                            var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
                            doc.pageMargins = [20, 60, 20, 20];
                            doc.defaultStyle.fontSize = 10;
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [{
                                        alignment: 'left',
                                        image: logo,
                                        width: 50, margin: [200, 5]
                                    }, {
                                        alignment: 'CENTER',
                                        text: 'List of Client',
                                        fontSize: 20,
                                        margin: [10, 35],
                                    },],
                                    margin: [20, 0, 0, 12], alignment: 'center',
                                }
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return 2;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#ccc';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 14;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 14;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp; Print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/></center>'
                                );
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },
                        footer: true,
                        autoPrint: true
                    },],
            });
            $('input.global_filter').on('keyup click', function () {
                filterGlobal();
            });

            $('input.column_filter').on('keyup click', function () {
                filterColumn($(this).parents('tr').attr('data-column'));
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    table.cell(cell).invalidate('dom');
                });
            }).draw();
            table.columns(6)
                .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                .draw();
            $("#choice").on("change", function () {
                var _val = $(this).val();//alert(_val);

                if (_val == 'Inactive') {
                    table.columns(6).search(_val).draw();
                } else if (_val == 'New') {
                    table.columns(6).search(_val).draw();
                } else if (_val == 'Active') {  //alert();
                    table.columns(6).search(_val).draw();
                    table.columns(6)
                        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                        .draw();
                } else {
                    table
                        .columns()
                        .search('')
                        .draw();
                }
            })
        });

        function filterGlobal() {
            $('#example').DataTable().search(
                $('#global_filter').val(),
                $('#global_regex').prop('checked'),
                $('#global_smart').prop('checked')
            ).draw();
        }

        function filterColumn(i) {
            $('#example').DataTable().column(i).search(
                $('#col' + i + '_filter').val(),
                $('#col' + i + '_regex').prop('checked'),
                $('#col' + i + '_smart').prop('checked')
            ).draw();
        }

        $("#types").on('change', function () {
            // For unique choice
            var selVal = $("#types option:selected").val();

            if (selVal == 'Type') {
                $('#filter_global').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col2').show();
            } else if (selVal == 'EE / User ID') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col3').show();
            } else if (selVal == 'Employee Name') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col4').show();
            } else if (selVal == 'Email ID') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col6').hide();
                $('#filter_col5').show();
            } else if (selVal == 'Tel. Number') {
                $('#filter_global').hide();
                $('#filter_col2').hide();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').show();
            } else {
                $('#filter_global').show();
                $('#filter_col3').hide();
                $('#filter_col4').hide();
                $('#filter_col5').hide();
                $('#filter_col6').hide();
                $('#filter_col2').hide();
            }
        });
    </script>
    <style>
        .dataTables_filter {
            display: none;
        }
    </style>
@endsection()