@extends('admin.layouts.app')
@section('main-content')
<style>
    .nav-tabs > li {
        width: 167px;
    }

    .nav-tabs > li > a {
        height: 46px;
    }

    .btn-center {
        margin: auto;
        width: 250px;
    }

    .Branch {
        width: 100%;
        margin: 20px 0 0 0;
        text-align: center;
        background: #428bca;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        padding: 5px;
    }

    .Branch h1 {
        padding: 0px;
        margin: 0px 0 0 0;
        color: #fff;
        font-size: 24px;
    }
</style>
<div class="content-wrapper">
    <div class="page-title">
        <h1>FSC Employee</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="display: inline-block;">
                <div class="card-body">
                    <div class="panel with-nav-tabs panel-primary">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab1primary" data-toggle="tab">General Info</a></li>
                                <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Hiring Info</a></li>
                                <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Pay Info</a></li>
                                <li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Personal Info</a></li>
                                <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Info</a></li>
                                <li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Other Info</a></li>
                            </ul>
                        </div>
                        <form method="post" action="{{route('employee.update',$emp->id)}}" class="form-horizontal" enctype="multipart/form-data"> {{csrf_field()}}{{method_field('PATCH')}}
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1primary">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('employee_id') ? ' has-error' : '' }}" style="margin-top:2%; ">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Employee ID : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" name="employee_id" id="employee_id" value="{{$emp->employee_id}}"> @if ($errors->has('employee_id'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('employee_id') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}{{ $errors->has('lastName') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Name : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="firstName" name="firstName" placeholder="First" value="{{$emp->firstName}}">@if ($errors->has('firstName'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" maxlength="1" class="form-control fsc-input" id="middleName" name="middleName" placeholder="Middle" value="{{$emp->middleName}}">
                                                            @if ($errors->has('middleName'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('middleName') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="lastName" name="lastName" value="{{$emp->lastName}}" placeholder="Last">
                                                            @if ($errors->has('lastName'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('lastName') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address1') ? ' has-error' : '' }} " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Address 1 : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="{{$emp->address1}}">@if ($errors->has('address1'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('address1') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Address 2 : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="{{$emp->address2}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('city') ? ' has-error' : '' }}{{ $errors->has('stateId') ? ' has-error' : '' }}{{ $errors->has('zip') ? ' has-error' : '' }} " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">City/State/Zip : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$emp->city}}">@if ($errors->has('city'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="stateId" id="stateId" class="form-control fsc-input">
                                                                    @if(empty($emp->stateId))
                                                                    <option value=''>State</option>
                                                                    @else
                                                                    <option value='{{$emp->stateId}}'>{{$emp->stateId}}</option>
                                                                    @endif
                                                                    <option value="ID">ID</option>
                                                                    <option value='AK'>AK</option>
                                                                    <option value='AS'>AS</option>
                                                                    <option value='AZ'>AZ</option>
                                                                    <option value='AR'>AR</option>
                                                                    <option value='CA'>CA</option>
                                                                    <option value='CO'>CO</option>
                                                                    <option value='CT'>CT</option>
                                                                    <option value='DE'>DE</option>
                                                                    <option value='DC'>DC</option>
                                                                    <option value='FM'>FM</option>
                                                                    <option value='FL'>FL</option>
                                                                    <option value='GA'>GA</option>
                                                                    <option value='GU'>GU</option>
                                                                    <option value='HI'>HI</option>
                                                                    <option value='ID'>ID</option>
                                                                    <option value='IL'>IL</option>
                                                                    <option value='IN'>IN</option>
                                                                    <option value='IA'>IA</option>
                                                                    <option value='KS'>KS</option>
                                                                    <option value='KY'>KY</option>
                                                                    <option value='LA'>LA</option>
                                                                    <option value='ME'>ME</option>
                                                                    <option value='MH'>MH</option>
                                                                    <option value='MD'>MD</option>
                                                                    <option value='MA'>MA</option>
                                                                    <option value='MI'>MI</option>
                                                                    <option value='MN'>MN</option>
                                                                    <option value='MS'>MS</option>
                                                                    <option value='MO'>MO</option>
                                                                    <option value='MT'>MT</option>
                                                                    <option value='NE'>NE</option>
                                                                    <option value='NV'>NV</option>
                                                                    <option value='NH'>NH</option>
                                                                    <option value='NJ'>NJ</option>
                                                                    <option value='NM'>NM</option>
                                                                    <option value='NY'>NY</option>
                                                                    <option value='NC'>NC</option>
                                                                    <option value='ND'>ND</option>
                                                                    <option value='MP'>MP</option>
                                                                    <option value='OH'>OH</option>
                                                                    <option value='OK'>OK</option>
                                                                    <option value='OR'>OR</option>
                                                                    <option value='PW'>PW</option>
                                                                    <option value='PA'>PA</option>
                                                                    <option value='PR'>PR</option>
                                                                    <option value='RI'>RI</option>
                                                                    <option value='SC'>SC</option>
                                                                    <option value='SD'>SD</option>
                                                                    <option value='TN'>TN</option>
                                                                    <option value='TX'>TX</option>
                                                                    <option value='UT'>UT</option>
                                                                    <option value='VT'>VT</option>
                                                                    <option value='VI'>VI</option>
                                                                    <option value='VA'>VA</option>
                                                                    <option value='WA'>WA</option>
                                                                    <option value='WV'>WV</option>
                                                                    <option value='WI'>WI</option>
                                                                    <option value='WY'>WY</option>
                                                                </select>
                                                                @if ($errors->has('stateId'))
                                                                <span class="help-block">
                        <strong>{{ $errors->first('stateId') }}</strong>
                        </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="zip" name="zip" value="{{$emp->zip}}" placeholder="Zip">@if ($errors->has('zip'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('countryId') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Country :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown">
                                                                <select name="countryId" id="countryId" class="form-control fsc-input">
                                                                    @if(empty($emp->countryId))
                                                                    <option value=''>Country</option>
                                                                    <option value='USA'>USA</option>
                                                                    @else
                                                                    <option value='{{$emp->countryId}}'>{{$emp->countryId}}</option>
                                                                    @endif

                                                                    @if ($errors->has('countryId'))
                                                                    <span class="help-block">
                        <strong>{{ $errors->first('countryId') }}</strong>
                        </span>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo1') ? ' has-error' : '' }} {{ $errors->has('telephoneNo1Type') ? ' has-error' : '' }} " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Telephone 1 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="telephoneNo1" name="telephoneNo1" placeholder="+_(___)___-____" value="{{$emp->telephoneNo1}}"> @if ($errors->has('telephoneNo1'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                    <option value="Mobile" @if($emp->telephoneNo1Type=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Resident" @if($emp->telephoneNo1Type=='Resident') selected @endif>Resident</option>
                                                                    <option value="Office" @if($emp->telephoneNo1Type=='Office') selected @endif>Office</option>
                                                                    <option value="Other" @if($emp->telephoneNo1Type=='Other') selected @endif>Other</option>
                                                                </select>
                                                                @if ($errors->has('telephoneNo1Type'))
                                                                <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1Type') }}</strong>
                        </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" readonly id="ext1" name="ext1" value="{{$emp->ext1}}" placeholder="Ext">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo2') ? ' has-error' : '' }} {{ $errors->has('telephoneNo2Type') ? ' has-error' : '' }} " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Telephone 2 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" id="telephoneNo2" name="telephoneNo2" placeholder="+_(___)___-____" value="{{$emp->telephoneNo2}}">
                                                            @if ($errors->has('telephoneNo2'))
                                                            <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2') }}</strong>
                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">

                                                                    <option value="Mobile" @if($emp->telephoneNo2Type=='Mobile') selected @endif>Mobile</option>
                                                                    <option value="Resident" @if($emp->telephoneNo2Type=='Resident') selected @endif>Resident</option>
                                                                    <option value="Office" @if($emp->telephoneNo2Type=='Office') selected @endif>Office</option>
                                                                    <option value="Other" @if($emp->telephoneNo2Type=='Other') selected @endif>Other</option>
                                                                </select>
                                                                @if ($errors->has('telephoneNo2Type'))
                                                                <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2Type') }}</strong>
                        </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" readonly id="ext2" name="ext2" value="{{$emp->ext2}}" placeholder="Ext">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Email : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" id="email" readonly name="email" value="{{$emp->email}}" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('photo') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Select Photo : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input name="photo" placeholder="Upload Service Image" class="form-control fsc-input" id="photo" type="file"><input name="photo1" placeholder="Upload Service Image" value="{{$emp->photo}}" class="form-control fsc-input" id="photo1" type="hidden"> <br><img style="width: 100px;height: 50px;" src="{{asset('public/employeeimage')}}/{{$emp->photo}}" alt="">
                                                    @if ($errors->has('photo'))
                                                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Status :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="check" id="check" class="form-control fsc-input">
                                                        <option value="0" @if($emp->check=='0') selected @endif>Inactive</option>
                                                        <option value="1" @if($emp->check=='1') selected @endif>Active</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2primary">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Hire Date : </label>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="hiremonth" id="hiremonth" class="form-control fsc-input">
                                                                    @if(empty($emp->hiremonth))
                                                                    <option value=''>Month</option>
                                                                    <?php formMonth(); ?>
                                                                    @else
                                                                    <option value='{{$emp->hiremonth}}' selected>{{$emp->hiremonth}}</option>
                                                                    <?php formMonth(); ?>
                                                                    @endif


                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="hireday" id="hireday" class="form-control fsc-input">
                                                                    <option value="">Day</option>
                                                                    @if(empty($emp->hireday))
                                                                    <option value=''>Month</option>
                                                                    <?php formDay(); ?>
                                                                    @else
                                                                    <option value='{{$emp->hireday}}' selected>{{$emp->hireday}}</option>
                                                                    <?php formDay(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="hireyear" id="hireyear" class="form-control fsc-input">
                                                                    <option value="">Year</option>
                                                                    @if(empty($emp->hireyear))
                                                                    <option value=''>Month</option>
                                                                    <?php formYear(); ?>
                                                                    @else
                                                                    <option value='{{$emp->hireyear}}' selected>{{$emp->hireyear}}</option>
                                                                    <?php formYear(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Termination Date :</label>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="termimonth" id="termimonth" class="form-control fsc-input">
                                                                    @if(empty($emp->termimonth))
                                                                    <option value=''>Month</option>
                                                                    <?php formMonth(); ?>
                                                                    @else
                                                                    <option value='{{$emp->termimonth}}' selected>{{$emp->termimonth}}</option>
                                                                    <?php formMonth(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="termiday" id="termiday" class="form-control fsc-input">

                                                                    @if(empty($emp->termiday))
                                                                    <option value=''>Day</option>
                                                                    <?php formDay(); ?>
                                                                    @else
                                                                    <option value='{{$emp->termiday}}' selected>{{$emp->termiday}}</option>
                                                                    <?php formDay(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="termiyear" id="termiyear" class="form-control fsc-input">

                                                                    @if(empty($emp->termiyear))
                                                                    <option value="">Year</option>
                                                                    <?php formYear(); ?>
                                                                    @else
                                                                    <option value='{{$emp->termiyear}}' selected>{{$emp->termiyear}}</option>
                                                                    <?php formYear(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                <label class="fsc-form-label">Note : </label>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                <textarea name="tnote" id="tnote" rows="1" class="form-control fsc-input">{{$emp->tnote}}</textarea>
                                            </div>
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Re-Hire Date :</label>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="rehiremonth" id="rehiremonth" class="form-control fsc-input">

                                                                    @if(empty($emp->rehiremonth))
                                                                    <option value=''>Month</option>
                                                                    <?php formMonth(); ?>
                                                                    @else
                                                                    <option value='{{$emp->rehiremonth}}' selected>{{$emp->rehiremonth}}</option>
                                                                    <?php formMonth(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="rehireday" id="rehireday" class="form-control fsc-input">
                                                                    @if(empty($emp->rehireday))
                                                                    <option value=''>Month</option>
                                                                    <?php formDay(); ?>
                                                                    @else
                                                                    <option value='{{$emp->rehireday}}' selected>{{$emp->rehireday}}</option>
                                                                    <?php formDay(); ?>
                                                                    @endif


                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="rehireyear" id="rehireyear" class="form-control fsc-input">
                                                                    @if(empty($emp->rehireyear))
                                                                    <option value=''>Year</option>
                                                                    <?php formYear(); ?>
                                                                    @else
                                                                    <option value='{{$emp->rehireyear}}' selected>{{$emp->rehireyear}}</option>
                                                                    <?php formYear(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                <label class="fsc-form-label">Note : </label>
                                            </div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                <textarea name="comments" id="comments" rows="1" class="form-control fsc-input">{{$emp->comments}}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="Branch">
                                                    <h1>Branch / Department Info</h1>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Branch City:</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="branch_city" id="branch_city" class="form-control fsc-input">
                                                                        <option value="">Select City</option>
                                                                        <option value="Vadodara">Vadodara</option>
                                                                        <option value="Navsari">Navsari</option>
                                                                        <option value="Norcross" selected="">Norcross</option>
                                                                        <option value="New Orleans">New Orleans</option>
                                                                        <option value="Ahemdabad">Ahemdabad</option>
                                                                        <option value="Navsari">Navsari</option>
                                                                        <option value="Vadodara">Vadodara</option>
                                                                        <option value="Navsari">Navsari</option>
                                                                        <option value="Mumbai">Mumbai</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                    <label class="fsc-form-label">Branch Name:</label>
                                                </div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                    <input type="text" readonly class="form-control fsc-input" id="branch_name" name="branch_name" placeholder="" value="{{$emp->branch_name}}">
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Position :</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="position" id="position" class="form-control fsc-input">
                                                                        <option value="4" selected="selected">Asst. Bookeeper</option>
                                                                        <option value="5">Office Assistant</option>
                                                                        <option value="6">Telemarketer</option>
                                                                        <option value="7">Web Desinger</option>
                                                                        <option value="17">Web Developer</option>
                                                                        <option value="19">CPA</option>
                                                                        <option value="20">Temp</option>
                                                                        <option value="21">Web Tester</option>
                                                                        <option value="29">Manager</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                    <label class="fsc-form-label">Note :</label>
                                                </div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="margin-top:2%;">
                                                    <textarea name="note" id="note" rows="1" class="form-control fsc-input">{{$emp->note}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="Branch">
                                                    <h1>Review Info</h1>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">First Review Days :</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <input type="text" class="form-control fsc-input" name="first_rev_day" id="first_rev_day" value="{{$emp->first_rev_day}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">First Review Date :</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="reviewmonth" id="reviewmonth" class="form-control fsc-input">
                                                                        @if(empty($emp->reviewmonth))
                                                                        <option value=''>Month</option>
                                                                        <?php formDay(); ?>
                                                                        @else
                                                                        <option value='{{$emp->reviewmonth}}' selected>{{$emp->reviewmonth}}</option>
                                                                        <?php formDay(); ?>
                                                                        @endif

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="reviewday" id="reviewday" class="form-control fsc-input">
                                                                        @if(empty($emp->reviewday))
                                                                        <option value=''>Day</option>
                                                                        <?php formDay(); ?>
                                                                        @else
                                                                        <option value='{{$emp->reviewday}}' selected>{{$emp->reviewday}}</option>
                                                                        <?php formDay(); ?>
                                                                        @endif

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="reviewyear" id="reviewyear" class="form-control fsc-input">
                                                                        @if(empty($emp->reviewyear))
                                                                        <option value="">Year</option>
                                                                        <?php formYear(); ?>
                                                                        @else
                                                                        <option value='{{$emp->reviewyear}}' selected>{{$emp->reviewyear}}</option>
                                                                        <?php formYear(); ?>
                                                                        @endif

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Comments :</label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <textarea name="hiring_comments" id="hiring_comments" rows="1" class="form-control fsc-input">{{$emp->hiring_comments}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3primary">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Pay Method :</label>
                                                    <div class="col-md-8">
                                                        <select name="pay_method" id="pay_method" class="form-control">
                                                            <option value="Salary" @if($emp->pay_method=='Salary') selected @endif>Salary</option>
                                                            <option value="Hourly" @if($emp->Hourly=='Salary') selected @endif>Hourly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Pay Scale :</label>
                                                    <div class="col-md-8">
                                                        <input name="pay_scale" value="{{$emp->pay_scale}}" type="text" id="pay_scale" class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Note1</label>
                                                    <div class="col-md-8">
                                                        <input name="fields" value="{{$emp->fields}}" type="text" id="fields" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Pay Duration :</label>
                                                    <div class="col-md-8">
                                                        <select name="pay_frequency" id="pay_frequency" class="form-control">
                                                            <option value="Weekly" @if($emp->pay_frequency=='Weekly') selected @endif>Weekly</option>
                                                            <option value="Bi-Weekly" @if($emp->pay_frequency=='Bi-Weekly') selected @endif>Bi-Weekly</option>
                                                            <option value="Bi-Monthly" @if($emp->pay_frequency=='Bi-Monthly') selected @endif>Bi-Monthly</option>
                                                            <option value="Monthly" @if($emp->pay_frequency=='Monthly') selected @endif>Monthly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab4primary">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Gender :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="radio-inline col-md-3">
                                                                <label class="control-label">
                                                                    <input name="gender" value="Male" @if($emp->gender=='Male') checked="checked" @endif id="inlineRadio1" type="radio">
                                                                    Male</label>
                                                            </div>
                                                            <div class="radio-inline col-md-3">
                                                                <label class="control-label">
                                                                    <input name="gender" value="Female" id="inlineRadio2" @if($emp->gender=='Female') checked="checked" @endif type="radio">
                                                                    Female</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Marital Status :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="marital" id="marital" class="form-control fsc-input">
                                                                    <option value="Married" @if($emp->marital=='Married') selected @endif>Married</option>
                                                                    <option value="UnMarried" @if($emp->marital=='UnMarried') selected @endif>UnMarried</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Date Of Birth : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="month" id="month" class="form-control fsc-input">

                                                                    @if(empty($emp->month))
                                                                    <option value="">Month</option>
                                                                    <?php formMonth(); ?>
                                                                    @else
                                                                    <option value='{{$emp->month}}' selected>{{$emp->month}}</option>
                                                                    <?php formMonth(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="day" id="day" class="form-control fsc-input">

                                                                    @if(empty($emp->day))
                                                                    <option value="">Day</option>
                                                                    <?php formDay(); ?>
                                                                    @else
                                                                    <option value='{{$emp->day}}' selected>{{$emp->day}}</option>
                                                                    <?php formDay(); ?>
                                                                    @endif


                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="year" id="year" class="form-control fsc-input">
                                                                    @if(empty($emp->year))
                                                                    <option value="">Year</option>
                                                                    <?php formYear(); ?>
                                                                    @else
                                                                    <option value='{{$emp->year}}' selected>{{$emp->year}}</option>
                                                                    <?php formYear(); ?>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Id Proof 1 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="pf1" class="form-control fsc-input" id="pf1">
                                                                    <option value="">Select Proof</option>
                                                                    <option value="Voter Id" @if($emp->pf1=='Voter Id') selected @endif>Voter Id</option>
                                                                    <option value="Driving Licence" @if($emp->pf1=='Driving Licence') selected @endif>Driving Licence</option>
                                                                    <option value="Pan Card" @if($emp->pf1=='DPan Card') selected @endif>Pan Card</option>
                                                                    <option value="Pass Port" @if($emp->pf1=='Pass Port') selected @endif>Pass Port</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('pfid1') ? ' has-error' : '' }}">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <input name="pfid1" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid1" type="file">

                                                                @if ($errors->has('pfid1'))
                                                                <span class="help-block">
                        <strong>{{ $errors->first('pfid1') }}</strong>
                        </span>
                                                                @endif


                                                                <input name="pfid12" placeholder="Upload Service Image" class="form-control fsc-input" value="{{$emp->pfid1}}" id="pfid12" type="hidden">
                                                                <input name="pfid22" placeholder="Upload Service Image" class="form-control fsc-input" value="{{$emp->pfid2}}" id="pfid22" type="hidden">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <img src="{{asset('public/employeeProof1')}}/{{$emp->pfid1}}" alt="" width="100px">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Id Proof 2 :</label>
                                                </div>
                                                <div class="col-lg-5	 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <select name="pf2" class="form-control fsc-input" id="pf2">
                                                                    <option value="">Select Proof</option>
                                                                    <option value="Voter Id" @if($emp->pf2=='Voter Id') selected @endif>Voter Id</option>
                                                                    <option value="Driving Licence" @if($emp->pf2=='Driving Licence') selected @endif>Driving Licence</option>
                                                                    <option value="Pan Card" @if($emp->pf2=='DPan Card') selected @endif>Pan Card</option>
                                                                    <option value="Pass Port" @if($emp->pf2=='Pass Port') selected @endif>Pass Port</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('pfid2') ? ' has-error' : '' }}">
                                                            <div class="dropdown" style="margin-top: 1%;">
                                                                <input name="pfid2" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid2" type="file">@if ($errors->has('pfid2'))
                                                                <span class="help-block">
                        <strong>{{ $errors->first('pfid2') }}</strong>
                        </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <img src="{{asset('public/employeeProof2')}}/{{$emp->pfid2}}" alt="" width="100px">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="Branch">
                                                        <h1>Emergency Contact Info</h1>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Person Name :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="epname" id="epname" value="{{$emp->epname}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Relationship :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="relation" id="relation" value="{{$emp->relation}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Address :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="{{$emp->eaddress1}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">City/State/Zip : </label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="ecity" name="ecity" placeholder="City" value="{{$emp->ecity}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="estate" id="estate" class="form-control fsc-input">
                                                                            <option value="ID">Select</option>
                                                                            <option value="ID">ID</option>
                                                                            <option value="AK">AK</option>
                                                                            <option value="AS">AS</option>
                                                                            <option value="AZ">AZ</option>
                                                                            <option value="AR">AR</option>
                                                                            <option value="CA">CA</option>
                                                                            <option value="CO">CO</option>
                                                                            <option value="CT">CT</option>
                                                                            <option value="DE">DE</option>
                                                                            <option value="DC">DC</option>
                                                                            <option value="FM">FM</option>
                                                                            <option value="FL">FL</option>
                                                                            <option value="GA">GA</option>
                                                                            <option value="GU">GU</option>
                                                                            <option value="HI">HI</option>
                                                                            <option value="ID">ID</option>
                                                                            <option value="IL">IL</option>
                                                                            <option value="IN">IN</option>
                                                                            <option value="IA">IA</option>
                                                                            <option value="KS">KS</option>
                                                                            <option value="KY">KY</option>
                                                                            <option value="LA">LA</option>
                                                                            <option value="ME">ME</option>
                                                                            <option value="MH">MH</option>
                                                                            <option value="MD">MD</option>
                                                                            <option value="MA">MA</option>
                                                                            <option value="MI">MI</option>
                                                                            <option value="MN">MN</option>
                                                                            <option value="MS">MS</option>
                                                                            <option value="MO">MO</option>
                                                                            <option value="MT">MT</option>
                                                                            <option value="NE">NE</option>
                                                                            <option value="NV">NV</option>
                                                                            <option value="NH">NH</option>
                                                                            <option value="NJ">NJ</option>
                                                                            <option value="NM">NM</option>
                                                                            <option value="NY">NY</option>
                                                                            <option value="NC">NC</option>
                                                                            <option value="ND">ND</option>
                                                                            <option value="MP">MP</option>
                                                                            <option value="OH">OH</option>
                                                                            <option value="OK">OK</option>
                                                                            <option value="OR">OR</option>
                                                                            <option value="PW">PW</option>
                                                                            <option value="PA">PA</option>
                                                                            <option value="PR">PR</option>
                                                                            <option value="RI">RI</option>
                                                                            <option value="SC">SC</option>
                                                                            <option value="SD">SD</option>
                                                                            <option value="TN">TN</option>
                                                                            <option value="TX">TX</option>
                                                                            <option value="UT">UT</option>
                                                                            <option value="VT">VT</option>
                                                                            <option value="VI">VI</option>
                                                                            <option value="VA">VA</option>
                                                                            <option value="WA">WA</option>
                                                                            <option value="WV">WV</option>
                                                                            <option value="WI">WI</option>
                                                                            <option value="WY">WY</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="ezipcode" name="ezipcode" value="{{$emp->ezipcode}}" placeholder="Zip">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Telephone 1 :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="etelephone1" name="etelephone1" placeholder="(000)000-0000" value="{{$emp->etelephone1}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="eteletype1" id="eteletype1" class="form-control fsc-input">
                                                                            <option value="Mobile" @if($emp->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                            <option value="Resident" @if($emp->eteletype1=='Resident') selected @endif>Resident</option>
                                                                            <option value="Office" @if($emp->eteletype1=='Office') selected @endif>Office</option>
                                                                            <option value="Other" @if($emp->eteletype1=='Other') selected @endif>Other</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" readonly id="eext1" name="eext1" value="{{$emp->eext1}}" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Telephone 2 :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="{{$emp->etelephone2}}">
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                    <div class="dropdown" style="margin-top: 1%;">
                                                                        <select name="eteletype2" id="eteletype2" class="form-control fsc-input">
                                                                            <option value="Mobile" @if($emp->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                                            <option value="Resident" @if($emp->eteletype1=='Resident') selected @endif>Resident</option>
                                                                            <option value="Office" @if($emp->eteletype1=='Office') selected @endif>Office</option>
                                                                            <option value="Other" @if($emp->eteletype1=='Other') selected @endif>Other</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text" class="form-control fsc-input" readonly id="eext2" name="eext2" value="{{$emp->eext2}}" placeholder="Ext">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Note For Emergency :</label>
                                                        </div>
                                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input">{{$emp->comments1}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab5primary">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">User Name :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input id="uname" class="form-control fsc-input" placeholder="User Name" value="{{$emp->uname}}" readonly="" name="uname" type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Password :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input placeholder="Password" class="form-control fsc-input" id="password" name="password" value="{{$emp->password}}" readonly="" type="password">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Question 1 : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="question1" id="question1" class="form-control fsc-input" disabled="disabled">
                                                        <option value="">Select</option>
                                                        <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                        <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                        <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                                        <option value="In what city were you born?" selected="selected">In what city were you born?</option>
                                                        <option value="What is the name of your first school?">What is the name of your first school?</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Answer 1 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input name="answer1" value="{{$emp->answer1}}" placeholder="" class="form-control fsc-input" id="answer1" disabled="disabled" type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Question 2 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="question2" id="question2" class="form-control fsc-input" disabled="disabled">
                                                        <option value="">Select</option>
                                                        <option value="What is your favorite movie?">What is your favorite movie?</option>
                                                        <option value="What was the make of your first car?">What was the make of your first car?</option>
                                                        <option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
                                                        <option value="What is your father's middle name?">What is your fathers middle name?</option>
                                                        <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Answer 2 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input name="answer2" value="{{$emp->answer2}}" placeholder="" class="form-control fsc-input" id="answer2" disabled="disabled" type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Question 3 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <select name="question3" id="question3" class="form-control fsc-input" disabled="disabled">
                                                        <option value="">Select</option>
                                                        <option value="What was your high school mascot?">What was your high school mascot?</option>
                                                        <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                                        <option value="In what year was your father born?">In what year was your father born?</option>
                                                        <option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
                                                        <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Answer 3 :</label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input name="answer3" value="{{$emp->answer3}}" placeholder="" class="form-control fsc-input" id="answer3" disabled="disabled" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab6primary">

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Other Information : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <textarea name="other_info" id="other_info" rows="1" class="form-control fsc-input">{{$emp->other_info}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Computer Name : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" name="computer_name" id="computer_name" value="{{$emp->other_info}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                    <label class="fsc-form-label">Computer IP : </label>
                                                </div>
                                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control fsc-input" name="computer_ip" id="computer_ip" value="{{$emp->computer_ip}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="btn-center">
                                        <button type="submit" class="btn btn-primary  fsc-form-submit class=" hvr-shutter-in-horizontal
                                        "">Save</button>&nbsp&nbsp&nbsp
                                        <button type="submit" class="btn btn-primary  fsc-form-submit class=" hvr-shutter-in-horizontal
                                        "">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
function formDay()
{
    for ($i = 1; $i <= 31; $i++) {
        $selected = ($i == date('d')) ? ' ' : '';
        echo '<option' . $selected . ' value="' . $i . '">' . $i . '</option>' . "\n";
    }
}

function formMonth()
{
    $month = strtotime(date('Y') . '-' . date('m') . '-' . date('j') . ' - 0 months');
    $end = strtotime(date('Y') . '-' . date('m') . '-' . date('j') . ' + 12 months');
    while ($month < $end) {
        $selected = (date('M', $month) == date('M')) ? ' ' : '';
        echo '<option' . $selected . ' value="' . date('M', $month) . '">' . date('M', $month) . '</option>' . "\n";
        $month = strtotime("+1 month", $month);
    }
}

function formYear()
{
    for ($i = 1980; $i <= date('Y'); $i++) {
        $selected = ($i == date('Y')) ? '' : '';
        echo '<option' . $selected . ' value="' . $i . '">' . $i . '</option>' . "\n";
    }
}

?>
<script>
    $("#telephoneNo1").mask("(999) 999-9999");
    $(".ext").mask("999");
    $("#ext1").mask("999");
    $("#ext2").mask("999");
    $("#eext1").mask("999");
    $("#eext2").mask("999");
    $("#telephoneNo2").mask("(999) 999-9999");
    $("#mobile_no").mask("(999) 999-9999");
    $(".usapfax").mask("(999) 999-9999");
    $("#etelephone2").mask("(999) 999-9999");
    $("#etelephone1").mask("(999) 999-9999");
    $("#computer_ip").mask("999.999.999.999");
    $("#zip").mask("9999");
    $("#ezipcode").mask("9999");
</script>
<script>
    $('#telephoneNo1Type').on('change', function () {

        if (this.value == 'Office') {
            document.getElementById('ext1').removeAttribute('readonly');
//document.getElementById("ext1").readonly= false; 
        } else {
            document.getElementById('ext1').readOnly = true;
//document.getElementById("ext1").readonly= true;
        }
    })
</script>
<script>
    $('#telephoneNo2Type').on('change', function () {

        if (this.value == 'Office') {
            document.getElementById('ext2').removeAttribute('readonly');
        } else {
            document.getElementById('ext2').readOnly = true;
        }
    })
</script>

<script>
    $('#eteletype2').on('change', function () {

        if (this.value == 'Office') {
            document.getElementById('eext2').removeAttribute('readonly');
        } else {
            document.getElementById('eext2').readOnly = true;
        }
    })
</script>
<script>
    $('#eteletype1').on('change', function () {

        if (this.value == 'Office') {
            document.getElementById('eext1').removeAttribute('readonly');
        } else {
            document.getElementById('eext1').readOnly = true;
        }
    })
</script>

@endsection()