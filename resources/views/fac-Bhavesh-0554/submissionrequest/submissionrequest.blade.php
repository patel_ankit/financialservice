@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .box3 h3 {
            margin-left: 0;
            font-size: 12px;
        }

        label {
            float: left;
        }

        .big-font {
            font-size: 22px;
        }

        .submission-list-box-bg {
        }

        .submission-list-box-bg .main-image {
            width: 153px;
            float: right;
            margin: 10px 3px 11px 0;
            background: #fff;
            padding-left: 10px;
            border-radius: 28px 0 0 28px;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .submission-list-box-bg .box3 {
            background: #c5dbe8;
        }

        .submission-list-box-bg .box3 h2 {
            padding: 32px 7px 24px;
        }

        .page-title {
            padding: 8px 19px !important;
        }

        .imgicon {
            background: #fff;
            display: block;
            margin-top: -6px;
            width: 35px;
            height: 35px;
            float: left;
            margin-right: 10px;
            float: left;
            margin-right: 10px;
            border-radius: 2px;
            padding: 3px;
            border: 1px solid #12186b;
        }

        .imgicon img {
            max-width: 100%;
            height: 27px;
            margin: 0px auto;
            display: block;
        }

        @media only screen and (max-width: 1100px) {
            .box1 h2, .box2 h2, .box3 h2 {
                font-size: 32px !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .col-xs-6.submission-list-box-bg {
                width: 100%;
            !important;
            }
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->

        <section class="content-header page-title" style="height:40px;">
            <div class="" style="">
                <div class="" style="text-align:center;">
                    <span><i style="float:left;" class="fa fa-workmg"><h2><span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Submission-05-05.png" alt="img"></span><b></b></h2></i></span>
                    <h2>Submission List <span class="right_title" style="text-align:right;padding-right: 20px;position: absolute;right: 0;"> View / Edit</span></h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12" style="padding:0px;">
                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                            @endif
                            @if ( session()->has('error') )
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                            @endif

                            @foreach($submission as $sub)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 submission-list-box-bg">
                                    <div class="main-box">
                                        <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                            <?php
                                            $common = DB::table('submitform')->select('submit_id', 'name', 'telephone', 'email')->where('submit_id', '=', $sub->id)->count();
                                            ?>

                                            <h2><?php echo $common;?></h2>
                                        <!-- <h3>{!!$sub->submission_name!!}</h3> -->
                                            <img src="{{asset('public/submission')}}/{{$sub->submission_image}}" alt="" class="main-image"/>
                                            <a href="{{url('/fac-Bhavesh-0554/submissionform')}}/?id={{$sub->id}}" class="btn btn-info information-btn">More info</a>

                                        </div>
                                    </div>
                                </div>
                            @endforeach


                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Service Name</th>
                                        <th>Name</th>
                                        <th>Email</th>

                                        <th>Telephone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

@endsection()