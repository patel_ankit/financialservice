@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: right
        }

        .ui-timepicker-container {
            z-index: 999999 !important
        }

        .content-wrapper {
            z-index: 99999999 !important;
            position: relative;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Edit Submit Form</h1>

        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>

                        <div class="card-body col-md-12">
                            <form method="post" action="{{ route('submissionrequest.update', $submitform->sid)}}" class="form-horizontal" id="submitform" name="submitform" autocomplete="off">
                                {{csrf_field()}}{{method_field('PATCH')}}

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="hidden" name='id' value="{{$submitform->sid}}">
                                                    <?php echo $submitform->submission_name;?>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control" name='name' value="{{$submitform->name}}">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Telephone : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control" name='telephone' value="{{$submitform->telephone}}">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control" name='email' value="{{$submitform->email}}">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Request Form : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <input type="text" class="form-control" name='requestform' value="{{$submitform->requestform}}">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Note : <span class="star-required">&nbsp;&nbsp;</span></label>
                                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                            <div class="row">
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                    <textarea class="form-control" name='note' rows="4" cols="200">{{$submitform->note}}</textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                    <div class="card-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-2">
                                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                            </div>
                                            <div class="col-md-2 row">
                                                <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/submissionrequest')}}">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="" id="Register"></div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->



@endsection()