@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Tax Authorities</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="Branch">
                            <h1>County / City</h1>
                        </div>
                        <br>
                        <form method="post" action="{{route('city.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">State :</label>
                                <div class="col-md-2">
                                    <select name="states" id="states" class="form-control category fsc-input">
                                        <option value="">---State---</option>
                                        <option value="AK">AK</option>
                                        <option value="AS">AS</option>
                                        <option value="AZ">AZ</option>
                                        <option value="AR">AR</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DE">DE</option>
                                        <option value="DC">DC</option>
                                        <option value="FM">FM</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="GU">GU</option>
                                        <option value="HI">HI</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="IA">IA</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="ME">ME</option>
                                        <option value="MH">MH</option>
                                        <option value="MD">MD</option>
                                        <option value="MA">MA</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MS">MS</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                        <option value="NE">NE</option>
                                        <option value="NV">NV</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NY">NY</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="MP">MP</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PW">PW</option>
                                        <option value="PA">PA</option>
                                        <option value="PR">PR</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VT">VT</option>
                                        <option value="VI">VI</option>
                                        <option value="VA">VA</option>
                                        <option value="WA">WA</option>
                                        <option value="WV">WV</option>
                                        <option value="WI">WI</option>
                                        <option value="WY">WY</option>
                                    </select>
                                    @if ($errors->has('authority_name'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('authority_name') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('short_name') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Name of the Tax Authority :</label>
                                <div class="col-md-4">
                                    <select name="tax_authority" id="tax_authority" class="form-control category1 fsc-input">
                                        <option value="">---Name of the Tax Authority---</option>
                                    </select>
                                    @if ($errors->has('short_name'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('short_name') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('type_of_tax') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">County Code :</label>
                                <div class="col-md-2">
                                    <input type="text" name="country_code" id="country_code" class="form-control " placeholder=""/>
                                    @if ($errors->has('type_of_tax'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('type_of_tax') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">County Address :</label>
                                <div class="col-md-4">
                                    <input type="text" name="address" id="address" class="form-control " placeholder="County Address"/>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">City / State / Zip :</label>
                                <div class="col-md-2">
                                    <input type="text" name="city" id="city" class="form-control" placeholder="city"/>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <select name="state" id="state" class="form-control fsc-input">
                                        <option value="">State</option>
                                        <option value="AK">AK</option>
                                        <option value="AS">AS</option>
                                        <option value="AZ">AZ</option>
                                        <option value="AR">AR</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DE">DE</option>
                                        <option value="DC">DC</option>
                                        <option value="FM">FM</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="GU">GU</option>
                                        <option value="HI">HI</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="IA">IA</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="ME">ME</option>
                                        <option value="MH">MH</option>
                                        <option value="MD">MD</option>
                                        <option value="MA">MA</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MS">MS</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                        <option value="NE">NE</option>
                                        <option value="NV">NV</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NY">NY</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="MP">MP</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PW">PW</option>
                                        <option value="PA">PA</option>
                                        <option value="PR">PR</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VT">VT</option>
                                        <option value="VI">VI</option>
                                        <option value="VA">VA</option>
                                        <option value="WA">WA</option>
                                        <option value="WV">WV</option>
                                        <option value="WI">WI</option>
                                        <option value="WY">WY</option>
                                    </select>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('') }}</strong>
                        </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="zip" id="" class="form-control" placeholder="zip"/>
                                    @if ($errors->has('zip'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Telehpone :</label>
                                <div class="col-md-4">
                                    <input type="text" name="telephone" id="telephone" class="form-control num" placeholder="Telephone"/>
                                    @if ($errors->has('telephone'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('telephone') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('website') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Website :</label>
                                <div class="col-md-4">
                                    <input type="text" name="website" id="website" class="form-control " placeholder="website"/>
                                    @if ($errors->has('website'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getcounty1')!!}?id=' + id, function (data) {
                    $('#tax_authority').empty();
                    $('#tax_authority').append('<option value="">---Name of the Tax Authority---</option>');
                    $.each(data, function (index, subcatobj) {
                        $('#tax_authority').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                    })

                });

            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category1', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getcountycode1')!!}?id=' + id, function (data) {
                    $('#country_code').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#country_code').val(subcatobj.countycode);

                    })

                });

            });
        });
    </script>
@endsection()