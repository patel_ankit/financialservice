@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('cservice.store')}}" class="form-horizontal" id="service" name="positionname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Service Name :</label>
                                    <div class="col-md-4">
                                        <input name="service" type="text" id="service" class="form-control" value=""/> @if ($errors->has('service'))
                                            <span class="help-block">
											<strong>{{ $errors->first('service') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group {{ $errors->has('billing') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Billing Contents :</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control fsc-input" rows="3" name="billing" placeholder="Billing Contents"></textarea>
                                        @if ($errors->has('billing'))
                                            <span class="help-block">
											<strong>{{ $errors->first('billing') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-3">
                                            <input class="btn_new_save btn-primary1" type="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/cservice')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()