@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Question</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('question.update',$position->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Question Name :</label>
                                <div class="col-md-4">
                                    <input name="question" type="text" value="{{$position->question}}" id="question" class="form-control"/>
                                    @if ($errors->has('question'))
                                        <span class="help-block">
											<strong>{{ $errors->first('question') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection()