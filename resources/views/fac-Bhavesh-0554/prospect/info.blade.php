@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;
        }

        .box-header {
            padding-top: 0px;
        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>View Of Prospect</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <?php


            $jsonR = json_decode($json);
            //print_r($jsonR);

            foreach($jsonR as $json1):
            $pid = $json1[0]->pid;
            $client_name = $json1[0]->client_name;
            $telephone = $json1[0]->telephone;
            $email = $json1[0]->email;
            $proposal_created = $json1[0]->proposal_created;
            $proposal_day = $json1[0]->proposal_day;
            $proposal_Amount = $json1[0]->fsc_fee;
            $proposal_priority = $json1[0]->priority;
            $send_by = $json1[0]->send_by;

            $service_name = $json1[0]->service_name;

            ?>

            <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px" style="margin:10px auto;" class="img-responsive">
            <hr>
            <h3 class="text-center">Proposal</h3>
            <hr>

            <h1><?php echo $send_by;?></h1>


            <div class="row">
                <div class="col-md-6">
                    <p><?php echo $client_name;?></p>
                    <p><strong>Voice : </strong><?php echo $telephone;?> </p>
                    <p><strong>Fax : </strong><?php echo $telephone;?> </p>
                    <p><strong>Fax : </strong><?php echo $email;?> </p>
                    <p><strong>Proposal To:</strong><?php echo $client_name;?></p>
                    <p> USA </p>
                </div>
                <div class="col-md-6">
                    <p class="text-right"><strong>Proposal No : </strong> <?php echo $pid;?></p>
                    <p class="text-right"><strong>Proposal Date : </strong><?php echo $proposal_created;?></p>
                    <p class="text-right"><strong>Proposal Day : </strong><?php echo $proposal_day;?></p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <th>Priority</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                    <td><?php echo $proposal_priority;?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <thead>
                    <th>Description</th>
                    <th>Amount</th>
                    </thead>
                    <tbody>
                    <td>
                        <?php
                        $count = 0;
                        foreach ($json1[1] as $subJson):
                            foreach ($subJson as $jsonData):
                                echo $jsonData->name . '<br/>';
                            endforeach;
                        endforeach;
                        ?>
                    </td>
                    <td><?php echo $proposal_Amount;?></td>
                    </tbody>
                </table>
            </div>


            <?php    endforeach;

            ?>
        </section>
        <!--</div>-->

@endsection()