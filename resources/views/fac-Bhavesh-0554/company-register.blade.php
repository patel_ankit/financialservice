@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-title">
                            <h3>Company Register</h3>
                            <a href="#">Add New</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="sampleTable3">

                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Business</th>
                                    <th>Business Category</th>
                                    <th>Business Brand</th>
                                    <th>Business Brand Category</th>
                                    <th>Company Name</th>
                                    <th>Mobile No</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Non-Profits-Org</td>
                                    <td>Auto Broker Dealer</td>
                                    <td>Non Brand</td>
                                    <td></td>
                                    <td>KKKK</td>
                                    <td>11111</td>
                                    <td>
                                        <a class="btn-action btn-view-edit" href="#">View/ Edit</a>
                                        <a class="btn-action btn-delete" href="#">Delete</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Personal</td>
                                    <td>Gas Station</td>
                                    <td>Non Brand</td>
                                    <td></td>
                                    <td>KKKK</td>
                                    <td>11111</td>
                                    <td>
                                        <a class="btn-action btn-view-edit" href="#">View/ Edit</a>
                                        <a class="btn-action btn-delete" href="#">Delete</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Business</td>
                                    <td>Gas Station</td>
                                    <td>Non Brand</td>
                                    <td></td>
                                    <td>KKKK</td>
                                    <td>11111</td>
                                    <td>
                                        <a class="btn-action btn-view-edit" href="#">View/ Edit</a>
                                        <a class="btn-action btn-delete" href="#">Delete</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Business</td>
                                    <td>Auto Broker Dealer</td>
                                    <td>Non Brand</td>
                                    <td></td>
                                    <td>KKKK</td>
                                    <td>11111</td>
                                    <td>
                                        <a class="btn-action btn-view-edit" href="#">View/ Edit</a>
                                        <a class="btn-action btn-delete" href="#">Delete</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Business</td>
                                    <td>Auto Broker Dealer</td>
                                    <td>Non Brand</td>
                                    <td></td>
                                    <td>KKKK</td>
                                    <td>123</td>
                                    <td>
                                        <a class="btn-action btn-view-edit" href="#">View/ Edit</a>
                                        <a class="btn-action btn-delete" href="#">Delete</a>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script type="text/javascript">
        $('#sampleTable3').DataTable();
    </script>
@endsection()