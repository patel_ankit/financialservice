@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>City / State / Country</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('citystate.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Zip Code :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="zip code">


                                        @if ($errors->has('zipcode'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('zipcode') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Country :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="country" id="country" class="form-control" placeholder="Country"/>
                                        @if ($errors->has('country'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('country') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">State :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="state" id="state" class="form-control" placeholder="State"/>
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">City :</label>
                                    <div class="col-md-4">
                                        <input type="text" name="city" id="city" class="form-control" placeholder="City"/>
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/citystate')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection()