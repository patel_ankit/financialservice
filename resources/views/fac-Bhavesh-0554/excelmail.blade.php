<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Excel Dawnload</title>
</head>
<body>

<img src="https://financialservicecenter.net/public/frontcss/images/fsc_logo.png" alt="img">
<br><br>
<table border='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;'>
    <tr style='background:#535da6;'>
        <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Excel Dawnload</td>
    </tr>


    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Fullname :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'>

            <p>
                {{$firstName}}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Email :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>

            <p>
                {{ $email }}
            </p>
        </td>
    </tr>


    <tr style='background:#535da6;'>
        <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Excel File : <a href="{{asset($excel)}}" dawnload target="_blank">Dawnload</a>
        </td>
    </tr>
</table>


</body>
</html>