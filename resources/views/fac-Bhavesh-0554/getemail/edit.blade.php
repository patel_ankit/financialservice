@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Edit Email </h1>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('email.update',$leave->id)}}" class="form-horizontal" id="contact" name="content" enctype="multipart/form-data">
                            {{csrf_field()}}{{method_field('PATCH')}}
                            <div class="form-group{{ $errors->has('for_whom') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">For Whom :</label>
                                <div class="col-md-5">
                                    <select name="for_whom" id="for_whom" class="form-control fsc-input">
                                        <option value="">---Select---</option>
                                        @foreach($emp as $e)
                                            <option value="{{$e->id}}" @if($e->id == $leave->for_whom) selected @endif >{{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} ({{ucwords($e->type)}})</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('for_whom'))
                                        <span class="help-block">
											<strong>{{ $errors->first('for_whom') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Email Address :</label>
                                <div class="col-md-5">
                                    <input name="email" type="text" id="email" class="form-control" value="{{$leave->email}}"/>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('access_address') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Access Address :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="access_address" type="text" id="access_address" class="form-control" value="{{$leave->access_address}}">
                                    </div>
                                    @if ($errors->has('access_address'))
                                        <span class="help-block">
											<strong>{{ $errors->first('access_address') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Password :</label>
                                <div class="col-md-5">
                                    <div class="box-body pad">
                                        <input name="password" type="password" id="password" class="form-control" value="{{$leave->password}}">
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn upload-image" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection()