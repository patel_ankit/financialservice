@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .example_filterWrapper {
            width: 500px;
            margin-bottom: 14px;
            position: relative;
        }

        .table > thead > tr > th {
            background: #ffff99;
            border-bottom: 8px solid #993366;
            text-align: center;
        }

        .dataTables_filter .form-control.input-sm {
            min-width: 187px;
            margin-left: 12px;
        }

        .dataTables_filter {
            position: absolute;
            margin-top: -56px;
        }

        .dt-buttons {
            position: absolute;
            margin-left: 86.6%;
            margin-top: -48px;
        }

        div.dataTables_wrapper div.dataTables_filter input {
            width: 75% !important;
        }

        .dt-button buttons-excel buttons-html5 {
            display: none !important;
        }

        .search-btn {
            position: absolute;
            top: 10px;
            right: 16px;
            background: transparent;
            border: transparent;
        }
    </style>
    <style></style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
            display: none !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
            display: none !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;
            display: none !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }

        #example_filter {
            display: none;
        }

        .fa {
            font-size: 16px !important;
        }

        .imgicon {
            background: #fff;
            display: block;
            margin-top: -6px;
            width: 35px;
            float: left;
            height: 35px;
            margin-right: 10px;
            float: left;
            margin-right: 10px;
            border-radius: 2px;
            padding: 3px;
            border: 1px solid #12186b;
        }

        .imgicon img {
            max-width: 100%;
        }

        @media only screen and (max-width: 991px) {
            .add_btn_resp .table-title a {
                margin-top: 5px !important;
            }
        }
    </style>
    <div class="content-wrapper">
        <section class="page-title content-header">
            <h2><span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Message-03.png" alt="img"></span><span style="float:left">FSC</span><span> Telephone Message (In) Logsheet</span><span style="float:right">Admin</span></h2>
            <h6 style="display:none">Telephone Message (In) Logsheet</h6>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <form class="form-horizontal" action="{{url('fac-Bhavesh-0554/msg/search')}}" method="post">

                                {{csrf_field()}}
                                <?php
                                if(isset($_POST['choice']) && $_POST['choice'] != '' )
                                {
                                ?>

                                <input type="hidden" name="choice1" value="<?php echo $_POST['choice'];?>">
                            <?php
                            }
                            ?>
                            <!--             <div class="col-md-3" style="margin-top:10px;">-->
                                <!--    &nbsp;-->
                                <!--</div>-->
                                <div class="col-md-3 cs1"><select name="choice" id="choice" class="form-control">
                                        <option value="New">New</option>
                                        <option value="All">All</option>
                                    </select></div>
                                <div class="col-md-3 cs3" style="">
                                    <table style="width: 100%; margin: 0 auto 0 10px;" cellspacing="0" cellpadding="3" border="0">
                                        <tbody>
                                        <tr id="filter_global">
                                            <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"></td>
                                        </tr>

                                        <tr id="filter_col2" data-column="1" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col3" data-column="2" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="EE / User Id">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col4" data-column="3" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col5" data-column="4" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email Id">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col6" data-column="5" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel. Number">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--         <div class="col-md-2">-->
                                <!--             <input type="submit" class="btn btn-success" name="sub" value="Ok">-->
                                <!--</div>-->

                            </form>
                            <div class="box-tools pull-right add_btn_resp" style="margin-right:0px !important;">
                                <div class="table-title">
                                    <a href="{{url('fac-Bhavesh-0554/msg/create')}}">Add New Message</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th style="width: 7% !important;">Priority</th>
                                        <th style="width:3% !important"> Dt.- Day</br> Time</th>
                                        <th>Message From</th>
                                        <th style="width: 10% !important;">Telephone</th>
                                        <th>Purpose</th>
                                        <th>Message For</th>
                                        <th>Received By</th>
                                        <!--	<th style="width:3% !important">Return.Call/</br> Date/</br> Day / Time</th>!-->
                                        <!--<th>Work Status</th>!-->
                                        <th width="8%;">Status</th>
                                        <th>Task Status</th>
                                        <th style="width: 7%;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $cnt = 0;?>
                                    @foreach($fullmsg as $com)
                                        <?php $cnt++;?>
                                        <?php
                                        if ($com->call_back == '1') {
                                            $callback = 'Regular';
                                        } else if ($com->call_back == '2') {
                                            $callback = 'Important';
                                        } else if ($com->call_back == '3') {
                                            $callback = 'Urgent';
                                        } else {
                                            $callback = '';
                                        }
                                        //echo "<pre>";print_r($com);
                                        ?>
                                        <tr @if($com->status =='2') style="background:#b9f0b2;" @endif
                                        >
                                            <td style="text-align:center;"> {{$cnt}}</td>
                                            <td>{{$callback}} @if($com->call_back=='2') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$callback}}" width="30px"> @elseif($com->call_back=='3')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$callback}}" width="30px"> @endif</td>
                                            <td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
                                            <td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach   @foreach($emp3 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif
                                                <BR>
                                                <?php if ($com->clientfile) {
                                                    echo $com->clientfile;
                                                } else if ($com->type == 'Other Person') {
                                                    echo 'Other Person';
                                                }?>
                                                <br>
                                                <?php if ($com->type == 'Other Person') {
                                                    echo $com->busname;
                                                }?>
                                                <?php if (($com->clientfile) && $com->business_id == '6') {
                                                    echo 'Personal';
                                                } else {
                                                    echo $com->company_name;
                                                }?>

                                            </td>
                                            <td>{{$com->clientno}}</td>
                                            <td>{!!$com->purpose!!}</td>
                                            <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach </td>
                                            <td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
                                        <!--	<td>{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!} </td>!-->
                                        <!--<td>{{$com->status1}}</td>!-->
                                        <!--	<td>{{$com->status1}} @if($com->checkstatus=='1')   <div class="gif">
                                    <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
                                    </div>  @endif</td>!-->
                                            <td><?php if($com->status == '2' && $com->status1 == '')  {?> <img src="{{asset('public/dashboard/images/newimage.gif')}}" style="display:initial;" alt="" class="img-responsive"><?php echo 'New'; } else {
                                                    echo 'All';
                                                }?></td>
                                            <td>{{$com->status1}}</td>

                                            <td class="text-center"><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}">
                                                    <i class="fa fa-edit"></i></a>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->id)}}"><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <!--
							@foreach($task1 as $com)
                                        <tr>
                                            <td></td>
                                            <td>{{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1)  @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td> @foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td  style="">{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
									<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                    @foreach($task2 as $com)
                                        <tr>
                                            <td> </td>
                                            <td> {{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td> {{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1)  @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person')  {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach </td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a><a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
 {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                    @foreach($task3 as $com)
                                        <tr >
                                            <td></td>
                                            <td>{{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td  style="">{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
									<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a> <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{route('msg.destroy',$com->id)}}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                            !-->
                                    </tbody>
                                </table>
                            <!--		{!!$fullmsg->render();!!}!-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            (function ($) {
                function calcDisableClasses(oSettings) {
                    var start = oSettings._iDisplayStart;
                    var length = oSettings._iDisplayLength;
                    var visibleRecords = oSettings.fnRecordsDisplay();
                    var all = length === -1;

                    // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
                    var page = all ? 0 : Math.ceil(start / length);
                    var pages = all ? 1 : Math.ceil(visibleRecords / length);

                    var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
                    var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

                    return {
                        'first': disableFirstPrevClass,
                        'previous': disableFirstPrevClass,
                        'next': disableNextLastClass,
                        'last': disableNextLastClass
                    };
                }

                function calcCurrentPage(oSettings) {
                    return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
                }

                function calcPages(oSettings) {
                    return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
                }

                var firstClassName = 'first';
                var previousClassName = 'previous';
                var nextClassName = 'next';
                var lastClassName = 'last';

                var paginateClassName = 'paginate';
                var paginatePageClassName = 'paginate_page';
                var paginateInputClassName = 'paginate_input';
                var paginateTotalClassName = 'paginate_total';

                $.fn.dataTableExt.oPagination.input = {
                    'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                        var nFirst = document.createElement('span');
                        var nPrevious = document.createElement('span');
                        var nNext = document.createElement('span');
                        var nLast = document.createElement('span');
                        var nInput = document.createElement('input');
                        var nTotal = document.createElement('span');
                        var nInfo = document.createElement('span');

                        var language = oSettings.oLanguage.oPaginate;
                        var classes = oSettings.oClasses;
                        var info = language.info || 'Page _INPUT_ of _TOTAL_';

                        nFirst.innerHTML = language.sFirst;
                        nPrevious.innerHTML = language.sPrevious;
                        nNext.innerHTML = language.sNext;
                        nLast.innerHTML = language.sLast;

                        nFirst.className = firstClassName + ' ' + classes.sPageButton;
                        nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                        nNext.className = nextClassName + ' ' + classes.sPageButton;
                        nLast.className = lastClassName + ' ' + classes.sPageButton;

                        nInput.className = paginateInputClassName;
                        nTotal.className = paginateTotalClassName;

                        if (oSettings.sTableId !== '') {
                            nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                            nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                            nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                            nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                            nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                        }

                        nInput.type = 'text';

                        info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                        info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                        nInfo.innerHTML = '<span>' + info + '</span>';

                        nPaging.appendChild(nFirst);
                        nPaging.appendChild(nPrevious);
                        $(nInfo).children().each(function (i, n) {
                            nPaging.appendChild(n);
                        });
                        nPaging.appendChild(nNext);
                        nPaging.appendChild(nLast);

                        $(nFirst).click(function () {
                            var iCurrentPage = calcCurrentPage(oSettings);
                            if (iCurrentPage !== 1) {
                                oSettings.oApi._fnPageChange(oSettings, 'first');
                                fnCallbackDraw(oSettings);
                            }
                        });

                        $(nPrevious).click(function () {
                            var iCurrentPage = calcCurrentPage(oSettings);
                            if (iCurrentPage !== 1) {
                                oSettings.oApi._fnPageChange(oSettings, 'previous');
                                fnCallbackDraw(oSettings);
                            }
                        });

                        $(nNext).click(function () {
                            var iCurrentPage = calcCurrentPage(oSettings);
                            if (iCurrentPage !== calcPages(oSettings)) {
                                oSettings.oApi._fnPageChange(oSettings, 'next');
                                fnCallbackDraw(oSettings);
                            }
                        });

                        $(nLast).click(function () {
                            var iCurrentPage = calcCurrentPage(oSettings);
                            if (iCurrentPage !== calcPages(oSettings)) {
                                oSettings.oApi._fnPageChange(oSettings, 'last');
                                fnCallbackDraw(oSettings);
                            }
                        });

                        $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                            // 38 = up arrow, 39 = right arrow
                            if (e.which === 38 || e.which === 39) {
                                this.value++;
                            }
                            // 37 = left arrow, 40 = down arrow
                            else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                                this.value--;
                            }

                            if (this.value === '' || this.value.match(/[^0-9]/)) {
                                /* Nothing entered or non-numeric character */
                                this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                                return;
                            }

                            var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                            if (iNewStart < 0) {
                                iNewStart = 0;
                            }
                            if (iNewStart >= oSettings.fnRecordsDisplay()) {
                                iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                            }

                            oSettings._iDisplayStart = iNewStart;
                            oSettings.oInstance.trigger("page.dt", oSettings);
                            fnCallbackDraw(oSettings);
                        });

                        // Take the brutal approach to cancelling text selection.
                        $('span', nPaging).bind('mousedown', function () {
                            return false;
                        });
                        $('span', nPaging).bind('selectstart', function () {
                            return false;
                        });

                        // If we can't page anyway, might as well not show it.
                        var iPages = calcPages(oSettings);
                        if (iPages <= 1) {
                            $(nPaging).hide();
                        }
                    },

                    'fnUpdate': function (oSettings) {
                        if (!oSettings.aanFeatures.p) {
                            return;
                        }

                        var iPages = calcPages(oSettings);
                        var iCurrentPage = calcCurrentPage(oSettings);

                        var an = oSettings.aanFeatures.p;
                        if (iPages <= 1) // hide paging when we can't page
                        {
                            $(an).hide();
                            return;
                        }

                        var disableClasses = calcDisableClasses(oSettings);

                        $(an).show();

                        // Enable/Disable `first` button.
                        $(an).children('.' + firstClassName)
                            .removeClass(oSettings.oClasses.sPageButtonDisabled)
                            .addClass(disableClasses[firstClassName]);

                        // Enable/Disable `prev` button.
                        $(an).children('.' + previousClassName)
                            .removeClass(oSettings.oClasses.sPageButtonDisabled)
                            .addClass(disableClasses[previousClassName]);

                        // Enable/Disable `next` button.
                        $(an).children('.' + nextClassName)
                            .removeClass(oSettings.oClasses.sPageButtonDisabled)
                            .addClass(disableClasses[nextClassName]);

                        // Enable/Disable `last` button.
                        $(an).children('.' + lastClassName)
                            .removeClass(oSettings.oClasses.sPageButtonDisabled)
                            .addClass(disableClasses[lastClassName]);

                        // Paginate of N pages text
                        $(an).find('.' + paginateTotalClassName).html(iPages);

                        // Current page number input value
                        $(an).find('.' + paginateInputClassName).val(iCurrentPage);
                    }
                };
            })(jQuery);
        </script>
        <!--<script>
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                dom: 'Bfrtlip',
                "pagingType": "input",
            "columnDefs": [ {
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                } ],


            } );
        });

               </script>
        !-->
        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {

                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();

                table.columns(8)
                    .search('^(?:(?!All).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'All') {
                        table.columns(8).search(_val).draw();
                    } else if (_val == 'New') {  //alert();
                        table.columns(8).search(_val).draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });

            function filterGlobal() {
                $('#example').DataTable().search(
                    $('#global_filter').val(),
                    $('#global_regex').prop('checked'),
                    $('#global_smart').prop('checked')
                ).draw();
            }

        </script>

@endsection()