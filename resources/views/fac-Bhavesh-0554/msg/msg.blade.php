@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .example_filterWrapper {
            width: 500px;
            margin-bottom: 14px;
            position: relative;
        }

        .table > thead > tr > th {
            background: #ffff99;
            border-bottom: 8px solid #993366;
            text-align: center;
        }

        .dataTables_filter .form-control.input-sm {
            min-width: 187px;
            margin-left: 12px;
        }

        .dataTables_filter {
            position: absolute;
            margin-top: -56px;
        }

        .dt-buttons {
            position: absolute;
            margin-left: 86.6%;
            margin-top: -48px;
        }

        div.dataTables_wrapper div.dataTables_filter input {
            width: 75% !important;
        }

    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }

        .imgicon {
            background: #fff;
            display: block;
            margin-top: -6px;
            width: 35px;
            float: left;
            height: 35px;
            margin-right: 10px;
            float: left;
            margin-right: 10px;
            border-radius: 2px;
            padding: 3px;
            border: 1px solid #12186b;
        }

        .imgicon img {
            max-width: 100%;
        }
    </style>
    <div class="content-wrapper">
        <section class="page-title content-header">
            <h2><span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Message-03.png" alt="img"></span><span style="float:left">FSC</span><span> Telephone Message (In) Logsheet</span><span style="float:right">Admin</span></h2>
            <h6 style="display:none">Telephone Message (In) Logsheet</h6>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="col-md-3" style="margin-left:255px;">
                                <div class="row">
                                    <!--<div class="col-md-3" style="margin-top:10px;"><label style="margin-left:28%">&nbsp;</label></div>-->
                                    <div class="col-md-9">
                                        <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                                            <option value="New">New</option>

                                            <option value="All">All</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tools pull-right" style="margin-top: 8px;position: absolute;margin-right:150px !important;">
                                <div class="table-title">
                                    <a href="{{url('fac-Bhavesh-0554/msg/create')}}">Add New Message</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Priority 111</th>
                                        <th style="width:3% !important"> Date</br> Day</br> Time</th>
                                        <th>Message From</th>
                                        <th style="width: 12% !important;">Contact No.</th>
                                        <th>Purpose</th>
                                        <th>For Whom?</th>
                                        <th>Received By</th>
                                        <!--	<th style="width:3% !important">Return.Call/</br> Date/</br> Day / Time</th>!-->
                                        <!--<th>Work Status</th>!-->
                                        <th>Status</th>
                                        <th style="width: 85px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($fullmsg as $com)
                                        <?php //echo "<pre>";print_r($com);?>
                                        <tr @if($com->status =='2') style="background:#b9f0b2;" @endif
                                        @if($com->status=='2' && $com->status1 !='Done')@endif>
                                            <td style="text-align:center;"></td>
                                            <td>{{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
                                            <td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
                                            <td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach   @foreach($emp3 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif
                                                <BR>
                                                <?php if ($com->clientfile) {
                                                    echo $com->clientfile;
                                                } else if ($com->type == 'Other Person') {
                                                    echo 'Other Person';
                                                }?>
                                                <br>
                                                <?php if ($com->type == 'Other Person') {
                                                    echo $com->busname;
                                                }?>
                                                <?php if (($com->clientfile) && $com->business_id == '6') {
                                                    echo 'Personal';
                                                } else {
                                                    echo $com->company_name;
                                                }?>

                                            </td>
                                            <td>{{$com->clientno}}</td>
                                            <td>{!!$com->purpose!!}</td>
                                            <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach </td>
                                            <td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
                                        <!--	<td>{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!} </td>!-->
                                        <!--<td>{{$com->status1}}</td>!-->
                                        <!--	<td>{{$com->status1}} @if($com->checkstatus=='1')   <div class="gif">
                                    <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive">
                                    </div>  @endif</td>!-->
                                            <td><span style="display:none;">{{$com->status1}}</span><?php if($com->status == '2' && $com->status1 == '')  {?> <img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" class="img-responsive"><?php echo 'New'; } else if ($com->status == '1') {
                                                    echo 'All';
                                                }?></td>


                                            <td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}">
                                                    <i class="fa fa-edit"></i></a>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->id)}}"><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <!--
							@foreach($task1 as $com)
                                        <tr>
                                            <td></td>
                                            <td>{{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1)  @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td> @foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td  style="">{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
									<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                    @foreach($task2 as $com)
                                        <tr>
                                            <td> </td>
                                            <td> {{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td> {{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1)  @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person')  {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach </td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a><a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
 {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{ route('msg.destroy',$com->id) }}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                    @foreach($task3 as $com)
                                        <tr >
                                            <td></td>
                                            <td>{{$com->call_back}} @if($com->call_back=='Important') <img src="{{URL::asset('public/img/Blinking_warning.gif')}}" alt="{{$com->call_back}}" width="30px"> @elseif($com->call_back=='Urgent')  <img src="{{URL::asset('public/img/giphy.gif')}}" alt="{{$com->call_back}}" width="30px"> @endif</td>
									<td>{{$com->date}}<br> {{$com->day}} <br> {{$com->time}}</td>
									<td>@foreach($emp2 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='employee')) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach @foreach($emp3 as $com1) @if(($com->admin_id==$com1->id) && ($com->title=='Active')) {{ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name)}} @endif @endforeach @if($com->type=='Other Person') {{$com->clientname}} @endif</td>
									<td>{{$com->clientno}}</td>
									<td>{!!$com->purpose!!}</td>
								    <td>@foreach($emp2 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td>@foreach($emp1 as $com1) @if($com->employee_id==$com1->id) {{ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)}} @endif @endforeach</td>
									<td  style="">{!!$com->returndate!!}<br> {!!$com->returnday!!}<br>{!!$com->returntime!!}  </td>
									<td>{!!$com->status1!!}</td>
									<td><a class="btn-action btn-view-edit" href="{{route('msg.edit',$com->id)}}"><i class="fa fa-edit"></i></a> <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
                                        <form action="{{route('msg.destroy',$com->id)}}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                            </td>
                                        </tr>
@endforeach
                                            !-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {

                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();

                table.columns(8)
                    .search('^(?:(?!All).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'All') {
                        table.columns(8).search(_val).draw();
                    } else if (_val == 'New') {  //alert();
                        table.columns(8).search(_val).draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>
@endsection()