@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Business Category</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" class="form-horizontal" id="" action="{{route('businesscategory.store')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('bussiness_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="bussiness_name" name="bussiness_name">
                                            <option value=''>Please select Business</option>
                                            @foreach($business as $bus)
                                                <option value='{{$bus->id}}'>{{$bus->bussiness_name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('bussiness_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bussiness_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('business_cat_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category :</label>
                                    <div class="col-md-4">
                                        <input name="business_cat_name" type="text" id="business_cat_name" class="form-control" value=""/> @if ($errors->has('business_cat_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_cat_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="naicss">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">NAICS / SIC Code: </label>
                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="padding-left:5px;">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input naics" id="" name="naics" placeholder="NAICS" value="">
                                                    </div>

                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                        <div class="dropdown" style="margin-top: 1%;">
                                                            <input type="text" class="form-control fsc-input sic" placeholder="SIC" id="" name="sic" value="">

                                                        </div>
                                                    </div>

                                                    <!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                         <input type="text" class="form-control fsc-input" placeholder="Search" id="" name="" value="">
                                                    </div>!-->

                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <!--<input type="button" class="add-row btn btn-primary" value="Go to Link">-->
                                                        <a href="https://www.naics.com/code-search/" target="_blank" class="add-row btn btn-primary">Go to Link</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group {{ $errors->has('business_cat_image') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category Image :</label>
                                    <div class="col-md-4">
                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="business_cat_image" style="opecity:0" placeholder="Upload Service Image" id="business_cat_image" type="file">
                                        </label>


                                        @if ($errors->has('business_cat_image'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_cat_image') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Url :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" value="" id="link" class="form-control"/>
                                        @if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/businesscategory')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(".naics").mask("999999");
        $(".sic").mask("9999");


    </script>
@endsection()