@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .business-category-header-box-bg {
            border-top: solid 3px #00a65a;
            margin-top: 10px;
            padding: 0px 0 0px 0;
            text-align: center;
        }

        .business-category-header-box-bg h3 {
            margin-top: 10px;
            background: #D6EBFA !important;
            margin-top: 0;
            padding: 10px;
        }

        .business-category-header-box-bg .table > thead > tr > th {
            padding: 3px 0 !important;
        }

        .business-category-header-box-bg .table > tbody > tr > td {
            padding: 10px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Business Category</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" class="form-horizontal" id="" action="{{ route('businesscategory.update',$category->cid) }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('bussiness_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="bussiness_name" name="bussiness_name">
                                            @foreach($business as $bus)
                                                @if($category->buss == $bus->id)
                                                    <option value='{{$category->buss}}' selected>{{$category->bussiness_name}}</option>
                                                @else
                                                    <option value='{{$bus->id}}'>{{$bus->bussiness_name}}</option>

                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('bussiness_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bussiness_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('business_cat_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category :</label>
                                    <div class="col-md-4">
                                        <input name="business_cat_name" type="text" value="{{$category->business_cat_name}}" id="business_cat_name" class="form-control"/>
                                        @if ($errors->has('business_cat_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_cat_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="naicss">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">NAICS / SIC Code: </label>
                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="padding-left:5px;">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input naics" id="" name="naics" placeholder="NAICS" value="<?php if (isset($category->naics)) {
                                                            echo $category->naics;
                                                        }?>">
                                                    </div>

                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                        <div class="dropdown" style="margin-top: 1%;">
                                                            <input type="text" class="form-control fsc-input sic" placeholder="SIC" id="" name="sic" value="<?php if (isset($category->sic)) {
                                                                echo $category->sic;
                                                            }?>">

                                                        </div>
                                                    </div>

                                                    <!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                         <input type="text" class="form-control fsc-input" placeholder="Search" id="" name="" value="">
                                                    </div>!-->

                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <!--<input type="button" class="add-row btn btn-primary" value="Go to Link">-->
                                                        <a href="https://www.naics.com/code-search/" target="_blank" class="add-row btn btn-primary">Go to Link</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group {{ $errors->has('business_cat_image') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category Image :</label>
                                    <div class="col-md-4">


                                        <label class="file-upload btn btn-primary" style=" margin-right:10px; ">
                                            Browse for file ... <input name="business_cat_image" style="opecity:0" placeholder="Upload Service Image" id="business_cat_image" type="file">
                                        </label>
                                        <img src="https://financialservicecenter.net/public/category/{{$category->business_cat_image}}" title="{{$category->business_cat_name}}" alt="{{$category->business_cat_name}}" width="100px">
                                    </div>
                                </div>

                                <input type="hidden" name="business_cat_image1" id="business_cat_image1" value="{{$category->business_cat_image}}">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Url :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" value="{{$category->link}}" id="link" class="form-control"/>@if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="Branch" style="text-align:left;padding-left: 15px;">
                                        <h1 class="text-center">License Information</h1>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php $strposlink = $category->licenses; $splittedstringposlink = explode(",", $strposlink);?>
                                    <label class="control-label col-md-3"> Require Licenses :</label>
                                    <div class="col-md-4">
                                        <select class="js-example-tags form-control" style="width:86.5%" name="licenses[]" id="vendor_product" multiple="multiple">
                                            @foreach($commonuser as $subcustomer1)
                                                <option value="{{$subcustomer1->id}}" @foreach($splittedstringposlink as $key => $value)
                                                @if($value ==$subcustomer1->id) selected @endif @endforeach >{{$subcustomer1->licensename}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="Branch" style="text-align:left;padding-left: 15px;">
                                        <h1 class="text-center">Sales Information</h1>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Sales Tax Require :</label>
                                    <div class="col-md-2" style="width: 13%;">
                                        <select class="form-control select_tax">
                                            <option>Select</option>
                                            <option value="y_tax">Yes</option>
                                            <option value="n_tax">No</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-3 show_sheet" style="width: 22%;"> Type of Sales Sheet :</label>
                                    <div class="col-md-2 show_sheet">
                                        <select class="form-control select_sheet">
                                            <option>Select</option>
                                            <option value="s_sheet1">General</option>
                                            <option value="s_sheet2">Liquor Store</option>
                                            <option value="s_sheet3">Conv. Store</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row general_sales">
                                    <div class="col-md-12">
                                        <div class="Branch" style="text-align:left;padding-left: 15px;">
                                            <h1 class="text-center">General Sales Tax Worksheet</h1>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-6"> Sales :</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-6"> Sales Tax Collected :</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-6" style="border-left: 8px solid #bcbcbc;">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Cash :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Credit Card :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Other :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" placeholder="Other Method...">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Adjustment :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" placeholder="Adjustment of...">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6" style="border-top: 2px solid #808080;padding-top: 10px;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Total :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="border-left: 8px solid #bcbcbc;border-top: 2px solid #808080;padding-top: 10px;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Total Receipt :</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div>
                                    <hr style="border-top: 2px solid #808080;margin-top:0px;">
                                    <div class="col-md-12">
                                        <label class="control-label col-md-5"> Difference :</label>
                                        <div class="col-md-2" style="width:13%;">
                                            <input type="text" class="form-control text-center diff" value="0" style="color:#ffffff;background:#00a65a !important;" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row liquor_sales">
                                    <div class="col-md-12">
                                        <div class="Branch" style="text-align:left;padding-left: 15px;">
                                            <h1 class="text-center">Liquor Sales Tax Worksheet</h1>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-4">
                                            <input type="radio" class="sum_show" id="summary" name="client" value="">
                                            <label for="summary" class="sum_show" style="margin-left:5px;">Summary</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" class="detail_show" id="detail" name="client" value="">
                                            <label for="detail" class="detail_show" style="margin-left:5px;">Detail</label>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div>
                                    <hr>
                                    <div class="show_liquer">
                                        <div class="summ">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Sales :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Sales Tax Collected :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Beer :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Wine :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Sales Tax Collected :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="border-left: 8px solid #bcbcbc;">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Cash :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Credit Card :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Other :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="Other Method...">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Adjustment :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="Adjustment of...">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6" style="border-top: 2px solid #808080;padding-top: 10px;">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"> Total :</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="border-left: 8px solid #bcbcbc;border-top: 2px solid #808080;padding-top: 10px;">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Total Receipt :</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear clearfix"></div>
                                        <hr style="border-top: 2px solid #808080;margin-top:0px;">
                                        <div class="col-md-12">
                                            <label class="control-label col-md-5"> Difference :</label>
                                            <div class="col-md-2" style="width:13%;">
                                                <input type="text" class="form-control text-center diff" value="0" style="color:#ffffff;background:#00a65a !important;" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row conv_sales">
                                    <div class="col-md-12">
                                        <div class="Branch" style="text-align:left;padding-left: 15px;">
                                            <h1 class="text-center">Conv. Sales Tax Worksheet</h1>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-6"> Sales :</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-6"> Sales Tax Collected :</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-6" style="border-left: 8px solid #bcbcbc;">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Cash :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Credit Card :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Other :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" placeholder="Other Method...">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Adjustment :</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" placeholder="Adjustment of...">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6" style="border-top: 2px solid #808080;padding-top: 10px;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6"> Total :</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="border-left: 8px solid #bcbcbc;border-top: 2px solid #808080;padding-top: 10px;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Total Receipt :</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control only_num text-right" placeholder="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div>
                                    <hr style="border-top: 2px solid #808080;margin-top:0px;">
                                    <div class="col-md-12">
                                        <label class="control-label col-md-5"> Difference :</label>
                                        <div class="col-md-2" style="width:13%;">
                                            <input type="text" class="form-control text-center diff" value="0" style="color:#ffffff;background:#00a65a !important;" readonly>
                                        </div>
                                    </div>
                                </div>

                                <!--	<div class="business-category-header-box-bg">
                                        <h3>License needed for this type of Business</h3>
                                        <a class="btn-action btn-view-edit pull-right" href="">Add <i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <table class="table table-hover table-bordered dataTable no-footer" id="example" role="grid" aria-describedby="example_info">
                                           <thead>
                                              <tr role="row">
                                                 <th style="border: 1px solid;" class="sorting_disabled" rowspan="1" colspan="1">State</th>
                                                 <th style="border: 1px solid;" class="sorting_disabled" rowspan="1" colspan="1">Departement</th>
                                                 <th style="border: 1px solid;" class="sorting_disabled" rowspan="1" colspan="1">Type of License</th>
                                                 <th style="border: 1px solid;" class="sorting_disabled" rowspan="1" colspan="1">Renew Option</th>

                                              </tr>
                                           </thead>
                                           <tbody>
                                              <tr role="row" class="odd">
                                                 <td  style="width:100px ">
                                                    <select class="form-control" id="" name="">
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                    </select>
                                                 </td>
                                                 <td>
                                                    <select class="form-control" id="" name="">
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                    </select>
                                                 </td>
                                                 <td>
                                                    <select class="form-control" id="" name="">
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                        <option value="">Select</option>
                                                    </select>
                                                 </td>
                                                 <td>Test</td>

                                              </tr>
                                           </tbody>
                                        </table>
                                    </div>
                                    !-->
                                <hr>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/businesscategory')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(".summ").hide();
            $(".show_liquer").hide();
            $(".detail").hide();
            $('.sum_show').click(function (e) {
                $(".summ").show();
                $(".show_liquer").show();
                $(".detail").hide();
            });
            $('.detail_show').click(function (e) {
                $(".summ").hide();
                $(".show_liquer").show();
                $(".detail").show();
            });


            $(".show_sheet").hide();
            $(".general_sales").hide();
            $(".liquor_sales").hide();
            $(".conv_sales").hide();
            $(document).ready(function () {
                $('.select_tax').on('change', function () {
                    if (this.value == 'y_tax') {
                        $(".show_sheet").show();
                    } else {
                        $(".show_sheet").hide();
                        $(".general_sales").hide();
                        $(".liquor_sales").hide();
                    }
                });
                $('.select_sheet').on('change', function () {
                    if (this.value == 's_sheet1') {
                        $(".general_sales").show();
                    } else {
                        $(".general_sales").hide();
                    }
                });
                $('.select_sheet').on('change', function () {
                    if (this.value == 's_sheet2') {
                        $(".liquor_sales").show();
                    } else {
                        $(".liquor_sales").hide();
                    }
                });
                $('.select_sheet').on('change', function () {
                    if (this.value == 's_sheet3') {
                        $(".conv_sales").show();
                    } else {
                        $(".conv_sales").hide();
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('.js-example-tags').select2({
                    tags: true,
                    tokenSeparators: [",", " "]
                });
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();
                table.columns(6)
                    .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'Inactive') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'New') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'Active') {  //alert();
                        table.columns(6).search(_val).draw();
                        table.columns(6)
                            .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                            .draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>
        <script>
            $(".naics").mask("999999");
            $(".sic").mask("9999");


        </script>

@endsection()