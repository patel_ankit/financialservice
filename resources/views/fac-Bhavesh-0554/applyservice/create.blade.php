@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Employment</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('employment.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Position Name :</label>
                                <div class="col-md-8">
                                    <input name="position_name" type="text" id="position_name" class="form-control" value=""/>
                                    @if ($errors->has('position_name'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('position_name') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Country:</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <select name="country" id="country" class="form-control fsc-input">
                                            <option value="">---Select Country---</option>
                                            <option value='USA'>USA</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('country') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">State :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <select name="state" id="state" class="form-control fsc-input">
                                            <option value="">---Select State---</option>
                                            <option value='AK'>AK</option>
                                            <option value='AS'>AS</option>
                                            <option value='AZ'>AZ</option>
                                            <option value='AR'>AR</option>
                                            <option value='CA'>CA</option>
                                            <option value='CO'>CO</option>
                                            <option value='CT'>CT</option>
                                            <option value='DE'>DE</option>
                                            <option value='DC'>DC</option>
                                            <option value='FM'>FM</option>
                                            <option value='FL'>FL</option>
                                            <option value='GA'>GA</option>
                                            <option value='GU'>GU</option>
                                            <option value='HI'>HI</option>
                                            <option value='ID'>ID</option>
                                            <option value='IL'>IL</option>
                                            <option value='IN'>IN</option>
                                            <option value='IA'>IA</option>
                                            <option value='KS'>KS</option>
                                            <option value='KY'>KY</option>
                                            <option value='LA'>LA</option>
                                            <option value='ME'>ME</option>
                                            <option value='MH'>MH</option>
                                            <option value='MD'>MD</option>
                                            <option value='MA'>MA</option>
                                            <option value='MI'>MI</option>
                                            <option value='MN'>MN</option>
                                            <option value='MS'>MS</option>
                                            <option value='MO'>MO</option>
                                            <option value='MT'>MT</option>
                                            <option value='NE'>NE</option>
                                            <option value='NV'>NV</option>
                                            <option value='NH'>NH</option>
                                            <option value='NJ'>NJ</option>
                                            <option value='NM'>NM</option>
                                            <option value='NY'>NY</option>
                                            <option value='NC'>NC</option>
                                            <option value='ND'>ND</option>
                                            <option value='MP'>MP</option>
                                            <option value='OH'>OH</option>
                                            <option value='OK'>OK</option>
                                            <option value='OR'>OR</option>
                                            <option value='PW'>PW</option>
                                            <option value='PA'>PA</option>
                                            <option value='PR'>PR</option>
                                            <option value='RI'>RI</option>
                                            <option value='SC'>SC</option>
                                            <option value='SD'>SD</option>
                                            <option value='TN'>TN</option>
                                            <option value='TX'>TX</option>
                                            <option value='UT'>UT</option>
                                            <option value='VT'>VT</option>
                                            <option value='VI'>VI</option>
                                            <option value='VA'>VA</option>
                                            <option value='WA'>WA</option>
                                            <option value='WV'>WV</option>
                                            <option value='WI'>WI</option>
                                            <option value='WY'>WY</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">City :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <input name="city" type="text" id="city" class="form-control txtOnly">
                                    </div>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Description :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                                    </div>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Date :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <input name="date" type="text" id="date" class="form-control" readonly="readonly">
                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Job Type :</label>
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <select name="type" id="type" class="form-control fsc-input">
                                            <option value="">---Select Post Type---</option>
                                            <option value='Full Time'>Full Time</option>
                                            <option value='Part Time'>Part Time</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                                <!--<label class="control-label col-md-3">Job Url :</label>-->
                                <div class="col-md-8">
                                    <div class="box-body pad">
                                        <input name="link" type="hidden" id="link" class="form-control" value="http://financialservicecenter.net/admin/employment/create">
                                    </div>
                                    @if ($errors->has('link'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('link') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn upload-image" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace('editor1');
    </script>
    <script>
        $(document).ready(function () {
            var dateInput = $('input[name="date"]'); // Our date input has the name "date"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
                format: 'M/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: truncateDate(new Date()) // <-- THIS WORKS
            });

            $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
        });

        function truncateDate(date) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }
    </script>
@endsection()