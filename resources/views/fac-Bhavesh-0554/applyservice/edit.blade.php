@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service Inquiry</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">

                        <div class="col-md-12">
                            <form method="post" action="{{route('applyservice.update',$service->cid)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <input type="hidden" name="serviceid" id="serviceid" value="{{$service->serviceid}}">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}" style="margin-top: 4%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Name :<span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="width: 108px;">
                                        <select class="form-control fsc-input" id="nametype" name="nametype">
                                            <option value="">Select</option>
                                            <option value="mr" @if($service->nametype=='mr') selected @endif>Mr.</option>
                                            <option value="mrs" @if($service->nametype=='mrs') selected @endif>Mrs.</option>
                                            <option value="miss" @if($service->nametype=='miss') selected @endif>Miss</option>
                                        </select>

                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" name="firstName" value="{{$service->firstName}}" class="form-control txtOnly fsc-input" id="firstName" placeholder="First Name">
                                        @if ($errors->has('firstName'))
                                            <span class="help-block">
											<strong>{{ $errors->first('firstName') }}</strong>
										</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" class="form-control txtOnly fsc-input" value="{{$service->middleName}}" name="middleName" id="middleName" placeholder="Middle Name">
                                        @if ($errors->has('middleName'))
                                            <span class="help-block">
											<strong>{{ $errors->first('middleName') }}</strong>
										</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" name="lastName" class="txtOnly form-control fsc-input" value="{{$service->lastName}}" id="lastName" placeholder="Last Name">
                                        @if ($errors->has('lastName'))
                                            <span class="help-block">
											<strong>{{ $errors->first('lastName') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Address :
                                            <span class="star-required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="text" class="form-control fsc-input" value="{{$service->address}}" id="address" name="address" placeholder="Residential / Office Address">
                                        @if ($errors->has('address'))
                                            <span class="help-block">
											<strong>{{ $errors->first('address') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('city') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">City / State / Zip :
                                            <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="text" class="form-control fsc-input txtOnly" value="{{$service->city}}" id="city" name="city" placeholder="City">
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
                                                @endif
                                            </div>

                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin {{ $errors->has('stateId') ? ' has-error' : '' }}">
                                                <div class="dropdown" style="margin-top: 1%;">
                                                    <select name="stateId" id="stateId" class="form-control fsc-input">
                                                        @if(!empty($service->stateId))
                                                            <option value="{{$service->stateId}}">{{$service->stateId}}</option>
                                                        @else
                                                            <option value="">Please Select State</option>
                                                        @endif
                                                        <option value='AK'>AK</option>
                                                        <option value='AS'>AS</option>
                                                        <option value='AZ'>AZ</option>
                                                        <option value='AR'>AR</option>
                                                        <option value='CA'>CA</option>
                                                        <option value='CO'>CO</option>
                                                        <option value='CT'>CT</option>
                                                        <option value='DE'>DE</option>
                                                        <option value='DC'>DC</option>
                                                        <option value='FM'>FM</option>
                                                        <option value='FL'>FL</option>
                                                        <option value='GA'>GA</option>
                                                        <option value='GU'>GU</option>
                                                        <option value='HI'>HI</option>
                                                        <option value='ID'>ID</option>
                                                        <option value='IL'>IL</option>
                                                        <option value='IN'>IN</option>
                                                        <option value='IA'>IA</option>
                                                        <option value='KS'>KS</option>
                                                        <option value='KY'>KY</option>
                                                        <option value='LA'>LA</option>
                                                        <option value='ME'>ME</option>
                                                        <option value='MH'>MH</option>
                                                        <option value='MD'>MD</option>
                                                        <option value='MA'>MA</option>
                                                        <option value='MI'>MI</option>
                                                        <option value='MN'>MN</option>
                                                        <option value='MS'>MS</option>
                                                        <option value='MO'>MO</option>
                                                        <option value='MT'>MT</option>
                                                        <option value='NE'>NE</option>
                                                        <option value='NV'>NV</option>
                                                        <option value='NH'>NH</option>
                                                        <option value='NJ'>NJ</option>
                                                        <option value='NM'>NM</option>
                                                        <option value='NY'>NY</option>
                                                        <option value='NC'>NC</option>
                                                        <option value='ND'>ND</option>
                                                        <option value='MP'>MP</option>
                                                        <option value='OH'>OH</option>
                                                        <option value='OK'>OK</option>
                                                        <option value='OR'>OR</option>
                                                        <option value='PW'>PW</option>
                                                        <option value='PA'>PA</option>
                                                        <option value='PR'>PR</option>
                                                        <option value='RI'>RI</option>
                                                        <option value='SC'>SC</option>
                                                        <option value='SD'>SD</option>
                                                        <option value='TN'>TN</option>
                                                        <option value='TX'>TX</option>
                                                        <option value='UT'>UT</option>
                                                        <option value='VT'>VT</option>
                                                        <option value='VI'>VI</option>
                                                        <option value='VA'>VA</option>
                                                        <option value='WA'>WA</option>
                                                        <option value='WV'>WV</option>
                                                        <option value='WI'>WI</option>
                                                        <option value='WY'>WY</option>
                                                    </select>
                                                    @if ($errors->has('stateId'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('stateId') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin  {{ $errors->has('zip') ? ' has-error' : '' }}">
                                                <input type="text" class="form-control fsc-input" value="{{$service->zip}}" id="zip" name="zip" placeholder="Zip" maxlength="5">
                                                @if ($errors->has('zip'))
                                                    <span class="help-block">
											<strong>{{ $errors->first('zip') }}</strong>
										</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('countryId') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Country :</label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <div class="dropdown">
                                                    <select name="countryId" id="countryId" class="form-control fsc-input">
                                                        @if(!empty($service->countryId))
                                                            <option value="{{$service->countryId}}">{{$service->countryId}}</option>
                                                        @else
                                                            <option value="">Please Select Country</option>
                                                        @endif
                                                        <option value="USA">USA</option>
                                                    </select>
                                                    @if ($errors->has('countryId'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('countryId') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo1') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Telephone 1 :
                                            <span class="star-required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input type="tel" class="form-control fsc-input usanumber" value="{{$service->telephoneNo1}}" id="telephoneNo1" name="telephoneNo1" placeholder="+_(___)___-____" data-inputmask="'alias': 'phone'">
                                                @if ($errors->has('telephoneNo1'))
                                                    <span class="help-block">
											<strong>{{ $errors->first('telephoneNo1') }}</strong>
										</span>
                                                @endif
                                            </div>

                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin {{ $errors->has('telephoneNo1Type') ? ' has-error' : '' }}">
                                                <div class="dropdown">
                                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                        @if(!empty($service->telephoneNo1Type))
                                                            <option value="{{$service->telephoneNo1Type}}">{{$service->telephoneNo1Type}}</option>
                                                        @else
                                                            <option value="">Select type</option>
                                                        @endif
                                                        <option value="Home" @if($service->telephoneNo1Type=='Home') selected @endif >Home</option>
                                                        <option value="Mobile" @if($service->telephoneNo1Type=='Mobile') selected @endif >Mobile</option>

                                                        <option value="Other" @if($service->telephoneNo1Type=='Other') selected @endif >Other</option>
                                                        <option value="Work" @if($service->telephoneNo1Type=='Work') selected @endif >Work</option>

                                                    </select>
                                                    @if ($errors->has('telephoneNo1Type'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('telephoneNo1Type') }}</strong>
										</span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="text" class="form-control fsc-input" id="ext1" name="ext1" readonly value="{{$service->ext1}}" placeholder="Ext.">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo2') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Telephone 2 : </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="row">
                                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="tek" class="form-control fsc-input usanumber" value="{{$service->telephoneNo2}}" id="telephoneNo2" name="telephoneNo2" placeholder="+_(___)___-____" data-inputmask="'alias': 'phone'">
                                                @if ($errors->has('telephoneNo2'))
                                                    <span class="help-block">
											<strong>{{ $errors->first('telephoneNo2') }}</strong>
										</span>
                                                @endif
                                            </div>

                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin {{ $errors->has('telephoneNo2Type') ? ' has-error' : '' }}">
                                                <div class="dropdown">
                                                    <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                        <option value="">Select type</option>
                                                        <option value="Home" @if($service->telephoneNo2Type=='Home') selected @endif >Home</option>
                                                        <option value="Mobile" @if($service->telephoneNo2Type=='Mobile') selected @endif >Mobile</option>
                                                        <option value="Other" @if($service->telephoneNo2Type=='Other') selected @endif >Other</option>
                                                        <option value="Work" @if($service->telephoneNo2Type=='Work') selected @endif >Work</option>

                                                    </select>
                                                    @if ($errors->has('telephoneNo2Type'))
                                                        <span class="help-block">
											<strong>{{ $errors->first('telephoneNo2Type') }}</strong>
										</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <input type="text" class="form-control fsc-input" id="ext2" name="ext2" readonly value="{{$service->ext2}}" placeholder="Ext.">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('fax') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Fax :
                                            <span class="star-required1"></span>
                                        </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="tel" name="fax" id="fax" class="form-control fsc-input" value="{{$service->fax}}" placeholder="  (___)___-____" data-inputmask="'alias': 'phone'">
                                        @if ($errors->has('fax'))
                                            <span class="help-block">
											<strong>{{ $errors->first('fax') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Email :
                                            <span class="star-required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <input type="email" class="form-control fsc-input" value="{{$service->email}}" readonly id="email" name="email" placeholder="Email Address">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('serviceTypeId') ? ' has-error' : '' }}" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Service Type :
                                            <span class="star-required">*</span>
                                        </label>
                                    </div>

                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <div class="dropdown">
                                            <select class="form-control fsc-input" id="serviceTypeId" name="serviceTypeId">

                                                @if(!empty($service->serviceTypeId))
                                                    <option value="{{$service->serviceTypeId}}">{{$service->serviceTypeId}}</option>
                                                @else
                                                    <option value="">Please select service type</option>
                                                @endif


                                                <option value="1">Inquiry</option>
                                                <option value="2">Write Up Service</option>
                                                <option value="3">Form Filing to all TAX Authorities Service</option>
                                                <option value="4">Tax Planning Service</option>
                                                <option value="5">Tax-Form Preparation Service</option>
                                            </select>
                                            @if ($errors->has('serviceTypeId'))
                                                <span class="help-block">
											<strong>{{ $errors->first('serviceTypeId') }}</strong>
										</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                        <label class="fsc-form-label">Note : </label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                        <textarea class="form-control fsc-input" rows="3" id="note" name="note" placeholder="Note">{{$service->note}}</textarea>
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="btn btn-primary fsc-form-submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>$("#telephoneNo1").mask("(999) 999-9999");
        $(".ext").mask("999");
        $("#fax").mask("(999) 999-9999");
        $("#telephoneNo2").mask("(999) 999-9999");
        $(".usapfax").mask("(999) 999-9999");
        $("#zip").mask("9999");
    </script>

    <script>
        var dat1 = $('#ext1').val();
        $('#telephoneNo1Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext1').removeAttribute('readonly');
                $('#ext1').val();
            } else {
                document.getElementById('ext1').readOnly = true;
                $('#ext1').val('');
            }
        })
    </script>
    <script>
        var dat1 = $('#ext2').val();
        $('#telephoneNo2Type').on('change', function () {

            if (this.value == 'Work') {
                document.getElementById('ext2').removeAttribute('readonly');
                $('#ext2').val();
            } else {
                document.getElementById('ext2').readOnly = true;
                $('#ext2').val('');
            }
        })
    </script>
@endsection()