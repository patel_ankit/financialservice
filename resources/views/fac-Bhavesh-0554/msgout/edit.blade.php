@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Message</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('msg.update',$task->id)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                                {{csrf_field()}} {{method_field('PATCH')}}
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date \Day \ Time :</label>
                                    <div class="col-md-2" style="width:130px">
                                        <div class="">
                                            <input type="text" name="date" id="date" class="form-control" placeholer="Date" value="{{$task->date}}">
                                        </div>

                                    </div>
                                    <div class="col-md-2" style="width:130px">
                                        <div class="">
                                            <input type="text" name="day" id="day" class="form-control" placeholer="Day" value="{{$task->day}}">
                                        </div>

                                    </div>

                                    <div class="col-md-2" style="width:115px">
                                        <div class="">
                                            <input type="text" name="time" id="time" class="form-control" placeholer="Time" value="{{$task->time}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">From :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="type" id="type" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="Approval" @if($task->type=='Client') selected @endif>Client</option>
                                                <option value="employee" @if($task->type=='employee') selected @endif>Employee</option>
                                                <option value="User" @if($task->type=='User') selected @endif>User</option>
                                                <option value="Other Person" @if($task->type=='Other Person') selected @endif>Other Person</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('employee'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Search :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="employee" id="employee" class="form-control fsc-input">

                                            </select>
                                        </div>
                                        @if ($errors->has('employee'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group client" @if(empty($task->busname)) style="display:none" @endif>
                                    <label class="control-label col-md-3">Client Business Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="busname" readonly id="busname" value="{{$task->busname}}" class="form-control">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group client" @if(empty($task->clientname)) style="display:none" @endif>
                                    <label class="control-label col-md-3">Client Name :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientname" readonly value="{{$task->clientname}}" id="clientname" class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client1" @if(empty($task->clientfile)) style="display:none" @endif>
                                    <label class="control-label col-md-3">Client File # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientfile" readonly id="clientfile" value="{{$task->clientfile}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group client1" @if(empty($task->clientno)) style="display:none" @endif>
                                    <label class="control-label col-md-3">Client Telephone # :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="clientno" readonly id="clientno" value="{{$task->clientno}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Call Purpose :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="purpose" id="purpose2" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                @foreach($purpose as $pr)
                                                    <option value="{{$pr->purposename}}">{{$pr->purposename}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Other :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <input type="text" name="title" value="{{$task->title}}" id="title" class="form-control fsc-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Message :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <textarea id="editor1" name="description" rows="10" cols="80">{{$task->content}}</textarea>
                                        </div>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Massage For :</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            <select name="rlt_msg" id="rlt_msg" class="form-control fsc-input">
                                                <option value="">---Select---</option>
                                                <option value="FSC-EE" @if($task->rlt_msg=='FSC-EE') selected @endif>FSC-EE</option>
                                                <option value="FSC-User" @if($task->rlt_msg=='FSC-EE') selected @endif>FSC-User</option>
                                                <option value="Other" @if($task->rlt_msg=='FSC-EE') selected @endif>Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <!-- <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                   <label class="control-label col-md-3">Call Back :</label>
                     <div class="col-md-8">
                        <div class="">
                          <select name="call_back" id="call_back" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                           </select>
                        </div>
                        @if ($errors->has('employee'))
                                <span class="help-block">
                                <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                        @endif
                                    </div>
                                 </div>-->
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Ok">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/msg')}}">Cancel</a>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on('change', '#type', function () {
                var id = $(this).val();

                if (id == 'Other Person') {
                    $('.client').show();
                    $('.client1').show();
                    document.getElementById('busname').removeAttribute('readonly');
                    document.getElementById('clientname').removeAttribute('readonly');
                    document.getElementById('clientfile').removeAttribute('readonly');
                    document.getElementById('clientno').removeAttribute('readonly');
                }

                $.get('{!!URL::to('/clientid')!!}?id=' + id, function (data) {
                    $('#employee').empty();
                    //  $('#clientname').empty();
                    // $('.client').hide();
                    // $('#clientname').append('<option value="">---Select---</option>');
                    $.each(data, function (index, subcatobj) {


//$('#employee1').val(subcatobj.firstName);
                        if (id == 'employee') {
                            // $('.client').show();
                            $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.middleName + ' ' + subcatobj.lastName + '</option>');
                            // $('#clientname').val(subcatobj.firstName + ' ' + subcatobj.middleName + ' ' + subcatobj.lastName);
                        }
                        if (id == 'Approval') {
                            //$('.client').show();
                            $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.middle_name + ' ' + subcatobj.last_name + '</option>');
// $('#clientname').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.middle_name + ' ' + subcatobj.last_name + '</option>');

                        }
                    })

                });

            });
        });


    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#employee', function () {
                var selectedCountry = $("#type option:selected").val();
                var id = $(this).val(); //alert(selectedCountry);
                $.get('{!!URL::to('/clientid')!!}?id=' + id + '&state=' + selectedCountry, function (data) {
                    $('#clientname').empty();
                    $('#clientname').empty();
                    $('#clientno').empty();
                    $('#busname').empty();
                    $('#clientfile').empty();
                    $.each(data, function (index, subcatobj) {
                        document.getElementById('busname').readOnly = true;
                        document.getElementById('clientname').readOnly = true;
                        document.getElementById('clientfile').readOnly = true;
                        document.getElementById('clientno').readOnly = true;
                        //   alert(subcatobj.type);
                        if ('employee' == subcatobj.type) {
                            $('.client').show();
                            $('.client1').show();
                            $('#clientname').val(subcatobj.firstName);
                            $('#clientno').val(subcatobj.telephoneNo1);
                            $('#busname').val(subcatobj.business_name);
                            $('#clientfile').val(subcatobj.employee_id);
                        }
                        if ('Approval' == subcatobj.status) {
                            $('.client').show();
                            $('.client1').show();
                            $('#clientname').val(subcatobj.first_name);
                            $('#clientno').val(subcatobj.business_no);
                            $('#busname').val(subcatobj.business_name);
                            $('#clientfile').val(subcatobj.filename);
                        }
                    })
                });
            });
        });
    </script>
    <script type="text/javascript">
        $("#date").datepicker({
            'dateFormat': 'yy-mm-dd',
            onSelect: function (dateText) {
                alert();
                var seldate = $(this).datepicker('getDate');
                seldate = seldate.toDateString();
                seldate = seldate.split(' ');
                var weekday = new Array();
                weekday['Mon'] = "Monday";
                weekday['Tue'] = "Tuesday";
                weekday['Wed'] = "Wednesday";
                weekday['Thu'] = "Thursday";
                weekday['Fri'] = "Friday";
                weekday['Sat'] = "Saturday";
                weekday['Sun'] = "Sunday";
                var dayOfWeek = weekday[seldate[0]];
                $('#day').val(dayOfWeek);
            }
        });

        $(document).ready(function () {
            $("#date").change(function () {
                var startdate = $("#date").val();
                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var durtion = $('#duration').val();
                var date = new Date(startdate);
                var day = weekday[date.getDay()];
                var monthly = 30;
                var weekly = 7;
                var bimonthly = 15;
                var biweekly = 14;
                var monthss = monthNames[(date.getMonth())];
                var yearss = date.getFullYear();
                var yyy = yearss % 4;
                //if(yyy)
                //{
                //alert('true');
                //}
                //else
                //{
                //alert('false');
                //}
                if (durtion == "Weekly") {
                    var totaldays = 6;
                } else if (durtion == "Monthly") {
                    if (monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec') {
                        var totaldays = 30;
                    } else if (monthss == 'Feb') {
                        //if(years / 4 = 0)
                        if (yyy == 0) {
                            var totaldays = 28;
                        } else if (yyy == 1) {
                            var totaldays = 27;
                        }
                    } else if (monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov') {
                        var totaldays = 29;
                    }
                } else if (durtion == "Bi-Weekly") {
                    var totaldays = 13;
                } else if (durtion == "Bi-Monthly") {
                    var totaldays = 14;
                }
                // var vv = day + totaldays;

                date.setDate(date.getDate() + totaldays);// alert(vv);
                var date1 = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
                // alert(date1);
                var newdate = new Date(date1);
                var day1 = weekday[newdate.getDay()];
                //alert(newdate);
                var date2 = monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear();
                // $('#sch_end_date').val(date1);
                $('#day').val(day);
                $('#sch_end_day').val(day1);
                //document.write(date2);
            });
            $("#duration").change(function () {
                $('#sch_end_date').val('');
                $('#sch_start_date').val('');
            });
        });
    </script><!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Call Purpose</h4>
                </div>
                <div class="modal-body" style="display: inline-table;">
                    <form action="" method="post" id="ajax2">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-3">Call Purpose :</label>
                            <div class="col-md-6">
                                <div class="">
                                    <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Call Purpose">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <input type="button" id="addopt" class="btn btn-primary" value="Add Call Purpose">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(function () {
            $('#addopt').click(function () { //alert();
                var newopt = $('#newopt').val();
                if (newopt == '') {
                    alert('Please enter something!');
                    return;
                }

                //check if the option value is already in the select box
                $('#purpose1 option').each(function (index) {
                    if ($(this).val() == newopt) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "{!!route('purpose.purposes')!!}",
                    dataType: "json",
                    data: $('#ajax2').serialize(),
                    success: function (data) {
                        alert('Successfully Add');
                        $('#purpose2').append('<option value=' + newopt + '>' + newopt + '</option>');
                        $("#div").load(" #div > *");
                        $("#newopt").val('');
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                $('#myModal').modal('hide');
            });
        });

    </script>
    <style>
        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 39px;
            border-redius: 4px;
            user-select: none;
            -webkit-user-select: none;
        }

    </style>

    <style>
        .select2 {
            width: 100% !important;
        }

        .select2-container .select2-selection--single {

            border: 2px solid #00468F;
        }
    </style>
@endsection()