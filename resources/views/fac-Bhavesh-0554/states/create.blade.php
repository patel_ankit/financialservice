@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .professional_btn {
            width: 10%;
        }


        .taxation-radio-tab {
            float: left;
            margin-left: 20%;
            width: 80%;
        }

        .taxation-radio-tab li {
            float: left;
            list-style: none;
            padding-left: 22px;
            font-size: 20px;
        }

        .renew-radio {
            margin-top: 6px;
        }

        .renew-radio label {
            margin-right: 6px;
        }

        .redius {
            height: 30px;
            text-align: center;
            line-height: 30px;
            width: 30px;
            background: #1685cc;
            color: #fff;
            padding: 0px;
            border-radius: 50%;
            margin-top: 3px;
            float: left;
            margin-right: 10px;
        }
    </style>




    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>State Tax Authorities / Add New License Details</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>

                        <div class="col-md-12">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Taxation :</label>
                                    <div class="col-md-4">
                                        <input type="radio" name="radiostate" id="statetax" checked value="statetax"/>State Taxation
                                        <input type="radio" name="radiostate" id="statelicence" value="statelicence"/>Licence
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-12 statetax">
                            <form method="post" action="{{route('states.store')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}


                                <div class="form-group {{ $errors->has('typeofform') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Type of Tax :</label>
                                    <div class="col-md-4">
                                        <select type="text" class="form-control fsc-input" name="typeofform" id="typeofform">
                                            <option value="">---Select---</option>
                                            <option value="Income Tax">Income Tax</option>
                                            <option value="Payroll Tax">Payroll Tax</option>
                                            <option value="Sales Tax">Sales Tax</option>
                                        </select>
                                        @if ($errors->has('typeofform'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('typeofform') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div id="hh">
                                    <div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Type of Entity :</label>
                                        <div class="col-md-4">
                                            <select type="text" class="form-control fsc-input" name="authority_name" id="authority_name">
                                                <option value="">---Select---</option>

                                                @foreach($entity as $entity1)
                                                    <option value="{{$entity1->typeentity}}">{{$entity1->typeentity}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('authority_name'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('authority_name') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">State :</label>
                                        <div class="col-md-4">
                                            <select type="text" class="form-control fsc-input" name="state" id="state" placeholder="Enter Your Company Name">
                                                <option value="">Select</option>
                                                <option value="AL">AL</option>
                                                <option value="AK">AK</option>
                                                <option value="AS">AS</option>
                                                <option value="AZ">AZ</option>
                                                <option value="AR">AR</option>
                                                <option value="AF">AF</option>
                                                <option value="AA">AA</option>
                                                <option value="AC">AC</option>
                                                <option value="AE">AE</option>
                                                <option value="AM">AM</option>
                                                <option value="AP">AP</option>
                                                <option value="CA">CA</option>
                                                <option value="CO">CO</option>
                                                <option value="CT">CT</option>
                                                <option value="DE">DE</option>
                                                <option value="DC">DC</option>
                                                <option value="FM">FM</option>
                                                <option value="FL">FL</option>
                                                <option value="GA">GA</option>
                                                <option value="GU">GU</option>
                                                <option value="HI">HI</option>
                                                <option value="ID">ID</option>
                                                <option value="IL">IL</option>
                                                <option value="IN">IN</option>
                                                <option value="IA">IA</option>
                                                <option value="KS">KS</option>
                                                <option value="KY">KY</option>
                                                <option value="LA">LA</option>
                                                <option value="ME">ME</option>
                                                <option value="MH">MH</option>
                                                <option value="MD">MD</option>
                                                <option value="MA">MA</option>
                                                <option value="MI">MI</option>
                                                <option value="MN">MN</option>
                                                <option value="MS">MS</option>
                                                <option value="MO">MO</option>
                                                <option value="MT">MT</option>
                                                <option value="NE">NE</option>
                                                <option value="NV">NV</option>
                                                <option value="NH">NH</option>
                                                <option value="NJ">NJ</option>
                                                <option value="NM">NM</option>
                                                <option value="NY">NY</option>
                                                <option value="NC">NC</option>
                                                <option value="ND">ND</option>
                                                <option value="MP">MP</option>
                                                <option value="OH">OH</option>
                                                <option value="OK">OK</option>
                                                <option value="OR">OR</option>
                                                <option value="PW">PW</option>
                                                <option value="PA">PA</option>
                                                <option value="PR">PR</option>
                                                <option value="RI">RI</option>
                                                <option value="SC">SC</option>
                                                <option value="SD">SD</option>
                                                <option value="TN">TN</option>
                                                <option value="TX">TX</option>
                                                <option value="UT">UT</option>
                                                <option value="VT">VT</option>
                                                <option value="VI">VI</option>
                                                <option value="VA">VA</option>
                                                <option value="WA">WA</option>
                                                <option value="WV">WV</option>
                                                <option value="WI">WI</option>
                                                <option value="WY">WY</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('filling_frequency') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3">Filling Frequency :</label>
                                        <div class="col-md-4">
                                            <select type="text" class="form-control fsc-input" name="filling_frequency" id="filling_frequency">
                                                <option value="">Select</option>
                                                <option value="Yearly">Yearly</option>
                                                <option value="Quarterly">Quarterly</option>
                                                <option value="Monthly">Monthly</option>
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Form Name :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="formname" id="formname" class="form-control" placeholder="Form Name"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Due Date :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="duedate" id="duedate" class="form-control" placeholder="Due Date"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <!--<label class="control-label col-md-3">Extension Due Date  :</label>-->
                                        <label class="control-label col-md-3">Start Date :</label>
                                        <div class="col-md-4">
                                            <!--<input type="text" name="extdate" id="extdate" class="form-control" placeholder="Start Date" />-->
                                            <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Authority Short Name :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="authority_level" id="authority_level" class="form-control" placeholder="Authority Level"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Authority Full Name :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="authority_name1" id="authority_name1" class="form-control" placeholder="Authority Name"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Website :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="website" id="website" class="form-control" placeholder="Website"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Telephone # :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="telephone" id="telephone" class="form-control" placeholder="Telephone"/>
                                        </div>
                                    </div>

                                    <div class="salestaxdiv" style="display:none">
                                        <div class="Branch subttl" style="background:#00b1ce; border-color:#016373;">
                                            <h4 class="text-left padleft20 bold" style="color:#fff;">Sales Tax Info </h4>
                                        </div>

                                        <div class="col-md-1">
                                        </div>
                                        <!--<div class="col-md-6">-->
                                        <!--                   <table class="table table-bordered table-striped" id="mytable">-->
                                        <!--                           <tr id="row1">-->
                                        <!--                                <th>Month</th>-->
                                        <!--                                <th>Year</th>-->
                                        <!--                                <th>Sales Tax Rate</th>-->
                                        <!--                                <th></th>-->
                                        <!--                           </tr>-->
                                        <!--                           <tr id="row2">-->
                                        <!--                       <td>-->
                                        <!--                           <div class="col-md-6">-->
                                        <!--                               <select name="state_tax_month[]" id="state_tax_month" class="form-control fsc-input" style="width:100px;" >-->
                                        <!--                                   <option value="">---Select month---</option>-->
                                        <!--                                   <option value="Jan">01</option>-->
                                        <!--                                   <option value="Feb">02</option>-->
                                        <!--                                   <option value="Mar">03</option>-->
                                        <!--                                   <option value="Apr">04</option>-->
                                        <!--                                   <option value="May">05</option>-->
                                        <!--                                   <option value="Jun">06</option>-->
                                        <!--                                   <option value="Jul">07</option>-->
                                        <!--                                   <option value="Aug">08</option>-->
                                        <!--                                   <option value="Sep">09</option>-->
                                        <!--                                   <option value="Oct">10</option>-->
                                        <!--                                   <option value="Nov">11</option>-->
                                        <!--                                   <option value="Dec">12</option>-->
                                        <!--                               </select>-->
                                        <!--                            </div>-->
                                        <!--                       </td>-->
                                        <!--                       <td>-->
                                        <!--                           <div class="col-md-6">-->
                                        <!--                               <select name="state_tax_year[]" id="state_tax_year" class="form-control fsc-input" style="width:100px;">-->
                                        <!--                                   <option value="">---Select year---</option>-->
                                        <!--                                   <option value="2020">2020</option>-->
                                        <!--                                   <option value="2021">2021</option>-->
                                        <!--                                   <option value="2022">2022</option>-->
                                        <!--                                   <option value="2023">2023</option>-->
                                        <!--                                   <option value="2024">2024</option>-->
                                        <!--                                   <option value="2025">2025</option>-->
                                        <!--                               </select>-->
                                        <!--                           </div>-->
                                        <!--                       </td>-->
                                        <!--                       <td>-->
                                        <!--                           <input type="text" name="state_tax_personal_rate[]" id="state_tax_personal_rate" class="form-control txtinput_1" placeholder="Rate"  minlength="2" maxlength="7"/>-->
                                        <!--                       </td>-->
                                        <!--                       <td>-->
                                        <!--                           <div class="col-md-6"><input type="button" class="add-row btn btn-primary" value="+"></div>-->
                                        <!--                       </td>-->
                                        <!--                    </tr>-->
                                        <!--                   </table>-->
                                        <!--               </div>-->


                                        <div class="col-md-6">
                                            <table class="table table-bordered table-striped" id="mytable">
                                                <tr id="row1">
                                                    <th>Month</th>
                                                    <th>Year</th>
                                                    <th>Sales Tax Rate</th>
                                                    <th></th>
                                                </tr>
                                                <tr id="row2">
                                                    <td>
                                                        <div class="col-md-6">
                                                            <select name="state_tax_month[]" id="state_tax_month" class="form-control fsc-input" style="width:100px;">
                                                                <option value="">---Select month---</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>

                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-6">
                                                            <select name="state_tax_year[]" id="state_tax_year" class="form-control fsc-input" style="width:100px;">
                                                                <option value="">---Select year---</option>
                                                                <option value="2020">2020</option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                                <option value="2025">2025</option>
                                                            </select>

                                                        </div>
                                                    </td>
                                                    <td><input type="text" name="state_tax_personal_rate[]" id="state_tax_personal_rate" class="form-control txtinput_1" placeholder="Rate"/></td>
                                                    <td>
                                                        <div class="col-md-6"><input type="button" class="add-row btn btn-primary" value="+"></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <!--	<div class="form-group">
                                            <label class="control-label col-md-3">Renew Link :</label>
                                            <div class="col-md-4">
                                                <input type="text" name="renewlink" id="renewlink" class="form-control" placeholder="Renew Link" />
                                            </div>
                                    </div>-->
                                </div>

                                <div class="form-group" id="payroll" style="display:none">
                                    <label class="control-label col-md-3">Payroll :</label>
                                    <div class="col-md-9">
                                        <div class="check_tab">
                                            <input type="radio" id="Federal" class="payroll" name="payroll" value="Federal">
                                            <label for="Federal">Federal</label>
                                        </div>
                                        <div class="check_tab">
                                            <input type="radio" id="State" name="payroll" class="payroll" value="State">
                                            <label for="State">State</label>
                                        </div>
                                        <div class="check_tab">
                                            <input type="radio" id="County" name="payroll" class="payroll" value="County">
                                            <label for="County">County</label>
                                        </div>
                                        <div class="check_tab">
                                            <input type="radio" id="Local" name="payroll" class="payroll" value="Local">
                                            <label for="Local">Local</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="Branch" id="federal" style="display:none">
                                    <div class="col-md-3" style="text-align:left;">
                                        <h1 id="fed">Federal</h1>
                                    </div>
                                    <div class="col-md-6">
                                        <h1 style="font-size:20px;">Payroll Tax</h1>
                                    </div>
                                </div>

                                <div class="federal_tabs_main" id="federal_tabs_main" style="display:none">
                                    <div class="federal_tabs">
                                        <div class="federal_tab federal_name">
                                            <label>Short Name</label>
                                            <input type="text" class="form-control" name="payroll_name" placeholder="">
                                        </div>
                                        <div class="federal_tab federal_depart">
                                            <label>Department Name</label>
                                            <input type="text" class="form-control" name="payroll_department_name" placeholder="">
                                        </div>
                                        <div class="federal_tab federal_link">
                                            <label>Link</label>
                                            <input type="text" class="form-control" name="payroll_link" placeholder="">
                                        </div>
                                        <div class="federal_tab federal_tele">
                                            <label>Telephone #</label>
                                            <input type="text" class="form-control" id="payroll_telephone" name="payroll_telephone" placeholder="">
                                        </div>


                                    </div>
                                </div>

                                <div class="filing_tabs_main" id="filing_tabs_main" style="display:none">
                                    <h3 class="comm_title_one">Filing Frequency</h3>
                                    <div class="filing_tabs">
                                        <div class="filing_tab filing_form">
                                            <label>Form #</label>
                                            <input type="text" class="form-control" name="filing_frequency_form[]" placeholder="">
                                        </div>
                                        <div class="filing_tab filing_name">
                                            <label>Form Name</label>
                                            <input type="text" class="form-control" placeholder="Form Name" name="filing_frequency_name[]">
                                        </div>
                                        <div class="filing_tab filing_freq">
                                            <label>Filing Frquency</label>
                                            <select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
                                                <option value="Annually">Yearly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Quaterly">Quaterly</option>
                                            </select>
                                        </div>
                                        <div class="filing_tab filing_due_date">
                                            <label>Due Date</label>
                                            <input type="text" class="form-control" placeholder="" id="filing_frequency_due_date" name="filing_frequency_due_date[]">
                                        </div>
                                        <div class="filing_add">
                                            <button type="button" id="add_row1" class="filing_addbtn btn-success">ADD</button>
                                        </div>
                                        <div class="filing_formula">
                                            <button type="button" id="" class="btn_formula btn-default">Formula</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="payment_tabs_main" style="display:none">
                                    <h3 class="comm_title_two">Payment Frequency</h3>
                                    <div class="payment_tabs">
                                        <div class="payment_tab payment_form">
                                            <label>Form #</label>
                                            <input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]">

                                        </div>

                                        <div class="payment_tab payment_name">
                                            <label>Form Name</label>
                                            <input type="text" class="form-control" placeholder="Form Name" name="filing_frequency_name1[]">
                                        </div>

                                        <div class="payment_tab payment_freq">
                                            <label>Payment Frquency</label>
                                            <select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
                                                <option value="Annually">Yearly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Quaterly">Quaterly</option>
                                            </select>
                                        </div>

                                        <div class="payment_tab payment_due_date">
                                            <label>Due Date</label>
                                            <input type="text" class="form-control" placeholder="" id='filing_frequency_due_date1' name="filing_frequency_due_date1[]">
                                        </div>

                                        <div class="payment_add">
                                            <button type="button" id="add_row2" class="payment_addbtn btn-success">ADD</button>
                                        </div>

                                        <div class="payment_formula">
                                            <button type="button" id="" class="btn_formula btn-default">Formula</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/states')}}">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-12 statelicence" style="display:none;">
                            <form method="post" action="{{route('taxation.store')}}" class="form-horizontal" id="positionname" name="positionname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div id="tabs">
                                    <ul class="tabs taxation-radio-tab" style="display:none">
                                        <!--  <li id="tab-1"><label for="tab1"><input name="tab" id="tab1" type="radio" checked value="Taxation" /> Taxation</label></li>-->
                                        <li id="tab-2"><label for="tab2"><input name="tab" id="tab2" type="radio" value="License" checked/> License</label></li>
                                    </ul>
                                    <div class="tab_container">

                                        <div id="wanted" class="tab_content">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Type of License :</label>
                                                <div class="col-md-4">
                                                    <select name="typeoflicense" type="text" id="currency" class="form-control">
                                                        <option value="">Type of License</option>
                                                        @foreach($currency as $cur)
                                                            <option value="{{$cur->id}}">{{$cur->licensename}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Type of Bussiness :</label>
                                                <div class="col-md-4">
                                                    <select name="bussiness_name" id="bussiness_name" class="form-control fsc-input category1">
                                                        <option value=''>---Select Business Category---</option>
                                                        @foreach($category as $cate)
                                                            <option value='{{$cate->id}}'>{{$cate->business_cat_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Jurisdiction Level :</label>
                                                <div class="col-md-4">
                                                    <select class="form-control fsc-input" name="typeofservice1" id="typeofservice1">
                                                        <option value=""> ---Select---</option>
                                                        <option value="City">City</option>
                                                        <option value="County">County</option>
                                                        <option value="Local">Local</option>
                                                        <option value="State">State</option>
                                                        <option value="Federal">Federal</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group countyname" style="display:none">
                                                <label class="control-label col-md-3">State :</label>
                                                <div class="col-md-4">
                                                    <select class="form-control bfh-states" id="bfh-states" name="state" data-country="USA" data-state="">
                                                        <option value=""> ---Select---</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="countyname" style="display:none">
                                                <div class="form-group city" style="display:none">
                                                    <label class="control-label col-md-3">City Name </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" placeholder="City Name" id="cityname" name="cityname">
                                                    </div>
                                                </div>
                                                <div class="form-group city1" style="display:none">
                                                    <label class="control-label col-md-3">County Name / Number</label>
                                                    <div class="col-md-2">
                                                        <select type="text" class="form-control" id="county" name="county">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" placeholder="County Number" id="countynumber" name="countynumber">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Authority Name :</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Authority Name" id="authorityname" name="authorityname">
                                                </div>
                                            </div>
                                            <div class="countyname" style="display:none">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Website</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" placeholder="Website" id="website" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Telephone #</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" placeholder="Telephone" id="telephone" name="telephone">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Fax #</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" placeholder="Fax" id="fax" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Renew</label>
                                                    <div class="col-md-4">
                                                        <div class="renew-radio">
                                                            <label for="yes"><input type="radio" name="renew" id="yes" value="Yes"> Yes</label>
                                                            <label for="no"><input type="radio" name="renew" id="no" value="No"> No</label></div>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display:none" id="renewal4">
                                                    <label class="control-label col-md-3">Renewal-Website:</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" value="" placeholder="Renewal Website" id="renewalwebsite" name="renewalwebsite">
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display:none" id="renewal1">
                                                    <label class="control-label col-md-3">Renewal</label>
                                                    <div class="col-md-4">
                                                        <select type="text" class="form-control" id="renewal" name="renewal">
                                                            <option value="">--Select--</option>
                                                            /
                                                            <option value="Universal">Universal</option>
                                                            <option value="Individual">Individual</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group" style="display:none" id="renewal2">
                                                    <label class="control-label col-md-3">License Expire Date</label>
                                                    <div class="col-md-2">
                                                        <select type="text" class="form-control" id="duedate" name="duedate">
                                                            <option value="">Month</option>
                                                            <?php
                                                            for ($i = 1; $i <= 12; $i++) {
                                                                $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));
                                                                echo '<option value="' . $month_name . '"' . $month_name . '>' . $month_name . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <?php
                                                        $date = '2003-09-01';
                                                        $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
                                                        ?>
                                                        <select type="text" class="form-control" id="duedate1" name="duedate1">
                                                            <option value="">Date</option>
                                                            <?php while(strtotime($date) <= strtotime($end)) {
                                                            $day_num = date('d', strtotime($date));
                                                            $day_name = date('l', strtotime($date));
                                                            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                                                            // echo "<td>$day_num <br/> $day_name</td>";
                                                            ?>
                                                            <option value="<?php echo $day_num;?>"><?php echo $day_num;?></option> <?php
                                                            }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display:none" id="renewal3">
                                                    <label class="control-label col-md-3">Renewal Due Date</label>

                                                    <div class="col-md-2">
                                                        <select type="text" class="form-control" id="expiredate" name="expiredate">
                                                            <option value="">Month</option>
                                                            <?php
                                                            for ($i = 1; $i <= 12; $i++) {
                                                                $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));
                                                                echo '<option value="' . $month_name . '"' . $month_name . '>' . $month_name . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div><?php
                                                    $date = '2003-09-01';
                                                    $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
                                                    ?>
                                                    <div class="col-md-2">
                                                        <select type="text" class="form-control" id="expiredate2" name="expiredate2">
                                                            <option value="">Date</option>
                                                            <?php while(strtotime($date) <= strtotime($end)) {
                                                            $day_num = date('d', strtotime($date));
                                                            $day_name = date('l', strtotime($date));
                                                            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                                                            // echo "<td>$day_num <br/> $day_name</td>";
                                                            ?>
                                                            <option value="<?php echo $day_num;?>"><?php echo $day_num;?></option> <?php
                                                            }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Annual Fees</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control annualfees" value="" placeholder="Annual Fees" id="annualfees" name="annualfees">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Processign Fees</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control processingfees" value="" placeholder="Processig Fees" id="processingfees" name="processingfees">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="col-md-2 col-md-offset-3">
                                                    <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                                </div>
                                                <div class="col-md-2 row">
                                                    <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/taxation')}}">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>


    <script>
        $(document).ready(function () {
            $('#statetax').on('change', function () {
                var var22 = $('#statetax').val();
                // alert(var22);
                if (var22 == '') {
                    $('.statetax').show();
                    $('.statelicence').hide();
                } else if (var22 == 'statetax') {
                    $('.statetax').show();
                    $('.statelicence').hide();
                }
            });

            $('#statelicence').on('change', function () {
                var var33 = $('#statelicence').val();
                //alert(var33);
                if (var33 == 'statelicence') {
                    $('.statetax').hide();
                    $('.statelicence').show();
                }
            });

        });
    </script>


    <script>
        $(document).ready(function () {
            $('#typeofform').on('change', function () {
                if (this.value == 'Sales Tax') {
                    $(".salestaxdiv").show();
                } else if (this.value == 'Income Tax') {
                    $(".salestaxdiv").hide();
                } else if (this.value == 'Tabbaco Tax') {
                    $(".salestaxdiv").hide();
                }

            });
        });
    </script>

    <script type="text/javascript">


        $(document).ready(function () {
            (function ($) {
                var minNumber = -100;
                var maxNumber = 100;
                $('.spinner .btn:first-of-type').on('click', function () {
                    if ($('.spinner input').val() == maxNumber) {
                        return false;
                    } else {
                        $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
                    }
                });

                $('.txtinput_1').on("blur", function () {
                    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                    if (minNumber > inputVal) {
                        inputVal = -100;
                    } else if (maxNumber < inputVal) {
                        inputVal = 100;
                    }
                    $(this).val(inputVal + '.00%');
                });

                $('.spinner .btn:last-of-type').on('click', function () {
                    if ($('.spinner input').val() == minNumber) {
                        return false;
                    } else {
                        $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
                    }
                });
            })(jQuery);

            //state_tax_month,state_tax_year,state_tax_personal_rate

            var k = 1;
            $(".add-row").click(function () {
                k++;
                var markup = "<tr><td><div class='col-md-6'><select name='state_tax_month[]' id='state_tax_month' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='state_tax_year[]' id='state_tax_year' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><input type='text' name='state_tax_personal_rate[]' id='state_tax_personal_rate' class='form-control txtinput_" + k + "'  placeholder='Rate'/></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
                $("#mytable").append(markup);
                var minNumber = -100;
                var maxNumber = 100;
                $('.txtinput_' + k).on("blur", function () {
                    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                    if (minNumber > inputVal) {
                        inputVal = -100;
                    } else if (maxNumber < inputVal) {
                        inputVal = 100;
                    }
                    $('.txtinput_' + k).val(inputVal + '.00%');
                });
            });
        });


        $("body").on("click", ".delete-row", function () {
            $(this).parents("tr").remove();
        });


        // $(document).ready(function(){

        // 	var maxField = 10; //Input fields increment limitation
        // 	var addButton = $('.add_button'); //Add button selector
        // 	var wrapper = $('.filing_tabs_main'); //Input field wrapper
        // 	var fieldHTML = '<div class="filing_tabs"><div class="filing_tab filing_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form[]"></div><div class="filing_tab filing_name"><label>Form Name</label><input type="text" name="filing_frequency_name[]" class="form-control" placeholder=""></div><div class="filing_tab filing_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="filing_tab filing_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]"></div><div class="filing_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="professional_btn"><a href="javascript:void(0);" id="remove_button_pro" class="btn_professional btn_professional_remove">Remove</a></div></div>';
        // 	var x = 1; //Initial field counter is 1
        // 	$(addButton).click(function(){ //Once add button is clicked
        // 		if(x < maxField){ //Check maximum number of input fields
        // 		x++; //Increment field counter
        // 		$(wrapper).append(fieldHTML); // Add field html
        // 		}
        // 	});
        // 	$(wrapper).on('click', '#add_row1', function(e){ //Once remove button is clicked
        // 		e.preventDefault();
        // 		$(wrapper).append(fieldHTML); //Remove field html
        // 		x++;
        // 	});
        // 	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
        // 		e.preventDefault();
        // 		$(this).parent().parent('.filing_tabs').remove(); //Remove field html
        // 		x--; //Decrement field counter
        // 	});
        // });
    </script>


    <script type="text/javascript">
        // $(document).ready(function(){

        // 	var maxField = 10; //Input fields increment limitation
        // 	var addButton = $('.add_button'); //Add button selector
        // 	var wrapper = $('.payment_tabs_main'); //Input field wrapper
        // 	var fieldHTML = '<div class="payment_tabs"><div class="payment_tab payment_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]"></div><div class="payment_tab payment_name"><label>Form Name</label><input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]"></div><div class="payment_tab payment_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="payment_tab payment_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]"></div><div class="payment_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="payment_add"><a href="javascript:void(0);" id="remove_button_pro1" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html
        // 	var x = 1; //Initial field counter is 1
        // 	$(addButton).click(function(){ //Once add button is clicked
        // 		if(x < maxField){ //Check maximum number of input fields
        // 		x++; //Increment field counter
        // 		$(wrapper).append(fieldHTML); // Add field


        // 		}
        // 	});
        // 	$(wrapper).on('click', '#add_row2', function(e){ //Once remove button is clicked
        // 		e.preventDefault();
        // 		$(wrapper).append(fieldHTML); //Remove field html
        // 		x++;
        // 	});
        // 	$(wrapper).on('click', '#remove_button_pro1', function(e){ //Once remove button is clicked
        // 		e.preventDefault();
        // 		$(this).parent().parent('.payment_tabs').remove(); //Remove field html
        // 		x--; //Decrement field counter
        // 	});
        // });
    </script>
    <script>
        $(document).ready(function () {
            $('#typeofform').change(function () {
                var selectedText = $('#typeofform').val();
                if (selectedText == 'Payroll Tax') {
                    $('#filing_tabs_main').show();
                    $('#federal').show();
                    $('#payroll').show();
                    $('#federal_tabs_main').show();
                    $('.payment_tabs_main').show();
                    $('#hh').hide();
                } else {

                    $('#filing_tabs_main').hide();
                    $('#federal').hide();
                    $('#payroll').hide();
                    $('#federal_tabs_main').hide();
                    $('.payment_tabs_main').hide();
                    $('#hh').show();
                }
            })
        })


        $(document).ready(function () {
            $('.payroll').click(function () {
                var va = $(this).val();
                $('#fed').html(va);
            });

        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#filing_frequency1").change(function () {
                var selectedText = $('#filing_frequency1').val();
                var selectedText1 = $('#federal_payment_frequency_quaterly').val();
                var selectedValue = $(this).val();
                if (selectedValue == 'Annually') {
                    $("#filing_frequency_due_date1").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                } else if (selectedValue == 'Monthly') {
                    if (selectedText == '') {
                        $("#filing_frequency_due_date1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    } else {
                        if (selectedText > '<?php echo date('M-d-Y');?>') {
                            $("#filing_frequency_due_date1").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else {
                            $("#filing_frequency_due_date1").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        }
                    }
                } else if (selectedValue == 'Quaterly') {
                    if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                        //  $("#filing_frequency_due_date").val('');
                    } else {
                        $("#filing_frequency_due_date1").val('<?php echo date("M", strtotime("3 month", strtotime(date("M")))) . '-' . date("t") . '-' . date("Y");?>');
                    }
                } else {

                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#filing_frequency").change(function () {
                var selectedText = $('#filing_frequency').val();
                var selectedText1 = $('#federal_payment_frequency_quaterly').val();
                var selectedValue = $(this).val();
                if (selectedValue == 'Annually') {
                    $("#filing_frequency_due_date").val('<?php echo "Jan" . '-' . date("t") . '-' . date("Y", strtotime("1 year", strtotime(date("Y"))));?>');
                } else if (selectedValue == 'Monthly') {
                    if (selectedText == '') {
                        $("#filing_frequency_due_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                    } else {
                        if (selectedText > '<?php echo date('M-d-Y');?>') {
                            $("#filing_frequency_due_date").val('<?php echo date("M", strtotime("1 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        } else {
                            $("#filing_frequency_due_date").val('<?php echo date("M", strtotime("0 month", strtotime(date("M")))) . '-' . "15" . '-' . date("Y");?>');
                        }
                    }
                } else if (selectedValue == 'Quaterly') {
                    if (selectedText1 > '<?php echo date('M-d-Y');?>') {
                        //  $("#filing_frequency_due_date").val('');
                    } else {
                        $("#filing_frequency_due_date").val('<?php echo date("M", strtotime("3 month", strtotime(date("M")))) . '-' . date("t") . '-' . date("Y");?>');
                    }
                } else {

                }
            });
        });


        $("#duedate").datepicker({
            autoclose: true,
            format: "M-dd",
        });
        // $("#extdate").datepicker({
        // 		autoclose: true,
        // format: "M-dd",
        // });

        $("#start_date").datepicker({
            autoclose: true,
            format: "M-dd",
        });
        $("#payroll_telephone").mask("(999) 999-9999");
        $("#telephone").mask("(999) 999-9999");
    </script>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Type of Entity</h4>
                </div>
                <div class="modal-body" style="display: inline-table;">
                    <form action="" method="post" id="ajax2">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-3">Type of Entity :</label>
                            <div class="col-md-6">
                                <div class="">
                                    <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Type of Entity">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <input type="button" id="addopt" class="btn btn-primary" value="Add Type of Entity">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(function () {
            $('#addopt').click(function () { //alert();
                var newopt = $('#newopt').val();
                if (newopt == '') {
                    alert('Please enter something!');
                    return;
                }

                //check if the option value is already in the select box
                $('#purpose1 option').each(function (index) {
                    if ($(this).val() == newopt) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "{!!route('typeof.typeofentity')!!}",
                    dataType: "json",
                    data: $('#ajax2').serialize(),
                    success: function (data) {
                        alert('Successfully Add');
                        $('#authority_name').append('<option value=' + newopt + '>' + newopt + '</option>');
                        $("#div").load(" #div > *");
                        $("#newopt").val('');
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                $('#myModal').modal('hide');
            });
        });

    </script>


    <script>
        //license

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $(function () {

            $('.annualfees').on('blur', function () {
                var annualfees = $('.annualfees').val();
                var sign53 = parseFloat(Math.round(annualfees * 100) / 100).toFixed(2);
                var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $(".annualfees").val(annual);
            });

            $('.processingfees').on('blur', function () {
                var processingfees = $('.processingfees').val();
                var sign53 = parseFloat(Math.round(processingfees * 100) / 100).toFixed(2);
                var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $(".processingfees").val(annual);
            });
        });
        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('removelicense.removelicense1')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            // alert(data);
                            $('#cur_' + id).remove();
                            $("#currency").load(" #currency > *");
                        }
                    })
                } else {
                    return false;
                }
            });


            $('input[type=radio][name=tab]').on('change', function () {
                if (this.value == 'Taxation') {
                    $('#wanted').hide();
                    $('#forsale').show();
                } else if (this.value == 'License') {
                    $('#wanted').show();
                    $('#forsale').hide();
                }
            });
        });

        $(document).ready(function () {
            $('#typeofservice').on('change', function () {
                if (this.value == 'Type of Business') {

                    $('#business_catagory_name_2').show();
                } else {

                    $('#business_catagory_name_2').hide();
                }
            });
        });

        $(document).ready(function () {
            $('#typeofservice1').on('change', function () {
                var id = this.value;
                if (id == 'City') {
                    $('.city').show();
                    $('.city1').show();
                    $('.countyname').show();
                    $('.state').hide();
                    $('.faderal').hide();
                } else if (id == 'County') {
                    $('.state').hide();
                    $('.city1').show();
                    $('.countyname').show();
                    $('.city').hide();
                    $('.faderal').hide();
                } else if (id == 'Local') {
                    $('.city').hide();
                    $('.countyname').show();
                    $('.state').hide();
                    $('.city1').hide();
                    $('.faderal').show();
                } else if (id == 'State') {
                    $('.city').hide();
                    $('.countyname').show();
                    $('.city1').hide();
                    $('.state').show();
                    $('.faderal').hide();
                } else if (id == 'Federal') {
                    // alert('federal');
                    $('.city').hide();
                    $('.countyname').show();
                    $('.state').hide();
                    $('.city1').hide();
                    $('.faderal').show();
                } else {
                    $('#city').hide();
                    $('.countyname').hide();
                    $('.state').hide();
                    $('.faderal').hide();
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('click', '#yes', function () {
                var value = $(this).val();
                if (value == 'Yes') {
                    $('#renewal1').show();
                    $('#renewal4').show();

                } else {
                    $('#renewal1').hide();
                }

            });
        });


        $(document).ready(function () {
            $(document).on('click', '#no', function () {

                var value = $(this).val();

                if (value == 'No') {
                    $('#renewal1').hide();
                    $('#renewal2').hide();
                    $('#renewal3').hide();
                    $('#renewal4').hide();

                } else {
                    $('#renewal2').hide();
                }

            });
        });

        $(document).ready(function () {
            $(document).on('change', '#renewal', function () {
                var valu = $(this).val();
                if (valu == 'Universal') {
                    $('#renewal2').show();
                    $('#renewal3').show();
                } else {
                    $('#renewal2').hide();
                    $('#renewal3').hide();
                }
            });

        })
        $(document).ready(function () {
            $(document).on('change', '#county', function () {
                var id = $(this).val();
                $.get('{!!URL::to('getcountycod')!!}?id=' + id, function (data) {
                    $('#countynumber').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#countynumber').val(subcatobj.countycode);
                    })
                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#bfh-states', function () {
                var id = $(this).val(); //`alert(id);
                $.get('{!!URL::to('getcountcount')!!}?id=' + id + '&state=' + id, function (data) {
                    $('#county').empty();
                    $('#county').append('<option value="">---Select---</option>');

                    $.each(data, function (index, subcatobj) {

                        $('#county').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');

                    })
                });
            });
        });
        $(document).ready(function () {
            /***phone number format***/
            $(".phone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("(" + curval + ")" + " ");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                    $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 9) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
            $('#expiredate,#expiredate,#duedate').datepicker();
        });

        // $('.expiredate').datepicker({format: "dd.mm.yyyy"});
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getRequest')!!}?id=' + id, function (data) {
                    $('#business_catagory_name').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#business_catagory_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                    })

                });

            });
        });

        $('.js-example-tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });

    </script>
    <!-- Currency--->
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Type of License </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="ajax">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Type of License"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="addopt" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#00c0ef !important;color:white;">
                    <h5 class="modal-title" style="text-align:center;" id="exampleModalLabel">Licence / Registration
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body" style="background-color:#ffff99 !important;">
                    <ul class="curency curency_ref" id="div">
                        @foreach($currency as $cur)
                            <li id="cur_{{$cur->id}}" style="margin-bottom:10px;"><a class="delete" id="{{$cur->id}}">{{$cur->licensename}} <span style="float:right;"><i class="fa fa-trash" style="color:red;"></i></span></a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" style="border:1px solid !important;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection()