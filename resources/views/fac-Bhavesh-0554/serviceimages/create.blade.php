@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service Image</h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">Services</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('serviceimages.store',Request::segment(4))}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" value="{{Request::segment(4)}}" name="service_id">

                                <div class="form-group {{ $errors->has('serviceimage') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Service Image :</label>
                                    <div class="col-md-8">
                                        <label class="file-upload btn btn-primary"> Browse for file ...
                                            <input type="file" name="serviceimage" id="serviceimage" class="form-control"/>

                                        </label>
                                        @if ($errors->has('serviceimage'))
                                            <span class="help-block">
											<strong>{{ $errors->first('serviceimage') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/serviceimages')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()