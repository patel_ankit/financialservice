@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-title">
                            <h3>Service image</h3>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="sampleTable3">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Service Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($serviceimages as $bus)

                                    <tr>
                                        <td>{{$loop->index + 1}}</td>

                                        <td><img src="{{asset('serviceimage','')}}/{{$bus->serviceimage}}" alt="" class="main-image"/></td>
                                        <td>
                                            <a class="btn-action btn-view-edit" href="{{url('serviceimages.edit', $bus->id)}}">View/ Edit</a>

                                            <form action="{{ route('serviceimages.destroy',[$bus->id]) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                                {{csrf_field()}} {{method_field('DELETE')}}
                                            </form>

                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                    {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href="">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>



@endsection()