@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .greenText {
            padding: 7px;
            background-color: green !important;
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px;
            outline: 0;
            color: white !important;
        }

        .blueText {
            background-color: blue;
        }

        .redText {
            padding: 7px;
            background-color: red !important;
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px !important;
            outline: 0;
            color: white !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 16px !important;
            padding-left: 0px;
            margin-top: -1px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field {
            border-color: #3c8dbc !important;
            border: 2px solid #2fa6f2;
            border-radius: 4px;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border-radius: 4px;
            mborder: 2px solid #2fa6f2;
        }

        .select2-container--default .select2-selection--multiple {
            border: 2px solid #2fa6f2;
        }

        .nex1:hover, .nex1:focus {
            background-color: transparent;
        }

        .ag {
            position: relative;
            top: -25px;
            font-size: 13px;
            left: -100px;
        }

        .ac {
            margin-left: 0.7%;
        }

        .ps_mr {
            width: 13% !important;
        }

        .ps_firstname {
            width: 28% !important;
        }

        .ps_middlename {
            WIDTH: 8% !IMPORTANT;
        }

        .ps_lastname {
            width: 31% !important;
        }

        .green-border {
            background-color: #003b6d !important;
        }

        .breadcrumb > li.ddd.active1 a {
            background: green !important;
        }

        .title-form-group p {
            font-size: 16px;
            line-height: 30px;
        }

        .tess {
            text-align: left;
            float: right;
            font-size: 11px;
            margin-right: 20px;
        }

        .star-required1 {
            color: transparent;
        }

        .fsc-reg-sub-header {
            padding-left: 0;
        }

        .pager li > a, .pager li > span {
            width: 93px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            color: #fff;
            background-color: green;
        }

        .bread.title-form-group p {
            font-size: 12px
        }

        li.next {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .nav-pills > li > a {
            border-radius: 0 !important;
        }

        li.previous {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .fsc-reg-sub-header-div {
            border-radius: 0;
            padding: 6px 4%;
            border-top: 2px solid #fff;
            border-bottom: 2px solid #fff;
        }

        .breadcrumb {
            padding: 0px;
            background: #D4D4D4;
            list-style: none;
            overflow: hidden;
            margin-top: -20px;
        }

        .breadcrumb > li + li:before {
            padding: 0;
        }

        .breadcrumb li {
            float: left;
            width: 24%;
            text-align: center;
            font-size: 15px;
        }

        }
        .breadcrumb li.active a {
            background: brown;
            background: green;
        }

        .breadcrumb li.completed a {
            background: brown;
            background: hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li.active a:after {
            border-left: 30px solid green;
        }

        .breadcrumb li.completed a:after {
            border-left: 30px solid hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li a {
            width: 95%;
            color: white;
            text-decoration: none;
            padding: 10px 0 10px 45px;
            position: relative;
            display: block;
            float: left;
        }

        .breadcrumb li a:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            border-bottom: 50px solid transparent;
            border-left: 30px solid hsla(0, 0%, 83%, 1);
            position: absolute;
            top: 50%;
            margin-top: -50px;
            left: 100%;
            z-index: 2;
        }

        .multiselectbox .select2-container {
            width: 100% !important;
        }

        .breadcrumb li a:before {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            /* Go big on the size, and let overflow hide */
            border-bottom: 50px solid transparent;
            border-left: 30px solid white;
            position: absolute;
            top: 50%;
            margin-top: -50px;
            margin-left: 1px;
            left: 100%;
            z-index: 1;
        }

        .breadcrumb li:first-child a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .content-wrapper {

            height: 100%;
        }

        .breadcrumb li:nth-o a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .pager .disabled > a, .pager .disabled > a:focus, .pager .disabled > a:hover, .pager .disabled > span {
            color: #fff;
            cursor: not-allowed;
            background-color: #428bca;
            font-size: 14px;
        }

        .pager li > a, .pager li > span {
            display: inline-block;
            padding: 5px 14px;
            background-color: #0472d0;
            border: 1px solid #ddd;
            border-radius: 15px;
            font-size: 14px;
            color: #fff;
        }

        .error {
            font-size: 16px;
            color: #ff0000;
            font-weight: normal;
        }

        .breadcrumb li a:hover {
            background: green !important;
        }

        .breadcrumb li a:hover:after {
            border-left-color: green !important;
        }

        .nav-tabs > li {
            width: 25%;
        }

        .nav-tabs > li > a {
            height: 40px;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .Red {
            background-color: red !important;
            color: #fff !important
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff !important
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff !important
        }

        .Yellow {
            background-color: Yellow !important;
            color: #555 !important
        }

        .Orange {
            background-color: Orange !important;
            color: #fff !important
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn3d.btn-info:hover, .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .fsc-form-label {
            display: block;
            text-align: right;
        }

        .tab-content > .tab-pane {
            padding: 20px 0;
        }

        .btnNext {
            float: right;
        }

        .btnPrevious {
            float: left;
        }

        .ps_main {
            float: left;
        }

        .ps_mr {
            float: left;
            width: 18%;
            margin-right: 1%;
        }

        .ps_firstname {
            float: left;
            width: 33%;
            margin: 0 1%;
        }

        .ps_middlename {
            float: left;
            width: 10%;
            margin: 0 1%;
        }

        .ps_lastname {
            float: left;
            width: 33%;
            margin-left: 1%;
        }

        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 8px 3px 8px 8px !important;
        }

        .center_main {
            width: 100%;
            text-align: center;
        }

        .center_tab {
            display: inline-block;
            width: 170px;
            margin: 0 10px;
        }

        .ser:nth-of-type(odd) {
            padding: 15px 0;
            width: 100%;
            background-color: #e8f7fd;
            margin: auto;
        }

        .ser:nth-of-type(even) {
            background-color: #fff5dd;
            padding: 15px 0;
            width: 100%;
            margin: auto;
        }

        .bg-color {
            background: #bdbdbd;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        .select2-container--default .select2-selection--single {
            height: 40px;
            line-height: 40px;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header" style="height:50px;">

            <div class="">
                <div class="" style="text-align:center;">
                    <h1>Vendor <span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;"> View / Edit</span></h1>
                </div>
            </div>

        </section>
        <?php //echo "<pre>";print_r($emp);?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                </div>
                                <form method="post" id="registrationForm" action="{{route('vendor.update',$emp->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                    {{csrf_field()}}{{method_field('PATCH')}}
                                    <div id="smartwizard">
                                        <div class="panel-body">
                                            <div class="tab-content">

                                                <div class="tab-pane fade in active" data-step="0" id="tab1primary">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <br/>
                                                    <!--	<div class="form-group {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" id="business_catagory_name_2">
												<label class="control-label col-md-3">Type of Business / Category: <span class="star-required">*</span></label>
												<div class="col-md-5 ac" style="width: 42.1%;">
													<div class="row">
													     <?php $str1 = $emp->business_catagory_name; $splittedstring1 = explode(",", $str1);?>
                                                            <div class="col-md-12">


                                                                <select name="business_catagory_name[]" id="business_catagory_name" multiple="multiple" class="js-example-tags  form-control fsc-input category1">
															<option value=''>---Select Business Category---</option>
															@foreach($category as $cate)
                                                        <option value='{{$cate->id}}' @foreach($splittedstring1 as $key => $value) @if($value ==$cate->id) selected @endif @endforeach >{{$cate->business_cat_name}}</option>
															@endforeach
                                                            </select>
                                                        </div>
                                                    </div>
@if ($errors->has('business_catagory_name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('business_catagory_name') }}</strong>
										</span>
									@endif
                                                            </div>

                                                             <div class="col-md-3 ac">
                                                         <div class="row">
                                                            <div class="col-md-12">
                                                             <select class="form-control categorys2" name="categorys">
                                                                 <option value="">Select</option>
                                                                 <option value="Utility" <?php if ($emp->categorys == 'Utility') {
                                                        echo 'Selected';
                                                    }?>>Utility</option>
                                                     <option value="Insurance" <?php if ($emp->categorys == 'Insurance') {
                                                        echo 'Selected';
                                                    }?>>Insurance</option>
                                                     
                                                 </select>
                                                </div>
                                             </div>
                                           
                                          </div>
												
												
											</div>
											
											      <div class="form-group showutility" <?php if($emp->categorys == 'Utility') {
                                                    }else { ?>style="display:none;"<?php } ?>>
                                          <label class="control-label col-md-3">Utilities Category: </label>
                                              <div class="col-md-4 ac">
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <select name="utilitiesname" id="utilitiesname" class="form-control fsc-input category1">
                                                      <option value=''>---Select Utilities Category---</option>
                                                      @foreach($utilities as $cate)
                                                        <option value='{{$cate->id}}' @if($emp->utilitiesname ==$cate->id) selected @endif>{{$cate->utilitiesname}}</option>
                                                      @endforeach
                                                            </select>
                                                         </div>
                                                      </div>
@if ($errors->has('business_catagory_name'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('business_catagory_name') }}</strong>
                                             </span>
                                             @endif
                                                            </div>

                                                             <div class="col-md-1"><a href="#" data-toggle="modal"
                                                                      data-target="#basicExampleModalUtilities" class="redius">
                                                                          <i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp;
                                                        <a href="#" data-toggle="modal" data-target="#basicExampleModal4" 
                                                        class="redius"><i class="fa fa-minus"></i></a> </div>
                                       </div>
                                  
                                  
                                  <div class="form-group showinsurance" <?php if($emp->categorys == 'Insurance' ) {
                                                    }else { ?>style="display:none;"<?php } ?>>
                                          <label class="control-label col-md-3">Type of Insurance: </label>
                                         
                                              <div class="col-md-4 ac">
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <select name="insurance_company" id="insurance_company" class="form-control fsc-input insurance_company">
                                                      <option value=''>---Select Insurance Category---</option>
                                                      @foreach($insurance as $cate)
                                                        <option value='{{$cate->id}}' @if($emp->insurance_company==$cate->id) selected @endif>{{$cate->insurance_company}}</option>
                                                      @endforeach
                                                            </select>
                                                         </div>
                                                      </div>

                                                   </div>

                                                   <div class="col-md-1"><a href="#" data-toggle="modal"
                                                             data-target="#basicExampleModal3insurance" class="redius">
                                                                 <i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp;
                                                        <a href="#" data-toggle="modal" data-target="#basicExampleModalinsurance" 
                                                        class="redius"><i class="fa fa-minus"></i></a> </div>
                                       </div>!-->

                                                        <div class="form-group {{ $errors->has('entity') ? ' has-error' : '' }}" id="entitys">
                                                            <label class="control-label col-md-3">Type of Business : <span class="star-required">*</span></label>
                                                            <div class="col-md-5 ac">
                                                                <div class="row">


                                                                    <div class="col-md-12">
                                                                        <select name="entity" id="entity" class="form-control fsc-input entitys">
                                                                            <option value=''>---Select---</option>
                                                                            <option value='Business' <?php if ($emp->entity == 'Business') {
                                                                                echo 'selected';
                                                                            }?>>Business (Service we provide)
                                                                            </option>
                                                                            <option value='Other Business' <?php if ($emp->entity == 'Other Business') {
                                                                                echo 'selected';
                                                                            }?>>Other Business
                                                                            </option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>

                                                        <div class="form-group businessshow {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" id="business_catagory_name_2" <?php if($emp->entity != 'Business'){ ?>style="display:none;" <?php } ?>>
                                                            <label class="control-label col-md-3 serviceprovide">Business (Service we provide): <span class="star-required">*</span></label>

                                                            <div class="col-md-8 ac" style="width: 42.1%;">
                                                                <div class="row">
                                                                    <div class="col-md-12 multiselectbox">
                                                                        <?php  $str2s = $emp->business_catagory_name;
                                                                        $splittedstring2 = explode(",", $str2s);
                                                                        // print_r($splittedstring2);?>
                                                                        <select name="business_catagory_name[]" id="business_catagory_name" multiple class="js-example-tags  form-control fsc-input category1">
                                                                            <option value=''>---Select Business Category---</option>
                                                                            @foreach($category as $cate)
                                                                                <option value='{{$cate->id}}' @foreach($splittedstring2 as $key => $value) @if($value ==$cate->id) selected @endif @endforeach>{{$cate->business_cat_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                @if ($errors->has('business_catagory_name'))
                                                                    <span class="help-block">
                                             <strong>{{ $errors->first('business_catagory_name') }}</strong>
                                             </span>
                                                                @endif
                                                            </div>

                                                        </div>


                                                        <div class="form-group categoryshow " id="business_catagory_name_2" <?php if($emp->entity != 'Other Business'){ ?>style="display:none;" <?php } ?>>

                                                            <label class="control-label col-md-3">Other Business: <span class="star-required">*</span></label>

                                                            <div class="col-md-5 ac">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <select class="form-control categorys2" name="categorys">
                                                                            <option value="">Select</option>
                                                                            @foreach($businesstypes as $cate)
                                                                                <option value="{{$cate->businesstype}}" <?php if ($emp->categorys == $cate->businesstype) {
                                                                                    echo 'selected';
                                                                                }?>>{{$cate->businesstype}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-1"><a href="#" data-toggle="modal"
                                                                                     data-target="#otherbusinessModal" class="redius">
                                                                    <i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp;
                                                                <a href="#" data-toggle="modal" data-target="#otherbusinessModal4"
                                                                   class="redius"><i class="fa fa-minus"></i></a></div>

                                                        </div>

                                                        <div class="form-group showutility" <?php if($emp->categorys != 'Utility'){ ?> style="display:none;" <?php } ?>>
                                                            <label class="control-label col-md-3">Utilities Category: </label>


                                                            <div class="col-md-5 ac">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <select name="utilitiesname" id="utilitiesname" class="form-control fsc-input category1 utility">
                                                                            <option value=''>---Select Utilities Category---</option>
                                                                            @foreach($utilities as $cate)
                                                                                <option value='{{$cate->id}}' <?php if ($emp->utilitiesname == $cate->id) {
                                                                                    echo 'selected';
                                                                                }?>>{{$cate->utilitiesname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1"><a href="#" data-toggle="modal"
                                                                                     data-target="#basicExampleModalUtilities" class="redius">
                                                                    <i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp;
                                                                <a href="#" data-toggle="modal" data-target="#basicExampleModal4"
                                                                   class="redius"><i class="fa fa-minus"></i></a></div>
                                                        </div>


                                                        <div class="form-group showinsurance" <?php if($emp->categorys != 'Insurance'){ ?> style="display:none;" <?php } ?>>
                                                            <label class="control-label col-md-3">Type of Insurance: </label>


                                                            <div class="col-md-5 ac">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <select name="insurance_company" id="insurance_company" class="form-control fsc-input insurance_company">
                                                                            <option value=''>---Select Insurance Category---</option>
                                                                            @foreach($insurance as $cate)
                                                                                <option value='{{$cate->id}}'>{{$cate->insurance_company}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-1"><a href="#" data-toggle="modal"
                                                                                     data-target="#basicExampleModal3insurance" class="redius">
                                                                    <i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp;
                                                                <a href="#" data-toggle="modal" data-target="#basicExampleModalinsurance"
                                                                   class="redius"><i class="fa fa-minus"></i></a></div>
                                                        </div>


                                                        <div class="form-group {{ $errors->has('business_name') ? ' has-error' : '' }}">
                                                            <label class="control-label col-md-3">Vendor Name : <span class="star-required">*</span></label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <div class="">
                                                                    <div class="business_name">
                                                                        <input type="text" class="form-control" placeholder="Vendor Name" value="{{$emp->business_name}}" id="business_name" name="business_name">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @if ($errors->has('business_name'))
                                                                <span class="help-block">
											<strong>{{ $errors->first('business_name') }}</strong>
										</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Address 1 :</label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" value="{{$emp->address1}}" placeholder="Address 1" name="address" id="address">
                                                                @if ($errors->has('address'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('address') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="form-group {{ $errors->has('address1') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Address 2 : </label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" value="{{$emp->address2}}" placeholder="Address 2" name="address1" id="address1">

                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('countryId') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Country : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <div class="dropdown">
                                                                            <select name="countryId" style="width:77%;" id="countries_states1" data-country="{{$emp->countryId}}" class="form-control bfh-countries fsc-input">

                                                                            </select>

                                                                        </div>
                                                                        @if ($errors->has('countryId'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('countryId') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">City / State / Zip : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$emp->city}}">
                                                                        @if ($errors->has('city'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-3" style="width:24%">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-state="{{$emp->stateId}}" data-country="countries_states1">
                                                                            </select>
                                                                            @if ($errors->has('stateId'))
                                                                                <span class="help-block">
											<strong>{{ $errors->first('stateId') }}</strong>
										</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3" style="width:27%">
                                                                        <input type="text" class="form-control fsc-input zip" id="zip" name="zip" value="{{$emp->zip}}" maxlength="6" placeholder="Zip">
                                                                        @if ($errors->has('zip'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('zip') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Email :</label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="Email ID" value="{{$emp->email}}">
                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                                                @endif   </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone 1 # : </label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="telephone" value="{{$emp->telephoneNo1}}" type="tel"
                                                                               id="motelephoneile" class="form-control" placeholder="(999) 999-9999">
                                                                        @if ($errors->has('telephone'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('telephone') }}</strong>
										</span>
                                                                        @endif

                                                                    </div>
                                                                    <div class="col-md-4" style="width:24%">
                                                                        <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business" <?php if ($emp->telephoneNo1Type == 'Business') {
                                                                                echo 'Selected';
                                                                            }?>>Business
                                                                            </option>
                                                                            <option value="Office" <?php if ($emp->telephoneNo1Type == 'Office') {
                                                                                echo 'Selected';
                                                                            }?>>Office
                                                                            </option>
                                                                            <option value="Technical Support" <?php if ($emp->telephoneNo1Type == 'Technical Support') {
                                                                                echo 'Selected';
                                                                            }?>>Tech. Sup.
                                                                            </option>
                                                                            <option value="Manager" <?php if ($emp->telephoneNo1Type == 'Manager') {
                                                                                echo 'Selected';
                                                                            }?>>Manager
                                                                            </option>
                                                                            <option value="Customer Service" <?php if ($emp->telephoneNo1Type == 'Customer Service') {
                                                                                echo 'Selected';
                                                                            }?>>CSR
                                                                            </option>

                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4" style="width:27%">
                                                                        <input class="form-control fsc-input" id="ext1" maxlength="5" readonly name="ext1" value="{{$emp->ext1}}" placeholder="Ext" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group showmanager" @if($emp->telephoneNo1Type !='Manager') style="display:none;" @endif>
                                                            <label class="control-label col-md-3">Name :</label>
                                                            <div class="col-md-6">
                                                                <div class="ps_main">
                                                                    <div class="ps_mr">
                                                                        <select type="text" class="form-control txtOnly" id="managermr1" name="managermr1">
                                                                            <option Value="Mr." @if($emp->managermr1 == 'Mr.') selected @endif>Mr.</option>
                                                                            <option Value="Mrs." @if($emp->managermr1 == 'Mrs.') selected @endif >Mrs.</option>
                                                                            <option Value="Miss." @if($emp->managermr1 == 'Miss.') selected @endif>Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="ps_firstname">
                                                                        <input type="text" class="form-control" placeholder="First Name" id="managerfname1" name="managerfname1" value="{{$emp->managerfname1}}">
                                                                    </div>
                                                                    <div class="ps_middlename">
                                                                        <input type="text" class="form-control" id="managermname1" placeholder="M" name="managermname1" maxlength="1" value="{{$emp->managermname1}}">
                                                                    </div>
                                                                    <div class="ps_lastname">
                                                                        <input type="text" class="form-control" id="managerlname1" name="managerlname1" placeholder="Last Name" value="{{$emp->managerlname1}}">
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone 2# :</label>
                                                            <div class="col-md-6 ac">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="motelephoneile1" value="{{$emp->telephoneNo2}}" type="tel" id="motelephoneile1" class="form-control" placeholder="(999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-4" style="width:24%">
                                                                        <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business" <?php if ($emp->telephoneNo2Type == 'Business') {
                                                                                echo 'Selected';
                                                                            }?>>Business
                                                                            </option>
                                                                            <option value="Office" <?php if ($emp->telephoneNo2Type == 'Office') {
                                                                                echo 'Selected';
                                                                            }?>>Office
                                                                            </option>
                                                                            <option value="Technical Support" <?php if ($emp->telephoneNo2Type == 'Technical Support') {
                                                                                echo 'Selected';
                                                                            }?>>Tech. Sup.
                                                                            </option>
                                                                            <option value="Manager" <?php if ($emp->telephoneNo2Type == 'Manager') {
                                                                                echo 'Selected';
                                                                            }?>>Manager
                                                                            </option>
                                                                            <option value="Customer Service" <?php if ($emp->telephoneNo2Type == 'Customer Service') {
                                                                                echo 'Selected';
                                                                            }?>>CSR
                                                                            </option>

                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4" style="width:27%">
                                                                        <input class="form-control fsc-input" id="ext2" maxlength="5" value="{{$emp->ext2}}" readonly name="ext2" placeholder="Ext" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group showmanager2" @if($emp->telephoneNo2Type !='Manager') style="display:none;" @endif>
                                                            <label class="control-label col-md-3">Name :</label>
                                                            <div class="col-md-6">
                                                                <div class="ps_main">
                                                                    <div class="ps_mr">
                                                                        <select type="text" class="form-control txtOnly" id="managermr2" name="managermr2">
                                                                            <option Value="Mr." @if($emp->managermr2 =='Mr.') selected @endif>Mr.</option>
                                                                            <option Value="Mrs." @if($emp->managermr2 =='Mrs.') selected @endif>Mrs.</option>
                                                                            <option Value="Miss." @if($emp->managermr2 =='Miss.') selected @endif>Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="ps_firstname">
                                                                        <input type="text" class="form-control" placeholder="First Name" id="managerfname2" name="managerfname2" value="{{$emp->managerfname2}}">
                                                                    </div>
                                                                    <div class="ps_middlename">
                                                                        <input type="text" class="form-control" id="managermname2" placeholder="M" name="managermname2" maxlength="1" value="{{$emp->managermname2}}">
                                                                    </div>
                                                                    <div class="ps_lastname">
                                                                        <input type="text" class="form-control" id="managerlname2" name="managerlname2" placeholder="Last Name" value="{{$emp->managerlname2}}">
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="form-group {{ $errors->has('business_fax') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Fax # : </label>
                                                            <div class="col-md-2 ac">
                                                                <input type="text" class="form-control fsc-input" value="{{$emp->fax}}" id="business_fax" name="business_fax" placeholder="(999) 999-9999">
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                                                            <label class="control-label col-md-3">Website : </label>
                                                            <div class="col-md-5 ac" style="width: 42.1%;">
                                                                <input type="text" class="form-control fsc-input" id="website" value="{{$emp->website}}" name="website" placeholder="Website address">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">How Bank Statement Shows? : </label>
                                                            <div class="col-md-5 ac" style="width:42.2%">
                                                                <input type="text" class="form-control fsc-input" id="howroshow" value="{{$emp->howtoshow}}" name="howtoshow" placeholder="How Bank Statement Shows">
                                                            </div>
                                                        </div>

                                                        <div class="Branch">
                                                            <h1>Contact Information</h1>
                                                        </div>

                                                        <br/>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Name :</label>
                                                                <div class="col-md-6">


                                                                    <div class="ps_main">
                                                                        <div class="ps_mr {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                                                            <select type="text" class="form-control fsc-input category1" id="minss" name="minss">

                                                                                <option Value="Mr.">Mr.</option>
                                                                                <option Value="Mrs.">Mrs.</option>
                                                                                <option Value="Miss.">Miss.</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="ps_firstname">
                                                                            <input type="text" class="form-control" placeholder="First Name" value="{{$emp->firstName}}" id="firstname" name="firstname">
                                                                        </div>
                                                                        <div class="ps_middlename {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                                                            <input type="text" class="form-control" id="middlename" placeholder="M" name="middlename" maxlength="1" value="{{$emp->middleName}}">
                                                                        </div>
                                                                        <div class="ps_lastname {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                                                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{{$emp->lastName}}">

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">

                                                                    <?php $strpos = $emp->positions; $splittedstringpos = explode(",", $strpos);?>
                                                                    <select class="js-example-tags  form-control fsc-input category1" multiple="multiple" name="positions[]" id="vendor_product2">
                                                                        @foreach($positions1 as $cur)
                                                                            <option value="{{$cur->id}}" @foreach($splittedstringpos as $key => $value) @if($value ==$cur->id) selected @endif @endforeach >{{$cur->position}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                            </div>
                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-3">
                                                                    <input type="checkbox" class="" id="billingtoo123" name="billingtoo123" onclick="FillBilling123(this.form)" value="1" @if($emp->billingtoo123 =='1') checked @endif> <label for="billingtoo123"> Same As Business Telephone</label>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Telephone 1 # :</label>
                                                                <div class="col-md-2">
                                                                    <input name="mobile_1" type="tel" id="mobile_1" class="form-control phone" placeholder="(999) 999-9999" value="<?php echo $emp->etelephone1;?>"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="width:13.2%">
                                                                    <select name="mobiletype_1" id="mobiletype_1" class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        <option value="Business" <?php if ($emp->eteletype1 == 'Business') {
                                                                            echo 'Selected';
                                                                        };?>>Business
                                                                        </option>
                                                                        <option value="Office" <?php if ($emp->eteletype1 == 'Office') {
                                                                            echo 'Selected';
                                                                        };?>>Office
                                                                        </option>
                                                                        <option value="Technical Support" <?php if ($emp->eteletype1 == 'Technical Support') {
                                                                            echo 'Selected';
                                                                        };?>>Tech. Sup.
                                                                        </option>
                                                                        <option value="Manager" <?php if ($emp->eteletype1 == 'Manager') {
                                                                            echo 'Selected';
                                                                        };?>>Manager
                                                                        </option>
                                                                        <option value="Customer Service" <?php if ($emp->eteletype1 == 'Customer Service') {
                                                                            echo 'Selected';
                                                                        };?>>CSR
                                                                        </option>

                                                                        <option value="Mobile" <?php if ($emp->eteletype1 == 'Mobile') {
                                                                            echo 'Selected';
                                                                        };?>>Mobile
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="width:13.6%">
                                                                    <input class="form-control fsc-input" id="ext2_1" maxlength="5" readonly name="ext2_1" placeholder="Ext" type="text" value="{{$emp->eext1}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group showcontactmanager1" @if($emp->eteletype1 !='Manager') style="display:none;" @endif>
                                                                <label class="control-label col-md-3">Name :</label>
                                                                <div class="col-md-6">
                                                                    <div class="ps_main">
                                                                        <div class="ps_mr">
                                                                            <select type="text" class="form-control txtOnly" id="contactmanagermr1" name="contactmanagermr1">
                                                                                <option Value="Mr." @if($emp->contactmanagermr1 =='Mr.') selected @endif>Mr.</option>
                                                                                <option Value="Mrs." @if($emp->contactmanagermr1 =='Mrs.') selected @endif>Mrs.</option>
                                                                                <option Value="Miss." @if($emp->contactmanagermr1 =='Miss.') selected @endif>Miss.</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="ps_firstname">
                                                                            <input type="text" class="form-control" placeholder="First Name" id="contactmanagerfname1" name="contactmanagerfname1" value="{{$emp->contactmanagerfname1}}">
                                                                        </div>
                                                                        <div class="ps_middlename">
                                                                            <input type="text" class="form-control" id="contactmanagermname1" placeholder="M" name="contactmanagermname1" maxlength="1" value="{{$emp->contactmanagermname1}}">
                                                                        </div>
                                                                        <div class="ps_lastname">
                                                                            <input type="text" class="form-control" id="contactmanagerlname1" name="contactmanagerlname1" placeholder="Last Name" value="{{$emp->contactmanagerlname1}}">
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Telephone 2 # :</label>
                                                                <div class="col-md-2">
                                                                    <input name="mobile_2" type="tel" id="mobile_2" class="form-control phone" placeholder="(999) 999-9999" value="{{$emp->etelephone2}}"/>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="width:13.2%">
                                                                    <select name="mobiletype_2" id="mobiletype_2" class="form-control fsc-input">
                                                                        <option value="">Select</option>
                                                                        <option value="Business" <?php if ($emp->eteletype2 == 'Business') {
                                                                            echo 'Selected';
                                                                        };?>>Business
                                                                        </option>
                                                                        <option value="Office" <?php if ($emp->eteletype2 == 'Office') {
                                                                            echo 'Selected';
                                                                        };?>>Office
                                                                        </option>
                                                                        <option value="Technical Support" <?php if ($emp->eteletype2 == 'Technical Support') {
                                                                            echo 'Selected';
                                                                        };?>>Tech. Sup.
                                                                        </option>
                                                                        <option value="Manager" <?php if ($emp->eteletype2 == 'Manager') {
                                                                            echo 'Selected';
                                                                        };?>>Manager
                                                                        </option>
                                                                        <option value="Customer Service" <?php if ($emp->eteletype2 == 'Customer Service') {
                                                                            echo 'Selected';
                                                                        };?>>CSR
                                                                        </option>

                                                                        <option value="Mobile" <?php if ($emp->eteletype2 == 'Mobile') {
                                                                            echo 'Selected';
                                                                        };?>>Mobile
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="width:13.6%">
                                                                    <input class="form-control fsc-input" id="ext2_2" maxlength="5" readonly name="ext2_2" placeholder="Ext" type="text" value="{{$emp->eext2}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group showcontactmanager2" @if($emp->eteletype2 !='Manager') style="display:none;" @endif>
                                                                <label class="control-label col-md-3">Name :</label>
                                                                <div class="col-md-6">
                                                                    <div class="ps_main">
                                                                        <div class="ps_mr">
                                                                            <select type="text" class="form-control txtOnly" id="contactmanagermr2" name="contactmanagermr2">
                                                                                <option Value="Mr." @if($emp->contactmanagermr2 =='Mr.') selected @endif>Mr.</option>
                                                                                <option Value="Mrs." @if($emp->contactmanagermr2 =='Mrs.') selected @endif>Mrs.</option>
                                                                                <option Value="Miss." @if($emp->contactmanagermr2 =='Miss.') selected @endif >Miss.</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="ps_firstname">
                                                                            <input type="text" class="form-control" placeholder="First Name" id="contactmanagerfname2" name="contactmanagerfname2" value="{{$emp->contactmanagerfname2}}">
                                                                        </div>
                                                                        <div class="ps_middlename">
                                                                            <input type="text" class="form-control" id="contactmanagermname2" placeholder="M" name="contactmanagermname2" maxlength="1" value="{{$emp->contactmanagermname2}}">
                                                                        </div>
                                                                        <div class="ps_lastname">
                                                                            <input type="text" class="form-control" id="contactmanagerlname2" name="contactmanagerlname2" placeholder="Last Name" value="{{$emp->contactmanagerlname2}}">
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-3">
                                                                    <input type="checkbox" class="" id="billingtoo" name="billingtoo" onclick="FillBilling(this.form)" value="1" @if($emp->billingtoo =='1') checked @endif> <label for="billingtoo">Same As Above Business Fax</label>
                                                                </div>

                                                            </div>
                                                            <div class="form-group {{ $errors->has('contact_fax') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Fax No. :</label>
                                                                <div class="col-md-2" style="width:17% !Important;">
                                                                    <input name="contact_fax_1" placeholder="(999) 999-9999" type="tel" value="{{$emp->efax}}" id="contact_fax" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999"/>

                                                                </div>
                                                            </div>
                                                            <div class="form-group" style="margin-bottom:0px;">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-4">
                                                                    <input type="checkbox" class="" id="emailbli2" name="emailbli2" onclick="emailbl12(this.form)" @if($emp->emailbli2 =='1') checked @endif> <label for="emailbli2">Same As Above Email</label>
                                                                </div>

                                                            </div>
                                                            <div class="form-group  {{ $errors->has('email_1') ? ' has-error' : '' }}">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-5" style="width:43.45%">
                                                                    <input type="text" class="form-control" id="email_1" name="email_1" value="{{$emp->email}}" placeholder="Email ID">

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Account No. :</label>
                                                                <div class="col-md-5" style="width:43.45%">
                                                                    <input type="text" class="form-control" id="accno" name="accno" value="{{$emp->accno}}" placeholder="Account No.">
                                                                </div>
                                                            </div>


                                                            <div class="Branch">
                                                                <h1>Product Information</h1>
                                                            </div>
                                                            <br/>
                                                            <div class="after-add-more">
                                                                <div class="form-group {{ $errors->has('productid') ? ' has-error' : '' }}">
                                                                    <label class="control-label col-md-3">Type of Product : <span class="star-required">*</span></label>
                                                                    <div class="col-md-6">
                                                                        <?php  $str = $emp->product;
                                                                        $splittedstring = explode(",", $str);?>
                                                                        <select class="js-example-tags form-control" style="width: 86.5%;" id="vendor_product" name="productid[]" multiple="multiple">
                                                                            @foreach($products1 as $cur)
                                                                                <option value="{{$cur->id}}" @foreach($splittedstring as $key => $value) @if($value ==$cur->id) selected @endif @endforeach >{{$cur->productname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if ($errors->has('productid'))
                                                                            <span class="help-block">
											<strong>{{ $errors->first('productid') }}</strong>
										</span>
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a></div>
                                                                </div>
                                                            </div>
                                                            <div class="Branch" style="background:skyblue;padding:0"></div>
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <div class="col-md-6">
                                                                        <label class="control-label col-md-3">Enter By :</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" id="vendortype" name="vendortype" value="{{ucwords($emp->vendortype)}}">
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <input type="text" class="form-control" id="" name="" value="{{$emp->vendorid}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <?php $unixtime = $emp->vendordate; $date = date("m-d-Y", strtotime($unixtime)); $time = date("l", strtotime($unixtime));$time1 = date("g:i a", strtotime($unixtime));?>
                                                                        <label class="control-label col-md-4" style=" width:33%; padding:10px 25px 0 0; ">Date / Day / Time :</label>
                                                                        <div class="col-md-8" style=" padding:0; ">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="row">
                                                                                        <input type="text" class="form-control" id="" readonly name="" value="{{$date}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" id="" name="" readonly value="{{$time}}">
                                                                                </div>
                                                                                <div class="col-md-4" style=" padding-left:0; ">
                                                                                    <input type="text" class="form-control" id="" name="" readonly value="{{$time1}}">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="Branch" style=" background:skyblue; ">
                                                                <h1>Admin Use Only</h1>
                                                            </div>
                                                            <br/>

                                                            <div class="after-add-more">
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Status</label>
                                                                    <div class="col-md-5" style="width:14%">
                                                                        <select name="status" id="status" class="form-control fsc-input" onchange="this.className=this.options[this.selectedIndex].className"
                                                                                name="agentstatus[]" id="agentstatus">
                                                                            <option value="">Select</option>
                                                                            <option class="greenText" value="1" @if($emp->check =='1') selected @endif>Active</option>
                                                                            <option class="redText" value="0" @if($emp->check =='0') selected @endif>Inactive</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Account Code</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <select class="js-example-tags form-control" id="accountcode" name="accountcode">
                                                                            <option value="">Select</option>
                                                                            @foreach($accountcode as $cur)
                                                                                <option value="{{$cur->accountcode}}" @if($emp->accountcode ==$cur->accountcode) selected @endif>{{$cur->accountcode}} ({{$cur->account_name}})</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Account Name</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" id="account_name" name="account_name" value="{{$emp->account_name}}" placeholder="Account Name">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Account Belong To</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" id="account_belongs_to" name="account_belongs_to" value="{{$emp->account_belongs_to}}" placeholder="Account Belong To">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group  ">
                                                                    <label class="control-label col-md-3">Note</label>
                                                                    <div class="col-md-5" style="width:43.45%">
                                                                        <input type="text" class="form-control" id="note" name="note" value="{{$emp->notes}}" placeholder="Note">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            </br>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch" style="text-align:left; padding-left:15px;">
                                                                    <h1 class="text-center">Conversation List</h1>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">

                                                                <div class="clear"></div>
                                                                <table class="table table-hover table-bordered dataTable no-footer">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>No.</th>

                                                                        <th> Date</br> Day</br> Time</th>

                                                                        <th>Related To</th>
                                                                        <th>Description</th>
                                                                        <th>Notes</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <?php
                                                                    $cnt = count($conversation);
                                                                    if($cnt > 0)
                                                                    {
                                                                    $cnt = 0;
                                                                    ?>
                                                                    @foreach($conversation as $conversation)
                                                                        <tr>
                                                                            <td style="text-align:center;">{{$loop->index+1}} </td>
                                                                            <td style="text-align:center;">{{$conversation->creattiondate}}<br> {{$conversation->day}} <br> {{$conversation->time}}</td>
                                                                            <td style="text-align:center;">{{$conversation->relatednames}} </td>
                                                                            <td>{{$conversation->condescription}} </td>
                                                                            <td>{{$conversation->connotes}} </td>
                                                                            <td style="text-align:center;">

                                                                                <a class="btn-action btn-view-edit conversationID" data-id="{{$conversation->id}}"><i class="fa fa-edit"></i></a>
                                                                            </td>
                                                                            @endforeach
                                                                        </tr>
                                                                        <?php
                                                                        }
                                                                        else
                                                                        {
                                                                        ?>
                                                                        <tr>
                                                                            <td style="text-align:center;" colspan="7">No records found</td>
                                                                        </tr>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        </tbody>

                                                                </table>
                                                            </div>

                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="Branch" style="text-align:left; padding-left:15px;">
                                                                    <h1 class="text-center">Notes List</h1>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">

                                                                <div class="clear"></div>
                                                                <table class="table table-hover table-bordered dataTable no-footer">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>No.</th>

                                                                        <th> Date</br> Day</br> Time</th>

                                                                        <th>Related To</th>
                                                                        <th>Description</th>
                                                                        <th>Notes</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <?php
                                                                    $cnt = count($notesdata);
                                                                    if($cnt > 0)
                                                                    {
                                                                    $cnt = 0;
                                                                    ?>
                                                                    @foreach($notesdata as $conversation)
                                                                        <tr>
                                                                            <td style="text-align:center;">{{$loop->index+1}} </td>
                                                                            <td style="text-align:center;">{{$conversation->creattiondate}}<br> {{$conversation->noteday}} <br> {{$conversation->notetime}}</td>
                                                                            <td style="text-align:center;">{{$conversation->notesrelated}} </td>
                                                                            <td>{{$conversation->notesrelatedcat}} </td>
                                                                            <td>{{$conversation->notes}} </td>
                                                                            <td style="text-align:center;">

                                                                                <a class="btn-action btn-view-edit notesID" data-id="{{$conversation->id}}"><i class="fa fa-edit"></i></a>
                                                                            </td>
                                                                            @endforeach
                                                                        </tr>
                                                                        <?php
                                                                        }
                                                                        else
                                                                        {
                                                                        ?>
                                                                        <tr>
                                                                            <td style="text-align:center;" colspan="7">No records found</td>
                                                                        </tr>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        </tbody>

                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="col-md-7 col-md-offset-3">
                                                        <div class="row">
                                                            <div class="col-md-3" style="margin-left:7px !important;">
                                                                <button type="submit" class="btn_new_save">Save</button>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="{{url('/fac-Bhavesh-0554/vendor')}}" class="btn_new_cancel">Cancel</a>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <a class="btn btn-danger" href="#" style="width:100%;padding:9px !important;" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                        {event.preventDefault();document.getElementById('delete-id-{{$emp->id}}').submit();} else{event.preventDefault();}">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </form>
                                <form action="{{route('vendor.destroy',$emp->id)}}" method="post" style="display:none" id="delete-id-{{$emp->id}}">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->


        <!--Modal Conversation Update Start-->
        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('vendor.updatecoversation')}}">
            {{csrf_field()}}


            <div id="conversationModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Conversation Sheet</h4>
                        </div>
                        <div class="modal-body">
                            <div style="max-width:700px; margin:0px auto;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right"><strong>Date:</strong></label>
                                                <div class="col-md-7">
                                                    <!--<input type="text" class="form-control effective_date2"/>-->
                                                    <input type="hidden" name="CovID" id="CovID" class="form-control">
                                                    <input type="text" name="date" id="creattiondate1" class="form-control" placeholder="Date" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right"><strong>Day:</strong></label>
                                                <div class="col-md-7">
                                                    <input type="text" name="day" id="day1" class="form-control" placeholder="Day" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3"><strong>Time:</strong></label>
                                                <div class="col-md-8">
                                                    <input type="text" name="time" id="time1" class="form-control" placeholder="Time" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('type_user') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Select:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control fsc-input type_user" name="type_user" id="type_user1" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                            @if($errors->has('type_user'))
                                                <span class="help-block">
                        <strong>{{ $errors->first('type_user') }}</strong>
                        </span>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group clientid2" id="clientid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="clientid" id="clientid1">
                                                <option>Select</option>
                                                @foreach($listclient as $client)
                                                    <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group employeeuserid2" id="employeeuserid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="employeeuserid" id="employeeuserid1">
                                                <option>Select</option>
                                                @foreach($listemployeeuser as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group vendorid2" id="vendorid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Vendor:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="vendorid" id="vendorid1">
                                                <option>Select</option>
                                                @foreach($listvendoe as $vendoe)
                                                    <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group otherid2" id="otherid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Other :</strong></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="otherid" id="otherid1">
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('conrelatedname') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Related to:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control" name="conrelatedname" id="conrelatedname1" required>
                                                    <option>Select</option>
                                                    @foreach($relatedNames as $relatedNames)
                                                        <option value="{{$relatedNames->id}}">{{$relatedNames->relatednames}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($errors->has('conrelatedname'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('conrelatedname') }}</strong>
                            </span>
                                            @endif
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                                        <div class="col-md-9">
                                            <textarea rows="3" cols="12" class="form-control" name="condescription" id="condescription1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Note</strong></label>
                                        <div class="col-md-9">
                                            <textarea rows="3" cols="12" class="form-control" name="connotes" id="connotes1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" id="recInsert" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <!--Modal Conversation Update End-->


        <!--Modal Notes Update Start-->
        <div id="notesModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:850px;">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('vendor.updatenotes')}}">
                {{csrf_field()}}


                <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> Note</h4>
                        </div>
                        <div class="modal-body">
                            <div style="max-width:700px; margin:0px auto;">
                                <div class="row mt-3" style="margin-top:20px;">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right" style="padding-right:28px!important;"><strong>Date:</strong></label>
                                                <div class="col-md-6" style="padding-left:12px!important;">
                                                    <!--<input type="text" class="form-control effective_date2"/>-->
                                                    <input type="hidden" name="NoteID" id="NoteID">
                                                    <input type="text" name="notedate" id="date2" class="form-control" value="{{date('m-d-Y')}}" placeholder="Date" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-4 text-right"><strong>Day:</strong></label>
                                                <div class="col-md-5">
                                                    <input type="text" name="noteday" id="day2" class="form-control" placeholder="Day" value="{{date('l')}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3"><strong>Time:</strong></label>
                                                <div class="col-md-5">
                                                    <input type="text" name="notetime" id="time2" class="form-control" value="{{date("g:i a")}}" placeholder="Time" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Select:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control fsc-input notetype_user" name="notetype_user" id="notetype_user1" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteclientid2" id="noteclientid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="noteclientid" id="noteclientid1">
                                                <option>Select</option>
                                                @foreach($listclient as $client)
                                                    <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteemployeeuserid2" id="noteemployeeuserid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="noteemployeeuserid" id="noteemployeeuserid1">
                                                <option>Select</option>
                                                @foreach($listemployeeuser as $employee)
                                                    <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group notevendorid2" id="notevendorid" style="display:none;">
                                    <div class="row">

                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Vendor :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="notevendorid" id="notevendorid1">
                                                <option>Select</option>
                                                @foreach($listvendoe as $vendoe)
                                                    <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteotherid2" id="noteotherid" style="display:none;">
                                    <div class="row">

                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Other :</strong></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="noteotherid" id="noteotherid1">
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('notesrelated') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Related to :</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control" name="notenotesrelatedname" id="noterelated1" required>
                                                    <option>Select</option>
                                                    @foreach($NotesNames as $notesRelated)
                                                        <option value="{{$notesRelated->id}}">{{$notesRelated->notesrelated}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($errors->has('notesrelated'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('notesrelated') }}</strong>
                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalNotes" class="redius"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Types of Note:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="notesrelatedcat" id="notesrelatedcat1" required>
                                                <option>Permenant Note</option>
                                                <option>Temporary Note</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                                        <div class="col-md-9" style="padding-right:0px;">
                                            <textarea rows="3" cols="12" class="form-control" name="notes" id="notes1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!--Modal Notes Update End-->

        <div class="modal fade" id="basicExampleModal3insurance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Insurance</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="ajaxi">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <input type="text" id="newopti" name="newopti" class="form-control" placeholder="Insurance Company  Name"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="addopti" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExampleModalinsurance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background:#038ee0;">
                        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Insurance Company
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h4>
                    </div>
                    <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                        <div class="curency curency_ref" id="div">
                            @foreach($insurance as $cur)
                                <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                                    <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                        <a class="delete3" style="color:#000;" id="{{$cur->id}}">{{$cur->insurance_company}}
                                            <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="basicExampleModalUtilities" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Utilities</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="ajax3">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Utilities Name"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="addopt3" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="basicExampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background:#038ee0;">
                        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Utilities
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h4>
                    </div>
                    <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                        <div class="curency curency_ref" id="div">
                            @foreach($utilities as $cur)
                                <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                                    <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                        <a class="delete2" style="color:#000;" id="{{$cur->id}}">{{$cur->utilitiesname}}
                                            <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function () {
                $(document).on('click', '.delete2', function () {
                    var id = $(this).attr('id');
                    if (confirm("Are you sure you want to Delete this data?")) {
                        $.ajax({
                            url: "{{route('removeutilities.removeutilitiess')}}",
                            mehtod: "get",
                            data: {id: id},
                            success: function (data) {
                                // alert(data);
                                $('#cur_' + id).remove();
                                $("#vendor_product").load(" #vendor_product > *");
                                window.location.reload();
                            }
                        })
                    } else {
                        return false;
                    }
                });

                $(document).on('change', '.entitys', function () {
                    //serviceprovide,otherbusiness
                    var thiss = $(this).val();
                    if (thiss == 'Business') {
                        $('.categoryshow').hide();
                        $('.businessshow').show();

                        $('.showinsurance').hide();
                        $('.showutility').hide();

                    } else if (thiss == 'Other Business') {
                        $('.categoryshow').show();
                        $('.businessshow').hide();

                        $('.showutility').hide();
                        $('.showinsurance').hide();
                    } else {
                        $('.categoryshow').hide();
                        $('.businessshow').hide();
                        $('.showinsurance').hide();
                        $('.showutility').hide();

                    }
                });


                $(document).on('click', '.delete3', function () {
                    var id = $(this).attr('id');
                    if (confirm("Are you sure you want to Delete this data?")) {
                        $.ajax({
                            url: "{{route('removeinsurance.removeinsurances')}}",
                            mehtod: "get",
                            data: {id: id},
                            success: function (data) {
                                // alert(data);
                                $('#cur_' + id).remove();
                                $("#vendor_product").load(" #vendor_product > *");
                                window.location.reload();
                            }
                        })
                    } else {
                        return false;
                    }
                });

                $(function () {
                    $('#addopt3').click(function () { //alert();
                        var newopt = $('#newopt').val();
                        if (newopt == '') {
                            alert('Please enter something!');
                            return;
                        }

                        //check if the option value is already in the select box
                        $('#vendor_product option').each(function (index) {
                            if ($(this).val() == newopt) {
                                alert('Duplicate option, Please enter new!');
                            }
                        })
                        $.ajax({
                            type: "post",
                            url: "{!!route('utilities.utilitiess')!!}",
                            dataType: "json",
                            data: $('#ajax3').serialize(),
                            success: function (data) {
                                alert('Successfully Added');
                                $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                                $("#div").load(" #div > *");
                                $("#newopt").val('');
                                window.location.reload();
                            },
                            error: function (data) {
                                alert("Error")
                            }
                        });

                        $('#basicExampleModalUtilities').modal('hide');
                    });
                });

                $(function () {
                    $('#addopti').click(function () { //alert();
                        var newopt = $('#newopti').val();
                        if (newopt == '') {
                            alert('Please enter something!');
                            return;
                        }

                        //check if the option value is already in the select box
                        $('#vendor_product option').each(function (index) {
                            if ($(this).val() == newopt) {
                                alert('Duplicate option, Please enter new!');
                            }
                        })
                        $.ajax({
                            type: "post",
                            url: "{!!route('insurance.insurances')!!}",
                            dataType: "json",
                            data: $('#ajaxi').serialize(),
                            success: function (data) {
                                alert('Successfully Added');
                                $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                                $("#div").load(" #div > *");
                                $("#newopti").val('');
                                window.location.reload();
                            },
                            error: function (data) {
                                alert("Error")
                            }
                        });

                        $('#basicExampleModal3insurance').modal('hide');
                    });
                });
                $(document).on('change', '.categorys2', function () {
                    var thiss = $(this).val();
                    if (thiss == 'Utility') {
                        $('.showutility').show();
                        $('.showinsurance').hide();

                    } else if (thiss == 'Insurance') {
                        $('.showutility').hide();
                        $('.showinsurance').show();
                    } else {
                        $('.showutility').hide();
                        $('.showinsurance').hide();
                    }

                });
                $('.type_user').on('change', function () {
                    //otherid,clientid,employeeuserid,vendorid
                    //Client,EE-User,Vendor,Other
                    var thisss = $('.type_user').val();
                    if (thisss == 'Client') {
                        $('#clientid').show();
                        $('#employeeuserid').hide();
                        $('#vendorid').hide();
                        $('#otherid').hide();
                    } else if (thisss == 'EE-User') {
                        $('#clientid').hide();
                        $('#employeeuserid').show();
                        $('#vendorid').hide();
                        $('#otherid').hide();
                    } else if (thisss == 'Vendor') {
                        $('#clientid').hide();
                        $('#employeeuserid').hide();
                        $('#vendorid').show();
                        $('#otherid').hide();
                    } else if (thisss == 'Other') {
                        $('#clientid').hide();
                        $('#employeeuserid').hide();
                        $('#vendorid').hide();
                        $('#otherid').show();
                    }
                });
            });

            $('.notetype_user').on('change', function () {
                //otherid,clientid,employeeuserid,vendorid
                //Client,EE-User,Vendor,Other
                var thisss = $('.notetype_user').val();
                if (thisss == 'Client') {
                    $('#noteclientid').show();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'EE-User') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').show();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'Vendor') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').show();
                    $('#noteotherid').hide();
                } else if (thisss == 'Other') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').show();
                }
            });

        </script>
        <script type="text/javascript">
            $(".notesID").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#notesid").val(ids);
                $('#notesModal').modal('show');
                //console.log(ids);
                $.get('{!!URL::to('getNotesemp')!!}?documentid=' + ids, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        //console.log(data.id);exit;
                        $('#NoteID').val(data.id);
                        $('#creattiondate1').val(data.creattiondate);
                        $('#noteday1').val(data.noteday);
                        $('#notetime1').val(data.notetime);
                        $('#notetype_user1').val(data.notetype_user);
                        $('#noteclientid1').val(data.noteclientid);
                        $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                        $('#notevendorid1').val(data.notevendorid);
                        $('#noteotherid1').val(data.noteotherid);
                        $('#noterelated1').val(data.noterelated);
                        $('#notesrelatedcat1').val(data.notesrelatedcat);
                        $('#notes1').val(data.notes);


                        var thisss = $('#notetype_user1').val();
                        if (thisss == 'Client') {
                            $('.noteclientid2').show();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'EE-User') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').show();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'Vendor') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').show();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'Other') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').show();
                        }

                    }


                });
            });


            $(".conversationID").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#conversationid").val(ids);
                $('#conversationModal').modal('show');
                //console.log(ids);
                $.get('{!!URL::to('getConversationdataemp')!!}?documentid=' + ids, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        //console.log(data.id);exit;
                        $('#CovID').val(data.id);
                        $('#creattiondate1').val(data.creattiondate);
                        $('#day1').val(data.day);
                        $('#time1').val(data.time);
                        $('#type_user1').val(data.type_user);
                        $('#clientid1').val(data.clientid);
                        $('#employeeuserid1').val(data.employeeuserid);
                        $('#vendorid1').val(data.vendorid);
                        $('#otherid1').val(data.otherid);
                        $('#conrelatedname1').val(data.conrelatedname);
                        $('#condescription1').val(data.condescription);
                        $('#connotes1').val(data.connotes);


                        var thisss = $('#type_user1').val();
                        if (thisss == 'Client') {
                            $('.clientid2').show();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').hide();
                            $('.otherid2').hide();
                        } else if (thisss == 'EE-User') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').show();
                            $('.vendorid2').hide();
                            $('.otherid2').hide();
                        } else if (thisss == 'Vendor') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').show();
                            $('.otherid2').hide();
                        } else if (thisss == 'Other') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').hide();
                            $('.otherid2').show();
                        }

                    }


                });
            });

        </script>

        <script>
            $('.js-example-tags').select2({
                tags: true,
                tokenSeparators: [",", " "]
            });


            $(".ext").mask("99999");
            $("#federal_id").mask("99-9999999");
            $("#contact_number11").mask("a999999");
            $(".ext").mask("99339");
            $("#zip").mask("99999");
            $("#company_zip").mask("99999");
            $("#second_zip").mask("99999");
            $('#motelephoneile1').mask("(999) 999-9999");
            $('#motelephoneile').mask("(999) 999-9999");
            $('#mobile_1').mask("(999) 999-9999");
            $('#contact_fax').mask("(999) 999-9999");
            $('#business_fax').mask("(999) 999-9999");


        </script>

        <script>
            function FillBilling(f) {
                if (f.billingtoo.checked == true) {
                    f.contact_fax_1.value = f.business_fax.value;
                } else {
                    $('#contact_fax').val('');
                }

            }

            function emailbl12(f) {
                if (f.emailbli2.checked == true) {
                    f.email_1.value = f.email.value;
                } else {
                    $('#email_1').val('');
                }

            }
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_catagory_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getRequest_1')!!}?id=' + id, function (data) {
                        if (data == "") {
//	$('#business_catagory_name_4').hide();
                        } else {
//	$('#business_catagory_name_4').show();
                        }
                        $('#image1').empty();

                        $.each(data, function (index, subcatobj) {
                            $('#image1').append('<img src="/public/category/' + subcatobj.business_cat_image + '" alt="" class="img-responsive">');
                        })
                    });
                });
            });


            $(document).ready(function () {
                $(document).on('change', '#accountcode', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getAccountcode')!!}?id=' + id, function (data) {
                        $('#account_name').val('');
                        $('#account_belongs_to').val('');
                        $.each(data, function (index, subcatobj) {
                            $('#account_name').val(subcatobj.account_name);
                            $('#account_belongs_to').val(subcatobj.account_belongs_to);
                        })
                    });
                });
            });

            $(document).ready(function () {

                $(".phone").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                    var curchr = this.value.length;
                    var curval = $(this).val();
                    if (curchr == 3 && curval.indexOf("(") <= -1) {
                        $(this).val("(" + curval + ")" + " ");
                    } else if (curchr == 4 && curval.indexOf("(") > -1) {
                        $(this).val(curval + ")-");
                    } else if (curchr == 5 && curval.indexOf(")") > -1) {
                        $(this).val(curval + "-");
                    } else if (curchr == 9) {
                        $(this).val(curval + "-");
                        $(this).attr('maxlength', '14');
                    }
                });
            });

        </script>

        <script type="text/javascript">

        </script>

        <script>

            $(document).ready(function () {
                $("select#status").change(function () {
                    var selectedCountry = $(this).children("option:selected").val();
                    var color = $("option:selected", this).attr("class");
                    $("#status").attr("class", color).addClass("form-control1 fsc-input");
                });
            });

        </script>
        <script>
            $(document).on("change", ".numeric1", function () {
                var sum = 0;
                $(".numeric1").each(function () {
                    sum += +$(this).val().replace("%", "");
                    var num = parseFloat($(this).val());
                    $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
//$(".numeric2").val(num.toFixed(2)); 
                });
                if (sum > 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                    $('#t1').show();
                } else if (sum < 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                    $('#t1').show();
                } else if (sum = 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                    $('#t1').hide();


                } else {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                }
            });
        </script>
        <script>
            function FillBilling123(f) {
                if (f.billingtoo123.checked == true) {
                    // alert();
                    if (f.telephoneNo1Type.value == 'Manager') {
                        $('.showcontactmanager1').show();
                        $('.showcontactmanager2').show();

                        f.contactmanagermr1.value = f.managermr1.value;
                        f.contactmanagerfname1.value = f.managerfname1.value;
                        f.contactmanagermname1.value = f.managermname1.value;
                        f.contactmanagerlname1.value = f.managerlname1.value;

                        f.contactmanagermr2.value = f.managermr2.value;
                        f.contactmanagerfname2.value = f.managerfname2.value;
                        f.contactmanagermname2.value = f.managermname2.value;
                        f.contactmanagerlname2.value = f.managerlname2.value;

                    } else {
                        $('.showcontactmanager1').hide();
                        $('.showcontactmanager2').hide();
                    }
                    f.mobile_1.value = f.telephone.value;
                    f.mobiletype_1.value = f.telephoneNo1Type.value;
                    f.ext2_1.value = f.ext1.value;

                    f.mobile_2.value = f.motelephoneile1.value;
                    f.mobiletype_2.value = f.telephoneNo2Type.value;
                    f.ext2_2.value = f.ext2.value;
                } else {
                    $('.showcontactmanager1').hide();
                    $('.showcontactmanager2').hide();
                    f.contactmanagermr1.value = '';
                    f.contactmanagerfname1.value = '';
                    f.contactmanagermname1.value = '';
                    f.contactmanagerlname1.value = '';

                    f.contactmanagermr2.value = '';
                    f.contactmanagerfname2.value = '';
                    f.contactmanagermname2.value = '';
                    f.contactmanagerlname2.value = '';
                    $('#showcontactmanager1').hide();
                    $('#showcontactmanager2').hide();

                    f.mobile_1.value = '';
                    f.mobiletype_1.value = '';
                    f.ext2_1.value = '';

                    f.mobile_2.value = '';
                    f.mobiletype_2.value = '';
                    f.ext2_2.value = '';

                }
            }
        </script>


        <script>
            var date = $('#ext2_1').val();
            $('#mobiletype_1').on('change', function () {

                if (this.value == 'Home') {
                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();
                } else {
                    document.getElementById('ext2_1').readOnly = true;
                    $('#ext2_1').val('');
                }
            })
        </script>


        <script>
            var date = $('#ext1').val();
            $('#telephoneNo1Type').on('change', function () {

                if (this.value == 'Office') {
                    document.getElementById('ext1').removeAttribute('readonly');
                    $('#ext1').val();
                } else if (this.value == 'Business') {
                    document.getElementById('ext1').removeAttribute('readonly');
                    $('#ext1').val();
                } else {
                    document.getElementById('ext1').readOnly = true;
                    $('#ext1').val('');
                }
            })
        </script>
        <script>
            var date = $('#ext2').val();
            $('#telephoneNo2Type').on('change', function () {

                if (this.value == 'Office') {
                    document.getElementById('ext2').removeAttribute('readonly');
                    $('#ext2').val();
                } else if (this.value == 'Business') {
                    document.getElementById('ext2').removeAttribute('readonly');
                    $('#ext2').val();
                } else {
                    document.getElementById('ext2').readOnly = true;
                    $('#ext2').val('');
                }
            })
        </script>


        <script>
            var date = $('#ext2_2').val();
            $('#mobiletype_2').on('change', function () {

                if (this.value == 'Home') {
                    document.getElementById('ext2_2').removeAttribute('readonly');
                    $('#ext2_2').val();
                } else {
                    document.getElementById('ext2_2').readOnly = true;
                    $('#ext2_2').val('');
                }
            })
        </script>

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                    //check if the option value is already in the select box
                    $('#vendor_product option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
                        type: "post",
                        url: "{!!route('product.productss')!!}",
                        dataType: "json",
                        data: $('#ajax').serialize(),
                        success: function (data) {
                            alert('Successfully Added');
                            location.reload();
                            $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                            $("#div").load(" #div > *");
                            $("#newopt").val('');
                        },
                        error: function (data) {
                            alert("Error")
                        }
                    });

                    $('#basicExampleModal').modal('hide');
                });
            });


            $(document).ready(function () {
                $(document).on('click', '.delete', function () {
                    var id = $(this).attr('id');
                    if (confirm("Are you sure you want to Delete this data?")) {
                        $.ajax({
                            url: "{{route('removeproduct.removeproducts')}}",
                            mehtod: "get",
                            data: {id: id},
                            success: function (data) {
                                // alert(data);
                                location.reload();
                                $('#cur_' + id).remove();
                                $("#vendor_product").load(" #vendor_product > *");
                                reload();
                            }
                        })
                    } else {
                        return false;
                    }
                });
            });
        </script>
        <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="ajax">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Product Name"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="addopt" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background:#038ee0;">
                        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Product
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h4>

                    </div>
                    <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                        <div class="curency curency_ref" id="div">
                            @foreach($products1 as $cur)
                                <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                                    <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">

                                        <a class="delete" style="color:#000;" id="{{$cur->id}}">{{$cur->productname}}

                                            <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
@endsection()