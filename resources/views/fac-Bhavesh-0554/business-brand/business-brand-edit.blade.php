@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <section class="page-title content-header">
            <h1>Business Brand</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">

                            <form method="post" class="form-horizontal" enctype="multipart/form-data" id="" action="{{ route('business-brand.update',$businessbrand->cid) }}">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('business_id') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control category" id="" name="business_id">
                                            @foreach($business as $bus)

                                                @if($businessbrand->business_id == $bus->id)
                                                    <option value='{{$businessbrand->business_id}}' selected>{{$businessbrand->bussiness_name}}</option>
                                                @else
                                                    <option value='{{$bus->id}}'>{{$bus->bussiness_name}}</option>
                                                @endif
                                            @endforeach

                                        </select>
                                        @if ($errors->has('business_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_id') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('business_cat_id') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control category1" id="business_cat_id" name="business_cat_id">
                                            <option value='{{$businessbrand->business_cat_id}}' selected>{{$businessbrand->business_cat_name }}</option>
                                        </select>
                                        @if ($errors->has('business_cat_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_cat_id') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('business_brand_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Brand :</label>
                                    <div class="col-md-4">
                                        <input name="business_brand_name" value="{{$businessbrand->business_brand_name}}" type="text" id="business_brand_name" class="form-control" value=""/>
                                        @if ($errors->has('business_brand_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_brand_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-md-3">Business Brand Image :</label>
                                    <div class="col-md-4">


                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="business_brand_image" style="opecity:0" placeholder="Upload Service Image" id="business_brand_image" type="file">
                                        </label>
                                        <img style="margin-left:25px;" src="https://financialservicecenter.net/public/businessbrand/{{$businessbrand->business_brand_image}}" width="100px">
                                    </div>
                                </div>
                                <input type="hidden" name="business_brand_image1" id="business_brand_image1" value="{{$businessbrand->business_brand_image}}">
                                <div class="form-group {{ $errors->has('business_brand_image') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Brand Url :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" id="link" class="form-control" value="{{$businessbrand->link}}"/>@if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/business-brand')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getRequest')!!}?id=' + id, function (data) {
                        $('#business_cat_id').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                        })

                    });

                });
            });
        </script>
@endsection()