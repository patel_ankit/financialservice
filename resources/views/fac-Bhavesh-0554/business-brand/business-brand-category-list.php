@extends('front-section.app')
@section('main-content')
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
            <h4>Brand Category List</h4>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div">
        @foreach($category as $cat)
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
            <a href="{{ url($cat->link) }}"><img src="{{URL::asset('category/')}}/{{$cat->business_cat_image}}"/></a>
            <center>
                <div class="services-tab"><a href="{{url($cat->link,$cat->id)}}"><span><div class="st_title">{{$cat->business_cat_name}}</div></span></a></div>
            </center>
        </div>
        @endforeach()
    </div>
</div>
@endsection()