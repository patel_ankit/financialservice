@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .Branch1 {
            width: 70%;
            /*! float: left; */
            /*! margin: 0 0 12px 0; */
            text-align: center;
            background: #fff2b3 !important;
            border: 2px solid #103b68;
            padding: 4px 0;
            margin: auto;
            /*! margin-top: 0px; */
            /*! margin: 0 0 10; */
        }

        .Branch1 h1 {
            padding: 0px;
            margin: 0px 0 0 0;
            color: #103b68 !important;
            font-size: 22px;
        }


    </style>

    <div class="content-wrapper">
        <section class="page-title content-header">
            <h1>Bank Information <span style="float:right:">VIEW / EDIT</span></h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('bankingsetup.update',$bankdata->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}} {{method_field('PATCH')}}


                                <div class="form-group {{ $errors->has('bankname') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Bank Name : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="bankname" type="text" id="bankname" class="form-control p-l-10" value="{{$bankdata->bankname}}"/>
                                        @if ($errors->has('bankname'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bankname') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('bankaddress') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Address : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="bankaddress" type="text" id="bankaddress" class="form-control p-l-10" value="{{$bankdata->bankaddress}}"/>
                                        @if ($errors->has('bankaddress'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bankaddress') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('routingnumber') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Routing Number : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="routingnumber" type="text" id="routingnumber" class="form-control p-l-10" value="{{$bankdata->routingnumber}}"/>
                                        @if ($errors->has('routingnumber'))
                                            <span class="help-block">
											<strong>{{ $errors->first('routingnumber') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group {{ $errors->has('contactnumber') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Bank Tel. Number : <span class="star-required">*</span></label>
                                    <div class="col-md-2">
                                        <input name="contactnumber" type="text" id="contactnumber" class="form-control p-l-10" value="{{$bankdata->contactnumber}}"/>
                                        @if ($errors->has('contactnumber'))
                                            <span class="help-block">
											<strong>{{ $errors->first('contactnumber') }}</strong>
										</span>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                        <select class="form-control" name="bankphonetype">
                                            <option value="">Select</option>
                                            <option value="Main" <?php if ($bankdata->bankphonetype == 'Main') {
                                                echo 'Selected';
                                            }?>>Main
                                            </option>
                                            <option value="Support" <?php if ($bankdata->bankphonetype == 'Support') {
                                                echo 'Selected';
                                            }?>>Support
                                            </option>
                                            <option value="CSR" <?php if ($bankdata->bankphonetype == 'CSR') {
                                                echo 'Selected';
                                            }?>>CSR
                                            </option>


                                        </select>
                                    </div>

                                </div>

                                <div class="form-group {{ $errors->has('bankwebsite') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Website : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="bankwebsite" type="text" id="bankwebsite" class="form-control p-l-10" value="{{$bankdata->bankwebsite}}"/>
                                        @if ($errors->has('bankwebsite'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bankwebsite') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="Branch1">
                                    <h1>Branch Info</h1>
                                </div>

                                <div class="form-group" style="margin-top:20px;">
                                    <label class="control-label col-md-3">Branch Name : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="branchname" value="{{$bankdata->branchname}}" type="text" id="" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Branch Address : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="branchaddress" value="{{$bankdata->branchaddress}}" type="text" id="" class="form-control"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">City / State / Zip : <span class="star-required">*</span></label>
                                    <div class="col-md-2">
                                        <input name="branchcity" type="text" id="" value="{{$bankdata->branchcity}}" class="form-control"/>

                                    </div>
                                    <div class="col-md-1">
                                        <select class="form-control" name="branchstate"/>
                                        <option value="">Select</option>
                                        @foreach($statedata as $state)
                                            <option value="{{$state->code}}" @if($state->code == $bankdata->branchstate) selected @endif>{{$state->code}}</option>\
                                            @endforeach
                                            </select>

                                    </div>
                                    <div class="col-md-1">
                                        <input name="branchzip" value="{{$bankdata->branchzip}}" id="zip" type="text" class="form-control"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Branch Contact Name / Position : <span class="star-required">*</span></label>
                                    <div class="col-md-2">
                                        <input name="branchcontactname" value="{{$bankdata->branchcontactname}}" type="text" id="" class="form-control"/>

                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="position"/>
                                        <option value="">Select</option>

                                        <option value="Manager" @if($bankdata->position == 'Manager') selected @endif>Manager</option>
                                        <option value="Teller" @if($bankdata->position == 'Teller') selected @endif>Teller</option>
                                        <option value="Personal Banker" @if($bankdata->position =='Personal Banker') selected @endif>Personal Banker</option>
                                        <option value="Other" @if($bankdata->position  == 'Other') selected @endif>Other</option>


                                        </select>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Telephone 1 # : <span class="star-required">*</span></label>
                                    <div class="col-md-2">
                                        <input name="telephone1" value="{{$bankdata->telephone1}}" type="text" id="branchtelephone1" class="form-control"/>

                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="telephone1type"/>
                                        <option value="">Select</option>
                                        <option value="Office" @if($bankdata->telephone1type  == 'Office') selected @endif>Office</option>
                                        <option value="Mobile" @if($bankdata->telephone1type == 'Mobile') selected @endif>Mobile</option>

                                        </select>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Telephone 2 # : <span class="star-required">*</span></label>
                                    <div class="col-md-2">
                                        <input name="telephone2" value="{{$bankdata->telephone2}}" type="text" id="branchtelephone2" class="form-control"/>

                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="telephone2type"/>
                                        <option value="">Select</option>

                                        <option value="Office" @if($bankdata->telephone2type ==  'Office') selected @endif>Office</option>
                                        <option value="Mobile" @if($bankdata->telephone2type == 'Mobile') selected @endif>Mobile</option>

                                        </select>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Email : <span class="star-required">*</span></label>
                                    <div class="col-md-4">
                                        <input name="email" value="{{$bankdata->email}}" type="text" id="" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Notes :</label>
                                    <div class="col-md-4">
                                        <textarea name="notes" cols="40" rows="4" class="form-control"/>{{$bankdata->notes}}</textarea>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/bankingsetup')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <style>
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                font-size: 16px !important;
                padding: 0;
                color: #000;
            }

            .p-l-10 {
                padding-left: 10px;
            }

            .select2-container--default .select2-selection--single .select2-selection__arrow b {
                border-color: #000 transparent transparent transparent;
            }

            .select2-container--default .select2-selection--single .select2-selection__arrow {
                top: 6px;
                right: 4px;
            }

            .select2-container {
                box-sizing: border-box;
                display: inline-block;
                margin: 0;
                position: relative;
                vertical-align: middle;
                width: 100% !important;
            }

            .select2-container--default .select2-selection--single {
                background-color: #fff;
                /* border: 1px solid #aaa; */
                border-radius: 4px;
                border: 2px solid #2fa6f2;
                height: 40px;
                padding: 8px;
            }</style>
        <script>
            $('.js-example-tags').select2({
                tags: true,
                tokenSeparators: [",", " "]
            });

        </script>

        <script>
            $("#contactnumber").mask("(999) 999-9999");


        </script>

@endsection()