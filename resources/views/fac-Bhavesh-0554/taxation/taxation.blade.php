@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .table > tbody > tr > td {
            padding: 6px 8px 6px 8px !important;
        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="height:50px;">
            <div class=" ">
                <div class="" style="text-align:center;">
                    <h2>List of License <span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;">Add / View / Edit</span></h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header" style="padding:4px;">
                            <div class="box-tools pull-right" style="margin-bottom:10px;margin-top:7px;position:reltive;z-index:9999;">
                                <div class="table-title">
                                    <a href="{{route('taxation.create')}}">Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Type of License</th>
                                        <th>Type of Business</th>
                                        <th>Renewal</th>
                                        <th>Due Date</th>
                                        <th style="width:145px !important;">Renewal Website</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($position as $pos)
                                        <?php $str1 = $pos->business_catagory_name; $splittedstring1 = explode(",", $str1);?>
                                        <tr>
                                            <td style="text-transform: capitalize; text-align:center;">{{$loop->index + 1}}</td>
                                            <td><?php if ($pos->question == '6') {
                                                    echo 'Annual Registration';
                                                } else {
                                                    echo $pos->question;
                                                }?></td>
                                            <td style="width:400px;"> <?php $arrs = print_r($splittedstring1[0]); if ($arrs == ' ') {
                                                    echo 'All';
                                                }?>    @foreach($category as $cate)
                                                    @foreach($splittedstring1 as $key => $value)  @if($value ==$cate->id) {{$cate->business_cat_name}}  @endif @endforeach
                                                @endforeach</td>
                                            <td>{{$pos->renewal}}</td>

                                            <td style="text-align:center;"> @if($pos->renewal=='Universal') {{$pos->expiredate.'-'.$pos->expiredate2.','.date('Y')}} @else {{$pos->expiredate.'-'.$pos->expiredate2}} @endif</td>
                                            <td style="text-align:center;">@if(!empty($pos->renewalwebsite))<a href="{{$pos->renewalwebsite}}" style="color:#222;width:140px;" target="_black" class="btn_new btn-renew col-md-2">Renew Now</a>@endif</td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('taxation.edit', $pos->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('taxation.destroy',$pos->id) }}" method="post" style="display:none" id="delete-id-{{$pos->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$pos->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();
                table.columns(6)
                    .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'Inactive') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'New') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'Active') {  //alert();
                        table.columns(6).search(_val).draw();
                        table.columns(6)
                            .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                            .draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>
@endsection()