@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        label {
            float: left;
        }

        .page-title {
            padding: 8px 19px !important;
        }
    </style>
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }

        #example_filter {
            display: none;
        }

        .search-btn {
            position: absolute;
            top: 10px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        .fa {
            font-size: 16px !important;
        }

        .dt-buttons {
            margin-bottom: 10px;
        }

        .imgicon {
            background: #fff;
            display: block;
            margin-top: -6px;
            width: 35px;
            float: left;
            height: 35px;
            margin-right: 10px;
            float: left;
            margin-right: 10px;
            border-radius: 2px;
            padding: 3px;
            border: 1px solid #12186b;
        }

        .imgicon img {
            max-width: 100%;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header page-title" style="height:40px;">
            <div class="" style="padding-right:0px;">
                <div style="text-align:center;">
                    <span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/telephone.png" alt="img"></span>
                    <h2>List of Contact Us Inquiry <span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;"> View / Edit</span></h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header" style="padding:0px;">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-9" style="margin-bottom: -36px;margin-top: 10px;">
                            <div class="">

                                <div class="col-md-3" style="padding:0px !important;">
                                    <table style="width: 100%;" cellspacing="0" cellpadding="3" border="0">
                                        <tbody>
                                        <tr id="filter_global">
                                            <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col2" data-column="1" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col3" data-column="2" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col4" data-column="3" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col5" data-column="4" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col6" data-column="5" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        <tr id="filter_col7" data-column="6" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone">
                                                <button class="search-btn"><i class="fa fa-search"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">


                            <div class="table-responsive">
                                @if ( session()->has('success') )
                                    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                                @endif
                                @if ( session()->has('error') )
                                    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                                @endif
                                <table class="table table-hover table-bordered" id="example">
                                    <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th style="width:15%;border:1px solid; min-width:155px">Date / Time</th>
                                        <th style="width:30%;border:1px solid;">Name</th>
                                        <th style="width:30%;border:1px solid;">Email</th>

                                        <th style="border:1px solid;" width="10%">Telephone</th>

                                        <th style="border:1px solid;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $cnt = 0;?>
                                    @foreach($contact as $employ)

                                        <tr>
                                            <th><?php echo $cnt++;?></th>
                                            <td>{{date('M-d-Y', strtotime($employ->created_at))}} &nbsp;&nbsp;&nbsp; {{date('g:i a', strtotime($employ->created_at))}}</td>
                                            <td>{{$employ->name}}</td>
                                            <td>{{$employ->email}}</td>
                                            <td>{{$employ->telephone}}</td>
                                            <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('contactinquery.edit',$employ->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('contactinquery.destroy',$employ->id) }}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": false,
                        "orderable": true,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            titleAttr: 'Copy',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            titleAttr: 'Excel',
                            title: $('h3').text(),
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                $('row c', sheet).attr('s', '51');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            titleAttr: 'CSV',
                            title: $('h3').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',

                            customize: function (doc) {
                                //Remove the title created by datatTables
                                doc.content.splice(0, 1);
                                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                doc.pageMargins = [20, 60, 20, 20];
                                doc.defaultStyle.fontSize = 10;
                                doc.styles.tableHeader.fontSize = 10;
                                doc['header'] = (function () {
                                    return {
                                        columns: [{
                                            alignment: 'left',
                                            image: logo,
                                            width: 50, margin: [200, 5]
                                        }, {
                                            alignment: 'CENTER',
                                            text: 'List of Client',
                                            fontSize: 20,
                                            margin: [10, 35],
                                        },],
                                        margin: [20, 0, 0, 12], alignment: 'center',
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return 2;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#ccc';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 14;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 14;
                                };
                                doc.content[0].layout = objLayout;
                            },
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            titleAttr: 'Print',
                            customize: function (win) {
                                $(win.document.body)
                                    .css('font-size', '10pt')
                                    .prepend(
                                        '<center><img src=""/></center>'
                                    );
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            },
                            footer: true,
                            autoPrint: true
                        },],
                });
                $('input.global_filter').on('keyup click', function () {
                    filterGlobal();
                });

                $('input.column_filter').on('keyup click', function () {
                    filterColumn($(this).parents('tr').attr('data-column'));
                });
                table.on('order.dt search.dt', function () {
                    table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                        table.cell(cell).invalidate('dom');
                    });
                }).draw();
                table.columns(6)
                    .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                    .draw();
                $("#choice").on("change", function () {
                    var _val = $(this).val();//alert(_val);

                    if (_val == 'Inactive') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'New') {
                        table.columns(6).search(_val).draw();
                    } else if (_val == 'Active') {  //alert();
                        table.columns(6).search(_val).draw();
                        table.columns(6)
                            .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
                            .draw();
                    } else {
                        table
                            .columns()
                            .search('')
                            .draw();
                    }
                })
            });
        </script>
@endsection()