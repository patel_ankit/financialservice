<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
	!-->

    <title>FSC - Admin</title>
    <style>
        .help-block {
            color: red
        }

        .semibold-text a {
            color: red;
        }
    </style>
    <style>

        body {
            margin: 0px;
            padding: 0px;
            font-family: 'Muli', sans-serif;
            background: url(../../../public/images/fscloginback.jpg) no-repeat center center fixed;
            background-size: cover;
        }

        h1, h2, input {
            font-family: 'Muli', sans-serif;
            text-align: center;
            color: #fff;
        }

        .mainwrapper {
            display: flex;
            flex-direction: column;
            width: 100vw;
            height: 100vh;
            justify-content: center;
            vertical-align: middle;
            align-items: center;
        }

        .loginwrapper {
            width: 60vw;
            overflow: hidden;
            height: 80vh;
            padding: 0px 0px;
            background: #ffffff;
            border-radius: 15px;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }

        .logobox {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 50vw;
            height: 100%;
            background: url(../../../public/images/logo_backfsc.jpg) no-repeat center center;
            background-size: contain;
        }

        .loginbox {
            display: flex;
            height: 100%;
            background: #103b68;
            justify-content: center;
            width: 50vw;
            flex-direction: column;
            border: 0px solid #000;
        }

        .loginbox img {
            max-width: 100%;
        }

        .loginform ul {
            margin: 0px 50px;
            padding: 0px;
        }

        .loginform ul li {
            list-style-type: none;
            margin-bottom: 10px;
            position: relative;
            text-align: center;
        }

        .loginform ul li a {
            color: #fff;
            margin: 0px;
            text-decoration: none;
            float: right;
            font-size: 13px;
        }

        .loginform ul li a:hover {
            color: #a2cbf5;
        }

        .adminlogo {
            margin-bottom: 0px;
            display: block;
        }

        h1 {
            margin: 0px 0px 20px;
            padding: 0px;
            font-size: 50px;
            font-weight: 700;
            color: #fff;
        }

        .formcontrol {
            background: #f8f8f8;
            text-align: left;
            font-size: 16px;
            outline: none;
            color: #000000;
            height: 50px;
            border-radius: 5px;
            border: 0px;
            width: 85%;
            padding: 0px 0px 0px 15%;
        }

        input[type="text"], input:-internal-autofill-selected {
            background: #f8f8f8 url(../../../public/images/icon_email.jpg) no-repeat 10px center !important;
            position: relative;
        }

        input[type="password"] {
            background: #f8f8f8 url(../../../public/images/icon_password.jpg) no-repeat 10px center;
        }

        input:-webkit-autofill {
            background-color: inherit !important;
        }

        input[type="text"]:before {
            content: '';
            position: absolute;
            top: 0px;
            left: 0px;
            background: url(../../../public/images/icon_email.jpg) no-repeat 10px center !important;
        }

        .alignleft {
            text-align: left;
        }

        .alignright {
            text-align: right;
        }

        ::-webkit-input-placeholder { /* Edge */
            color: #8b8a8a;
            font-size: 15px;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: #8b8a8a;
            font-size: 15px;
        }

        ::placeholder {
            color: #8b8a8a;
            font-size: 15px;
        }

        .btnlogin {
            background: #00264e;
            margin-top: 5px;
            color: #fff;
            transition: all ease 0.5s;
            height: 54px;
            border-radius: 5px;
            cursor: pointer;
            outline: none;
            text-align: center;
            font-size: 20px;
            width: 100%;
            border: 0px;
            box-shadow: none;
            transform: all ease 0.5s;
        }

        .btnlogin:hover {
            background: #1f4c7b;
            transform: all ease 0.5s;
        }

        .toggle-password {
            font-size: 25px;
            cursor: pointer;
            margin-left: 0;
            position: absolute;
            right: 15px;
            top: 12px;
        }

        .forgotlinks {
            flex-direction: row;
            justify-content: space-between;
        }

        @media screen and (max-width: 1100px) {
            .loginwrapper {
                width: 90vw;
            }
        }

        @media screen and (max-width: 825px) {
            .loginwrapper {
                width: 60vw;
                flex-direction: column;
            }

            .logobox, .loginbox {
                width: 100%;
                height: 50%;
            }
        }

        @media screen and (max-width: 767px) {
            .loginwrapper {
                width: 60vw;
                justify-content: space-between;
            }

            .logobox {
                width: 100%;
                display: flex;
            }

            .logobox img {
                height: 80px;
            }

            .loginbox {
                width: 100%;
                height: auto;
                padding: 30px 0px;
            }

            .loginform ul {
                margin: 0px 30px;
                padding: 0px;
            }

        }

        @media screen and (max-width: 640px) {
            .loginwrapper {
                width: 90vw;
            }

            .loginbox {
                width: 95%;
            }

            .logobox {
                width: 100%;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }

            .logobox img {
                height: 100px;
            }

            .loginbox {
                width: 100%;
                height: auto;
            }
        }

        @media screen and (max-width: 1100px) and (orientation: landscape) {
            .loginwrapper {
                height: 600px;
                margin: 40px 0px;
            }

            .mainwrapper {
                height: 700px;
            }
        }


        .alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-success {
            color: #fff;
            background-color: green;
            border-color: green;
        }

        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        .alert-warning {
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
        }
    </style>
</head>

<body>
<div class="mainwrapper">
    <div class="loginwrapper">
        <div class="logobox">
            <a href="#" class="adminlogo"><img src="{{asset('public/dashboard/images/logo_fsc.png')}}" alt="Financial Service Center"/></a>
        </div>
        <div class="loginbox">
            <form class="login-form" method="post" action="" id="admin-login">
                {{ csrf_field() }}

                <h1>Login</h1>
                <div class="loginform">


                    <ul>
                        <li class="form-group"><input class="formcontrol" type="text" name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </li>
                        <li class="form-group"><input class="formcontrol" type="password" name="password" id="newpassword" placeholder="Password">
                            <span toggle="#newpassword" style="font-size: 25px;margin-left:-35px;margin-top: -30px;" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </li>
                        <li>
                            <button type="submit" name="submit" class="btnlogin">SIGN IN<i class="fa fa-sign-in fa-lg"></i> <img class="dvloader" src="https://www.drupal.org/files/issues/throbber_12.gif" style="width: 21px;display:none"></button>
                        </li>
                        <li>
                            <div class="forgotlinks"><a class="alignright" href="adminpassword">Forgot Password?</a></div>
                        </li>

                    </ul>
                    <center>
                        @if ( session()->has('success') )<br><br>
                        <div class="alert alert-success alert-dismissable" style="display: inline-block;">{{ session()->get('success') }}</div>
                        @endif
                        @if ( session()->has('error') )<br><br>
                        <div class="alert alert-danger alert-dismissable" style="display: inline-block;">{{ session()->get('error') }}</div>
                        @endif
                        <div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div>
                    </center>


                </div>
            </form>
        </div>
    </div>
</div>
<!--	<section class="material-half-bg">
		<div class="cover"></div>
	</section>
	<section class="login-content">
		<div class="logo">
			<img src="{{asset('public/dashboard/images/fsc_logo.png')}}" alt="" />
		</div>
		<div class="login-box">
			<form class="login-form" method="post" action="" id="admin-login">
			{{ csrf_field() }}
        <h3 class="login-head"><i class="fa fa-logo-lg fa-fw fa-user"></i>SIGN IN </h3>

        <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input class="form-control" type="text"  name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
                    @if ($errors->has('email'))
    <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
        </div>

        <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" id="newpassword" placeholder="Password">
             <span toggle="#newpassword" style="font-size: 25px;margin-left:-35px;margin-top: -30px;" class="fa fa-fw fa-eye field-icon toggle-password"></span>
@if ($errors->has('password'))
    <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
        </div>

        <div class="form-group">
            <div class="utility">
                <p class="semibold-text mb-0"><a href="adminpassword">Forgot Password ?</a></p>
<p class="semibold-text mb-0"><a href="{{ route('forgotusername.index') }}">Forgot Username ?</a></p>
					</div>
				</div>
				
				<div class="form-group btn-container">
					<button  type="submit" name="submit" class="btn btn-primary btn-block">SIGN IN<i class="fa fa-sign-in fa-lg"></i> <img class="dvloader" src="https://www.drupal.org/files/issues/throbber_12.gif" style="width: 21px;display:none"></button>
				</div>
				<center>
			@if ( session()->has('success') )<br><br>
            <div class="alert alert-success alert-dismissable" style="display: inline-block;">{{ session()->get('success') }}</div>
            @endif
@if ( session()->has('error') )<br><br>
            <div class="alert alert-danger alert-dismissable" style="display: inline-block;">{{ session()->get('error') }}</div>
            @endif
        <div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div></center>
        </form>
        <form class="forget-form"  method="POST" action="{{ route('fac-Bhavesh-0554.password.email') }}" id="forgotuser" >
			 {{ csrf_field() }}
        <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email" class="control-label">EMAIL</label>
					<input class="form-control" id="email" type="email" name="email" value="{{ old('email') }}" required placeholder="Email">
                    @if ($errors->has('email'))
    <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
        </div>
        <div id="emailmsg"></div>
        <div class="form-group btn-container">
            <button  type="submit" name="submit" class="btn btn-primary btn-block">RESET<i class="fa fa-sign-in fa-lg"></i></button>
        </div>
        <div class="form-group mt-20">
            <p class="semibold-text mb-0"><a data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
        </div>
        <center><div id="Register1" style="margin-top:20px;width: 100%;display: inline-block;"></div></center>
    </form>
@if (session('status'))
    <div class="alert alert-success">
{{ session('status') }}
            </div>
@endif
        </div>
    </section>
  !-->

<script src="{{asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/plugins/pace.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/main.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

<script>
    $(document).ready(function () {
        $("#loginuser").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: "required"
            },

            // Specify the validation error messages
            messages: {

                email: {
                    required: "Please Enter Email Address",
                    email: "Please Enter Proper Email"
                },
                password: "Please enter password"

            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#forgotuser").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            // Specify the validation error messages
            messages: {

                email: {
                    required: "Please Enter Email Address",
                    email: "Please Enter Proper Email"
                }

            },
            submitHandler: function (form) {

                // form.submit();
            }
        });

        function validationError(errorMap, errorList) {
            if (errorList.length == 0)
                return;
            var msgs = [];
            for (var err = 0; err < errorList.length; err++) {
                msgs.push({message: errorList[err].message});
            }
            $("#errorlist").notification({caption: "One or more invalid inputs found:", messages: msgs, sticky: true});
        }

        function validationSuccess(form) {
            $("#loginuser").ajaxSubmit({
                beforeSubmit: formRequest,
                success: formResponse,
                dataType: 'json'
            });
        }

        function formRequest(formData, jqForm, options) {
        }

        function formResponse(responseText, statusText) {
            if (statusText == "success") {
                if (responseText.type == "success") {
                    $("#errorlist").notification({
                        caption: "Login Successfully", type: "information", sticky: false, onhide: function () {
                            window.location = "index.php";
                        }
                    });
                } else if (responseText.type == "msg") {
                    $("#errorlist").notification({
                        caption: "Invalid Username or Password", type: "warning", sticky: true, onhide: function () {

                        }
                    });
                }
            } else {
                $("#errorlist").notification({caption: "Unable to communicate with server.", type: "warning", sticky: true});
            }
        }
    });
</script>

<script>
    $.ajaxSetup({headers: {'X-CSRF-Token': $('input[name="_token"]').val()}});

    $('.login-form').bootstrapValidator({

        message: 'This value is not valid',
        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function (validator, form, submitButton) {
            $(".dvloader").show();
            $.ajax({
                type: "post",
                url: "{{ route('fac-Bhavesh-0554.login')}}",
                data: $('.login-form').serialize(),
                success: function (msg) {
                    $(".dvloader").hide();
                    if (msg == 'save1') {
                        $('#Register').html('<span class="alert alert-danger">Your Username And Password Invalid ...</span>');

                    } else {
                        $('#Register').html('<span class="alert alert-success">You are login successfully...</span>');
                        setTimeout(function () {
                            location.href = "{{route('fac-Bhavesh-0554.dashboard')}}";
                        }, 0);
                    }
                },
                error: function () {

                }
            });//close ajax
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email !!!'
                    },
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
                    }

                }
            },
            password: {

                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Password !!!'
                    }
                }
            },
        } // end field
    });// bootstrapValidator


</script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
</body>
</html>