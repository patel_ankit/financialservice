@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .buttons-pdf {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #fff !important;
            border-color: #c6c6c6 !important;
            color: red !important;
        }

        .buttons-print {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;
            background: #3c8dbc !important;
            border-color: #367fa9 !important;
        }

        .buttons-excel {
            font-size: 0px !important;
            border-radius: 3px;
            padding: 8px 10px !important;

            background: #00a65a !important;
            border-color: #008d4c !important;


        }

        .buttons-excel:hover {
            background: #008d4c !important;

        }

        .buttons-pdf:hover {
            background: #f6f6f6 !important;
        }

        .buttons-print:hover {
            background: #367fa9 !important;
        }


        .fa {
            font-size: 16px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Branch</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">
                                <div class="table-title">

                                    <a href="{{route('branch.create')}}">Add New Branch</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            @if ( session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable3">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Branch Name</th>
                                        <th>country</th>
                                        <th>City</th>
                                        <th>Branch Type</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($branch as $bra)

                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$bra->branchname}}</td>
                                            <td>{{$bra->country}}</td>
                                            <td>{{$bra->city}}</td>
                                            <td>{{$bra->positionid}}</td>
                                            <td>
                                                <a class="btn-action btn-view-edit" href="{{route('branch.edit', $bra->id)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('branch.destroy',$bra->id) }}" method="post" style="display:none" id="delete-id-{{$bra->id}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>

                                                <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$bra->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

@endsection()