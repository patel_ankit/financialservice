@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Logo</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('logo.update',$business->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('logoname') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Logo Name :</label>
                                    <div class="col-md-4">
                                        <input name="logoname" type="text" value="{{$business->logoname}}" id="logoname" class="form-control" value=""/>@if ($errors->has('logoname'))
                                            <span class="help-block">
											<strong>{{ $errors->first('logoname') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('logourl') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Logo Url :</label>
                                    <div class="col-md-4">
                                        <input name="logourl" type="text" value="{{$business->logourl}}" id="logourl" class="form-control" value=""/>@if ($errors->has('logourl'))
                                            <span class="help-block">
											<strong>{{ $errors->first('logourl') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Logo Image :</label>
                                    <div class="col-md-4">
                                        <input type="file" name="logo" id="logo" class="form-control" value="{{$business->logo}}"/>
                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" name="bussiness_image_name" id="logo" type="file" value="{{$business->logo}}"/>
                                        </label><img src="https://financialservicecenter.net/public/business/{{$business->logo}}" title="{{$business->logoname}}" alt="{{$business->logoname}}" width="100px">
                                    </div>
                                    <input type="hidden" name="logo1" id="logo1" value="{{$business->logo}}">
                                </div>

                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/logo')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()