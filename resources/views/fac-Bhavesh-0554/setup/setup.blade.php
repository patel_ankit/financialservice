@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .btn-box-tool.btn:hover {
            color: #333;
        }

        .btn-box-tool.btn:active {
            color: #333;
        }

        .box {
            background: #fff;
        }

        .btn-box-tool {
            border: transparent
        }

        .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
            color: #333;
        }

        .box-body {
            padding: 10px
        }

        .modal-body .form-control {
            height: 30px;
            margin-bottom: 5px;
        }

        .modal-body table tr th {
            padding-bottom: 7px !important;
        }

        .modal-title {
            width: 95%;
            float: left;
            font-size: 18px;
            color: #0281cc !important;
            text-align: center;
        }

        #table8 tr td, #table8 tr th {
            text-align: center !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h2>Price Setup</h2>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- /Section -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- /Position -->
                            <div class="col-md-6 col-xs-12">
                                <div class="box" style="background-color:#ffff99;border:1px solid;">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Online Access Fee Set up</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                        <ul class="todo-list ui-sortable">
                                            <li><a href="{{url('/fac-Bhavesh-0554/price/create')}}?online=1">Add Online Access Fee</a></li>
                                            <li><a href="{{url('/fac-Bhavesh-0554/price')}}?online=1">View / Edit Online Access Fee</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /End Position -->

                            <!-- /Branch -->
                            <div class="col-md-6 col-xs-12">
                                <div class="box" style="background-color:#ffff99;border:1px solid;">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Service Fee Set Up</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                        <ul class="todo-list ui-sortable">
                                            <li><a href="{{url('/fac-Bhavesh-0554/price/create')}}?online=0">Add Service Fee</a></li>
                                            <li><a href="{{url('/fac-Bhavesh-0554/price')}}?online=0">View / Edit Service Fee</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /End Branch -->
                            <div class="col-md-6 col-xs-12">
                                <div class="box" style="background-color:#ffff99;border:1px solid;">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Online Price List</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                        <ul class="todo-list ui-sortable">
                                            <li><a href="#exampleModalone" data-toggle="modal" data-target="#exampleModalone">Accounting Service Price List</a></li>
                                            <li><a href="#exampleModaltwo" data-toggle="modal" data-target="#exampleModaltwo">Taxation Service Price List</a></li>
                                            <li><a href="#exampleModalthree" data-toggle="modal" data-target="#exampleModalthree">Bookkeeping Service</a></li>
                                            <li><a href="#exampleModalfour" data-toggle="modal" data-target="#exampleModalfour">Payroll Service</a></li>
                                            <li><a href="#exampleModalfive" data-toggle="modal" data-target="#exampleModalfive">Residential Mortgage Service</a></li>
                                            <li><a href="#exampleModalsix" data-toggle="modal" data-target="#exampleModalsix">Commercial Mortgage Service</a></li>
                                            <li><a href="#exampleModalseven" data-toggle="modal" data-target="#exampleModalseven">Financial Service</a></li>
                                            <li><a href="#exampleModaleight" data-toggle="modal" data-target="#exampleModaleight">License Service</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="box" style="background-color:#ffff99;border:1px solid;">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Office Price List</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                        <ul class="todo-list ui-sortable">
                                            <li><a href="#exampleModalone" data-toggle="modal" data-target="#exampleModalone">Accounting Service Price List</a></li>
                                            <li><a href="#exampleModaltwo" data-toggle="modal" data-target="#exampleModaltwo">Taxation Service Price List</a></li>
                                            <li><a href="#exampleModalthree" data-toggle="modal" data-target="#exampleModalthree">Bookkeeping Service</a></li>
                                            <li><a href="#exampleModalfour" data-toggle="modal" data-target="#exampleModalfour">Payroll Service</a></li>
                                            <li><a href="#exampleModalfive" data-toggle="modal" data-target="#exampleModalfive">Residential Mortgage Service</a></li>
                                            <li><a href="#exampleModalsix" data-toggle="modal" data-target="#exampleModalsix">Commercial Mortgage Service</a></li>
                                            <li><a href="#exampleModalseven" data-toggle="modal" data-target="#exampleModalseven">Financial Service</a></li>
                                            <li><a href="#exampleModaleight" data-toggle="modal" data-target="#exampleModaleight">License Service</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./endsection -->
        </section>
        <!--</div>-->


        <div class="modal fade" id="exampleModalone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Accounting Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table1">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="CPA"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Book keeper/Tax Preparer"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Office Assistant"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowone();'>Add</a>
                            <a class="btn btn-danger " onClick='removeone();'>Remove</a>


                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModaltwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Taxation Service Price List</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table2">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price/hr</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="Business Personal Return Tax Preparation-S Corp/Corp/LLC"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Personal Tax Return Preparation-1040 & one state "/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowtwo();'>Add</a>
                            <a class="btn btn-danger " onClick='removetwo();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalthree" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bookkeeping Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table7">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td><input type="text" class="form-control" value="Monthly Bookkeeping Service"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Quarterly Bookkeeping Service"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnadd" onClick='addrowseven();'>Add</a>
                            <a class="btn btn-danger " onClick='removeseven();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalfour" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Payroll Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="height:500px; overflow:auto;">
                        <table class="table table-bordered table-striped" id='table8'>
                            <thead>
                            <tr>
                                <th># of Employees</th>
                                <th style="width:100px">Weekly</th>
                                <th style="width:100px">Bi-Weekly</th>
                                <th style="width:100px">Semi-Weekly</th>
                                <th style="width:100px">Monthly</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>1</td>
                                <td style="width:100px">10.50</td>
                                <td style="width:100px">16.50</td>
                                <td style="width:100px">16.50</td>
                                <td style="width:100px">22.50</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>12.00</td>
                                <td>18.00</td>
                                <td>18.00</td>
                                <td style="width:100px">24.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>13.50</td>
                                <td>19.50</td>
                                <td>19.50</td>
                                <td>25.50</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>14.95</td>
                                <td>20.95</td>
                                <td>20.95</td>
                                <td>26.95</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>16.40</td>
                                <td>22.40</td>
                                <td>22.40</td>
                                <td>28.40</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>17.85</td>
                                <td>23.85</td>
                                <td>23.85</td>
                                <td>29.85</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>19.30</td>
                                <td>25.30</td>
                                <td>25.30</td>
                                <td>31.30</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>20.75</td>
                                <td>26.75</td>
                                <td>26.75</td>
                                <td>32.75</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>22.20</td>
                                <td>28.20</td>
                                <td>28.20</td>
                                <td>34.20</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>23.65</td>
                                <td>29.65</td>
                                <td>29.65</td>
                                <td>35.65</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>30.40</td>
                                <td>36.40</td>
                                <td>36.40</td>
                                <td>42.40</td>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>36.65</td>
                                <td>42.65</td>
                                <td>42.65</td>
                                <td>48.65</td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>46.15</td>
                                <td>52.15</td>
                                <td>52.15</td>
                                <td>58.15</td>
                            </tr>
                            <tr>
                                <td>35</td>
                                <td>50.40</td>
                                <td>56.40</td>
                                <td>56.40</td>
                                <td>62.40</td>
                            </tr>
                            <tr>
                                <td>40</td>
                                <td>54.65</td>
                                <td>60.65</td>
                                <td>60.65</td>
                                <td>66.65</td>
                            </tr>
                            <tr>
                                <td>45</td>
                                <td>58.40</td>
                                <td>64.40</td>
                                <td>64.40</td>
                                <td>70.40</td>
                            </tr>
                            <tr>
                                <td>50</td>
                                <td>62.15</td>
                                <td>68.15</td>
                                <td>68.15</td>
                                <td>74.15</td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnadd" onClick='addroweight();'>Add</a>
                            <a class="btn btn-danger " onClick='removeeight();'>Remove</a>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped" id="table9">
                            <thead>
                            <tr>
                                <th style="width:50px; text-align:Center">No.</th>
                                <th>Additional Payroll Reporting Fees</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>$5.00 Per Payroll period covers ALL tax deposits</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>W-3 at 10.00 base charge and 2.85 per W-2</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Multiple states cost extra</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnadd" onClick='addrownine();'>Add</a>
                            <a class="btn btn-danger " onClick='removenine();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalfive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Residential Mortgage Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table3">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="CPA"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Book keeper/Tax Preparer"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Office Assistant"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowthree();'>Add</a>
                            <a class="btn btn-danger " onClick='removethree();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalsix" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Commercial Mortgage Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table4">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="CPA"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Book keeper/Tax Preparer"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Office Assistant"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowfour();'>Add</a>
                            <a class="btn btn-danger " onClick='removefour();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalseven" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Financial Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table5">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="CPA"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Book keeper/Tax Preparer"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Office Assistant"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowfive();'>Add</a>
                            <a class="btn btn-danger " onClick='removefive();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModaleight" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">License Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped" id="table6">
                            <thead>
                            <tr>
                                <th style="text-align:left!important;">Description</th>
                                <th style="width:150px">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" class="form-control" value="CPA"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Book keeper/Tax Preparer"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" value="Office Assistant"/></td>
                                <td style="width:150px;"><input type="text" class="form-control text-right num_only"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a class="btn btn-success btnaddone" onClick='addrowsix();'>Add</a>
                            <a class="btn btn-danger " onClick='removesix();'>Remove</a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <script>

            function addrowone() {
                // alert('');

                $('#table1').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removeone() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table1 tbody tr:last').remove();
            }


            function addrowtwo() {
                // alert('');

                $('#table2').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removetwo() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table2 tbody tr:last').remove();
            }


            function addrowthree() {
                // alert('');

                $('#table3').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removethree() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table3 tbody tr:last').remove();
            }

            function addrowthree() {
                // alert('');

                $('#table3').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removethree() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table3 tbody tr:last').remove();
            }


            function addrowfour() {
                // alert('');

                $('#table4').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removefour() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table4 tbody tr:last').remove();
            }


            function addrowfive() {
                // alert('');

                $('#table5').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removefive() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table5 tbody tr:last').remove();
            }


            function addrowsix() {
                // alert('');

                $('#table6').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removesix() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table6 tbody tr:last').remove();
            }

            function addrowseven() {
                // alert('');

                $('#table7').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removeseven() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table7 tbody tr:last').remove();
            }

            function addroweight() {
                // alert('');

                $('#table8').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removeeight() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table8 tbody tr:last').remove();
            }

            function addrownine() {
                // alert('');

                $('#table9').append('<tr><td><input type="text" class="form-control"/></td><td><input type="text" class="form-control"/></td></tr>');
            }

            function removenine() {
                // alert('');
                //$("table tbody").find('tr').last().remove();
                $('#table9 tbody tr:last').remove();
            }

            $('.num_only').keyup(function () {
                this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');
            });

        </script>



@endsection()