@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Form Category</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('formcategory.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            {{csrf_field()}}


                            <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Category :</label>
                                <div class="col-md-4">
                                    <input name="category" type="text" id="category" class="form-control" value=""/> @if ($errors->has('category'))
                                        <span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Type :</label>
                                <div class="col-md-4">
                                    <select name="type" type="text" id="type" class="form-control" value="">
                                        <option value="">Select</option>
                                        <option value="Federal">Federal</option>
                                        <option value="State">State</option>
                                        <option value="County">County</option>
                                        <option value="Local City">Local City</option>
                                        <option value="Community Link">FSC Community Link</option>
                                    </select> @if ($errors->has('type'))
                                        <span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
                                    @endif
                                </div>
                            </div>


                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-3">
                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        $(function () {
            $("#category").change(function () {
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                if (selectedValue == 'Business For Sale') {
                    document.getElementById('hidden_div').style.display = "block";
                    document.getElementById('hidden_div1').style.display = "none";
                }
                if (selectedValue == 'Looking For Business') {
                    document.getElementById('hidden_div1').style.display = "block";
                    document.getElementById('hidden_div').style.display = "none";
                }
            });
        });
    </script>
@endsection()