@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service Type</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('servicetype.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('servicetype') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Service Type :</label>
                                    <div class="col-md-4">
                                        <select name="servicetype" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($service as $ser)
                                                <option value="{{$ser->id}}">{{$ser->service_name}}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('servicetype'))
                                            <span class="help-block">
											<strong>{{ $errors->first('servicetype') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group {{ $errors->has('servicename') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Service Name :</label>
                                    <div class="col-md-4">
                                        <input name="servicename" type="text" id="servicename" class="form-control" value=""/>
                                        @if ($errors->has('servicename'))
                                            <span class="help-block">
											<strong>{{ $errors->first('servicename') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/servicetype')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()