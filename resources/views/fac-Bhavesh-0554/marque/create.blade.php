@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Marque</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('marque.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group {{ $errors->has('marque') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Name :</label>
                                    <div class="col-md-4">
                                        <input name="marque" type="text" id="marque" class="form-control" value=""/> @if ($errors->has('marque'))
                                            <span class="help-block">
										<strong>{{ $errors->first('marque') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1 primary1" type="button" id="primary1" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/marque')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()