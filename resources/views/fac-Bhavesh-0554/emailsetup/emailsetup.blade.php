@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .table-title a {
            padding: 6px 15px 6px !important;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header page-title">
            <div class="" style="margin-top:5px;padding-right:0px;">
                <div style="text-align:center;">
                    <h2>Email Message Setup</h2>
                </div>

            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="col-md-12">

                            <div class="table-title">
                                <h3></h3>
                                <a style="margin:10px 0px;" href="{{route('emailsetup.create')}}">Add New Email Message Setup</a>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @if ( session()->has('success') )
                                        <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                                    @endif
                                    @if ( session()->has('error') )
                                        <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>To Whom</th>
                                                <th>Name</th>
                                                <th>Subject</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($email as $a)
                                                <tr>
                                                    <td>
                                                        <center>{{$loop->index+1}}</center>
                                                    </td>
                                                    <td>
                                                        <center>{{ucwords($a->type)}}</center>
                                                    </td>
                                                    <td>
                                                        @foreach($emp as $e)
                                                            @if($e->id == $a->userid)
                                                                {{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} @endif
                                                        @endforeach
                                                    </td>
                                                    <td>{{$a->subject}}</td>
                                                    <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('emailsetup.edit', $a->id)}}"><i class="fa fa-edit"></i></a>
                                                        <form action="{{ route('emailsetup.destroy',$a->id) }}" method="post" style="display:none" id="delete-id-{{$a->id}}">
                                                            {{csrf_field()}} {{method_field('DELETE')}}
                                                        </form>
                                                        <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                {event.preventDefault();document.getElementById('delete-id-{{$a->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()