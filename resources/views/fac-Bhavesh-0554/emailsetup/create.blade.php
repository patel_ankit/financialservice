@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1>Add New Email Message Setup</h1>
        </div>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" id="" action="{{route('emailsetup.store')}}" class="form-horizontal" enctype="" novalidate="">
                                {{csrf_field()}}
                                <div class="form-group ">
                                    <label class="control-label col-md-3">To Whom : <span class="star-required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="type" id="type" class="form-control fsc-input">
                                                    <option value="">---Select---</option>
                                                    <option value="All">All</option>
                                                    <option value="Approval">Client</option>
                                                    <option value="All">FSC Client Employee</option>
                                                    <option value="employee">FSC Employee</option>
                                                    <option value="user">FSC User</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-3 text-right">To : <span class="star-required">&nbsp;</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="employee" id="employee" class="form-control fsc-input">
                                                    <option value="">---Select---</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-3 text-right">Subject : <span class="star-required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control fsc-input" placeholder="" name="subject" id="subject" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-3 text-right">Message : <span class="star-required">&nbsp;</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3" style="padding-left: 7px;">
                                        <input class="btn_new_save" type="submit" value="Add">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/emailsetup')}}">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--</div>-->
        </section>
        <!--</div>-->
        <script>
            function myFunction() {
                var y = '@financialservicecenter.net';
                var x = document.getElementById("email");
                t = x.value;
                z = t + y;
                $('#email').val(z);
            }

            $(document).ready(function () {
                $(document).on('change', '#type', function () {
                    var id = $(this).val();
                    $('#employee').empty();
                    $.get('{!!URL::to('/clientid')!!}?id=' + id, function (data) {
                        $('#employee').empty();
                        $('#employee').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            if (id == 'employee') {
                                $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');
                            } else if (id == 'user') {
                                $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');
                            }
                            if (id == 'Active') {
                                $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.last_name + '</option>');
                            }
                        })
                    });
                });
            });
        </script>
@endsection()