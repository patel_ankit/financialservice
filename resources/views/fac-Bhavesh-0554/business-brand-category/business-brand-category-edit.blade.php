@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Business Brand Category</h1>
        </section>
        <!-- Main content -->
        <section class="content">


            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" enctype="multipart/form-data" class="form-horizontal" action="{{ route('business-brand-category.update', $categorybusiness->cid)}}" id="">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('business_id') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control category" id="" name="business_id">
                                            @foreach($business as $bus)
                                                @if($categorybusiness->business_id == $bus->id)
                                                    <option value='{{$categorybusiness->business_id}}' selected>{{$categorybusiness->bussiness_name}}</option>
                                                @else
                                                    <option value='{{$bus->id}}'>{{$bus->bussiness_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('business_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_id') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('business_cat_id') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Category Name :</label>
                                    <div class="col-md-4">
                                        <select class="form-control category1 category2" id="business_cat_id" name="business_cat_id">
                                            <option value='{{$categorybusiness->business_cat_id}}'>{{$categorybusiness->business_cat_name}}</option>
                                        </select>
                                        @if ($errors->has('business_cat_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_cat_id') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('business_brand_id') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Brand :</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="business_brand_id" name="business_brand_id">
                                            @foreach($businessbrand as $bus1)
                                                @if($categorybusiness->business_brand_id == $bus1->id)
                                                    <option value='{{$categorybusiness->business_brand_id}}' selected>{{$categorybusiness->business_brand_name}}</option>
                                                @else
                                                    <option value='{{$bus1->id}}'>{{$bus1->business_brand_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('business_brand_id'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_brand_id') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('business_brand_category_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Brand Category :</label>
                                    <div class="col-md-4">
                                        <input name="business_brand_category_name" type="text" id="business_brand_category_name" class="form-control" value="{{$categorybusiness->business_brand_category_name}}"/>
                                        @if ($errors->has('business_brand_category_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('business_brand_category_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Business Brand Image :</label>
                                    <div class="col-md-4">


                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="business_brand_category_image" style="opecity:0" placeholder="Upload Service Image" id="business_brand_category_image" type="file">
                                        </label>
                                        <img src="https://financialservicecenter.net/public/businessbrandcategory/{{$categorybusiness->business_brand_category_image}}" style="width: 100px; margin-left:10px" class="small-image" alt=""/>
                                    </div>

                                </div>

                                <input type="hidden" name="business_brand_category_image1" id="business_brand_category_image1" value="{{$categorybusiness->business_brand_category_image}}">
                                <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Url :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" id="link" class="form-control" value="{{$categorybusiness->link}}"/> @if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/business-brand-category')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getRequest')!!}?id=' + id, function (data) {
                        $('#business_cat_id').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                        })

                    });

                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category2', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('{!!URL::to('getRequestbrand')!!}?id=' + id, function (data) {
                        $('#business_brand_id').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#business_brand_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_brand_name + '</option>');
                        })

                    });

                });
            });
        </script>
@endsection()