@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header page-title" style="height:50px;">
            <div class="">
                <div class="" style="text-align:center;">
                    <h2>List of Business Brand Category </h2>
                </div>

            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <div style="text-align:right; margin-right:15px">
                            <h2><a class="btn btn-primary" href="{{route('business-brand-category.create')}}">Add New</a></h2>
                        </div>
                        <div class="box-header" style="padding:5px;">

                            <div class="box-tools pull-right" style="margin-right: 290px;margin-top: 8px;position: absolute;">
                                <div class="table-title">


                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            @if (session()->has('success') )
                                <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable2">

                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Business Names</th>
                                        <th>Business Category</th>
                                        <th>Business Brand</th>
                                        <th>Business Brand Category</th>
                                        <th>Business Brand Image</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $cnts = 0;?>
                                    @foreach($categorybusiness as $cat)
                                        <?php $cnts++;?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $cnts;?></td>
                                            <td>{{$cat->bussiness_name}}</td>
                                            <td>{{$cat->business_cat_name}}</td>
                                            <td>{{$cat->business_brand_name}}</td>
                                            <td>{{$cat->business_brand_category_name}}</td>
                                            <td style="text-align:center;"><img style="width: 100px;" src="{{url('public/businessbrandcategory')}}/{{$cat->business_brand_category_image}}" class="extra-small-image" alt=""/></td>
                                            <td style="text-align:center;">
                                                <a class="btn-action btn-view-edit" href="{{route('business-brand-category.edit', $cat->cid)}}"><i class="fa fa-edit"></i></a>
                                                <form action="{{route('business-brand-category.destroy',$cat->cid) }}" method="post" style="display:none" id="delete-id-{{$cat->cid}}">
                                                    {{csrf_field()}} {{method_field('DELETE')}}
                                                </form>
                                                <a class="btn-action btn-delete" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                        {event.preventDefault();document.getElementById('delete-id-{{$cat->cid}}').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--</div>-->

        <script>
            $(document).ready(function () {
                var table = $('#sampleTable2').DataTable({
                    "ordering": true,
                    dom: 'Bfrtlip',
                    "columnDefs": [{
                        "searchable": false,
                        "orderable": true,
                        "targets": 0
                    }],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                            //titleAttr: 'Copy',
                            title: $('h1').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                            // titleAttr: 'Excel',
                            title: $('h1').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                            // titleAttr: 'CSV',
                            title: $('h1').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                            //   titleAttr: 'PDF',
                            title: $('h1').text(),
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6], // Only name, email and role
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>&nbsp; Print',
                            title: $('h1').text(),
                            exportOptions: {
                                columns: ':not(.no-print)'
                            },
                            footer: true,
                            autoPrint: true
                        },
                    ],
                });
            });
        </script>
@endsection()