@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>License Edit</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">

                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('license.update',$position->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <input type="hidden" name="id" value="{{$position->id}}">
                                <div class="form-group {{ $errors->has('licensename') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">License Name :</label>
                                    <div class="col-md-4">
                                        <input name="licensename" type="text" id="licensename" class="form-control" value="{{$position->licensename}}"/> @if ($errors->has('question'))
                                            <span class="help-block">
											<strong>{{ $errors->first('licensename') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/license')}}">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            $('#renewalDropdown').on('change', function () {
                var renewalval = $('#renewalDropdown').val();
                //alert(renewalval);

                if (renewalval == 'Yes') {
                    //alert('');
                    $('#duedate').show();
                } else {
                    $('#duedate').hide();
                }
            });


        </script>
        <!--</div>-->
@endsection()