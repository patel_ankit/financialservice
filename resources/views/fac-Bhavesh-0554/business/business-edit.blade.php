@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Business</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('business.update',$business->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('bussiness_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Business Name :</label>
                                    <div class="col-md-4">
                                        <input name="bussiness_name" type="text" value="{{$business->bussiness_name}}" id="bussiness_name" class="form-control" value=""/>@if ($errors->has('bussiness_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('bussiness_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Business Image :</label>
                                    <div class="col-md-4">
                                        <input type="file" name="bussiness_image_name" id="bussiness_image_name" class="form-control" value="{{$business->bussiness_image_name}}"/>
                                        <label class="file-upload btn btn-primary">
                                            Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" name="bussiness_image_name" id="bussiness_image_name" type="file" value="{{$business->bussiness_image_name}}"/>
                                        </label><img src="https://financialservicecenter.net/public/business/{{$business->bussiness_image_name}}" style="width: 100px; margin-left:10px" title="{{$business->business_name}}" alt="{{$business->business_name}}" width="100px">
                                    </div>
                                    <input type="hidden" name="bussiness_image_name1" id="bussiness_image_name1" value="{{$business->bussiness_image_name}}">
                                </div>
                                <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Url :</label>
                                    <div class="col-md-4">
                                        <input name="link" type="text" value="{{$business->link}}" id="link" class="form-control" value=""/>@if ($errors->has('link'))
                                            <span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/business')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()