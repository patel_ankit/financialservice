<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
    <title>FSC - Admin</title>
    <style>
        .help-block {
            color: red
        }

        .semibold-text a {
            color: red;
        }
    </style>
</head>

<body>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
    <div class="logo">
        <img src="{{asset('public/dashboard/images/fsc_logo.png')}}" alt=""/>
    </div>
    <div class="login-box">
        <form class="login-form" method="post" action="" id="admin-login">
            {{ csrf_field() }}
            <h3 class="login-head"><i class="fa fa-logo-lg fa-fw fa-user"></i>SIGN IN </h3>

            <div class="form-group">
                <label class="control-label">USERNAME</label>
                <input class="form-control" type="text" name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">PASSWORD</label>
                <input class="form-control" type="password" name="password" id="newpassword" placeholder="Password">
                <span toggle="#newpassword" style="font-size: 25px;margin-left:-35px;margin-top: -30px;" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="utility">
                    <p class="semibold-text mb-0"><a href="adminpassword">Forgot Password ?</a></p>
                    <p class="semibold-text mb-0"><a href="{{ route('forgotusername.index') }}">Forgot Username ?</a></p>
                </div>
            </div>

            <div class="form-group btn-container">
                <button type="submit" name="submit" class="btn btn-primary btn-block">SIGN IN<i class="fa fa-sign-in fa-lg"></i> <img class="dvloader" src="https://www.drupal.org/files/issues/throbber_12.gif" style="width: 21px;display:none"></button>
            </div>
            <center>
                @if ( session()->has('success') )<br><br>
                <div class="alert alert-success alert-dismissable" style="display: inline-block;">{{ session()->get('success') }}</div>
                @endif
                @if ( session()->has('error') )<br><br>
                <div class="alert alert-danger alert-dismissable" style="display: inline-block;">{{ session()->get('error') }}</div>
                @endif
                <div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div>
            </center>
        </form>
        <form class="forget-form" method="POST" action="{{ route('fac-Bhavesh-0554.password.email') }}" id="forgotuser">
            {{ csrf_field() }}
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">EMAIL</label>
                <input class="form-control" id="email" type="email" name="email" value="{{ old('email') }}" required placeholder="Email">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div id="emailmsg"></div>
            <div class="form-group btn-container">
                <button type="submit" name="submit" class="btn btn-primary btn-block">RESET<i class="fa fa-sign-in fa-lg"></i></button>
            </div>
            <div class="form-group mt-20">
                <p class="semibold-text mb-0"><a data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
            </div>
            <center>
                <div id="Register1" style="margin-top:20px;width: 100%;display: inline-block;"></div>
            </center>
        </form>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>
</section>


<script src="{{asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/plugins/pace.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/main.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
    $(document).ready(function () {
        $("#loginuser").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: "required"
            },

            // Specify the validation error messages
            messages: {

                email: {
                    required: "Please Enter Email Address",
                    email: "Please Enter Proper Email"
                },
                password: "Please enter password"

            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#forgotuser").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            // Specify the validation error messages
            messages: {

                email: {
                    required: "Please Enter Email Address",
                    email: "Please Enter Proper Email"
                }

            },
            submitHandler: function (form) {

                // form.submit();
            }
        });

        function validationError(errorMap, errorList) {
            if (errorList.length == 0)
                return;
            var msgs = [];
            for (var err = 0; err < errorList.length; err++) {
                msgs.push({message: errorList[err].message});
            }
            $("#errorlist").notification({caption: "One or more invalid inputs found:", messages: msgs, sticky: true});
        }

        function validationSuccess(form) {
            $("#loginuser").ajaxSubmit({
                beforeSubmit: formRequest,
                success: formResponse,
                dataType: 'json'
            });
        }

        function formRequest(formData, jqForm, options) {
        }

        function formResponse(responseText, statusText) {
            if (statusText == "success") {
                if (responseText.type == "success") {
                    $("#errorlist").notification({
                        caption: "Login Successfully", type: "information", sticky: false, onhide: function () {
                            window.location = "index.php";
                        }
                    });
                } else if (responseText.type == "msg") {
                    $("#errorlist").notification({
                        caption: "Invalid Username or Password", type: "warning", sticky: true, onhide: function () {

                        }
                    });
                }
            } else {
                $("#errorlist").notification({caption: "Unable to communicate with server.", type: "warning", sticky: true});
            }
        }
    });
</script>

<script>
    $.ajaxSetup({headers: {'X-CSRF-Token': $('input[name="_token"]').val()}});

    $('.login-form').bootstrapValidator({

        message: 'This value is not valid',
        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function (validator, form, submitButton) {
            $(".dvloader").show();
            $.ajax({
                type: "post",
                url: "{{ route('fac-Bhavesh-0554.login')}}",
                data: $('.login-form').serialize(),
                success: function (msg) {
                    $(".dvloader").hide();
                    if (msg == 'save1') {
                        $('#Register').html('<span class="alert alert-danger">Your Username And Password Invalid ...</span>');

                    } else {
                        $('#Register').html('<span class="alert alert-success">You are successfully login...</span>');
                        setTimeout(function () {
                            location.href = "{{route('fac-Bhavesh-0554.dashboard')}}";
                        }, 0);
                    }
                },
                error: function () {

                }
            });//close ajax
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email !!!'
                    },
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
                    }

                }
            },
            password: {

                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Password !!!'
                    }
                }
            },
        } // end field
    });// bootstrapValidator


</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
</body>
</html>