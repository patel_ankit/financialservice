@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Service Inquiry</h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <br>
                        <div class="card-body">
                            <form method="post" action="{{route('insurance.store')}}" class="form-horizontal" id="contact" name="content" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group {{ $errors->has('date_attend') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Date Attend :</label>
                                    <div class="col-md-5">
                                        <input name="date_attend" type="text" id="date_attend" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('course_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Course Name :</label>
                                    <div class="col-md-5">
                                        <input name="course_name" type="text" id="course_name" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('school_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">School Name :</label>
                                    <div class="col-md-5">
                                        <div class="">
                                            <input name="school_name" type="text" id="school_name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('field_of_study') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Field of Study :</label>
                                    <div class="col-md-5">
                                        <div class="">
                                            <input name="field_of_study" type="text" id="field_of_study" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('ethics_hrs') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Ethics Hrs. :</label>
                                    <div class="col-md-5">
                                        <div class="">
                                            <input name="ethics_hrs" type="text" id="ethics_hrs" class="form-control txt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('general_hrs') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">General Hrs. :</label>
                                    <div class="col-md-5">
                                        <div class="">
                                            <input name="general_hrs" type="text" id="general_hrs" class="form-control txt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('total_ce_hrs') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Total CE Hrs :</label>
                                    <div class="col-md-5">
                                        <div class="">
                                            <input name="total_ce_hrs" type="text" id="total_ce_hrs" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-3">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/cestatus')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#date_attend").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
                //endDate: "today"
            });
            //iterate through each textboxes and add keyup
            //handler to trigger sum event
            $(".txt").each(function () {
                $(this).keyup(function () {
                    calculateSum();
                });
            });

        });

        function calculateSum() {

            var sum1 = 0;
            //iterate through each textboxes and add the values
            $(".txt").each(function () {

                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum1 += parseFloat(this.value);
                }

            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            //	$("#sum").html(sum.toFixed(2));
            document.getElementById('total_ce_hrs').value = sum1;
        }
    </script>
@endsection()