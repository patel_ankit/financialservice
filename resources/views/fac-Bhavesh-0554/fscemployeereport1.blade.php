<table class="table table-hover table-bordered" id="example">
    <thead>
    <tr>

        <th>Date Day</th>
        <th>Name</th>
        <th>Clock In</th>
        <th>Lunch In / Out</th>
        <th>Clock Out</th>
        <th>Total Hours(D)</th>
        <th>EE Notes</th>
        <th>IP Address</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Date Day</th>
        <th>Name</th>
        <th>Clock In</th>
        <th>Lunch In / Out</th>
        <th>Clock Out</th>
        <th>Total Hours(D)</th>
        <th>EE Notes</th>
        <th>IP Address</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($employee as $emp)
        <?php
        $start = date_create($emp->emp_in);
        if (empty($emp->emp_out)) {
            $date = date('Y-m-d h:i:s');
            $end = date_create($date);
        } else {
            $end = date_create($emp->emp_out);
        }
        $diff = date_diff($end, $start);


        $tim = $diff->h . ' : ' . '' . $diff->i;
        ?>
        <tr>
            <td>{{date('Y-d-m',strtotime($emp->emp_in_date))}}</td>
            <td>{{$emp->name}}</td>
            <td>{{date('h:i:s',strtotime($emp->emp_in))}}</td>
            <td>@if($emp->launch_in== null) 00:00:00-00:00:00 @else {{date('h:i:s',strtotime($emp->launch_in)).'-'.date('h:i:s',strtotime($emp->launch_out))}}@endif</td>
            <td>@if($emp->emp_out== null)  00:00:00 @else{{date('h:i:s',strtotime($emp->emp_out))}}@endif</td>
            <td>{{$tim}}</td>
            <td>{{$emp->note}}</td>
            <td>{{$emp->ip_address}}</td>
        </tr>
    @endforeach
    </tbody>
</table>