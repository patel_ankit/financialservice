<!--<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}

</style>
<div class="content-wrapper">
	<div class="page-title">
		<h1>Work Status</h1>
	</div>
	<div class="row"> 

<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-9" style="margin-left:-1.6%">
              <div class="row">
              <div class="col-md-2">
                  <div class="col-md-"><label style="margin-left:2%;margin-top: 11px;">Filter: </label></div>
                <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
        <option value="1">All</option>
        <option value="2" selected="">Active</option>
        <option value="3">Inactive</option>
        <option value="4">Client Id</option>
        <option value="5">Client Name</option>
       </select>
    </div>
       <div class="col-md-4">
         <div class="col-md-1"><label style="margin-top: 11px;">Search: </label></div>
    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
        <option value="All" selected="">All</option>
        <option value="Type">Client ID</option>
        <option value="EE / User ID">Company Name</option>
        <option value="Employee Name">Bussiness Name</option>
        <option value="Email ID">Bussiness Telephone</option>
        <option value="Tel. Number">Contact Name</option>
        <option value="Contact Telephone">Contact Telephone</option>
    </select>
    </div>
     <div class="col-md-3">
            <div class="col-md-1"><label style="margin-top: 11px;"> &nbsp;</label></div>
     <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
            <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col7" data-column="6" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	
            </div>
				<div class="col-md-12">
					<div class="table-title">
					</div>
										<div class="table-responsive">
						<div id="example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons" style="margin-bottom:10px;">          <button class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-files-o"></i> &nbsp; Copy</span></button> <button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-excel-o"></i>&nbsp; Excel</span></button> <button class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-text-o"></i> &nbsp; CSV</span></button> <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-pdf-o"></i>&nbsp;  PDF</span></button> <button class="dt-button buttons-print" tabindex="0" aria-controls="example"><span><i class="fa fa-print"></i>&nbsp; Print</span></button> </div><div id="example_filter" class="dataTables_filter"></div></div><table class="table table-hover table-bordered dataTable no-footer" id="example" role="grid" aria-describedby="example_info">
							<thead>
								<tr role="row"><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 19px;" aria-label="No: activate to sort column ascending">No</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 70px;" aria-label="Client ID: activate to sort column ascending">Client ID</th><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 185.933px;" aria-label="Company Name (Legal Name): activate to sort column descending" aria-sort="ascending">Client Legal Name <br><span style="font-size: 13px;">(Legal Name)</span></th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 65px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
							</thead>							
							<tbody>
	
														<tr role="row" class="odd">
									<td style="text-align:center">1</td>
									<td>GA-214-1</td>
									<td class="sorting_1">Ashish of Gwinnett, Inc.</td>
								    <td style="text-align:center;">                                     <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73/edit"><i class="fa fa-edit"></i></a>
									<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-73').submit();} else{event.preventDefault();}" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73"><i class="fa fa-trash"></i></a>
<form action="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73" method="post" style="display:none" id="delete-id-73">
                                        <input type="hidden" name="_token" value="UIc72HzKfEcynHkwpo8DmzSmaVrzZquPqmhXJjIr"> <input type="hidden" name="_method" value="DELETE">
                                        </form></td>
								</tr><tr role="row" class="even">
									
								</tr></tbody>
						</table><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 11 entries </div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">Next</a></li></ul></div></div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>!-->


@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <style>
        .search-btn {
            position: absolute;
            top: 46px;
            right: 16px;
            background: transparent;
            border: transparent;
        }

        #countryList {
            margin: 40px 0px 0px 0px;
            padding: 0px 0px;
            border: 0px solid #ccc;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 245px;
            z-index: 99999;
        }

        #countryList li {
            border-bottom: 1px solid #dcdcdc;
            background: #f4f4f4;
        }

        #countryList li a {
            padding: 10px 10px;
            display: block;
        }

        #countryList li:hover {
            background: #e7e5e5;
        }

        .new_images_sec .col-md-4 {
            position: relative;
        }

        .new_images_sec .col-md-4 .arrow {
            position: absolute;
            top: 10px;
            right: -6px;
        }

        .new_images_sec .col-md-4 {
            position: relative;
        }

        .new_images_sec .col-md-4 img {
            height: 42px !important;
        }

        .new_images_sec .col-md-4 .arrow {
            position: absolute;
            top: 10px;
            right: -6px;
        }

        .new_images_sec .col-md-4.lastimgbox .arrow {
            position: absolute;
            top: 10px;
            left: -6px;
        }

        .searchboxmain {
            float: left;
            display: FLEX;
            margin-top: -7px;
            justify-content: space-between;
        }

        .searchboxmain .form-control {
            margin-right: 10px !important;
        }

        .clear {
            clear: both;
        }

        .page-title h1 {
            float: left;
            margin-left: 15%;
        }

        .page-title {
            padding: 15px 30px 5px !important;
        }

        #countryList {
            margin: 40px 0px 0px 0px;
            padding: 0px 0px;
            border: 0px solid #ccc;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 320px;
            z-index: 99999;
        }

        #countryList li {
            border-bottom: 1px solid #0070b1;
            background: #038bd9;
        }

        #countryList li a {
            padding: 10px 10px;
            display: block;
            color: #fff !important;
            text-align: left;
        }

        #countryList li:hover {
            background: #27a1e7;
        }

        .nav.nav-tabs li {
            width: 15.8% !important;
        }

        .clear {
            clear: both;
        }

        .mt25 {
            margin-top: 25px;
        }

        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .pointernone {
            pointer-events: none
        }

        .pad20 {
            padding: 20px;
        }

        .nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
            cursor: default;
        }

        .nav-tabs > li {
            margin: 0px 0 0 8px;
        }

        .nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
            border-color: #000 !important;
            color: #000 !important;
            background: #ffff99;
            border-radius: 5px !important;
        }

        .nav-tabs {
            padding: 12px;
            border: 1px solid #3598dc !important;
        }

        .btnaddmore {
            background: #337ab7;
            display: inline-block;
            margin-bottom: 5px;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .btnremove {
            background: #ff0000;
            padding: 6px 10px;
            color: #fff;
            border-radius: 4px
        }

        .padzero {
            padding: 0px !important;
        }

        .officermainbox {
            border: 1px solid #ccc;
            padding: 20px;
            margin-bottom: 30px;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border-color: #ccc !important;
        }

        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .card ul li {
            width: 24% !important;
        }

        .card ul.test li a {
            background: #ffcc66 !important;
        }

        .card ul.test li.active a {
            background: #00a0e3 !important;
        }

        .card ul li a {
            display: block;
            width: 100%;
            color: #333;
            text-transform: capitalize;
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);
            border: 1px solid #979800 !important;
        }

        .card ul li a:hover {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover {
            color: #fff !important;
            background: #12186b !important;
            border: 1px solid #12186b !important;
        }

        .card .nav-tabs {
            border: 0px !important;
        }

        .feeschargesbox .form-control {
            text-align: right;
        }

        .hrdivider {
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
            margin-top: 33px;
        }

        .hrdivider2 {
            margin-top: 14px;
            margin-bottom: 30px;
            border-bottom: 2px solid #ccc !important;
            width: 100%;
            height: 1px;
        }

        .officerchange .form-control {
            background: #fff !important;
        }

        .add-row {
            background: #007bff;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .delete-row {
            background: #dc3545;
            padding: 3px 5px;
            color: #fff;
            border-radius: 3px;
            cursor: pointer;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #FFF !important;
            border: 1px solid #12186b !important;
            background: #12186b !important;
            cursor: default;
            border-radius: 8px;
        }
    </style>
    <div class="content-wrapper">

        <div class="page-title">
            <div class="searchboxmain">
                <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">
                {{ csrf_field() }}
                <ul id="countryList"></ul>
                <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important; padding:10px 20px;" href="https://financialservicecenter.net/fac-Bhavesh-0554/workstatus">Reset</a>

            </div>
            <h1>Work Status</h1>
            <div class="clear"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="">

                    @if(!empty($common->filename))
                        <section class="content-header">
                            <div class="new-page-title" style="padding:0px;margin: -7px 0 7px 0;">
                                <div class="new-page-title-tab">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <div class="new_client_id">
                                            <p>{{$common->filename}} </p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <div class="new_company_name">
                                            <label>Company Name :</label>
                                            <p>{{$common->company_name}}</p>
                                        </div>
                                        <div class="new_company_name">
                                            <label>Business Name :</label>
                                            <p>{{$common->business_name}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <div class="new_images_sec">
                                            @if(empty($common->business_cat_id))
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    @else
                                                        @if(empty($common->business_brand_category_id))
                                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                                @else
                                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                                        @endif
                                                                        @endif
                                                                        @foreach($business as $busi)
                                                                            @if($busi->id==$common->business_id)
                                                                                <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive">
                                                                                <div class="arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                                @foreach($category as $cate)
                                                                    @if($common->business_cat_id==$cate->id)
                                                                        @if(empty($common->business_brand_category_id))

                                                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                                            @else
                                                                                <!--  <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>-->
                                                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                                                        @endif
                                                                                        <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive"/>
                                                                                    </div>
                                                                                @endif
                                                                                @endforeach
                                                                                @foreach($cb as $bb1)
                                                                                    @if($common->business_brand_category_id == $bb1->id)

                                                                                        <div class="col-md-4 col-sm-12 col-xs-12 lastimgbox">
                                                                                            <div class="arrow">
                                                                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                                            </div>
                                                                                            <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive"/>
                                                                                        </div>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                            </div>
                                                </div>
                                        </div>
                        </section>
                    @else

                    @endif
                    <div class="col-md-12">


                        @if(!empty($common->filename))
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" id="myTab" style="padding:3px;">
                                        <li class="active"><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
                                        <li><a href="#tab4primary" data-toggle="tab">License</a></li>
                                        <li><a href="#tab6primary" data-toggle="tab">Taxation</a></li>
                                        <li><a href="#tab3primary" data-toggle="tab">4</a></li>
                                        <li><a href="#tab5primary" data-toggle="tab">5</a></li>
                                        <li><a href="#tab7primary" data-toggle="tab">6 </a></li>
                                    </ul>
                                </div>

                                <?php
                                //echo "<pre>";print_r($common);?>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active disablebox newcheckbox" id="tab1primary">
                                        <div class="col-md-12" style="padding-left:5px; margin-top:5px;">

                                            <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                                                <div class="col-md-4" style="padding-top:10px; padding-right:0px; width:36.5%;">
                                                    <?php $dbcolors = $common->type_of_entity_answer; $str = explode(",", $dbcolors);?>
                                                    <label class="col-md-1 text-right padtop7" style="width: 60px;padding-left: 0px; margin-top:10px;">State :</label>
                                                    <div class="col-md-3" style="width: 95px;padding-left: 0px;padding-right: 0px;"><input type="text" value="@if(empty($str[2])) @else {{$str[2]}} @endif" style="width:80px;" class="form-control fsc-input"/></div>
                                                    <label class="col-md-2 text-right padtop7" style="width: 85px;padding-left: 0px;margin-top:10px;">Control #:</label>
                                                    <div class="col-md-3" style="width: 100px;padding-left: 0px;padding-right: 0px;"><input type="text" value="{{$common->contact_number}}" class="form-control fsc-input" style="width:100px;"/></div>
                                                </div>
                                                <div class="col-md-8" style="width: 63%; padding-right:0px;">
                                                    <div class="panel-heading">
                                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                            <li style=" width: 30.2% !important;" class="active"><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>
                                                            <li style=" width: 22.2% !important;"><a href="#subtab2primary" class="" data-toggle="tab">Share Ledger</a></li>
                                                            <li style=" width:20.2% !important;"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>
                                                            <li style=" width:20.2% !important;"><a href="#subtab3primary" class="" data-toggle="tab">Minutes</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-content pad20" style="padding-top:5px; ">
                                                <div class="tab-pane fade in active  newcheckbox" id="subtab1primary">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered tablestriped">
                                                            <thead>
                                                            <tr>
                                                                <th>Year</th>
                                                                <th>Amount Paid</th>
                                                                <th>Paid date</th>
                                                                <th>Paid By</th>
                                                                <th>Done By</th>
                                                                <th>Receipt</th>
                                                                <th>Officer</th>
                                                                <th>Date</th>
                                                                <th>Action</th>
                                                                <th>Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="text-center"><?php echo $common->work_year;?></td>
                                                                <td class="text-right">$ <?php echo $common->work_annualfees;?></td>
                                                                <td class="text-center">Paid date</td>
                                                                <td><?php echo $common->work_paidby;?></td>
                                                                <td>Done by name</td>
                                                                <td class="text-center"><a data-toggle="modal" data-target="#myModals_<?php echo $common->cid;?>">Annula Receipt</a>
                                                                </td>
                                                                <td class="text-center"><a data-toggle="modal" data-target="#myModals1_<?php echo $common->cid;?>">Officer</a></td>
                                                                <td class="text-center">Datebox</td>
                                                                <td class="text-center">Delete</td>
                                                                <td><?php echo $common->work_status;?></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>


                                                        <div id="myModals_<?php echo $common->cid;?>" class="modal fade">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title">Annual Receipt </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>
                                                                            <iframe height="450" width="530" src="<?php echo url('/');?>/public/adminupload/<?php echo $common->work_annualreceipt;?>"></iframe>
                                                                        </p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div id="myModals1_<?php echo $common->cid;?>" class="modal fade">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title">Work Officer </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>
                                                                            <iframe height="450" width="530" src="<?php echo url('/');?>/public/adminupload/<?php echo $common->work_officer;?>"></iframe>
                                                                        </p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="clear"></div>

                                                <div class="tab-pane fade  newcheckbox" id="subtab2primary">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>NAME OF CERTIFICATE HOLDER</th>
                                                                <th>CERTIFICATES ISSUED NO. SHARES*</th>
                                                                <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                                                                <th>AMOUNT PAID THEREON</th>
                                                                <th>DATE OF TRANSFER OF SHARES*</th>
                                                                <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                                                                <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>A</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>B</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>C</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>D</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade  newcheckbox" id="subtab3primary">
                                                    Amendment
                                                </div>
                                                <div class="tab-pane fade newcheckbox" id="subtab4primary">
                                                    Minutes
                                                </div>
                                            </div>


                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#country_name').keyup(function () {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('workstatus.fetch') }}",
                        method: "POST",
                        data: {query: query, _token: _token},
                        success: function (data) {
                            $('#countryList').fadeIn();
                            $('#countryList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'tr', function () {
                $('#country_name').val($(this).text());
                $('#countryList').fadeOut();
            });

        });


    </script>
@endsection