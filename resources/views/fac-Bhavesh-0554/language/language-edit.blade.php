@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h1>Language</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="{{route('language.update',$language->id)}}" class="form-horizontal" id="languagename" name="languagename">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="form-group {{ $errors->has('language_name') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Language Name :</label>
                                    <div class="col-md-4">
                                        <input name="language_name" type="text" value="{{$language->language_name}}" id="language_name" class="form-control" value=""/>@if ($errors->has('language_name'))
                                            <span class="help-block">
											<strong>{{ $errors->first('language_name') }}</strong>
										</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2 row">
                                            <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/language')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->
@endsection()