@extends('front-section.app')
@section('main-content')

    <style>
        .simpletext {
            font-size: 12px;

        }

        .calc-text {
            font-size: 12px;
            margin-top: 10px;
        }

        .selmonth {
            font-size: 11px;
        }

        .pwd {
            color: red;
            font-size: 10px;
            padding: 11px;
        }

        .client-login {
            font-size: 18px;
            color: #fff;
            padding: 3px 0;
        }

        a:focus, a:hover {
            color: #428bca !important;
            text-decoration: none;
            text-decoration-color: currentcolor;
            text-decoration-line: none;
            text-decoration-style: solid;
        }

        .fsc-form-submit {
            padding: 7px 12px;
        }

        #monthlyPaymentCount {
            background-color: green !important;
            margin-top: 15px;
            border-color: #428bca !important;
        }

        .cc {
            text-align: right;
            position: absolute;
            top: 6px;
            font-size: 18px;
            right: 26px;
        }

        .cc:after {
            content: "\f295";
            font-family: FontAwesome;
            color: #b4b4b4;
            font-size: 14px;
        }

        .dd {
            text-align: right;
            position: absolute;
            top: 6px;
            font-size: 18px;
            right: 26px;
        }

        .dd:after {
            content: "\f15b";
            font-family: FontAwesome;
            color: #b4b4b4;
            font-size: 14px;
        }
    </style>
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
                <h4>TOOLS</h4>
            </div>
        </div>

        <h2 style="margin-top: 3%; margin-bottom: 2%;">Choose any tool :</h2>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs">
                <li class="active"><img data-toggle="tab" href="#home" style="cursor:pointer;" id="monthly" class="img-responsive" src="images/calculator1.png"/></li>
                <li><img data-toggle="tab" href="#menu1" style="cursor:pointer;" class="img-responsive" id="payment" src="images/calculator2.png"/></li>
                <li><img data-toggle="tab" href="#menu2" style="cursor:pointer;" class="img-responsive" id="timesheet" src="images/calculator3.png"/></li>
                <li><img data-toggle="tab" href="#menu3" style="cursor:pointer;" class="img-responsive" id="timesheet1" src="images/Aurto Calculator(1).png"/></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="home" role="tabpanel" class="tab-pane fade in active">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>Monthly Payment Calculator</h4>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Loan Amount :</label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="text" class="form-control fsc-input" id="loanAmount1" placeholder="Amount" name='loanAmount1'>
                                <input name="loanAmount" type="hidden" class="form-control fsc-input" id="loanAmount" size="6">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Rate of Interest : </label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="text" class="form-control fsc-input" onkeypress="return isNumberKey(event)" id="rateofIntrest1" name='rateofIntrest1' placeholder="">
                                <input name="rateofIntrest" type="hidden" class="form-control fsc-input" id="rateofIntrest" size="6">

                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Term (Year) : </label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="text" class="form-control fsc-input" id="term" name='term' placeholder="Enter in Year">
                                <!--<div class="dd"></div>-->
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" onclick='Countmonthlypayemnet()' class="pull-left btn btn-primary btn-lg fsc-form-submit" style="">CALCULATE</button>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                <button type="reset" class="btn btn-default btn-lg fsc-form-submit" onclick="resetmonthlycalc()">RESET</button>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;display:none" id='monthlypaymentcalc'>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <center>
                                    <p style='font-weight:bold;font-size:15px;'>Your Monthly Payment <br>
                                        <button type="button" class="btn btn-primary btn-lg fsc-form-submit" id='monthlyPaymentCount' style="width: 200px !important;float: inherit;"></button>
                                    </p>
                                </center>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

            </div>

            <div id="menu3" role="tabpanel" class="tab-pane fade">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="col-lg-12 col-md-12 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>Auto Mileage Record Sheet</h4>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                    <div class="col-lg-12 col-md-12 col-sm-10 col-xs-10 fsc-content-box">


                        <form name="" method="post" action="#" id="">
                            <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" id="tblCustomers" style="magin-left: 5px;" class="order-list">
                                <tbody>
                                <tr>
                                    <td align="left" colspan="6" valign="bottom">
                                        <div class="">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Company Name :</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="companyname" type="text" class="form-control fsc-input" id="companyname">

                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>

                                    <td colspan="6" align="left" valign="bottom">

                                        <div class="">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Employee Name :</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="employeename" type="text" class="form-control fsc-input" id="employeename">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="6" align="left" valign="bottom">
                                        <div class="">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Period : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="period" type="text" class="form-control fsc-input" id="period" placeholder="Enter in Period">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Date : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="date" type="text" class="form-control fsc-input" id="date" placeholder="Date">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Perticulars : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="perticulars" type="text" class="form-control fsc-input" id="perticulars" placeholder="Enter in Perticulars">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Start Mile : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="startmile" type="text" class="form-control fsc-input" id="startmile" placeholder="Enter in Start Mile">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">End Mile : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="endmile" type="text" class="form-control fsc-input" id="endmile" placeholder="Enter in End Mile">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Net Mile : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="netmile" type="text" class="form-control fsc-input" id="netmile" placeholder="Enter in Net Mile">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="" align="left" valign="bottom">
                                        <div class="" style="margin-top: 2%;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Notes : </label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="notes" type="text" class="form-control fsc-input" id="notes" placeholder="Enter in Notes">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                </tr>


                                </tbody>
                            </table>
                            <div class="col-md-3">
                                <input type="submit" name="Submit" class="btn btn-primary btn-lg" style="margin-left: 30%;width: 129px;font-size: 19px;
                                                                height: 40px;" value="Calculate">
                            </div>
                            <div class="col-md-2">
                                <input type="reset" name="Submit2" value="Reset" class="btn btn-primary btn-lg" style="width: 124px;font-size: 19px;
                                                                height: 40px; margin-left: 15px" onclick="javascript: loanAmt.focus();">
                            </div>
                            <div class="col-md-2">
                                <input type="button" class="btn btn-primary btn-lg" style="margin-left: 30%;width: 129px;font-size: 19px;
                                                                height: 40px;" id="addrow" value="Add Row"/>
                            </div>

                            <div class="col-md-2">
                                <input type="button" style="margin-left: 30%;width: 129px;font-size: 19px;
                                                                height: 40px;" class="btn btn-primary btn-lg" id="btnExport" value="Export"/>

                            </div>
                    </div>
                    <!--	<div class="col-md-12" id="resultPayment" style="display:none;background-color:#ccc;font-size:15px;padding:8px;margin-top:10px">
                            <h3>Payment Result</h3>
                            <table class="table table-bordered">
                                <tr>
                                    <th id="monthcount"></th>
                                </tr>
                                <tr>
                                    <th id="ratecount"></th>
                                </tr>
                                <tr>
                                    <th id="termscount"></th>
                                </tr>
                                <tr>
                                    <th id="Costcount"></th>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Payment Month</th>
                                    <th>Monthly FIX Payment</th>
                                    <th>Interest Payment</th>
                                    <th>Principal Payment</th>
                                    <th>Balance</th>
                                </tr>
                                <tr id="monthCountdisplay"></tr>
                            </table>
                        </div>-->
                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

            </div>


            <div id="menu1" role="tabpanel" class="tab-pane fade">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>Payment Schedule Calculator</h4>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">
                        <h4 style="margin: 2% 4% 0%; text-align: center; font-family: 'Proza Libre', serif;">This tool will calculate your payment schedule based on your loan amount, interest rate and term that you provided.</h4>
                        <form method="post">
                            <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" id="calctab" style="magin-left: 5px;">
                                <tbody>
                                <tr>
                                    <td align="left" colspan="3" valign="bottom">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Loan Amount :</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="loanAmt" type="text" class="form-control fsc-input" id="loanAmt" onkeyup="javascript: if(/[^0-9.]/.test(this.value)) {alert('Enter Digit value only!'); this.focus();this.value='0,00,000,000'; this.select();}" size="10">
                                                <input name="loanAmt1" type="hidden" class="form-control fsc-input" id="loanAmt1" size="10">

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 10px"></td>
                                    <td style="height: 10px"></td>
                                    <td style="height: 10px"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" valign="bottom">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Rate of Interest :</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="intRate" type="text" class="form-control fsc-input" id="intRate" onkeyup="javascript: if(/[^\d+([,\.]\d{1,2})?$" .test(this.value))="" { alert('enter="" digit="" value="" only!');="" this.focus();this.value="00.00" ;="" this.select();}"="" size="6">
                                                <input name="intRate1" type="hidden" class="form-control fsc-input" id="intRate1" size="6">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" valign="bottom">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                <label class="fsc-form-label">Term (Year) : </label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input name="term" type="text" class="form-control fsc-input" id="paymentterm" placeholder="Enter in Year" onkeyup="javascript: if(/[^0-9.]/.test(this.value)) { alert('Enter Digit value only!'); this.focus();this.value='00.00'; this.select();}" size="6" id="Text1">


                                                <!--<div class="dd"></div>-->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="14" colspan="3" align="center" valign="bottom">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" align="center" valign="bottom">
                                        &nbsp;
                                    </td>
                                    <td height="30" align="right" valign="bottom">
                                        <input type="submit" name="Submit" class="btn btn-primary btn-lg" style="margin-left: 30%;width: 129px;font-size: 19px;
                                                                height: 40px;" value="Calculate" onclick="return calculateIt12(loanAmt1.value, intRate1.value, term.value);">
                                    </td>
                                    <td align="left" valign="bottom">
                                        <input type="reset" name="Submit2" value="Reset" class="btn btn-primary btn-lg" style="width: 124px;font-size: 19px;
                                                                height: 40px; margin-left: 15px" onclick="javascript: loanAmt.focus();">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="19" colspan="3" align="center" valign="bottom">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="3" align="left" valign="bottom">
                                        <p>
                                        </p>
                                        <div class="selmonth" id="Div1">
                                            <strong>Here is the Payment Schedule for the specified number of years ...</strong>
                                        </div>
                                        <p></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="3" align="left" valign="bottom">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="3" align="left" valign="bottom">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="3" align="left" valign="bottom">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--	<div class="col-md-12" id="resultPayment" style="display:none;background-color:#ccc;font-size:15px;padding:8px;margin-top:10px">
                                    <h3>Payment Result</h3>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th id="monthcount"></th>
                                        </tr>
                                        <tr>
                                            <th id="ratecount"></th>
                                        </tr>
                                        <tr>
                                            <th id="termscount"></th>
                                        </tr>
                                        <tr>
                                            <th id="Costcount"></th>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Payment Month</th>
                                            <th>Monthly FIX Payment</th>
                                            <th>Interest Payment</th>
                                            <th>Principal Payment</th>
                                            <th>Balance</th>
                                        </tr>
                                        <tr id="monthCountdisplay"></tr>
                                    </table>
                                </div>-->
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

                </div>

            </div>

            <div id="menu2" class="tab-pane fade">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-section-head">
                        <h4>Time Sheet Calculator</h4>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 fsc-content-box">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"></div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Time In : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="text" class="form-control fsc-input" id="timeIn" placeholder="HH : MM">
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                <select class="form-control fsc-input" id="timeIn_1" name="timeIn_1" placeholder="HH : MM">
                                    >
                                    <option value="am">AM</option>
                                    <option value="pm">PM</option>
                                </select></div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"></div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                <label class="fsc-form-label">Time Out :</label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <input type="text" class="form-control fsc-input" id="timeOut" placeholder="HH : MM">
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                <select class="form-control fsc-input" id="timeIn_2" name="timeIn_2" placeholder="HH : MM">

                                    <option value="am">AM</option>
                                    <option value="pm">PM</option>
                                </select></div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" onclick='CountTimeSheetSchedule()' class="btn btn-primary btn-lg fsc-form-submit">CALCULATE</button>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;display:none" id='timsheetcalc'>
                            <div class="col-md-12 " style="margin-top:10px">
                                <table class="table table-bordered" style='background-color:#ccc;font-size:15px'>
                                    <tr id="timeperoidTable">
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                        <th>Work Hour</th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

                </div>

            </div>

        </div>

    </div>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script type="text/javascript" src="{{URL::asset('public/frontcss/js/moment.js')}}"></script>
    <!--<script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-datetimepicker.min.js')}}"></script>-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="{{URL::asset('public/frontcss/js/pcsFormatNumber.jquery.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#intRate").change(
                function () {

                    // alert("changing");
                    var num = parseFloat($("#intRate").val());
                    $("#intRate").val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                    $("#intRate1").val(num.toFixed(2));
                }
            );
            $("#loanAmt").change(
                function () {
                    // alert("changing");
                    var num1 = parseFloat($("#loanAmt").val());//alert(num1);
                    $("#loanAmt").val(num1.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $("#loanAmt1").val(num1.toFixed(2));
                }
            );
            //$(':input').setMask();
            //$('input[data-mask]').each(function () {
            //    $(this).setMask($(this).data('mask'));
            //});

        });
    </script>
    <script language="javascript" type="text/javascript">
        //Calculation and printing in the prdefined location.
        //Get the help from http://www.w3schools.com/js/js_examples_3.asp
        function calculateIt12(amt, rate, trm) {//parseFloat(amt);
            if (amt == "" || parseFloat(amt) == 0 || rate == "" || parseFloat(rate) == 0) {
                alert("Either Amount or Rate remains blank or contains 0 value!");
                return false;
            }
            if (!(/\./.test(amt))) {
                if (/^[0]/.test(amt)) {
                    amt = parseFloat(amt).toString();
                }
            }
            //reference to the 8th row of table that is having id calctab
            var tabRef8 = document.getElementById("calctab").rows[8].cells;
            //reference to the 8th row of table that is having id calctab
            var tabRef9 = document.getElementById("calctab").rows[9].cells;
            //reference to the 9th row of table that is having id calctab
            var tabRef10 = document.getElementById("calctab").rows[10].cells;
            //FMInst var contain the amount for the Fix Installment of each months
            //amount of the fixed payment is determined by 
            //M = Pi/[q(1-[1+(i/q)]-nq)]
            FMInst = [(parseFloat(amt)) * (parseFloat(rate) / 100)] / [12 * (1 - Math.pow([1 + (parseFloat(rate) / (100 * 12))], (-(parseInt(trm) * 12))))];
            //html code for Payment Result
            var html = "<table rules='rows' width='100%' align='center' border='1' cellpadding='0' cellspacing='0' bordercolor='#428bca' style='border-collapse:collapse;margin-left:3px;'>";
            html = html + "<tr><td colspan='2' align='center' valign='middle' height='25' bgcolor='#428bca' class='client-login'><b>Payment Result</b></td></tr>";
            html = html + "<tr class='simpletext'><td ><b>&nbsp;Fix Mothly Payment</b></td><td align='right'>" + CommaFormattedCurrency12(FMInst.toString().substring(0, FMInst.toString().indexOf(".") + 3)) + "</td></tr>";
            html = html + "<tr class='simpletext'><td><b>&nbsp;Rate of Interest</td></b><td align='right'>" + rate + "%</td></tr>";
            html = html + "<tr class='simpletext'><td><b>&nbsp;Term(years)</b></td><td align='right'>" + trm + "</td></tr>";
            var totint_amt = 0;
            var yrly_bal = parseInt(amt);
            for (i = 1; i <= parseInt(trm); i++) {
                for (j = 1; j <= 12; j++) {
                    totint_amt = totint_amt + (yrly_bal * parseFloat(rate) / (100 * 12));
                    yrly_bal = yrly_bal - (FMInst - (yrly_bal * parseFloat(rate) / (100 * 12)));
                }
            }
            totamt = (parseFloat(amt) + totint_amt);
            html = html + "<tr class='simpletext'><td><b>&nbsp;Total Cost</b></td><td align='right'> " + CommaFormattedCurrency12(totamt.toString().substring(0, totamt.toString().indexOf(".") + 3)) + "</td></tr></table>";
            //this will print the first table for the payment schedule
            tabRef8[0].innerHTML = html;
            //set html to null so that we can enter other things for tabRef9	
            html = "<span class='pwd'>* The last remaining balance 0 of last month of the last year will be added to Principal Payment at a time of actual payment</span><table width='100%' align='center' border='1' cellpadding='0' cellspacing='0' bordercolor='#428bca' style='border-collapse:collapse;margin-left:3px;'>";
            html = html + "<tr><td colspan='5' align='center'  valign='middle' height='25' bgcolor='#428bca' class='client-login'><b>Annual Schedule For The Loan Amount " + CommaFormattedCurrency12(amt) + "</b></td></tr>";
            html = html + "<tr align='center' bgcolor='lightyellow' class='selmonth'>";
            html = html + "<td ><br/><b>Payment Year</b></td>";
            html = html + "<td ><br/><b>Principal Payment</b></td>";
            html = html + "<td ><br/><b>Interest Payment</b></td>";
            html = html + "<td ><br/><b>Balance</b></td>";
            html = html + "<td ><br/><b>Monthly Breakdown</b></td></tr>";
            var yrly_bal = parseInt(amt);
            var yrly_remain_bal = yrly_bal;
            for (i = 1; i <= parseInt(trm); i++) {
                var yrlyint_amt = 0;
                var yrly_princ_amt = 0;
                html = html + "<tr class='calc-text'>"
                html = html + "<td align='center' width='10%'>Year " + i + "</td>";
                for (j = 1; j <= 12; j++) {
                    yrlyint_amt = yrlyint_amt + (yrly_bal * parseFloat(rate) / (100 * 12));
                    yrly_princ_amt = yrly_princ_amt + (FMInst - (yrly_bal * parseFloat(rate) / (100 * 12)));
                    yrly_bal = yrly_bal - (FMInst - (yrly_bal * parseFloat(rate) / (100 * 12)));
                }
                html = html + "<td align='center' width='25%'>" + CommaFormattedCurrency12(yrly_princ_amt.toString().substring(0, yrly_princ_amt.toString().indexOf(".") + 3)) + "</td>";
                html = html + "<td align='center' width='25%'>" + CommaFormattedCurrency12(yrlyint_amt.toString().substring(0, yrlyint_amt.toString().indexOf(".") + 3)) + "</td>";
                if (yrly_bal < 10) {
                    yrly_bal = 0;
                }
                html = html + "<td align='center' width='25%'>" + CommaFormattedCurrency12(yrly_bal.toString().substring(0, yrly_bal.toString().indexOf(".") + 3)) + ((yrly_bal == 0) ? "<span class='pwd'>*</span>" : '') + "</td>";
                html = html + "<td align='center' width='15%'><b><a href='#view' onclick=\"MonthlyView(" + yrly_remain_bal + "," + rate + "," + FMInst + ")\">View<a></b></td></tr>";
                yrly_remain_bal = yrly_remain_bal - yrly_princ_amt;
            }
            html = html + "</table>"
            tabRef9[0].innerHTML = "<p></p>";
            tabRef10[0].innerHTML = html;
            //MonthlyInterest=(parseInt(amt)*(parseFloat(rate)/100)*parseInt(trm))
            //abc[0].innerHTML="<B><span class=\"selmonth\">"+"Calculation Says...".blink()+"</span><br/><br/><span class=\"simpletext\">&nbsp&nbsp&nbsp&nbspYour monthly payment for the loan amount of $"+amt+" at "+rate+"% <br/>for the term of " + trm + " Years is $"+ FMInst.toString().substring(0,FMInst.toString().indexOf(".")+5) +"</span></B><br/>";
            return false;
        }

        function MonthlyView(bal, rate, FIXInst) {
            //alert(bal+":"+rate);	
            var tabRef9 = document.getElementById("calctab").rows[9].cells;
            var html = "";
            rembal = bal; //contains remaining balance
            mi = 0; //contains monthly interest
            prpay = 0; //contains Principal Payment for the month	
            //tabRef9[0].innerHTML="<div id=\"topbar\"><table align='center' border='1' width='100%' cellpadding='0' cellspacing='0'><tr><td align='right'><a href=\"\" onClick=\"closebar(); return false\"><img src=\"close.gif\" border=\"0\" /></a></td></tr>";
            //<a href=\"\" onClick=\"closebar(); return false\"><img src=\"images\\close.png\" border=\"0\" /></a>
            html = "<div style='overflow: auto'  id=\"topbar\" ><table align='center' border='1' bordercolor='#000000' width='100%' cellpadding='0' cellspacing='0' ><tr><td colspan='5'><table width='100%'><tr><td align='right' colspan='5'></td></tr>";
            html = html + "<tr><td colspan='5' align='center'  valign='middle' height='25' bgcolor='#428bca' class='client-login' ><h3>View of <b class='client-login'> Monthly Schedule </b> for the Selected Year</h3><p></td></tr></table></td></tr>";
            html = html + "<tr align='center' bgcolor='lightyellow' class='selmonth'><td align='center' width='65'>Payment Month</td><td width='127' align='center'>Monthly FIX Payment</td><td align='center' width='122'>Principal Payment</td><td align='center' width='127'>Interest Payment</td><td align='center' width='129'>Balance</td></tr>"
            for (i = 1; i <= 12; i++) {
                mi = parseFloat(rembal) * (parseFloat(rate) / (100 * 12));
                prpay = parseFloat(FIXInst) - parseFloat(mi);
                rembal = parseFloat(rembal) - parseFloat(prpay);
                var strmi = mi.toString().substring(0, mi.toString().indexOf(".") + 3);
                var strprpay = prpay.toString().substring(0, prpay.toString().indexOf(".") + 3);
                if (rembal < 10) {
                    rembal = 0;
                }
                var strrembal = rembal.toString().substring(0, rembal.toString().indexOf(".") + 3);
                var strFIXInst = FIXInst.toString().substring(0, FIXInst.toString().indexOf(".") + 3);
                html = html + "<tr class='calc-text'><b><td align='center' width='65'>Month " + i + "</td><td width='127' align='center'>" + CommaFormattedCurrency12(strFIXInst) + "</td><td align='center' width='122'>" + CommaFormattedCurrency12(strprpay) + "</td><td align='center' width='127'>" + CommaFormattedCurrency12(strmi) + "</td><td align='center' width='129'>" + CommaFormattedCurrency12(strrembal) + ((strrembal == 0) ? "<span class='pwd'>*</span>" : '') + "</td></tr>";
                //html=html+<tr><td><p>"+bal+":"+rate+"<br/></td></tr>";
            }
            html = html + "</table></div>";
            tabRef9[0].innerHTML = html;
            staticbar();
        }

        function CommaFormattedCurrency12(amount) {
            var delimiter = ","; // replace comma if desired
            if (amount.indexOf(".") < 0) {
                amount = amount + ".";
            }
            var a = amount.split('.', 2)
            var d = a[1];
            var i = parseInt(a[0]);
            if (isNaN(i)) {
                return '';
            }
            var minus = '';
            if (i < 0) {
                minus = '-';
            }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }
            if (n.length > 0) {
                a.unshift(n);
            }
            n = a.join(delimiter);
            if (d.length < 1) {
                amount = n;
            } else {
                amount = n + '.' + d;
            }
            amount = minus + amount;
            return amount;
        }

    </script>
    <script type="text/javascript">
        function parseTime(s) {
            var part = s.match(/(\d+):(\d+)(?: )?(am|pm)?/i);
            var hh = parseInt(part[1], 10);
            var mm = parseInt(part[2], 10);
            var ap = part[3] ? part[3].toUpperCase() : null;
            if (ap === "AM") {
                if (hh == 12) {
                    hh = 0;
                }
            }
            if (ap === "PM") {
                if (hh != 12) {
                    hh += 12;
                }
            }
            return {hh: hh, mm: mm};
        }

        function CountTimeSheetSchedule() {
            if ($('#timeIn').val() == "") {
                $('#timeIn').css("border", "2px solid red");
                return;
            }
            if ($('#timeOut').val() == "") {
                $('#timeOut').css("border", "2px solid red");
                return;
            }
            var now = "04/09/2013 " + $('#timeOut').val();
            var then = "04/09/2013 " + $('#timeIn').val();
            total = moment.utc(moment(now, "DD/MM/YYYY HH:mm AA").diff(moment(then, "DD/MM/YYYY HH:mm AA"))).format("HH:mm");
            $("#timsheetcalc").show();
            $("#timeperoidTable").after("<tr><td>" + $('#timeIn').val() + "</td><td>" + $('#timeOut').val() + "</td><td>" + total + "</td></tr>");
        }

        function Countmonthlypayemnet() {
            if ($("#loanAmount").val() == "") {
                $("#loanAmount").css("border", "2px solid red");
                return false;
            }
            if ($("#rateofIntrest").val() == "") {
                $("#rateofIntrest").css("border", "2px solid red");
                return false;
            }
            if ($("#term").val() == "") {
                $("#term").css("border", "2px solid red");
                return false;
            }
            var M;

            var s1 = $("#loanAmount").val();

            var s2 = s1.replace(",", "");

            var P = parseInt(s2);
            var I = ($("#rateofIntrest").val()) / 100 / 12;
            var N = $("#term").val() * 12; //number of payments months
            M = monthlyPayment(P, N, I);
            if (isNaN(M)) {
                M = 0;
            }
            console.log(M);
            $("#monthlypaymentcalc").show();
            var num = Math.round(M * 100) / 100;
            var no = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $("#monthlyPaymentCount").html(no);
        }

        function monthlyPayment(p, n, i) {
            return p * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1);
        }

        function CountpayemnetSchedule() {
            if ($("#paymentloanAmount").val() == "") {
                $("#paymentloanAmount").css("border", "1px solid red");
                return;
            }
            if ($("#paymentrateofIntrest").val() == "") {
                $("#paymentrateofIntrest").css("border", "1px solid red");
                return;
            }
            if ($("#paymentterm").val() == "") {
                $("#paymentterm").css("border", "1px solid red");
                return;
            }
            var M;
            var s1 = $("#paymentloanAmount").val();
            var s2 = s1.replace(",", "");
            var P = parseInt(s2);
            var I = $("#paymentrateofIntrest").val() / 100 / 12; //monthly interest rate
            var N = $("#paymentterm").val() * 12; //number of payments months
            M = monthlyPayment(P, N, I);
            $("#resultPayment").show();
            var monthcount = Math.round(M * 100) / 100;
            $("#monthcount").html("<span class='pull-left'>Fix Mothly Payment</span><span  class='pull-right'>" + monthcount + "</span>");
            $("#ratecount").html("<span class='pull-left'>Rate of Interest</span><span  class='pull-right'>" + $("#paymentrateofIntrest").val() + "</span>");
            $("#termscount").html("<span class='pull-left'>Term(years)</span><span  class='pull-right'>" + $("#paymentterm").val() + "</span>");
            var cost = ($("#paymentterm").val() * 12) * monthcount;
            $("#Costcount").html("<span class='pull-left'>Total Cost</span><span  class='pull-right'>" + cost + "</span>");
            //$("#paymentCountDisplay").html("Total Cost"+ $("#term").val()).show();
            createTable(monthcount);
        }

        function createTable(monthcount) {
            var terms = $("#paymentterm").val();
            var s1 = $("#paymentloanAmount").val();
            var s2 = s1.replace(",", "");
            var P = parseInt(s2);
            var I = $("#paymentrateofIntrest").val();
            var totalterms = terms * 12;
            var i, principal = 0, intrest = 0, totalblance = 0;
            j = 1;
            intrest = (terms / I) / totalterms * P;
            principal = monthcount - intrest;
            totalblance = P - principal;
            $(".displayValue").html("");

            for (i = 1; i <= totalterms; i++) {
                if (i % 12 == 1) {
                    $("#monthCountdisplay").before("<tr class='displayValue'><th colspan='5' style='text-align:center;font-size:20px'>Year " + j + "</th></tr>");
                    j++;
                }
                $("#monthCountdisplay").before("<tr class='displayValue'><td> Month " + i + "</td><td>$ " + monthcount + "</td><td> $ " + Math.round(intrest * 100) / 100 + "</td><td> $ " + Math.round(principal * 100) / 100 + "</td><td>$ " + Math.round(totalblance * 100) / 100 + "</td></tr>");
                intrest = (terms / I) / totalterms * totalblance;
                principal = monthcount - intrest;
                totalblance = totalblance - principal;
                if (totalblance < 0) {
                    totalblance = 0;
                }
            }
        }

        $(document).ready(function () {
            $("#loanAmount,#rateofIntrest,#term").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $("#monthly").trigger("click");
        });
        $("#monthly").click(function () {
            document.getElementById("monthly").src = "public/frontcss/images/calculator1Hover.png";
            document.getElementById("payment").src = "public/frontcss/images/calculator2.png";
            document.getElementById("timesheet").src = "public/frontcss/images/calculator3.png";
            document.getElementById("timesheet1").src = "public/frontcss/images/Aurto.png";
        });
        $("#payment").click(function () {
            document.getElementById("monthly").src = "public/frontcss/images/calculator1.png";
            document.getElementById("payment").src = "public/frontcss/images/calculator2Hover.png";
            document.getElementById("timesheet").src = "public/frontcss/images/calculator3.png";
            document.getElementById("timesheet1").src = "public/frontcss/images/Aurto.png";
        });
        $("#timesheet").click(function () {
            document.getElementById("monthly").src = "public/frontcss/images/calculator1.png";
            document.getElementById("payment").src = "public/frontcss/images/calculator2.png";
            document.getElementById("timesheet").src = "public/frontcss/images/calculator3Hover.png";
            document.getElementById("timesheet1").src = "public/frontcss/images/Aurto.png";
        });
        $("#timesheet1").click(function () {
            document.getElementById("monthly").src = "public/frontcss/images/calculator1.png";
            document.getElementById("payment").src = "public/frontcss/images/calculator2.png";
            document.getElementById("timesheet").src = "public/frontcss/images/calculator3Hover.png";
            document.getElementById("timesheet1").src = "public/frontcss/images/Aurto.png";
        });

        function resetmonthlycalc() {
            $("#loanAmount").val(' ');
            $("#loanAmount1").val(' ');
            $("#rateofIntrest").val('');
            $("#rateofIntrest1").val('');
            $("#term").val('');
            $("#monthlypaymentcalc").hide();
        }

        function resetpaymentsedual() {
            $("#paymentloanAmount").val("");
            $("#paymentrateofIntrest").val("");
            $("#paymentterm").val("");
            $("#resultPayment").hide();
        }
    </script>

    <!---<script>
    $(document).ready(function(){
      $('input#paymentloanAmount').keyup(function(event){
          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40){
              event.preventDefault();
          }
          var $this = $(this);
          var num = $this.val().replace(/,/gi, "").split("").reverse().join("");

          var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

          console.log(num2);


          // the following line has been simplified. Revision history contains original.
          $this.val(num2);
      });



     $('input#loanAmount').keyup(function(event){
          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40){
              event.preventDefault();
          }
          var $this = $(this);
          var num = $this.val().replace(/,/gi, "").split("").reverse().join("");

          var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

          console.log(num2);


          // the following line has been simplified. Revision history contains original.
          $this.val(num2);
      });
    });

    function RemoveRougeChar(convertString){


        if(convertString.substring(0,1) == ","){

            return convertString.substring(1, convertString.length)

        }
        return convertString;

    }

    </script>
    <script>
    $(document).ready(function(){
    blur = 0;
        $("#rateofIntrest").blur(function(event){

            var x = document.getElementById("rateofIntrest");
             x.value = x.value * 100;
    $('#cc').addClass('cc');
        });
    });
    $(document).ready(function(){
        $("#paymentrateofIntrest").blur(function(){
            var x = document.getElementById("paymentrateofIntrest");
        x.value = x.value * 100.00;
    $('#c1').addClass('cc');
        });
    });
    </script>-->

    <script type="text/javascript">
        $(document).ready(function () {

            $("#loanAmount1").change(
                function () {

                    //alert("changing");
                    var num = parseFloat($("#loanAmount1").val());
                    $("#loanAmount1").val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $("#loanAmount").val(num.toFixed(2));
                }
            );
            $("#rateofIntrest1").change(
                function () {
                    // alert("changing");
                    var num1 = parseFloat($("#rateofIntrest1").val());//alert(num1);
                    $("#rateofIntrest1").val(num1.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                    $("#rateofIntrest").val(num1.toFixed(2));
                }
            );
            //$(':input').setMask();
            //$('input[data-mask]').each(function () {
            //    $(this).setMask($(this).data('mask'));
            //});

        });
        $(document).ready(function () {
            var counter = 0;

            $("#addrow").on("click", function () {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="date' + counter + '" placeholder="Date"/></div></td>';
                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="perticulars' + counter + '" placeholder="Perticulars"/></div></td>';
                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="startmile' + counter + '" placeholder="Enter in Start Mile"/></div></td>';
                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="endmiled' + counter + '" placeholder="End Mile"/></div></td>';
                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="netmile' + counter + '" placeholder="Net Mile "/></div></td>';
                cols += '<td><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="notes' + counter + '" placeholder="Notes "/></div></td>';

                cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
                newRow.append(cols);
                $("table.order-list").append(newRow);
                counter++;
            });


            $("table.order-list").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
            });


        });


        function calculateRow(row) {
            var price = +row.find('input[name^="price"]').val();

        }

        function calculateGrandTotal() {
            var grandTotal = 0;
            $("table.order-list").find('input[name^="price"]').each(function () {
                grandTotal += +$(this).val();
            });
            $("#grandtotal").text(grandTotal.toFixed(2));
        }
    </script>



    <script src="https://www.aspsnippets.com/demos/scripts/table2excel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "Table.xls"
                });
            });
        });
    </script>


@endsection()