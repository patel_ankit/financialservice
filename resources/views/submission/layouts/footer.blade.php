<style>
    .logo-footer {
        margin-top: 7px;
    }
</style>
<footer>
    <div class="container-fluid show1">
        <div class="col-md-3 col-sm-3 col-xs-12">

        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <a href="#"><span class="first-letter">F</span>inancial
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a> &nbsp; All rights reserved. </p></div>
        <div class="col-md-3 col-sm-3 col-xs-12 ">
            <!--<img src="public/frontcss/images/design-by.png" class="design-by-logo" alt="design-by"/>--><a href="http://vimbo.com/" target="_blank"><img src="http://financialservicecenter.net/public/frontcss/images/footer-logo-right.png" alt="logo" class="img-responsive pull-right logo-footer"> </a>
        </div>
    </div>
    <div class="container-fluid hide1">
        <div class=" col-xs-12">
            <p><strong>Copyright © {{date('Y')}}-{{date('Y', strtotime('+1 years'))}} &nbsp; <br><br><a href="#"><span class="first-letter">F</span>inancial
                        <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</strong></a><br><br> All rights reserved. </p></div>
        <div style="background: #fff;width: 100%;
    display: inline-block;">
            <center><img src="{{url('public/dashboard/images/footer-logo.png')}}" alt="" class="img-responsive"></center>
        </div>
    </div>
</footer>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-success').fadeIn('slow', function () {
            $('.alert-success').delay(5000).fadeOut();
        });
    });
</script>
<script>
    //When the page has loaded.
    $(document).ready(function () {
        $('.alert-danger').fadeIn('slow', function () {
            $('.alert-danger').delay(5000).fadeOut();
        });
    });
</script>
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sampleTable3').DataTable({

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i> &nbsp; Copy',
                    //titleAttr: 'Copy',
                    title: $('h1').text(),
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                    // titleAttr: 'Excel',
                    title: $('h1').text(),
                },
                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                    // titleAttr: 'CSV',
                    title: $('h1').text(),
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                    //   titleAttr: 'PDF',
                    title: $('h1').text(),
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp; Print',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true
                },
            ]
        });
    });
</script>
<script type="text/javascript">
    //$('.alert-success').fadeIn('slow').delay(4000).fadeOut('slow');
    $(document).ready(function () {
        /***phone number format***/
        $(".phone").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
            var curchr = this.value.length;
            var curval = $(this).val();
            if (curchr == 3 && curval.indexOf("(") <= -1) {
                $(this).val("(" + curval + ")" + " ");
            } else if (curchr == 4 && curval.indexOf("(") > -1) {
                $(this).val(curval + ")-");
            } else if (curchr == 5 && curval.indexOf(")") > -1) {
                $(this).val(curval + "-");
            } else if (curchr == 9) {
                $(this).val(curval + "-");
                $(this).attr('maxlength', '14');
            }
        });
    });
    var date = $('#ext2_1').val();
    $('#mobiletype_1').on('change', function () {

        if (this.value == 'Home') {
            document.getElementById('ext2_1').removeAttribute('readonly');
            $('#ext2_1').val();
        } else {
            document.getElementById('ext2_1').readOnly = true;
            $('#ext2_1').val('');
        }
    })

    var date = $('#businessext').val();
    $('#businesstype').on('change', function () {

        if (this.value == 'Office') {
            document.getElementById('businessext').removeAttribute('readonly');
            $('#businessext').val();
        } else if (this.value == 'Business') {
            document.getElementById('businessext').removeAttribute('readonly');
            $('#businessext').val();
        } else {
            document.getElementById('businessext').readOnly = true;
            $('#businessext').val('');
        }
    })
    var date = $('#ext2_2').val();
    $('#mobiletype_2').on('change', function () {

        if (this.value == 'Home') {
            document.getElementById('ext2_2').removeAttribute('readonly');
            $('#ext2_2').val();
        } else {
            document.getElementById('ext2_2').readOnly = true;
            $('#ext2_2').val('');
        }
    })

    function FillBilling(f) {
        if (f.billingtoo.checked == true) {
            f.mailing_address.value = f.address.value;
            f.mailing_address1.value = f.address1.value;
            f.mailing_city.value = f.city.value;
            f.mailing_state.value = f.stateId.value;
            f.mailing_zip.value = f.zip.value;
        }

    }
</script>
<script>
    $(document).ready(function () {
        $(".textonly").keypress(function (event) {
            var inputValue = event.charCode;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
                event.preventDefault();
            }
        });
    });
    $(document).ready(function () {
        //called when key is pressed in textbox
        $(".zip").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });

</script>