<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
    @include('submission.layouts.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('submission.layouts.header')
    @section('main-content')
    @show
    @include('submission.layouts.leftsidebar')
    @include('submission.layouts.footer')
</div>
</body>
</html>