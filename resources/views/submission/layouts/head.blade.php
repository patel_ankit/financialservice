<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>FSC - Submission</title>
<link rel="shortcut icon" href="{{URL::asset('public/dashboard/images/favicon.png')}}" type="image/x-icon">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/skins/_all-skins.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/morris.js/morris.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/custom.css')}}">
<script src="{{URL::asset('public/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/morris.js/morris.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/moment/min/moment.min.js')}}"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{URL::asset('public/adminlte/dist/js/adminlte.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/dist/js/pages/dashboard.js')}}"></script>
<script src="{{URL::asset('public/adminlte/dist/js/demo.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')}}"></script>
<script>
    $(function () {
        CKEDITOR.replace('editor1')
        $('#editor1').wysihtml5()
    })
    $(function () {
        CKEDITOR.replace('editor2')
        $('#editor2').wysihtml5()
    })
</script>
<style>
    .modal-dialog {
        width: 60%;
        margin: 30px auto;
    }

    .sw-main .sw-container {
        z-index: 9
    }

    footer p {
        text-align: center;
        margin-top: 20px;
        color: #054b94;
        font-size: 17px;
    }

    .tess {
        text-align: left;
        position: absolute;
        font-size: 11px;
        margin: 22px 16px 0 0;
        right: -78px;
        width: 100%;
    }

    .fixed .main-header, .fixed .main-sidebar, .fixed .left-side {
        overflow-y: inherit;
    }

    #smartwizard .nav-pills {
        background-color: #828282;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location;
        $('ul.sidebar-menu a').filter(function () {
            return this.href == url;
        }).parent().siblings().removeClass('active').end().addClass('active');
        $('ul.treeview-menu a').filter(function () {
            return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
    });
</script>
@guest

@endguest