<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="active"><a href="{{url('submission/dashboard')}}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{ route('submission.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out sidebar-icon"></i><span>Logout</span></a></li>
            <form id="logout-form" action="{{ route('submission.logout') }}" method="POST" style="display: none;">{{ csrf_field() }} </form>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>