<?php
$ccountry = "America/New_York";
date_default_timezone_set($ccountry);
$time = date_default_timezone_get();
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js"></script>
<script type="text/javascript">
    $(function () {
        setInterval(function () {
            var divUtc = $('#divUTC');
            var divLocal = $('#divLocal');
            //put UTC time into divUTC
            divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss a'));
            var localTime = moment.utc(divUtc.text()).toDate();
            //localTime = moment(localTime).format('YYYY-MM-DD HH:mm:ss a');
            //divLocal.text(localTime);
            //$('#divThai').text(moment.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss a'));

            $('#divUsa').text(moment.tz('{{$time}}').format('hh:mm:ss a'));
        }, 1000);
    });
</script>
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #1d7dff;
        border: 1px solid #000;
        border-radius: 4px;
        cursor: default;
        float: left;
        margin-right: 5px;
        margin-top: 5px;
        padding: 0 5px;
        font-size: 18px;
    }

    .top_company.top-company-name-bg .top_date, .top_time, .top_day {
        width: 100%;
    }

    .main-header .logo .logo-lg {
        margin: 13px auto;
    }

    .main-sidebar {
        padding-top: 114px;
    }

    .top_name {
        margin-top: 38px;
    }

    .top_company {
        float: left;
        margin: 34px 0 0 0;
    }

    .top_company.top-company-name-bg {
        background: #fff;
        border-radius: 5px;
        margin: 10px 0;
        padding: 0;
    }

    .top_company.top-company-name-bg h3 {
        margin: 0;
        color: #fff;
        background: #12186b;
        padding: 10px;
        border-radius: 5px 5px 0 0px;
        font-size: 18px;
        min-width: 230px;
        text-align: center;
    }

    .top_company.top-company-name-bg p {
        color: #000;
        padding-left: 0;
        padding: 10px;
        text-align: center;
        display: block;
        font-size: 18px;
    }

    .top_company.top-company-name-bg span {
        color: #000;
        font-size: 14px;
    }

    .top_company.top-company-name-bg i {
        color: #000;
    }

    .top_company.top-company-name-bg .top_date, .top_time, .top_day {
        padding: 3px 0;
    }

    .skin-blue .main-header .navbar .sidebar-toggle {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 999;
    }

    .topbar {
        margin: 16px 0 16px 30px;
    }

    .topbar h1 {
        text-align: center;
        font-size: 28px;
        font-family: 'Proze Libre', serif;
        letter-spacing: 1.5px;
        font-weight: 800;
        text-transform: uppercase;
        margin: 0;
        color: #222 !important;
    }

    .topbar {
        box-shadow: 3px 3px 5px #191c1f91;
        padding: 12px 20px;
        background-color: #fff;
        border-radius: 0;
        float: left;
        margin: 16px 0 0 20px;
    }

    @media (max-width: 1100px) {
        .top_company.top-company-name-bg h3 {
            min-width: auto;
        }
    }

    @media (max-width: 900px) {
        .top_company.top-company-name-bg {
            width: 100%;
        }

        .topbar {
            float: none;
            margin: 15px auto;
            width: 81%;
        }
    }

    @media (max-width: 767px) {
        .content {
            margin-top: 0px !important;
        }

        .portlet.box.blue {
            margin-top: 0px !important;
            top: 30px;
        }

        .top_company.top-company-name-bg {
            margin: 10px auto;
            float: none !important;
            width: 230px;
            display: table;
        }

        .main-header .navbar-custom-menu {
            margin-top: 0 !important;
            left: 0 !important;
        }

        .skin-blue .main-header .navbar .sidebar-toggle {
            margin-top: 20px;
            position: absolute;
            z-index: 999;
            font-size: 20px;
        }

        .main-sidebar {
            top: 270px !important;
            padding-top: 0 !important;
            z-index: 9999 !important;
        }

        .main-sidebar .sidebar {
            top: 0px;
        }

        .logo .img-responsive {
            width: auto !important;
        }
    }
</style>
<?php  if(isset($_REQUEST['radio'])){
} else{ ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myModal").modal('show');
    });
</script>
<?php }?>
<!-- Modal -->
<form action="/submission/dashboard" method="get">
    {{csrf_field()}}
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Company For Reporting</h4>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Select</th>
                            <th scope="col">Client ID</th>
                            <th scope="col">Business Legal Name</th>
                            <th scope="col">Business Name</th>
                            <th scope="col">City</th>
                            <th scope="col">State</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($com as $com1)
                            <tr>
                                <th scope="row"><input type="radio" name="radio" value="{{$com1->id}}"></th>
                                <td>{{$com1->filename}}</td>
                                <td>{{$com1->company_name}}</td>
                                <td>{{$com1->business_name}}</td>
                                <td>{{$com1->city}}</td>
                                <td>{{$com1->stateId}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-default">Ok</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
</form>
</div>
</div>
<header class="main-header">
    <!-- Logo -->
    <a href="{{url('submission/dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
      <img style="width:44px" src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt=""/>
      </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
      <img src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive"/>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
    @guest @else @endguest
    <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="">
            @guest
            @else
                <div class="col-md-6">
                    <div class="topbar"><h1><span class="first-letter">F</span>inancial <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</h1></div>
                </div>
                <div class="col-md-6 col-xs-12 pull-right">
                    <div class="navbar-right-menu">
                        <div class="col-md-12 col-lg-7 col-xs-12">
                            <div class="top_company top-company-name-bg">
                                <h3>Submission</h3>
                                <p> {{ Auth::user()->firstname }} {{ Auth::user()->middlename }} {{ Auth::user()->lastname }}</p>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-5 col-xs-12" style="padding:0px;">
                            <div class="top_date_section top_company top-company-name-bg pull-right" style="padding: 3px 15px;">
                                <div class="top_date"><i class="fa fa-calendar"></i><span> {{ date('F-d-Y')}}</span></div>
                                <div class="top_day"><i class="fa fa-calendar-o"></i><span> {{ date('l')}}</span></div>
                                <div class="top_time"><i class="fa fa-clock-o"></i> <span id="divUsa"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endguest
        </div>
    </nav>
</header>