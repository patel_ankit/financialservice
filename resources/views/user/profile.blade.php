@extends('layouts.app')
@section('main-content')
    <style>.panel-primary > .panel-heading {
            color: #fff;
            background-color: transparent;
            border-color: transparent;
        }

        .nav-tabs > li {
            float: left;
            width: 166px;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }
    </style>

    <div class="content-wrapper">
        <div class="page-title">
            <h1>Profile</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if ( session()->has('success') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                        @endif
                        @if ( session()->has('error') )
                            <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                        @endif
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active"><a href="#tab1primary" data-toggle="tab">Basic Information</a></li>
                                    <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Business Information</a></li>
                                    <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Company Information</a></li>
                                    <li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Contact Information</a></li>
                                    <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Licence Information</a></li>
                                    <li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Information</a></li>
                                </ul>
                            </div>
                            <form method="post" action="{{ route('profile.update',Auth::user()->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                {{csrf_field()}}{{method_field('PATCH')}}
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">


                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">File NO : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="fileno" name="fileno" value="GA-00{{$common->cid}}" placeholder="" readonly>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Status : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="status" id="status" class="form-control fsc-input">
                                                            @if($common->status==0)

                                                                <option value="{{$common->status}}">Pending</option>
                                                                <option value="{{$common->status}}">Active</option>

                                                            @else
                                                                <option value="{{$common->status}}">Active</option>
                                                            @endif
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('legalname') ? ' has-error' : '' }}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Legal name : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="legalname" id="legalname" value="{{$common->legalname}}">
                                                        @if ($errors->has('legalname'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('legalname') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('dbaname') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">DBA Name : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="dbaname" id="dbaname" value="{{$common->dbaname}}">
                                                        @if($errors->has('dbaname'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('dbaname') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Address : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="address" id="address" value="{{$common->address}}">
                                                        @if($errors->has('address'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('address') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} {{ $errors->has('countryId') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City/State/Zip : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$common->city}}">
                                                                @if($errors->has('city'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
                                                                @endif
                                                            </div>

                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="stateId" id="stateId" class="form-control fsc-input">
                                                                        <option value="{{$common->stateId}}">{{$common->stateId}}</option>
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    @if($errors->has('stateId'))
                                                                        <span class="help-block">
											<strong>{{ $errors->first('stateId') }}</strong>
										</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="zip" name="zip" value="{{$common->zip}}" placeholder="Zip">
                                                                @if($errors->has('zip'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('zip') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Country :</label>
                                                    </div>

                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown">
                                                                    <select name="countryId" id="countryId" class="form-control fsc-input">
                                                                        <option value="{{$common->countryId}}">{{$common->countryId}}</option>
                                                                        <option value='USA'>USA</option>
                                                                    </select>
                                                                    @if($errors->has('countryId'))
                                                                        <span class="help-block">
											<strong>{{ $errors->first('countryId') }}</strong>
										</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Mailing Address: </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="mailing_address" name="mailing_address" value="{{$common->mailing_address}}" placeholder="Mailing Address">
                                                        @if($errors->has('mailing_address'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('mailing_address') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('mailing_city') ? 'has-error' : ''}} {{ $errors->has('mailing_state') ? 'has-error' : ''}}{{ $errors->has('mailing_zip') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City/State/Zip : </label>
                                                    </div>


                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                        <div class="row">


                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="mailing_city" name="mailing_city" placeholder="City" value="{{$common->mailing_city}}">
                                                                @if($errors->has('mailing_city'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('mailing_city') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="mailing_state" id="mailing_state" class="form-control fsc-input">
                                                                        @if($common->mailing_state==NULL)
                                                                            <option value="">---Select State---</option>
                                                                        @else
                                                                            <option value="{{$common->mailing_state}}">{{$common->mailing_state}}</option>
                                                                        @endif
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>
                                                                    @if($errors->has('mailing_state'))
                                                                        <span class="help-block">
											<strong>{{ $errors->first('mailing_state') }}</strong>
										</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="mailing_zip" name="mailing_zip" value="{{$common->mailing_zip}}" placeholder="Zip">
                                                                @if($errors->has('mailing_zip'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('mailing_zip') }}</strong>
										</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('email') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Email : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="abc@abc.com" value="{{$common->email}}">
                                                        @if($errors->has('email'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_no') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Telephone # : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="business_no" value="{{$common->business_no}}" name="business_no" placeholder="Business Telephone" onkeypress="PutNo()">
                                                        @if($errors->has('business_no'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('business_no') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('business_fax') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Fax # : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" value="{{$common->business_fax}}" id="business_fax" name="business_fax" placeholder="Business Fax">
                                                        @if($errors->has('business_fax'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('business_fax') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('website') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Website : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="website" value="{{$common->website}}" name="website" placeholder="Website address">
                                                        @if($errors->has('website'))
                                                            <span class="help-block">
											<strong>{{ $errors->first('website') }}</strong>
										</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            <!--<select style="visibility:hidden;" name="user_type" id="user_type" class="form-control fsc-input category1">
									<option value='{{$common->user_type}}' selected>{{$common->user_type}}</option>	                                  
                                                </select>-->
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="tab2primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Type of Business : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" id="user_type" name="user_type" style="margin-left:10px; " value="{{$common->user_type}}" placeholder="" readonly>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Brand : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <select name="business_id" id="business_id" class="form-control fsc-input">
                                                            <option value="{{$common->business_id}}">{{$common->business_brand_name}}</option>
                                                        </select></div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('dbaname') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Store Name : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business_store_name" id="business_store_name" value="{{$common->business_store_name}}">

                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Address : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business_address" id="business_address" value="{{$common->business_address}}">

                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('zip') ? 'has-error' : ''}} {{ $errors->has('stateId') ? 'has-error' : ''}} {{ $errors->has('city') ? 'has-error' : ''}} {{ $errors->has('countryId') ? 'has-error' : ''}}" style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">City/State/Zip : </label>
                                                    </div>
                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="business_city" name="business_city" placeholder="business_city" value="{{$common->business_city}}">
                                                                @if($errors->has('city'))
                                                                    <span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
                                                                @endif
                                                            </div>

                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <div class="dropdown" style="margin-top: 1%;">
                                                                    <select name="business_state" id="business_state" class="form-control fsc-input">
                                                                        @if(empty($common->business_state))
                                                                            <option value="">---Select State---</option>
                                                                        @else
                                                                            <option value="{{$common->business_state}}">{{$common->business_state}}</option>
                                                                        @endif
                                                                        <option value='AK'>AK</option>
                                                                        <option value='AS'>AS</option>
                                                                        <option value='AZ'>AZ</option>
                                                                        <option value='AR'>AR</option>
                                                                        <option value='CA'>CA</option>
                                                                        <option value='CO'>CO</option>
                                                                        <option value='CT'>CT</option>
                                                                        <option value='DE'>DE</option>
                                                                        <option value='DC'>DC</option>
                                                                        <option value='FM'>FM</option>
                                                                        <option value='FL'>FL</option>
                                                                        <option value='GA'>GA</option>
                                                                        <option value='GU'>GU</option>
                                                                        <option value='HI'>HI</option>
                                                                        <option value='ID'>ID</option>
                                                                        <option value='IL'>IL</option>
                                                                        <option value='IN'>IN</option>
                                                                        <option value='IA'>IA</option>
                                                                        <option value='KS'>KS</option>
                                                                        <option value='KY'>KY</option>
                                                                        <option value='LA'>LA</option>
                                                                        <option value='ME'>ME</option>
                                                                        <option value='MH'>MH</option>
                                                                        <option value='MD'>MD</option>
                                                                        <option value='MA'>MA</option>
                                                                        <option value='MI'>MI</option>
                                                                        <option value='MN'>MN</option>
                                                                        <option value='MS'>MS</option>
                                                                        <option value='MO'>MO</option>
                                                                        <option value='MT'>MT</option>
                                                                        <option value='NE'>NE</option>
                                                                        <option value='NV'>NV</option>
                                                                        <option value='NH'>NH</option>
                                                                        <option value='NJ'>NJ</option>
                                                                        <option value='NM'>NM</option>
                                                                        <option value='NY'>NY</option>
                                                                        <option value='NC'>NC</option>
                                                                        <option value='ND'>ND</option>
                                                                        <option value='MP'>MP</option>
                                                                        <option value='OH'>OH</option>
                                                                        <option value='OK'>OK</option>
                                                                        <option value='OR'>OR</option>
                                                                        <option value='PW'>PW</option>
                                                                        <option value='PA'>PA</option>
                                                                        <option value='PR'>PR</option>
                                                                        <option value='RI'>RI</option>
                                                                        <option value='SC'>SC</option>
                                                                        <option value='SD'>SD</option>
                                                                        <option value='TN'>TN</option>
                                                                        <option value='TX'>TX</option>
                                                                        <option value='UT'>UT</option>
                                                                        <option value='VT'>VT</option>
                                                                        <option value='VI'>VI</option>
                                                                        <option value='VA'>VA</option>
                                                                        <option value='WA'>WA</option>
                                                                        <option value='WV'>WV</option>
                                                                        <option value='WI'>WI</option>
                                                                        <option value='WY'>WY</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" id="bussiness_zip" name="bussiness_zip" value="{{$common->bussiness_zip}}" placeholder="Zip">

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="tab-pane fade" id="tab3primary">Coming Soon</div>
                                        <div class="tab-pane fade" id="tab4primary">

                                            <div class="pull-right">


                                                <a href="javascript:void(0)" class="addMore pull-right"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Field</a>
                                                @foreach($info as $infor)
                                                    <div class="fieldGroup">

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Contact Person Name : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" value="{{$infor->contact_person_name}}" name="contact_person_name[]" id="contact_person_name" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Telephone No. : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input telephone" name="telephone[]" value="{{$infor->telephone}}" id="telephone">
                                                                <input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="@if(!empty($infor->id)){{$infor->id}} @else {{0}} @endif">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" name="business[]" value="{{$infor->business}}" id="business" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Business Fax : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" value="{{$infor->business_fax}}" id="business_fax1" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Residence : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" name="residence[]" value="{{$infor->residence}}" id="residence" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Cell : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="{{$infor->cell}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Residence Fax : </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="{{$infor->residence_fax}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label">Email: </label>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="{{$infor->email}}">
                                                            </div>
                                                            <div class="pull-right" style="margin-top: 10px;" id="deleteTheProduct">
                                                                <a href="{{ route('co.delete1',[$infor->id,$common->cid]) }}" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

                                                            </div>
                                                        </div>
                                                        <br>
                                                        <br>
                                                    </div>
                                                @endforeach
                                                @if(empty($info1->id))
                                            </div>
                                            <div class="fieldGroup">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Contact Person Name : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="contact_person_name[]" id="contact_person_name" value=""><input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Telephone No. : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input telephone" name="telephone[]" id="telephone" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="business[]" id="business" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Business Fax : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" id="business_fax1" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Residence : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="residence[]" id="residence" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Cell : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Residence Fax : </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                        <label class="fsc-form-label">Email: </label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="">
                                                    </div>

                                                </div>
                                                <br>
                                                <br>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab5primary">Coming Soon</div>
                                        <div class="tab-pane fade" id="tab6primary">
                                            <div class="row">

                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">UserName :</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="" value="{{ Auth::user()->name }}" name="name">
                                                        </div>
                                                    </div>

                                                <!--	<div class="form-group">
							<label class="control-label col-md-3">Email :</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="email" readonly name="email" value="{{ Auth::user()->email }}">								
							</div>
						</div>-->
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 1 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question1" id="question1" class="form-control">
                                                                @if (Auth::user()->question1)
                                                                    <option value="{{ Auth::user()->question1 }}">{{ Auth::user()->question1 }}</option>
                                                                @else
                                                                    <option value="">---Select---</option>
                                                                    @endelse
                                                                @endif
                                                                <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                                                <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                                                <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                                                <option value="In what city were you born?">In what city were you born?</option>
                                                                <option value="What is the name of your first school?">What is the name of your first school?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 2 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question2" id="question2" class="form-control">
                                                                @if (Auth::user()->question2)
                                                                    <option value="{{ Auth::user()->question2 }}">{{ Auth::user()->question2 }}</option>
                                                                @else
                                                                    <option value="">---Select---</option>
                                                                    @endelse
                                                                @endif
                                                                <option value="What is your favorite movie?">What is your favorite movie?</option>
                                                                <option value="What was the make of your first car?">What was the make of your first car?</option>
                                                                <option value="What is your favorite color?">What is your favorite color?</option>
                                                                <option value="What is your father's middle name?">What is your father's middle name?</option>
                                                                <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Question 3 :</label>
                                                        <div class="col-md-8">
                                                            <select name="question3" id="question3" class="form-control">
                                                                @if (Auth::user()->question3)
                                                                    <option value="{{ Auth::user()->question3 }}">{{ Auth::user()->question3 }}</option>
                                                                @else
                                                                    <option value="">---Select---</option>
                                                                    @endelse
                                                                @endif

                                                                <option value="What was your high school mascot?">What was your high school mascot?</option>
                                                                <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                                                <option value="In what year was your father born?">In what year was your father born?</option>
                                                                <option value="What is the name of your favorite childhood friend?">What is the name of your favorite childhood friend?</option>
                                                                <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="col-md-8">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="col-md-8">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 1:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer1" value="{{ Auth::user()->answer1 }}" type="text" id="answer1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 2:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer2" value="{{ Auth::user()->answer2 }}" type="text" id="answer2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Answer 3:</label>
                                                        <div class="col-md-8">
                                                            <input name="answer3" value="{{ Auth::user()->answer3 }}" type="text" id="answer3" class="form-control"/>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" style="display: table; margin: 0 auto;" class="btn btn-primary  fsc-form-submit class=" hvr-shutter-in-horizontal
                                        "">Save</button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- copy of input fields group -->
    <div class="form-group fieldGroupCopy" style="display: none;">
        <div class="input-group">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Contact Person Name : </label>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="contact_person_name[]" id="contact_person_name" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Telephone No. : </label>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input telephone" name="telephone[]" id="telephone" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Business : </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="business[]" id="business" value="">
                </div>
            </div>
            <input type="hidden" class="form-control fsc-input" name="conid[]" id="conid" value="">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Business Fax : </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input business_fax1" name="business_fax1[]" id="business_fax1" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Residence : </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="residence[]" id="residence" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Cell : </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input cell" name="cell[]" id="cell" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Residence Fax : </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input residence_fax" name="residence_fax[]" id="residence_fax" value="">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                    <label class="fsc-form-label">Email: </label>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                    <input type="text" class="form-control fsc-input" name="contemail[]" id="contemail" value="">
                </div>
                <div class="pull-right" style="margin-top: 10px;">
                    <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#business_no").mask("(999) 999-9999");
        $(".ext").mask("999");
        $("#business_fax").mask("(999) 999-9999");
        $(".business_fax1").mask("(999) 999-9999");
        $(".residence_fax").mask("(999) 999-9999");
        $("#mobile_no").mask("(999) 999-9999");
        $(".cell").mask("(999) 999-9999");
        $(".telephone").mask("(999) 999-9999");
        $(".usapfax").mask("(999) 999-9999");
        $("#zip").mask("9999");
        $("#mailing_zip").mask("9999");
        $("#bussiness_zip").mask("9999");
    </script>
    <script>
        $(document).ready(function () {
            //group add limit
            var maxGroup = 3;

            //add more fields group
            $(".addMore").click(function () {
                if ($('body').find('.fieldGroup').length < maxGroup) {
                    var fieldHTML = '<div class="form-group fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maximum ' + maxGroup + ' Persons are allowed.');
                }
            });

            //remove fields group
            $("body").on("click", ".remove", function () {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
    <script>
        function FillBilling(f) {
            if (f.billingtoo.checked == true) {
                f.mailing_address.value = f.address.value;
                f.mailing_address1.value = f.address1.value;
            }
        }
    </script>
    <script>
        function showDiv(elem) {
            if (elem.value == 'Federal') {
                document.getElementById('hidden_div').style.display = "none";
            } else {
                document.getElementById('hidden_div').style.display = "block";
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();
                $.get('{!!URL::to('getcat1')!!}?id=' + id, function (data) {
                    $('#user_type').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#user_type').append('<option value="' + subcatobj.bussiness_name + '">' + subcatobj.bussiness_name + '</option>');
                    })

                });

            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('change', '.category', function () {
                //console.log('htm');
                var id = $(this).val();//alert(id);
                $.get('{!!URL::to('getRequest2')!!}?id=' + id, function (data) {
                    $('#business_cat_id').empty();
                    $.each(data, function (index, subcatobj) {
                        $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                    })

                });

            });
        });
    </script>

@endsection()