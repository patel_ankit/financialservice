<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Adminupload extends Model
{
    protected $fillable = ['upload_name', 'upload'];
}