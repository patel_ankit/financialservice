<?php

namespace App\Model;

use Eloquent;

class Marque extends Eloquent
{
    protected $fillable = ['marque'];
}