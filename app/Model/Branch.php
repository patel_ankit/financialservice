<?php

namespace App\Model;

use Eloquent;

class Branch extends Eloquent
{
    protected $fillable = ['positionid', 'country', 'city', 'branchname'];
}