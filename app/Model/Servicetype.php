<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Servicetype extends Model
{
    protected $fillable = ['service_id', 'servicetype', 'servicename'];
}