<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class taxstate extends Model
{
    protected $fillable = ['county', 'countycode', 'address', 'city', 'state', 'zip', 'website', 'licence_form_number', 'licence_form_name', 'licence_tax_year', 'licence_filling_frequency', 'licence_due_date', 'licence_telephone', 'licence_contact_name', 'licence_secondary_telephone', 'licence_email', 'licence_file', 'licence_website', 'licence_payment_online', 'licence_note', 'personal_filling_frequency', 'property_filling_frequency', 'county_website', 'county_address', 'county_telephone', 'county_city', 'county_state', 'county_zip', 'county_fax', 'personal_form_number', 'personal_form_name', 'personal_due_date', 'personal_website', 'personal_telephone', 'personal_contact_name', 'personal_secondary_telephone', 'personal_email', 'personal_file', 'personal_tax_note', 'property_form_number', 'property_form_name', 'property_due_date', 'property_website', 'property_telephone', 'property_contact_name', 'property_secondary_telephone', 'property_email', 'property_file', 'property_tax_note'];
}