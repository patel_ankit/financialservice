<?php

namespace App\Model;

use Eloquent;

class Relatednames extends Eloquent
{
    protected $fillable = ['relatednames'];
}