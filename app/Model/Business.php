<?php

namespace App\Model;

use Eloquent;

class Business extends Eloquent
{
    protected $fillable = ['bussiness_name', 'bussiness_image_name', 'status', 'link'];

    public function cateogry()
    {
        return $this->hasTo('App\Model\category');
    }
}