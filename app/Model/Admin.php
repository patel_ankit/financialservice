<?php

namespace App\Model;

use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';
    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['formation_work_annualreceipt', 'formation_work_officer', 'name', 'email', 'password', 'fax', 'fname', 'lname', 'city', 'state', 'zip', 'job_title', 'mobile', 'address', 'address1', 'question1', 'question2', 'question3', 'answer1', 'answer2', 'answer3', 'company_name', 'contact_person_name', 'company_email', 'username', 'dba_name', 'telephone', 'website', 'reset_day', 'remaining_day', 'login_day'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }
}