<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{

    protected $fillable = ['licensename'];

    public function price()
    {
        return $this->belongsToMany(Price::class);
    }

}