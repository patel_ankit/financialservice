<?php

namespace App\model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ClientEmployee extends Authenticatable
{
    use Notifiable;

    protected $guard = 'client';
    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'fscemployees';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}