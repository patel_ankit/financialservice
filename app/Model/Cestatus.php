<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cestatus extends Model
{
    protected $table = 'ce_status';
}