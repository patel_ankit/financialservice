<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class admin_shareholder extends Model
{
    protected $fillable = ['agent_fname1', 'agent_mname1', 'agent_lname1', 'agent_position', 'agent_position1', 'agent_per', 'total', 'admin_id'];
}