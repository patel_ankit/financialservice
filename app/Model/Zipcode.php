<?php

namespace App\Model;

use Eloquent;

class Zipcode extends Eloquent
{
    protected $fillable = ['zip'];

}