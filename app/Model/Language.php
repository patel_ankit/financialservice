<?php

namespace App\Model;

use Eloquent;

class Language extends Eloquent
{
    protected $fillable = ['language_name'];

}