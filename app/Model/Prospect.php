<?php

namespace App\Model;

use Eloquent;

class Prospect extends Eloquent
{
    public $table = "prospect_services";
    protected $fillable = ['name', 'parent_id'];
}