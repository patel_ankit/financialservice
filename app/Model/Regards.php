<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Regards extends Model
{
    public $table = "appointment_regards";
}