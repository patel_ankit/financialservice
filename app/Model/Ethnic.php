<?php

namespace App\Model;

use Eloquent;

class Ethnic extends Eloquent
{
    protected $fillable = ['ethnic_name'];

}