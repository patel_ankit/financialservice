<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Taxtitle extends Model
{

    protected $fillable = ['title'];

    public function price()
    {
        return $this->belongsToMany(Price::class);
    }

}