<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Accountcode extends Model
{
    protected $fillable = ['accountcode', 'account_name', 'account_belongs_to', 'status'];
}