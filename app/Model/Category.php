<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['bussiness_name', 'business_cat_name', 'business_cat_image', 'link'];
}