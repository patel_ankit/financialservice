<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['longitude', 'latitude', 'email', 'placename', 'phone', 'fax'];
}