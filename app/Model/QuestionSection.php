<?php

namespace App\Model;

use Eloquent;

class QuestionSection extends Eloquent
{
    protected $fillable = ['question_type', 'status'];
}