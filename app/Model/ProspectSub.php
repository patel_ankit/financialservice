<?php

namespace App\Model;

use Eloquent;

class ProspectSub extends Eloquent
{
    public $table = "prospect_services_sub";
    protected $fillable = ['name', 'service_id'];
}