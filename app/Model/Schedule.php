<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['emp_city', 'emp_name', 'duration', 'sch_start_date', 'sch_end_date', 'schedule_in_time', 'schedule_out_time', 'status'];
}