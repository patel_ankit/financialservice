<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rules extends Model
{
    protected $fillable = ['rules'];
}