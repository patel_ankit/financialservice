<?php

namespace App\Model;

use Eloquent;

class MyWork extends Eloquent
{
    public $table = "mywork";
    protected $fillable = ['priority', 'type_of_work'];
}