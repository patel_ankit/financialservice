<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Formcategory extends Model
{
    protected $fillable = ['category', 'picture', 'type', 'status'];
}