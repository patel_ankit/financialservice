<?php

namespace App\Model;

use Eloquent;

class Answer extends Eloquent
{
    protected $fillable = ['answer', 'q_id'];
}