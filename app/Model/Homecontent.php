<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Homecontent extends Model
{
    protected $fillable = ['title', 'content'];
}