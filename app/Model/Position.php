<?php

namespace App\Model;

use Eloquent;

class Position extends Eloquent
{
    protected $fillable = ['position'];
}