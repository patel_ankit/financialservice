<?php

namespace App\Model;

use Eloquent;

class formationsetup extends Eloquent
{
    protected $fillable = ['question'];
}