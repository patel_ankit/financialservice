<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact_userinfo extends Model
{
    protected $fillable = ['nametype_sec', 'user_id', 'firstname_sec', 'middlename_sec', 'lastname_sec', 'contact_address_sec_1', 'contact_address_sec_2', 'city_sec', 'state_sec', 'zip_sec', 'mobile_sec_1', 'mobiletype_sec_1', 'ext_sec_1', 'mobile_sec_2', 'mobiletype_sec_2', 'ext_sec_2', 'contact_fax_sec', 'email_sec'];
}