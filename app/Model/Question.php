<?php

namespace App\Model;

use Eloquent;

class Question extends Eloquent
{
    protected $fillable = ['question'];
}