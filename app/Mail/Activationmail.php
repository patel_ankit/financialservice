<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Activationmail extends Mailable
{
    use Queueable, SerializesModels;

    public $kk;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($kk)
    {
        $this->first_name = $kk['first_name'];
        $this->email = $kk['email'];
        $this->password = $kk['password'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('activation')->with([
            'email' => $this->email,
            'password' => $this->password,
            'first_name' => $this->first_name,
        ]);;;
    }
}