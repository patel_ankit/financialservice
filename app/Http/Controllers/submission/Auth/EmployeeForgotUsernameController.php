<?php

namespace App\Http\Controllers\employee\Auth;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeForgotUsernameController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fscemployee/forgotempusername/forgotempusername');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->email;
        $user = Fscemployee::where('email', '=', $email)->first();
        if ($user === null) {
            return redirect(route('forgotempusername.index'))->with('error', 'This Email not found');
        } else {
            $msg = 'Forgot Password';
            $activation_link = route('forgotempusername.edit', ['id' => $user->id, 'email' => $email, 'token' => urlencode($user->remember_token)]);
            $data = array('email' => $email, 'token' => $activation_link, 'messages' => $msg);
            \Mail::send('fscemployee/forgotempusername1', $data, function ($message) use ($data) {
                $message->to($data['email'])->from($data['email'], $data['messages'], $data['token'])->subject('Forgot Username');
            });
            return redirect(route('forgotempusername.index'))->with('success', 'Please Check Your Email');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = Fscemployee::where('id', $id)->first();
        return View('forgotempusername.index', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Fscemployee::where('id', $id)->first();
        return view('fscemployee/forgotempusername/edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required',

        ]);
        $business = Fscemployee::find($id);
        $business->email = $request->email;
        $business->update();
        return redirect(route('forgotempusername.index'))->with('success', 'Your Username Successfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}