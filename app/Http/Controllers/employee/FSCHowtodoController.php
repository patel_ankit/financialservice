<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Howtodo;
use App\Model\Workcategory;
use Auth;
use DB;
use Illuminate\Http\Request;

class FSCHowtodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $howtodo=DB::table('howtodos_steps')->where('noteid','=',null)->get(); 
        // $howtodo1 =DB::table('howtodos_steps')->get();  
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        // $homecontent = Howtodo::get();
        //  $worktodo = DB::table('client_worknew')->select('commonregisters.id','commonregisters.filename','commonregisters.company_name','client_worknew.id','client_worknew.adminid',
        // 'client_worknew.clientid','client_worknew.worknew_category','client_worknew.worknew_type',
        // 'client_worknew.worknew_priority','client_worknew.worknew_duedate','client_worknew.worknew_emp','client_worknew.worknew_details','client_worknew.worknew_note',
        // 'client_worknew.created_at','employees.id as eid','employees.employee_id','employees.firstName','employees.middleName','employees.lastName','workcategories.id as workid','workcategories.category_name')
        //     ->leftJoin('commonregisters', function($join){ $join->on('client_worknew.clientid', '=', 'commonregisters.id');})
        //     ->leftJoin('workcategories', function($join){ $join->on('client_worknew.worknew_category', '=', 'workcategories.id');})
        //     ->leftJoin('employees', function($join){ $join->on('client_worknew.worknew_emp', '=', 'employees.employee_id');})
        //     ->where('employees.id','=',$user_id)
        //     ->get();

        $worktodo = DB::table('client_worknew')->select('commonregisters.id', 'commonregisters.filename', 'commonregisters.company_name', 'client_worknew.id', 'client_worknew.adminid',
            'client_worknew.clientid', 'client_worknew.worknew_category', 'client_worknew.worknew_type',
            'client_worknew.worknew_priority', 'client_worknew.worknew_duedate', 'client_worknew.worknew_emp', 'client_worknew.worknew_details', 'client_worknew.worknew_note',
            'client_worknew.created_at', 'employees.employee_id', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'workcategories.id as workid', 'workcategories.category_name')
            ->leftJoin('commonregisters', function ($join) {
                $join->on('client_worknew.clientid', '=', 'commonregisters.id');
            })
            ->leftJoin('workcategories', function ($join) {
                $join->on('client_worknew.worknew_category', '=', 'workcategories.id');
            })
            ->leftJoin('employees', function ($join) {
                $join->on('client_worknew.worknew_emp', '=', 'employees.employee_id');
            })
            ->orderBy('client_worknew.created_at', 'desc')->get();

        $categoryname = DB::table('workcategories')
            ->select('workcategories.*', DB::raw('group_concat(category_name) as names'))
            ->get();
        //  echo "<pre>";
        // print_r($worktodo);die;

        //return view('fscemployee/fschowtodo/fscworktodo', compact(['worktodo','howtodo','homecontent','employee','howtodo1']));
        return view('fscemployee/fschowtodo/fschowtodo', compact(['categoryname', 'worktodo', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/fschowtodo/create', compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [


        ]);
        $user_id = Auth::user()->user_id;
        $position = new Howtodo;
        $position->subject = $request->subject;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $position->empid = $user_id;
        $noteid = $request->stepid;
        $adminnotes = $request->step;
        $k = 0;

        $position->save();
        $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
        foreach ($adminnotes as $notess) {
            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`) values('" . $note1 . "','" . $iddd . "')");
            } else {

                $users = DB::table('howtodos_steps')->where('steps', '')->delete();
            }
        }
        return redirect('fscemployee/fschowtodo')->with('success', 'New How To Do Added Successfully');
    }

    public function howtodo11delete($id)
    {
        // Contact_userinfo::where('id',$id)->delete();
        DB::table('howtodos_steps')->where('id', '=', $id)->delete();
        return redirect('fscemployee/fschowtodo');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ids)
    {
        //$homecontent = Howtodo::where('id',$id)->first(); 
        //$howtodo=DB::table('howtodos_steps')->where('howtodo_id','=',$id)->get(); 
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();


        $empname = Employee::where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        $worktodo = DB::table('client_worknew')->select('employees.employee_id', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.business_id',
            'commonregisters.company_name', 'commonregisters.business_no', 'commonregisters.first_name', 'commonregisters.last_name', 'client_worknew.id as ids', 'client_worknew.adminid', 'client_worknew.worknew_petname', 'client_worknew.worknew_fname', 'client_worknew.worknew_mname', 'client_worknew.worknew_lname', 'client_worknew.worknew_telephone', 'client_worknew.worknew_email', 'client_worknew.clientid', 'client_worknew.worknew_category', 'client_worknew.worknew_type', 'client_worknew.worknew_priority', 'client_worknew.worknew_duedate', 'client_worknew.worknew_emp', 'client_worknew.worknew_details', 'client_worknew.worknew_note')
            ->leftJoin('employees', function ($join) {
                $join->on('client_worknew.worknew_emp', '=', 'employees.employee_id');
            })
            ->leftJoin('commonregisters', function ($join) {
                $join->on('client_worknew.clientid', '=', 'commonregisters.id');
            })
            ->where('client_worknew.id', '=', $ids)->first();
        $workcategory = Workcategory::orderBy('category_name', 'asc')->get();


        //$workcategory = Workcategory::orderBy('category_name', 'asc')->get();
        // $worktodo = DB::table('client_worknew')->select('commonregisters.business_id','commonregisters.id','commonregisters.filename','commonregisters.company_name','client_worknew.id as ids','client_worknew.adminid',
        // 'client_worknew.clientid','client_worknew.worknew_category','client_worknew.worknew_type',
        // 'client_worknew.worknew_priority','client_worknew.worknew_duedate','client_worknew.worknew_emp','client_worknew.worknew_details','client_worknew.worknew_note',
        // 'client_worknew.created_at','workcategories.id as workid','workcategories.category_name')
        //     ->leftJoin('commonregisters', function($join){ $join->on('client_worknew.clientid', '=', 'commonregisters.id');})
        //     ->leftJoin('workcategories', function($join){ $join->on('client_worknew.worknew_category', '=', 'workcategories.id');})
        //     ->where('client_worknew.id','=',$id)
        //     ->first();
        //    echo "<pre>";print_r($worktodo);exit;
        //return view('fscemployee.fschowtodo.edit',compact(['workcategory','worktodo','homecontent','howtodo','employee']));
        return view('fscemployee.fschowtodo.edit', compact(['workcategory', 'worktodo', 'empname', 'employee']));
    }


    function fetch3(Request $request)
    {
        echo "<pre>";
        print_r($_REQUEST);
        exit;
        if (isset($_POST['ids']) != '') {
            $ids = $_POST['ids'];
        } else {
            $ids = '';
        }

        if (isset($_POST['worknew_petname']) != '') {
            $worknew_petname = $_POST['worknew_petname'];
        } else {
            $worknew_petname = '';
        }

        if (isset($_POST['worknew_mname']) != '') {
            $worknew_fname = $_POST['worknew_fname'];
        } else {
            $worknew_fname = '';
        }

        if (isset($_POST['worknew_petname']) != '') {
            $worknew_mname = $_POST['worknew_mname'];
        } else {
            $worknew_mname = '';
        }

        if (isset($_POST['worknew_lname']) != '') {
            $worknew_lname = $_POST['worknew_lname'];
        } else {
            $worknew_lname = '';
        }

        if (isset($_POST['worknew_telephone']) != '') {
            $worknew_telephone = $_POST['worknew_telephone'];
        } else {
            $worknew_telephone = '';
        }


        if (isset($_POST['worknew_email']) != '') {
            $worknew_email = $_POST['worknew_email'];
        } else {
            $worknew_email = '';
        }


        $id = DB::table('client_worknew')
            ->where('id', $ids)
            ->update([

                'worknew_petname' => $worknew_petname,
                'worknew_fname' => $worknew_fname,
                'worknew_mname' => $worknew_mname,
                'worknew_lname' => $worknew_lname,
                'worknew_telephone' => $worknew_telephone,
                'worknew_email' => $worknew_email,


                'worknew_category' => $_POST['worknew_category'],
                'worknew_type' => $_POST['worknew_type'],
                'worknew_priority' => $_POST['worknew_priority'],
                'worknew_duedate' => date('Y-m-d', strtotime($_POST['worknew_duedate'])),
                'worknew_emp' => $_POST['worknew_emp'],
                'worknew_details' => $_POST['worknew_details'],
                'worknew_note' => $_POST['worknew_note'],
            ]);

        return redirect('fac-Bhavesh-0554/worktodo')->with('success', 'Worktodo Update Successfully');

    }

    public function updatesssssss(Request $request, $id)
    {


        $position = Howtodo::find($id);
        $position->subject = $request->subject;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $noteid = $request->stepid;
        $position->empid = $user_id;
        $adminnotes = $request->step;
        $k = 0;
        $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
        foreach ($adminnotes as $notess) {
            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`) values('" . $note1 . "','" . $iddd . "')");
            } else {
                $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                    ->update(['steps' => $note1,
                        'howtodo_id' => $id,
                    ]);
                $users = DB::table('howtodos_steps')->where('steps', '')->delete();
            }
        }
        $position->update();
        return redirect('fscemployee/fschowtodo')->with('success', 'Success fully update How To Do');
    }


    public function destroy($ids)
    {
        DB::table('client_worknew')->where('id', $ids)->delete();
        return redirect(route('fschowtodo.index'))->with('success', 'Success fully deleted worktodo');
    }
}