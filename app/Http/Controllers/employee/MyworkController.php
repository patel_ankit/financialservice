<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Workcategory;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;


class MyworkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user_id = Auth::user()->user_id;
        $id = Auth::user()->id;

        $employee = Employee::where('id', '=', $user_id)->first();

        return view('fscemployee/mywork/mywork', compact('employee'));
    }

    public function getClientfederaldata2(Request $request)
    {
        $documentRow1 = DB::table('client_taxfederal')->where('id', $request->federalid)->first();

        $documentRow = DB::table('commonregisters')->select('statetax.taxform', 'statetax.id as stateids', 'statetax.statename', 'commonregisters.id', 'commonregisters.business_id', 'commonregisters.filename', 'commonregisters.company_name',
            'commonregisters.stateId', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.created_at',
            'client_to_taxation.id as taxids', 'client_to_taxation.clientid', 'client_to_taxation.taxation_service_period', 'client_to_taxation.pricetype', 'client_to_taxation.taxyears')
            ->leftJoin('client_to_taxation', function ($join) {
                $join->on('commonregisters.id', '=', 'client_to_taxation.clientid');
            })
            ->leftJoin('statetax', function ($join) {
                $join->on('statetax.statename', '=', 'commonregisters.stateId');
            })
            ->where('commonregisters.id', $request->federalid)->where('client_to_taxation.id', '=', $request->taxid)->first();
        return response()->json($documentRow);
    }

    public function store(Request $request)
    {
        // ECHO "<PRE>";print_r($_POST);EXIT;
        $client_id = $request->client_id;
        $federalsyear = $request->federalsyear;
        $federalstax = $request->federalstax;
        $federalsduedate = date('Y-m-d', strtotime($request->federalsduedate));
        $federalsform = $request->federalsform;
        $federalsmethod = $request->federalsmethod;
        $federalsdate = date('Y-m-d', strtotime($request->federalsdate));
        $federalsstatus = $request->federalsstatus;
        $federalsnote = $request->federalsnote;
        if ($federalstax == 'Original') {
            $personalstatus = '1';

        } else {
            $personalstatus = '0';
        }

        if ($federalsyear == '2019' && $federalstax == 'Extension') {
            $dates = date('Y-m-d', strtotime("+3 months", strtotime($federalsduedate)));
        } else {
            $dates = $federalsduedate;
        }

        // if(isset($_FILES['federalsfile']['name'])!='')
        // {
        //     $path1= public_path().'/adminupload/'.$_FILES['federalsfile']['name'];
        //     if(move_uploaded_file($_FILES['federalsfile']['tmp_name'], $path1)) 
        //     {
        //       $federal_copy= $_FILES['federalsfile']['name']; 
        //     }
        //     else
        //     {
        //         $federal_copy= ''; 
        //     }
        // }
        // $federalsfile=$federal_copy;

        DB::table('commonregisters')->where('id', $client_id)
            ->update([
                'federalstax' => $request->federalstax,
                'federalsduedate' => $dates,
                'personalstatus' => $personalstatus
            ]);

        DB::table('client_to_taxation')->where('clientid', $client_id)
            ->update([
                'expiredate' => $dates
            ]);
        DB::table('client_taxfederal')->where('client_id', '=', $client_id)->delete();
        DB::table('client_taxstate')->where('client_id', '=', $client_id)->delete();

        $ids = DB::table('client_taxfederal')->insertGetId(array(
            'client_id' => $client_id,
            'federalsyear' => $federalsyear,
            'federalstax' => $federalstax,
            'federalsduedate' => $dates,
            'federalsform' => $federalsform,
            'federalsmethod' => $federalsmethod,
            'federalsdate' => $federalsdate,
            'federalsstatus' => $federalsstatus,
            //             'federalsfile' =>$federalsfile,
            'federalsnote' => $federalsnote,
        ));
        //$ids=$data->id;
        //echo $ids;die;


        $l = 0;
        if (empty($request->statetax)) {
        } else {
            foreach ($request->statetax as $typeofservice11) {
                $client_id1 = $request->client_id;
                $federal_id1 = $ids;
                $stateyear = $request->federalsyear;
                $statetax1 = $request->statetax[$l];
                $stateformno1 = $request->stateformno[$l];
                $statemethod1 = $request->statemethod[$l];
                $statedate1 = date('Y-m-d', strtotime($request->statedate[$l]));
                $fillingdate1 = $request->fillingdate[$l];
                $statestatus1 = $request->statestatus[$l];
                //$statefile1 = $request->statefile[$l];
                $statenote1 = $request->statenote[$l];


                // if(isset($_FILES['statefile']['name'][$l])!='')
                // {
                //     $path1= public_path().'/adminupload/'.$_FILES['statefile']['name'][$l];
                //     if(move_uploaded_file($_FILES['statefile']['tmp_name'][$l], $path1)) 
                //     {
                //       $state_copy= $_FILES['statefile']['name'][$l]; 
                //     }
                //     else
                //     {
                //         $state_copy= ''; 
                //     }
                // }
                // $statefile1=$state_copy;

                $l++;
                DB::insert("insert into client_taxstate(`client_id`,`federal_id`,`stateyear`,`statetax`,`stateformno`,`statemethod`,`statedate`,`statestatus`,`statenote`) 
                values('" . $client_id1 . "','" . $federal_id1 . "','" . $stateyear . "','" . $statetax1 . "','" . $stateformno1 . "','" . $statemethod1 . "','" . $statedate1 . "','" . $statestatus1 . "','" . $statenote1 . "')");
            }
        }


        // $stateyear=$request->stateyear;
        // $statetax=$request->statetax;
        // $stateduedate=date('Y-m-d',strtotime($request->stateduedate));
        // $stateform=$request->stateform;
        // $statemethod=$request->statemethod;
        // $statedate=date('Y-m-d',strtotime($request->statedate));
        // $statestatus=$request->statestatus;
        // $statenote=$request->statenote;
        // $path1= public_path().'/adminupload/'.$_FILES['statefile']['name'];
        // if(isset($_FILES['statefile']['name'])!='')
        // {
        //     if(move_uploaded_file($_FILES['statefile']['tmp_name'], $path1)) 
        //     {
        //       $state_copy= $_FILES['statefile']['name']; 
        //     }
        //     else
        //     {
        //         $state_copy= ''; 
        //     }
        // }
        // $statefile=$state_copy;

        // $data=DB::table('client_taxstate')
        //     ->insert([
        //         'client_id' =>$client_id,
        //         'federal_id' =>$ids,
        //         'stateyear' =>$stateyear,
        //         'statetax' =>$statetax,
        //         'stateduedate' =>$stateduedate,
        //         'stateform' =>$stateform,
        //         'statemethod' =>$statemethod,
        //         'statedate' =>$statedate,
        //         'statestatus' =>$statestatus,
        //         'statefile' =>$statefile,
        //         'statenote' =>$statenote,
        //     ]);


        return redirect()->back()->with('success', 'Taxation Added Successfully');

    }


    public function edit($ids)
    {

        $empname = Employee::where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        // $worktodo = DB::table('commonregisters')->select('employees.employee_id','employees.firstName','employees.middleName','employees.lastName','commonregisters.id','commonregisters.filename','commonregisters.business_id',
        // 'commonregisters.company_name','commonregisters.business_no','commonregisters.first_name','commonregisters.last_name','client_worknew.id as ids','client_worknew.adminid','client_worknew.clientid','client_worknew.worknew_category','client_worknew.worknew_type','client_worknew.worknew_priority','client_worknew.worknew_duedate','client_worknew.worknew_emp','client_worknew.worknew_details','client_worknew.worknew_note')
        // ->leftJoin('employees', function($join){ $join->on('commonregisters.worknew_emp', '=', 'employees.employee_id');})
        // ->leftJoin('client_worknew', function($join){ $join->on('commonregisters.id', '=', 'client_worknew.clientid');})
        // ->where('client_worknew.id','=',$ids)->first();

        $worktodo = DB::table('client_worknew')->select('employees.employee_id', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.business_id',
            'commonregisters.company_name', 'commonregisters.business_no', 'commonregisters.first_name', 'commonregisters.last_name', 'client_worknew.id as ids', 'client_worknew.adminid', 'client_worknew.worknew_petname', 'client_worknew.worknew_fname', 'client_worknew.worknew_mname', 'client_worknew.worknew_lname', 'client_worknew.worknew_telephone', 'client_worknew.worknew_email', 'client_worknew.clientid', 'client_worknew.worknew_category', 'client_worknew.worknew_type', 'client_worknew.worknew_priority', 'client_worknew.worknew_duedate', 'client_worknew.worknew_emp', 'client_worknew.worknew_details', 'client_worknew.worknew_note')
            ->leftJoin('employees', function ($join) {
                $join->on('client_worknew.worknew_emp', '=', 'employees.employee_id');
            })
            ->leftJoin('commonregisters', function ($join) {
                $join->on('client_worknew.clientid', '=', 'commonregisters.id');
            })
            ->where('client_worknew.id', '=', $ids)->first();

        // $worktodo = DB::table('client_worknew')->select('*')->where('client_worknew.id','=',$ids)->first();
        //print_r($worktodo);die;

        $workcategory = Workcategory::orderBy('category_name', 'asc')->get();

        return View('fscemployee.workregular.edit', compact(['workcategory', 'worktodo', 'empname']));
    }


}