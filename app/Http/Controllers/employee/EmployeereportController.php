<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use View;

class EmployeereportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        //$start =date('Y-m-d',strtotime($request->startdate));
        //$end = date('Y-m-d',strtotime($request->enddate));
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();

        $emp = Employee::where('check', '=', 1)->where('type', '=', $employee->type)->get();
        $start = date('Y-m-d', strtotime($request->startdate));
        $end = date('Y-m-d', strtotime($request->enddate));
        $thisdate = date('Y-m-d', strtotime($start));
        $thismonday = date('Y-m-d', strtotime('monday this week', strtotime($thisdate)));
        $thisfriday = date('Y-m-d', strtotime('next sunday', strtotime($thisdate)));
        $nextmonday = date('Y-m-d', strtotime('+1 day', strtotime($thisfriday)));
        $nextfriday = date('Y-m-d', strtotime('+6 day', strtotime($nextmonday)));
        //  $user_id = $request->emp_name;
        $employee2 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,launch_in_status,launch_in_status_1,clock_out_status')->whereBetween('emp_in_date', array($thismonday, $thisfriday))->get();
        $employee3 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,launch_in_status,launch_in_status_1,clock_out_status')->whereBetween('emp_in_date', array($nextmonday, $nextfriday))->get();
        $employee1 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,launch_in_status,launch_in_status_1,clock_out_status')->whereBetween('emp_in_date', array($start, $end))->get();
        return view('fscemployee/report/report', compact(['emp', 'employee1', 'employee2', 'employee3', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function employeedetail122(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $employee1 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date')->whereBetween('emp_in_date', array($request->min, $request->max))->get();
        return view('fscemployee/report/report', compact(['employee', 'employee1']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}