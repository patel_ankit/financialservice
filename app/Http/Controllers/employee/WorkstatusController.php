<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Model\Currency;
use App\Model\Employee;
use App\Model\Ethnic;
use App\Model\Language;
use App\Model\Logo;
use App\Model\Period;
use App\Model\Price;
use App\Model\Task;
use App\Model\taxstate;
use App\Model\Taxtitle;
use App\Model\Typeofser;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class WorkstatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }


    public function index(Request $request)
    {


        $id = $request->id;
        $admin = Admin::All();
        $taskall = Task::where('client', $id)->orderBy('created_at', 'desc')->get();
        $empfsc = Fscemployee::All();
        $set1 = Employee::All();
        $document = DB::table('clientdocument')->get();
        $documentupload = DB::table('clienttodocument')->where('client_id', '=', $id)->get();
        $taxstate = taxstate::All();
        $language = language::orderBy('language_name', 'asc')->get();
        $ethnic = Ethnic::orderBy('ethnic_name', 'asc')->get();
        $newclient12 = Commonregister::where('id', $id)->first();
        if (empty($newclient12)) {
            $other_first_language1 = '';
            $other_second_language1 = '';
            $category = '';
            $cb = '';
            $bid = '';
            $businessbrand = '';
        } else {
            $bid = $newclient12->business_id;
            $catid = $newclient12->business_cat_id;
            $brand_id = $newclient12->business_brand_id;
            $businessbrand = BusinessBrand::where('business_cat_id', '=', $catid)->orderBy('business_brand_name', 'asc')->get();
            $cb = Categorybusiness::where('business_brand_id', '=', $brand_id)->orderBy('business_brand_category_name', 'asc')->get();
            $category = Category::where('bussiness_name', '=', $bid)->orderby('business_cat_name', 'asc')->get();
            $other_first_language1 = Employee::where('id', '=', $newclient12->other_first_language1)->first();
            $other_second_language1 = Employee::where('id', '=', $newclient12->other_second_language1)->first();
        }

        $business = Business::orderBy('bussiness_name', 'asc')->get();

        $info = Contact_userinfo::where('user_id', '=', $id)->get();
        $info1 = Contact_userinfo::where('user_id', '=', $id)->first();
        $user_id = Auth::user()->id;
        $business_cat_id = Auth::user()->business_cat_id;
        $user = User::where('user_id', '=', $id)->first();
        $newclient = Commonregister::where('id', $id)->update(['newclient' => 2]);
        $subcustomer = User::get();
        $note = DB::table('notes')->get();
        $newlocations = DB::table('newlocations')->where('clientid', $id)->get();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
        $period = Period::All();

        $taxtitle = Taxtitle::All();
        $clienttopersonaltax = DB::table('clienttopersonaltax')->where('client_id', '=', $id)->get();
        $admin_notes = DB::table('notes')->where('type', '=', 'admin')->where('userid', '=', $id)->get();
        $clientser = DB::table('clientservices')->where('clientid', '=', $id)->get();
        $clientser5 = DB::table('clientservices')->where('clientid', '=', $id)->get()->first();
        $clientsertitle = DB::table('clientservicetitles')->where('clientid', '=', $id)->where('clientid', '=', $id)->get();
        $client = DB::table('notes')->where('admin_id', '=', $id)->where('type', '=', 'client')->get();
        $fsc = DB::table('notes')->where('admin_id', '=', '')->where('type', '=', 'fsc')->get();
        $every = DB::table('notes')->where('type', '=', 'Everybody')->get();
        $employee1 = Employee::where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        $employee2 = Fscemployee::orderBy('name', 'asc')->get();
        $common = DB::table('commonregisters')->select('commonregisters.formation_register_entity', 'commonregisters.type_of_entity_answer', 'commonregisters.contact_number as contact_number', 'commonregisters.work_year', 'commonregisters.work_status', 'commonregisters.work_annualfees', 'commonregisters.work_processingfees', 'commonregisters.work_totalamt', 'commonregisters.work_paidby', 'commonregisters.work_paymentmethod', 'commonregisters.work_note', 'commonregisters.work_officer', 'commonregisters.work_annualreceipt', 'commonregisters.work_renewyear', 'commonregisters.work_zip', 'commonregisters.work_renewperiod', 'commonregisters.work_address', 'commonregisters.work_city', 'commonregisters.work_state', 'commonregisters.work_address as work_address', 'commonregisters.work_changes as work_changes', 'commonregisters.id as cid', 'commonregisters.contact_title as contact_title', 'commonregisters.paymentmode11 as paymentmode11', 'commonregisters.carttype1 as carttype1', 'commonregisters.cartno1 as cartno1', 'commonregisters.acountname1 as acountname1', 'commonregisters.paymentnote1 as paymentnote1', 'commonregisters.acountname1 as acountname1', 'commonregisters.acountno1 as acountno1', 'commonregisters.routingno1 as routingno1', 'commonregisters.bankname1 as bankname1', 'commonregisters.maritial_spouse1 as maritial_spouse1', 'commonregisters.maritial_spouse as maritial_spouse', 'commonregisters.ext2 as ext2', 'commonregisters.telephoneNo2Type as telephoneNo2Type', 'commonregisters.motelephoneile1 as motelephoneile1', 'commonregisters.maritial_last_name as maritial_last_name', 'commonregisters.maritial_middle_name as maritial_middle_name', 'commonregisters.maritial_first_name as maritial_first_name', 'commonregisters.personalname as personalname', 'commonregisters.CL as CL', 'commonregisters.other_maritial_status1 as other_maritial_status1', 'commonregisters.other_maritial_status as other_maritial_status', 'commonregisters.other_dob_month as other_dob_month', 'commonregisters.other_dob_day as other_dob_day', 'commonregisters.other_spouse_month as other_spouse_month', 'commonregisters.other_spouse_day as other_spouse_day', 'commonregisters.other_marriage_month as other_marriage_month', 'commonregisters.other_marriage_day as other_marriage_day', 'commonregisters.other_ethnic as other_ethnic', 'commonregisters.other_main_language as other_main_language', 'commonregisters.other_first_language as other_first_language', 'commonregisters.other_second_language as other_second_language', 'commonregisters.emailbli1 as emailbli1', 'commonregisters.faxbli1 as faxbli1', 'commonregisters.faxbli3 as faxbli3', 'commonregisters.cartnote as cartnote', 'commonregisters.billingtoo as billingtoo', 'commonregisters.cartno as cartno', 'commonregisters.carttype as carttype', 'commonregisters.paymentmode1 as paymentmode1', 'commonregisters.acountname as acountname', 'commonregisters.acountno as acountno', 'commonregisters.routingno as routingno', 'commonregisters.bankname as bankname', 'commonregisters.paymentnote as paymentnote', 'commonregisters.paymentmode as paymentmode', 'commonregisters.locations as locations', 'commonregisters.multilocation as multilocation', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.user_answer3 as user_answer3', 'commonregisters.user_answer2 as user_answer2', 'commonregisters.user_answer1 as user_answer1', 'commonregisters.limited_answer3 as limited_answer3', 'commonregisters.limited_answer2 as limited_answer2', 'commonregisters.limited_answer1 as limited_answer1', 'commonregisters.user_question3 as user_question3', 'commonregisters.user_question2 as user_question2', 'commonregisters.user_question1 as user_question1', 'commonregisters.limited_question3 as limited_question3', 'commonregisters.limited_question2 as limited_question2', 'commonregisters.limited_question1 as limited_question1', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdate as user_resetdate', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdays as user_resetdays', 'commonregisters.user_active as user_active', 'commonregisters.limited_resetdate as limited_resetdate', 'commonregisters.limited_resetdays as limited_resetdays', 'commonregisters.limited_active as limited_active', 'commonregisters.limited_user as limited_user', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_resetdays as subscription_resetdays', 'commonregisters.subscription_resetdate as subscription_resetdate', 'commonregisters.subscription_active as subscription_active', 'commonregisters.subscription_lock as subscription_lock', 'commonregisters.subscription_user as subscription_user', 'commonregisters.user_cell as user_cell', 'commonregisters.user_email as user_email', 'commonregisters.user_name as user_name', 'commonregisters.creationdate as creationdate', 'commonregisters.nametype as nametype', 'commonregisters.etelephone2 as etelephone2', 'commonregisters.eext2 as eext2', 'commonregisters.eteletype2 as eteletype2', 'commonregisters.eext1  as eext1', 'commonregisters.eteletype1 as eteletype1', 'commonregisters.filename as filename', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.businessext as businessext', 'commonregisters.businesstype as businesstype', 'commonregisters.contact_address1 as contact_address1', 'commonregisters.contact_address2 as contact_address2', 'commonregisters.city_1 as city_1', 'commonregisters.state_1 as state_1', 'commonregisters.zip_1 as zip_1', 'commonregisters.mobile_1 as mobile_1', 'commonregisters.mobiletype_1 as mobiletype_1', 'commonregisters.ext2_1 as ext2_1', 'commonregisters.mobile_2 as mobile_2', 'commonregisters.mobiletype_2 as mobiletype_2', 'commonregisters.ext2_2 as ext2_2', 'commonregisters.contact_fax_1 as contact_fax_1', 'commonregisters.email_1 as email_1', 'commonregisters.minss as minss', 'commonregisters.firstname as firstname', 'commonregisters.middlename as middlename', 'commonregisters.lastname as lastname', 'commonregisters.mailing_address1 as mailing_address1', 'commonregisters.bussiness_zip as bussiness_zip', 'commonregisters.due_date as due_date', 'commonregisters.department as department', 'commonregisters.type_of_activity as type_of_activity', 'commonregisters.county_no as county_no', 'commonregisters.county_name as county_name', 'commonregisters.level as level', 'commonregisters.setup_state as setup_state', 'commonregisters.business_state as business_state', 'commonregisters.business_city as business_city', 'commonregisters.business_country as business_country', 'commonregisters.business_address as business_address', 'commonregisters.business_store_name as business_store_name', 'commonregisters.mailing_address as mailing_address', 'commonregisters.legalname as legalname', 'commonregisters.dbaname as dbaname', 'commonregisters.mailing_city as mailing_city', 'commonregisters.mailing_state as mailing_state', 'commonregisters.mailing_zip as mailing_zip', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name', 'commonregisters.formation_yearbox', 'commonregisters.formation_yearvalue', 'commonregisters.formation_amount', 'commonregisters.formation_payment', 'commonregisters.record_status', 'commonregisters.annualreceipt', 'commonregisters.formation_work_officer', 'commonregisters.formation_date', 'commonregisters.paiddate', 'commonregisters.id')
            ->leftJoin('categories', function ($join) {
                $join->on('commonregisters.business_cat_id', '=', 'categories.id');
            })
            ->leftJoin('businesses', function ($join) {
                $join->on('commonregisters.business_id', '=', 'businesses.id');
            })
            ->leftJoin('business_brands', function ($join) {
                $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
            })
            ->leftJoin('categorybusinesses', function ($join) {
                $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
            })
            ->where('commonregisters.id', '=', "$id")->get()->first();

        $buslicense = DB::table('client_license')->select('*')->where('client_id', '=', $id)->get();

        $logo = Logo::where('id', '=', 1)->first();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();

        $conversation = DB::table('conversation_sheet as t1')->select('t1.*', 't2.id as ids', 't2.relatednames')
            ->Join('relatednames as t2', function ($join) {
                $join->on('t2.id', '=', 't1.conrelatedname');
            })
            ->where('t1.clientid', '=', $id)
            ->get();
        $listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type', 'employee')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        //print_r($listemployeeuser);die;
        $relatedNames = DB::table('relatednames')->get();

        $notesdata = DB::table('notes_sheet as t1')->select('t1.*', 't2.id as ids', 't2.notesrelated')
            ->Join('notesrelateds as t2', function ($join) {
                $join->on('t2.id', '=', 't1.noterelated');
            })
            ->where('t1.noteclientid', '=', $id)
            ->get();
        //print_r($notesdata);die;
        $NotesNames = DB::table('notesrelateds')->get();
        $datastate = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate2 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate3 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $Incometaxfederal = DB::table('client_taxfederal')->where('client_id', '=', $id)->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->get();
        $Incometaxfederal2 = DB::table('client_taxfederal')->where('client_id', '=', $id)->where('federalsyear', '=', '2019')->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->count();
        $Incometax3 = DB::table('client_taxfederal')->where('client_id', '=', $id)->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->get();
        $Incometaxstates = DB::table('client_taxstate')->orderBy('stateyear', 'DESC')->groupBy('stateyear')->get();
        $Incometax2 = DB::table('client_taxstate')->orderBy('stateyear', 'DESC')->get();

        return view('fscemployee/workstatus/workstatus', compact(['datastate2', 'datastate3', 'datastate', 'Incometax2', 'Incometaxstates', 'Incometaxfederal2', 'Incometax3', 'Incometaxfederal', 'admin', 'set1', 'empfsc', 'taskall', 'notesdata', 'NotesNames', 'listclient', 'listvendoe', 'listemployeeuser', 'relatedNames', 'conversation', 'logo', 'employee2', 'clienttopersonaltax', 'document', 'documentupload', 'buslicense', 'other_first_language1', 'other_second_language1', 'ethnic', 'language', 'newlocations', 'subcustomer', 'employee1', 'employee', 'clientser5', 'clientser', 'clientsertitle', 'note', 'every', 'fsc', 'taxstate', 'admin_notes', 'client', 'common', 'category', 'business', 'businessbrand', 'cb', 'user', 'info', 'info1', 'position', 'currency', 'period', 'typeofser', 'taxtitle']));
    }

    function fetchrec(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            // $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->get();

            $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->get();

            $output = '';
            $i;
            foreach ($data as $row) {
                if ($row->business_id == '6') {
                    $names = $row->first_name . ' ' . $row->middle_name . ' ' . $row->last_name;
                } else {
                    $names = $row->company_name;
                }
                $output .= '<li><a href="' . url('fscemployee/workstatus?id=') . '' . $row->id . '"><span class="clientalign">' . $row->filename . '</span> <span class="entityname">' . $names . '</span></a></li>';
            }
            $output .= '';
            echo $output;
        }
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'states' => 'required',
            'tax_authority' => 'required',
            'country_code' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'website' => 'required',
        ]);
        $position = new City;
        $position->authority_name = $request->states;
        $position->short_name = $request->tax_authority;
        $position->type_of_tax = $request->country_code;
        $position->telephone = $request->telephone;
        $position->address = $request->address;
        $position->city = $request->city;
        $position->state = $request->state;
        $position->zip = $request->zip;
        $position->website = $request->website;

        $position->save();
        return redirect('fscemployee/workstatus')->with('success', 'State Added Successfully');
    }


    public function update(Request $request, $id)
    {
        // echo "<pre>";print_r($_FILES); print_r($_POST);die;
        $id;


        $this->validate($request, [
            'formation_yearbox' => 'required',
            'formation_yearvalue' => 'required',
            'formation_amount' => 'required',
            'formation_payment' => 'required',
            'record_status' => 'required',
            //'formation_work_officer'=>'required',
            // 'annualreceipt'=>'required',
            //'formation_work_annualreceipt'=>'required',
        ]);


        $business = Commonregister::find($id);
        $business->formation_yearbox = $request->formation_yearbox;
        $business->formation_yearvalue = $request->formation_yearvalue;
        $business->formation_amount = $request->formation_amount;
        $business->formation_payment = $request->formation_payment;
        $business->record_status = $request->record_status;

        //$business->paiddate = $request->paiddate;

        $path1 = public_path() . '/adminupload/' . $_FILES['annualreceipt']['name'];
        $path2 = public_path() . '/adminupload/' . $_FILES['formation_work_officer']['name'];
        if (move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) {
            $filesname1 = $_FILES['annualreceipt']['name'];
        } else {
            $filesname1 = $_POST['annualreceipt_1'];
        }
        $business->annualreceipt = $filesname1;


        if (move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) {
            $filesname2 = $_FILES['formation_work_officer']['name'];
        } else {
            $filesname2 = $_POST['officers'];
        }
        $business->formation_work_officer = $filesname2;
        $business->update();

        return redirect(route('workstatus.index'))->with('success', 'Your Profile Successfully Updated');
    }

    public function destroyfederaltax(Request $request, $id)
    {
        DB::table('client_taxfederal')->where('id', '=', $id)->delete();
        DB::table('client_taxstate')->where('federal_id', '=', $id)->delete();
        return redirect()->back()->with('success', 'Your Record Deleted Successfully ');
    }

    public function destroy(Request $request, $id)
    {
        //echo "<pre>"; print_r($_POST);die;
        $id;


        $this->validate($request, []);

        $business = Commonregister::find($id);
        $business->formation_yearbox = '';
        $business->formation_yearvalue = '';
        $business->formation_amount = '';
        $business->formation_payment = '';
        $business->record_status = '';
        //$business->paiddate = '';
        $business->work_paidby = '';
        $business->annualreceipt = '';
        $business->formation_work_officer = '';
        $business->update();

        // $path1= public_path().'/adminupload/'.$_FILES['annualreceipt']['name'];
        // $path2= public_path().'/adminupload/'.$_FILES['formation_work_officer']['name'];
        // if(move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) 
        // {
        //   $filesname1= $_FILES['annualreceipt']['name']; 
        // } 
        // else
        // {
        //   $filesname1= $_POST['annualreceipt_1']; 
        // }
        // $business->annualreceipt=$filesname1;


        // if(move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) 
        // {
        //   $filesname2= $_FILES['formation_work_officer']['name']; 
        // } 
        // else
        // {
        //   $filesname2= $_POST['officers']; 
        // }
        // $business->formation_work_officer=$filesname2;


        return redirect(route('workstatus.index'))->with('success', 'Your Profile Successfully Deleted');
    }

    public function updatedocumentsemp(Request $request)
    {
        //print_r($_REQUEST);die;
        // print_r($_POST);die;

        $idss = $request->idaa;
        $filename_id = $request->filename_idss;
        $client_id = $request->client_id;
        $documentsname = $request->documentsname;
        $path1 = public_path() . '/clientupload/' . $_FILES['clientdocument']['name'];
        if (isset($_FILES['clientdocument']['name']) != '') {
            if (move_uploaded_file($_FILES['clientdocument']['tmp_name'], $path1)) {
                $filesname1 = $_FILES['clientdocument']['name'];
            } else {
                $filesname1 = $_POST['clientdocument_1'];
            }

        } else {
            $filesname1 = $_POST['clientdocument_1'];
        }

        $returnValue = DB::table('clienttodocument')->where('id', '=', $idss)
            ->update([
                'filename_id' => $filename_id,
                'client_id' => $client_id,
                'documentsname' => $documentsname,
                'clientdocument' => $filesname1
            ]);


        return redirect('fscemployee/workstatus?id=' . $client_id)->with('success', 'Your Document Successfully Updated');;

    }

    public function destroydocumentemp(Request $request, $id, $clientids)
    {

        DB::table('clienttodocument')->where('id', '=', $id)->delete();
        //  return redirect(route('workstatus.index'))->with('success','Client Document Successfully Deleted');

        return redirect('fscemployee/workstatus?id=' . $clientids)->with('success', 'Your Document Successfully Deleted');;

    }

    public function getcounty(Request $request)
    {
        $data = taxstate::select('county', 'id', 'countycode')->where('state', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode', 'id')->where('county', $request->id)->take(1000)->get();
        return response()->json($data);
    }


}