<?php

namespace App\Http\Controllers\employee;

use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Business;
use App\Model\Category;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\Period;
use App\Model\Taxtitle;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;

class EmpclientreportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $logo = Logo::where('id', '=', 1)->first();
        $employee = Employee::where('id', '=', $user_id)->first();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        $period = Period::All();
        if (($request->status == '1')) {
            $client = Commonregister::where('business_id', '=', $request->emp_name)->where('service_period', '=', $request->monthlytype)->get();
        } else {
            $client = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->emp_name)->where('service_period', '=', $request->monthlytype)->get();
        }

        $category = Category::orderBy('business_cat_name', 'asc')->get();
        $taxtitles1 = Taxtitle::whereIn('id', [7, 8])->orderBy('id', 'asc')->get();
        $taxtitles = Taxtitle::orderBy('id', 'asc')->get();
        // $businessfirst = Business::where('id','=',$_REQUEST['business_id'])->first();
        //$servicefirst = Taxtitle::where('id','=',$request->personaltype)->first();

        return view('fscemployee.empclientreport/empclientreport', compact(['employee', 'taxtitles', 'taxtitles1', 'business', 'category', 'logo', 'period']));

    }

    public function store(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $logo = Logo::where('id', '=', 1)->first();
        $employee = Employee::where('id', '=', $user_id)->first();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        $category = Category::orderBy('business_cat_name', 'asc')->get();
        $period = Period::All();
        $taxtitles = Taxtitle::orderBy('id', 'asc')->get();

        $servicefirst = Taxtitle::where('id', '=', $request->personaltype)->first();
        $taxyesas = $request->personalyear;


        //  $businessfirst = Business::where('id','=',$_REQUEST['business_id'])->first();
        //   print_r($_POST);exit;
        $taxtitles1 = Taxtitle::whereIn('id', [7, 8])->orderBy('id', 'asc')->get();
        if ((isset($request->type_of_service) && $request->type_of_service == 'Accounting Service') && $request->acperiod == '') {
            // exit('111');
            $categoryfirst = '';
            $businessfirst = '';

            if (isset($request->acperiod) && $request->acperiod != '') {
                $acp = explode('-', $request->acperiod);
                $acperiods = Period::where('id', '=', $acp[0])->first();
            } else {
                $acperiods = '';
            }
            if ($request->status == '1') {
                // exit('222');
                $clients1 = Commonregister::whereIn('accounting_period', array('10-2', '4-2', '5-2', '7-2'))->orderBy('filename', 'ASC')->get();
                //print_r($clients1);
                $client1 = Commonregister::whereIn('accounting_period', array('10-2', '4-2', '5-2', '7-2'))->count();
            } else {
                $clients1 = Commonregister::where('status', '=', $request->status)->whereIn('accounting_period', array('10-2', '4-2', '5-2', '7-2'))->orderBy('filename', 'ASC')->get();
                //print_r($clients1);
                $client1 = Commonregister::where('status', '=', $request->status)->whereIn('accounting_period', array('10-2', '4-2', '5-2', '7-2'))->count();

            }


        } else if ((isset($request->type_of_service) && $request->type_of_service == 'Accounting Service') && isset($request->acperiod) && $request->acperiod != '') {
            $categoryfirst = '';
            $businessfirst = '';
            if ($request->status != '1') {
                // exit('111');
                if (isset($request->acperiod) && $request->acperiod != '') {
                    $acp = explode('-', $request->acperiod);
                    $acperiods = Period::where('id', '=', $acp[0])->first();
                } else {
                    $acperiods = '';
                }
                $clients1 = Commonregister::where('status', '=', $request->status)->where('accounting_period', '=', $request->acperiod)->orderBy('filename', 'ASC')->get();
                $client1 = Commonregister::where('status', '=', $request->status)->where('accounting_period', '=', $request->acperiod)->count();
            } else {
                if (isset($request->acperiod) && $request->acperiod != '') {
                    $acp = explode('-', $request->acperiod);
                    $acperiods = Period::where('id', '=', $acp[0])->first();
                } else {
                    $acperiods = '';
                }
                //  exit('222');
                $clients1 = Commonregister::where('accounting_period', '=', $request->acperiod)->orderBy('filename', 'ASC')->get();
                $client1 = Commonregister::where('accounting_period', '=', $request->acperiod)->count();

            }
        } else if ((isset($request->type_of_service) && ($request->type_of_service != 'Accounting Service' || $request->type_of_service != 'Payroll Service'))) {

            // exit('222');
            $categoryfirst = '';
            $businessfirst = '';
            if ($request->status != '1') {
                if (isset($request->acperiod) && $request->acperiod != '') {
                    $acp = explode('-', $request->acperiod);
                    $acperiods = Period::where('id', '=', $acp[0])->first();
                } else {
                    $acperiods = '';
                }

                $clients1 = DB::table('commonregisters')->select('taxtitles.id as tids', 'taxtitles.title', 'commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'commonregisters.contactnametype', 'commonregisters.company_name', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                    ->leftJoin('client_to_taxation', function ($join) {
                        $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                    })
                    ->leftJoin('taxtitles', function ($join) {
                        $join->on('taxtitles.id', '=', 'client_to_taxation.taxation_service');
                    })
                    ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->type_of_service)->orderBy('commonregisters.filename', 'ASC')->get();

                $client1 = Commonregister::where('status', '=', $request->status)->where('accounting_period', '=', $request->acperiod)->count();
            } else {
                if (isset($request->acperiod) && $request->acperiod != '') {
                } else {
                    $acperiods = '';
                }


                $clients1 = DB::table('commonregisters')->select('taxtitles.id as tids', 'taxtitles.title', 'commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'commonregisters.contactnametype', 'commonregisters.company_name', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                    ->leftJoin('client_to_taxation', function ($join) {
                        $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                    })
                    ->leftJoin('taxtitles', function ($join) {
                        $join->on('taxtitles.id', '=', 'client_to_taxation.taxation_service');
                    })
                    ->where('client_to_taxation.taxation_service', '=', $request->type_of_service)->orderBy('commonregisters.filename', 'ASC')->get();
                $client1 = Commonregister::where('status', '=', $request->status)->where('accounting_period', '=', $request->acperiod)->count();

                $client1 = Commonregister::where('accounting_period', '=', $request->acperiod)->count();

            }
        } else if ($request->status == '1' && $request->business_id == '' && $request->business_catagory_name == '' && $request->monthlytype == '') {
            if (isset($request->acperiod) && $request->acperiod != '') {
                $acp = explode('-', $request->acperiod);
                $acperiods = Period::where('id', '=', $acp[0])->first();
            } else {
                $acperiods = '';
            }

            $clients1 = Commonregister::orderBy('filename', 'ASC')->get();
            // print_r($clients1);exit;
            $client1 = Commonregister::count();
            $businessfirst = '';
            $categoryfirst = '';


        } else if (($request->status == 'Active' || $request->status == 'Inactive') && $request->business_id == '' && $request->business_catagory_name == '' && $request->monthlytype == '') {
            if (isset($request->acperiod) && $request->acperiod != '') {
                $acp = explode('-', $request->acperiod);
                $acperiods = Period::where('id', '=', $acp[0])->first();
            } else {
                $acperiods = '';
            }

            //   exit('222222222');
            $clients1 = Commonregister::where('status', '=', $request->status)->orderBy('filename', 'ASC')->get();
            // print_r($clients1);exit;
            $client1 = Commonregister::where('status', '=', $request->status)->count();
            $businessfirst = '';
            $categoryfirst = '';


        } else if ($request->status == 'Active') {

            $categoryfirst = Category::where('id', '=', $request->business_catagory_name)->first();

            $businessfirst = Business::where('id', '=', $request->business_id)->first();
            if ($request->business_id == '6') {
                if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();

                    //    $clients1 = Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->orderBy('filename','ASC')->get();
                    //  $client1= Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->count();
                } else if ($request->personaltype != '' && $request->personalyear == '' && $request->monthlytype == '') {
                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                } else if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype == '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();
                } else if ($request->personaltype == '' && $request->personalyear == '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();
                } else {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->count();
                }
            } else {

                if ($request->business_id != '' && $request->business_catagory_name != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->count();
                } else if ($request->business_id != '' && $request->business_catagory_name == '' && $request->monthlytype == '') {
                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->count();
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                } else if ($request->business_id != '' && $request->business_catagory_name != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->count();
                } else if ($request->business_id != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }

                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->count();
                }


            }


        } else if ($request->status == '1') {

            $categoryfirst = Category::where('id', '=', $request->business_catagory_name)->first();

            $businessfirst = Business::where('id', '=', $request->business_id)->first();
            if ($request->business_id == '6') {
                if (isset($request->acperiod) && $request->acperiod != '') {
                    $acp = explode('-', $request->acperiod);
                    $acperiods = Period::where('id', '=', $acp[0])->first();
                } else {
                    $acperiods = '';
                }


                if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype != '') {
                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();

                    //    $clients1 = Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->orderBy('filename','ASC')->get();
                    //  $client1= Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->count();
                } else if ($request->personaltype != '' && $request->personalyear == '' && $request->monthlytype == '') {
                    // exit('212222');
                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();

                } else if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype == '') {
                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();
                } else if ($request->personaltype == '' && $request->personalyear == '' && $request->monthlytype != '') {
                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();
                } else {
                    $clients1 = Commonregister::where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('business_id', '=', $request->business_id)->count();
                }
            } else {

                if ($request->business_id != '' && $request->business_catagory_name != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->count();
                } else if ($request->business_id != '' && $request->business_catagory_name == '' && $request->monthlytype == '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('business_id', '=', $request->business_id)->count();

                } else if ($request->business_id != '' && $request->business_catagory_name != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->count();
                } else if ($request->business_id != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->count();
                }

            }

        } else if ($request->status == 'Inactive') {
            $categoryfirst = Category::where('id', '=', $request->business_catagory_name)->first();

            $businessfirst = Business::where('id', '=', $request->business_id)->first();

            if ($request->business_id == '6') {
                if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();

                    //    $clients1 = Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->orderBy('filename','ASC')->get();
                    //  $client1= Commonregister::where('status','=',$request->status)->where('business_id','=',$request->business_id)->where('service_period','=',$request->monthlytype)->count();
                } else if ($request->personaltype != '' && $request->personalyear == '' && $request->monthlytype == '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();

                } else if ($request->personaltype != '' && $request->personalyear != '' && $request->monthlytype == '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('client_to_taxation.taxyears', '=', $request->personalyear)->where('client_to_taxation.taxation_service', '=', $request->personaltype)->get()->count();
                } else if ($request->personaltype == '' && $request->personalyear == '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears', 'client_to_taxation.taxation_service')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('commonregisters.service_period', '=', $request->monthlytype)->orderBy('commonregisters.filename', 'ASC')->get();

                    $client1 = DB::table('commonregisters')->select('commonregisters.email', 'commonregisters.business_name', 'commonregisters.status', 'commonregisters.business_id', 'commonregisters.service_period', 'commonregisters.id', 'commonregisters.filename', 'commonregisters.first_name', 'commonregisters.middle_name', 'commonregisters.last_name', 'commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.business_no', 'client_to_taxation.clientid', 'client_to_taxation.taxyears')
                        ->leftJoin('client_to_taxation', function ($join) {
                            $join->on('client_to_taxation.clientid', '=', 'commonregisters.id');
                        })
                        ->where('commonregisters.status', '=', $request->status)->where('commonregisters.service_period', '=', $request->monthlytype)->get()->count();
                } else {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->count();
                }
            } else {
                if ($request->business_id != '' && $request->business_catagory_name != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->where('service_period', '=', $request->monthlytype)->count();
                } else if ($request->business_id != '' && $request->business_catagory_name == '' && $request->monthlytype == '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->count();

                } else if ($request->business_id != '' && $request->business_catagory_name != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('business_cat_id', '=', $request->business_catagory_name)->count();
                } else if ($request->business_id != '' && $request->monthlytype != '') {
                    if (isset($request->acperiod) && $request->acperiod != '') {
                        $acp = explode('-', $request->acperiod);
                        $acperiods = Period::where('id', '=', $acp[0])->first();
                    } else {
                        $acperiods = '';
                    }


                    $clients1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->orderBy('filename', 'ASC')->get();
                    $client1 = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->business_id)->where('service_period', '=', $request->monthlytype)->count();
                }

            }

        }

        return view('fscemployee.empclientreport/empclientreport', compact(['employee', 'acperiods', 'taxtitles', 'taxyesas', 'servicefirst', 'taxtitles1', 'categoryfirst', 'business', 'businessfirst', 'category', 'clients1', 'client1', 'logo', 'period']));


    }

}