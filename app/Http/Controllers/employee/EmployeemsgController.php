<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\Msg;
use Auth;
use DB;
use Illuminate\Http\Request;

class EmployeemsgController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Auth::user()->id;
        // $emp = Fscemployee::All();
        $admin = Admin::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $employee1 = Employee::All();
        $clients = Commonregister::All();


        $task = Msg::where('send2', '=', '1')->where('employee_id', '=', $user_id)->orderBy('id', 'DESC')->get();
        $logo = Logo::where('id', '=', 1)->first();
        $silver = DB::table("commonregisters")
            ->select("commonregisters.first_name", "commonregisters.middle_name", "commonregisters.last_name", "commonregisters.address", "commonregisters.city", "commonregisters.stateId", "commonregisters.countryId"
                , "commonregisters.business_no", "commonregisters.email", "commonregisters.status", "commonregisters.id")->where('commonregisters.status', '=', "Active");

        $emp = DB::table("employees")->select("employees.firstName", "employees.middleName", "employees.lastName", "employees.address1", "employees.city", "employees.stateId", "employees.countryId"
            , "employees.telephoneNo1", "employees.email", "employees.type", "employees.id")->where('employees.check', '=', "1")->unionAll($silver)->get();
        return view('fscemployee/getmsg/getmsg', compact(['clients', 'task', 'emp', 'admin', 'employee', 'employee1', 'logo']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->where('check', '=', '1')->first();
        return view('fscemployee/getmsg/create', compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $start)->first();
        $type = $employee->type;
        $position = new Msg;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->day = $request->day;
        $position->status = 1;
        $position->status3 = 'All';

        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $start;
        $position->type = $type;
        $position->rlt_msg = $request->rlt_msg;
        $position->call_back = $request->call_back;
        $position->busname = $request->busname;
        $position->clientfile = $request->clientfile;
        $position->clientno = $request->clientno;
        $position->clientname = $request->clientname;
        $position->purpose = $request->purpose;
        $position->send2 = 1;
        $position->busname2 = $request->busname2;
        $position->save();
        return redirect('fscemployee/getmsg')->with('success', 'Success fully add Message');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purpose = DB::table('purposes')->get();
        $task = Msg::where('id', $id)->first();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $employee1 = Employee::all();
        $emp = Fscemployee::All();
        $admin = Admin::All();
        return View('fscemployee.getmsg.edit', compact(['task', 'emp', 'admin', 'employee', 'employee1', 'purpose']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'employee' => 'required',
        ]);
        $position = Msg::find($id);
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $employee;
        $position->type = $request->type;
        $position->rlt_msg = $request->rlt_msg;
        $position->call_back = $request->call_back;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->busname = $request->busname;
        $position->clientfile = $request->clientfile;
        $position->clientno = $request->clientno;
        $position->clientname = $request->clientname;
        $position->purpose = $request->purpose;
        $position->update();
        return redirect('fac-Bhavesh-0554/msg')->with('success', 'Success fully update Message');
    }

    public function getmessages(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        if ($request->id == 'employee') {
            $data = Employee::select('firstName', 'id', 'middleName', 'lastName', 'type', 'employee_id', 'telephoneNo1')->where('check', '=', '1')->Where('type', $request->id)->take(1000)->get();
        } else if (($request->id) and ($request->state == 'employee')) {
            $data = Employee::select('firstName', 'id', 'middleName', 'lastName', 'type', 'employee_id', 'telephoneNo1')->where('check', '=', '1')->where('id', $request->id)->take(1000)->get();
        } else {

            $data = Commonregister::select('firstname', 'lastname', 'filename', 'company_name', 'first_name', 'id', 'middle_name', 'last_name', 'business_no', 'status', 'filename', 'email', 'business_name', 'business_id')->where('status', $request->id)->orWhere('id', $request->id)->take(1000)->get();
        }
        return response()->json($data);
    }

    public function getmessage12(Request $request)
    {
        $use = Commonregister::where('id', '=', $request->id)->first();
        $data = Commonregister::select('first_name', 'id', 'middle_name', 'company_name', 'business_name', 'last_name', 'business_no', 'status', 'email', 'filename', 'business_name', 'business_id')->Where('email', '=', $use->email)->take(1000)->get();
        return response()->json($data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Msg::where('id', $id)->delete();
        return redirect(route('getmsg.index'));
    }
}