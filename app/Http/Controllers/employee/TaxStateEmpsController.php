<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\taxstate;
use Auth;
use DB;
use Illuminate\Http\Request;

class TaxStateEmpsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = taxstate::All();
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/empsstates/empsstates', compact(['price', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = taxstate::All();
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/empsstates/create', compact(['position', 'employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'countycode' => 'required',
            'county' => 'required',
            'state' => 'required',
        ]);
        $position = new taxstate;

        $position->countycode = $request->countycode;
        $position->county = $request->county;
        $position->state = $request->state;
        $position->type = $request->type;
        $position->rate = $request->rate;
        $position->save();
        return redirect('fscemployee/empsstates')->with('success', 'Success fully add County');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $employee = Employee::where('id', $user_id)->first();
        $state = taxstate::where('id', $id)->first();
        return View('fscemployee.empsstates.edit', compact(['state', 'employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'countycode' => 'required',
            'county' => 'required',
            'state' => 'required',
        ]);
        $position = taxstate::find($id);
        $position->countycode = $request->countycode;
        $position->county = $request->county;
        $position->state = $request->state;
        $position->type = $request->type;
        $position->rate = $request->rate;
        $position->update();
        return redirect('fscemployee/empsstates')->with('success', 'Success fully update state');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        taxstate::where('id', $id)->delete();;
        return redirect(route('empsstates.index'));
    }
}