<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Language;
use App\Model\Position;
use App\Model\Rules;
use App\Model\Super;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Validator;
use View;

class ProController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // exit('2222');

        $super = Super::All();
        $user = Fscemployee::All();
        $language = Language::orderBy('language_name', 'asc')->get();

        $user_id = Auth::user()->user_id;
        $reviewinfo = DB::table('emp_review')->where('emp_id', $user_id)->get();
        $whinfo = DB::table('emp_wh')->where('emp_id', $user_id)->get();
        $whinfo2 = DB::table('emp_wh')->where('emp_id', $user_id)->get();


        $employee = Employee::where('id', $user_id)->first();
        $emp = Employee::where('id', $user_id)->first();
        $super1 = Employee::where('super', '=', '1')->get();
        $position = Position::orderBy('position', 'asc')->get();
        $rules = Rules::where('type', '=', 'Rules')->get();
        $resposibilty = Rules::where('type', '=', 'Resposibilty')->orderBy('id', 'desc')->get();
        $branch = DB::table('branches')->select('city')->groupBy('city')->get();
        $info1 = DB::table('employee_pay_info')->where('employee_id', $user_id)->first();
        $info = DB::table('employee_pay_info')->where('employee_id', $user_id)->get();
        // print_r($info);exit;
        //  echo count($info);exit;
        $info3 = DB::table('employee_other_info')->where('employee_id', $user_id)->get();
        $info2 = DB::table('employee_other_info')->where('employee_id', $user_id)->first();
        $review1 = DB::table('employee_review')->where('employee_id', $user_id)->get();
        $review = DB::table('employee_review')->where('employee_id', $user_id)->first();
        return view('fscemployee/employeeprofile', compact(['whinfo', 'whinfo2', 'reviewinfo', 'language', 'super1', 'super', 'resposibilty', 'employee', 'rules', 'emp', 'info1', 'info', 'info2', 'info3', 'review1', 'review', 'position', 'branch']));
    }

    public function getemp1(Request $request)
    {

        $user = DB::table('employees')->where('employee_id', Input::get('employee_id'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }

        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    public function checkemailAvailability(Request $request)
    {

        $user = DB::table('employees')->where('email', Input::get('email'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }

        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // echo  $ids=Auth::user()->id;
        //exit;
        $pay_method = $request->pay_method;

        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee = $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;
        $i = 0;
        $j = 0;
        $k = 0;
        DB::table('employee_pay_info')->where('employee_id', $id)->first();
        DB::table('employee_pay_info')->where('employee_id', $id)->delete();

        if (!empty($pay_scale)) {

            foreach ($pay_scale as $post) {
                $pay_method1 = $pay_method[$i];
                $pay_frequency1 = $pay_frequency[$i];
                $pay_scale1 = $pay_scale[$i];
                $effective_date1 = date('Y-m-d', strtotime($effective_date[$i]));
                $fields1 = $fields[$i];
                $employee1 = $employee[$i];
                $i++;
//if(empty($employee1))
//{
                $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) 
values('" . $id . "','" . $pay_method1 . "','" . $pay_frequency1 . "','" . $post . "','" . $effective_date1 . "','" . $fields1 . "')");

//}
            }
        }

//------------------------------//

        $review_date = $request->review_date;
        $reviewday = $request->review_day;
        $next_review_date = $request->next_review_date;
        $review_rate = $request->review_rate;
        $review_comments = $request->review_comments;


        DB::table('emp_review')->where('emp_id', $id)->delete();

        if ($review_date) {
            foreach ($review_date as $post) {
                $review_date1 = date('Y-m-d', strtotime($post));
                $reviewday1 = $reviewday[$k];
                $next_review_date1 = date('Y-m-d', strtotime($next_review_date[$k]));
                $review_rate1 = $review_rate[$k];
                $review_comments1 = $review_comments[$k];

                $k++;

                $insert2 = DB::insert("insert into emp_review(`emp_id`,`review_date`,`review_day`,`next_review_date`,`review_rate`,`review_comments`)
    values('" . $id . "','" . $review_date1 . "','" . $reviewday1 . "','" . $next_review_date1 . "','" . $review_rate1 . "','" . $review_comments1 . "')");
                //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
        }

//------------------------------//


//------------------------------//

        $year_wh = $request->year_wh;
        $fillingstatus = $request->fillingstatus;
        $fed_claim = $request->fed_claim;
        $fed_wh = $request->fed_wh;

        $st_claim = $request->st_claim;
        $st_wh = $request->st_wh;

        $local_claim = $request->local_claims;
        $local_wh = $request->local_wh;

        DB::table('emp_wh')->where('emp_id', $id)->delete();
        $k = 0;
        if ($year_wh) {
            foreach ($year_wh as $post) {
                $year_wh1 = $post;
                $fillingstatus1 = $fillingstatus[$k];
                $fed_claim1 = $fed_claim[$k];
                $fed_wh1 = $fed_wh[$k];
                $st_claim1 = $st_claim[$k];
                $st_wh1 = $st_wh[$k];
                $local_claim1 = $local_claim[$k];
                $local_wh1 = $local_wh[$k];

                $k++;

                $insert2 = DB::insert("insert into emp_wh(`emp_id`,`year_wh`,`fillingstatus`,`fed_claim`,`fed_wh`,`st_claim`,`st_wh`,`local_claims`,`local_wh`)
    values('" . $id . "','" . $year_wh1 . "','" . $fillingstatus1 . "','" . $fed_claim1 . "','" . $fed_wh1 . "','" . $st_claim1 . "','" . $st_wh1 . "','" . $local_claim1 . "','" . $local_wh1 . "')");
                //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
        }
        // echo $id;exit;
        //  echo "<pre>";print_r($_POST);exit;
        $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'pfid1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'pfid2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('photo')) {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        } else {
            $filname = $request->photo1;
        }
        if ($request->hasFile('pfid1')) {
            $filname1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/employeeProof1', $filname1);
        } else {
            $filname1 = $request->pfid12;
        }
        if ($request->hasFile('pfid2')) {
            $filname2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/employeeProof2', $filname2);
        } else {
            $filname2 = $request->pfid22;
        }

        if ($request->hasFile('additional_attach')) {
            $additional_att = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/additional_attach', $additional_att);
        } else {
            $additional_att = $request->additional_attach1;
        }
        if ($request->hasFile('additional_attach_1')) {
            $additional_att_1 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/additional_attach1', $additional_att_1);
        } else {
            $additional_att_1 = $request->additional_attach2;
        }
        if ($request->hasFile('additional_attach_2')) {
            $additional_att_2 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/additional_attach2', $additional_att_2);
        } else {
            $additional_att_2 = $request->additional_attach3;
        }

        if ($request->hasFile('agreement')) {
            $agreement1 = $request->agreement->getClientOriginalName();
            $request->agreement->move('public/agreement', $agreement1);
        } else {
            $agreement1 = $request->agreement_1;
        }

        if ($request->hasFile('resume')) {
            $resume1 = $request->resume->getClientOriginalName();
            $request->resume->move('public/resumes', $resume1);
        } else {
            $resume1 = $request->resume_1;
        }
        if ($request->terms == '2') {
            $rules = '2';
        } else {
            $rules = '1';
        }
//return $id;
        $user_id = Auth::user()->user_id;
        $resetdays = $request->resetdays;
        $enddate = date('Y-m-d', strtotime("+$resetdays days"));
        $startdate = date('Y-m-d');
        $employee = Employee::find($user_id);
        $employee->employee_id = $request->employee_id;
        $employee->read = $request->terms;
        $employee->rulesdate = $request->rulesdate;
        $employee->firstName = $request->firstName;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->termimonth = $request->termimonth;
        $employee->hiremonth = $request->hiremonth;
        $employee->hireday = $request->hireday;
        $employee->hireyear = $request->hireyear;
        // $employee->password = bcrypt($request->newpassword);
        // $employee->newpassword = $request->newpassword;
        $employee->termiday = $request->termiday;
        $employee->termiyear = $request->termiyear;
        $employee->tnote = $request->tnote;
        $employee->rehiremonth = $request->rehiremonth;
        $employee->rehireday = $request->rehireday;
        $employee->rehireyear = $request->rehireyear;
        $employee->branch_city = $request->branch_city;
        $employee->branch_name = $request->branch_name;
        $employee->position = $request->position;
        $employee->note = $request->note;
        // $employee->additional_attach=$additional_att;
        // $employee->additional_attach_1=$additional_att_1;
        //  $employee->additional_attach_1=$additional_att_2;
        //   $employee->pay_method= $request->pay_method;
        //  $employee->pay_frequency= $request->pay_frequency;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        // $employee->pfid1= $filname1;
        $employee->pf2 = $request->pf2;
        $employee->nametype = $request->nametype;
        //  $employee->pfid2= $filname2;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->uname = $request->uname;
        //$employee->password= $request->password;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->languages = implode(',', $request->languages);

        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        //   $employee->check= $request->check;
        $employee->filling_status = $request->filling_status;
        $employee->fedral_claim = $request->fedral_claim;
        $employee->additional_withholding = $request->additional_withholding;
        $employee->state_claim = $request->state_claim;
        $employee->additional_withholding_1 = $request->additional_withholding_1;
        $employee->local_claim = $request->local_claim;
        $employee->additional_withholding_2 = $request->additional_withholding_2;
        $employee->resume = $resume1;
        $employee->type_agreement = $request->type_agreement;
        $employee->firstName_1 = $request->firstName_1;
        $employee->agreement = $agreement1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->super = $request->super;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;


        //$employee->work_responsibility= $request->work_responsibility;
        $employee->employee_rules = $request->employee_rules;
        $employee->eemail = $request->eemail;


        $employee->update();


        return redirect('fscemployee/employeeprofile?tab=0')->with('success', 'Your Record Updated Successfully');

//$business->update();
//$employee->update();

    }

    public function getbranch1(Request $request)
    {
        $data = Branch::select('branchname', 'city')->where('city', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function getemployeepassword(Request $request)
    {

        $data = $request->all();
        $user = Fscemployee::find(auth()->user()->id);
        if (!Hash::check($data['oldpassword'], $user->password)) {
            $isAvailable = false;
        } else {
            $isAvailable = true;
        }
        echo json_encode(
            array('valid' => $isAvailable));
    }

    public function responsibility($id, Request $request)
    {
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $position = Rules::where('id', $id)->first();
        return view('fscemployee/responsibility', compact(['position', 'employee']));
    }

    public function responupdate($id, Request $request)
    {

        $position = Rules::find($id);
        $position->status = $request->status;
        $position->responupdate();
        return view('fscemployee/employeeprofile', compact(['position']));
    }


    public function profilephoto(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('photo_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'photo' => 'image',
                ],
                [
                    'photo.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('photo')->getClientOriginalExtension();
            $dir = 'public/employeeimage/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('photo')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['photo' => $filename]);
            return $filename;
        }
    }


    public function additional1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file' => 'image',
                ],
                [
                    'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_2' => $filename]);
            return $filename;
        }
    }


    public function additional2(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload1');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_1' => 'image',
                ],
                [
                    'file_1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_1')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_1' => $filename]);
            return $filename;
        }
    }


    public function additional3(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_2' => 'image',
                ],
                [
                    'file_2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_2')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach' => $filename]);
            return $filename;
        }
    }


    public function pfidd1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid1' => 'image',
                ],
                [
                    'pfid1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid1')->getClientOriginalExtension();
            $dir = 'public/employeeProof1/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid1' => $filename]);
            return $filename;
        }
    }


    public function pfidd(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid2' => 'image',
                ],
                [
                    'pfid2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid2')->getClientOriginalExtension();
            $dir = 'public/employeeProof2/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid2' => $filename]);
            return $filename;
        }
    }

    public function reviewdelete11($id)
    {
        $logoStatus = DB::table('employee_review')->where('id', '=', $id)->delete();
        return redirect('fscemployee/employeeprofile?tab=0')->with('error', 'Your Record Deleted Successfully');
    }

    public function paydelete11($id)
    {
        $logoStatus = DB::table('employee_pay_info')->where('id', '=', $id)->delete();
        return redirect('fscemployee/employeeprofile?tab=0')->with('error', 'Your Record Deleted Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();
        return redirect(route('employee.index'))->with('success', 'Success Fully Delete Record');
    }

}