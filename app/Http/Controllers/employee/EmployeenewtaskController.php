<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Employee;
use App\Model\Task;
use Auth;
use Illuminate\Http\Request;

class EmployeenewtaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Auth::user()->user_id;
        $emp = Fscemployee::All();
        $task = Task::where('employeeid', '=', $start)->get();
        $admin = Admin::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        return view('fscemployee/newtask/newtask', compact(['task', 'emp', 'admin', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $emp1 = Employee::where('type', '=', 'employee')->where('check', '=', '1')->get();
        return view('fscemployee/newtask/create', compact(['emp', 'employee', 'emp1']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'employee' => 'required',
        ]);
        $position = new Task;
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $request->employee;
        $position->duedate = $request->duedate;
        $position->priority = $request->priority;
        $position->save();
        return redirect('fscemployee/newtask')->with('success', 'Success fully add Task');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp1 = Employee::where('type', '=', 'employee')->where('check', '=', '1')->get();
        $task = Task::where('id', $id)->first();
        $countries = Task::where('id', $id)->update(['checked' => '1']);
        $emp = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();

        return View('fscemployee.newtask.edit', compact(['task', 'emp', 'employee', 'emp1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required'

        ]);

        $position = Task::find($id);
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $request->employee;
        $position->comment = $request->comment;
        $position->status = $request->status;
        $position->duedate = $request->duedate;
        $position->priority = $request->priority;
        $position->update();
        return redirect('fscemployee/newtask')->with('success', 'Success fully update Task');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Task::where('id', $id)->delete();
        return redirect(route('fscemployee.index'));
    }
}