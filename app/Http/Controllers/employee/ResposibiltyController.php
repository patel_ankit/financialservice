<?php

namespace App\Http\Controllers\employee;

use App\employee\Leave;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Rules;
use Auth;
use DB;
use Illuminate\Http\Request;

class ResposibiltyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leave = Leave::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $rules = Rules::where('employee_id', '=', $employee->id)->get();
        return view('fscemployee/resposibilty/resposibilty', compact(['rules', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/leave/create', compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'total_days' => 'required',

        ]);

        $branch = new Leave;
        $branch->start_date = $request->start_date;
        $branch->total_days = $request->total_days;
        $branch->end_date = $request->end_date;
        $branch->leave_reason = $request->leave_reason;
        $branch->employee_id = $request->employee_id;
        $branch->creation_date = date('Y-m-d');
        $branch->save();
        return redirect('fscemployee/leave')->with('success', 'Success fully add Leave');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $homecontent = Rules::where('id', $id)->first();
        return view('fscemployee.resposibilty.edit', compact(['homecontent', 'employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [

        ]);
        $position = Rules::find($id);
        $position->status = $request->checked;
        if ($request->checked == 2) {
            $position->rulesdate = $request->rulesdate;
        }
        $position->update();
        return redirect('fscemployee/employeeprofile?tab=1')->with('success', 'Success fully update Resposibilty');
        //return redirect('fscemployee/leave')->with('success','Success fully Update Leave');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect(route('addressbook.index'));

    }
}