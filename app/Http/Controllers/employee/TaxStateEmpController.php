<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\FilingFrequency;
use App\Model\PaymentFrequency;
use App\Model\taxstatesses;
use Auth;
use DB;
use Illuminate\Http\Request;

class TaxStateEmpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $price = taxstatesses::All();
        //$affectedRows = taxstate::where('city','=',Null)->update(array('type' => 'County'));
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/empstates/empstates', compact(['price', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $employee = Employee::where('id', $user_id)->first();
        $position = taxstatesses::All();
        $entity = DB::table('typeofentity')->get();
        return view('fscemployee/empstates/create', compact(['position', 'employee', 'entity']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'countycode' => 'required',
            'county' => 'required',
            'state' => 'required',
        ]);
        $position = new taxstatesses;
        $position->countycode = $request->countycode;
        $position->county = $request->county;
        $position->state = $request->state;
        $position->type = $request->type;
        $position->rate = $request->rate;
        $position->save();
        return redirect('fscemployee/empstates')->with('success', 'Success fully add County');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */

    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bussiness = taxstatesses::where('id', $id)->first();
        $user_id = Auth::user()->user_id;
        $uid = Auth::user()->id;
        $entity = DB::table('typeofentity')->get();
        $employee = Employee::where('id', $user_id)->first();
        $type = $bussiness->uniques;
        $fill = FilingFrequency::where('type', $type)->get();
        $pay = PaymentFrequency::where('type', $type)->get();
        return View('fscemployee.empstates.edit', compact(['bussiness', 'employee', 'entity', 'pay', 'fill']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'countycode' => 'required',
            'county' => 'required',
            'state' => 'required',
        ]);
        $position = taxstatesses::find($id);
        $position->countycode = $request->countycode;
        $position->county = $request->county;
        $position->state = $request->state;
        $position->type = $request->type;
        $position->rate = $request->rate;
        $position->update();
        return redirect('fscemployee/empstates')->with('success', 'Success fully update state');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        taxstate::where('id', $id)->delete();;
        return redirect(route('empstates.index'));
    }
}