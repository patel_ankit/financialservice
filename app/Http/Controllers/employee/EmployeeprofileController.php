<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Language;
use App\Model\Position;
use App\Model\Rules;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use View;

class EmployeeprofileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // exit('111');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //exit('22222222222222222222');

        $language = Language::orderBy('language_name', 'asc')->get();
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;

        $rules = Rules::All();
        $emp = Employee::where('id', $user_id)->first();
        $employee = Employee::where('id', $user_id)->first();
        $position = Position::All();
        //$branch = Branch::All();
        $branch = DB::table('branches')->select('city')->groupBy('city')->get();
        $info1 = DB::table('employee_pay_info')->where('employee_id', $user_id)->first();
        $info = DB::table('employee_pay_info')->where('employee_id', $user_id)->get();
        $info3 = DB::table('employee_other_info')->where('employee_id', $user_id)->get();
        $info2 = DB::table('employee_other_info')->where('employee_id', $user_id)->first();
        $review1 = DB::table('employee_review')->where('employee_id', $user_id)->get();
        $review = DB::table('employee_review')->where('employee_id', $user_id)->first();
        return view('fscemployee.empprofile.edit', compact(['language', 'employee', 'emp', 'info1', 'info', 'info2', 'info3', 'review1', 'review', 'position', 'branch', 'rules']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, []);
        if ($request->hasFile('photo')) {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        } else {
            $filname = $request->photo1;
        }
        if ($request->hasFile('pfid1')) {
            $filname1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/employeeProof1', $filname1);
        } else {
            $filname1 = $request->pfid12;
        }
        if ($request->hasFile('pfid2')) {
            $filname2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/employeeProof2', $filname2);
        } else {
            $filname2 = $request->pfid22;
        }

        if ($request->hasFile('additional_attach')) {
            $additional_att = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/additional_attach', $additional_att);
        } else {
            $additional_att = $request->additional_attach1;
        }
        if ($request->hasFile('additional_attach_1')) {
            $additional_att_1 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/additional_attach1', $additional_att_1);
        } else {
            $additional_att_1 = $request->additional_attach2;
        }
        if ($request->hasFile('additional_attach_2')) {
            $additional_att_2 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/additional_attach2', $additional_att_2);
        } else {
            $additional_att_2 = $request->additional_attach3;
        }

        if ($request->hasFile('agreement')) {
            $agreement1 = $request->agreement->getClientOriginalName();
            $request->agreement->move('public/agreement', $agreement1);
        } else {
            $agreement1 = $request->agreement_1;
        }

        if ($request->hasFile('resume')) {
            $resume1 = $request->resume->getClientOriginalName();
            $request->resume->move('public/resumes', $resume1);
        } else {
            $resume1 = $request->resume_1;
        }
        $business = Fscemployee::find($id);
        $user_id = Auth::user()->user_id;
        $resetdays = $request->resetdays;
        $enddate = date('Y-m-d', strtotime("+$resetdays days"));
        $startdate = date('Y-m-d');
        $business->question1 = $request->question1;
        $business->question2 = $request->question2;
        $business->question3 = $request->question3;
        $business->answer1 = $request->answer1;
        $business->answer2 = $request->answer2;
        $business->answer3 = $request->answer3;
        $business->active = '1';
        $business->resetdays = $request->resetdays;
        $business->remaining_day = $request->resetdays;
        $business->startdate = $startdate;
        // $business->enddate =  $request->reset_date;
        $business->password = bcrypt($request->newpassword);
        $business->newpassword = $request->newpassword;
        $employee = Employee::find($user_id);
        $employee->firstName = $request->firstName;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        $employee->pfid1 = $filname1;
        $employee->pf2 = $request->pf2;
        $employee->pfid2 = $filname2;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        $employee->firstName_1 = $request->firstName_1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;
        $employee->update();
        $fields = $request->fields;
        $employee = $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;
        $i = 0;
        $j = 0;
        $k = 0;
        $first_rev_day = $request->first_rev_day;
        $reviewmonth = $request->reviewmonth;
        $hiring_comments = $request->hiring_comments;
        $ree = $request->ree;
        $business->update();
        return redirect('fscemployee/home')->with('success', 'Your Profile Successfully Update');
    }

    public function getBranches(Request $request)
    {
        $data = Branch::select('branchname', 'city')->where('city', $request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getemployeepassword(Request $request)
    {

        $data = $request->all();
        $user = Fscemployee::find(auth()->user()->id);
        if (!Hash::check($data['oldpassword'], $user->password)) {
            $isAvailable = false;
        } else {
            $isAvailable = true;
        }
        echo json_encode(
            array('valid' => $isAvailable));
    }
}