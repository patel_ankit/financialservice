<?php

namespace App\Http\Controllers\employee;

use App\employee\Leave;
use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Clienttechnical;
use App\Model\Employee;
use Auth;
use DB;
use Illuminate\Http\Request;

class Clienttechnicalsupport extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leave = Leave::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $rules = Clienttechnical::All();
        return view('fscemployee/clientsupports/clientsupports', compact(['rules', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $tech = Fscemployee::where('id', '30')->first();
        return view('fscemployee/clientsupports/create', compact(['employee', 'tech']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'subject' => 'required',

        ]);
        $user_id = Auth::user()->user_id;

        if ($request->hasFile('attachment')) {
            $filname = $request->attachment->getClientOriginalName();
            $request->attachment->move('public/attachment', $filname);
        } else {
            $filname = '';
        }
        $branch = new Clienttechnical;
        $branch->to_supporter = $request->to;
        $branch->subject = $request->subject;
        $branch->details = $request->details;
        //$branch->attachment =  $filname;
        $branch->client_id = $user_id;
        $branch->date = $request->date;
        $branch->time = $request->time;
        $branch->day = $request->day;


        $branch->save();
        return redirect('fscemployee/clientsupports')->with('success', 'Success fully add Technicalsupport');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $homecontent = Clienttechnical::where('id', $id)->first();
        $tech = Fscemployee::where('id', '30')->first();
        $employee1 = Employee::get();
        return view('fscemployee.clientsupports.edit', compact(['homecontent', 'employee', 'tech', 'employee1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [

        ]);

        $user_id = Auth::user()->user_id;

        if ($request->hasFile('attachment')) {
            $filname = $request->attachment->getClientOriginalName();
            $request->attachment->move('public/attachment', $filname);
        } else {
            $filname = '';
        }
        $branch = Clienttechnical::find($id);
        $branch->to_supporter = $request->to;
        $branch->subject = $request->subject;
        $branch->details = $request->details;
        $branch->answer = $request->answer;
        //$branch->attachment =  $filname;
        // $branch->client_id = $user_id;
        $branch->date = $request->date;
        $branch->time = $request->time;
        $branch->day = $request->day;
        $branch->update();
        return redirect('fscemployee/clientsupports')->with('success', 'Success fully update Technicalsupport');
        //return redirect('fscemployee/leave')->with('success','Success fully Update Leave');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Clienttechnical::where('id', $id)->delete();
        return redirect(route('clientsupports.index'));

    }
}