<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\Submission;
use App\Model\Super;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class SubmissionRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submission = Submission::All();
        $logo = Logo::where('id', '=', 1)->first();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        return view('fscemployee/submissionrequest/submissionrequest', compact(['submission', 'employee', 'logo']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Employee::where('super', '=', '1')->orderby('firstName', 'asc')->get();
        return view('fscemployee/supervisor/create', compact(['emp']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'supervisorcode' => 'required',
            'supervisorname' => 'required',
        ]);
        $position = new Super;
        $position->username = $request->supervisorname;
        $position->code = $request->supervisorcode;
        $position->save();
        return redirect('fscemployee/supervisor')->with('success', 'Supervisor Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Super::where('id', $id)->first();
        $emp = Employee::where('super', '=', '1')->orderby('firstName', 'asc')->get();
        return View('fscemployee.supervisor.edit', compact(['task', 'emp']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Super::find($id);
        $position->username = $request->supervisorname;
        $position->code = $request->supervisorcode;
        $position->update();
        return redirect('fscemployee/supervisor')->with('success', 'Supervisor Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Super::where('id', $id)->delete();
        return redirect(route('supervisor.index'));
    }
}