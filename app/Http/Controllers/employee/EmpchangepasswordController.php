<?php

namespace App\Http\Controllers\employee;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Validator;
use View;

class EmpchangepasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Auth::user()->lock;
        $start1 = strtotime($start . "+30 minutes");
        $end1 = strtotime($start);
        $ck = $start1 - $end1;
        $currentdate = strtotime(date('Y-m-d H:i:s'));
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        if ($currentdate >= $start1) {
            $returnValue = DB::table('fscemployee')->where('flag', '=', '3')->update(['flag' => '0']);
        }
        return view('fscemployee/empchangepassword', compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $user = Fscemployee::where('id', $id)->first();
        return View('fscemployee/empchangepassword/empchangepassword', compact(['user', 'employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = ['oldpassword' => 'required|password',
            'newpassword' => 'required|min:8|different:oldpassword',
            'cpassword' => 'required|same:newpassword',];
        Fscemployee::find(auth()->user()->password);
        // custom rule for 'password'
        Validator::extend('password', function ($attribute, $value, $parameters) {
            $user = Fscemployee::find(auth()->user()->id);
            $pass = $user->password;
            // compare the entered password with what the database has, e.g. validates the current password
            return Hash::check($value, $pass);
        });

        // custom message if validation for password fails
        $messages = ['password' => 'Your current password does not match our records.'];

        // validate input with rules, adding in custom messages
        $validation = Validator::make(Input::all(), $rules, $messages);

        // if validation fails, redirect back to previous page
        if ($validation->fails()) {
            $business = User::find($id);
            $business->flag = $request->flag;
            $business->lock = date('Y-m-d H:i:s');
            $business->update();
            return Redirect::back()->withInput()->withErrors($validation->messages());
        } else {
            $resetdays = $request->resetdays;
            $startdate = date('Y-m-d');
            $enddate = date('Y-m-d', strtotime("+$resetdays days"));
            $business = Fscemployee::find($id);
            $business->flag = '0';
            $business->password = bcrypt($request->newpassword);
            $business->newpassword = $request->newpassword;
            $business->resetdays = $request->resetdays;
            $business->remaining_day = $request->resetdays;
            $business->startdate = $startdate;
            $business->enddate = $enddate;
            $business->update();
            return redirect('fscemployee/empchangepassword')->with('success', 'Your Password Successfully changed');
        }
        /*$this->validate($request,[
                   'oldpassword'=>'required',
                   'password'=>'required|min:6',
                   'cpassword'=>'required|same:password',

               ]);
               $data = $request->all();
                  $user = Fscemployee::find(auth()->user()->id);

                   $business =Fscemployee::find($id);
                   $business->password = Hash::make($request->newpassword);
                   $business->update();
                   return redirect('fscemployee/')->with('success', 'Your Password Successfully changed');*/

    }

    public function getpassword12(Request $request)
    {

        $data = $request->all();
        $user = Fscemployee::find(auth()->user()->id);
        if (!Hash::check($data['oldpassword'], $user->password)) {
            $isAvailable = false;
        } else {
            $isAvailable = true;
        }
        echo json_encode(
            array('valid' => $isAvailable));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}