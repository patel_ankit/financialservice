<?php

namespace App\Http\Controllers\employee;

use App\employee\ckemp;
use App\employee\Empschedule;
use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Employee;
use App\Model\Msg;
use App\Model\Notesrelated;
use App\Model\Rules;
use App\Model\Schedule;
use App\Model\Super;
use App\Model\Task;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Validator;
use View;

class EmploController extends Controller
{
    /**
     * Create a new controller instance.
     *e
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');

    }

    public function index()
    {
        // exit('222');
        $useremp = Fscemployee::All();
        $employees = Employee::All();
        $commonregisterall = Commonregister::All();
        $admin = Admin::All();

        //    $howtodostep = DB::table('howtodos as t1')->select('t1.*','t2.id as ids,t2.howtodo_id,t2.steps')
        //->Join('howtodos_steps as t2', function($join){ $join->on('t1.id', '=','t2.howtodo_id');})
        //->where('t1.employeeuserid','=',$id)
        //  ->get();

        $user_id = Auth::user()->user_id;
        $id = Auth::user()->id;

        $msg2 = Msg::where('send', '=', 1)->where('employeeid', '=', $user_id)->where('status', '=', 2)->where('status1', '=', '')->count();

        $rulescount = Rules::where('employee_id', '=', $user_id)->where('type', '=', 'Resposibilty')->where('status', '!=', 2)->count();
        $respdaily = Rules::where('employee_id', '=', $user_id)->where('type', '=', 'Resposibilty')->where('respon_type', '=', 'Daily')
            ->where('status', '!=', 2)->where('flag', '=', 0)->get();

        //   $emp22 = DB::table('rules')->select('rules.super','employees.firstName','employees.middleName','employees.lastName','employees.id','super.username')
        //    ->leftJoin('super', function($join){ $join->on('employees.id', '=', 'super.username');})
        //    ->where('employees.super', '=', '1')->orderBy('employees.firstName', 'asc')->get();


        $rules = Rules::where('employee_id', '=', $user_id)->where('status', '=', 1)->count();
        //return  $rules = DB::table('rules')->select('status', DB::raw('count(*) as rulestotal'))->where('employee_id','=',$user_id)->where('status','=',1)->groupBy('status')->get(); 
        $taskall = Task::where('employeeid', '=', $user_id)->where('status', '=', 0)->where('checked', '=', 0)->orderBy('created_at', 'desc')->get();

        $tk = DB::table('tasks')->select('checked', DB::raw('count(*) as emptotal'))->where('employeeid', '=', $id)->groupBy('checked')->get()->first();
        //$msg = DB::table('msgs')->select('status', DB::raw('count(*) as msgtotal'))->where('admin_id','=',$user_id)->where('status','=',1)->groupBy('status')->get();

        $tknew = Task::where('employeeid', '=', $user_id)->where('checked', '=', 1)->count();
        //echo $user_id;die;
        //print_r($tknew);die;

        $msg = DB::table('msgs')->select('recieve', DB::raw('count(*) as msgtotal'))->where('employeeid', '=', $user_id)->where('recieve', '=', 1)->groupBy('recieve')->get();
        //print_r($msg);die;
        //DB::table('msgs')->select('status', DB::raw('count(*) as msgtotal'))->whereRaw("find_in_set('".$user_id."',msgs.admin_id)")->groupBy('status')->first();
        $employee = Employee::where('id', '=', $user_id)->first();
        $schedule = Schedule::where('emp_name', '=', $user_id)->first();
        // echo "<pre>"; print_r($schedule);
        // echo $schedule->id;
        $scheduleemployee = DB::table('employees')->select('employees.id', 'employees.employee_id', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'employees.status',
            'schedules.type', 'schedules.emp_name', 'schedules.schedule_in_time', 'schedules.schedule_out_time')
            ->Join('schedules', function ($join) {
                $join->on('schedules.emp_name', '=', 'employees.id');
            })
            ->where('employees.status', '=', '1')->where('employees.type', '=', 'employee')->where('employees.id', '=', $user_id)->groupBy('employees.id')->orderBy('employees.firstName', 'asc')->get();
        $mondaystr = date('Y-m-d', strtotime('Monday this week'));
        $sundaystr = date('Y-m-d', strtotime('Friday this week'));
        $mondaystr1 = strtotime($mondaystr);
        $sundaystr1 = strtotime($sundaystr);

        $scheduledates = DB::table('schedules')->select('schedules.id', 'schedule_emp_dates.emp_sch_id', 'schedule_emp_dates.date_1', 'schedule_emp_dates.clockin',
            'schedule_emp_dates.clockout', 'employees.id as empid', 'employees.status as statuss', 'employees.employee_id')
            ->Join('schedule_emp_dates', function ($join) {
                $join->on('schedules.id', '=', 'schedule_emp_dates.emp_sch_id');
            })
            ->Join('employees', function ($join) {
                $join->on('employees.id', '=', 'schedules.emp_name');
            })
            ->where('employees.status', '=', '1')->where('employees.id', '=', $user_id)->whereBetween('schedule_emp_dates.date_1', array($mondaystr1, $sundaystr1))
            ->get();

        if (empty($schedule->id)) {
            $empschedule = '';
        } else {
            // echo "<pre>";print_r($schedule);
            echo 'sss' . $schedule->date;
            //  echo $schedule->id;
            //echo strtotime(date('m/d/Y'));
            $date1 = date('Y-m-d');
            $empschedule = DB::Table('schedule_emp_dates')->where('emp_sch_id', '=', $schedule->id)->where('date', '=', $date1)->first();
            // print_r();
            // print_r($empschedule);

        }
        $timer = DB::Table('checktimer')->where('emp_id', '=', $user_id)->where('date', '=', date('m-d-Y'))->orderBy('id', 'desc')->first();
        //print_r($timer);
        $emp_schedule = Empschedule::where('employee_id', '=', $user_id)->where('emp_in_date', '=', date('Y-m-d'))->orderBy('id', 'desc')->first();
        $remainder = Empschedule::where('employee_id', '=', $user_id)->where('emp_out', '=', Null)->orderBy('id', 'asc')->get()->first();
        if (empty($out)) {
            //$out = "0.0";   
        } else {
            $out = $emp_schedule->emp_out;
        }
        $super = Super::All();
        // $emp22= Employee::where('super','=','1')->groupBy('id')->get();
        $emp22 = DB::table('employees')->select('employees.super', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'employees.id', 'super.username')
            ->leftJoin('super', function ($join) {
                $join->on('employees.id', '=', 'super.username');
            })
            ->where('employees.super', '=', '1')->orderBy('employees.firstName', 'asc')->get();

        $monday = date('Y-m-d', strtotime('Monday this week'));
        $friday = date('Y-m-d', strtotime('Friday this week'));
        $current_time = Carbon::now()->toDateTimeString();
        $del = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('employee_id', '=', $user_id)->where('emp_in_date', '=', date('Y-m-d'))->selectRaw('emp_in,emp_out,launch_out,launch_in,launch_out_second,launch_in_second')->first();
        if (empty($del->emp_in)) {
            $currenttime = "0.0";
        } else {
            $actual_start_at = Carbon::parse($del->emp_in);
            $actual_end_at = Carbon::parse($current_time);
            $launch_in = Carbon::parse($del->launch_in);
            $launch_out = Carbon::parse($del->launch_out);
            $min = $launch_out->diffInMinutes($launch_in, true);
            $mins = $actual_end_at->diffInMinutes($actual_start_at, true);
            if (empty($out)) {
                $currenttime = (($mins / 60) - ($min / 60));
            } else {
                $currenttime = "0.0";
            }
        }


        $listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        // $listemployeeuser = DB::table('employees')->where('type','employee')->where('firstName','!=','')->where('lastName','!=','')->orderBy('firstName', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type', 'employee')->where('type', 'user')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $relatedNames = DB::table('relatednames')->get();
        $relatedNames1 = DB::table('relatednames')->get();
        $schedulesetup = DB::table('schedulesetups')->where('type', 'employee')->where('duration', '=', 'Bi-weekly')->first();

        $clientoriginal = DB::table("client_taxfederal")->where('federalstax', '=', 'Original')->groupBy('client_id')->get();

        $notesRelated = DB::table('notesrelateds')->get();
        $notesRelated1 = DB::table('notesrelateds')->get();
        $taxtitles = DB::table('taxtitles')->orderBy('title', '=', 'asc')->get();
        $clientttaxation = DB::table('client_to_taxation')->groupBy('taxation_service')->get();

        $commonone1 = DB::table("client_to_taxation as t1")->select("t1.clientid as ccid", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName", "t3.lastName", "t3.employee_id", "t2.company_name", "t2.federalstax", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
            ->leftJoin('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            })
            ->leftJoin('employees as t3', function ($join) {
                $join->on('t3.id', '=', 't1.employee_id');
            })
            ->leftJoin('client_taxfederal as t4', function ($join) {
                $join->on('t4.client_id', '=', 't1.clientid');
            })
            ->where('t4.federalstax', '!=', 'Original')->orWhereNull('t4.federalstax')->
            where('t2.status', '=', 'Active')->groupBy('t2.id')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        $decimal1 = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('employee_id', '=', $user_id)->whereNotNull('emp_out')->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,launch_out_second,launch_in_second')->get();
        return view('fscemployee/home', compact(['msg2', 'clientttaxation', 'taxtitles', 'commonone1', 'clientoriginal', 'respdaily', 'rulescount', 'schedulesetup', 'scheduledates', 'scheduleemployee', 'commonregisterall', 'admin', 'useremp', 'employees', 'taskall', 'notesRelated1', 'notesRelated', 'relatedNames1', 'listclient', 'listemployeeuser', 'listvendoe', 'relatedNames', 'tknew', 'rules', 'empschedule', 'emp22', 'timer', 'employee', 'schedule', 'emp_schedule', 'decimal1', 'currenttime', 'tk', 'msg', 'super', 'remainder']));

    }

    public function removerelatednames(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('relatednames')->where('id', $id)->delete();
        if ($student) {
            echo 'Data Deleted';
        }
    }


    public function addweges(Request $request, $id)
    {
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;
        $this->validate($request, [
            //'business_catagory_name' =>'required',
            //'business_name' =>'required|unique:employees',
            //'productid' =>'required'
        ]);

        $employee = new Employee;
        //$employee->employee_id = $request->employee_id;
        //$employee->vendorid = Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname;
        $employee->client_id = $id;
        //$employee->wegesyear = $request->employeryear;

        $employee->type = 'Vendor';
        $employee->income_id = $request->income_id;
        $employee->nametype = $request->employernametype;
        $employee->firstName = $request->employerfname;
        $employee->middleName = $request->employermname;
        $employee->lastName = $request->employerlname;
        $employee->address1 = $request->employeraddress;
        $employee->city = $request->employercity;
        $employee->stateId = $request->employerstate;
        $employee->zip = $request->employerzip;
        $employee->positions = $request->employerposition;
        $employee->employerstdate = $request->employerstdate;
        $employee->employereddate = $request->employereddate;
        //$employee->employerstdate= date('Y-m-d',strtotime($request->employerstdate));
        //$employee->employereddate= date('Y-m-d',strtotime($request->employereddate));
        $employee->vendordate = date('Y-m-d H:i:s');
        $employee->save();
        return redirect()->back()->with('success', 'Your record Inserted Successfully');
    }

    public function addwegesbanks(Request $request, $id)
    {
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;
        $this->validate($request, [
            //'business_catagory_name' =>'required',
            //'business_name' =>'required|unique:employees',
            //'productid' =>'required'
        ]);

        $client_id = $id;
        //$wegesyear = $request->employeryear;
        $income_id = $request->income_id;
        $bankname = $request->employerbankname;
        $address = $request->employeraddress;
        $city = $request->employercity;
        $stateId = $request->employerstate;
        $zip = $request->employerzip;
        $business_no = $request->employertelephone;
        $incometype = $request->incometype;
        $type = 'Vendor';
        //$employee->save();

        $returnValue = DB::table('otherincome')
            ->insert([
                'income_id' => $income_id,
                'client_id' => $client_id,
                //'wegesyear' =>$wegesyear,
                'bankname' => $bankname,
                'address' => $address,
                'city' => $city,
                'stateId' => $stateId,
                'zip' => $zip,
                'business_no' => $business_no,
                'incometype' => $incometype,
                'type' => $type,
            ]);

        return redirect()->back()->with('success', 'Your record Inserted Successfully');
    }

    // public function destroyfilling(Request $request,$id)
    // {   
    //     DB::table('personaltaxation')->where('id','=',$id)->delete();
    //     return redirect()->back()->with('success','Your Record Deleted Successfully ');
    // }

    // public function destroydependent(Request $request,$id)
    // {   
    //     DB::table('personaldependent')->where('id','=',$id)->delete();
    //     return redirect()->back()->with('success','Your Record Deleted Successfully ');
    // }

    public function addnotesrelated1(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'notetype_user' => 'required',
            'notesrelatedcat' => 'required',
        ]);


        $notedate = $request->notedate;
        $noteday = $request->noteday;
        $notetime = $request->notetime;
        $notetype_user = $request->notetype_user;
        if ($request->noteclientid != '') {
            $noteclientid = $request->noteclientid;
        } else {
            $noteclientid = '';
        }

        if ($request->noteemployeeuserid != '') {
            $noteemployeeuserid = $request->noteemployeeuserid;
        } else {
            $noteemployeeuserid = '';
        }

        if ($request->notevendorid != '') {
            $notevendorid = $request->notevendorid;
        } else {
            $notevendorid = '';
        }

        if ($request->noteotherid != '') {
            $noteotherid = $request->noteotherid;
        } else {
            $noteotherid = '';
        }

        $noterelated = $request->noterelated;
        $notesrelatedcat = $request->notesrelatedcat;
        $notes = $request->notes;

        $returnValue = DB::table('notes_sheet')
            ->insert([
                'creattiondate' => $notedate,
                'noteday' => $noteday,
                'notetime' => $notetime,
                'notetype_user' => $notetype_user,
                'noteclientid' => $noteclientid,
                'noteemployeeuserid' => $noteemployeeuserid,
                'notevendorid' => $notevendorid,
                'noteotherid' => $noteotherid,
                'noterelated' => $noterelated,
                'notesrelatedcat' => $notesrelatedcat,
                'notes' => $notes,

            ]);


        return redirect()->back()->with('success', 'Record Added Successfully');
    }

    public function getComplete(Request $request)
    {
        $returnValue = DB::table('rules')->where('id', '=', $request->id)->update(['flag' => '1']);
    }

    public function addcoversationsheet1(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'type_user' => 'required',
            'conrelatedname' => 'required',
        ]);


        $date = $request->date;
        $day = $request->day;
        $time = $request->time;
        $type_user = $request->type_user;
        if ($request->clientid != '') {
            $clientid = $request->clientid;
        } else {
            $clientid = '';
        }

        if ($request->employeeuserid != '') {
            $employeeuserid = $request->employeeuserid;
        } else {
            $employeeuserid = '';
        }

        if ($request->vendorid != '') {
            $vendorid = $request->vendorid;
        } else {
            $vendorid = '';
        }

        if ($request->otherid != '') {
            $otherid = $request->otherid;
        } else {
            $otherid = '';
        }

        $conrelatedname = $request->conrelatedname;
        $condescription = $request->condescription;
        $connotes = $request->connotes;

        $returnValue = DB::table('conversation_sheet')
            ->insert([
                'creattiondate' => $date,
                'day' => $day,
                'time' => $time,
                'type_user' => $type_user,
                'clientid' => $clientid,
                'employeeuserid' => $employeeuserid,
                'vendorid' => $vendorid,
                'otherid' => $otherid,
                'conrelatedname' => $conrelatedname,
                'condescription' => $condescription,
                'connotes' => $connotes,

            ]);


        return redirect()->back()->with('success', 'Record Added Successfully');
    }

    public function notesrelateds1()
    {
        $newopt2 = Input::get('newopt2');
        //$sign = Input::get('sign');
        if (DB::table('notesrelateds')->where('notesrelated', '=', $newopt2)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $notesrelated = new Notesrelated;
            $notesrelated->notesrelated = $newopt2;
            // $notesrelated->sign = $sign;
            $notesrelated->save();
            if ($notesrelated) {
                return response()->json([
                    'status' => 'success',
                    'newopt2' => $newopt2]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }

    public function removenotenotes(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('notesrelateds')->where('id', $id)->delete();
        if ($student) {
            echo 'Data Deleted';
        }
    }


    public function fetch2(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');


        if ($type == 'C') {
            $commonregiser = DB::table('commonregisters')
                ->select('commonregisters.email', 'commonregisters.business_no as telephone', 'commonregisters.mobile_1 as telephone2',
                    'commonregisters.telephoneNo1Type as telephonetype', 'commonregisters.eteletype1 as telephonetype2', 'commonregisters.businessext as ext1', 'commonregisters.type as Type',
                    'commonregisters.id as cid', 'commonregisters.filename as entityid', 'commonregisters.first_name as fname', 'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname', 'commonregisters.company_name as cname',
                    'commonregisters.business_id', 'commonregisters.business_name', 'commonregisters.firstname', 'commonregisters.lastname',
                    'commonregisters.etelephone1 as telephoneNo2', 'commonregisters.eteletype1 as telephoneNo2Type',
                    'commonregisters.eext1 as ext2', 'commonregisters.personalemail as fscemail')
                ->where('id', '=', $id)->where('status', '=', 'Active')->first();


        } else if ($type == 'EE' || $type == 'U' || $type == 'V' || $type == 'C-EE') {

            $commonregiser = DB::table('employees')
                ->select('employees.email', 'employees.telephoneNo1 as telephone', 'employees.etelephone1 as telephone2', 'employees.telephoneNo1Type as telephonetype',
                    'employees.eteletype1 as telephonetype2', 'employees.ext1', 'employees.type as Type', 'employees.id as cid', 'employees.employee_id as entityid',
                    'employees.firstName as fname', 'employees.middleName as mname', 'employees.lastName as lname', 'employees.business_id', 'employees.business_name',
                    'employees.firstname', 'employees.lastname', 'employees.telephoneNo2', 'employees.telephoneNo2Type', 'employees.ext2', 'employees.fscemail')
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //echo "<pre>";print_r($commonregiser);
        }
        echo json_encode($commonregiser);
        exit;

    }


    public function fetch11(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            // $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->
            // orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->get();

            $commonregiser = DB::table('commonregisters')
                ->select('commonregisters.type as Type', 'commonregisters.id as cid', 'commonregisters.filename as entityid', 'commonregisters.first_name as fname', 'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname', 'commonregisters.company_name as cname', 'commonregisters.etelephone1', 'commonregisters.business_no', 'commonregisters.business_name',
                    'commonregisters.business_id', 'commonregisters.status')
                ->where('filename', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('first_name', 'LIKE', "%{$query}%")->orwhere('last_name', 'LIKE', "%{$query}%");


            $employ = DB::table('employees')
                ->select('employees.type as Type', 'employees.id as cid', 'employees.employee_id as entityid', 'employees.firstName as fname', 'employees.middleName as mname',
                    'employees.lastName as lname', 'employees.company_name as cname', 'employees.business_no', 'employees.telephoneNo1', 'employees.business_name', 'employees.business_id', 'employees.status')
                ->where('employee_id', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->orwhere('firstName', 'LIKE', "%{$query}%")->orwhere('lastName', 'LIKE', "%{$query}%");

            $datas = $commonregiser->union($employ)->get();
            // echo "<pre>";print_r($datas);

            $output = '';
            $i;
            foreach ($datas as $row) {
                // print_r($row);
                // echo $row->filename;
                // echo $query;
                if ($row->status == '1' || $row->status == 'Active') {

                    //if($row->Type =='employee' || $row->Type =='Vendor' || $row->Type =='clientemployee')
                    if ($row->Type == 'employee' || $row->Type == 'user' || $row->Type == 'Vendor' || $row->Type == 'clientemployee') {
                        $id = $row->cid;
                        if ($row->Type == 'Vendor') {
                            $names = $row->business_name;

                        } else {
                            $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                        }
                        if ($row->Type == 'employee') {
                            $types1 = 'EE';
                        } else if ($row->Type == 'user') {
                            $types1 = 'U';
                        } else if ($row->Type == 'Vendor') {
                            $types1 = 'V';
                        } else if ($row->Type == 'clientemployee') {
                            $types1 = 'C-EE';

                        }


                    } else {


                        if ($row->business_id == '6') {
                            $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                            $types1 = "C";
                            $id = $row->cid;
                        } else {
                            $names = $row->cname;
                            $types1 = "C";
                            $id = $row->cid;
                        }
                    }
                    ?>
                    <li style="text-align:left;"><a onclick="myfun('<?php echo $id; ?>','<?php echo $types1; ?>')"><span class="bgcolors"><?php echo $types1; ?></span><span class="clientalign"><?php if (isset($row->entityid) != '') {
                                    echo $row->entityid . ' ';
                                } else {
                                    echo $id . ' ';
                                } ?></span><span class="entityname"><?php echo $names; ?></span></a></li>
                    <?php
                }


            }
            $output .= '';
            echo $output;
        }
    }

    public function fetch111(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');


        if ($type == 'C') {
            $commonregiser = DB::table('commonregisters')
                ->select('commonregisters.email', 'commonregisters.business_no as telephone', 'commonregisters.mobile_1 as telephone2',
                    'commonregisters.businesstype as telephonetype', 'commonregisters.eteletype1 as telephonetype2', 'commonregisters.businessext as ext1', 'commonregisters.type as Type',
                    'commonregisters.id as cid', 'commonregisters.filename as entityid', 'commonregisters.first_name as fname', 'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname', 'commonregisters.company_name as cname',
                    'commonregisters.business_id', 'commonregisters.company_name', 'commonregisters.firstname', 'commonregisters.lastname',
                    'commonregisters.etelephone1 as telephoneNo2', 'commonregisters.eteletype1 as telephoneNo2Type', 'commonregisters.eext1 as ext2', 'commonregisters.personalemail as fscemail')->where('id', '=', $id)->where('status', '=', 'Active')->first();


        } else if ($type == 'EE' || $type == 'V' || $type == 'C-EE') {

            $commonregiser = DB::table('employees')
                ->select('employees.email', 'employees.telephoneNo1 as telephone', 'employees.etelephone1 as telephone2', 'employees.telephoneNo1Type as telephonetype',
                    'employees.eteletype1 as telephonetype2', 'employees.ext1', 'employees.type as Type', 'employees.id as cid', 'employees.employee_id as entityid',
                    'employees.firstName as fname', 'employees.middleName as mname', 'employees.lastName as lname', 'employees.business_id', 'employees.business_name as company_name',
                    'employees.firstname', 'employees.lastname', 'employees.telephoneNo2', 'employees.telephoneNo2Type', 'employees.ext2', 'employees.fscemail')
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //echo "<pre>";print_r($commonregiser);
        }
        echo json_encode($commonregiser);
        exit;

    }


    public function supervisor($id, Request $request)
    {
        // $halaman = 'tindaklayanan';
        $keluhan = Empschedule::findOrFail($id);
        $keluhan->super = $request->code;
        $sasaran->save();
        return redirect('/');
    }

    public function supervisor_suepr(Request $request)
    {
        $user = DB::table('super')->where('code', Input::get('code'))->count();
        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    public function supervisor_suepr1(Request $request)
    {
        $user = DB::table('super')->where('code', Input::get('code1'))->count();
        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $timer = DB::Table('checktimer')->where('emp_id', '=', $user_id)->where('date', '=', date('m-d-Y'))->first();
        if ($request->supervisorname) {
            $ckemp = new ckemp;
            $ckemp->emp_id = $user_id;
            $ckemp->after_code = $request->code;
            $ckemp->before_code = $request->code1;
            $ckemp->date = date('m-d-Y');

            $ckemp->save();
        } else {
            $remainder = date('Y-m-d', strtotime("+3 days"));
            $employee = Employee::where('id', '=', $user_id)->first();
            $ip = $_SERVER['REMOTE_ADDR'];
            $branch = new Empschedule;
            $branch->employee_id = $request->employee_id;
            $branch->emp_in_date = $request->emp_in_date;
            $branch->emp_in = $request->emp_in;
            $branch->note = $request->note;
            //$branch->note= $request->note;
            $branch->fsccity = $employee->branch_city;
            $branch->ip_address = $ip;
            $branch->remainder_date = $remainder;
            $branch->save();
        }
        return redirect('fscemployee/home');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function show(home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $schedule = Schedule::where('emp_name', '=', $user_id)->first();
        $emp_schedule = Empschedule::where('id', $id)->first();
        return view('fscemployee/home', compact(['employee', 'schedule', 'emp_schedule']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->supervisorname) {
            $ckemp = ckemp::find($id);
            $ckemp->before_code = $request->code1;
            $ckemp->update();
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
            $branch = Empschedule::find($id);
            $branch->emp_out = $request->emp_out;
            $branch->work_note = $request->work;
            $branch->note = $request->note;
            $branch->launch_in = $request->launch_in;
            $branch->launch_out = $request->launch_out;
            $branch->launch_in_second = $request->launch_in_second;
            $branch->launch_out_second = $request->launch_out_second;
            $branch->ip_address = $ip;
            $branch->save();
        }
        return redirect('fscemployee/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(home $home)
    {
        //
    }
}