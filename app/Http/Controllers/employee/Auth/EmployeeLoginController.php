<?php

namespace App\Http\Controllers\employee\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesEmployee;
use Illuminate\Http\Request;

class EmployeeLoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
      */

    use AuthenticatesEmployee;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'fscemployee/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    public function logout1(Request $request)
    {
        $this->guard('employee')->logout1();
        $request->session()->invalidate();
        return redirect('/login');
    }

}