<?php

namespace App\Http\Controllers\Clients;

use App\employee\ckemp;
use App\employee\Empschedule;
use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\ClientEmployee;
use App\Model\Employee;
use App\Model\Rules;
use App\Model\Schedule;
use App\Model\Super;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Validator;
use View;

class ClientsController extends Controller
{

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     *
     */
    public function __construct()
    {
        $this->middleware('auth:client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    public function index()
    {

        // $user_id  = Auth::user()->user_id;
        // $empsuper= DB::table('employees')->where('id','=',$user_id)->first();
        // print_r($empsuper);
        // die;


        $user = ClientEmployee::All();
        $user_id = Auth::user()->user_id;
        $id = Auth::user()->id;


        $rules = Rules::where('employee_id', '=', $user_id)->where('status', '=', 1)->count();
        $tk = DB::table('tasks')->select('checked', DB::raw('count(*) as emptotal'))->where('employeeid', '=', $id)->groupBy('checked')->get()->first();
        $msg = DB::table('msgs')->select('recieve', DB::raw('count(*) as msgtotal'))->where('employeeid', '=', $user_id)->where('recieve', '=', 1)->groupBy('recieve')->get();
        $employee = Employee::where('id', '=', $user_id)->first();
        $schedule = Schedule::where('emp_name', '=', $user_id)->first();
        if (empty($schedule->id)) {
            $empschedule = '';
        } else {
            $empschedule = DB::Table('schedule_emp_dates')->where('emp_sch_id', '=', $schedule->id)->where('date_1', '=', strtotime(date('m/d/Y')))->first();
        }

        $timer = DB::Table('checktimer')->where('emp_id', '=', $user_id)->where('date', '=', date('m-d-Y'))->orderBy('id', 'desc')->first();

        $emp_schedule = Empschedule::where('employee_id', '=', $user_id)->where('emp_in_date', '=', date('Y-m-d'))->orderBy('id', 'desc')->first();
        $remainder = Empschedule::where('employee_id', '=', $user_id)->where('emp_out', '=', Null)->orderBy('id', 'asc')->get()->first();
        if (empty($out)) {
            //$out = "0.0";   
        } else {
            $out = $emp_schedule->emp_out;
        }

        $super = Super::All();

        $empsupwerwise = DB::Table('fscemployees')->where('id', '=', $id)->first();
        $empsupwerwise->clienttype;
        $emp22 = DB::table('commonregisters')->where('id', '=', $empsupwerwise->clienttype)->first();
        // echo "<pre>";
        //     print_r($emp22);
        //     die;    
        $monday = date('Y-m-d', strtotime('Monday this week'));
        $friday = date('Y-m-d', strtotime('Friday this week'));
        $current_time = Carbon::now()->toDateTimeString();
        $del = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('employee_id', '=', $user_id)->where('emp_in_date', '=', date('Y-m-d'))->selectRaw('emp_in,emp_out,launch_out,launch_in,launch_out_second,launch_in_second')->first();
        if (empty($del->emp_in)) {
            $currenttime = "0.0";
        } else {
            // First Start
            $actual_start_at = Carbon::parse($del->emp_in);
            $actual_end_at = Carbon::parse($current_time);
            $launch_in = Carbon::parse($del->launch_in);
            $launch_out = Carbon::parse($del->launch_out);
            $min = $launch_out->diffInMinutes($launch_in, true);
            $mins = $actual_end_at->diffInMinutes($actual_start_at, true);
            if (empty($out)) {
                $currenttime = (($mins / 60) - ($min / 60));
            } else {

                $currenttime = "0.0";
            }
        }

        $decimal1 = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('employee_id', '=', $user_id)->whereNotNull('emp_out')->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,launch_out_second,launch_in_second')->get();
        return view('clientemployee/home', compact(['emp22', 'user_id', 'rules', 'empschedule', 'emp22', 'timer', 'employee', 'schedule', 'emp_schedule', 'decimal1', 'currenttime', 'tk', 'msg', 'super', 'remainder']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function supervisor_sueprstart2(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $empsuper = DB::table('employees')->where('id', '=', $user_id)->first();
        //echo "<br>";
        $empsuper->clienttype;
        //echo "<br>";
        //print_r($_REQUEST);die;

        $user = DB::table('commonregisters')->where('code', Input::get('code'))->where('id', $empsuper->clienttype)->count();

        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }


    public function supervisor_sueprend2(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $empsuper = DB::table('employees')->where('id', '=', $user_id)->first();
        //echo "<br>";
        $empsuper->clienttype;
        //echo "<br>";
        //print_r($_REQUEST);die;

        $user = DB::table('commonregisters')->where('code', Input::get('code1'))->where('id', $empsuper->clienttype)->count();
        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }


    public function create()
    {
        //
    }


    public function supervisor($id, Request $request)
    {
        // $halaman = 'tindaklayanan';
        $keluhan = Empschedule::findOrFail($id);
        $keluhan->super = $request->code;
        $sasaran->save();
        return redirect('/');
    }

    public function supervisor_suepr(Request $request)
    {
        $user = DB::table('super')->where('code', Input::get('code'))->count();
        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    public function supervisor_suepr1(Request $request)
    {
        $user = DB::table('super')->where('code', Input::get('code1'))->count();
        if ($user > 0) {
            $isAvailable = TRUE;
        } else {
            $isAvailable = FALSE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $timer = DB::Table('checktimer')->where('emp_id', '=', $user_id)->where('date', '=', date('m-d-Y'))->first();
        if ($request->supervisorname) {
            $ckemp = new ckemp;
            $ckemp->emp_id = $user_id;
            $ckemp->after_code = $request->code;
            $ckemp->before_code = $request->code1;
            $ckemp->date = date('m-d-Y');

            $ckemp->save();
        } else {
            $remainder = date('Y-m-d', strtotime("+3 days"));
            $employee = Employee::where('id', '=', $user_id)->first();
            $ip = $_SERVER['REMOTE_ADDR'];
            $branch = new Empschedule;
            $branch->employee_id = $request->employee_id;
            $branch->emp_in_date = $request->emp_in_date;
            $branch->emp_in = $request->emp_in;
            $branch->note = $request->note;
            //$branch->note= $request->note;
            $branch->fsccity = $employee->branch_city;
            $branch->ip_address = $ip;
            $branch->remainder_date = $remainder;
            $branch->save();
        }
        return redirect('clientemployee');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function show(home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        $schedule = Schedule::where('emp_name', '=', $user_id)->first();
        $emp_schedule = Empschedule::where('id', $id)->first();
        return view('clientemployee', compact(['employee', 'schedule', 'emp_schedule']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->supervisorname) {
            $ckemp = ckemp::find($id);
            $ckemp->before_code = $request->code1;
            $ckemp->update();
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
            $branch = Empschedule::find($id);
            $branch->emp_out = $request->emp_out;
            $branch->work_note = $request->work;
            $branch->note = $request->note;
            $branch->launch_in = $request->launch_in;
            $branch->launch_out = $request->launch_out;
            $branch->launch_in_second = $request->launch_in_second;
            $branch->launch_out_second = $request->launch_out_second;
            $branch->ip_address = $ip;
            $branch->save();
        }
        return redirect('clientemployee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\employee\home $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(home $home)
    {
        //
    }
}