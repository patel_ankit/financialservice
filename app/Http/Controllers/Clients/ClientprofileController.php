<?php

namespace App\Http\Controllers\Clients;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Position;
use App\Model\Rules;
use App\Model\Super;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Validator;
use View;

class ClientprofileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $super = Super::All();
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $emp = Employee::where('id', $user_id)->first();
        $super1 = Employee::where('super', '=', '1')->get();
        $position = Position::orderBy('position', 'asc')->get();
        $rules = Rules::where('type', '=', 'Rules')->get();
        $resposibilty = Rules::where('type', '=', 'Resposibilty')->orderBy('id', 'desc')->get();
        $branch = DB::table('branches')->select('city')->groupBy('city')->get();
        $info1 = DB::table('employee_pay_info')->where('employee_id', $user_id)->first();
        $info = DB::table('employee_pay_info')->where('employee_id', $user_id)->get();
        $info3 = DB::table('employee_other_info')->where('employee_id', $user_id)->get();
        $info2 = DB::table('employee_other_info')->where('employee_id', $user_id)->first();
        $review1 = DB::table('employee_review')->where('employee_id', $user_id)->get();
        $review = DB::table('employee_review')->where('employee_id', $user_id)->first();
        return view('clientemployee/clientprofile', compact(['super1', 'super', 'resposibilty', 'employee', 'rules', 'emp', 'info1', 'info', 'info2', 'info3', 'review1', 'review', 'position', 'branch']));
    }

    public function getemp1(Request $request)
    {
        $user = DB::table('employees')->where('employee_id', Input::get('employee_id'))->count();
        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    public function checkemailAvailability(Request $request)
    {

        $user = DB::table('employees')->where('email', Input::get('email'))->count();
        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
    }


    public function update($id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'pfid1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'pfid2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('photo')) {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        } else {
            $filname = $request->photo1;
        }
        if ($request->hasFile('pfid1')) {
            $filname1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/employeeProof1', $filname1);
        } else {
            $filname1 = $request->pfid12;
        }
        if ($request->hasFile('pfid2')) {
            $filname2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/employeeProof2', $filname2);
        } else {
            $filname2 = $request->pfid22;
        }

        if ($request->hasFile('additional_attach')) {
            $additional_att = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/additional_attach', $additional_att);
        } else {
            $additional_att = $request->additional_attach1;
        }
        if ($request->hasFile('additional_attach_1')) {
            $additional_att_1 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/additional_attach1', $additional_att_1);
        } else {
            $additional_att_1 = $request->additional_attach2;
        }
        if ($request->hasFile('additional_attach_2')) {
            $additional_att_2 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/additional_attach2', $additional_att_2);
        } else {
            $additional_att_2 = $request->additional_attach3;
        }
        if ($request->hasFile('agreement')) {
            $agreement1 = $request->agreement->getClientOriginalName();
            $request->agreement->move('public/agreement', $agreement1);
        } else {
            $agreement1 = $request->agreement_1;
        }

        if ($request->hasFile('resume')) {
            $resume1 = $request->resume->getClientOriginalName();
            $request->resume->move('public/resumes', $resume1);
        } else {
            $resume1 = $request->resume_1;
        }
        if ($request->terms == '2') {
            $rules = '2';
        } else {
            $rules = '1';
        }
        $business = Fscemployee::find($id);
        $user_id = Auth::user()->user_id;
        $resetdays = $request->resetdays;
        $enddate = date('Y-m-d', strtotime("+$resetdays days"));
        $startdate = date('Y-m-d');
        $business->rule_status = $rules;
        $business->question1 = $request->question1;
        $business->question2 = $request->question2;
        $business->question3 = $request->question3;
        $business->answer1 = $request->answer1;
        $business->answer2 = $request->answer2;
        $business->answer3 = $request->answer3;
        $business->active = '1';

        if (isset($request->newpassword) != '') {
            $business->resetdays = $request->resetdays;
            $business->remaining_day = $request->resetdays;
            $business->startdate = $startdate;
            //$business->enddate =  $request->reset_date;
            $business->password = bcrypt($request->newpassword);
            $business->newpassword = $request->newpassword;
        }


        $employee = Employee::find($user_id);
        $employee->employee_id = $request->employee_id;
        $employee->read = $request->terms;
        $employee->rulesdate = $request->rulesdate;
        $employee->firstName = $request->firstName;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->stateId = $request->stateId;

        if ($request->city != '' && $request->stateId != '') {
            $aa = $request->city;
            $bb = $request->stateId;
            $cc = $aa . ', ' . $bb;
        } else {
            $cc = '';
        }
        $employee->branch_city = $cc;

        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->termimonth = $request->termimonth;
        $employee->hiremonth = $request->hiremonth;
        $employee->hireday = $request->hireday;
        $employee->hireyear = $request->hireyear;
        $employee->termiday = $request->termiday;
        $employee->termiyear = $request->termiyear;
        $employee->tnote = $request->tnote;
        $employee->rehiremonth = $request->rehiremonth;
        $employee->rehireday = $request->rehireday;
        $employee->rehireyear = $request->rehireyear;
        //$employee->branch_city= $request->branch_city;
        $employee->branch_name = $request->branch_name;
        $employee->position = $request->position;
        $employee->note = $request->note;
        $employee->pay_method = $request->pay_method;
        $employee->pay_frequency = $request->pay_frequency;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        $employee->pf2 = $request->pf2;
        $employee->nametype = $request->nametype;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->uname = $request->uname;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        $employee->filling_status = $request->filling_status;
        $employee->fedral_claim = $request->fedral_claim;
        $employee->additional_withholding = $request->additional_withholding;
        $employee->state_claim = $request->state_claim;
        $employee->additional_withholding_1 = $request->additional_withholding_1;
        $employee->local_claim = $request->local_claim;
        $employee->additional_withholding_2 = $request->additional_withholding_2;
        $employee->resume = $resume1;
        $employee->type_agreement = $request->type_agreement;
        $employee->firstName_1 = $request->firstName_1;
        $employee->agreement = $agreement1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->super = $request->super;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;
        $employee->employee_rules = $request->employee_rules;
        $employee->eemail = $request->eemail;
        $employee->update();
        $pay_method = $request->pay_method;
        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee = $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;

        $i = 0;
        $j = 0;
        $k = 0;
        DB::table('employee_pay_info')->where('employee_id', $user_id)->first();
        if (isset($request->pay_scale) != '') {
            foreach ($pay_scale as $post) {
                $pay_method1 = $pay_method;
                $pay_frequency1 = $pay_frequency;
                $pay_scale1 = $pay_scale[$i];
                $effective_date1 = $effective_date[$i];
                $fields1 = $fields[$i];
                $employee1 = $employee[$i];
                $i++;
                DB::table('employee_pay_info')->where('pay_scale', '=', '')->delete();
                if (empty($employee1)) {
                    $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) values('" . $user_id . "','" . $pay_method . "','" . $pay_frequency . "','" . $post . "','" . $effective_date1 . "','" . $fields1 . "')");
                    DB::table('employee_pay_info')->where('pay_scale', '=', '')->delete();
                } else {
                    $returnValue = DB::table('employee_pay_info')->where('id', '=', $employee1)
                        ->update(['employee_id' => $user_id,
                            'pay_method' => $pay_method,
                            'pay_frequency' => $pay_frequency,
                            'pay_scale' => $post,
                            'effective_date' => $effective_date1,
                            'fields' => $fields1
                        ]);
                    DB::table('employee_pay_info')->where('pay_scale', '=', '')->delete();
                }
            }
        }

        $first_rev_day = $request->first_rev_day;
        $reviewmonth = $request->reviewmonth;
        $hiring_comments = $request->hiring_comments;
        $ree = $request->ree;

        DB::table('employee_review')->where('employee_id', $user_id)->first();

        if (isset($request->first_rev_day) != '') {
            foreach ($first_rev_day as $post) {
                $first_rev_day1 = $first_rev_day[$k];
                $reviewmonth1 = $reviewmonth[$k];
                $hiring_comments1 = $hiring_comments[$k];
                $ree1 = $ree[$k];
                $k++;
                DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                if (empty($ree1)) {
                    $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('" . $user_id . "','" . $post . "','" . $reviewmonth1 . "','" . $hiring_comments1 . "')");
                    DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                } else {
                    //DB::table('employee_review')->where('id', $ree1)->delete();
                    $returnValue = DB::table('employee_review')->where('id', '=', $ree1)
                        ->update(['employee_id' => $user_id,
                            'first_rev_day' => $post,
                            'reviewmonth' => $reviewmonth1,
                            'hiring_comments' => $hiring_comments1
                        ]);
                    DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                    //$affectedRows = employee_pay_info::where('first_rev_day', '=', '')->delete();
                }
            }
        }


        $business->update();
        if ($business->update()) {
            return response()->json([
                'status' => 'success',
                'newopt' => $request->company_name]);
        } else {
            return response()->json([
                'status' => 'error']);
        }
    }


    public function getbranch1(Request $request)
    {
        $data = Branch::select('branchname', 'city')->where('city', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function getemployeepassword(Request $request)
    {

        $data = $request->all();
        $user = Fscemployee::find(auth()->user()->id);
        if (!Hash::check($data['oldpassword'], $user->password)) {
            $isAvailable = false;
        } else {
            $isAvailable = true;
        }
        echo json_encode(
            array('valid' => $isAvailable));
    }

    public function responsibility($id, Request $request)
    {
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', $user_id)->first();
        $position = Rules::where('id', $id)->first();
        return view('fscemployee/responsibility', compact(['position', 'employee']));
    }

    public function responupdate($id, Request $request)
    {
        $position = Rules::find($id);
        $position->status = $request->status;
        $position->responupdate();
        return view('fscemployee/employeeprofile', compact(['position']));
    }


    public function profilephoto(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('photo_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'photo' => 'image',
                ],
                [
                    'photo.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('photo')->getClientOriginalExtension();
            $dir = 'public/employeeimage/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('photo')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['photo' => $filename]);
            return $filename;
        }
    }


    public function additional1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file' => 'image',
                ],
                [
                    'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_2' => $filename]);
            return $filename;
        }
    }


    public function additional2(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload1');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_1' => 'image',
                ],
                [
                    'file_1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_1')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_1' => $filename]);
            return $filename;
        }
    }


    public function additional3(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_2' => 'image',
                ],
                [
                    'file_2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_2')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach' => $filename]);
            return $filename;
        }
    }


    public function pfidd1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid1' => 'image',
                ],
                [
                    'pfid1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid1')->getClientOriginalExtension();
            $dir = 'public/employeeProof1/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid1' => $filename]);
            return $filename;
        }
    }


    public function pfidd(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid2' => 'image',
                ],
                [
                    'pfid2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid2')->getClientOriginalExtension();
            $dir = 'public/employeeProof2/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid2' => $filename]);
            return $filename;
        }
    }

    public function reviewdelete11($id)
    {
        $logoStatus = DB::table('employee_review')->where('id', '=', $id)->delete();
        return redirect('fscemployee/employeeprofile?tab=0')->with('error', 'Your Record Deleted Successfully');
    }

    public function paydelete11($id)
    {
        $logoStatus = DB::table('employee_pay_info')->where('id', '=', $id)->delete();
        return redirect('fscemployee/employeeprofile?tab=0')->with('error', 'Your Record Deleted Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();
        return redirect(route('employee.index'))->with('success', 'Success Fully Delete Record');
    }

}