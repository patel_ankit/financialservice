<?php

namespace App\Http\Controllers\Clients\Auth;

use App\Http\Controllers\Controller;
use App\Model\ClientEmployee;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesClient;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    use AuthenticatesClient;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'clientemployee/home';


    public function __construct()
    {
        $this->middleware('guest:client');
    }

    public function showLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        $user = ClientEmployee::where('email', $request->email)->first();
        if ($user == Null) {
            return redirect()->back()->withInput($request->only('email', 'remember'))->with('error', 'Your Username Password  Not Registered!!!');
        } else {
            if ($user->type == 1) {
                if (Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password, 'type' => 1], $request->remember)) {
                    return redirect()->intended(redirect('clientemployee/home'))->with('success', 'Welcome Our Dashboard!!!');
                }
            } else if ($user->type == 0) {
                return redirect()->back()->withInput($request->only('email', 'remember'))->with('error', 'Your Account is Inactive pelase contact your admin!!!');
            }
        }
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('error', 'Your Username Password Invalid!!!');
    }

    public function logout(Request $request)
    {
        Auth::guard('client')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('clientemployee.login.submit'));
    }

    protected function credentials(Request $request)
    {
        return ['email' => $request->email, 'password' => $request->password, 'type' => 1];
    }
}