<?php

namespace App\Http\Controllers\Auth;

use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class fscemployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:employee');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in

        $user = Fscemployee::where('email', $request->emp_email)->where('employee_id', $request->employeeid)->first();
        if ($user['email'] && $user['employee_id']) {
            if ($request->type1 == '1') {
                if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password, 'type' => 1], $request->remember)) {
                    //echo '3';
                    return redirect()->intended('fscemployee/home')->with('success', 'Welcome Our Dashboard!!!');
                } else {
                    // echo '5';
                    return redirect()->back()->withInput($request->only('email', 'remember'))->with('error', 'Your User Id and password wrong');
                }
            } else {
                echo '2';
            }
        } else {

            echo "0";
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('employee')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('fscemployee.login.submit'));
    }

    protected function credentials(Request $request)
    {
        //$input = $request->only($this->username(), 'password');
        // $input['type'] = 1; //Add extra key,value which need to be checked.
        return ['email' => $request->email, 'password' => $request->password, 'type' => 1];

    }


}