<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\Employee;
use App\Model\Schedule;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        //print_r($_REQUEST);die;

        if (isset($request->filename) != '') {
            $newclientdata = DB::table('commonregisters')->select('id', 'filename')->where('filename', $request->filename)->first();
            //print_r($newclientdata);die;
            $category = Category::All();
            $business = Business::all();
            $businessbrand = BusinessBrand::All();
            $cd = Categorybusiness::All();
            $user = User::All();
            $user_id = Auth::user()->user_id;
            $commonyy = DB::table('commonregisters')->select('id', 'subscription_active')->where('id', $user_id)->first();

            $monday = date('Y-m-d', strtotime('Monday this week'));
            $friday = date('Y-m-d', strtotime('Friday this week'));
            $schedule = Schedule::groupBy('emp_city')->selectRaw('emp_city,GROUP_CONCAT(emp_name) as emp_name')->get();
            $employee = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('emp_in_date', '=', date('Y-m-d'))->selectRaw('emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,employee_id,fsccity,launch_out_second,launch_in_second')->get();
            $common = DB::table('commonregisters')->select('commonregisters.id as cid', 'commonregisters.productid', 'commonregisters.filename', 'commonregisters.user_type as user_type', 'commonregisters.productid', 'commonregisters.contact_fax_1 as contact_fax_1', 'commonregisters.eext2 as eext2', 'commonregisters.eteletype2 as eteletype2', 'commonregisters.etelephone2 as etelephone2', 'commonregisters.eext1 as eext1', 'commonregisters.eteletype1 as eteletype1', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.city_1 as city_1', 'commonregisters.zip_1 as zip_1', 'commonregisters.contact_address2 as contact_address2', 'commonregisters.contact_address1 as contact_address1', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.nametype as nametype', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name')
                ->leftJoin('categories', function ($join) {
                    $join->on('commonregisters.business_cat_id', '=', 'categories.id');
                })
                ->leftJoin('businesses', function ($join) {
                    $join->on('commonregisters.business_id', '=', 'businesses.id');
                })
                ->leftJoin('business_brands', function ($join) {
                    $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
                })
                ->leftJoin('categorybusinesses', function ($join) {
                    $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
                })
                ->where('commonregisters.id', '=', $newclientdata->id)->get()->first();
            $set = Employee::groupBy('branch_city')->selectRaw('branch_city')->get();
            $set1 = DB::Table('empschedules')->where('emp_in_date', '=', date('Y-m-d'))->get();
            $set1 = Employee::where('clienttype', '=', $newclientdata->id)->get();


            $newclient12 = DB::table('commonregisters')->where('id', $newclientdata->id)->first();
            $commonuser = DB::table('commonregisters')->select('commonregisters.check_status', 'commonregisters.company_name', 'commonregisters.filename', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->where('status', '=', 'Active')->orderby('filename', 'asc')->get();
            $linkuser = DB::table('commonregisters')->select('commonregisters.productid', 'commonregisters.company_name', 'commonregisters.filename', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->whereRaw('FIND_IN_SET("' . $newclient12->filename . '",productid)')->first();

            return view('client/home', compact(['commonyy', 'commonuser', 'linkuser', 'common', 'category', 'business', 'businessbrand', 'cd', 'schedule', 'set', 'set1', 'employee', 'monday', 'friday']));

        } else {

            $category = Category::All();
            $business = Business::all();
            $businessbrand = BusinessBrand::All();
            $cd = Categorybusiness::All();
            $user = User::All();
            $user_id = Auth::user()->user_id;
            $commonyy = DB::table('commonregisters')->select('id', 'subscription_active')->where('id', $user_id)->first();
            //print_r($commonyy);die;

            $monday = date('Y-m-d', strtotime('Monday this week'));
            $friday = date('Y-m-d', strtotime('Friday this week'));
            $schedule = Schedule::groupBy('emp_city')->selectRaw('emp_city,GROUP_CONCAT(emp_name) as emp_name')->get();
            $employee = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('emp_in_date', '=', date('Y-m-d'))->selectRaw('emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,employee_id,fsccity,launch_out_second,launch_in_second')->get();
            $common = DB::table('commonregisters')->select('commonregisters.id as cid', 'commonregisters.productid', 'commonregisters.filename', 'commonregisters.user_type as user_type', 'commonregisters.productid', 'commonregisters.contact_fax_1 as contact_fax_1', 'commonregisters.eext2 as eext2', 'commonregisters.eteletype2 as eteletype2', 'commonregisters.etelephone2 as etelephone2', 'commonregisters.eext1 as eext1', 'commonregisters.eteletype1 as eteletype1', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.city_1 as city_1', 'commonregisters.zip_1 as zip_1', 'commonregisters.contact_address2 as contact_address2', 'commonregisters.contact_address1 as contact_address1', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.nametype as nametype', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name')
                ->leftJoin('categories', function ($join) {
                    $join->on('commonregisters.business_cat_id', '=', 'categories.id');
                })
                ->leftJoin('businesses', function ($join) {
                    $join->on('commonregisters.business_id', '=', 'businesses.id');
                })
                ->leftJoin('business_brands', function ($join) {
                    $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
                })
                ->leftJoin('categorybusinesses', function ($join) {
                    $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
                })
                ->where('commonregisters.id', '=', $user_id)->get()->first();
            $set = Employee::groupBy('branch_city')->selectRaw('branch_city')->get();
            $set1 = DB::Table('empschedules')->where('emp_in_date', '=', date('Y-m-d'))->get();
            $set1 = Employee::where('clienttype', '=', $user_id)->get();

            $newclient12 = DB::table('commonregisters')->where('id', $user_id)->first();
            $commonuser = DB::table('commonregisters')->select('commonregisters.check_status', 'commonregisters.company_name', 'commonregisters.filename', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->where('status', '=', 'Active')->orderby('filename', 'asc')->get();
            $linkuser = DB::table('commonregisters')->select('commonregisters.productid', 'commonregisters.company_name', 'commonregisters.filename', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->whereRaw('FIND_IN_SET("' . $newclient12->filename . '",productid)')->first();

            return view('client/home', compact(['commonyy', 'commonuser', 'linkuser', 'common', 'category', 'business', 'businessbrand', 'cd', 'schedule', 'set', 'set1', 'employee', 'monday', 'friday']));
        }


    }


    public function fetch33(Request $request)
    {
        $user_id = Auth::user()->id;
        if ($request->get('query')) {
            $query = $request->get('query');


            $commonregiser = DB::table('commonregisters')
                ->select('commonregisters.type as Type', 'commonregisters.id as cid', 'commonregisters.filename as entityid', 'commonregisters.first_name as fname', 'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname', 'commonregisters.company_name as cname', 'commonregisters.business_no', 'commonregisters.business_name',
                    'commonregisters.business_id', 'commonregisters.status', 'commonregisters.business_catagory_name')->where('business_catagory_name', 'LIKE', "%{$query}%")->orwhere('id', '=', $user_id)->get();


            $datas = $commonregiser;


            $output = '';
            $i;
            foreach ($datas as $row) {
                // print_r($row);
                // echo $row->filename;
                // echo $query;
                if ($row->status == '1' || $row->status == 'Active') {

                    if ($row->Type == 'employee' || $row->Type == 'Vendor' || $row->Type == 'clientemployee') {
                        $id = $row->cid;
                        $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                        if ($row->Type == 'employee') {
                            $types1 = 'EE';
                        }
                        // else if($row->Type =='user')
                        // {
                        //     $types1='U';
                        // }
                        else if ($row->Type == 'Vendor') {
                            $types1 = 'V';
                        } else if ($row->Type == 'clientemployee') {
                            $types1 = 'C-EE';

                        }


                    } else {


                        if ($row->business_id == '6') {
                            $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                            $types1 = "C";
                            $id = $row->cid;
                        } else {
                            $names = $row->cname;
                            $types1 = "C";
                            $id = $row->cid;
                        }
                    }
                    ?>
                    <li style="text-align:left;"><a onclick="myfun('<?php echo $id; ?>','<?php echo $types1; ?>')"><span class="bgcolors"><?php echo $types1; ?></span><span class="clientalign"><?php if (isset($row->entityid) != '') {
                                    echo $row->entityid . ' ';
                                } else {
                                    echo $id . ' ';
                                } ?></span><span class="entityname"><?php echo $names; ?></span></a></li>
                    <?php
                }


            }
            $output .= '';
            echo $output;
        }
    }

    public function fetch333(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');


        if ($type == 'C') {
            $commonregiser = DB::table('commonregisters')
                ->select('commonregisters.email', 'commonregisters.business_no as telephone', 'commonregisters.mobile_1 as telephone2',
                    'commonregisters.businesstype as telephonetype', 'commonregisters.eteletype1 as telephonetype2', 'commonregisters.businessext as ext1', 'commonregisters.type as Type',
                    'commonregisters.id as cid', 'commonregisters.filename as entityid', 'commonregisters.first_name as fname', 'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname', 'commonregisters.company_name as cname',
                    'commonregisters.business_id', 'commonregisters.company_name', 'commonregisters.firstname', 'commonregisters.lastname',
                    'commonregisters.etelephone1 as telephoneNo2', 'commonregisters.eteletype1 as telephoneNo2Type', 'commonregisters.eext1 as ext2', 'commonregisters.personalemail as fscemail')->where('id', '=', $id)->where('status', '=', 'Active')->first();


        } else if ($type == 'EE' || $type == 'V' || $type == 'C-EE') {

            $commonregiser = DB::table('employees')
                ->select('employees.email', 'employees.telephoneNo1 as telephone', 'employees.etelephone1 as telephone2', 'employees.telephoneNo1Type as telephonetype',
                    'employees.eteletype1 as telephonetype2', 'employees.ext1', 'employees.type as Type', 'employees.id as cid', 'employees.employee_id as entityid',
                    'employees.firstName as fname', 'employees.middleName as mname', 'employees.lastName as lname', 'employees.business_id', 'employees.business_name as company_name',
                    'employees.firstname', 'employees.lastname', 'employees.telephoneNo2', 'employees.telephoneNo2Type', 'employees.ext2', 'employees.fscemail')
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //echo "<pre>";print_r($commonregiser);
        }
        echo json_encode($commonregiser);
        exit;

    }


    public function getusers(Request $request, $filename)
    {

        $k = 0;
        foreach ($_REQUEST['filenamess2'] as $filenamess) {


            $filenamestring = $filenamess[$k];
            $newclientdata = DB::table('commonregisters')->select('id', 'filename')->where('filename', $filenamess)->first();
            $userclientdata = DB::table('users')->where('client_id', $newclientdata->filename)->first();
            $user_id = Auth::user()->id;
            $userclientlogin = DB::table('users')->where('id', $user_id)->first();
            $email = $userclientlogin->email;
            $password = $userclientlogin->password;
            $newpassword = $userclientlogin->newpassword;
            $checkstatus = 1;
            $k++;
            if ($userclientdata == '') {
                $insert2 = DB::insert("insert into users(`client_id`,`email`,`password`,`newpassword`,`check_status`) values('" . $filenamess . "','" . $email . "','" . $password . "','" . $newpassword . "','" . $checkstatus . "')");
                $insert3 = DB::table('commonregisters')->where('filename', '=', $filenamess)
                    ->update([

                        'check_status' => 1
                    ]);
            } else {
                $returnValue = DB::table('users')->where('client_id', '=', $filenamess)
                    ->update([
                        'email' => $userclientlogin->email,
                        'password' => $userclientlogin->password,
                        'newpassword' => $userclientlogin->newpassword,
                        'check_status' => 1
                    ]);


                $insert3 = DB::table('commonregisters')->where('filename', '=', $filenamess)
                    ->update([

                        'check_status' => 1
                    ]);
            }
        }
        return redirect()->back();
    }



    // public function getusers(Request $request,$filename)
    // {
    //   // echo $filename;
    //   //print_r($_REQUEST);die;


    //     $k=0;
    //     foreach($_REQUEST['filenamess2'] as $filenamess)
    //     {


    //         $newclientdata = DB::table('commonregisters')->where('filename',$filenamestring)->first();
    //         $userclientdata = DB::table('users')->where('user_id',$newclientdata->id)->where('client_id',$newclientdata->filename)->first();
    //         $user_id  = Auth::user()->id;
    //         $userclientlogin = DB::table('users')->where('id',$user_id)->first();
    //         $email=$userclientlogin->email;
    //         $password=$userclientlogin->password;
    //         $newpassword=$userclientlogin->newpassword;    
    //         $checkstatus=1;    
    //          $filenamestring =$filenamess[$k];
    //         $k++;    
    //           if($userclientdata!='')
    //           {

    //                 $insert2 = DB::insert("insert into users(`client_id`,`email`,`password`,`newpassword`,`check_status`) values('".$filenamestring."','".$email."','".$password."','".$newpassword."','".$checkstatus."')");        
    //                 $insert3 = DB::table('commonregisters')->where('filename', '=', $filenamess)
    //                         ->update([

    //                             'check_status' => 1
    //                         ]);
    //           }
    //           else
    //           {
    //               $userclientloginexist = DB::table('users')->where('client_id',$filenamestring)->first();
    //               $returnValue = DB::table('users')->where('client_id', '=', $filenamess)
    //                         ->update([
    //                             //'client_id' => $filenamestring,
    //                             'email' => $userclientlogin->email,
    //                             'password' => $userclientlogin->password,
    //                             'newpassword' => $userclientlogin->newpassword,
    //                             'check_status' => 1
    //                         ]);


    //               $insert3 = DB::table('commonregisters')->where('filename', '=', $filenamess)
    //                         ->update([

    //                             'check_status' => 1
    //                         ]);
    //           }


    //     }

    //     //die;


    //   return redirect()->back();
    // }


    // public function getusers($filename)
    // {

    //     //echo $filename;
    //     $newclientdata = DB::table('commonregisters')->where('filename',$filename)->first();
    //     $userclientdata = DB::table('users')->where('user_id',$newclientdata->id)->where('client_id',$newclientdata->filename)->first();
    //     $user_id  = Auth::user()->id;
    //     $userclientlogin = DB::table('users')->where('id',$user_id)->first();
    //     $userclientlogin->email;
    //     $userclientlogin->password;
    //     $userclientlogin->newpassword;
    //   //print_r($userclientdata);die;
    //   if($userclientdata!='')
    //   {
    //       $returnValue = DB::table('users')
    //                 ->insert([

    //                     'client_id' => $filename,
    //                     'email' => $userclientlogin->email,
    //                     'password' => $userclientlogin->password,
    //                     'newpassword' => $userclientlogin->newpassword

    //                 ]);
    //   }
    //   else
    //   {
    //       $userclientloginexist = DB::table('users')->where('client_id',$filename)->first();
    //       $returnValue = DB::table('users')->where('client_id', '=', $userclientloginexist->client_id)
    //                 ->update([

    //                     'client_id' => $filename,
    //                     'email' => $userclientlogin->email,
    //                     'password' => $userclientlogin->password,
    //                     'newpassword' => $userclientlogin->newpassword

    //                 ]);
    //   }
    //   return redirect()->back();
    // }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::All();
        $business = Business::all();
        $businessbrand = BusinessBrand::All();
        $cb = Categorybusiness::All();
        $user_id = Auth::user()->user_id;
        $common = DB::table('commonregisters')->select('commonregisters.id as cid', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name')
            ->leftJoin('categories', function ($join) {
                $join->on('commonregisters.business_cat_id', '=', 'categories.id');
            })
            ->leftJoin('businesses', function ($join) {
                $join->on('commonregisters.business_id', '=', 'businesses.id');
            })
            ->leftJoin('business_brands', function ($join) {
                $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
            })
            ->leftJoin('categorybusinesses', function ($join) {
                $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
            })
            ->where('commonregisters.id', '=', $user_id)->get()->first();
        return view('client/home', compact(['common', 'category', 'business', 'businessbrand', 'cb']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        if ($request->hasFile('pf1')) {
            $filname = $request->pf1->getClientOriginalName();
            $request->pf1->move('public/pf1', $filname);
        } else {
            $filname = '';
        }
        if ($request->hasFile('pf2')) {
            $filname1 = $request->pf2->getClientOriginalName();
            $request->pf2->move('public/pf2', $filname);
        } else {
            $filname1 = '';
        }


        $email = $request->email;
        $business_id = $request->business_id;
        $business_cat_id = $request->business_cat_id;
        $business_brand_id = $request->business_brand_id;
        $business_brand_category_id = $request->business_brand_category_id;
        $company_name = $request->company_name;
        $business_name = $request->business_name;
        $address = $request->address;
        $address1 = $request->address1;
        $countryId = $request->countryId;
        $city = $request->city;
        $stateId = $request->stateId;
        $zip = $request->zip;
        $mailing_address = $request->mailing_address;
        $mailing_address1 = $request->mailing_address1;
        $mailing_city = $request->mailing_city;
        $mailing_state = $request->mailing_state;
        $mailing_zip = $request->mailing_zip;
        $business_no = $request->business_no;
        $businesstype = $request->businesstype;
        $businessext = $request->businessext;
        $business_fax = $request->business_fax;
        $website = $request->website;
        $user_type = $request->user_type;
        $nametype = $request->nametype;
        $first_name = $request->first_name;
        $middle_name = $request->middle_name;
        $last_name = $request->last_name;
        $contact_address1 = $request->contact_address1;
        $contact_address2 = $request->contact_address2;
        $city_1 = $request->city_1;
        $state = $request->state;
        $zip_1 = $request->zip_1;
        $mobile_1 = $request->mobile_1;
        $mobiletype_1 = $request->mobiletype_1;
        $ext2_1 = $request->ext2_1;
        $mobile_2 = $request->mobile_2;
        $mobiletype_2 = $request->mobiletype_2;
        $ext2_2 = $request->ext2_2;
        $contact_fax_1 = $request->contact_fax_1;
        $startdate = date('Y-m-d');
        $resetdays = $request->resetdays;
        $enddate = $request->reset_date;


        //$personname = $request->personname;
//$relation = $request->relation;
        // $eaddress1 = $request->eaddress1;
        //$per_city = $request->per_city;
        //$etelephone1 = $request->etelephone1;
        //$per_stateId = $request->per_stateId;
        //$per_zip = $request->per_zip;
        //$eext1 = $request->eext1;
        //$eext2 = $request->eext2;
        //$eteletype1 = $request->eteletype1;
        // $eteletype2 = $request->eteletype2;
        //$etelephone2 = $request->etelephone2;
        //$comments = $request->comments;
        // $marital = $request->marital;
        // $month = $request->month;
        //$day = $request->day;
        //$year = $request->year;
        // $pf1 = $filname;
        //  $pf2 = $filname1;
        // $eteletype3 = $request->eteletype3;
        //$eteletype4 = $request->eteletype4;
        //  $gender = $request->gender;

        $user = User::find(auth()->user()->id);
        $business = User::find($id);
        $user_id = Auth::user()->user_id;
        $business->password = Hash::make($request->newpassword);
        $business->newpassword = $request->newpassword;
        $business->question1 = $request->question1;
        $business->question2 = $request->question2;
        $business->question3 = $request->question3;
        $business->answer1 = $request->answer1;
        $business->answer2 = $request->answer2;
        $business->answer3 = $request->answer3;
        $business->resetdays = $resetdays;
        $business->remaining_day = $resetdays;
        $business->startdate = $startdate;
        $business->active = '1';
        $business->enddate = $enddate;
        $returnValue = DB::table('commonregisters')
            ->where('id', '=', $user_id)
            ->update([
                'business_name' => $business_name,
                'company_name' => $company_name,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'last_name' => $last_name,
                'firstname' => $first_name,
                'middlename' => $middle_name,
                'lastname' => $last_name,
                'nametype' => $nametype,
                'email' => $email,
                'address' => $address,
                'address1' => $address1,
                'city' => $city,
                'stateId' => $stateId,
                'zip' => $zip,
                'countryId' => $countryId,
                'mailing_address' => $mailing_address,
                'mailing_address1' => $mailing_address,
                'mailing_city' => $mailing_city,
                'mailing_state' => $mailing_state,
                'mailing_zip' => $mailing_zip,
                'business_no' => $business_no,
                'business_fax' => $business_fax,
                'website' => $website,
                'user_type' => $user_type,
                //'business_id' => $business_id,
                //'business_cat_id' => $business_cat_id,
                //'business_brand_id' => $business_brand_id,
                //'business_brand_category_id' => $business_brand_category_id,
                'contact_address1' => $contact_address1,
                'contact_address2' => $contact_address2,
                'city_1' => $city_1,
                'state_1' => $state,
                'zip_1' => $zip_1,
                'mobile_1' => $mobile_1,
                'mobiletype_1' => $mobiletype_1,
                'ext2_1' => $ext2_1,
                'mobile_2' => $mobile_2,
                'mobiletype_2' => $mobiletype_2,
                'ext2_2' => $ext2_2,
                'contact_fax_1' => $contact_fax_1,
            ]);
        $business->update();
        return redirect('client/home')->with('success', 'Your Profile Successfully Update');

    }

    public function getpassword(Request $request)
    {

        $data = $request->all();
        $user = User::find(auth()->user()->id);
        if (!Hash::check($data['oldpassword'], $user->password)) {
            $isAvailable = false;
        } else {
            $isAvailable = true;
        }
        echo json_encode(
            array('valid' => $isAvailable));
    }


    public function getcategory2(Request $request)
    {
        $data = Category::select('business_cat_name', 'id')->where('bussiness_name', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function getcategory111(Request $request)
    {
        $data = Business::select('bussiness_name', 'id')->where('id', $request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}