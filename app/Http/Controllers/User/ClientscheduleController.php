<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\empschedule;
use App\Model\Schedule;
use App\Model\Schedulesetup;
use Auth;
use DB;
use Illuminate\Http\Request;

class ClientscheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user()->user_id;
        //return $start;
        $branch = Branch::All();
        // return $branch;
        $schedule = Schedule::where('type', '=', 'clientemployee')->where('entity_id', $userid)->get();
        // return $schedule;
        $emp = Employee::All();
        $empschedule = empschedule::All();
        $schedulesetup = Schedulesetup::All();
        return view('client.clientschedule.clientschedule', compact(['branch', 'schedule', 'emp', 'empschedule', 'schedulesetup']));
    }

    public function getdurationempclient2(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $data = Employee::select('pay_frequency', 'id', 'firstName', 'middleName', 'lastName', 'clienttype')->where('clienttype', $user_id)->where('pay_frequency', $request->id)->where('check', '1')->where('type', $request->type1)->take(100)->get();
        //$data1 = $data->pay_frequency;
        //$data2 = Schedulesetup::select('duration','id','sch_start_date','sch_start_day','sch_end_date','sch_end_day')->where('duration',$data1)->take(100)->get();
        return response()->json($data);
    }

    public function getduration3(Request $request)
    {
        $data = Employee::select('pay_frequency', 'id')->where('id', $request->id)->first();
        $data1 = $data->pay_frequency;
        $data2 = Schedulesetup::select('*')->where('duration', $data1)->take(100)->get();
        return response()->json($data2);
    }


    public function getEmpcity2(Request $request)
    {

        $data = Employee::select('firstName', 'lastName', 'id', 'clienttype', 'type', 'check', 'branch_city')->where('branch_city', $request->id)->where('type', 'clientemployee')->where('clienttype', $request->loginemp_id)->where('check', '1')->take(100)->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = Branch::All();
        return view('client/clientschedule/create', compact('branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'emp_city' => 'required',
            'emp_name' => 'required|unique:schedules',
            'duration' => 'required',
            'sch_start_date' => 'required',
        ]);
        $position = new Schedule;
        $position->emp_city = $request->emp_city;
        $position->type = 'clientemployee';
        $position->emp_name = $request->emp_name;
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_end_date = $request->sch_end_date;
        $position->schedule_in_time = $request->schedule_in_time;
        $position->schedule_out_time = $request->schedule_out_time;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_day = $request->sch_end_day;
        $position->entity_id = $request->entity_id;

        $position->save();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;
        $date_to1 = $request->sch_end_date;
        $date_to = strtotime($date_to);

        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            $day = date("D", $i);
            if ($day == 'Sun') {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $lastId . "','" . $i . "','','')");
            } else if ($day == 'Sat') {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $lastId . "','" . $i . "','','')");
            } else {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $lastId . "','" . $i . "','" . $date_from2 . "','" . $date_from3 . "')");
            }
        }
        return redirect('client/clientschedule')->with('success', 'Success fully add Schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    public function employee2(Request $request)
    {
        $data = Employee::select('firstName', 'middleName', 'lastName', 'id')->where('branch_city', $request->id)->where('type', 'clientemployee')->take(100)->get();
        return response()->json($data);
    }

    public function clientemployee2(Request $request)
    {
        $data = Employee::select('firstName', 'middleName', 'lastName', 'id')->where('branch_city', $request->id)->where('type', 'clientemployee')->where('clienttype', $request->client)->take(100)->get();
//return $data;
        return response()->json($data);
    }

    public function getduration12(Request $request)
    {
        $data = Employee::select('pay_frequency', 'id')->where('id', $request->id)->first();
        $data1 = $data->pay_frequency;
        $data2 = Schedulesetup::select('duration', 'id', 'sch_start_date', 'sch_start_day', 'sch_end_date', 'sch_end_day')->where('type', 'clientemployee')->where('duration', $data1)->take(100)->get();
//return $data2;
        return response()->json($data2);
    }

    public function schedules2(Request $request)
    {
        if ($request->companyId1 == '1' && !empty($request->companyId2)) {
            $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['status' => 0, 'clockin' => $request->companyId2, 'clockout' => $request->companyId3]);
        } else {
            $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['status' => 1, 'clockin' => '', 'clockout' => '']);
        }
        return response()->json($returnValue);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::All();

        $schedule = Schedule::where('id', $id)->first();
        $na = $schedule->emp_name;
        $na1 = $schedule->id;
        $duration = $schedule->duration;
        $emp = Employee::where('id', $na)->first();
        $empschedule = empschedule::where('emp_sch_id', $na1)->orderBy('id', 'asc')->get();
        $schedulesetup = Schedulesetup::where('duration', '=', $duration)->first();
        return View('client.clientschedule.edit', compact(['branch', 'schedule', 'emp', 'empschedule', 'schedulesetup']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'emp_city' => 'required',
            'emp_name' => 'required',
            'duration' => 'required',
            'sch_start_date' => 'required',
        ]);
        $position = $schedule;
        $position->emp_city = $request->emp_city;
        $position->emp_name = $request->emp_name;
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_end_date = $request->sch_end_date;
        $position->schedule_in_time = $request->schedule_in_time;
        $position->schedule_out_time = $request->schedule_out_time;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_day = $request->sch_end_day;
        $position->update();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;
        $date_to1 = $request->sch_end_date;
        $date_to = strtotime($date_to);
//$date_to4 = $request->schedule_date; 
//$date_to4 = strtotime($date_to4);
        $na1 = $schedule->id;
//$empschedule = empschedule::where('emp_sch_id',$na1)->where('date_1',$date_to4)->get();
        for ($i = $date_from; $i <= $date_to; $i += 86400) {


//$insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
//DB::table('schedule_emp_dates')->where('emp_sch_id', $lastId)->where('date_1','!=', $i)->delete();
        }
        $schedule_date = $request->schedule_date;
        $schedule_clockin = $request->schedule_clockin;
        $schedule_id = $request->schedule_id;
        $schedule_clockout = $request->schedule_clockout;
        $j = 0;
        foreach ($schedule_date as $post) {
            $schedule_date1 = $schedule_date[$j];
            $schedule_date1 = strtotime($schedule_date1);
            $day = date("D", $schedule_date1);
            $schedule_clockin1 = $schedule_clockin[$j];
            $schedule_id1 = $schedule_id[$j];
            $schedule_clockout1 = $schedule_clockout[$j];
            $j++;
            if ($day == 'Sun') {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => '',
                        'clockout' => '',

                    ]);
            } else if ($day == 'Sat') {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => '',
                        'clockout' => '',

                    ]);
            } else {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => $schedule_clockin1,
                        'clockout' => $schedule_clockout1,

                    ]);
            }
            //return $schedule_date1;

        }
        return redirect('client/clientschedule')->with('success', 'Success fully Update Schedule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::where('id', $id)->delete();
        return redirect(route('clientschedule.index'))->with('success', 'Success Fully Delete Record');
    }


    public function Clockin(Request $request)
    {
        $student = empschedule::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }
}