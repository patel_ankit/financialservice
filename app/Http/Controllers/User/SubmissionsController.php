<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Auth;
use DB;

class SubmissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('client/submissions/submissions');
    }


}