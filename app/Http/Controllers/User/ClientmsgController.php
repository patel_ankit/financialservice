<?php

namespace App\Http\Controllers\User;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Employee;
use App\Model\Msg;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class ClientmsgController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Auth::user()->id;
        $emp = User::All();
        $admin = Admin::All();
        $user_id = Auth::user()->user_id;
        $employee = Commonregister::where('id', '=', $user_id)->first();
        $task = Msg::where('type', '=', 'Approval')->get();

        return view('client/clientmsg/clientmsg', compact(['task', 'emp', 'admin', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $purpose = DB::table('purposes')->get();
        $employee = User::where('id', '=', $user_id)->first();
        //$employee = Employee::where('id','=',$user_id)->first();
        $client = Commonregister::where('status', '=', 'Active')->orderBy('first_name', 'asc')->get();
        $employee1 = Fscemployee::where('user_id', '!=', $user_id)->where('type', '=', '1')->orderBy('name', 'asc')->get();
        return view('client/clientmsg/create', compact(['emp', 'employee', 'client', 'employee1', 'purpose']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start = Auth::user()->id;
        $start1 = Auth::user()->user_id;
        if (empty($request->busname2)) {
            $names_str = '';
        } else {
            $names_arr = $request->busname2;
            $names_str = implode(" , ", $names_arr);
        }
        $position = new Msg;
        $position->busname2 = $names_str;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->day = $request->day;
        $position->title = $request->type;
        $position->content = $request->othermsg;
        if (empty($request->clientname)) {
            $position->admin_id = $request->employee;
        } else {
            $position->admin_id = $request->clientname;
        }
        $position->employee_id = $start1;
        $position->employeeid = $request->employees;
        $position->type = $request->type;
        $position->rlt_msg = $request->other;
        $position->call_back = $request->purpose1;
        $position->status = 1;
        $position->busname = $request->busname;
        $position->clientfile = $request->clientfile;
        $position->clientno = $request->clientno;
        $position->clientname = $request->clientname;
        $position->purpose = $request->purpose;
        $position->send = 1;
        $position->send2 = 1;
        $position->recieve = 1;
        $position->status = 2;
        $position->save();
        return redirect('client/clientmsg')->with('success', 'Success fully add Message');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Msg::where('id', $id)->first();
        $user_id = Auth::user()->user_id;
        $employee1 = Commonregister::where('id', '=', $user_id)->first();
        $employee = Commonregister::where('id', '=', $user_id)->first();
        $emp = Fscemployee::All();
        $admin = Admin::All();
        return View('client.clientmsg.edit', compact(['task', 'emp', 'admin', 'employee1', 'employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'employee' => 'required',
        ]);
        $position = Msg::find($id);
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $request->employee;
        $position->update();
        return redirect('client/clientmsg')->with('success', 'Success fully update Message');
    }


    public function getmessagesclient12(Request $request)
    {
        $user_id = Auth::user()->user_id;
        $employee = Employee::where('id', '=', $user_id)->first();
        if ($request->id == 'employee') {
            $data = Employee::select('firstName', 'id', 'middleName', 'lastName', 'type', 'employee_id', 'telephoneNo1')->where('check', '=', '1')->Where('type', $request->id)->take(1000)->get();
        } else if (($request->id) and ($request->state == 'employee')) {
            $data = Employee::select('firstName', 'id', 'middleName', 'lastName', 'type', 'employee_id', 'telephoneNo1')->where('check', '=', '1')->where('id', $request->id)->take(1000)->get();
        } else {
            $data = Commonregister::select('first_name', 'id', 'middle_name', 'last_name', 'business_no', 'status', 'filename', 'email', 'business_name')->where('status', $request->id)->orWhere('id', $request->id)->take(1000)->get();
        }
        return response()->json($data);
    }

    public function getmessageclients1(Request $request)
    {
        $use = Commonregister::where('id', '=', $request->id)->first();
        $data = Commonregister::select('first_name', 'id', 'middle_name', 'company_name', 'business_name', 'last_name', 'business_no', 'status', 'email', 'filename', 'business_name')->Where('email', '=', $use->email)->take(1000)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Msg::where('id', $id)->delete();
        return redirect(route('msg.index'));
    }
}