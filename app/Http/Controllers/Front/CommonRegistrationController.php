<?php

namespace App\Http\Controllers\Front;

use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Mail\uservarification;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\Slider;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use Validator;

class CommonRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index(Request $request)
    {
        $request->all();
        $Category = Category::all();
        $businessBrand = BusinessBrand::all();
        $categorybusiness = Categorybusiness::all();
        $commonregistery = Commonregister::all();
        return view('comman-registration.create', compact(['business', 'category', 'businessBrand', 'Commonregister', 'slider']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $business_id = $request->input('business_id');
        $business_cat_id = $request->input('business_cat_id');
        $business = Business::all();
        $category = Category::all();
        $businessBrand = BusinessBrand::all();
        $categorybusiness = Categorybusiness::all();
        $commonregistery = Commonregister::all();
        $slider = Slider::All();
        return view('comman-registration.create', compact(['business', 'category', 'businessBrand', 'commonregistery', 'business_cat_id', 'business_id', 'slider', 'categorybusiness']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $bulletin = implode(",", $request->get('bulletin'));
        // return  $taxation = implode(',', $request->get('taxation'));
        $due_date = implode(',', $request->get('due_date'));
        $employee = implode(',', $request->get('employee'));
        $businessbrand = new Commonregister;
        $businessbrand->company_name = $request->company_name;
        $businessbrand->business_name = $request->business_name;
        $businessbrand->first_name = $request->first_name;
        $businessbrand->middle_name = $request->middle_name;
        $businessbrand->last_name = $request->last_name;
        $businessbrand->address = $request->address;
        $businessbrand->address1 = $request->business_address;
        $businessbrand->city = $request->city;
        $businessbrand->stateId = $request->stateId;
        $businessbrand->zip = $request->zip;
        $businessbrand->countryId = $request->countryId;
        $businessbrand->business_no = $request->business_no;
        $businessbrand->user_type = $request->user_type;
        $businessbrand->email = $request->email;
        $businessbrand->etelephone1 = $request->telephone1;
        $businessbrand->eteletype1 = $request->telephone1type;
        $businessbrand->eext1 = $request->telephone1ext;
        $businessbrand->etelephone2 = $request->telephone2;
        $businessbrand->eteletype2 = $request->telephone2type;
        $businessbrand->eext2 = $request->telephone2ext;
        $businessbrand->business_fax = $request->business_fax;
        $businessbrand->website = $request->website;
        $businessbrand->business_cat_id = $request->business_cat_id;
        $businessbrand->business_brand_id = $request->business_brand_id;
        $businessbrand->business_brand_category_id = $request->business_brand_category_id;
        $businessbrand->business_id = $request->business_id;
        $businessbrand->bulletin = $bulletin;
        $businessbrand->employee = $employee;
        $businessbrand->taxation = $request->taxation;
        $businessbrand->due_date1 = $due_date;
        $businessbrand->terms = $request->terms;
        $businessbrand->mobile_no = $request->mobile_no;
        $businessbrand->count = 1;
        $businessbrand->monthly_fee = $request->monthly_fee;
        $businessbrand->yearly_fee = $request->yearly_fee;
        $businessbrand->amount = $request->amount;
        $businessbrand->credit_card = $request->credit_card;
        $businessbrand->expire = $request->expire;
        $businessbrand->security_code = $request->security_code;
        $businessbrand->credit_card_holder_name = $request->credit_card_holder_name;
        $businessbrand->credit_address = $request->credit_address;
        $businessbrand->credit_city = $request->credit_city;
        $businessbrand->credit_state = $request->credit_state;
        $businessbrand->credit_zip = $request->credit_zip;
        $businessbrand->businesstype = $request->businesstype;
        $businessbrand->businessext = $request->businessext;
        $businessbrand->save();
        \Mail::to('vijay@businesssolutionteam.com')->send(new uservarification($businessbrand));
        return redirect('registrationthank')->with('success', 'You Are SuccessFully Registered');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getcategory2(Request $request)
    {

        $data = Category::select('business_cat_name', 'id')->where('bussiness_name', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function unique(Request $request)
    {
        if ($this->Commonregister->where('email', $request->get('email'))->exists()) {
            return \Response::json(false);
        } else {
            return \Response::json(true);
        }
    }

    public function getfilename(Request $request)
    {
        // return $request->id;
        $user = DB::table('commonregisters')->where('filename', $request->id)->whereNotNull('filename')->count();
        if (isset($user) && $user > 0) {
            echo 'false';

        } else {
            echo 'true';


        }
    }


    public function clientname()
    {

        $user = DB::table('commonregisters')->where('filename', Input::get('filename'))->count();
        if ($user > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function checkclientAvailability()
    {

        $silver = DB::table("commonregisters")->select("commonregisters.email");
        $user = DB::table("applyservicesses")->select("applyservicesses.email")->where('email', Input::get('email'))->unionAll($silver)->count();
        if ($user > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function checkclientAvailability11()
    {


        $user = DB::table("users")->select("users.email")->where('email', Input::get('email'))->count();
        if ($user == '1') {
            $isAvailable = true;
        } else {
            $isAvailable = false;
        }
        echo json_encode(array(
            'valid' => $isAvailable,
        ));
    }

    public function checkclientcheckclientids()
    {

        //  $silver = DB::table("commonregisters")->select("commonregisters.filename");
        $user = DB::table("users")->select("users.client_id")->where('client_id', Input::get('clientid'))->count();
        if ($user == '1') {
            $isAvailable = true;
        } else {
            $isAvailable = false;
        }
        echo json_encode(array(
            'valid' => $isAvailable,
        ));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}