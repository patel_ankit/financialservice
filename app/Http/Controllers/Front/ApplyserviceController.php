<?php

namespace App\Http\Controllers\Front;

use App\Front\Applyservice;
use App\Http\Controllers\Controller;
use App\Model\Citystate;
use App\Model\Servicetype;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApplyserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {

        $applyservice = Applyservice::all();
        $service = Servicetype::where('servicetype', '=', $id)->get();
        return view('apply-service/create', compact(['applyservice', 'service']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $applyservice = new Applyservice;
        $applyservice->serviceid = $request->serviceid;
        $applyservice->firstName = $request->firstName;
        $applyservice->middleName = $request->middleName;
        $applyservice->lastName = $request->lastName;
        $applyservice->telephoneNo1 = $request->telephoneNo1;
        $applyservice->email = $request->email;
        $applyservice->address = $request->address;
        $applyservice->address2 = $request->address2;
        $applyservice->telephoneNo1Type = $request->telephoneNo1Type;
        $applyservice->ext1 = $request->ext1;
        $applyservice->city = $request->city;
        $applyservice->stateId = $request->stateId;
        $applyservice->zip = $request->zip;
        $applyservice->countryId = $request->countryId;
        $applyservice->telephoneNo2Type = $request->telephoneNo2Type;
        $applyservice->ext2 = $request->ext2;
        $applyservice->fax = $request->fax;
        $applyservice->telephoneNo2 = $request->telephoneNo2;
        $applyservice->serviceTypeId = $request->serviceTypeId;
        $applyservice->note = $request->note;
        $applyservice->nametype = $request->nametype;
        $applyservice->save();

        return redirect('servicethanks');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function show(Applyservice $applyservice)
    {
        //
    }

    public function checkemailAvailability()
    {

        $user = DB::table('applyservices')->where('email', Input::get('email'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    public function getempid()
    {

        $user = DB::table('employees')->where('employee_id', Input::get('employee_id'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    public function getempid1()
    {

        $user = DB::table('employees')->where('email', Input::get('email'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function edit(Applyservice $applyservice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applyservice $applyservice)
    {
        //
    }

    public function getzipcode(Request $request)
    {

        $data = Citystate::select('country', 'state', 'city', 'county')->where('zipcode', $request->zip)->take(100)->get();

        return response()->json($data);
    }

    public function getstate(Request $request)
    {

        $data = Citystate::select('country', 'zipcode', 'city', 'county', 'state')->where('state', $request->state)->take(100)->get();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applyservice $applyservice)
    {
        //
    }
}