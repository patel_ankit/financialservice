<?php

namespace App\Http\Controllers\Front;

use App\Front\StoreContact;
use App\Http\Controllers\Controller;
use App\Model\Contact;
use App\Model\Link;
use DB;
use Hash;
use Illuminate\Http\Request;
use Mail;

class ContactShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::orderBy('id', 'desc')->first();
        $contact1 = StoreContact::All();
        return view('contacts', compact(['contact', 'contact1']));
    }

    public function DailyCronResponsibility()
    {
        exit('Test');
        $returnValue = DB::table('rules')->update(['flag' => '0']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $contact = Contact::orderBy('id', 'desc')->first();
        $link = Link::where('post_id', '=', $request->id)->get()->first();
        $contact1 = StoreContact::All();
        return view('contacts', compact(['contact', 'contact1', 'link']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store2()
    {
        $id = $_REQUEST['id'];

        DB::table('store_contacts')->where('id', $id)->update(['verifycheck' => '1']);

        return redirect()->back()->with('success', 'Your message successfully sent, we will contact soon...');

    }

    public function store(Request $request)
    {


        /*$google_recpachscrkey = "6Lcr7OAZAAAAALrw9Osn8B_0rI2F3XlBvnu2BJAf";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$google_recpachscrkey."&response=".$_POST['g-recaptcha-response']);
        $response = json_decode($response, true);
        if($response["success"] === true){
        echo "USER Form Submit Successfully.";
        }else{
        echo "You Simple are a robot";
        }
        */
        //print_r($_REQUEST);die;
        $name = $request->name;
        $email = $request->email;
        $telephone = $request->telephone;
        $message1 = $request->message;
        // $captcha= $request->captcha;
        //$post_id = $request->post_id;

        // $branch  = new StoreContact;
        // $branch->name = $request->name;
        // $branch->email = $request->email;
        // $branch->telephone = $request->telephone;
        // $branch->message = $request->message;
        // $branch->captcha = $request->captcha;
        // $branch->post_id = $request->post_id;

        $returnValue = DB::table('store_contacts')
            ->insertGetId([
                'name' => $name,
                'email' => $email,
                'telephone' => $telephone,
                'message' => $message1
            ]);

        $data = array('id' => $returnValue, 'email' => $request->email, 'name' => $name, 'telephone' => $telephone, 'msg' => $message1, 'from' => 'vijay2@businesssolutionteam.com');
        \Mail::send('contactinfo', $data, function ($message) use ($data) {
            $message->to($data['email'])->from($data['id'], $data['from'], $data['name'], $data['msg'], $data['telephone'])->subject('Welcome!');
        });

        //     return redirect('contacts')->with('success','Please check your email to confirm & click on the link to confirm to your email verification than it will goes to contact.');
        // }
        // else
        // {    
        //     $branch->verify = 2;
        //     $branch->save();
        //     return redirect('consuccess')->with('success','     Your message successfully sent, we will contact soon...');

        // }

        return redirect()->back()->with('success', 'Please Check Your email and verify data...');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = StoreContact::find($id);
        $branch->verify = 2;
        $branch->update();
        return redirect('consuccess')->with('success', 'Sent your message successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}