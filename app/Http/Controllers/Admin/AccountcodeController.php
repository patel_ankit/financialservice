<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Accountcode;
use Illuminate\Http\Request;

class AccountcodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountcode = Accountcode::orderBy('accountcode', 'asc')->get();
        return view('fac-Bhavesh-0554.accountcode.accountcode', compact('accountcode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accountcode = Accountcode::orderBy('accountcode', 'asc')->get();
        return view('fac-Bhavesh-0554.accountcode.create', compact('accountcode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'accountcode' => 'required|unique:accountcodes,accountcode',
            'account_name' => 'required',
            'account_belongs_to' => 'required',
        ]);


        $business = new Accountcode;
        $business->accountcode = $request->accountcode;
        $business->account_name = $request->account_name;
        $business->account_belongs_to = $request->account_belongs_to;
        $business->save();
        //    return redirect(route('accountcode.index'));
        return redirect(route('accountcode.index'))->with('success', 'Account Code Added Seccessfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accountcode = Accountcode::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.accountcode.accountcode', compact('accountcode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accountcode1 = Accountcode::orderBy('accountcode', 'asc')->get();
        $accountcode = Accountcode::where('id', $id)->first();
        return View('fac-Bhavesh-0554.accountcode.edit', compact(['accountcode', 'accountcode1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'accountcode' => 'required',
            'account_name' => 'required',
            'account_belongs_to' => 'required',
        ]);

        $business = Accountcode::find($id);
        $business->accountcode = $request->accountcode;
        $business->account_name = $request->account_name;
        $business->account_belongs_to = $request->account_belongs_to;
        $business->update();
        return redirect(route('accountcode.index'))->with('success', 'Account Code Updated Seccessfully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Accountcode::where('id', $id)->delete();
        return redirect(route('accountcode.index'))->with('success', 'Account Code Deleted Seccessfully ');
    }
}