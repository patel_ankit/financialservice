<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Howtodo;
use DB;
use Illuminate\Http\Request;

class HowtodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $howtodo = DB::table('howtodos_steps')->where('noteid', '=', null)->get();
        $howtodo1 = DB::table('howtodos_steps')->get();
        $homecontent = Howtodo::All();
        //echo "<pre>";
        //print_r($homecontent);die;
        return view('fac-Bhavesh-0554/howtodo/howtodo', compact(['howtodo', 'homecontent', 'howtodo1']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('fac-Bhavesh-0554/howtodo/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [


        ]);

        $position = new Howtodo;
        $position->subject = $request->subject;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $position->software = $request->software;
        $noteid = $request->stepid;
        $adminnotes = $request->step;
        $k = 0;
        $position->save();
        $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
        foreach ($adminnotes as $notess) {
            $path1 = public_path() . '/adminupload/' . $_FILES['photo']['name'][$k];


            if (isset($_FILES['photo']['name'][$k]) != '') {
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$k], $path1)) {
                    $copy = $_FILES['photo']['name'][$k];
                } else {
                    $copy = '';
                }
            }

            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $img = $copy;
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`,`photo`) values('" . $note1 . "','" . $iddd . "','" . $img . "')");
            } else {
                $returnValue = DB::table('howtodos_steps')->where('id', '=', $noteid1)
                    ->update(['steps' => $note1,
                        'howtodo_id' => $id,
                        'noteid' => $noteid3,
                    ]);
            }
        }
//return redirect('fac-Bhavesh-0554/howtodo/edit' , $iddd)->with('success','New How To Do Added Successfully');   
//return back()->with('success','New How To Do Added Successfully');
//return redirect()->route('howtodo.index');
        return redirect('fac-Bhavesh-0554/howtodo')->with('success', 'New How To Do Added Successfully');

        //return back()->with('success','Success fully Add How To Do');

    }

    public function howtodo11delete($id)
    {
        // Contact_userinfo::where('id',$id)->delete();
        DB::table('howtodos_steps')->where('id', '=', $id)->delete();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $homecontent = Howtodo::where('id', $id)->first();
        $howtodo = DB::table('howtodos_steps')->where('howtodo_id', '=', $id)->get();
        $howtodo1 = DB::table('howtodos_steps')->where('howtodo_id', '=', $id)->get();
        return view('fac-Bhavesh-0554.howtodo.edit', compact(['homecontent', 'howtodo', 'howtodo1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Howtodo::find($id);
        $position->subject = $request->subject;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $position->software = $request->software;
        $noteid = $request->stepid;
        $noteid2 = $request->noteid;
        $photo2 = $request->photos2;
        $adminnotes = $request->step;
        $k = 0;
        $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
        // echo "<pre>";print_r($_POST['photos2']);exit;
        foreach ($adminnotes as $notess) {
            $path1 = public_path() . '/adminupload/' . $_FILES['photo']['name'][$k];


            if (move_uploaded_file($_FILES['photo']['tmp_name'][$k], $path1)) {
                $filesname1 = $_FILES['photo']['name'][$k];
            } else {
                if (isset($_POST['photos2'][$k]) != '') {
                    $filesname1 = $_POST['photos2'][$k];
                } else {
                    $filesname1 = '';
                }

            }

            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $noteid3 = $noteid2[$k];
            $img = $filesname1;
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`,`noteid`,`photo`) values('" . $note1 . "','" . $iddd . "','" . $noteid3 . "','" . $img . "')");
                //$insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`,`noteid`) values('".$note1."','".$iddd."','".$noteid3."')");
            } else {
                $returnValue = DB::table('howtodos_steps')->where('id', '=', $noteid1)
                    ->update([
                        'steps' => $note1,
                        'howtodo_id' => $id,
                        'noteid' => $noteid3,
                        'photo' => $img,
                    ]);
                //$users = DB::table('howtodos_steps')->where('steps','')->delete();
            }
        }

        $position->update();
        // return back()->with('success','Success fully update How To Do');
        return redirect('fac-Bhavesh-0554/howtodo')->with('success', 'Success fully update How To Do');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Howtodo::where('id', $id)->delete();
        return back();
    }
}