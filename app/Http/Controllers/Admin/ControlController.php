<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Control;
use DB;
use Illuminate\Http\Request;

class ControlController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $control = Control::All();
        return view('fac-Bhavesh-0554/control/control', compact(['control']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/control/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'controlname' => 'required',
            'statename' => 'required',

        ]);
        $branch = new Control;
        $branch->controlname = $request->controlname;
        $branch->statename = $request->statename;

        $branch->save();
        return redirect('fac-Bhavesh-0554/control')->with('success', 'Success fully add Control');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Control::where('id', $id)->first();

        return view('fac-Bhavesh-0554.control.edit', compact(['branch']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'controlname' => 'required',
            'statename' => 'required',
        ]);
        $branch = Control::find($id);
        $branch->controlname = $request->controlname;
        $branch->statename = $request->statename;
        $branch->update();
        return redirect('fac-Bhavesh-0554/control')->with('success', 'Success fully Update control');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Control::where('id', $id)->delete();
        return redirect(route('control.index'));

    }
}