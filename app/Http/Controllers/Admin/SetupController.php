<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Appointment_worktype;
use App\Model\Price;
use App\Model\Regards;
use Illuminate\Http\Request;

class SetupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = Price::All();
        return view('fac-Bhavesh-0554/setup/setup', compact('price'));
    }

    public function appointment()
    {
        $regards = Regards::All();
        $work_type = Appointment_worktype::All();
        return view('fac-Bhavesh-0554/setup/appointment', compact('regards', 'work_type'));
    }

    public function storeRegard(Request $request)
    {
        $regading = new Regards();
        $regading->regarding = $request->regarding;
        $regading->short_name = $request->short_name;
        $regading->save();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    public function updateRegard(Request $request)
    {
        $regading = new Regards();
        $regading = Regards::find($request->regard_id);
        $regading->regarding = $request->regarding;
        $regading->short_name = $request->short_name;
        $regading->save();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    public function delRegards($id)
    {
        $res = Regards::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    public function storeWorkType(Request $request)
    {
        $work_type = new Appointment_worktype();
        $work_type->worktype = $request->worktype;
        $work_type->short_name = $request->short_names;
        $work_type->color = $request->color;
        $work_type->save();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    public function updateWorkType(Request $request)
    {
        $work_type = new Appointment_worktype();
        $work_type = Appointment_worktype::find($request->work_id);
        $work_type->worktype = $request->worktype;
        $work_type->short_name = $request->short_names;
        $work_type->color = $request->color;
        $work_type->save();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    public function delWorkType($id)
    {
        $res = Appointment_worktype::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/setup/appointment');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}