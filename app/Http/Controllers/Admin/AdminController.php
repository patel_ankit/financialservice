<?php

namespace App\Http\Controllers;

use App\Front\Commonregister;
use App\Model\Admin;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\Home;
use App\User;
use Auth;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business = Business::All();
        $admin = Admin::All();
        $category = Category::All();
        $businessbrand = BusinessBrand::All();
        $commonregister = Commonregister::All();
        $cetegorybusiness = Categorybusiness::All();
        $user = User::All();
        $services = Service::All();
        $common1 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->groupBy('user_type')->get();


        return view('fac-Bhavesh-0554.home', compact(['common1', 'admin', 'services']));
    }

}