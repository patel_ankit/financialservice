<?php

namespace App\Http\Controllers\Admin;

use App\employee\Empschedule;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Schedule;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class FscemployeereportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'employee')->get();
        // print_r($emp);

        $schedule1 = Schedule::orderBy('emp_name', 'asc')->get();
        $start = date('Y-m-d', strtotime($request->startdate));
        $end = date('Y-m-d', strtotime($request->enddate));
        $thisdate = date('Y-m-d', strtotime($start));
        $thismonday = date('Y-m-d', strtotime('monday this week', strtotime($thisdate)));
        $thisfriday = date('Y-m-d', strtotime('next sunday', strtotime($thisdate)));
        $nextmonday = date('Y-m-d', strtotime('+1 day', strtotime($thisfriday)));
        $nextfriday = date('Y-m-d', strtotime('+6 day', strtotime($nextmonday)));
        $user_id = $request->emp_name;
        $schedule = Schedule::where('emp_name', '=', $user_id)->first();
        $employee2 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,clock_out_status,launch_in_status,launch_out_status,launch_in_status_1,launch_out_status_1')->whereBetween('emp_in_date', array($thismonday, $thisfriday))->get();
        $employee3 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,clock_out_status,launch_in_status,launch_out_status,launch_in_status_1,launch_out_status_1')->whereBetween('emp_in_date', array($nextmonday, $nextfriday))->get();
        $employee1 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second,clock_in_status,clock_out_status,launch_in_status,launch_out_status,launch_in_status_1,launch_out_status_1')->whereBetween('emp_in_date', array($start, $end))->get();
        // echo "<pre>";print_r($employee1);
        return view('fac-Bhavesh-0554.fscemployeereport/fscemployeereport', compact(['user_id', 'emp', 'employee1', 'employee2', 'employee3', 'schedule', 'schedule1']));
    }

    public function report()
    {
        return Response::json($employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'employee')->get();
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d', strtotime($request->date));
        $clock = Input::get('clockin') . ':' . Input::get('clockin_second') . ':' . '00';
        $clockout1 = Input::get('clockout') . ':' . Input::get('clockout_second') . ':' . '00';
        $lunch = $request->lunchin . ':' . $request->lunchin_second . ':' . '00';
        $lunchout = $request->lunchout . ':' . $request->lunchout_second . ':' . '00';
        $branch = new Empschedule;
        $branch->employee_id = $request->fullname;
        $branch->emp_in_date = $date;
        $branch->emp_in = $date . ' ' . $clock;
        $branch->emp_out = $date . ' ' . $clockout1;
        $branch->work_note = $request->note;
        $branch->clock_in_status = 2;
        $branch->clock_out_status = '2';
        if ($request->lunchin == null) {
            $branch->launch_in = '';
            $branch->launch_out = '';
        } else {
            $branch->launch_in = $date . ' ' . $lunch;
            $branch->launch_out = $date . ' ' . $lunchout;
            $branch->launch_in_status = '2';
            $branch->launch_out_status = '2';

        }
        $branch->ip_address = "Manually Add";
        $branch->save();
        if ($branch->save()) {
            return response()->json([
                'status' => 'success',
                'newopt' => $clock]);
        } else {
            return response()->json([
                'status' => 'error']);
        }
        //return view('fac-Bhavesh-0554.fscemployeereport/fscemployeereport',compact(['emp']));
    }

    public function reportedit1(Request $request)
    {
        $student = Empschedule::find($request->input('id'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Empschedule::find($id);
        return view('fac-Bhavesh-0554.fscemployeereport.edit', compact(['student']));
    }

    public function reportremove1(Request $request)
    {
        $student = Empschedule::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'employee')->get();
        $emp1 = Empschedule::where('id', '=', $id)->first();
        $branch = Empschedule::find($id);
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d', strtotime($request->date));
        $clock = $request->clockin . ':' . $request->clockin_second . ':' . '00';
        $clock1 = $request->clockout . ':' . $request->clockout_second . ':' . '00';
        $lunch = $request->lunchin . ':' . $request->lunchin_second . ':' . '00';
        $lunchout = $request->lunchout . ':' . $request->lunchout_second . ':' . '00';
        $branch->emp_in_date = $date;
        $branch->emp_in = $date . ' ' . $clock;
        $branch->emp_out = $date . ' ' . $clock1;
        $branch->work_note = $request->note;
        if ($request->lunchin == null) {
            $branch->launch_in = '';
            $branch->launch_out = '';
        } else {
            $branch->launch_in = $date . ' ' . $lunch;
            $branch->launch_out = $date . ' ' . $lunchout;
        }

        $clockin = $date . ' ' . $clock;
        $clockout = $date . ' ' . $clock1;
        $lin = $date . ' ' . $lunch;
        $lout = $date . ' ' . $lunchout;
        $dateA = $emp1->emp_in;
        $dateB = $clockin;
        if (($dateA > $dateB) || ($dateA < $dateB)) {
            $branch->clock_in_status = 2;
        }

        if (($emp1->emp_out > $clockout) || ($emp1->emp_out < $clockout)) {

            $branch->clock_out_status = '2';
        }

        if (($emp1->launch_in > $lin) || ($emp1->launch_in < $lin)) {
            $branch->launch_in_status = '2';
        }
        if (($emp1->launch_out > $lout) || ($emp1->launch_out < $lout)) {

            $branch->launch_out_status = '2';
        }
        /*
        if($branch->launch_in_second!=$lin1){
          $branch->launch_in_status_1= '2';
        }
        if($branch->launch_out_second!=$lout1){

          $branch->launch_out_status_1= '2';
        }*/
        $branch->ip_address = "Manually Add";
        $branch->update();
        if ($branch->update()) {
            return response()->json([
                'status' => 'success',
                'newopt' => $clock]);
        } else {
            return response()->json([
                'status' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}