<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\MyWork;
use DB;
use Excel;
use Illuminate\Http\Request;
use Input;
use Session;

class MyworkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mywork = DB::table('mywork')->where('flag', '0')->orderBy('priority_id')->get();
        return view('fac-Bhavesh-0554.mywork.mywork', compact('mywork'));
    }

    public function getWorkJsonData()
    {
        $mywork = DB::table('mywork')->where('flag', '0')->orderBy('priority_id')->get();
        echo json_encode($mywork);
    }

    public function getWorkJsonDataForUnder()
    {
        $mywork = DB::table('mywork')->where('flag', '1')->where('status', 'Under Process')->orderBy('priority_id')->get();
        echo json_encode($mywork);
    }

    public function getWorkJsonDataForWaiting()
    {
        $mywork = DB::table('mywork')->where('flag', '1')->where('status', 'Wating')->orderBy('priority_id')->get();
        echo json_encode($mywork);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.mywork.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "<pre>";
        print_r($_REQUEST);

        $store_work = new MyWork;


        $store_work->priority = $request->work_priority;

        if ($request->work_priority == "Urgent") {
            $store_work->priority_id = 1;
        } else if ($request->work_priority == "Time Sensitive") {
            $store_work->priority_id = 2;
        } else if ($request->work_priority == "Regular") {
            $store_work->priority_id = 3;
        } else if ($request->work_priority == "Appointment") {
            $store_work->priority_id = 4;
        }
        $store_work->type_of_work = $request->type_of_work;
        if (strlen($request->due_date) > 0) {
            $store_work->due_date = date('Y-m-d', strtotime($request->due_date));
        } else {
            $store_work->due_date = NULL;
        }
        $store_work->type_of_client = $request->client_type;
        if (strlen($request->client_id) > 0) {
            $store_work->client_id = $request->client_id;
        } else {
            $store_work->client_id = NULL;
        }
        $store_work->client_name = $request->client_name;
        $store_work->details = $request->details;
        $store_work->estimated_time = $request->est_time;
        $store_work->status = $request->status;
        $store_work->flag = 0;

        $store_work->save();

        //exit;
        return redirect(route('mywork.index'));


    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo $id;
        //$city= Zipcode::where('id',$id)->first();    
        //return View('admin.zipcode.edit',compact('city'));
    }

    public function getJsonOfWork($typeofwork)
    {
        $rows = array();
        if ($typeofwork == "0") {
            $mywork = DB::table('mywork')->where('flag', '0')->where('status', '!=', 'Under Progress')->where('status', '!=', 'Waiting')->orderBy('priority_id')->get();
        } else if ($typeofwork == "UP") {
            $mywork = DB::table('mywork')->where('status', 'Under Progress')->orderBy('priority_id')->get();
        } else if ($typeofwork == "W") {
            $mywork = DB::table('mywork')->where('status', 'Waiting')->orderBy('priority_id')->get();
        }


        foreach ($mywork as $work):
            $isUSelected = '';
            $isHSelected = '';
            $isWSelected = '';

            $action = '<a href="' . route("mywork.edit", $work->id) . '" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>';
            $action .= '  <a class="btn-action btn-delete" href="#" onclick="if(confirm("Are you sure, You want to delete this record ?")){} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>';

            if ($work->status == "Under Progress") {
                $isUSelected = 'Selected';
            } else if ($work->status == "Waiting") {
                $isWSelected = 'Selected';
            } else if ($work->status == "Hold") {
                $isHSelected = 'Selected';
            }

            if ($typeofwork != "UP" && $typeofwork != "W") {
                $option = '<select class="form-control myworkstatus" name="status" id="" onchange="updateStatus(this);">';
                $option .= '<option value="">Select</option>';
                $option .= '<option value="Under Progress" ' . $isUSelected . ' data-price="' . $work->id . '">Under Progress</option>';
                $option .= '<option value="Hold" ' . $isHSelected . ' data-price="' . $work->id . '">Hold</option>';
                $option .= '<option value="Waiting" ' . $isWSelected . ' data-price="' . $work->id . '">Waiting</option>';
                $option .= '</select>';
            } else {
                $option = $work->status;
            }
            array_push($rows, array(
                'No' => $work->id,
                'Priority' => $work->priority,
                'Priority_ID' => $work->priority_id,
                'Date' => $work->due_date != NULL ? date('m/d/Y', strtotime($work->due_date)) : '',
                'Time' => 'T',
                'Client ID' => $work->client_id,
                'Client Name' => $work->client_name,
                'Type of Work' => $work->type_of_work,
                'EstTime' => $work->estimated_time,
                'Status' => $option,
                'Action' => $action
            ));
        endforeach;

        echo json_encode(array('data' => $rows));
    }

    public function changeOrder($id, $orderid)
    {
        // echo $id . $orderid;
        // $query = DB::table('mywork')->where('id',$id)->update(array('status'=>$orderid));
        // echo "Success";

        if ($id != 0 && $orderid != 0) {

            $query1 = DB::table('mywork')->where('id', $id)->first();
            $query2 = DB::table('mywork')->where('id', $orderid)->first();
            $f = $query1->priority_id;
            $l = $query2->priority_id;

            if ($f == $l) {
                echo 'Same Group';
            } else {
                echo 'Id is :' . $id . "Move " . $query1->priority . " Group To :" . $query2->priority;
                $query = DB::table('mywork')->where('id', $id)->update(array(
                    'priority' => $query2->priority,
                    'priority_id' => $query2->priority_id
                ));

            }
        }


    }

    public function changeStatus($id, $statusval)
    {
        echo $id . $statusval;
        $query = DB::table('mywork')->where('id', $id)->update(array('status' => $statusval));
        echo "Success";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}