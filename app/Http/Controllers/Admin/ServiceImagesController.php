<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\ServiceImages;
use Illuminate\Http\Request;

class ServiceImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->id;
        $serviceimages = ServiceImages::where('service_id', $id)->get();
        return view('fac-Bhavesh-0554/serviceimages/serviceimages', compact('serviceimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('fac-Bhavesh-0554/serviceimages/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'serviceimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($request->hasFile('serviceimage')) {
            $filname = $request->serviceimage->getClientOriginalName();
            $request->serviceimage->move('public/serviceimage', $filname);
        }
        $srvice = ServiceImages::where('service_id', '=', $request->service_id)->first();
        if ($srvice === null) {
            $active = "active";
        } else {
            $active = "";
        }
        $category = new ServiceImages;
        $category->service_id = $request->service_id;
        $category->serviceimage = $filname;
        $category->active = $active;
        $category->save();
        return redirect(route('services.index'))->with('success', 'Successfully Upload Image');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviceImages = ServiceImages::where('id', $id)->first();
        return View('fac-Bhavesh-0554/serviceimages/edit', compact('serviceImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('serviceimage')) {
            $filname = $request->serviceimage->getClientOriginalName();
            $request->serviceimage->move('public/serviceimage', $filname);
        } else {
            $filname = $request->serviceimage1;
        }
        $category = serviceImages::find($id);
        $category->service_id = $request->service_id;
        $category->serviceimage = $filname;
        $category->update();
        return redirect(route('services.index'))->with('success', 'Successfully Upload Image');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceImages::where('id', $id)->delete();
        return redirect(route('services.index'));
    }
}