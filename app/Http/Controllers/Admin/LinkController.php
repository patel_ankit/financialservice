<?php

namespace App\Http\Controllers\Admin;

use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Link;
use App\Model\Linkcategory;
use DB;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $link = Link::All();
        return view('fac-Bhavesh-0554/link/link', compact('link'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commonregistery1 = Link::orderBy('post_id', 'desc')->first();
        if (!empty($commonregistery1)) {
            $commonid = $commonregistery1->post_id;
            $exp11 = explode('-', $commonid);
            $exp12 = $exp11[1];
            $exp13 = $exp11[0];
            $n = $exp12;
            $n2 = str_pad($n + 1, 4, 0, STR_PAD_LEFT);
            $cmppcode = $exp13 . '-' . $n2;
        } else {
            $cmppcode = "18-0001";
        }
        $client = Commonregister::All();
        return view('fac-Bhavesh-0554/link/create', compact(['cmppcode', 'client']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
        ]);


        $input = $request->all();
        $images = array();
        if ($files = $request->file('linkimage')) {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                $file->move('public/link', $name);
                $images[] = $name;
            }
        }
        $business = new Link;
        $business->name = $request->name;
        $business->type = $request->type;
        $business->post_id = $request->post_id;
        $business->category = $request->category;
        $business->post_date = $request->post_date;
        $business->content = $request->description;
        $business->link = $request->link;
        $business->linkimage = implode("|", $images);
        $business->save();
        return redirect(route('link.index'));
    }

    public function getlink1(Request $request)
    {
        $data = Linkcategory::select('category', 'category')->where('type', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function dropzoneDelete(Request $request, $id, $image)
    {
        $link = Link::where('id', '=', $id)->first();
        $hdnListCL = $link->linkimage;
        $hdnListCL = explode("|", $hdnListCL);
        $index = array_search($image, $hdnListCL);
        if ($index !== false) {
            unset($hdnListCL[$index]);
        }
        $hdnListCL = implode('|', $hdnListCL);
        $link->linkimage = $hdnListCL;
        $link->update();
        return redirect(route('link.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
    }

    public function getclient1(Request $request)
    {
        $data = Commonregister::select('email', 'first_name', 'business_no', 'filename')->where('filename', $request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        $client = Commonregister::All();
        return View('fac-Bhavesh-0554.link.edit', compact(['link', 'client']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $input = $request->all();
        $a = $request->linkimage1;
        $images = array($a);
        if ($files = $request->file('linkimage')) {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                $file->move('public/link', $name);
                $images[] = $name;
            }
            $filename1 = implode("|", $images);
            $filename = $filename1;
        } else {
            $filename = $request->linkimage1;
        }
        $business = $link;
        $business->name = $request->name;
        $business->post_id = $request->post_id;
        $business->post_date = $request->post_date;
        $business->type = $request->type;
        $business->content = $request->description;
        $business->linkimage = $filename;
        $business->category = $request->category;
        $business->link = $request->link;
        $business->update();
        return redirect(route('link.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        $link->delete();
        return redirect(route('link.index'));
    }
}