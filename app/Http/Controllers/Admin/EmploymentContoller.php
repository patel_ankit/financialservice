<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Employment;
use Illuminate\Http\Request;

class EmploymentContoller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employment = Employment::orderBy('id', 'asc')->get();
        return view('fac-Bhavesh-0554/employment/employment', compact('employment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/employment/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'position_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'description' => 'required',
            'date' => 'required',
            'type' => 'required',
            'link' => 'required',
        ]);

        $employment = new Employment;

        $employment->position_name = $request->position_name;
        $employment->country = $request->country;
        $employment->state = $request->state;
        $employment->city = $request->city;
        $employment->description = $request->description;
        $employment->date = $request->date;
        $employment->type = $request->type;
        $employment->link = $request->link;
        $employment->save();
        return redirect('fac-Bhavesh-0554/employment')->with('success', 'You Are SuccessFully Registered');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function show(Employment $employment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function edit(Employment $employment)
    {
        return View('fac-Bhavesh-0554.employment.edit', compact('employment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employment $employment)
    {
        $this->validate($request, [
            'position_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'description' => 'required',
            'date' => 'required',
            'type' => 'required',
            'link' => 'required',
        ]);

        $employment1 = $employment;
        $employment1->position_name = $request->position_name;
        $employment1->country = $request->country;
        $employment1->state = $request->state;
        $employment1->city = $request->city;
        $employment1->description = $request->description;
        $employment1->created_at = date('Y-m-d', strtotime($request->date));
        $employment1->type = $request->type;
        $employment1->link = $request->link;
        $employment1->update();
        return redirect('fac-Bhavesh-0554/employment')->with('success', 'You Are SuccessFully Updated');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employment $employment)
    {
        $employment->delete();
        return redirect(route('employment.index'))->with('success', 'You Are Record Delete SuccessFully ');
    }
}