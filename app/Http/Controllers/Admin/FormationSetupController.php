<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\formationsetup;
use DB;
use Illuminate\Http\Request;

class FormationSetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $position = formationsetup::where('type1', '=', 'Formation')->get();
        return view('fac-Bhavesh-0554/formationsetup/formationsetup', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = formationsetup::All();
        return view('fac-Bhavesh-0554/formationsetup/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',

        ]);
        $position = new formationsetup;
        $position->question = $request->question;
        $position->type = $request->typeofservice;
        $position->date = $request->expiredate;
        $position->type1 = $request->expiredate;
        $position->save();
        return redirect('fac-Bhavesh-0554/formationsetup')->with('success', 'Success fully add Question');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = formationsetup::where('id', $id)->first();
        return view('fac-Bhavesh-0554.formationsetup.edit', compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'question' => 'required',

        ]);
        $position = formationsetup::find($id);
        $position->question = $request->question;
        $position->type = $request->typeofservice;
        $position->date = $request->expiredate;
        $position->update();
        return redirect('fac-Bhavesh-0554/formationsetup')->with('success', 'Success fully update question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        formationsetup::where('id', $id)->delete();
        return redirect(route('formationsetup.index'));
    }
}