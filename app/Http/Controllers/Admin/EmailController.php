<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Email;
use App\Model\Employee;
use App\Model\Logo;
use DB;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //DB::enableQueryLog();
        //      $email = DB::table('email_emp')->select('email_emp.id as ids','email_emp.for_whom','email_emp.email','email_emp.access_address','email_emp.password','email_emp.ext','email_emp.telephone','email_emp.location','employees.id','employees.status','employees.type','employees.firstName','employees.middleName','employees.lastName','employees.check')
        // ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'email_emp.for_whom');})
        // ->where('employees.status', '=', "1")->where('employees.check', '=', "1")
        // ->get();

        $email = DB::table('email_emp')->select('email_emp.id as ids', 'email_emp.for_whom', 'email_emp.email', 'email_emp.access_address', 'email_emp.password', 'email_emp.ext', 'email_emp.telephone', 'email_emp.location', 'employees.id', 'employees.status', 'employees.type', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'employees.check')
            ->leftJoin('employees', function ($join) {
                $join->on('employees.id', '=', 'email_emp.for_whom');
            })->where('employees.status', '=', "1")->orWhere('employees.status', '=', NULL)
            ->get();

        //print_r(DB::getQueryLog());
        //exit();
        // $email = Email::All();
        $emp = Employee::get();
        $logo = Logo::where('id', '=', 1)->first();

        $mainemail = DB::table('mainemail')->select('mainemail', 'maintelephone', 'mainfax', 'password')->first();

        return view('fac-Bhavesh-0554/email/email', compact(['emp', 'email', 'logo', 'mainemail']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $email = Email::All();
        $emp1 = Employee::where('check', '=', 1)->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        return view('fac-Bhavesh-0554/email/create', compact(['emp1', 'email']));
    }

    public function getEmailList($id)
    {
        $email = DB::table('email_emp')->get();
        $filledemail = DB::table('emp_email_rights')->Where('emp_id', $id)->get();

        $emailarray = array();
        foreach ($filledemail as $mails):
            $emailarray[] = $mails->emp_email_id;
        endforeach;

        $option = "<option value=''> -- Select -- </option>";
        foreach ($email as $mail):
            if (in_array($mail->id, $emailarray)) {

            } else {
                $option .= '<option value=' . $mail->id . '>' . $mail->email . '</option>';
            }

        endforeach;

        echo $option;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->for_whom == 'Other') {
            $this->validate($request, [
                //'for_whom' =>'required|unique:email_emp,for_whom',
                'email' => 'required',
                'access_address' => 'required',
                'password' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'for_whom' => 'required|unique:email_emp,for_whom',
                'email' => 'required',
                'access_address' => 'required',
                'password' => 'required',
            ]);
        }


        // $this->validate($request,[
        //     'for_whom' =>'required|unique:email_emp,for_whom',
        //     'email' =>'required',
        //     'access_address' =>'required',
        //     'password' =>'required',                                
        // ]);
        $branch = new Email;
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->telephone = $request->telephone;
        $branch->ext = $request->ext;
        $branch->location = $request->location;
        $branch->save();
        return redirect('fac-Bhavesh-0554/email')->with('success', 'Success fully add Email');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function mainstore(Request $request)
    {
        $users = DB::table('mainemail')->delete();

        $insert2 = DB::insert("insert into mainemail(`mainemail`,`maintelephone`,`mainfax`,`password`) values('" . $request->main_email . "','" . $request->main_telephone . "','" . $request->main_fax . "','" . $request->password . "')");
        return redirect('fac-Bhavesh-0554/email')->with('success', 'Success fully Sent Email Data');


    }

    public function edit($id)
    {
        //$branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();
        $emp = Employee::where('check', '=', 1)->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();;
        $leave = Email::where('for_whom', $id)->first();
        // $email = Email::All();
        // print_r($leave);exit;
        //       $email = DB::table('email_emp')->select('email_emp.id as ids','email_emp.for_whom','email_emp.email','email_emp.access_address','email_emp.password','email_emp.ext','email_emp.telephone','email_emp.location','employees.id as eid','employees.status','employees.type','employees.firstName','employees.middleName','employees.lastName','employees.check')
        // ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'email_emp.for_whom');})
        // ->where('employees.status', '=', "1")->where('employees.check', '=', "1")
        // ->where('email_emp.id', '=', $id)->first();

        $email = DB::table('email_emp')->select('email_emp.id as ids', 'email_emp.for_whom', 'email_emp.email', 'email_emp.access_address', 'email_emp.password', 'email_emp.ext', 'email_emp.telephone', 'email_emp.location', 'employees.id as eid', 'employees.status', 'employees.type', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'employees.check')
            ->leftJoin('employees', function ($join) {
                $join->on('employees.id', '=', 'email_emp.for_whom');
            })
            ->where('email_emp.id', '=', $id)->where('employees.status', '=', "1")->orWhere('employees.status', '=', NULL)->first();


        return view('fac-Bhavesh-0554/email.edit', compact(['leave', 'emp', 'email']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Email::find($id);
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->telephone = $request->telephone;
        $branch->ext = $request->ext;
        $branch->location = $request->location;
        $branch->update();
        return redirect('fac-Bhavesh-0554/email')->with('success', 'Success fully Update Email');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Email::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/email');
    }

    public function getemails(Request $request)
    {

        $data2 = Employee::select('email', 'id')->where('id', $request->id)->take(100)->get();
        return response()->json($data2);
    }
}