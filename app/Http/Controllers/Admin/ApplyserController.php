<?php

namespace App\Http\Controllers\Admin;

use App\Front\Applyservice;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class ApplyserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $applyservice = DB::select("select applyservices.id as cid,applyservices.created_at as created_at,applyservices.serviceid as serviceid,applyservices.note as note,applyservices.serviceTypeId as serviceTypeId,applyservices.email as email,applyservices.firstName as firstName,applyservices.middleName as middleName,applyservices.lastName as lastName,applyservices.fax as fax,applyservices.telephoneNo2 as telephoneNo2,applyservices.telephoneNo2Type as telephoneNo2Type,applyservices.telephoneNo1Type as telephoneNo1Type,applyservices.telephoneNo1 as telephoneNo1,applyservices.countryId as countryId,applyservices.address as address,applyservices.zip as zip,applyservices.city as city,applyservices.stateId as stateId,services.id as sid,services.service_name as service_name from applyservices left join services on services.id=applyservices.serviceid");
        return view('fac-Bhavesh-0554/applyservice/applyservice', compact('applyservice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = DB::table('applyservices')->select('applyservices.id as cid', 'applyservices.nametype as nametype', 'applyservices.serviceid as serviceid', 'applyservices.ext2 as ext2', 'applyservices.ext1 as ext1', 'applyservices.note as note', 'applyservices.serviceTypeId as serviceTypeId', 'applyservices.email as email', 'applyservices.firstName as firstName', 'applyservices.middleName as middleName', 'applyservices.lastName as lastName', 'applyservices.fax as fax', 'applyservices.telephoneNo2 as telephoneNo2', 'applyservices.telephoneNo2Type as telephoneNo2Type', 'applyservices.telephoneNo1Type as telephoneNo1Type', 'applyservices.telephoneNo1 as telephoneNo1', 'applyservices.countryId as countryId', 'applyservices.address as address', 'applyservices.zip as zip', 'applyservices.city as city', 'applyservices.stateId as stateId', 'services.id as sid', 'services.service_name as service_name')
            ->leftJoin('services', function ($join) {
                $join->on('applyservices.serviceid', '=', 'services.id');
            })
            ->where('applyservices.id', '=', "$id")->get()->first();
        return View('fac-Bhavesh-0554/applyservice/edit', compact(['service']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'firstName' => 'required',
            'middleName' => 'required',
            'lastName' => 'required',
            'telephoneNo1' => 'required',
            'telephoneNo2' => 'required',
            'telephoneNo1Type' => 'required',

            'city' => 'required',
            'stateId' => 'required',
            'zip' => 'required',
            'address' => 'required',

            'serviceTypeId' => 'required',
            'countryId' => 'required',
        ]);
        $applyservice = Applyservice::find($id);
        $applyservice->serviceid = $request->serviceid;
        $applyservice->firstName = $request->firstName;
        $applyservice->middleName = $request->middleName;
        $applyservice->lastName = $request->lastName;
        $applyservice->telephoneNo1 = $request->telephoneNo1;
        $applyservice->email = $request->email;
        $applyservice->address = $request->address;
        $applyservice->telephoneNo1Type = $request->telephoneNo1Type;
        $applyservice->city = $request->city;
        $applyservice->stateId = $request->stateId;
        $applyservice->zip = $request->zip;
        $applyservice->countryId = $request->countryId;
        $applyservice->telephoneNo2Type = $request->telephoneNo2Type;
        $applyservice->fax = $request->fax;
        $applyservice->telephoneNo2 = $request->telephoneNo2;
        $applyservice->serviceTypeId = $request->serviceTypeId;
        $applyservice->ext1 = $request->ext1;
        $applyservice->ext2 = $request->ext2;
        $applyservice->note = $request->note;
        $applyservice->update();
        return redirect(route('applyservice.index'))->with('success', 'You Are Update SuccessFully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Applyservice::where('id', $id)->delete();
        return redirect(route('applyservice.index'));
    }
}