<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\Task;
use DB;
use Hash;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = DB::table('tasks')->orderBy('id', 'desc')->get();
        $emp = DB::table('employees')->get();
        $clients = DB::table('commonregisters')->get();
        // $clients = DB::table('commonregisters.first_name','commonregisters.middle_name','commonregisters.last_name','commonregisters.id')->get();
        //$emp1 = DB::table('employees')->where('check','=',1)->where('status','=',1)->orderBy('firstName', 'asc')->get();
        $emp1 = Employee::get();
        $logo = Logo::where('id', '=', 1)->first();
        //   echo "<pre>";
        //     print_r($emp);die;
        return view('fac-Bhavesh-0554/task/task', compact(['clients', 'task', 'emp', 'emp1', 'logo']));
    }

    public function search()
    {
        $emp = DB::table('employees')->get();
        $clients = DB::table('commonregisters')->get();
        $emp1 = Employee::get();
        $logo = Logo::where('id', '=', 1)->first();
        $search1 = $_POST['choice'];
        if (isset($_POST['sub'])) {
            if (isset($_POST['choice']) && $_POST['choice'] != '') {
                $task = DB::table('tasks')->where('status', '=', $search1)->orderBy('id', 'desc')->get();
            } else {
                $task = DB::table('tasks')->orderBy('id', 'desc')->get();
            }
        }


        //   echo "<pre>";
        //     print_r($emp);die;
        return view('fac-Bhavesh-0554/task/task', compact(['clients', 'task', 'emp', 'emp1', 'logo']));
    }





    // public function index()
    // {
    //     //$emp1 = Employee::get();
    //     //$task = Task::All();
    //     $task = DB::table('Task as t1')
    //         ->select('t1.id,t1.title,t1.content,t1.admin_id,t1.employeeid,t1.priority,t1.duedate,t1.status,t2.firstName,t2.middleName')
    //         ->join('Employee as t2', 't2.id', '=', 't1.employeeid')
    //         ->get();
    //     $emp = Fscemployee::All();
    //     $logo = Logo::where('id','=',1)->first();
    //     return view('fac-Bhavesh-0554/task/task', compact(['task','emp','logo']));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Fscemployee::All();
        $emp1 = Employee::where('check', '=', 1)->where('status', '=', 1)->where('employee_id', '!=', '')->orderBy('firstName', 'asc')->get();
        // print_r($emp1);die;
        $customer = Commonregister::where('status', '=', 'Active')->get();
        return view('fac-Bhavesh-0554/task/create', compact(['emp', 'emp1', 'customer']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'employee' => 'required',
        ]);
        $position = new Task;
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $request->employee;
        $position->whone = $request->whone;
        $position->client = $request->client;
        $position->othername = $request->othername;
        $position->priority = $request->priority;
        $position->duedate = $request->duedate;
        $position->creattiondate = $request->date;
        $position->day = $request->day;
        $position->time = $request->time;
        $position->save();
        return redirect('fac-Bhavesh-0554/task')->with('success', 'Task Added Successfully');
    }


    public function edit($id)
    {

        $task = DB::table('tasks')->where('id', '=', $id)->first();
        $emp1 = DB::table('employees')->where('check', '=', 1)->where('status', '=', 1)->where('employee_id', '!=', '')->orderBy('firstName', 'asc')->get();
        $customer = Commonregister::where('status', '=', 'Active')->get();
        DB::table('tasks')->where('id', $id)->update(['checked' => '1']);
        // echo "<pre>";
        // print_r($customer);die;
        return View('fac-Bhavesh-0554.task.edit', compact(['task', 'emp1', 'customer']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'employee' => 'required',
        ]);
        $position = Task::find($id);
        $position->title = $request->title;
        $position->content = $request->description;
        $position->admin_id = $request->admin_id;
        $position->employeeid = $request->employee;
        $position->whone = $request->whone;
        $position->client = $request->client;
        $position->othername = $request->othername;
        $position->priority = $request->priority;
        $position->duedate = $request->duedate;
        $position->creattiondate = $request->date;
        $position->day = $request->day;
        $position->time = $request->time;
        $position->update();
        return redirect('fac-Bhavesh-0554/task')->with('success', 'Task Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Task::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/task')->with('success', 'Task Deleted Successfully');
    }
}