<?php

namespace App\Http\Controllers\Admin;

use App\Front\ApplyEmployment;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Employment;
use App\Model\Position;
use App\Model\Rules;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Validator;
use View;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $application = DB::select("select apply_employments.id as cid,apply_employments.firstName as firstName,apply_employments.middleName as middleName,apply_employments.employee_date as employee_date,apply_employments.candidate_date as candidate_date,apply_employments.status as status,apply_employments.newapp as newapp,apply_employments.lastName as lastName,apply_employments.address1 as address1,apply_employments.address2 as address2,apply_employments.city as city,apply_employments.stateId as stateId,apply_employments.zip as zip,apply_employments.countryId as countryId,apply_employments.telephoneNo1 as telephoneNo1,apply_employments.telephoneNo2 as telephoneNo2,apply_employments.telephoneNo1Type as telephoneNo1Type,apply_employments.telephoneNo2Type as telephoneNo2Type,apply_employments.email as email,apply_employments.requiremnetId as requiremnetId,apply_employments.resume as resume,employments.position_name as position_name,employments.id as cd,employments.country as country,employments.state as state,employments.city as city1,employments.description as description,employments.date as date,employments.type as type from apply_employments left join employments on employments.id=apply_employments.employment_id");
        $employment = Employment::All();
        return view('fac-Bhavesh-0554/candidate/candidate', compact(['application', 'employment']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $rules = Rules::All();
        $newclient = ApplyEmployment::where('id', $id)->update(['newapp' => 3]);
        $employment = DB::table('apply_employments')->select('apply_employments.id as cid', 'apply_employments.employment_id as employment_id', 'apply_employments.firstName as firstName', 'apply_employments.middleName as middleName', 'apply_employments.newapp as newapp', 'apply_employments.lastName as lastName', 'apply_employments.address1 as address1', 'apply_employments.address2 as address2', 'apply_employments.city as city', 'apply_employments.stateId as stateId', 'apply_employments.candidate_date as candidate_date', 'apply_employments.zip as zip', 'apply_employments.countryId as countryId', 'apply_employments.telephoneNo1 as telephoneNo1', 'apply_employments.telephoneNo2 as telephoneNo2', 'apply_employments.telephoneNo1Type as telephoneNo1Type', 'apply_employments.telephoneNo2Type as telephoneNo2Type', 'apply_employments.email as email', 'apply_employments.requiremnetId as requiremnetId', 'apply_employments.resume as resume', 'employments.position_name as position_name', 'employments.id as cd', 'employments.country as country', 'employments.state as state', 'employments.city as city1', 'employments.description as description', 'employments.date as date', 'employments.type as type')
            ->leftJoin('employments', function ($join) {
                $join->on('apply_employments.employment_id', '=', 'employments.id');
            })
            ->where('apply_employments.id', '=', $id)
            ->get()
            ->first();
        $employment1 = Employment::All();
        $position = Position::All();
//$branch = Branch::All(); 
        $inventory = DB::table('branches')->select('city')->groupBy('city')->get();


//Branch::orderBy('city', 'desc')->get();
        return view('fac-Bhavesh-0554.candidate.edit', compact(['employment', 'employment1', 'position', 'inventory', 'rules']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('resume')) {
            $filname = $request->resume->getClientOriginalName();
            $request->resume->move('public/resumes', $filname);
        } else {
            $filname = $request->resume_1;
        }
        $applyemployment = ApplyEmployment::find($id);
        $applyemployment->firstName = $request->firstName;
        $applyemployment->middleName = $request->middleName;
        $applyemployment->lastName = $request->lastName;
        $applyemployment->address1 = $request->address1;
        $applyemployment->address2 = $request->address2;
        $applyemployment->city = $request->city;
        $applyemployment->stateId = $request->stateId;
        $applyemployment->zip = $request->zip;
        $applyemployment->countryId = $request->countryId;
        $applyemployment->telephoneNo1 = $request->telephoneNo1;
        $applyemployment->telephoneNo2 = $request->telephoneNo2;
        $applyemployment->telephoneNo1Type = $request->telephoneNo1Type;
        $applyemployment->telephoneNo2Type = $request->telephoneNo2Type;
        $applyemployment->email = $request->email;
        $applyemployment->requiremnetId = $request->requiremnetId;
        $applyemployment->resume = $filname;
        $applyemployment->employment_id = $request->employment_id;
        $applyemployment->status = 2;
        //$applyemployment->candidate_date = date('M-d-Y');         
        /*$commonregistery1 = Employee::orderBy('employee_id', 'desc')->first();
        if(!empty($commonregistery1)) {
        $commonid = $commonregistery1->employee_id;
        $exp11=explode('-',$commonid); 
        $exp13=$exp11[0];       
        $exp12=$exp11[1];
        $exp10=$exp11[2];               
        $n=$exp12;
        $n2 = str_pad($exp10 + 1, 3, 0, STR_PAD_LEFT);
        $cmppcode=$exp13.'-'.$n.'-'.$n2;   
}
else
{
$cmppcode ="USA-GA-001";
} */

        if ($request->hasFile('photo')) {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        } else {
            $filname = $request->photo1;
        }
        if ($request->hasFile('pfid1')) {
            $filname1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/employeeProof1', $filname1);
        } else {
            $filname1 = $request->pfid12;
        }
        if ($request->hasFile('pfid2')) {
            $filname2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/employeeProof2', $filname2);
        } else {
            $filname2 = $request->pfid22;
        }

        if ($request->hasFile('additional_attach')) {
            $additional_att = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/additional_attach', $additional_att);
        } else {
            $additional_att = $request->additional_attach1;
        }
        if ($request->hasFile('additional_attach_1')) {
            $additional_att_1 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/additional_attach1', $additional_att_1);
        } else {
            $additional_att_1 = $request->additional_attach2;
        }
        if ($request->hasFile('additional_attach_2')) {
            $additional_att_2 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/additional_attach2', $additional_att_2);
        } else {
            $additional_att_2 = $request->additional_attach3;
        }

        if ($request->hasFile('agreement')) {
            $agreement1 = $request->agreement->getClientOriginalName();
            $request->agreement->move('public/agreement', $agreement1);
        } else {
            $agreement1 = $request->agreement_1;
        }
        if ($request->check == '0') {
            $status = 0;
            $password1 = '';
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 0, 'name' => $name]);
        } else {
            $status = '1';
            $email = $request->email;
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 1, 'name' => $name]);

            $password = mt_rand();
            $password1 = bcrypt($password);
            $user = User::where('email', '=', $email)->first();
            if ($user === null) {
                $insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`) values('" . $name . "','" . $email . "','" . bcrypt($password) . "','" . $password . "','" . $id . "','1')");
                $data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' => $password);
                \Mail::send('fac-Bhavesh-0554/employe', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['firstName'], $data['password'])->subject('Welcome!');
                });
            }

        }
        $employee = new Employee;
        $employee->employee_id = $request->employee_id;
        $employee->type = 'employee';
        $employee->firstName = $request->firstName;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->photo = $filname;
        $employee->hiremonth = $request->hiremonth;
        $employee->hireday = $request->hireday;
        $employee->hireyear = $request->hireyear;
        //$employee->termimonth= $request->termimonth;
        $employee->termiday = $request->termiday;
        $employee->termiyear = $request->termiyear;
        $employee->tnote = $request->tnote;
        $employee->rehiremonth = $request->rehiremonth;
        $employee->rehireday = $request->rehireday;
        $employee->rehireyear = $request->rehireyear;
        $employee->branch_city = $request->branch_city;
        $employee->branch_name = $request->branch_name;
        $employee->position = $request->position;
        $employee->note = $request->note;
        $employee->additional_attach = $additional_att;
        $employee->additional_attach_1 = $additional_att_1;
        $employee->additional_attach_1 = $additional_att_2;
        $employee->pay_method = $request->pay_method;
        $employee->pay_frequency = $request->pay_frequency;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        $employee->pfid1 = $filname1;
        $employee->pf2 = $request->pf2;
        $employee->pfid2 = $filname2;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->uname = $request->uname;
        $employee->password = $request->password;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        $employee->check = $request->check;
        $employee->filling_status = $request->filling_status;
        $employee->fedral_claim = $request->fedral_claim;
        $employee->additional_withholding = $request->additional_withholding;
        $employee->state_claim = $request->state_claim;
        $employee->additional_withholding_1 = $request->additional_withholding_1;
        $employee->local_claim = $request->local_claim;
        $employee->additional_withholding_2 = $request->additional_withholding_2;
        $employee->resume = $filname;
        $employee->type_agreement = $request->type_agreement;
        $employee->firstName_1 = $request->firstName_1;
        $employee->agreement = $agreement1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;
        //$employee->work_responsibility= $request->work_responsibility;
        $employee->employee_rules = $request->employee_rules;
        $employee->eemail = $request->eemail;
        $employee->save();
        $pay_method = $request->pay_method;
        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee = $request->employee;
        // $employee= $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;
        $i = 0;
        $j = 0;
        $k = 0;
        DB::table('employee_pay_info')->where('employee_id', $request->employee_id)->first();
        foreach ($pay_scale as $post) {
            $pay_method1 = $pay_method;
            $pay_frequency1 = $pay_frequency;
            $pay_scale1 = $pay_scale[$i];
            $effective_date1 = $effective_date[$i];
            $fields1 = $fields[$i];
            $employee1 = $employee[$i];
            $i++;
            $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) values('" . $id . "','" . $pay_method . "','" . $pay_frequency . "','" . $post . "','" . $effective_date1 . "','" . $fields1 . "')");
            if (empty($employee1)) {
            } else {
                DB::table('employee_pay_info')->where('id', $employee1)->delete();
//$affectedRows = employee_pay_info::where('employee_id', '=', $id)->delete();
            }
        }
        /*
        DB::table('employee_other_info')->where('employee_id', $request->employee_id)->first();
        foreach($work_responsibility1 as $post)
        {
        $work1 = $work[$j];
        $j++;
        $insert2 = DB::insert("insert into employee_other_info(`employee_id`,`work_responsibility`) values('".$id."','".$post."')");
        if(empty($work1))
        {
        }
        else
        {
        DB::table('employee_other_info')->where('id', $work1)->delete();
        //$affectedRows = employee_pay_info::where('employee_id', '=', $id)->delete();
        }
        }

        $first_rev_day = $request->first_rev_day;
        $reviewmonth= $request->reviewmonth;
        $hiring_comments= $request->hiring_comments;
        $ree= $request->ree;

        DB::table('employee_review')->where('employee_id', $request->employee_id)->first();
        foreach($first_rev_day as $post)
        {
        $first_rev_day1 = $first_rev_day[$k];
        $reviewmonth1 = $reviewmonth[$k];
        $hiring_comments1 = $hiring_comments[$k];
        $ree1 = $ree[$k];
        $k++;
        $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('".$id."','".$post."','".$reviewmonth1."','".$hiring_comments1."')");
        if(empty($ree1))
        {
        }
        else
        {
        DB::table('employee_review')->where('id', $ree1)->delete();
        //$affectedRows = employee_pay_info::where('employee_id', '=', $id)->delete();
        }
        }*/
        $applyemployment->update();
        return redirect('fac-Bhavesh-0554/candidate')->with('success', 'This Record convert Candidate to Employee successfully');
    }


    public function getemp(Request $request)
    {

        $user = DB::table('employees')->where('employee_id', Input::get('employee_id'))->take(1000)->get();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }

        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }


    public function update1($id)
    {
        $logoStatus = ApplyEmployment::findOrFail($id);
        $logoStatus->update(['status' => '1']);
        return redirect('fac-Bhavesh-0554/candidate')->with('success', 'You Are Update SuccessFully');
    }

    public function deleteto($id)
    {
        $logoStatus = ApplyEmployment::findOrFail($id);
        ApplyEmployment::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/candidate')->with('error', 'You Are Reject The Application');
    }

    public function getbranch(Request $request)
    {
        $data = Branch::select('branchname', 'city')->where('city', $request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ApplyEmployment::where('id', $id)->delete();
        return redirect(route('candidate.index'));
    }
}