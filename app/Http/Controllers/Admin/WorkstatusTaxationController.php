<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class WorkstatusTaxationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    function edit($id)
    {
        //echo $id;die;
        $datastate = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate2 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate3 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();

        $Incometaxfederal = DB::table('client_taxfederal')->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->get();

        $federaldata = DB::table('client_taxfederal')->where('id', $id)->first();
        $statedatatax = DB::table('client_taxstate')->where('federal_id', $id)->get();

        $fname = DB::table('commonregisters')->select('commonregisters.id', 'commonregisters.company_name', 'commonregisters.filename', 'client_taxfederal.id as fid',
            'client_taxfederal.client_id as cids')
            ->leftJoin('client_taxfederal', function ($join) {
                $join->on('client_taxfederal.client_id', '=', 'commonregisters.id');
            })
            ->where('client_taxfederal.id', $id)->first();

        // echo "<pre>";
        //    print_r($fname);die;

        return view('fac-Bhavesh-0554.workstatustaxation.edit', compact(['fname', 'datastate', 'datastate2', 'datastate3', 'Incometaxfederal', 'federaldata', 'statedatatax']));
    }


    function update(Request $request)
    {
        // echo $id;die;
        //echo "<pre>";
        //print_r($_REQUEST);die;

        $federalid = $request->federalid;
        $client_id = $request->client_id;
        $federalsyear = $request->federalsyear;
        $federalstax = $request->federalstax;
        $federalsduedate = date('Y-m-d', strtotime($request->federalsduedate));
        $federalsform = $request->federalsform;
        $federalsmethod = $request->federalsmethod;
        $federalsdate = date('Y-m-d', strtotime($request->federalsdate));
        $federalsstatus = $request->federalsstatus;
        $federalsnote = $request->federalsnote;

        if (isset($_FILES['federalsfile']['name']) != '') {
            $path1 = public_path() . '/adminupload/' . $_FILES['federalsfile']['name'];

            if (move_uploaded_file($_FILES['federalsfile']['tmp_name'], $path1)) {
                $federal_copy = $_FILES['federalsfile']['name'];
            } else {
                $federal_copy = $request->ffile;
            }

        } else {
            $federal_copy = $request->ffile;
        }
        $federalsfile = $federal_copy;

        DB::table('client_taxfederal')->where('id', $federalid)
            ->update([
                'client_id' => $client_id,
                'federalsyear' => $federalsyear,
                'federalstax' => $federalstax,
                'federalsduedate' => $federalsduedate,
                'federalsform' => $federalsform,
                'federalsmethod' => $federalsmethod,
                'federalsdate' => $federalsdate,
                'federalsstatus' => $federalsstatus,
                'federalsfile' => $federalsfile,
                'federalsnote' => $federalsnote,
            ]);
        //$ids=$data->id;
        //echo $ids;die;

        if ($federalstax != 'Extension') {
            $taxid = $request->taxid;
            //$client_id = $request->client_id;
            //$federal_id= $ids;
            //$stateyear= $request->stateyear;
            $statetax = $request->statetax;
            $stateformno = $request->stateformno;
            $statemethod = $request->statemethod;
            $statedate = $request->statedate;
            $statestatus = $request->statestatus;
            //$statefile=$request->statefile;
            $statenote = $request->statenote;

            $i = 0;

            foreach ($statetax as $statetax1) {
                $path2 = public_path() . '/adminupload/' . $_FILES['statefile']['name'][$i];

                //  if(isset($_FILES['statefile']['name'][$i])!='')
                //{

                if (move_uploaded_file($_FILES['statefile']['tmp_name'][$i], $path2)) {
                    $state_copy = $_FILES['statefile']['name'][$i];
                } else {
                    $state_copy = $_POST['sfile'][$i];
                }

                // }
                //else
                // {
                //    $state_copy= $_POST['sfile'][$i];

                //}
                $statefile = $state_copy;

                $taxid1 = $taxid[$i];
                $client_id = $client_id;
                $federal_id1 = $federalid;
                $stateyear1 = $federalsyear;
                $statetax1 = $statetax[$i];
                $stateformno1 = $stateformno[$i];
                $statemethod1 = $statemethod[$i];
                $statedate1 = $statedate[$i];
                $statestatus1 = $statestatus[$i];
                $statefile1 = $statefile;
                $statenote1 = $statenote[$i];
                $i++;
                if (empty($taxid1)) {
                    $insert2 = DB::insert("insert into client_taxstate(`client_id`,`federal_id`,`stateyear`,`statetax`,`stateformno`,`statemethod`,`statedate`,`statestatus`,`statefile`,`statenote`) values('" . $client_id1 . "','" . $federal_id1 . "','" . $stateyear1 . "','" . $statetax1 . "','" . $stateformno1 . "','" . $statemethod1 . "','" . $statedate . "','" . $statestatus1 . "','" . $statefile1 . "','" . $statenote1 . "')");
                } else {
                    $returnValue = DB::table('client_taxstate')->where('id', '=', $taxid1)
                        ->update([
                            'client_id' => $client_id,
                            'federal_id' => $federal_id1,
                            'stateyear' => $stateyear1,
                            'statetax' => $statetax1,
                            'stateformno' => $stateformno1,
                            'statemethod' => $statemethod1,
                            'statedate' => $statedate1,
                            'statestatus' => $statestatus1,
                            'statefile' => $statefile1,
                            'statenote' => $statenote1,
                        ]);
                    //$affectedRows = admin_shareholder::where('id', '=',$conid1)->delete();
                    $affectedRows = DB::table('client_taxstate')->where('id', '=', '')->delete();
                }
            }
        }

        return redirect('fac-Bhavesh-0554/workstatus?id=' . $client_id . '&&action=tax')->with('success', 'State Added Successfully');

    }


}