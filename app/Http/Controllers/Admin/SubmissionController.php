<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Submission;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submission = Submission::All();
        return view('fac-Bhavesh-0554/submissionreq/submissionreq', compact('submission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.submissionreq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'submission_name' => 'required|unique:submissions,submission_name',
            'submission_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link' => 'required',
        ]);
        if ($request->hasFile('submission_image')) {
            $filname = $request->submission_image->getClientOriginalName();
            $request->submission_image->move('public/submission', $filname);
        }
        if ($request->hasFile('singleimage')) {
            $filname1 = $request->singleimage->getClientOriginalName();
            $request->singleimage->move('public/submission', $filname1);
        }
        if ($request->hasFile('siteimage')) {
            $filname2 = $request->siteimage->getClientOriginalName();
            $request->siteimage->move('public/submission', $filname2);
        }
        $business = new Submission;
        $business->submission_name = $request->submission_name;
        $business->link = $request->link;
        $business->description = $request->description;
        $business->submission_image = $filname;
        $business->singleimage = $filname1;
        $business->siteimage = $filname2;
        $business->save();
        return redirect(route('submissionreq.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Submission $submission
     * @return \Illuminate\Http\Response
     */
    public function show(Submission $submission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Submission $submission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submission = Submission::where('id', $id)->first();
        return View('fac-Bhavesh-0554.submissionreq.edit', compact('submission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Submission $submission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'submission_name' => 'required',

            'link' => 'required',
        ]);
        if ($request->hasFile('submission_image')) {
            $filname = $request->submission_image->getClientOriginalName();
            $request->submission_image->move('public/submission', $filname);
        } else {
            $filname = $request->submission_image1;
        }

        if ($request->hasFile('singleimage')) {
            $filname1 = $request->singleimage->getClientOriginalName();
            $request->singleimage->move('public/submission', $filname1);
        } else {
            $filname1 = $request->singleimage1;
        }
        if ($request->hasFile('siteimage')) {
            $filname2 = $request->siteimage->getClientOriginalName();
            $request->siteimage->move('public/submission', $filname2);
        } else {
            $filname2 = $request->siteimage1;
        }
        // $submission= Submission::find($id); 
        $business = Submission::find($id);
        $business->submission_name = $request->submission_name;
        $business->link = $request->link;
        $business->submission_image = $filname;
        $business->singleimage = $filname1;
        $business->siteimage = $filname2;
        $business->description = $request->description;
        $business->update();
        return redirect(route('submissionreq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Submission $submission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Submission::where('id', $id)->delete();
        return redirect(route('submissionreq.index'));
    }
}