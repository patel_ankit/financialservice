<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Business;
use App\Model\Category;
use DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = DB::select("select categories.id as cid,categories.licenses,categories.naics,categories.sic,categories.link as link,categories.business_cat_image as business_cat_image,categories.business_cat_name as business_cat_name,businesses.bussiness_name,businesses.id as bid from categories left join businesses on businesses.id=categories.bussiness_name");
        return view('fac-Bhavesh-0554.businesscategory.businesscategory', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $business = Business::all();
        return view('fac-Bhavesh-0554.businesscategory.business-category-add-new', compact(['business']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'bussiness_name' => 'required',
            'business_cat_name' => 'required|unique:categories,business_cat_name',
            'business_cat_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link' => 'required',
        ]);
        if ($request->hasFile('business_cat_image')) {
            $filname = $request->business_cat_image->getClientOriginalName();
            $request->business_cat_image->move('public/category', $filname);
        }

        $category = new Category;
        $category->bussiness_name = $request->bussiness_name;
        $category->business_cat_name = $request->business_cat_name;
        $category->link = $request->link;
        $category->business_cat_image = $filname;
        $category->naics = isset($request->naics) ? $request->naics : "";
        $category->sic = isset($request->sic) ? $request->sic : "";


        $category->save();
        return redirect(route('businesscategory.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.businesscategory.businesscategory', compact(['category', 'business']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commonuser = DB::table('license')->orderby('licensename', 'asc')->get();

        $category = DB::table('categories')->select('categories.naics', 'categories.sic', 'categories.licenses', 'categories.id as cid', 'categories.business_cat_name as business_cat_name', 'categories.business_cat_image as business_cat_image', 'categories.link as link', 'categories.bussiness_name as buss', 'businesses.bussiness_name as bussiness_name')
            ->leftJoin('businesses', function ($join) {
                $join->on('categories.bussiness_name', '=', 'businesses.id');
            })
            ->where('categories.id', '=', "$id")->get()->first();
        // print_r($category);exit;
        //$category = DB::select("select categories.id as cid,categories.link as link,categories.business_cat_image as business_cat_image,categories.business_cat_name as business_cat_name,businesses.bussiness_name,businesses.id as bid from categories left join businesses on businesses.id=categories.bussiness_name");    
        $business = Business::all();
        // $category = Category::where('id', $id)->first();
        return View('fac-Bhavesh-0554.businesscategory.category-edit', compact(['category', 'business', 'commonuser']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bussiness_name' => 'required',
            'business_cat_name' => 'required',
            //   'link'=>'required',

        ]);
        if ($request->hasFile('business_cat_image')) {
            $filname = $request->business_cat_image->getClientOriginalName();
            $request->business_cat_image->move('public/category', $filname);
        } else {
            $filname = $request->business_cat_image1;
        }
        if (!empty($request->licenses)) {
            $license = implode(',', $request->licenses);
        } else {
            $license = '';
        }

        $category = Category::find($id);
        $category->bussiness_name = $request->bussiness_name;
        $category->business_cat_name = $request->business_cat_name;
        $category->naics = isset($request->naics) ? $request->naics : "";
        $category->sic = isset($request->sic) ? $request->sic : "";
        $category->licenses = $request->licenses;
        $category->licenses = $license;


        $category->link = $request->link;
        $category->business_cat_image = $filname;
        $category->save();
        return redirect(route('businesscategory.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id', $id)->delete();
        return redirect(route('businesscategory.index'));
    }
}