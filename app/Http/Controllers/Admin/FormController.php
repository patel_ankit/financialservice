<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Form;
use App\Model\Formcategory;
use Illuminate\Http\Request;

class FormController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = Form::orderBy('id', 'desc')->get();
        return view('fac-Bhavesh-0554/forms/forms', compact('form'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/forms/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'form_department' => 'required',
            'form_name' => 'required',
            'form_upload' => 'required|mimes:doc,pdf,docx|max:10240',
        ]);
        if ($request->hasFile('form_upload')) {
            $filname = $request->form_upload->getClientOriginalName();
            $request->form_upload->move('public/formpdf', $filname);
        }

        $business = new Form;
        $business->form_name = $request->form_name;
        $business->form_department = $request->form_department;
        $business->form_no = $request->form_no;
        $business->link = $request->link;
        $business->form_upload = $filname;
        $business->category = $request->category;
        $business->save();
        return redirect(route('forms.index'));
    }

    public function getform1(Request $request)
    {
        $data = Formcategory::select('category', 'category')->where('type', $request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Admin\Form $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Admin\Form $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        return view('fac-Bhavesh-0554/forms/edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Admin\Form $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        $this->validate($request, [
            'form_department' => 'required',
            'form_name' => 'required',
        ]);
        if ($request->hasFile('form_upload')) {
            $filname = $request->form_upload->getClientOriginalName();
            $request->form_upload->move('public/formpdf', $filname);
        } else {
            $filname = $request->form_upload1;
        }
        $business = $form;
        $business->form_name = $request->form_name;
        $business->form_department = $request->form_department;
        $business->form_no = $request->form_no;
        $business->form_upload = $filname;
        $business->link = $request->link;
        $business->category = $request->category;
        $business->save();
        return redirect(route('forms.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Admin\Form $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        $form->delete();
        return redirect(route('forms.index'))->with('success', 'You Are Success Fully Delete Record');
    }
}