<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Service;
use App\Model\Servicetype;
use Illuminate\Http\Request;

class ServicetypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::All();
        $servicetype = Servicetype::All();
        return view('fac-Bhavesh-0554/servicetype/servicetype', compact(['service', 'servicetype']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = Service::All();
        return view('fac-Bhavesh-0554.servicetype.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'servicename' => 'required',
            'servicetype' => 'required',
        ]);
        $businessbrand = new Servicetype;
        $businessbrand->servicename = $request->servicename;
        $businessbrand->servicetype = $request->servicetype;
        $businessbrand->save();
        return redirect(route('servicetype.index'))->with('success', 'Service SuccessFully Add');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Admin\Service $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Admin\Service $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $service = Service::All();
        $servicetype = Servicetype::where('id', $id)->first();
        return View('fac-Bhavesh-0554.servicetype.edit', compact(['service', 'servicetype']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Admin\Service $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'servicename' => 'required',
            'servicetype' => 'required',
        ]);
        $businessbrand = Servicetype::find($id);
        $businessbrand->servicename = $request->servicename;
        $businessbrand->servicetype = $request->servicetype;
        $businessbrand->update();
        return redirect(route('servicetype.index'))->with('success', 'Service SuccessFully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Admin\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Servicetype::where('id', $id)->delete();
        return redirect(route('servicetype.index'));
    }
}