<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\admin_professional;
use App\Model\Adminupload;
use DB;
use Illuminate\Http\Request;

class AdminUploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $upload = Adminupload::orderBy('upload_name', 'ASC')->get();;
        $professional = admin_professional::All();
        return view('fac-Bhavesh-0554/adminupload/adminupload', compact(['upload', 'professional']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin_professional = admin_professional::All();
        return view('fac-Bhavesh-0554.adminupload.create', compact('admin_professional'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'upload_name' => 'required|unique:adminuploads,upload_name',
            'upload' => 'required',
        ]);
        if ($request->hasFile('upload')) {
            $filname = $request->upload->getClientOriginalName();
            $request->upload->move('public/adminupload', $filname);
        }

        $business = new Adminupload;
        $business->upload_name = $request->upload_name;
        $business->license_period = $request->license_period;
        $business->expired_date = $request->expired_date;
        $business->website_link = $request->website_link;
        $business->upload_name = $request->upload_name;
        $business->year = date('Y');
        $business->upload = $filname;
        $business->admin_id = $request->admin_id;

        $business->reminder = $request->reminder;
        $business->notification = $request->notification;
        $business->warning = $request->warning;
        $business->name = $request->name;
        $business->main_website = $request->main_website;
        $business->telephone = $request->telephone;
        $business->save();
        return redirect(route('adminupload.index'))->with('success', 'Your Record Upload Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Adminupload $adminupload
     * @return \Illuminate\Http\Response
     */
    public function show(Adminupload $adminupload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Adminupload $adminupload
     * @return \Illuminate\Http\Response
     */
    public function clientid($id, Request $request)
    {
        return $id;
        $admin_professional = admin_professional::All();
        return View('fac-Bhavesh-0554.adminupload.edit', compact(['adminupload', 'admin_professional']));
    }

    public function edit(Adminupload $adminupload)
    {
        $admin_professional = admin_professional::All();
        return View('fac-Bhavesh-0554.adminupload.edit', compact(['adminupload', 'admin_professional']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Adminupload $adminupload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adminupload $adminupload)
    {

        $this->validate($request, [
            'upload_name' => 'required',

        ]);
        if ($request->hasFile('upload')) {
            $filname = $request->upload->getClientOriginalName();
            $request->upload->move('public/adminupload', $filname);
        } else {
            $filname = $request->upload1;
        }
        $business = $adminupload;
        DB::insert("insert into adminupload_history(`upload_name`,`upload`,`year`,`upload_id`) values('" . $business->upload_name . "','" . $business->upload . "','" . $business->year . "','" . $business->id . "')");
        $business->upload_name = $request->upload_name;
        $business->year = date('Y');
        $business->license_period = $request->license_period;
        $business->expired_date = $request->expired_date;
        $business->website_link = $request->website_link;
        $business->upload = $filname;
        $business->admin_id = $request->admin_id;
        $business->reminder = $request->reminder;
        $business->notification = $request->notification;
        $business->warning = $request->warning;
        $business->name = $request->name;
        $business->main_website = $request->main_website;
        $business->telephone = $request->telephone;
        $business->update();
        return redirect(route('adminupload.index'))->with('success', 'Your Record Upload Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Adminupload $adminupload
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adminupload $adminupload)
    {
        $adminupload->delete();
        return redirect(route('adminupload.index'));
    }
}