<?php

namespace App\Http\Controllers\Admin;

use App\Front\StoreContact;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class ContactinqueryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $contact = StoreContact::where('verify','=','2')->orderBy('id', 'desc')->paginate(100000);
        $contact = DB::table('store_contacts')->where('verifycheck', '=', '1')->orderBy('created_At', 'desc')->paginate(100000);
        return view('fac-Bhavesh-0554/contactinquery/contactinquery', compact('contact'));

        return redirect('contacts/store2')->with('success', 'You Are SuccessFully Registered');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        $newclient = StoreContact::where('id', $id)->update(['status' => 2]);
        $city = StoreContact::where('id', $id)->first();
        return View('fac-Bhavesh-0554.contactinquery.edit', compact(['city']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StoreContact::where('id', $id)->delete();
        return redirect(route('contactinquery.index'))->with('error', 'Your Record Deleted Successfully');
    }
}