<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\Msg;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $logo = Logo::where('id', '=', 1)->first();
        $task = Msg::where('send', '=', '1')->where('call_back', '=', 'Urgent')->orderBy('id', 'desc')->paginate(30000);
        $task1 = Msg::where('send', '=', '1')->where('call_back', '=', 'Important')->orderBy('id', 'desc')->paginate(30000);
        $task2 = Msg::where('send', '=', '1')->where('call_back', '=', 'Regular')->orderBy('id', 'desc')->paginate(30000);
        $task3 = Msg::where('send', '=', '1')->where('call_back', '=', null)->orderBy('id', 'desc')->paginate(30000);
        $task4 = Msg::where('send', '=', '1')->orderBy('id', 'desc')->paginate(30000);

        // $fullmsg=Msg::where('send','=','1')->orderBy('id', 'desc')->paginate(30000);


        $fullmsg = DB::table('msgs')->select('msgs.id', 'msgs.title', 'msgs.content', 'msgs.admin_id', 'msgs.employeeid', 'msgs.status', 'msgs.created_at', 'msgs.comment', 'msgs.checked',
            'msgs.type', 'msgs.rlt_msg', 'msgs.call_back', 'msgs.date', 'msgs.time', 'msgs.busname', 'msgs.clientfile', 'msgs.clientno', 'msgs.clientname', 'msgs.purpose', 'msgs.day', 'msgs.send',
            'msgs.recieve', 'msgs.notes', 'msgs.returndate', 'msgs.status1', 'msgs.time1', 'msgs.employee_id', 'msgs.returntime', 'msgs.client_name', 'msgs.forwordmsg', 'msgs.returnday',
            'msgs.whattodo', 'msgs.instruction', 'msgs.forword_id', 'msgs.send1', 'msgs.send2', 'msgs.ckmagges', 'msgs.busname2', 'msgs.checkstatus', 'msgs.status3',
            'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.business_id')
            ->leftJoin('commonregisters', function ($join) {
                $join->on('commonregisters.filename', '=', 'msgs.clientfile');
            })
            ->where('msgs.send', '=', '1')->orderBy('msgs.call_back', 'desc')->orderBy('msgs.date', 'desc')->paginate(30000);

        $emp1 = DB::table("employees")->select("employees.telephoneNo1", "employees.firstName", "employees.middleName", "employees.lastName", "employees.address1", "employees.city", "employees.stateId", "employees.countryId"
            , "employees.telephoneNo1", "employees.email", "employees.type", "employees.id")->get();
        //echo "<pre>";print_r($emp1);
        $emp = Fscemployee::All();
        $emp2 = Employee::All();
        $emp3 = Commonregister::All();
        // return view('fac-Bhavesh-0554/msg/msg', compact(['task4','fullmsg','task','task1','task2','task3','emp','emp1','emp2','emp3','logo']));
        return view('fac-Bhavesh-0554/msg/newmsg', compact(['task4', 'fullmsg', 'task', 'task1', 'task2', 'task3', 'emp', 'emp1', 'emp2', 'emp3', 'logo']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function search()
    {
        $logo = Logo::where('id', '=', 1)->first();
        $task = Msg::where('send', '=', '1')->where('call_back', '=', 'Urgent')->orderBy('id', 'desc')->paginate(30000);
        $task1 = Msg::where('send', '=', '1')->where('call_back', '=', 'Important')->orderBy('id', 'desc')->paginate(30000);
        $task2 = Msg::where('send', '=', '1')->where('call_back', '=', 'Regular')->orderBy('id', 'desc')->paginate(30000);
        $task3 = Msg::where('send', '=', '1')->where('call_back', '=', null)->orderBy('id', 'desc')->paginate(30000);
        $task4 = Msg::where('send', '=', '1')->orderBy('id', 'desc')->paginate(30000);
        $emp1 = DB::table("employees")->select("employees.telephoneNo1", "employees.firstName", "employees.middleName", "employees.lastName", "employees.address1", "employees.city", "employees.stateId", "employees.countryId"
            , "employees.telephoneNo1", "employees.email", "employees.type", "employees.id")->get();
//echo "<pre>";print_r($emp1);
        $emp = Fscemployee::All();
        $emp2 = Employee::All();
        $emp3 = Commonregister::All();

        if (isset($_POST['sub']) || (isset($_POST['choice1']) != '')) {
            if ($_POST['choice'] == 'New') {
                $fullmsg = DB::table('msgs')->select('msgs.id', 'msgs.title', 'msgs.content', 'msgs.admin_id', 'msgs.employeeid', 'msgs.status', 'msgs.created_at', 'msgs.comment', 'msgs.checked',
                    'msgs.type', 'msgs.rlt_msg', 'msgs.call_back', 'msgs.date', 'msgs.time', 'msgs.busname', 'msgs.clientfile', 'msgs.clientno', 'msgs.clientname', 'msgs.purpose', 'msgs.day', 'msgs.send',
                    'msgs.recieve', 'msgs.notes', 'msgs.returndate', 'msgs.status1', 'msgs.time1', 'msgs.employee_id', 'msgs.returntime', 'msgs.client_name', 'msgs.forwordmsg', 'msgs.returnday',
                    'msgs.whattodo', 'msgs.instruction', 'msgs.forword_id', 'msgs.send1', 'msgs.send2', 'msgs.ckmagges', 'msgs.busname2', 'msgs.checkstatus', 'msgs.status3',
                    'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.business_id')
                    ->leftJoin('commonregisters', function ($join) {
                        $join->on('commonregisters.filename', '=', 'msgs.clientfile');
                    })
                    ->where('msgs.send', '=', '1')->where('msgs.status', '=', '2')->where('msgs.status1', '=', '')->orderBy('msgs.call_back', 'desc')->orderBy('msgs.date', 'desc')->paginate(30000);
            } else if ($_POST['choice'] == 'All') {
                $fullmsg = DB::table('msgs')->select('msgs.id', 'msgs.title', 'msgs.content', 'msgs.admin_id', 'msgs.employeeid', 'msgs.status', 'msgs.created_at', 'msgs.comment', 'msgs.checked',
                    'msgs.type', 'msgs.rlt_msg', 'msgs.call_back', 'msgs.date', 'msgs.time', 'msgs.busname', 'msgs.clientfile', 'msgs.clientno', 'msgs.clientname', 'msgs.purpose', 'msgs.day', 'msgs.send',
                    'msgs.recieve', 'msgs.notes', 'msgs.returndate', 'msgs.status1', 'msgs.time1', 'msgs.employee_id', 'msgs.returntime', 'msgs.client_name', 'msgs.forwordmsg', 'msgs.returnday',
                    'msgs.whattodo', 'msgs.instruction', 'msgs.forword_id', 'msgs.send1', 'msgs.send2', 'msgs.ckmagges', 'msgs.busname2', 'msgs.checkstatus', 'msgs.status3',
                    'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.business_id')
                    ->leftJoin('commonregisters', function ($join) {
                        $join->on('commonregisters.filename', '=', 'msgs.clientfile');
                    })
                    ->where('msgs.send', '=', '1')->orderBy('msgs.call_back', 'desc')->orderBy('msgs.date', 'desc')->paginate(30000);


            }
        }
        return view('fac-Bhavesh-0554/msg/newmsg', compact(['task4', 'fullmsg', 'task', 'task1', 'task2', 'task3', 'emp', 'emp1', 'emp2', 'emp3', 'logo']));


    }

    public function messageview(Request $request)
    {
        $status = $request->input('status');


    }

    public function create()
    {
        $purpose = DB::table('purposes')->get();
        $emp = Fscemployee::All();
        $emp1 = Employee::where('type', '=', 'employee')->where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        $employee1 = Employee::where('status', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        return view('fac-Bhavesh-0554/msg/create', compact(['emp', 'emp1', 'purpose', 'employee1']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = $request->get('employee');
        $position = new Msg;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->day = $request->day;
        $position->title = $request->type;
        $position->content = $request->othermsg;
        $position->admin_id = $request->employee;
        $position->employeeid = $request->employees;
        $position->type = $request->type;
        $position->rlt_msg = $request->other;
        $position->call_back = $request->purpose1;
        $position->busname = $request->busname;
        $position->clientfile = $request->clientfile;
        $position->clientno = $request->clientno;
        $position->clientname = $request->clientname;
        $position->purpose = $request->purpose;
        $position->send1 = 1;
        $position->recieve = 1;
        $position->status = 2;
        $position->status3 = 'New';

        $position->save();
        return redirect('fac-Bhavesh-0554/msg')->with('success', 'Message Sent Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $returnValue11 = Msg::where('id', '=', $id)
            ->update([
                'checkstatus' => '0'
            ]);

        $purpose = DB::table('purposes')->get();
        $task = Msg::where('id', $id)->first();
        $emp = Fscemployee::All();
        $client = Employee::where('status', '=', '1')->where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('check', '=', '1')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        $emp1 = Employee::where('type', '=', 'employee')->where('id', '=', $task->employeeid)->first();
        $emp2 = Employee::where('id', '=', $task->employeeid)->first();
        // print_r($emp2);
        $emp3 = Employee::where('id', '=', $task->admin_id)->where('type', '=', $task->title)->first();
        $userclient1 = Commonregister::where('id', '=', $task->employeeid)->first();
        $userclient2 = Commonregister::where('id', '=', $task->admin_id)->where('status', '=', $task->title)->first();
        // $newclient = Msg::where('id',$id)->update(['status' => 1,'status3' => 'All']);
        $userclient = Commonregister::All();
        return View('fac-Bhavesh-0554.msg.edit', compact(['task', 'emp', 'emp1', 'purpose', 'client', 'emp2', 'userclient', 'userclient1', 'emp3', 'userclient2']));
    }

    public function removepurpose2(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('purposes')->where('id', $id)->delete();
        if ($student) {
            echo 'Data Deleted';
        }
    }

    public function removemsgs(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('msgs')->where('id', $id)->delete();
        if ($student) {
            echo 'Data Deleted';
        }
    }

    public function removenotes(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('notesrelateds')->where('id', $id)->delete();
        if ($student) {
            $data = array();
            $remove = DB::table('notesrelateds')->get();
            foreach ($remove as $rm) {
                $data[] = [
                    'status' => 'success',
                    'id' => $rm->id,
                    'newopt' => $rm->notesrelated];
            }
        }
        return json_encode($data);
    }

    public function removerelateds(Request $request)
    {
        $id = $request->input('id');
        $student = DB::table('relatednames')->where('id', $id)->delete();
        if ($student) {
            $data = array();
            $remove = DB::table('relatednames')->get();
            foreach ($remove as $rm) {
                $data[] = [
                    'status' => 'success',
                    'id' => $rm->id,
                    'newopt' => $rm->relatednames];
            }
        }
        return json_encode($data);
    }

    public function purposes()
    {
        $newopt = Input::get('newopt');
        //$sign = Input::get('sign');
        if (DB::table('purposes')->where('purposename', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {

            $position = DB::insert('insert into purposes (purposename) values (?)', array($newopt));

            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {/*
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
                               
        ]);*/

        //ECHO ltrim($_POST['busname1']);
        //ECHO "<PRE>"; print_r($_POST);EXIT;
        //return $request->busname;
        $position = Msg::find($id);
        //$position->title= $request->title;
        $position->content = $request->description;
        // $position->admin_id= $request->admin_id;
        //$position->employeeid= $request->cd;
        // $position->type= $request->type;
        $position->day = $request->day;
        $position->rlt_msg = $request->rlt_msg;
        // $position->call_back= $request->call_back;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->busname = $request->busname;
        //$position->clientfile= $request->clientfile;
        //$position->clientno= $request->clientno;
        //$position->clientname= $request->clientname;
        // $position->purpose= $request->purpose;
        $position->status1 = $request->status1;
        $position->notes = $request->notes;
        $position->returndate = $request->returndate;
        $position->returntime = $request->returntime;
        $position->returnday = $request->returnday;
        $position->whattodo = $request->whattodo;

        $position->instruction = $request->instruction;
        if ($request->client_name == '') {

        } else {


        }

        if ($request->status1 == 'Forword') {
            $position->client_name = $request->client_name;
            $position->forword_id = $request->client_name;
            $position->forwordmsg = $request->forwordmsg;
            $position->recieve = 1;
            $position->send = '';
        }
        // $position->send= 1;
        $position->update();
        return redirect('fac-Bhavesh-0554/msg')->with('success', 'Message Sent Successfully');
    }


    public function clientname12(Request $request)
    {
        if (($request->id == 'employee') or ($request->id == 'user') or ($request->state == 'employee')) {
            $data = Employee::select('firstName', 'id', 'middleName', 'lastName', 'type', 'employee_id', 'telephoneNo1')->where('type', $request->id)->orWhere('id', $request->id)->take(1000)->get();
        } else {
            //$data = Commonregister::select('firstname','id','middlename','lastname','business_no','status','filename','business_name','company_name')->where('status','Active')->orWhere('id', $request->id)->get();
            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_name',
                    'commonregisters.business_id',
                    'commonregisters.business_no',
                    'commonregisters.status')
                ->where('status', 'Active')
                ->orWhere('id', $request->id)->get();
            $data = array();
            foreach ($commonregiser as $row) {
                if ($row->status == '1' || $row->status == 'Active') {
                    if ($row->business_id == '6') {
                        $data[] = array(
                            'filename' => $row->entityid,
                            'company_name' => $row->fname . ' ' . $row->mname . ' ' . $row->lname,
                            'business_no' => $row->business_no
                        );
                    } else {
                        $data[] = array(
                            'filename' => $row->entityid,
                            'company_name' => $row->cname,
                            'business_no' => $row->business_no
                        );
                    }
                }
            }
        }
        //foreach($data as $d):
        //  echo $d->status;
        //endforeach;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Msg::where('id', $id)->delete();
        return redirect(route('msg.index'));
    }
}