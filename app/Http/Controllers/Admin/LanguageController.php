<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Language;
use Illuminate\Http\Request;


class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $language = Language::All();
        return view('fac-Bhavesh-0554.language.language', compact('language'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.language.language-add-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'language_name' => 'required'

        ]);

        $language = new Language;
        $language->language_name = $request->language_name;
        $language->save();
        return redirect(route('language.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
        $language = Language::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.language.language', compact('language'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $language = Language::where('id', $id)->first();
        return View('fac-Bhavesh-0554.language.language-edit', compact('language'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'language_name' => 'required'

        ]);

        $language = Language::find($id);
        $language->language_name = $request->language_name;
        $language->update();
        return redirect(route('language.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Language::where('id', $id)->delete();
        return redirect(route('language.index'));
    }
}