<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\admin_professional;
use App\Model\Cestatus;
use App\Model\Email;
use App\Model\Employee;
use App\Model\Insurance;
use DB;
use Illuminate\Http\Request;

class CestatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professional = admin_professional::where('ce_check', '=', '1')->get();
        $admin = Admin::All();
        $cestatus = Cestatus::All();
        $insurance = Insurance::All();
        return view('fac-Bhavesh-0554/cestatus/cestatus', compact(['professional', 'cestatus', 'insurance']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch = new Cestatus;
        $bulletin = implode(",", $request->get('notes'));
        $dateStr = strtotime($request->reporting_year);
        $dateArray = date('Y', $dateStr);
        $newdate = date('d-M-Y', strtotime("+1 year", $dateStr));
        $branch->profession = $request->profession;
        $branch->reporting_period = $request->reporting_period;
        $branch->reporting_year = $request->reporting_year;
        $branch->rules = $request->rules;
        $branch->gen_hour = $request->gen_hour;
        $branch->gen_hour_1 = $request->gen_hour_1;
        $branch->spe_hour = $request->spe_hour;
        $branch->spe_hour_1 = $request->spe_hour_1;
        $branch->spe_total_hr = $request->sum1;
        $branch->gen_total_hr = $request->sum;
        $branch->year = $dateArray;
        $branch->due_date = $newdate;
        $branch->professionid = $request->professionid;
        $branch->notes = $bulletin;
        $branch->save();
        return redirect('fac-Bhavesh-0554/cestatus')->with('success', 'Add CE Status Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();
        $emp = Employee::All();
        $leave = Email::where('id', $id)->first();
        return view('fac-Bhavesh-0554/cestatus.edit', compact(['leave', 'emp']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Email::find($id);
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->update();
        return redirect('fac-Bhavesh-0554/cestatus')->with('success', 'Update Email Successfully');
    }

    public function getdata1(Request $request)
    {
        $output = "";
        $products = DB::table('ce_status')->where('year', 'LIKE', '%' . $request->id . "%")->get();
        if ($products) {
            $sum = 0;
            $sum1 = 0;
            $sum2 = 0;
            $sum3 = 0;
            foreach ($products as $key => $product) {
                $sum += $product->gen_hour;
                $sum1 += $product->spe_hour;
                $sum2 += $product->gen_total_hr;
                $sum3 += $product->spe_total_hr;
                // $t1 = sprintf("%0.2f", time_to_float($product->gen_hour));
                $output .= '<tr>' .
                    '<td>' . $product->year . '</td>' .
                    '<td><table style="margin:0" class="table table-hover table-bordered"><tr><td scope="col">' . $product->gen_hour . '</td><td scope="col">' . $product->spe_hour . '</td><td scope="col">' . $product->spe_hour . '</td></tr></table></td>' .

                    '<td><table style="margin:0" class="table table-hover table-bordered"><tr><td scope="col">' . $product->spe_total_hr . '</td><td scope="col">' . $product->spe_hour . '</td><td scope="col">' . $product->spe_hour . '</td></tr></table></td>' .
                    '<td>' . $product->due_date . '</td>' .
                    '</tr>';
            }
            /*$output .='<tr>'.
                '<td>'.$sum.'</td>'.
                '<td>'.$sum1.'</td>'.
                '<td>'.$sum2.'</td>'.
                '<td>'.$sum3.'</td>'.
            '</tr>';*/
        } else {
            $output .= '<tr>' .
                '<td>Data is not fount</td>' .
                '</tr>';
        }
        return Response($output);
    }


    public function cestatus(Request $request)
    {
        $data = Cestatus::select('profession', 'reporting_period', 'reporting_year', 'rules', 'notes', 'gen_hour', 'gen_hour_1', 'spe_hour', 'spe_hour_1', 'spe_total_hr', 'gen_total_hr', 'year', 'due_date')->where('professionid', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Email::where('id', $id)->delete();
        return redirect(route('fac-Bhavesh-0554/cestatus'));
    }
}