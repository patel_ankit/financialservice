<?php

namespace App\Http\Controllers\Admin;

use App\Front\applyservicess;
use App\Front\Commonregister;
use App\Front\Scheduler_table;
use App\Http\Controllers\Controller;
use App\Model\Citystate;
use App\Model\Logo;
use App\Model\Service;
use App\Model\Servicetype;
use App\Model\taxstate;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ServiceinquiryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $logo = Logo::where('id', '=', 1)->first();
        $name = $request->input('typeofcorp');
        //echo $name;die;
        //$service = Service::All();
        //echo "<pre>";
        // print_r($service);

        $service = DB::table('services')->get();
        if (isset($name)) {
            // Financial Services
            //$applyservice = applyservicess::where('typeofcorp', '=', $name)->get();
            $applyservice = DB::table('applyservicesses')->where('ser_check', '!=', 0)->where('typeofcorp', '=', $name)->orderBy('created_at', 'desc')->get();

            $admin_professional1 = DB::table('applyservicesses as t1')->select('t1.*', 't2.id as ids', 'service_name')
                ->leftJoin('services as t2', function ($join) {
                    $join->on('t1.id', '=', 't2.id');
                })
                ->where('t1.typeofcorp', '=', $name)
                ->orderBy('t1.id', 'ASC')->get();

        } else {
            //$applyservice = applyservicess::get();
            $applyservice = DB::table('applyservicesses')->where('ser_check', '!=', 0)->orderBy('created_at', 'desc')->get();
        }


        return view('fac-Bhavesh-0554/servicesprocess/servicesprocess', compact(['logo', 'applyservice', 'service']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {


        $service = Servicetype::where('servicetype', '=', $id)->get();
        return view('servicesprocess/create', compact(['service']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $applyservice = new applyservicess;
        $applyservice->serviceid = $request->serviceid;
        $applyservice->typeofservice = $request->typeofservice;
        $applyservice->newbusiness = $request->newbusiness;
        $applyservice->domestic_business = $request->domestic_business;
        $applyservice->foreign_business = $request->foreign_business;
        $applyservice->choice = $request->c_email;
        $applyservice->reservation = $request->company_name;
        $applyservice->s_st_choice = $request->business_name;
        $applyservice->t_st_choice = $request->type_of_business;
        $applyservice->stateId = $request->stateId;
        $applyservice->address1 = $request->address1;
        $applyservice->address2 = $request->address2;
        $applyservice->city = $request->city;
        $applyservice->state = $request->state;
        $applyservice->scheduler_fname = $request->m_name;
        $applyservice->county = $request->business_address;
        $applyservice->address3 = $request->telephonetype;
        $applyservice->address4 = $request->address4;
        $applyservice->business_city = $request->business_city;
        $applyservice->business_state = $request->business_state;
        $applyservice->business_zip = $request->business_zip;
        $applyservice->county2 = $request->county2;
        $applyservice->agent_fname = $request->c_person_name;
        $applyservice->agent_mname = $request->c_type;
        $applyservice->agent_lname = $request->agent_lname;
        $applyservice->typeofbusiness = $request->typeofbusiness;
        $applyservice->shares = $request->shares;
        $applyservice->common = $request->common;
        $applyservice->typeofcorp = $request->typeofcorp;
        $applyservice->date = $request->date;
        $applyservice->sign = $request->sign;

        $applyservice->unique = $request->unique;
        $applyservice->save();

        return redirect(route('servicesprocess.index'))->with('success', 'You Are SuccessFully Apply Service');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function show(Applyservice $applyservice)
    {
        //
    }

    public function checkemailAvailability()
    {

        $user = DB::table('applyservices')->where('email', Input::get('email'))->count();

        if ($user > 0) {
            $isAvailable = FALSE;
        } else {
            $isAvailable = TRUE;
        }
        echo json_encode(
            array(
                'valid' => $isAvailable
            ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // print_r($request);exit;
        $newclient = applyservicess::where('id', $id)->update(['sign' => 2]);
        $service = Service::All();
        $taxstate = taxstate::All();
        $servicetype = applyservicess::where('id', $id)->first();
        $user1 = DB::table('client_shareholders')->where('client_id', $id)->get();
        //print_r($user1);die;
        return View('fac-Bhavesh-0554.servicesprocess.edit', compact(['service', 'servicetype', 'taxstate', 'user1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        if ($request->checkedclient_id != '') {
            // echo "aaa";
            // echo "<pre>";
            // print_r($_REQUEST);die;

            if ($request->client_id1 != '' && $request->client_id2 != '' && $request->client_id3 != '') {
                $resStr = strtoupper($request->client_id1);
                //$files = $request->client_id1.'-'.$request->client_id2.'-'.$resStr;
                $files = $resStr . '-' . $request->client_id2 . '-' . $request->client_id3;
            } else {
                $resStr = strtoupper($request->client_id1);
                $files = $resStr . '-' . $request->client_id2;
            }


            //$myString= $request->client_id1;
            //$myString = str_replace("-", "", $myString);
            $applyservice = new Commonregister;
            //$applyservice->register_id = $myString;
            $applyservice->register_id = $request->client_id2;
            $applyservice->filename = $files;
            $applyservice->recstatus = '1';
            $applyservice->status = $request->status;

            $applyservice->company_name = $request->company_name;
            $applyservice->business_name = $request->type_of_business;
            $applyservice->address = $request->address;
            $applyservice->city = $request->city;
            $applyservice->city_1 = $request->city;
            $applyservice->state_1 = $request->state;

            $applyservice->stateId = $request->state;
            $applyservice->countryId = 'USA';
            $applyservice->zip = $request->zip;
            $applyservice->zip_1 = $request->zip;

            $applyservice->business_no = $request->business_telephone;
            $applyservice->businesstype = $request->telephoneNo1Type;
            $applyservice->businessext = $request->ext1;
            $applyservice->website = $request->website;
            $applyservice->email = $request->email;
            $applyservice->etelephone1 = $request->phonenumber;

            $applyservice->eteletype1 = $request->phonetype;

            $applyservice->eext1 = $request->ext;
            $applyservice->minss = $request->nametype;
            $applyservice->firstname = $request->fname;
            $applyservice->middlename = $request->mname;
            $applyservice->lastname = $request->lname;
            $applyservice->save();


            $applyservice = applyservicess::find($id);
            $applyservice->ser_check = '0';

            $applyservice->update();


        } else {
            // echo "bbb";
            // echo "<pre>";
            // print_r($_REQUEST);die;

            $applyservice = applyservicess::find($id);
            if ($request->get('linense_type')) {
                $bulletin = implode(",", $request->get('linense_type'));
            } else {
                $bulletin = '';
            }

            $applyservice->serviceid = $request->serviceid;
            //$applyservice->uniques= $request->unique; 
            $applyservice->typeofcorp = $request->typeofcorp;
            $applyservice->typeofservice = $request->typeofservice;
            $applyservice->company_name_1 = $request->company_name_1;
            $applyservice->company_name_2 = $request->company_name_2;

            $applyservice->fname = $request->fname;
            $applyservice->mname = $request->mname;
            $applyservice->lname = $request->lname;
            $applyservice->address = $request->address;
            $applyservice->city = $request->city;
            $applyservice->state = $request->state;
            $applyservice->zip = $request->zip;
            $applyservice->phonenumber = $request->phonenumber;
            $applyservice->phonetype = $request->phonetype;
            $applyservice->ext = $request->ext;
            $applyservice->website = $request->website;
            $applyservice->newbusiness = $request->newbusiness;
            $applyservice->linense_type = $request->$bulletin;
            $applyservice->typeofservice1 = $request->typeofservice1;
            $applyservice->newbusiness1 = $request->newbusiness1;
            $applyservice->domestic_business = $request->domestic_business;
            $applyservice->foreign_business = $request->foreign_business;
            $applyservice->company_name = $request->company_name;
            $applyservice->business_name = $request->business_name;
            $applyservice->type_of_business = $request->type_of_business;
            $applyservice->business_telephone = $request->business_telephone;
            $applyservice->telephoneNo1Type = $request->telephoneNo1Type;
            $applyservice->ext1 = $request->ext1;
            $applyservice->term = $request->terms;
            $applyservice->email = $request->basic_email;
            $applyservice->username = $request->cemail;
            $applyservice->directpay = $request->directpay;
            $applyservice->availabilitynote = $request->availabilitynote;
            $applyservice->business_address = $request->business_address;
            $applyservice->c_type = $request->c_type;


            //$applyservice->password=bcrypt($request->password);
            // $applyservice->newpassword= $request->password;  
            $applyservice->update();

            if ($request->status == 'Approval') {
                $name = $request->fname . ' ' . $request->lname . ' ' . $request->mname;
                //DB::table('users')->where('user_id', $id)->update(['type'=>1,'name'=>$name]);
                $client_id = $request->client_id;
                $email = $request->cemail;
                $password = $request->password;
                $user_type = $request->typeofcorp;
                $user = User::where('email', '=', $email)->first();
                if ($user === null) {
                    $insert = DB::insert("insert into commonregisters(`filename`,`status`,`company_name`,`first_name`,`middle_name`,`last_name`,`firstname`,`middlename`,`lastname`,`address`,`business_name`,`city`,`stateId`,`zip`,`email`,`website`,`business_no`,`businesstype`,`businessext`,`serviceid`) values('" . $client_id . "','" . $request->status . "','" . $request->company_name . "','" . $request->fname . "','" . $request->mname . "','" . $request->lname . "','" . $request->fname . "','" . $request->mname . "','" . $request->lname . "','" . $request->address . "','" . $request->business_name . "','" . $request->city . "','" . $request->state . "','" . $request->zip . "','" . $request->email . "','" . $request->website . "','" . $request->phonenumber . "','" . $request->phonetype . "','" . $request->ext . "','" . $id . "')");
                    $emp = Commonregister::where('serviceid', '=', $id)->first();
                    $insert = DB::insert("insert into users(`name`,`email`,`password`,`newpassword`,`user_id`,`user_type`,`type`) values('" . $name . "','" . $email . "','" . bcrypt($password) . "','" . $password . "','" . $emp->id . "','" . $user_type . "','1')");
                    $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' => $password, 'client' => $client_id);
                    \Mail::send('activation', $data, function ($message) use ($data) {
                        $message->to($data['email'])->from($data['from'], $data['first_name'], $data['password'], $data['client'])->subject('Welcome!');
                    });
                } else {
                    //return $user->type;
                    if ($user->type === '2') {
                        $user_type = $request->typeofcorp;
                        $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type, 'type' => 1, 'name' => $name, 'password' => bcrypt($password), 'newpassword' => $password]);
                        $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' => $password, 'client' => $client_id);
                        \Mail::send('activation', $data, function ($message) use ($data) {
                            $message->to($data['email'])->from($data['from'], $data['first_name'], $data['password'], $data['client'])->subject('Welcome!');
                        });
                    } else {
                        $user_type = $request->typeofcorp;
                        $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type, 'type' => 1, 'name' => $name]);
                    }
                }
            } else {
                // echo $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   die;
                $name = $request->fname . ' ' . $request->mname . ' ' . $request->lname;
                $status = $request->status;
                $email = $request->cemail;
                // $name = $request->first_name;
                $user_type = $request->user_type;
                $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type, 'type' => 2, 'name' => $name]);
                $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com');
                \Mail::send('fac-Bhavesh-0554/hold', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['first_name'])->subject('Welcome!');
                });
            }

        }


        return redirect(route('servicesprocess.index'))->with('success', 'You Are SuccessFully Apply Service');
    }


//     public function store(Request $request)
//     {
//         //print_r($_REQUEST);die;
//         $this->validate($request,[]);

//           if($request->client_id3!='' && $request->client_id3!='' && $request->filename3!='')
//           {
//               $resStr = strtoupper($request->client_id3);
//               $files = $request->client_id1.'-'.$request->client_id2.'-'.$resStr;
//           }
//           else
//           {
//               $files = $request->client_id1.'-'.$request->client_id2;
//           }


//           $myString= $request->client_id1;
//           $myString = str_replace("-", "", $myString);
//           $businessbrand = new Commonregister;
//           $businessbrand->register_id = $myString;
//           $businessbrand->filename = $files;
//           $businessbrand->recstatus = '1';

//           $businessbrand->status = $request->status;
//           $businessbrand->business_id = $request->business_id;  
//           $businessbrand->business_name = $request->business_name;      
//           $businessbrand->business_cat_id = $request->business_catagory_name;
//           $businessbrand->company_name1= $request->company_name1;
//           $businessbrand->company_name= $request->company_name;
//           $businessbrand->service_period = $request->serviceperiod;
//           $businessbrand->first_name = $request->firstname;
//           $businessbrand->middle_name = $request->middlename;      
//           $businessbrand->last_name = $request->lastname; 
//           $businessbrand->legalname= $request->legalname;
//           $businessbrand->dbaname = $request->dbaname;
//           $businessbrand->address = $request->address;      
//           $businessbrand->address1 = $request->address1;
//           $businessbrand->city= $request->city;        
//           $businessbrand->countryId = $request->countryId;      
//           $businessbrand->stateId = $request->stateId; 
//           $businessbrand->zip = $request->zip;
//           $businessbrand->mailing_address= $request->second_address;
//           $businessbrand->mailing_address1 = $request->second_address1;
//           $businessbrand->mailing_city = $request->second_city;      
//           $businessbrand->mailing_state = $request->second_state;
//           $businessbrand->mailing_zip = $request->second_zip;
//           $businessbrand->email = $request->email;
//           $businessbrand->mobile_no = $request->mobile_no;  
//           $businessbrand->business_no= $request->business_no;
//           $businessbrand->motelephoneile1 = $request->motelephoneile1;
//           $businessbrand->businesstype= $request->businesstype;
//           $businessbrand->telephoneNo2Type= $request->telephoneNo2Type;
//           $businessbrand->businessext= $request->businessext;
//           $businessbrand->business_fax = $request->business_fax;
//           $businessbrand->website = $request->website;      
//           $businessbrand->business_brand_id = $request->business_brand_name;
//           $businessbrand->business_brand_category_id= $request->business_brand_category_name;
//           $businessbrand->business_store_name = $request->business_store_name;
//           $businessbrand->business_address = $request->business_address;      
//           $businessbrand->business_city = $request->business_city; 
//           $businessbrand->business_country= $request->business_country;
//           $businessbrand->business_state = $request->business_state;
//           $businessbrand->bussiness_zip = $request->bussiness_zip;      
//           $businessbrand->level = $request->level;
//           $businessbrand->setup_state= $request->setup_state;        
//           $businessbrand->county_name = $request->county_name;      
//           $businessbrand->county_no = $request->county_no; 
//           $businessbrand->type_of_activity = $request->type_of_activity;
//           $businessbrand->department= $request->department;
//           $businessbrand->due_date = $request->due_date; 
//           $businessbrand->user_type = $request->user_type;        
//           $businessbrand->minss = $request->minss;        
//           $businessbrand->firstname = $request->firstname;        
//           $businessbrand->middlename = $request->middlename;        
//           $businessbrand->lastname = $request->lastname;        
//           $businessbrand->contact_address1 = $request->contact_address1;        
//           $businessbrand->contact_address2 = $request->contact_address2;        
//           $businessbrand->city_1 = $request->city_1;        
//           $businessbrand->state_1 = $request->state_1;        
//           $businessbrand->zip_1 = $request->zip_1;        
//           $businessbrand->etelephone1 = $request->etelephone1;
//           //  $businessbrand->mobile_1 = $request->mobile_1;       
//           $businessbrand->eteletype1 = $request->mobiletype_1;        
//           $businessbrand->eext1 = $request->ext2_1;        
//           //   $businessbrand->etelephone2 = $request->mobile_2; 
//           $businessbrand->mobile_2 = $request->mobile_2; 
//           $businessbrand->eteletype2 = $request->mobiletype_2;        
//           $businessbrand->eext2 = $request->ext2_2;        
//           $businessbrand->email_1 = $request->email_1;        
//           $businessbrand->contact_fax_1 = $request->contact_fax_1;    
//           $businessbrand->nametype = $request->nametype;
//           $businessbrand->billingtoo = $request->billingtoo;
//           $businessbrand->faxbli3 = $request->faxbli3;
//           $businessbrand->guardian = isset($request->guardian) ? $request->guardian:"";
//           $businessbrand->guardian2 = isset($request->guardian2) ? $request->guardian2:"";
//           $businessbrand->faxbli3_g = $request->faxbli3_g;

//           $businessbrand->faxbli1 = $request->faxbli2;
//           $businessbrand->emailbli1 = $request->emailbli2;


//           if($request->business_id=='6')
//           {
//           $businessbrand->CL = $request->CL;        
//           $businessbrand->other_maritial_status1 = $request->other_maritial_status;        
//           $businessbrand->personalname = $request->personalname;        
//           $businessbrand->maritial_first_name = $request->maritial_first_name;        
//           $businessbrand->maritial_middle_name = $request->maritial_middle_name;        
//           $businessbrand->maritial_last_name = $request->maritial_last_name;  
//           $businessbrand->maritial_spouse = $request->maritial_spouse;    
//           $businessbrand->maritial_spouse1 = $request->maritial_spouse1;
//           $businessbrand->telephoneNo2Type = $request->telephoneNo2Type;
//           $businessbrand->motelephoneile1 = $request->motelephoneile1;
//           $businessbrand->ext2 = $request->ext2;
//           }
//           $businessbrand->newclient = 2;
//           $businessbrand->save(); 
//           $em = $request->email;
//           $common = Commonregister::where('email',$em)->first();
//           $id =$common->id;
//           $sign = $request->ckq;


//           $l = 0;
//                 if(empty($request->pricetype))
//                 {
//                 }
//                 else
//                 {
//                 foreach($request->regularprice as $typeofservice11)
//                 { 
//                 $typeofservice1 = $request->typeofservice[$l];

//                 $regularprice1 = $request->regularprice[$l];
//                 $comboprice1 = $request->comboprice[$l];
//                 $price1 = $request->price[$l];
//                 $serviceincludes1 = $request->serviceincludes[$l];
//                 $empids = $request->employee_res[$l];
//                 $empnotes = $request->monthlynote[$l];

//                 $l++;
//                 $clientservices = DB::insert("insert into clientservices(`employee_id`,`employeenote`,`sign`,`total`,`serviceincludes`,`note`,`clientid`,`pricetype`,`currency`,`typeofservice`,
//                 `serviceperiod`,`regularprice`,`comboprice`,`price`,`discountprice`) 
//                 values('".$empids."','".$empnotes."','".$sign."','".$request->totalprice1."','".$serviceincludes1."','".$request->pricenote."','".$id."','".$request->pricetype."','".$request->currency."','".$typeofservice1."',
//                 '".$request->serviceperiod."','".$regularprice1."','".$comboprice1."','".$price1."','".$request->discountprice."')");
//                 //$users = DB::table('clientservices')->where('regularprice','=','')->delete();    
//                 }
//                 }

//               /* $ll = 0;
//                 if(empty($request->titles))
//                 {

//                 }
//                 else{
//                 foreach($request->titles as $typeofservice11)
//                 { 
//                 $taxid1=$request->taxid[$ll];
//                 $taxnote1=$request->taxnote[$ll];
//                 $titles1=$request->titles[$ll];
//                 $comboprice12=$request->comboprice1[$ll];
//                 $regularprice12=$request->regularprice1[$ll];
//                 if(empty($request->taxprice[$ll]))
//                 {
//                     $taxprice1=0;
//                 }
//                 else
//                 {
//                 $taxprice1=$request->taxprice[$ll];

//                 }

//                 $ll++;
//                 if(empty($taxid1))
//                 { 
//                     if(!empty($titles1)){
//                      //   return $id;
//                 $clientservicetitles = DB::insert("insert into clientservicetitles(`clientid`,`servicetype`,`regularprice1`,`comboprice1`,`titles`,`taxprice`,`taxnote`) 
//                 values('".$id."','3','".$regularprice12."','".$comboprice12."','".$titles1."','".$taxprice1."','".$taxnote1."')");
//                 $users = DB::table('clientservicetitles')->where('regularprice1', '=', '')->delete();  
//                  }
//                 }
//                 }
//                 }
//                 */

//         if(($request->status=='Pending') or ($request->status=='Hold'))
//         {
//         $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
//         $status = $request->status;
//         $email = $request->email;
//         $user_type=  $request->user_type;
//         $name = $request->first_name;  
//         DB::table('users')->where('user_id', $id)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
//         $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
//         $data = array('email' => $email, 'first_name' => $name, 'from' =>'vijay@businesssolutionteam.com');       
//         /*\Mail::send('fac-Bhavesh-0554/hold', $data, function( $message ) use ($data)
//         {
//         $message->to($data['email'] )->from($data['from'], $data['first_name'])->subject('Account Approval');
//         });*/
//         }
//         else
//         {
//         $status = $request->status;
//         $email = $request->email;
//         $name = $request->first_name;
//         $password =  mt_rand();
//         $user_type=  $request->user_type;
//         $client_id = $files;
//         $ccc = $request->filename;
//         $user = User::where('email', '=', $email)->where('client_id', '=', $client_id)->first();
//         }

//         return redirect('fac-Bhavesh-0554/customer')->with('success','Client added successfully');
// }


    public function getzipcode(Request $request)
    {

        $data = Citystate::select('country', 'state', 'city', 'county')->where('zipcode', $request->zip)->take(100)->get();

        return response()->json($data);
    }

    public function getstate(Request $request)
    {

        $data = Citystate::select('country', 'zipcode', 'city', 'county', 'state')->where('state', $request->state)->take(100)->get();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Front\Applyservice $applyservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        applyservicess::where('id', $id)->delete();
        Scheduler_table::where('serviceid', $id)->delete();
        return redirect(route('servicesprocess.index'));
    }
}