<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\ApplyEmployment;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Holiday;
use App\Model\Language;
use App\Model\Logo;
use App\Model\Msg;
use App\Model\Position;
use App\Model\Rules;
use App\Model\Schedule;
use App\Model\Task;
use App\Model\Team;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Redirect;
use Session;
use Validator;
use View;


class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //exit('22222222222');
        $position = Position::All();
        $branch = Branch::All();
        $logo = Logo::where('id', '=', 1)->first();
        $employee = Employee::where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->orderBy('firstName', 'asc')->get();
        //  echo "<pre>";print_r($employee);
        return view('fac-Bhavesh-0554/employee/employee', compact(['employee', 'position', 'branch', 'logo']));
    }

    public function checkemailAvailability(Request $request)
    {
        $user = DB::table('employees')->where('email', Input::get('email'))->count();
        if ($user > 0) {
            return "<span style=\"color:red;font-size:16px;\">Email Id already exists</span>";
        } else {
            return "<span style=\"color:green;font-size:16px;\">Email Id Available</span>";
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $team = Team::orderBy('team', 'asc')->get();
        $position = Position::All();
        $branch = Branch::All();
        return view('fac-Bhavesh-0554/employee/create', compact(['team', 'position', 'branch']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        if ($request->hasFile('photo')) {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        } else {
            $filname = '';
        }
        $employee = new Employee;
        $employee->employee_id = $request->employee_id;
        $employee->type = $request->type;
        $employee->team = $request->team;

        $employee->firstName = $request->firstName;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->categorys = $request->categorys;

        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->nametype = $request->nametype;
        $employee->photo = $filname;
        //   $employee->status=1;
        // $employee->check=1;
        $employee->save();

        $id = $employee->id;


        if ($request->check == '0') {
            $status = 0;
            $password1 = '';
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 0, 'name' => $name]);
            DB::table('employees')->where('id', $id)->update(['check' => 0]);
        } else {
            $status = '1';
            $email = $request->email;
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 1, 'name' => $name]);
            if ($request->status == '0') {
                $password = mt_rand();
                $cc = $request->type;
                $password1 = bcrypt($password);
                $user = Fscemployee::where('email', '=', $email)->first();
                if ($user === null) {
                    $insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`) values('" . $name . "','" . $email . "','" . bcrypt($password) . "','" . $password . "','" . $id . "','1')");
                    $data = array('email' => $email, 'firstName' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' => $password, 'type' => $cc);
                    /*\Mail::send( 'fac-Bhavesh-0554/employe', $data, function( $message ) use ($data)
                    {
                    $message->to($data['email'],$data['firstName'])->from( $data['from'], $data['firstName'], $data['password'], $data['type'] )->subject( 'FSC Employee' );
                    }); 
                    */
                }
            } else {
                $password1 = $request->password;
            }
        }
        return redirect('fac-Bhavesh-0554/employee')->with('success', 'Employee Seccessfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        // $super = DB::table('super')->groupBy('username')->get();
        $super = DB::table('employees')->join('super', function ($join) {
            $join->on('employees.id', '=', 'super.username')
                ->groupBy('super.username');
        })->get();


        $newclient = Employee::where('id', $id)->update(['newemp' => 2]);
        $emp = Employee::where('id', $id)->first();
        $super1 = Employee::where('super', '=', '1')->get();
        $empfsc = Fscemployee::where('user_id', $id)->first();
        $position = Position::orderBy('position', 'asc')->get();
        $language = Language::orderBy('language_name', 'asc')->get();
        $holiday = Holiday::orderBy('holiday_name', 'asc')->get();

        $rules = Rules::All();
        //$branch = Branch::All();
        $branch = DB::table('branches')->select('city')->groupBy('city')->get();
        $info1 = DB::table('employee_pay_info')->where('employee_id', $id)->first();
        $employes = DB::table('employee_rules')->where('emp_id', $id)->get();
        $employes2 = DB::table('employee_rules')->where('emp_id', $id)->first();
        $info = DB::table('employee_pay_info')->where('employee_id', $id)->get();
        $reviewinfo = DB::table('emp_review')->where('emp_id', $id)->get();
        $whinfo = DB::table('emp_wh')->where('emp_id', $id)->get();
        $whinfo2 = DB::table('emp_wh')->where('emp_id', $id)->get();

        $info3 = DB::table('employee_other_info')->where('employee_id', $id)->get();
        $info2 = DB::table('employee_other_info')->where('employee_id', $id)->first();
        $review1 = DB::table('employee_review')->where('employee_id', $id)->get();
        $review = DB::table('employee_review')->where('employee_id', $id)->first();
        $admin_notes = DB::table('notes')->where('admin_id', '=', $id)->where('type', '=', $emp->type)->get();
        //   $conversation = DB::table('conversation_sheet as t1')->select('t1.*','t2.id as ids')
        //   ->Join('employees as t2', function($join){ $join->on('t2.id', '=','t1.employeeuserid');})
        //   ->where('t1.employeeuserid','=',$id)
        //   ->get();
        $service1 = DB::table('typeofser')->whereIn('id', [2, 8])->get();
        $service2 = DB::table('taxtitles')->get();
        $accservice1 = DB::table('typeofser')->whereIn('id', [2, 8])->get();
        $accservice2 = DB::table('taxtitles')->get();

        $conversation = DB::table('conversation_sheet as t1')->select('t1.*', 't2.id as ids', 't2.relatednames')
            ->Join('relatednames as t2', function ($join) {
                $join->on('t2.id', '=', 't1.conrelatedname');
            })
            ->where('t1.employeeuserid', '=', $id)
            ->get();

        $listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type', 'employee')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $relatedNames = DB::table('relatednames')->get();

        $notesdata = DB::table('notes_sheet as t1')->select('t1.*', 't2.id as ids', 't2.notesrelated')
            ->Join('notesrelateds as t2', function ($join) {
                $join->on('t2.id', '=', 't1.noterelated');
            })
            ->where('t1.noteemployeeuserid', '=', $id)
            ->get();
        //print_r($notesdata);die;
        $NotesNames = DB::table('notesrelateds')->get();
        $team = Team::orderBy('team', 'asc')->get();
        $emp_emai_rights = DB::table('emp_email_rights as remp')->select('eemp.id as empemailid', 'eemp.email', 'remp.access', 'remp.id')
            ->leftjoin('email_emp as eemp', function ($join) {
                $join->on('remp.emp_email_id', '=', 'eemp.id');
            })->where('remp.emp_id', $id)
            ->get();
        $emp_access = DB::table('email_emp')->where('for_whom', $id)->first();


        $getEmpId = DB::table('employees')->Where('id', $id)->first();
        $appoinment = DB::table('appointment_participant')->Where('participant_id', $getEmpId->employee_id)->get();
        $appodata = array();

        foreach ($appoinment as $appt) {

            //echo $this->getsingleTeamName($appo->appo_with) .'-'. $appo->appo_with."<br/>";
            $appo = DB::table('appointment')->Where('id', $appt->appt_id)->first();
            $enddates = '';
            if ($appo->enddate == '0000-00-00') {
                $enddates = date('m/d/Y', strtotime($appo->startdate));
            } else {
                $enddates = date('m/d/Y', strtotime($appo->enddate));
            }
            if (!empty($appo->participant)) {
                $parti = $this->getTeamName($appo->participant);
            } else {
                $parti = '';
            }
            $appo_with = $this->getsingleTeamName($appo->appo_with);

            if (empty($appo->client_id)) {
                $tempRep = "";
            } else {
                $tempRep = $this->getTeamRep2($appo->client_id);//$appo->client_id;
            }
            $appodata[] = array(
                'id' => $appo->id,
                'regarding' => $appo->regard_short,
                'startdate' => date('m/d/Y', strtotime($appo->startdate)),
                'enddate' => $enddates,
                'starttime' => $appo->starttime,
                'endtime' => $appo->endtime,
                'clientId' => $appo->client_id,
                'priority' => $appo->priority,
                'teamRep' => $tempRep,
                'type' => $appo->work_type_id,
                'work_type' => $appo->work_type,
                'appo_with' => $appo_with
            );
        }

        return View('fac-Bhavesh-0554.employee.edit', compact(['appodata', 'emp_access', 'emp_emai_rights', 'whinfo2', 'whinfo', 'reviewinfo', 'accservice1', 'accservice2', 'service1', 'service2', 'notesdata', 'NotesNames', 'listclient', 'listvendoe', 'listemployeeuser', 'relatedNames', 'conversation', 'team', 'holiday', 'language', 'super', 'super1', 'admin_notes', 'rules', 'emp', 'info1', 'info', 'empfsc', 'info2', 'info3', 'review1', 'review', 'position', 'branch', 'employes', 'employes2']));
    }

    function getTeamName($data)
    {
        $datas = explode(',', $data);
        $participant = "";
        for ($i = 0; $i < count($datas); $i++) {
            $employee = DB::table('employees')
                ->select('employees.*', 'teams.team as teamname')
                ->leftjoin('teams', 'teams.id', '=', 'employees.team')
                ->where('employees.employee_id', '=', $datas[$i])->first();

            if ($i == count($datas) - 1) {
                $participant .= $employee->teamname;
            } else {
                $participant .= $employee->teamname . ',';
            }

        }
        return $participant;
    }

    function getsingleTeamName($empid)
    {
        $employee = DB::table('employees')
            ->select('teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.employee_id', '=', $empid)->first();
        return $employee->teamname;
    }

    public function getTeamRep2($id)
    {
        $returnData = '';
        $commonRe = DB::table('commonregisters')->select('id')->where('filename', '=', $id)->first();
        $client_id = $commonRe->id;
        $clientser = DB::table('clientservices')->select('clientservices.employee_id')->where('clientid', '=', $client_id)->orderBy('id', 'asc')->first();
        if (!empty($clientser)) {
            $empid = $clientser->employee_id;
            $employee = DB::table('employees')
                ->select('teams.team as teamname')
                ->leftjoin('teams', 'teams.id', '=', 'employees.team')
                ->where('employees.id', '=', $empid)->first();
            if (!empty($employee)) {
                $returnData = $employee->teamname;
            } else {
                $returnData = '';
            }
        } else {
            $returnData = '';
        }

        return $returnData;
    }

    public function getConversationdataemp(Request $request)
    {
        $documentRow = DB::table('conversation_sheet')->where('id', $request->documentid)->first();
        return response()->json($documentRow);
    }

    public function getNotesemp(Request $request)
    {
        $documentRow = DB::table('notes_sheet')->where('id', $request->documentid)->first();
        return response()->json($documentRow);
    }

    public function updatecoversation(Request $request)
    {
        //print_r($_REQUEST);EXIT;

        $this->validate($request, [
            'type_user' => 'required',
            'conrelatedname' => 'required',
        ]);


        $CovID = $request->CovID;
        $date = $request->date;
        $day = $request->day;
        $time = $request->time;
        $type_user = $request->type_user;
        if ($request->clientid != '') {
            $clientid = $request->clientid;
        } else {
            $clientid = '';
        }

        if ($request->employeeuserid != '') {
            $employeeuserid = $request->employeeuserid;
        } else {
            $employeeuserid = '';
        }

        if ($request->vendorid != '') {
            $vendorid = $request->vendorid;
        } else {
            $vendorid = '';
        }

        if ($request->otherid != '') {
            $otherid = $request->otherid;
        } else {
            $otherid = '';
        }

        $conrelatedname = $request->conrelatedname;
        $condescription = $request->condescription;
        $connotes = $request->connotes;

        $returnValue = DB::table('conversation_sheet')->where('id', $CovID)
            ->update([
                'creattiondate' => $date,
                'day' => $day,
                'time' => $time,
                'type_user' => $type_user,
                'clientid' => $clientid,
                'employeeuserid' => $employeeuserid,
                'vendorid' => $vendorid,
                'otherid' => $otherid,
                'conrelatedname' => $conrelatedname,
                'condescription' => $condescription,
                'connotes' => $connotes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }


    public function updatenotes(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'notetype_user' => 'required',
            'notesrelatedcat' => 'required',
        ]);


        $NoteID = $request->NoteID;
        $notedate = $request->notedate;
        $noteday = $request->noteday;
        $notetime = $request->notetime;
        $notetype_user = $request->notetype_user;
        if ($request->noteclientid != '') {
            $noteclientid = $request->noteclientid;
        } else {
            $noteclientid = '';
        }

        if ($request->noteemployeeuserid != '') {
            $noteemployeeuserid = $request->noteemployeeuserid;
        } else {
            $noteemployeeuserid = '';
        }

        if ($request->notevendorid != '') {
            $notevendorid = $request->notevendorid;
        } else {
            $notevendorid = '';
        }

        if ($request->noteotherid != '') {
            $noteotherid = $request->noteotherid;
        } else {
            $noteotherid = '';
        }

        //$notesrelatedname=$request->notesrelatedname;
        $notesrelatedcat = $request->notesrelatedcat;
        $notes = $request->notes;

        $returnValue = DB::table('notes_sheet')->where('id', $NoteID)
            ->update([
                'creattiondate' => $notedate,
                'noteday' => $noteday,
                'notetime' => $notetime,
                'notetype_user' => $notetype_user,
                'noteclientid' => $noteclientid,
                'noteemployeeuserid' => $noteemployeeuserid,
                'notevendorid' => $notevendorid,
                'noteotherid' => $noteotherid,
                //'notesrelatedname' =>$notesrelatedname,
                'notesrelatedcat' => $notesrelatedcat,
                'notes' => $notes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }

    public function update($id, Request $request)
    {
        // echo $id;exit;
        // echo "<pre>";print_r($_POST);EXIT;
        //echo "<pre>";print_r($_POST);exit;
        //exit;
        //  echo $request->firstName;
        $this->validate($request, []);
        if ($request->check == '0') {
            $status = 0;
            $password1 = '';
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 0, 'name' => $name]);
            DB::table('employees')->where('id', $id)->update(['check' => 0]);
        } else {
            $status = '1';
            $email = $request->email;
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 1, 'name' => $name]);
            $password = $request->password;
            $cc = $request->go;
            $password1 = bcrypt($password);

            $user = Employee::where('id', '=', $id)->first();
            //echo $user->flag;exit;
            //echo "<pre>";print_r($user);exit;
            if ($user->flag == '0') {


                //$insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','1')");
                $data = array('email' => $email, 'firstName' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' => $password, 'type' => $cc);
                \Mail::send('fac-Bhavesh-0554/employe', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['firstName'], $data['password'], $data['type'])->subject('FSC Employee');
                });


            }


        }
        $pay_method = $request->pay_method;

        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee = $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;
        $i = 0;
        $j = 0;
        $k = 0;
        DB::table('employee_pay_info')->where('employee_id', $id)->first();
        DB::table('employee_pay_info')->where('employee_id', $id)->delete();

        if (!empty($pay_scale)) {

            foreach ($pay_scale as $post) {
                $pay_method1 = $pay_method[$i];
                $pay_frequency1 = $pay_frequency[$i];
                $pay_scale1 = $pay_scale[$i];
                $effective_date1 = date('Y-m-d', strtotime($effective_date[$i]));
                $fields1 = $fields[$i];
                $employee1 = $employee[$i];
                $i++;
                //if(empty($employee1))
                //{
                $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) 
        values('" . $id . "','" . $pay_method1 . "','" . $pay_frequency1 . "','" . $post . "','" . $effective_date1 . "','" . $fields1 . "')");

                //}
            }
        }
        $first_rev_day = $request->first_rev_day;
        $reviewmonth = $request->reviewmonth;
        $hiring_comments = $request->hiring_comments;
        $ree = $request->ree;

        DB::table('employee_review')->where('employee_id', $id)->first();
        if ($first_rev_day) {
            foreach ($first_rev_day as $post) {
                $first_rev_day1 = $first_rev_day[$k];
                $reviewmonth1 = $reviewmonth[$k];
                $hiring_comments1 = $hiring_comments[$k];
                $ree1 = $ree[$k];
                $k++;
                DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                if (empty($ree1)) {

                    $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('" . $id . "','" . $post . "','" . $reviewmonth1 . "','" . $hiring_comments1 . "')");
                    //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                } else {
                    //DB::table('employee_review')->where('id', $ree1)->delete();
                    $returnValue = DB::table('employee_review')->where('id', '=', $ree1)
                        ->update(['employee_id' => $id,
                            'first_rev_day' => $first_rev_day1,
                            'reviewmonth' => $reviewmonth1,
                            'hiring_comments' => $hiring_comments1
                        ]);
                    //DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                    //$affectedRows = employee_pay_info::where('first_rev_day', '=', '')->delete();
                }
            }
        }

        //------------------------------//

        $review_date = $request->review_date;
        $reviewday = $request->review_day;
        $next_review_date = $request->next_review_date;
        $review_rate = $request->review_rate;
        $review_comments = $request->review_comments;


        DB::table('emp_review')->where('emp_id', $id)->delete();

        if ($review_date) {
            foreach ($review_date as $post) {
                $review_date1 = date('Y-m-d', strtotime($post));
                $reviewday1 = $reviewday[$k];
                $next_review_date1 = date('Y-m-d', strtotime($next_review_date[$k]));
                $review_rate1 = $review_rate[$k];
                $review_comments1 = $review_comments[$k];

                $k++;

                $insert2 = DB::insert("insert into emp_review(`emp_id`,`review_date`,`review_day`,`next_review_date`,`review_rate`,`review_comments`)
            values('" . $id . "','" . $review_date1 . "','" . $reviewday1 . "','" . $next_review_date1 . "','" . $review_rate1 . "','" . $review_comments1 . "')");
                //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
        }

        //------------------------------//


        //------------------------------//

        $year_wh = $request->year_wh;
        $fillingstatus = $request->fillingstatus;
        $fed_claim = $request->fed_claim;
        $fed_wh = $request->fed_wh;

        $st_claim = $request->st_claim;
        $st_wh = $request->st_wh;

        $local_claim = $request->local_claims;
        $local_wh = $request->local_wh;

        DB::table('emp_wh')->where('emp_id', $id)->delete();
        $k = 0;
        if ($year_wh) {
            foreach ($year_wh as $post) {
                $year_wh1 = $post;
                $fillingstatus1 = $fillingstatus[$k];
                $fed_claim1 = $fed_claim[$k];
                $fed_wh1 = $fed_wh[$k];
                $st_claim1 = $st_claim[$k];
                $st_wh1 = $st_wh[$k];
                $local_claim1 = $local_claim[$k];
                $local_wh1 = $local_wh[$k];

                $k++;

                $insert2 = DB::insert("insert into emp_wh(`emp_id`,`year_wh`,`fillingstatus`,`fed_claim`,`fed_wh`,`st_claim`,`st_wh`,`local_claims`,`local_wh`)
            values('" . $id . "','" . $year_wh1 . "','" . $fillingstatus1 . "','" . $fed_claim1 . "','" . $fed_wh1 . "','" . $st_claim1 . "','" . $st_wh1 . "','" . $local_claim1 . "','" . $local_wh1 . "')");
                //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
        }

        //------------------------------//

        $noteid = $request->noteid;
        $adminnotes = $request->adminnotes;
        $k = 0;
        $type = $request->types;
        $users = DB::table('notes')->where('admin_id', '=', $id)->first();
        foreach ($adminnotes as $notess) {
            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('" . $note1 . "','" . $id . "','" . $type . "')");
            } else {
                $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                    ->update(['notes' => $note1,
                        'admin_id' => $id,
                        'type' => $type
                    ]);
                $users = DB::table('notes')->where('notes', '=', '')->delete();
            }
        }
        $employee = Employee::find($id);
        $employee->employee_id = $request->employee_id;
        $employee->super = $request->super;
        $employee->firstName = $request->firstName;


        if (isset($request->languages)) {
            $employee->languages = implode(',', $request->languages);
        } else {
            $employee->languages = '';
        }
        if (isset($request->holidays)) {
            $employee->holidays = implode(',', $request->holidays);
        } else {
            $employee->holidays = '';
        }
        // print_r($request);
        $employee->type = $request->types;

        $service_1 = $request->emp_service1;
        if (empty($service_1)) {
            $List1 = '';
        } else {
            $List1 = implode(', ', $service_1);
        }

        $service_2 = $request->emp_service2;
        if (empty($service_2)) {
            $List2 = '';
        } else {
            $List2 = implode(', ', $service_2);
        }

        $service_3 = $request->acc_service_1;
        if (empty($service_3)) {
            $List3 = '';
        } else {
            $List3 = implode(', ', $service_3);
        }

        $service_4 = $request->acc_service_2;
        if (empty($service_4)) {
            $List4 = '';
        } else {
            $List4 = implode(', ', $service_4);
        }


        if (strlen($request->access) == 0) {
            $emailSingleAccess = 0;
        } else {
            $emailSingleAccess = 1;
        }
        DB::table('employees')->where('id', $id)->update(array('email_access' => $emailSingleAccess));
        //echo isset($_POST['accessemail']);
        //exit('dfsdf');
        if (Input::has('accessemail')) {
            $emailAccess = $_POST['accessemail'];

            $access = isset($_POST['access2']) ? $_POST['access2'] : [];
            $counts = count($emailAccess);

            DB::table('emp_email_rights')->where('emp_id', $id)->delete();
            //create by ankit patel
            for ($i = 0; $i < $counts; $i++) {
                $val1 = $emailAccess[$i];
                $val2 = 0;
                if (isset($access[$i])) {
                    $val2 = 1;
                } else {
                    $val2 = 0;
                }
                //echo "<br/>".$val2;
                $inserts = DB::insert('insert into emp_email_rights(emp_id,emp_email_id,access) values("' . $id . '","' . $val1 . '","' . $val2 . '")');
            }
            //
            //exit();
        }

        //exit('sdfds');

        $employee->emp_service1 = $List1;
        $employee->emp_service2 = $List2;
        $employee->acc_service_1 = $List3;
        $employee->acc_service_2 = $List4;

        //  $employee->type= $request->type;
        // $employee->read= $request->terms;
        $employee->middleName = $request->middleName;

        $employee->lastName = $request->lastName;
        $employee->team = $request->team;
        $employee->flag = '1';


        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->password = $request->password;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->fscemail = $request->fscemail;
        $employee->categorys = $request->categorys;

        $employee->hiremonth = $request->hiremonth;
        $employee->hireday = $request->hireday;
        $employee->hireyear = $request->hireyear;
        $employee->termimonth = $request->termimonth;
        $employee->termiday = $request->termiday;
        $employee->termiyear = $request->termiyear;
        $employee->tnote = $request->tnote;
        $employee->tnote1 = $request->tnote1;
        $employee->rehiremonth = $request->rehiremonth;
        $employee->rehireday = $request->rehireday;
        $employee->rehireyear = $request->rehireyear;
        $employee->branch_city = $request->branch_city;
        $employee->branch_name = $request->branch_name;
        $employee->position = $request->position;
        $employee->note = $request->note;
        //  $employee->pay_method= $request->pay_method;
        //    $employee->pay_frequency= $request->pay_frequency;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        $employee->pf2 = $request->pf2;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->uname = $request->uname;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        $employee->check = $request->check;
        //   $employee->filling_status= $request->filling_status;
        $employee->fedral_claim = $request->fedral_claim;
        $employee->additional_withholding = $request->additional_withholding;

        if ($request->hasFile('additional_attach')) {
            $attach1 = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/uploads', $attach1);

        } else {
            $attach1 = $request->file_name_2;
        }


        if ($request->hasFile('additional_attach_1')) {

            $attach2 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/uploads', $attach2);
        } else {
            $attach2 = $request->file_name_1;
        }

        if ($request->hasFile('additional_attach_2')) {

            $attach3 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/uploads', $attach3);
        } else {
            $attach3 = $request->file_name;
        }

        if ($request->hasFile('pfid1')) {
            $prfid1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/uploads', $prfid1);

        } else {
            $prfid1 = $request->pfid1_name;
        }


        if ($request->hasFile('pfid2')) {
            $prfid2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/uploads', $prfid2);

        } else {
            $prfid2 = $request->pfid2_name;
        }
        $employee->additional_attach = $attach1;
        $employee->additional_attach_1 = $attach2;
        $employee->additional_attach_2 = $attach3;
        $employee->pfid1 = $prfid1;
        $employee->pfid2 = $prfid2;


        $employee->state_claim = $request->state_claim;
        $employee->additional_withholding_1 = $request->additional_withholding_1;
        // $employee->local_claim= $request->local_claim;
        $employee->additional_withholding_2 = $request->additional_withholding_2;
        $employee->type_agreement = $request->type_agreement;
        $employee->firstName_1 = $request->firstName_1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;
        $employee->resetdate = $request->reset_date;
        $employee->nametype = $request->nametype;
        $employee->eemail = $request->eemail;
        $employee->technical_support = $request->technical_support;
        $employee->timing_support = $request->timing_support;
        $employee->system_support = $request->system_support;
        $employee->other_support = $request->other_support;
        $employee->timesheet = $request->timesheet;

        //Added by ankit patel

        $employee->paid_leave = $request->paid_leave;
        $employee->paid_time = $request->paid_leave_time;
        $employee->sick_leave = $request->sick_leave;
        $employee->sick_time = $request->sick_leave_time;
        $employee->benefit_holiday = $request->holiday_1;
        $employee->benefit_notes = $request->note1;

        //end Update

        $employee->status = $status;


        if ($request->check == '0') {
            $employee->check = $request->check;
        }
        $employee->type = $request->types;
        $employee->update();
        if ($employee->update()) {
            return redirect()->route('employee.edit', $id)->with('success', 'Successfully update Employee');
        } else {
            return redirect()->route('employee.edit', $id)->with('erroe', 'Error');
        }
        //return redirect()->route('employee.edit', $id)->with('success','Successfully update Employee');
    }

    public function removeEmailRights($id, $empid)
    {
        DB::table('emp_email_rights')->where('id', $id)->delete();
        return redirect()->route('employee.edit', $empid)->with('success', 'Successfully delete Employee Email Rights');
    }


    public function getbranch1(Request $request)
    {
        $data = Branch::select('branchname', 'city')->where('city', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function destroyteam(Request $request, $id)
    {
        DB::table('teams')->where('id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/employee/create')->with('success', 'Your Team Successfully Deleted');;
    }


    public function reviewdelete1($id)
    {
        $user = DB::table('employee_review')->where('id', '=', $id)->first();
        $logoStatus = DB::table('employee_review')->where('id', '=', $id)->delete();
        return redirect()->route('employee.edit', $user->employee_id)->with('error', 'Your Record Deleted Successfully');
    }

    public function removeimage($id)
    {
        DB::table('employees')->where('id', '=', $id)->update(['photo' => '']);
        return redirect()->route('employee.edit', $id)->with('success', 'Your Record Updated Successfully');
    }

    public function removeimage1($id)
    {
        DB::table('employees')->where('id', '=', $id)->update(['additional_attach' => '']);
        return redirect()->route('employee.edit', $id)->with('success', 'Your Record Updated Successfully');
    }

    public function removeimage2($id)
    {
        DB::table('employees')->where('id', '=', $id)->update(['additional_attach_1' => '']);
        return redirect()->route('employee.edit', $id)->with('success', 'Your Record Updated Successfully');
    }

    public function removeimage3($id)
    {
        DB::table('employees')->where('id', '=', $id)->update(['additional_attach_2' => '']);
        return redirect()->route('employee.edit', $id)->with('success', 'Your Record Updated Successfully');
    }


    public function paydelete1($id)
    {
        $user = DB::table('employee_pay_info')->where('id', '=', $id)->first();
        $logoStatus = DB::table('employee_pay_info')->where('id', '=', $id)->delete();
        return redirect()->route('employee.edit', $user->employee_id)->with('error', 'Your Record Deleted Successfully');

        //return redirect(route('employee.index'))->with('error','Your Record Deleted Successfully');
    }

    public function empnotedelete($id)
    {
        $users = DB::table('notes')->where('id', $id)->delete();
        return redirect(route('employee.index'))->with('error', 'Your Record Deleted Successfully');
    }

    public function emppaydelete($id)
    {
        $user = DB::table('employee_pay_info')->where('id', '=', $id)->first();
        $users = DB::table('employee_pay_info')->where('id', $id)->delete();
        return redirect()->route('employee.edit', $user->employee_id)->with('error', 'Your Record Deleted Successfully');

    }

    public function empwhdelete($id)
    {
        $user = DB::table('emp_wh')->where('id', '=', $id)->first();
        $users = DB::table('emp_wh')->where('id', $id)->delete();
        return redirect()->route('employee.edit', $user->emp_id)->with('error', 'Your Record Deleted Successfully');

    }

    public function empreviewdelete($id)
    {
        $user = DB::table('emp_review')->where('id', '=', $id)->first();

        $users = DB::table('emp_review')->where('id', $id)->delete();
        return redirect()->route('employee.edit', $user->emp_id)->with('error', 'Your Record Deleted Successfully');

    }


    public function ajaxImage(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload');
        else {
            $validator = Validator::make($request->all(),
                ['file' => 'image',], ['file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)']);
            if ($validator->fails())
                return array('fail' => true, 'errors' => $validator->errors());
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_2' => $filename]);
            return $filename;
        }
    }

    public function deleteImage($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }

    public function ajaxImage1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload1');
        else {
            $validator = Validator::make($request->all(),
                ['file_1' => 'image',], ['file_1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)']);
            if ($validator->fails())
                return array('fail' => true, 'errors' => $validator->errors());
            $extension = $request->file('file_1')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach_1' => $filename]);
            return $filename;
        }
    }

    public function deleteImage1($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }

    public function ajaxImage2(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload2');
        else {
            $validator = Validator::make($request->all(),
                ['file_2' => 'image',], ['file_2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)']);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors());
            $extension = $request->file('file_2')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['additional_attach' => $filename]);
            return $filename;
        }
    }

    public function deleteImage2($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }

    public function pfid_upload1(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload');
        else {
            $validator = Validator::make($request->all(),
                ['pfid1' => 'image',], ['pfid1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)']);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid1')->getClientOriginalExtension();
            $dir = 'public/employeeProof1/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid1')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid1' => $filename]);
            return $filename;
        }
    }

    public function pfid_upload2(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid2' => 'image',
                ],
                [
                    'pfid2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array('fail' => true, 'errors' => $validator->errors());
            $extension = $request->file('pfid2')->getClientOriginalExtension();
            $dir = 'public/employeeProof2/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid2')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['pfid2' => $filename]);
            return $filename;
        }
    }

    public function photo_upload2(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('photo_upload2');
        else {
            $validator = Validator::make($request->all(),
                ['photo' => 'image',], ['photo.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)']);
            if ($validator->fails())
                return array('fail' => true, 'errors' => $validator->errors());
            $extension = $request->file('photo')->getClientOriginalExtension();
            $dir = 'public/employeeimage/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('photo')->move($dir, $filename);
            DB::table('employees')->where('id', '=', $id)->update(['photo' => $filename]);
            return $filename;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ss = Employee::where('id', $id)->first();
        $k = $ss->email;
        ApplyEmployment::where('email', $k)->delete();
        Employee::where('id', $id)->delete();
        Schedule::where('emp_name', $id)->delete();
        Schedule::where('emp_name', $id)->delete();
        DB::table('empschedules')->where('employee_id', $id);
        Msg::where('employeeid', $id)->delete();
        Msg::where('admin_id', $id)->delete();
        Task::where('employeeid', $id)->delete();
        Fscemployee::where('user_id', $id)->delete();
        DB::table('employee_review')->where('employee_id', '=', $id)->first();
        return redirect(route('employee.index'))->with('success', 'Success Fully Delete Record');
    }
}