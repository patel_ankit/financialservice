<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Currency;
use App\Model\Period;
use App\Model\Price;
use App\Model\Taxtitle;
use App\Model\Typeofser;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // exit('111111');
        $price = DB::table('prices')->leftJoin('payroll_service', 'payroll_service.id', '=', 'prices.payrollservice')->leftJoin('currencies', 'prices.currency', '=', 'currencies.id')->leftJoin('period', 'prices.period', '=', 'period.id')
            ->leftJoin('typeofser', 'prices.typeofservice', '=', 'typeofser.id')
            ->select('prices.*', 'payroll_service.id as pid', 'payroll_service.servicename', 'currencies.id', 'currencies.currency', 'currencies.sign', 'period.id', 'typeofser.id', 'period.period', 'typeofser.typeofservice', 'prices.id')->orderby('typeofser.typeofservice', 'asc')->get();
        // print_r($price);

        return view('fac-Bhavesh-0554/price/price', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::All();
        $period = Period::All();
        $taxtitle = Taxtitle::All();

        $payrollservice = DB::table('payroll_service')->get();
        //  print_r($payrollservice);exit;
        return view('fac-Bhavesh-0554/price/create', compact(['payrollservice', 'position', 'currency', 'period', 'typeofser', 'taxtitle']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'currency' => 'required',
        ]);
        $position = new Price;
        $position->currency = $request->currency;
        $position->typeofservice = $request->typeofservice;
        $position->period = $request->serviceperiod;
        $position->serviceincludes = $request->serviceincludes;
        $position->payrollservice = $request->payrollservice;

        $position->regularprice = $request->regularprice;
        $position->comboprice = $request->comboprice;
        $position->note = $request->note;
        $position->save();
        $price_id = $position->id;
        $taxid = $request->taxid;
        $title = $request->titles;
        $serviceincludes1 = $request->serviceincludes1;
        $regularprice1 = $request->regularprice1;
        $comboprice1 = $request->comboprice1;
        $k = 0;
        foreach ($title as $notess) {
            $taxid1 = $taxid[$k];
            $title1 = $title[$k];
            $serviceincludes2 = $serviceincludes1[$k];
            $regularprice2 = $regularprice1[$k];
            $comboprice2 = $comboprice1[$k];
            $k++;
            if (empty($taxid1)) {
                $insert2 = DB::insert("insert into taxationservice(`title`,`regularprice1`,`comboprice1`,`price_id`,`currency`,`typeofservice`,`period`) values('" . $title1 . "','" . $regularprice2 . "','" . $comboprice2 . "','" . $price_id . "','" . $request->currency . "','" . $request->typeofservice . "','" . $request->serviceperiod . "')");
                $users = DB::table('taxationservice')->where('title', '=', '')->delete();
            }
        }
        $price_id = $position->id;
        $priceid = $request->priceid;
        $eecount = $request->eecount;
        $weekly = $request->weekly;
        $biweekly = $request->biweekly;
        $semimonthly = $request->semimonthly;
        $monthly = $request->monthly;
        $p = 0;
        foreach ($eecount as $notess) {
            $priceid1 = $priceid[$p];
            $eecount1 = $eecount[$p];
            $weekly1 = $weekly[$p];
            $biweekly1 = $biweekly[$p];
            $semimonthly1 = $semimonthly[$p];
            $monthly1 = $monthly[$p];
            $p++;
            if (empty($priceid1)) {
                $insert2 = DB::insert("insert into priceemp(`price_id`,`priceid`,`eecount`,`weekly`,`biweekly`,`semimonthly`,`monthly`) values('" . $price_id . "','" . $priceid1 . "','" . $eecount1 . "','" . $weekly1 . "','" . $biweekly1 . "','" . $semimonthly1 . "','" . $monthly1 . "')");
                $users = DB::table('priceemp')->where('eecount', '=', '')->delete();
            }
        }
        return redirect('fac-Bhavesh-0554/price/?online=0')->with('success', 'Success fully add Price');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Price $price
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Price $price
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price = Price::where('id', $id)->first();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::All();
        $period = Period::All();
        $taxtitle = Taxtitle::All();
        $priceemp = DB::table('priceemp')->where('price_id', '=', $id)->get();
        $taxa = DB::table('taxationservice')->where('price_id', '=', $id)->get();
        return View('fac-Bhavesh-0554.price.edit', compact(['price', 'currency', 'period', 'typeofser', 'taxa', 'taxtitle', 'priceemp']));
    }

    public function currencies()
    {
        $newopt = Input::get('newopt');
        $sign = Input::get('sign');
        if (DB::table('currencies')->where('currency', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Currency;
            $position->currency = $newopt;
            $position->sign = $sign;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    public function taxtitle()
    {
        $newopt = Input::get('title');
        if (DB::table('taxtitles')->where('title', '=', $newopt)->exists()) {
        } else {
            $position = new Taxtitle;
            $position->title = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'title' => $newopt
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    public function getsign1(Request $request)
    {
        $users = DB::table('clientservices')->where('clientid', '=', $request->clientid)->delete();
        $users = DB::table('clientservicetitles')->where('clientid', '=', $request->clientid)->delete();
        $data = Currency::select('sign', 'id')->where('id', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function typeofser()
    {
        $newopt = Input::get('newopt1');
        if (DB::table('typeofser')->where('typeofservice', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Typeofser;
            $position->typeofservice = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt1' => $newopt
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    public function period1()
    {
        $newopt = Input::get('newopt2');
        if (DB::table('period')->where('period', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Period;
            $position->period = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt2' => $newopt
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Price $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo "<pre>";print_r($_POST);exit;
        $position = Price::find($id);
        $position->currency = $request->currency;
        $position->typeofservice = $request->typeofservice;
        $position->period = $request->serviceperiod;
        $position->serviceincludes = $request->serviceincludes;
        $position->regularprice = $request->regularprice;
        $position->comboprice = $request->comboprice;
        $position->note = $request->note;
        $position->update();

        $taxid = $request->taxid;
        $title = $request->titles;
        $serviceincludes1 = $request->serviceincludes1;
        $regularprice1 = $request->regularprice1;
        $comboprice1 = $request->comboprice1;
        $onlynote1 = $request->onlynote;

        $k = 0;
        if (empty($title)) {
        } else {
            foreach ($title as $notess) {
                $taxid1 = $taxid[$k];
                $title1 = $title[$k];
                $serviceincludes2 = $serviceincludes1[$k];
                $regularprice2 = $regularprice1[$k];
                $comboprice2 = $comboprice1[$k];
                $onltnote = isset($onlynote1[$k]) ? $onlynote1[$k] : "";

                $k++;
                if (empty($taxid1)) {
                    $insert2 = DB::insert("insert into taxationservice(`onlynote`,`currency`,`typeofservice`,`period`,`title`,`serviceincludes1`,`regularprice1`,`comboprice1`,`price_id`) values('" . $onltnote . "','" . $request->currency . "','" . $request->typeofservice . "','" . $request->serviceperiod . "','" . $title1 . "','" . $serviceincludes2 . "','" . $regularprice2 . "','" . $comboprice2 . "','" . $id . "')");
                } else {
                    $returnValue = DB::table('taxationservice')->where('id', '=', $taxid1)->update([
                        'onlynote' => $onltnote, 'currency' => $request->currency, 'period' => $request->serviceperiod, 'typeofservice' => $request->typeofservice, 'title' => $title1, 'serviceincludes1' => $serviceincludes2, 'regularprice1' => $regularprice2, 'comboprice1' => $comboprice2, 'price_id' => $id
                    ]);
                    $users = DB::table('taxationservice')->where('title', '=', '')->delete();
                }
            }
        }
        $price_id = $position->id;
        $priceid = $request->priceid;
        $eecount = $request->eecount;
        $weekly = $request->weekly;
        $biweekly = $request->biweekly;
        $semimonthly = $request->semimonthly;
        $monthly = $request->monthly;
        $p = 0;
        if (empty($eecount)) {
        } else {
            //$users = DB::table('prices')->where('admin_id', '=', $usid)->first();
            foreach ($eecount as $notess) {
                $priceid1 = $priceid[$p];
                $eecount1 = $eecount[$p];
                $weekly1 = $weekly[$p];
                $biweekly1 = $biweekly[$p];
                $semimonthly1 = $semimonthly[$p];
                $monthly1 = $monthly[$p];
                $p++;
                if (empty($priceid1)) {
                    $insert2 = DB::insert("insert into priceemp(`price_id`,`priceid`,`eecount`,`weekly`,`biweekly`,`semimonthly`,`monthly`) values('" . $price_id . "','" . $priceid1 . "','" . $eecount1 . "','" . $weekly1 . "','" . $biweekly1 . "','" . $semimonthly1 . "','" . $monthly1 . "')");
                    $users = DB::table('priceemp')->where('eecount', '=', '')->delete();
                } else {
                    $returnValue = DB::table('priceemp')->where('id', '=', $priceid1)
                        ->update([
                            'eecount' => $eecount1,
                            'weekly' => $weekly1,
                            'biweekly' => $biweekly1,
                            'semimonthly' => $semimonthly1,
                            'monthly' => $monthly1,
                            'price_id' => $id
                        ]);
                    $users = DB::table('priceemp')->where('eecount', '=', '')->delete();
                }
            }
        }
        return redirect('fac-Bhavesh-0554/price/?online=0')->with('success', 'Success fully Update Price');
    }

    public function taxadelete1($id)
    {
        $users = DB::table('taxationservice')->where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/price/?online=0')->with('error', 'Your Record Deleted Successfully');
    }

    public function checkperiod1(Request $request)
    {
        $data = DB::table('taxationservice')->select('currency', 'id', 'typeofservice', 'period')->where('currency', $request->state)->where('typeofservice', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function ckcurrency1(Request $request)
    {
        $data = DB::table('prices')->select('currency', 'id', 'typeofservice', 'period')->where('currency', $request->id)->where('typeofservice', $request->state)->take(1000)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Price $price
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //  $price->delete();
        Price::where('id', $id)->delete();
        DB::table('taxationservice')->where('price_id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/price/?online=0');
    }

    public function removecurrency2(Request $request)
    {
        $student = Currency::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }


    public function removepriceemp2(Request $request)
    {
        $student = DB::table('priceemp')->where('id', '=', $request->input('id'))->delete();
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removetypeofservice2(Request $request)
    {
        $student = Typeofser::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removeperiod2(Request $request)
    {
        $student = Period::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }
}