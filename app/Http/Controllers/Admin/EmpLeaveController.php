<?php

namespace App\Http\Controllers\Admin;

use App\employee\Leave;
use App\employees\Fscemployee;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class EmpLeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leave = Leave::All();
        $emp = Fscemployee::All();
        return view('fac-Bhavesh-0554/empleave/empleave', compact(['leave', 'emp']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('fac-Bhavesh-0554/empleave/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'total_days' => 'required',

        ]);
        $branch = new Leave;
        $branch->start_date = $request->start_date;
        $branch->total_days = $request->total_days;
        $branch->end_date = $request->end_date;
        $branch->leave_reason = $request->leave_reason;
        $branch->employee_id = $request->employee_id;
        $branch->creation_date = date('Y-m-d');
        $branch->save();
        return redirect('fac-Bhavesh-0554/empleave')->with('success', 'Leave Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();

        $leave = Leave::where('id', $id)->first();
        return view('fac-Bhavesh-0554/empleave.edit', compact(['leave']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [

        ]);
        $branch = Leave::find($id);
        $branch->status = $request->status;
        $branch->update();
        return redirect('fac-Bhavesh-0554/empleave')->with('success', 'Leave Updated Successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect(route('fac-Bhavesh-0554/empleave'));

    }
}