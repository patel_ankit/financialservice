<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class StatetaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $bankdata = DB::table('statetax')->get();
        return view('fac-Bhavesh-0554.statetax.statetax', compact(['bankdata']));
    }


    public function create(Request $request)
    {
        $datastate = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        return view('fac-Bhavesh-0554.statetax.create', compact(['datastate']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r($_REQUEST);die;    
        $this->validate($request, [
            'statename' => 'required',
            'taxform' => 'required',

        ]);

        $statename = $request->statename;
        $taxform = $request->taxform;


        $returnValue = DB::table('statetax')
            ->insert([
                'statename' => $statename,
                'taxform' => $taxform,
            ]);

        return redirect(route('statetax.index'))->with('success', 'Record Added Seccessfully');
    }


    public function edit($id)
    {
        $datastate = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $bankdata = DB::table('statetax')->where('id', '=', $id)->first();
        return View('fac-Bhavesh-0554.statetax.edit', compact(['bankdata', 'datastate']));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'statename' => 'required',
            'taxform' => 'required',
        ]);

        $statename = $request->statename;
        $taxform = $request->taxform;

        $returnValue = DB::table('statetax')->where('id', '=', $id)
            ->update([
                'statename' => $statename,
                'taxform' => $taxform,
            ]);
        return redirect(route('statetax.index'))->with('success', 'Record Updated Seccessfully ');
    }


    public function destroy($id)
    {
        DB::table('statetax')->where('id', $id)->delete();
        return redirect(route('statetax.index'))->with('success', 'Record Deleted Seccessfully ');
    }
}