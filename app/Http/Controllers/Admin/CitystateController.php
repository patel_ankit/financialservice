<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Citystate;
use App\Model\Zipcode;
use Illuminate\Http\Request;

class CitystateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $business = Citystate::paginate(10);
        $zip = Zipcode::All();
        return view('fac-Bhavesh-0554.citystate.citystate', compact(['business', 'zip']))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zip = Zipcode::All();
        return view('fac-Bhavesh-0554.citystate.create', compact(['zip']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'zipcode' => 'required|unique:citystates,zipcode',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
        ]);
        $business = new Citystate;
        $business->zipcode = $request->zipcode;
        $business->country = $request->country;
        $business->state = $request->state;
        $business->city = $request->city;
        $business->save();
        return redirect(route('citystate.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zip = Zipcode::All();
        $city = Citystate::where('id', $id)->first();
        return View('fac-Bhavesh-0554.citystate.edit', compact(['city', 'zip']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'zipcode' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
        ]);
        $business = Citystate::find($id);
        $business->zipcode = $request->zipcode;
        $business->country = $request->country;
        $business->state = $request->state;
        $business->city = $request->city;
        $business->update();
        return redirect(route('citystate.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Citystate::where('id', $id)->delete();
        return redirect(route('citystate.index'));
    }
}