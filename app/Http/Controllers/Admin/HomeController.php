<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\Home;
use App\User;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business = Business::All();
        $category = Category::All();
        $businessbrand = BusinessBrand::All();
        $commonregister = Commonregister::All();
        $cetegorybusiness = Categorybusiness::All();
        $user = Fscemployee::All();
        $user_id = Auth::user()->user_id;
        $user = User::All();

        $employee = Empschedule::where('employee_id', '=', $user_id)->orderBy('id', 'desc')->get();
        //  print_r($employee);
        $common1 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->groupBy('user_type')->get()->All();
        return view('admin.home', compact(['business', 'category', 'businessbrand', 'commonregister', 'cetegorybusiness', 'user', 'common1', 'employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Home $home
     * @return \Illuminate\Http\Response
     */
    public function show(Home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Home $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Home $home)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Home $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Home $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home $home)
    {
        //
    }
}