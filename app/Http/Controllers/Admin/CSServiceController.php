<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\CsService;
use Illuminate\Http\Request;

class CSServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = CsService::All();
        return view('fac-Bhavesh-0554/cservice/cservice', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = CsService::All();
        return view('fac-Bhavesh-0554/cservice/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service' => 'required',
            'billing' => 'required',

        ]);
        $position = new CsService;
        $position->service = $request->service;
        $position->billing = $request->billing;
        $position->save();
        return redirect('fac-Bhavesh-0554/cservice')->with('success', 'Success fully add Service');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\CsService $csService
     * @return \Illuminate\Http\Response
     */
    public function show(CsService $csService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\CsService $csService
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $csService = CsService::where('id', $id)->first();
        return View('fac-Bhavesh-0554.cservice.edit', compact('csService'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\CsService $csService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'service' => 'required',
            'billing' => 'required',

        ]);
        $position = csService::find($id);
        $position->service = $request->service;
        $position->billing = $request->billing;
        $position->update();
        return redirect('fac-Bhavesh-0554/cservice')->with('success', 'Success fully update Service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\CsService $csService
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        csService::where('id', $id)->delete();
        return redirect(route('cservice.index'));
    }
}