<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $position = DB::table('license')->get();
        return view('fac-Bhavesh-0554/license/license', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/license/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'licensename' => 'required'

        ]);

        $licensename = $request->licensename;
        $returnValue = DB::table('license')
            ->insert([
                'licensename' => $licensename

            ]);
        return redirect('fac-Bhavesh-0554/license/')->with('success', 'Data Added Successfully');
    }


    public function edit($id)
    {

        $position = DB::table('license')->where('id', $id)->first();
        return view('fac-Bhavesh-0554.license.edit', compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'licensename' => 'required',
        ]);

        $id = $request->id;
        $licensename = $request->licensename;
        $returnValue = DB::table('license')->where('id', '=', $id)
            ->update([
                'licensename' => $licensename

            ]);
        return redirect('fac-Bhavesh-0554/license')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('license')->where('id', $id)->delete();
        return redirect(route('license.index'))->with('success', 'Data Deleted Successfully');;
    }
}