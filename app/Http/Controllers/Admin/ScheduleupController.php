<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Schedule;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use View;

class ScheduleupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scheduledate = Schedule::get();
        foreach ($scheduledate as $a) {
            $sch = DB::table('schedule_emp_dates')->where('emp_sch_id', $a->id)->orderBy('id', 'DESC')->get();
            $duration = $a->duration;
            $date_from = $a->sch_end_date;
            $currdate = date('m/d/Y');
            $date_from1 = strtotime($date_from);
            $cdate = strtotime($currdate);
            if ($cdate > $date_from1) {
                if ($duration == 'Bi-Weekly') {
                    //DB::table('schedule_emp_dates')->where('emp_sch_id', $a->id)->delete();
                    $date = date("m/d/Y");
                    $tdate = date("Y-m-d");

                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+13 day", strtotime($date)));
                    $date3 = date("l", strtotime("+13 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+17 day", strtotime($date)));
                    $date5 = date("l", strtotime("+17 day", strtotime($date)));
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Bi-Weekly')
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`,`date`) values('" . $a->id . "','" . $i . "','','','1','" . $tdate . "')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`,`date`) values('" . $a->id . "','" . $i . "','','','1','" . $tdate . "')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`date`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "','" . $tdate . "')");
                        }
                    }
                }
                if ($a->duration == 'Weekly') {
                    $date = date("m/d/Y");
                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+6 day", strtotime($date)));
                    $date3 = date("l", strtotime("+6 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+10 day", strtotime($date)));
                    $date5 = date("l", strtotime("+10 day", strtotime($date)));
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Weekly')
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                } else if ($a->duration == 'Monthly') {
                    $month = date("M");
                    if ($month == 'Jan' || $month == 'Mar' || $month == 'May' || $month == 'Jul' || $month == 'Aug' || $month == 'Oct' || $month == 'Dec') {
                        $totaldays = 30;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+30 day", strtotime($date)));
                        $date3 = date("l", strtotime("+30 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+35 day", strtotime($date)));
                        $date5 = date("l", strtotime("+35 day", strtotime($date)));
                    } else if ($month == 'Feb') {
                        $totaldays = 27;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+27 day", strtotime($date)));
                        $date3 = date("l", strtotime("+27 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+27 day", strtotime($date)));
                        $date5 = date("l", strtotime("+27 day", strtotime($date)));
                    } else if ($month == 'Apr' || $month == 'Jun' || $month == 'Sep' || $month == 'Nov') {
                        $totaldays = 29;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+29 day", strtotime($date)));
                        $date3 = date("l", strtotime("+29 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+29 day", strtotime($date)));
                        $date5 = date("l", strtotime("+29 day", strtotime($date)));
                    }

                    $up = DB::table('schedulesetups')->where('duration', '=', 'Monthly')
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update(['sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                } else if ($a->duration == 'Semi-Monthly') {
                    $date = date("m/d/Y");
                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+14 day", strtotime($date)));
                    $date3 = date("l", strtotime("+14 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+18 day", strtotime($date)));
                    $date5 = date("l", strtotime("+18 day", strtotime($date)));
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Semi-Monthly')
                        ->update(['sch_start_date' => $date, 'sch_end_date' => $date1, 'sch_end_day' => $date3, 'sch_start_day' => $date2, 'sch_pay_date' => $date4, 'sch_pay_day' => $date5,]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update(['sch_start_date' => $date, 'sch_end_date' => $date1, 'sch_start_day' => $date2, 'sch_end_day' => $date3,]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $a->id . "','" . $i . "','','','1')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                }
            }
        }
    }
}