<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Position;
use DB;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $position = Position::All();
        $branch = Branch::All();
        /*$branch = DB::select("select branches.id as cid,branches.country as country,branches.city as city,branches.branchname as branchname,positions.position,positions.id as bid from branches left join positions on positions.id=branches.positionid");*/
        return view('fac-Bhavesh-0554/branch/branch', compact(['branch', 'position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Position::All();
        $branch = Branch::All();
        return view('fac-Bhavesh-0554/branch/create', compact(['position', 'branch']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'branch' => 'required',
            'branchtype' => 'required',
        ]);
        $branch = new Branch;
        $branch->positionid = $request->branchtype;
        $branch->branchname = $request->branch;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->save();
        return redirect('fac-Bhavesh-0554/branch')->with('success', 'Success fully add Branch');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::where('id', $id)->first();
        $position = Position::All();
        return view('fac-Bhavesh-0554.branch.edit', compact(['branch', 'position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'branch' => 'required',
            'branchtype' => 'required',
        ]);
        $branch = Branch::find($id);
        $branch->positionid = $request->branchtype;
        $branch->branchname = $request->branch;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->update();
        return redirect('fac-Bhavesh-0554/branch')->with('success', 'Success fully Update Branch');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect(route('branch.index'));

    }
}