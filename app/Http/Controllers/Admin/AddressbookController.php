<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Position;
use DB;
use Illuminate\Http\Request;

class AddressbookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $silver = DB::table("commonregisters")
            ->select("commonregisters.first_name", "commonregisters.middle_name", "commonregisters.last_name", "commonregisters.address", "commonregisters.city", "commonregisters.stateId", "commonregisters.countryId"
                , "commonregisters.business_no", "commonregisters.email", "commonregisters.status", "commonregisters.id")->where('commonregisters.status', '=', "Active");
        $gold = DB::table("employees")->select("employees.firstName", "employees.middleName", "employees.lastName", "employees.address1", "employees.city", "employees.stateId", "employees.countryId"
            , "employees.telephoneNo1", "employees.email", "employees.type", "employees.id")->where('employees.check', '=', "1")->where('employees.employee_id', '!=', "")->unionAll($silver)->get();
        return view('fac-Bhavesh-0554/addressbook/addressbook', compact(['gold']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Position::All();
        $branch = Branch::All();
        return view('fac-Bhavesh-0554/addressbook/create', compact(['position', 'branch']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'branch' => 'required',
            'branchtype' => 'required',
        ]);
        $branch = new Branch;
        $branch->positionid = $request->branchtype;
        $branch->branchname = $request->branch;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->save();
        return redirect('fac-Bhavesh-0554/addressbook')->with('success', 'Added Branch Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $type)
    {
        //$branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();

        $silver = DB::table("commonregisters")
            ->select("commonregisters.first_name", "commonregisters.middle_name", "commonregisters.last_name", "commonregisters.address", "commonregisters.city", "commonregisters.stateId", "commonregisters.countryId"
                , "commonregisters.business_no", "commonregisters.email", "commonregisters.status", "commonregisters.id")->where('id', $id);
        $gold = DB::table("employees")->select("employees.firstName", "employees.middleName", "employees.lastName", "employees.address1", "employees.city", "employees.stateId", "employees.countryId"
            , "employees.telephoneNo1", "employees.email", "employees.type", "employees.id")->where('id', $id)->where('type', $type)->unionAll($silver)->first();

        return view('fac-Bhavesh-0554.addressbook.edit', compact(['gold']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'branch' => 'required',
            'branchtype' => 'required',
        ]);
        $branch = Branch::find($id);
        $branch->positionid = $request->branchtype;
        $branch->branchname = $request->branch;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->update();
        return redirect('fac-Bhavesh-0554/addressbook')->with('success', 'Update Branch Successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect(route('addressbook.index'));

    }
}