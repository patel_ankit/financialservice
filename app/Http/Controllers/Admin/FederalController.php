<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Federal;
use Illuminate\Http\Request;

class FederalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = Federal::All();
        return view('fac-Bhavesh-0554/federal/federal', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Federal::All();
        return view('fac-Bhavesh-0554/federal/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'authority_name' => 'required',
            'short_name' => 'required',
            'type_of_tax' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'website_link_name' => 'required',
            'website_link' => 'required',
            'website' => 'required',
        ]);
        $position = new Federal;
        $position->authority_name = $request->authority_name;
        $position->short_name = $request->short_name;
        $position->type_of_tax = $request->type_of_tax;
        $position->telephone = $request->telephone;
        $position->address = $request->address;
        $position->city = $request->city;
        $position->state = $request->state;
        $position->zip = $request->zip;

        $position->due_date = $request->due_date;
        $position->type_of_form = $request->type_of_form;
        $position->website = $request->website;
        $position->website_link = $request->website_link;
        $position->website_link_name = $request->website_link_name;
        $position->save();
        return redirect('fac-Bhavesh-0554/federal')->with('success', 'Success fully add Federal');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function show(Federal $federal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function edit(Federal $federal)
    {
        return View('fac-Bhavesh-0554.federal.edit', compact('federal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Federal $federal)
    {

        $this->validate($request, [
            'authority_name' => 'required',
            'short_name' => 'required',
            'type_of_tax' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'website_link_name' => 'required',
            'website_link' => 'required',
            'website' => 'required',
        ]);
        $position = $federal;
        $position->authority_name = $request->authority_name;
        $position->short_name = $request->short_name;
        $position->type_of_tax = $request->type_of_tax;
        $position->telephone = $request->telephone;
        $position->address = $request->address;
        $position->city = $request->city;
        $position->state = $request->state;
        $position->zip = $request->zip;
        $position->due_date = $request->due_date;
        $position->type_of_form = $request->type_of_form;
        $position->website = $request->website;
        $position->website_link = $request->website_link;
        $position->website_link_name = $request->website_link_name;
        $position->update();
        return redirect('fac-Bhavesh-0554/federal')->with('success', 'Success fully update Federal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Federal $federal)
    {
        $federal->delete();
        return redirect(route('federal.index'));
    }
}