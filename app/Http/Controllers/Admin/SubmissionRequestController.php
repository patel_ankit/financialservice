<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Submission;
use App\Model\Super;
use DB;
use Illuminate\Http\Request;

class SubmissionRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submission = Submission::All();


        return view('fac-Bhavesh-0554/submissionrequest/submissionrequest', compact(['submission']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Employee::where('super', '=', '1')->orderby('firstName', 'asc')->get();
        return view('fac-Bhavesh-0554/supervisor/create', compact(['emp']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'supervisorcode' => 'required',
            'supervisorname' => 'required',
        ]);
        $position = new Super;
        $position->username = $request->supervisorname;
        $position->code = $request->supervisorcode;
        $position->save();
        return redirect('fac-Bhavesh-0554/supervisor')->with('success', 'Supervisor Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Super::where('id', $id)->first();
        $emp = Employee::where('super', '=', '1')->orderby('firstName', 'asc')->get();
        return View('fac-Bhavesh-0554.supervisor.edit', compact(['task', 'emp']));
    }


    public function edit1($id)
    {

        $submitform = DB::table('submitform')->select('submitform.id as sid', 'submitform.submit_id', 'submitform.name', 'submitform.telephone', 'submitform.email',
            'submitform.requestform', 'submitform.note', 'submissions.id', 'submissions.submission_name')->leftJoin('submissions', function ($join) {
            $join->on('submissions.id', '=', 'submitform.submit_id');
        })
            ->where('submitform.submit_id', '=', $id)->first();


        return view('fac-Bhavesh-0554/submissionrequest/edit', compact(['submitform']));
    }


    public function update(Request $request, $id)
    {

        //print_r($_REQUEST);die;
        $name = $request->name;
        $telephone = $request->telephone;
        $email = $request->email;
        $requestform = $request->requestform;
        $note = $request->note;


        $returnValue = DB::table('submitform')->where('id', '=', $id)
            ->update([
                'name' => $name,
                'telephone' => $telephone,
                'email' => $email,
                'requestform' => $requestform,
                'note' => $note,

            ]);

        return redirect()->back()->with('success', 'Record Updated Successfully');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $position = Super::find($id);
    //   $position->username= $request->supervisorname;
    //     $position->code= $request->supervisorcode;
    //     $position->update();
    //     return redirect('fac-Bhavesh-0554/supervisor')->with('success','Supervisor Update Successfully');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Super::where('id', $id)->delete();
        return redirect(route('supervisor.index'));
    }

    public function destroy1(Request $request, $id)
    {

    }
}