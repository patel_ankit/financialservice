<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Question;
use App\Model\QuestionSection;
use DB;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = Question::All();
        return view('fac-Bhavesh-0554/question/question', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questionsection = QuestionSection::All();
        $position = Question::All();
        return view('fac-Bhavesh-0554/question/create', compact(['position', 'questionsection']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',

        ]);
        $position = new Question;
        $position->question = $request->question;
        $position->type = $request->type;
        $position->save();
        return redirect('fac-Bhavesh-0554/question')->with('success', 'Success fully add Question');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Question::where('id', $id)->first();
        return view('fac-Bhavesh-0554.question.edit', compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'question' => 'required',
        ]);
        $position = Question::find($id);
        $position->question = $request->question;
        $position->type = $request->type;
        $position->update();
        return redirect('fac-Bhavesh-0554/question')->with('success', 'Success fully update question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::where('id', $id)->delete();
        return redirect(route('question.index'));
    }
}