<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\admin_professional;
use App\Model\admin_shareholder;
use App\Model\Adminupload;
use App\Model\Control;
use App\Model\FilingFrequency;
use App\Model\PaymentFrequency;
use App\Model\taxfederal;
use App\Model\taxstate;
use App\Model\taxstatesses;
use Auth;
use DB;
use Illuminate\Http\Request;

class AdminProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formation = DB::table('admin_formation')->where('admin_id', '=', 1)->first();
        $buslicense = DB::table('admin_license')->select('*')->where('admin_id', '=', 1)->orderBy('id', 'DESC')->first();
        //print_r($buslicense);exit;
        $buslicense1 = DB::table('admin_license')->select('*')->where('admin_id', '=', 1)->orderBy('id', 'DESC')->get();

        $bankdata = DB::table('admin_bank')->select('*')->where('admin_id', '=', 1)->get();
        $carddata = DB::table('admin_credit_information')->select('*')->where('admin_id', '=', 1)->get();

        $bankmaster = DB::table('bankmaster')->get();
        //print_r($bankmaster);die;
        $ttt = Auth::user()->typeofservice;
        $entity = DB::table('typeofentity')->orderBy('typeentity', 'asc')->get();
        $history = DB::table('adminupload_history')->get();
        $total = admin_shareholder::sum('agent_per');
        $taxstate = taxstate::All();
        $control = Control::All();
        $upload = Adminupload::All();
        $upload1 = Adminupload::where('upload_name', '=', 'SOS Certificate')->first();
        $admin_notes = DB::table('notes')->where('admin_id', '=', '1')->where('type', '=', 'admin')->get();
        $admin_shareholder = admin_shareholder::orderBy('agent_position', 'ASC')->get();
        $agent = admin_shareholder::where('agent_position', '=', 'Agent')->get()->first();
        $ceo = admin_shareholder::where('agent_position', '=', 'CEO')->get()->first();
        $cfo = admin_shareholder::where('agent_position', '=', 'CFO')->get()->first();
        $sece = admin_shareholder::where('agent_position', '=', 'Secretary')->get()->first();
        $sec = admin_shareholder::where('agent_position', '=', 'Sec')->get()->first();
        // $admin_professional = admin_professional::get();


        $admin_professional = DB::table('admin_professionals as t1')->select('t1.*', 't2.id as ids', 't2.admin_id', 't2.admin_lic_id', 't2.pro_license_year', 't2.pro_license_fee', 't2.pro_license_copy', 't2.status', 't2.pro_license_note', 't2.pro_license_website')
            ->leftJoin('admin_pro_lic as t2', function ($join) {
                $join->on('t1.id', '=', 't2.admin_lic_id');
            })
            ->orderBy('t1.id', 'ASC')->get();
        // echo "<pre>";
        // print_r($admin_professional);die;

        $payroll = taxfederal::where('short_name', '=', 'Payroll')->orderBy('short_name', 'asc')->first();
        $taxfederal = taxfederal::where('authority_name', '=', $ttt)->orderBy('short_name', 'asc')->first();
        if (empty($payroll->uniques)) {
            $pay = PaymentFrequency::where('type', '=', '')->get()->first();
            $fill = FilingFrequency::where('type', '=', '')->get()->first();
        } else {
            $pay = PaymentFrequency::where('type', '=', $payroll->uniques)->get()->first();
            $fill = FilingFrequency::where('type', '=', $payroll->uniques)->get()->first();
        }
        return view('fac-Bhavesh-0554.adminprofile', compact(['buslicense1', 'bankmaster', 'carddata', 'bankdata', 'buslicense', 'formation', 'taxfederal', 'entity', 'upload1', 'admin_notes', 'fill', 'pay', 'ceo', 'cfo', 'sece', 'sec', 'admin_shareholder', 'total', 'control', 'upload', 'admin_professional', 'taxstate', 'history', 'payroll', 'agent']));
    }


    public function getfilename(Request $request)
    {

        $bb = explode('-', $request->filename);

        $countyRow = DB::table('admin_professionals')->where('profession', '=', $bb[0])->where('profession_state', '=', $bb[1])->where('professiontype', '=', $bb[2])->get();
        $wordCount = $countyRow->count();
        return response()->json($wordCount);
    }

    public function getfilenameprofession(Request $request)
    {

        $bb = explode('-', $request->filename);

        $countyRow = DB::table('admin_professionals')->where('profession', '=', $bb[0])->where('profession_state', '=', $bb[1])->where('professiontype', '=', $bb[2])->get();
        $wordCount = $countyRow->count();
        return response()->json($wordCount);
    }

    public function edit($id)
    {
        $admin = Admin::where('id', $id)->first();
        return View('fac-Bhavesh-0554.adminprofile', compact('admin'));
    }


    public function update(Request $request, $id)
    {

        // echo "<pre>";
        //print_r($_REQUEST);die;
        //  echo $_REQUEST['address1'];exit;
        if ($request->hasFile('soscertificate')) {
            $filname = $_FILES['soscertificate']['name'];
            $request->soscertificate->move('public/adminupload', $filname);
        } else {
            $filname = $request->soscertificate1;
        }

        if ($request->hasFile('sosaoi')) {
            $filname1 = $_FILES['sosaoi']['name'];
            $request->sosaoi->move('public/adminupload', $filname1);
        } else {
            $filname1 = $request->sosaoi1;
        }

        /* $this->validate($request,[
             'dba_name'=>'required',
             'address1'=>'required',
             'company_city'=>'required',
             'company_state'=>'required',
             'company_zip'=>'required',
             'company_name'=>'required',
             'company_email'=>'required',
             'physical_1'=>'required',
             'physical_county'=>'required',
             'physical_city'=>'required',
             'physical_state'=>'required',
             'physical_zip'=>'required',

             // 'formation_yearbox'=>'required',
             // 'formation_yearvalue'=>'required',
             // 'formation_amount'=>'required',
             // 'formation_payment'=>'required',
             // 'record_status'=>'required',
             // 'annualreceipt'=>'required',
             // 'formation_work_officer'=>'required',
         ]);

        */


        $federal_frequency = $request->federal_frequency;
        $federal_frequency1 = $request->federal_payment_frequency;
        $federal_frequency2 = $request->frequency;
        $federal_frequency3 = $request->payment_frequency;
        $federal_frequency4 = $request->frequency_type_form_1;
        $federal_frequency5 = $request->payment_frequency_1;
        $federal_frequency6 = $request->quaterly;
        $federal_frequency7 = $request->payment_frequency_2;


        $business = Admin::find($id);
        if ($federal_frequency6 == 'Yearly') {
            $business->frequency_due_date_year = $request->quaterly_due_date;
        } else if ($federal_frequency6 == 'Monthly') {
            $business->quaterly_monthly = $request->quaterly_due_date;
        } else if ($federal_frequency6 == 'Quaterly') {
            $business->quaterly_quaterly = $request->quaterly_due_date;
        } else {

        }

        if ($federal_frequency7 == 'Yearly') {
            $business->frequency_due_date_year = $request->payment_frequency_date_2;
        } else if ($federal_frequency7 == 'Monthly') {
            $business->payment_quaterly_monthly = $request->payment_frequency_date_2;
        } else if ($federal_frequency7 == 'Quaterly') {
            $business->payment_quaterly_quaterly = $request->payment_frequency_date_2;
        } else {

        }

        if ($federal_frequency2 == 'Yearly') {
            $business->frequency_due_date_year = $request->frequency_due_date;
        } else if ($federal_frequency2 == 'Monthly') {
            $business->frequency_due_date_monthly = $request->frequency_due_date;
        } else if ($federal_frequency2 == 'Quaterly') {
            $business->frequency_due_date_quaterly = $request->frequency_due_date;
        } else {

        }

        if ($federal_frequency4 == 'Yearly') {
            $business->frequency_due_date_year_1 = $request->frequency_due_date_1;
        } else if ($federal_frequency4 == 'Monthly') {
            $business->frequency_due_date_monthly_1 = $request->frequency_due_date_1;
        } else if ($federal_frequency4 == 'Quaterly') {
            $business->frequency_due_date_quaterly_1 = $request->frequency_due_date_1;
        } else {

        }

        if ($federal_frequency5 == 'Yearly') {
            $business->frequency_due_date_year_2 = $request->payment_frequency_date_1;
        } else if ($federal_frequency5 == 'Monthly') {
            $business->frequency_due_date_monthly_2 = $request->payment_frequency_date_1;
        } else if ($federal_frequency5 == 'Quaterly') {
            $business->frequency_due_date_quaterly_2 = $request->payment_frequency_date_1;
        } else {

        }

        if ($federal_frequency3 == 'Yearly') {
            $business->payment_frequency_year = $request->payment_frequency_date;
        } else if ($federal_frequency3 == 'Monthly') {
            $business->payment_frequency_monthly = $request->payment_frequency_date;
        } else if ($federal_frequency3 == 'Quaterly') {
            $business->payment_frequency_quaterly = $request->payment_frequency_date;
        } else {
        }

        if ($federal_frequency1 == 'Yearly') {
            $business->federal_payment_frequency_year = $request->federal_payment_frequency_date;
        } else if ($federal_frequency1 == 'Monthly') {
            $business->federal_payment_frequency_month = $request->federal_payment_frequency_date;
        } else if ($federal_frequency1 == 'Quaterly') {
            $business->federal_payment_frequency_quaterly = $request->federal_payment_frequency_date;
        } else {

        }

        if ($federal_frequency == 'Yearly') {
            $business->federal_frequency_due_year = $request->federal_frequency_due_date;
        } else if ($federal_frequency == 'Monthly') {
            $business->federal_frequency_due_monthly = $request->federal_frequency_due_date;
        } else if ($federal_frequency == 'Quaterly') {
            $business->federal_frequency_due_quaterly = $request->federal_frequency_due_date;
        } else {

        }
        $business->ga_dept = $request->ga_dept;
        $business->agent_expiredate = $request->agent_expiredate;
        $business->formation = $request->formation;
        $business->lic_required = $request->lic_required;


        $business->form_number_4 = $request->form_number_4;
        $business->form_number_3 = $request->form_number_3;
        $business->form_number_2 = $request->form_number_2;
        $business->form_number_1 = $request->form_number_1;
        $business->form_authority = $request->form_authority;
        $business->frequency = $request->frequency;
        $business->fedral_state_1 = $request->fedral_state_1;
        $business->typeofcorp_effect_2 = $request->typeofcorp_effect_2;
        $business->ga_dept_1 = $request->ga_dept_1;
        $business->withholding_authority = $request->withholding_authority;
        $business->form_authority_1 = $request->form_authority_1;
        $business->frequency_type_form_1 = $request->frequency_type_form_1;
        $business->frequency_due_date_1 = $request->frequency_due_date_1;
        $business->frequency_due_date = $request->frequency_due_date;
        $business->quaterly_due_date = $request->quaterly_due_date;
        $business->quaterly = $request->quaterly;
        $business->formauthority = $request->formauthority;
        $business->ga_dept_labour = $request->ga_dept_labour;
        $business->fedral_state_2 = $request->fedral_state_2;
        $business->unemployent = $request->unemployent;
        $business->due_date1 = $request->due_date1;
        $business->due_date2 = $request->due_date2;
        $business->due_date_2 = $request->due_date_2;

        $business->due_date3 = $request->due_date3;
        $business->type_form1 = $request->type_form1;
        $business->physical_1 = $request->physical_1;
        $business->physical_2 = $request->physical_2;
        $business->physical_city = $request->physical_city;
        $business->physical_state = $request->physical_state;
        $business->physical_zip = $request->physical_zip;
        $business->business_license3 = $request->business_license3;
        $business->business_license2 = $request->business_license2;
        $business->business_license1 = $request->business_license1;
        $business->business_license = $request->business_license;
        $business->email = $request->email;
        $business->typeofcorp_effect = $request->typeofcorp_effect;
        $business->email_1 = $request->email_1;
        $business->mobile = $request->mobile;
        $business->typeofservice = $request->typeofservice;
        $business->typeofcorp = $request->typeofcorp;
        $business->question1 = $request->question1;
        $business->question2 = $request->question2;
        $business->question3 = $request->question3;
        $business->answer1 = $request->answer1;
        $business->answer2 = $request->answer2;
        $business->answer3 = $request->answer3;
        $business->address = $request->address1;
        $business->address1 = $request->address2;
        $business->fax = $request->fax;
        $business->physical_county = $request->physical_county;
        $business->physical_county_no = $request->physical_county_no;
        $business->fname = $request->firstname;
        $business->mname = $request->middlename;
        $business->lname = $request->lastname;
        $business->city = $request->city;
        $business->state = $request->state;
        $business->zip = $request->zip;
        $business->notes = $request->notes;
        $business->company_name = $request->company_name;
        //$business->contact_person_name = $request->contact_person_name;
        $business->dba_name = $request->dba_name;
        $business->telephone = $request->telephone;
        $business->website = $request->website;
        $business->company_email = $request->company_email;
        $business->contact_address1 = $request->contact_address1;
        $business->contact_address2 = $request->contact_address2;
        $business->contact_fax = $request->contact_fax;
        $business->mobiletype = $request->mobiletype;
        $business->company_city = $request->company_city;
        $business->company_state = $request->company_state;
        $business->company_zip = $request->company_zip;
        $business->company_mobile = $request->company_mobile;
        $business->telephoneNo1Type = $request->telephoneNo1Type;
        $business->ext1 = $request->ext1;
        $business->ext2 = $request->ext2;
        $business->mobile1 = $request->mobile1;
        $business->mobiletype1 = $request->mobiletype1;
        $business->ext21 = $request->ext21;
        $business->minss = $request->minss;
        $business->state_of_formation = $request->state_of_formation;
        $business->legal_name = $request->legal_name;
        $business->contact_number = $request->contact_number;
        $business->agent_fname = $request->agent_fname;
        $business->agent_mname = $request->agent_mname;
        $business->agent_lname = $request->agent_lname;
        $business->total = $request->total;
        $business->federal_id = $request->federal_id;
        $business->type_form = $request->type_form;
        $business->due_date = $request->due_date;
        $business->fedral_state = $request->fedral_state;
        $business->authority = $request->authority;
        $business->taxpayer_id = $request->taxpayer_id;
        $business->taxpayer_id1 = $request->taxpayer_id1;
        $business->state_withholding = $request->state_withholding;
        $business->state_withholding1 = $request->state_withholding1;
        $business->state_unemployment = $request->state_unemployment;
        $business->state_unemployment1 = $request->state_unemployment1;
        $business->business_license_jurisdiction = $request->business_license_jurisdiction;
        $business->agent_position1 = $request->agent_position1;
        $business->typeofcorp1 = $request->typeofcorp1;
        $business->ex_date = $request->ex_date;
        $business->state_id = $request->state_id;
        $business->number_1 = $request->number_1;
        $business->number_2 = $request->number_2;
        $business->number_3 = $request->number_3;
        $business->number_4 = $request->number_4;
        $business->form_1 = $request->form_1;
        $business->form_2 = $request->form_2;
        $business->form_3 = $request->form_3;
        $business->payment_frequency = $request->payment_frequency;
        $business->payment_frequency_date = $request->payment_frequency_date;
        $business->payment_frequency_1 = $request->payment_frequency_1;
        $business->payment_frequency_date_1 = $request->payment_frequency_date_1;
        $business->payment_frequency_2 = $request->payment_frequency_2;
        $business->payment_frequency_date_2 = $request->payment_frequency_date_2;
        $business->federal_frequency = $request->federal_frequency;
        $business->federal_payment_frequency = $request->federal_payment_frequency;
        //$business->remainder_date = $remainder;

        $business->soscertificate = $filname;
        $business->sosaoi = $filname1;


        $business->type_form = $request->type_form;
        $business->extension_due_date = $request->extension_due_date;
        $business->due_date = $request->due_date;
        $business->state_id = $request->state_id;
        $business->type_form_file2 = $request->type_form_file2;
        // $business->due_date2 = $request->due_date2;
        $business->extension_due_date2 = $request->extension_due_date2;
        $business->federal_payment_frequency_date = $request->federal_payment_frequency_date;
        $business->federal_frequency_due_date = $request->federal_frequency_due_date;

        $business->quarter_type = $request->quarter_type;
        $business->quarter_date = $request->quarter_date;
        $business->eptpspin = $request->eptpspin;
        $business->pin = $request->pin;
        $business->pay_pw = $request->pay_pw;

        $business->quarter_type_holding1 = $request->quarter_type_holding1;
        $business->quarter_date_holding = $request->quarter_date_holding;
        $business->frequency_type_holding1 = $request->frequency_type_holding1;
        $business->federal_frequency_due_date_holding = $request->federal_frequency_due_date_holding;
        $business->payment_frequency_type_holding2 = $request->payment_frequency_type_holding2;
        $business->payment_frequency_due_date_holding = $request->payment_frequency_due_date_holding;
        $business->quarter_type_unemploy1 = $request->quarter_type_unemploy1;
        $business->quarter_date_unemploy = $request->quarter_date_unemploy;
        $business->frequency_type_unemploy1 = $request->frequency_type_unemploy1;
        $business->federal_frequency_due_date_unemploy = $request->federal_frequency_due_date_unemploy;
        $business->payment_frequency_type_unemploy2 = $request->payment_frequency_type_unemploy2;
        $business->payment_frequency_due_date_unemploy = $request->payment_frequency_due_date_unemploy;

        $business->federal_note = $request->federal_note;
        $business->holding_uname = $request->holding_uname;
        $business->holding_password = $request->holding_password;
        $business->unemploy_uname = $request->unemploy_uname;
        $business->unemploy_password = $request->unemploy_password;
        $business->due_date4 = $request->due_date4;

        $business->tec_company_name = $request->tec_company_name;
        $business->tec_dba_name = $request->tec_dba_name;
        $business->tec_contact_address1 = $request->tec_contact_address1;
        $business->tec_contact_address2 = $request->tec_contact_address2;
        $business->tec_company_city = $request->tec_company_city;
        $business->tec_company_state = $request->tec_company_state;
        $business->tec_company_zip = $request->tec_company_zip;
        $business->tec_company_mobile = $request->tec_company_mobile;
        $business->tec_telephoneNo1Type = $request->tec_telephoneNo1Type;
        $business->tec_ext1 = $request->tec_ext1;
        $business->tec_fax = $request->tec_fax;
        $business->tec_website = $request->tec_website;
        $business->tec_company_email = $request->tec_company_email;


        $business->bank_accounts_count = $request->bank_accounts_count;


        $bus_license_exp_date = $request->bus_license_exp_date;
        if (!empty($request->bus_license_exp_date)) {
            $business_id = $request->business_id;
            $bus_license_exp_date = $request->bus_license_exp_date;
            $returnValue = DB::table('admin_license')->where('id', '=', $business_id)
                ->update([
                    'bus_license_exp_date' => date('M-d-Y', strtotime($bus_license_exp_date)),
                ]);
        }


        $agent_fname1 = $request->agent_fname1;
        $agent_mname1 = $request->agent_mname1;
        $agent_lname1 = $request->agent_lname1;
        $agent_position = $request->agent_position;
        $agent_position2 = $request->agent_position1;
        $agent_per = $request->agent_per;
        $effective_date = $request->effective_date;
        $conid = $request->conid;
        $agentstatus = $request->agentstatus;
        $total = $request->total;
        $i = 0;
        $info = admin_shareholder::where('admin_id', '=', $id)->first();
        foreach ($agent_fname1 as $post) {
            $conid1 = $conid[$i];
            $agent_fname11 = $agent_fname1[$i];
            $agent_mname11 = $agent_mname1[$i];
            $agent_lname11 = $agent_lname1[$i];
            $agentstatus1 = $agentstatus[$i];
            $agent_position1 = $agent_position[$i];
            $agent_position11 = $agent_position2;
            $agent_per1 = $agent_per[$i];
            $effective_date1 = $effective_date[$i];
            $total1 = $total;
            $i++;

            if (empty($conid1)) {
                $insert2 = DB::insert("insert into admin_shareholders(`agentstatus`,`agent_fname1`,`agent_mname1`,`agent_lname1`,`agent_position`,`agent_position1`,`total`,`admin_id`,`agent_per`,`effective_date`) values('" . $agentstatus1 . "','" . $agent_fname11 . "','" . $agent_mname11 . "','" . $agent_lname11 . "','" . $agent_position1 . "','" . $agent_position11 . "','" . $total1 . "','" . $id . "','" . $agent_per1 . "','" . $effective_date1 . "')");
            } else {
                $returnValue = DB::table('admin_shareholders')->where('id', '=', $conid1)
                    ->update(['agent_fname1' => $agent_fname11,
                        'agent_mname1' => $agent_mname11,
                        'agent_lname1' => $agent_lname11,
                        'agent_position' => $agent_position1,
                        'agent_position1' => $agent_position11,
                        'total' => $total1,
                        'admin_id' => $id,
                        'agent_per' => $agent_per1,
                        'effective_date' => $effective_date1,
                        'agentstatus' => $agentstatus1
                    ]);
                //$affectedRows = admin_shareholder::where('id', '=',$conid1)->delete();
                $affectedRows = admin_shareholder::where('agent_fname1', '=', '')->delete();
            }
        }

        //     $profession = $request->profession;
        //     $professiontype = $request->professiontype;
        //     $profession_state= $request->profession_state;
        //     $profession_effective_date= $request->profession_effective_date;
        //     $profession_license= $request->profession_license;
        //     $profession_note=$request->profession_note;
        //     $profession_id=$request->profession_id;
        //     $ce = $request->ce;
        //     //$profession_effective_date=$request->profession_effective_date;
        //     $profession_exp_date=$request->profession_exp_date;

        //     if($request->profession_priority!='')
        //     {
        //         $profession_priority=$request->profession_priority;
        //     }
        //     else
        //     {
        //         $profession_priority=$request->profession_priority2;
        //     }

        //     // $profession_priority=$request->profession_priority;
        //     $check_status=$request->check_status;
        //   // print_r($profession_exp_date);
        //     $jk = 0;
        //     //$info = admin_professional::where('admin_id', '=', $id)->first();
        //     if(!empty($profession))
        //     {
        //     foreach($profession as $post)
        //     {
        //         //$jk++;
        //         $profession_id1 =$profession_id[$jk];
        //         $profession1 =$post;
        //         $professiontype1 =$professiontype[$jk];
        //         $profession_state1 =$profession_state[$jk];
        //       // $profession_effective_date1 =$profession_effective_date[$jk];
        //         $profession_license1 =$profession_license[$jk];
        //         $profession_note1 =$profession_note[$jk];
        //         $profession_effective_date1 =$profession_effective_date[$jk];
        //         $profession_exp_date1 =$profession_exp_date[$jk];
        //         $profession_priority1 =$profession_priority[$jk];

        //         //$new_date = strtotime('+ 1 year', $profession_exp_date1);
        //         //$updateddate=date('Y-m-d', $new_date);

        //         $proid = $profession1.'-'.$profession_state1.'-'.$profession_license1;
        //         $ce1 = $ce[$jk];
        //         $jk++;

        //         if(empty($profession_id1))
        //         {
        //             // $insert2 = DB::insert("insert into admin_professionalss(`profession`,`professiontype`,`profession_state`,`profession_epr_date`,`profession_license`,`profession_note`,`admin_id`,`pro_id`,`profession_epr_date1`,`ce_check`)
        //             // values('".$profession1."','".$professiontype."','".$profession_state1."','".$profession_epr_date1."','".$profession_license1."','".$profession_note1."','".$id."','".$proid."','".$profession_effective_date1."','".$ce1."')");

        //             $insert2 = DB::insert("insert into admin_professionalss(`profession`,`professiontype`,`profession_state`,`profession_license`,`profession_note`,`admin_id`,`pro_id`,`profession_epr_date1`,`ce_check`)
        //             values('".$profession1."','".$professiontype."','".$profession_state1."','".$profession_license1."','".$profession_note1."','".$id."','".$proid."','".$profession_effective_date1."','".$ce1."')");

        //             // $affectedRows1 = admin_professional::where('profession', '=', '')->delete();
        //         }
        //         else
        //         {
        //             $returnValue = DB::table('admin_professionals')->where('id', '=',$profession_id1)
        //             ->update([
        //                   'profession' => $profession1,
        //                   'professiontype' => $professiontype1,
        //                   'profession_state' =>$profession_state1,
        //                   //'profession_epr_date' =>$profession_epr_date1,
        //                   'profession_license' =>$profession_license1,
        //                   'profession_note' =>$profession_note1,
        //                   'admin_id' =>$id,
        //                   'pro_id' =>$proid,
        //                   'profession_epr_date1' =>$profession_effective_date1,
        //                   'profession_exp_date' =>date('Y-m-d',strtotime($profession_exp_date1)),
        //                   //'profession_exp_date2' =>date('M-d-Y',strtotime($profession_exp_date2)),
        //                   'profession_priority' =>$profession_priority1,
        //                   'ce_check'=>$ce1,
        //                   'check_status'=>$check_status,

        //             ]);
        //             //$jk++;
        //             //$affectedRows1 = admin_professional::where('profession', '=', '')->delete();
        //         }

        //     }
        //     }


        //pro license start

        if (isset($request->profession) != '') {
            $profession_id = $request->profession_id;
            $persinfo = count(array_filter($request->profession));
            for ($i = 0; $i < $persinfo; $i++) {
                if ($request->profession != '' || $request->profession != null || $request->profession != '0') {
                    if (isset($profession_id[$i]) != '' || isset($profession_id[$i]) != null || isset($profession_id[$i]) != '0') {
                        $profession_id1 = $profession_id[$i];
                    } else {

                    }

                    $admin_id = $id;
                    $profession1 = isset($request->profession[$i]) ? $request->profession[$i] : "";
                    $professiontype1 = isset($request->professiontype[$i]) ? $request->professiontype[$i] : "";
                    $profession_state1 = isset($request->profession_state[$i]) ? $request->profession_state[$i] : "";
                    $profession_effective_date1 = isset($request->profession_effective_date[$i]) ? $request->profession_effective_date[$i] : "";
                    $profession_license1 = isset($request->profession_license[$i]) ? $request->profession_license[$i] : "";
                    $profession_note1 = isset($request->profession_note[$i]) ? $request->profession_note[$i] : "";
                    $ce1 = isset($request->ce[$i]) ? $request->ce[$i] : "";
                    $profession_exp_date22 = isset($request->profession_exp_date[$i]) ? $request->profession_exp_date[$i] : "";
                    //$profession_priority1 = isset($request->profession_priority[$i]) ? $request->profession_priority[$i]:"" ;


                    // $today=date('Y');
                    // $today2 = (int)$today;
                    // $profdateplus=$today2+$profession_priority1;
                    // $profession_exp_date1=$profession_exp_date22.'-'.$profdateplus;
                    // $check_flag1 = 1;

                    if (isset($request->profession_priority1[$i]) != '') {
                        $profession_priority1 = isset($request->profession_priority1[$i]) ? $request->profession_priority1[$i] : "";
                    } else if (isset($request->profession_priority2[$i]) != '') {
                        $profession_priority1 = isset($request->profession_priority2[$i]) ? $request->profession_priority2[$i] : "";
                    } else if (isset($request->profession_priority2[$i]) != '') {
                        $profession_priority1 = isset($request->profession_priority3[$i]) ? $request->profession_priority3[$i] : "";
                    }


                    if (isset($request->check_flag[$i]) != 1) {
                        $today = date('Y');
                        $today2 = (int)$today;
                        echo $profdateplus = $today2 + $profession_priority1;
                        $profession_exp_date1 = $profession_exp_date22 . '/' . $profdateplus;
                        $check_flag1 = 1;
                    } else {
                        $profession_exp_date1 = isset($request->profession_exp_date[$i]) ? $request->profession_exp_date[$i] : "";
                        $check_flag1 = isset($request->check_flag[$i]) ? $request->check_flag[$i] : "";
                    }

                    $proid = $profession1 . '-' . $profession_state1 . '-' . $profession_license1;
                    $check_type = $profession1 . '-' . $professiontype1 . '-' . $profession_state1 . '-' . $profession_license1;
                    $check_type = $profession1 . '-' . $profession_state1 . '-' . $professiontype1 . '-' . $profession_license1;
                    $profession_license_copy1 = isset($request->profession_license_copy[$i]) ? $request->profession_license_copy[$i] : "";
                    $profession_license_website1 = isset($request->profession_license_website[$i]) ? $request->profession_license_website[$i] : "";


                    $insert2 = DB::insert("insert into admin_professionals(`profession`,`professiontype`,`profession_state`,`profession_license`,`profession_note`,`admin_id`,`pro_id`,`profession_epr_date1`,`profession_exp_date`,`profession_priority`,`profession_license_copy`,`profession_license_website`,`ce_check`,`check_flag`,`check_type`)
                        values('" . $profession1 . "','" . $professiontype1 . "','" . $profession_state1 . "','" . $profession_license1 . "','" . $profession_note1 . "','" . $id . "','" . $proid . "','" . $profession_effective_date1 . "','" . $profession_exp_date1 . "','" . $profession_priority1 . "','" . $profession_license_copy1 . "','" . $profession_license_website1 . "','" . $ce1 . "','" . $check_flag1 . "','" . $check_type . "')");
                    if (empty($profession_id)) {

                    } else {
                        DB::table('admin_professionals')->where('id', '=', $profession_id1)->delete();
                    }
                }
            }
        }

        //pro license end


        // Banking Start


        if (isset($request->bank_name) != '') {
            $bank_id = $request->bank_id;
            $persinfo = count(array_filter($request->bank_name));
            for ($i = 0; $i < $persinfo; $i++) {
                if ($request->bank_name != '' || $request->bank_name != null || $request->bank_name != '0') {
                    if (isset($bank_id[$i]) != '' || isset($bank_id[$i]) != null || isset($bank_id[$i]) != '0') {
                        $bank_id1 = $bank_id[$i];
                    } else {

                    }

                    $admin_id = $id;
                    $bank_name1 = isset($request->bank_name[$i]) ? $request->bank_name[$i] : "";
                    $nick_name1 = isset($request->nick_name[$i]) ? $request->nick_name[$i] : "";
                    $fourdigit1 = isset($request->fourdigit[$i]) ? $request->fourdigit[$i] : "";
                    $statement1 = isset($request->statement[$i]) ? $request->statement[$i] : "";
                    $stubs1 = isset($request->stubs[$i]) ? $request->stubs[$i] : "";
                    $opendate1 = isset($request->opendate[$i]) ? $request->opendate[$i] : "";
                    $bankstatus1 = isset($request->bankstatus[$i]) ? $request->bankstatus[$i] : "";
                    $closedate1 = isset($request->closedate[$i]) ? $request->closedate[$i] : "";

                    DB::insert("insert into admin_bank(`admin_id`,`bank_name`,`nick_name`,`fourdigit`,`statement`,`stubs`,`opendate`,`bankstatus`,`closedate`)
                        values('" . $admin_id . "','" . $bank_name1 . "','" . $nick_name1 . "','" . $fourdigit1 . "','" . $statement1 . "','" . $stubs1 . "','" . $opendate1 . "','" . $bankstatus1 . "','" . $closedate1 . "')");
                    if (empty($bank_id)) {

                    } else {
                        DB::table('admin_bank')->where('id', '=', $bank_id1)->delete();
                    }
                }
            }
        }
        // Banking End


        // Card Start


        if (isset($request->card_bank_name) != '') {
            $card_id = $request->card_id;
            $persinfo = count(array_filter($request->card_bank_name));
            for ($i = 0; $i < $persinfo; $i++) {
                if ($request->card_bank_name != '' || $request->card_bank_name != null || $request->card_bank_name != '0') {
                    if (isset($card_id[$i]) != '' || isset($card_id[$i]) != null || isset($card_id[$i]) != '0') {
                        $card_id1 = $card_id[$i];
                    } else {

                    }

                    $admin_id = $id;
                    $card_bank_name1 = isset($request->card_bank_name[$i]) ? $request->card_bank_name[$i] : "";
                    $card_nick_name1 = isset($request->card_nick_name[$i]) ? $request->card_nick_name[$i] : "";
                    $card_fourdigit1 = isset($request->card_fourdigit[$i]) ? $request->card_fourdigit[$i] : "";
                    $card_statement1 = isset($request->card_statement[$i]) ? $request->card_statement[$i] : "";
                    $card_notes1 = isset($request->card_notes[$i]) ? $request->card_notes[$i] : "";
                    $card_opendate1 = isset($request->card_opendate[$i]) ? $request->card_opendate[$i] : "";
                    $card_bankstatus1 = isset($request->card_bankstatus[$i]) ? $request->card_bankstatus[$i] : "";
                    $card_closedate1 = isset($request->card_closedate[$i]) ? $request->card_closedate[$i] : "";

                    DB::insert("insert into admin_credit_information(`admin_id`,`card_bank_name`,`card_nick_name`,`card_fourdigit`,`card_statement`,`card_notes`,`card_opendate`,`card_bankstatus`,`card_closedate`)
                        values('" . $admin_id . "','" . $card_bank_name1 . "','" . $card_nick_name1 . "','" . $card_fourdigit1 . "','" . $card_statement1 . "','" . $card_notes1 . "','" . $card_opendate1 . "','" . $card_bankstatus1 . "','" . $card_closedate1 . "')");

                    if (empty($card_id)) {

                    } else {
                        DB::table('admin_credit_information')->where('id', '=', $card_id1)->delete();
                    }
                }
            }
        }


        // Card End

        $noteid = $request->noteid;
        $adminnotes = $request->adminnotes;
        $k = 0;
        $users = DB::table('notes')->where('admin_id', '=', $id)->first();
        if (!empty($adminnotes)) {
            foreach ($adminnotes as $notess) {
                $noteid1 = $noteid[$k];
                $note1 = $adminnotes[$k];
                $k++;
                if (empty($noteid1)) {
                    $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('" . $note1 . "','" . $id . "','Admin')");
                } else {
                    $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                        ->update(['notes' => $note1,
                            'admin_id' => $id,
                            'type' => 'admin'
                        ]);
                    $users = DB::table('notes')->where('notes', '')->delete();
                }
            }
        }


        if (isset($request->formation_yearbox) != '') {


            $formationid = $request->formationid;
            $admin_id = $request->admin_id;
            if ($request->formation_yearbox != '') {
                $formation_yearbox = $request->formation_yearbox;
            } else {
                $formation_yearbox = null;
            }

            if ($request->formation_yearbox2 != '') {
                $formation_yearbox = $request->formation_yearbox2;
            }

            if ($request->formation_yearbox3 != '') {
                $formation_yearbox = $request->formation_yearbox3;
            }

            $formation_yearvalue = $request->formation_yearvalue;
            $formation_amount = $request->formation_amount;
            $formation_payment = $request->formation_payment;
            $record_status = $request->record_status;

            if (isset($_FILES['annualreceipt']['name']) != '') {
                $path1 = public_path() . '/adminupload/' . $_FILES['annualreceipt']['name'];
                if (move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) {
                    $filesname1 = $_FILES['annualreceipt']['name'];
                }
                $annualreceipt = $filesname1;

            } else {
                $annualreceipt = null;
            }

            if (isset($_FILES['formation_work_officer']['name']) != '') {
                $path2 = public_path() . '/adminupload/' . $_FILES['formation_work_officer']['name'];
                if (move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) {
                    $filesname2 = $_FILES['formation_work_officer']['name'];
                }
                $formation_work_officer = $filesname2;
            } else {
                $formation_work_officer = null;
            }


            if (empty($admin_id)) {

                $insert2 = DB::insert("insert into admin_formation(admin_id,formation_yearbox,formation_yearvalue,formation_amount,formation_payment,record_status,annualreceipt,formation_work_officer) 
                                                                values('$id','$formation_yearbox','$formation_yearvalue','$formation_amount','$formation_payment','$record_status','$annualreceipt','$formation_work_officer')");
            } else {

                $returnValue = DB::table('admin_formation')->where('id', '=', $formationid)
                    ->update([

                        'admin_id' => $admin_id,
                        'formation_yearbox' => $formation_yearbox,
                        'formation_yearvalue' => $formation_yearvalue,
                        'formation_amount' => $formation_amount,
                        'formation_payment' => $formation_payment,
                        'record_status' => $record_status,
                        'annualreceipt' => $annualreceipt,
                        'formation_work_officer' => $formation_work_officer,

                    ]);
            }

        }

        $business->update();

        if ($business->update()) {
            // return response()->json(['status'     => 'success']);
            return redirect(route('adminprofile.index'))->with('success', 'Your Profile Successfully Updated');
        } else {
            // return response()->json(['status' => 'error']);
            return redirect(route('adminprofile.index'))->with('error', 'Your Profile Not Successfully Updated');
        }

        // return redirect(route('adminprofile.index'))->with('success','Your Profile Successfully Updated');
    }


    public function updateformation(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        // if(isset($request->formation_yearbox)!='')
        // {


        $formationid = $request->formationid;
        $admin_id = $request->admin_id;
        if ($request->formation_yearbox != '') {
            $formation_yearbox = $request->formation_yearbox;
        }

        if ($request->formation_yearbox2 != '') {
            $formation_yearbox = $request->formation_yearbox2;
        }

        if ($request->formation_yearbox3 != '') {
            $formation_yearbox = $request->formation_yearbox3;
        }

        $formation_yearvalue = $request->formation_yearvalue;
        $formation_amount = $request->formation_amount;
        $formation_paid = $request->formation_paid;
        $formation_payment = $request->formation_payment;
        $record_status = $request->record_status;

        // $annualreceipt = $request->annualreceipt;
        // $formation_work_officer = $request->formation_work_officer;

        if (isset($_FILES['annualreceipt']['name']) != '') {
            $path1 = public_path() . '/adminupload/' . $_FILES['annualreceipt']['name'];
            if (move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) {
                $filesname1 = $_FILES['annualreceipt']['name'];
                $annualreceipt = $filesname1;
            } else {
                $annualreceipt = '';
            }

        } else {
            $annualreceipt = '';
        }

        if (isset($_FILES['formation_work_officer']['name']) != '') {
            $path2 = public_path() . '/adminupload/' . $_FILES['formation_work_officer']['name'];
            if (move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) {
                $filesname2 = $_FILES['formation_work_officer']['name'];
                $formation_work_officer = $filesname2;
            } else {
                $formation_work_officer = '';
            }

        } else {
            $formation_work_officer = '';
        }

        $formation_website = $request->formation_website;


        $returnValue = DB::table('admin_formation')
            ->insert([
                'admin_id' => 1,
                'formation_yearbox' => $formation_yearbox,
                'formation_yearvalue' => $formation_yearvalue,
                'formation_amount' => $formation_amount,
                'formation_paid' => $formation_paid,
                'formation_payment' => $formation_payment,
                'record_status' => $record_status,
                'annualreceipt' => $annualreceipt,
                'formation_work_officer' => $formation_work_officer,
                'formation_website' => $formation_website,

            ]);

        //}

        return redirect(route('adminworkstatus.index'))->with('success', 'Formatin Successfully Added');

    }

    public function notedelete($id)
    {
        $users = DB::table('notes')->where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/adminprofile')->with('error', 'Your Record Deleted Successfully');
    }


    public function admindelete($id)
    {
        $logoStatus = admin_shareholder::findOrFail($id);
        $va = admin_shareholder::where('id', $id)->get()->first();
        admin_shareholder::where('id', $id)->delete();
        Adminupload::where('upload_name', '=', $va->pro_id)->delete();
        return redirect('fac-Bhavesh-0554/adminprofile')->with('error', 'Your Record Deleted Successfully');
    }

    public function admindelete1($id)
    {
        $logoStatus = admin_professional::findOrFail($id);
        Adminupload::where('upload_name', '=', $logoStatus->upload_name)->delete();
        admin_professional::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/adminprofile')->with('error', 'Your Record Deleted Successfully');
    }

    public function adminbankdelete($id)
    {
        DB::table('admin_bank')->where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/adminprofile')->with('error', 'Your Record Deleted Successfully');
    }


    public function admincarddelete($id)
    {
        DB::table('admin_credit_information')->where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/adminprofile')->with('error', 'Your Record Deleted Successfully');
        //return redirect()->back();
    }

    public function getAdminprolic(Request $request)
    {
        // $data = DB::table('admin_professionals')->select('*')->where('id', '=',$request->id)->get();
        //return response()->json($data);

        //$data = DB::table('admin_professionals')->select('*')->where('check_type', '=',$request->id)->get();
        // $data = DB::table('admin_pro_lic as t1')->select('t1.*','t2.id as ids','t2.profession','t2.profession','t2.professiontype','t2.profession_state','t2.profession_license','t2.profession_license_copy','t2.check_type')
        //                             ->leftJoin('admin_professionals as t2', function($join){ $join->on('t1.admin_lic_id', '=', 't2.id');})
        //                             ->where('t1.check_type', '=',$request->id)
        //                             ->get();

        $data = DB::table('admin_pro_lic as t1')->select('t1.*', 't2.id as ids', 't2.profession', 't2.profession', 't2.professiontype', 't2.profession_state', 't2.profession_license', 't2.profession_license_copy', 't2.check_type')
            ->leftJoin('admin_professionals as t2', function ($join) {
                $join->on('t1.check_type', '=', 't2.check_type');
            })
            ->where('t1.check_type', '=', $request->id)
            ->get();
        if (!$data) {
            $data = "File not Uploaded";
            return response()->json($data);
        } else {
            return response()->json($data);
        }
    }

    public function getAdmincertificate(Request $request)
    {

        //echo $id=$_POST['id'];exit;
        //print_r($request);
        $data = Admin::select('id', 'soscertificate')->where('id', $request->id)->get();
        //print_r($data);
        if (!$data) {
            $data = "File not Uploaded";
            return response()->json($data);
        } else {
            return response()->json($data);
        }
    }

    public function getAdminaoi(Request $request)
    {

//echo $id=$_POST['id'];exit;
//print_r($request);
        $data = Admin::select('id', 'sosaoi')->where('id', $request->id)->get();
        //print_r($data);
        if (!$data) {
            $data = "File not Uploaded";
            return response()->json($data);
        } else {
            return response()->json($data);
        }
    }

    public function getcontrol1(Request $request)
    {
        $data = Control::select('controlname')->where('statename', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function getformss(Request $request)
    {

        $data = taxfederal::select('authority_name', 'extension_due_date', 'short_name', 'telephone', 'formname', 'address', 'due_date', 'city', 'zip', 'website_link_name', 'typeofform')->where('telephone', $request->id)->take(100)->get();
        return response()->json($data);

    }

    public function getfaderal(Request $request)
    {
        $data = taxfederal::select('authority_name', 'payroll_department_name', 'extension_due_date', 'short_name', 'typeofform1', 'due_date', 'authority_level', 'typeofform', 'payroll_name', 'payroll', 'website_link_name', 'formname')->where('authority_name', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function getfaderalss(Request $request)
    {
        $data = taxstatesses::select('authority_name_state', 'state', 'payroll_department_name_state', 'extension_due_date_state', 'short_name_state', 'typeofform1_state', 'due_date_state', 'authority_level_state', 'typeofform_state', 'payroll_name_state', 'payroll_state', 'website_link_name_state', 'formname_state')->where('authority_name_state', $request->id)->take(100)->get();
        return response()->json($data);
    }


    public function getimages(Request $request)
    {
        $data = Adminupload::select('upload_name', 'upload')->where('upload_name', $request->id)->take(100)->get();
        if (!$data) {
            $data = "File not Uploaded";
            return response()->json($data);
        } else {
            return response()->json($data);
        }
    }


    public function history(Request $request)
    {
        $data = DB::table('adminupload_history')->select('upload_name', 'upload')->where('upload_name', $request->id)->take(100)->get();
        if (!$data) {
            $data = "File not Uploaded";
            return response()->json($data);
        } else {
            return response()->json($data);
        }
    }

    public function getcountycounty(Request $request)
    {
        $data = taxstate::select('county', 'id', 'countycode', 'zip')->where('state', $request->state)->take(1000)->get();
        return response()->json($data);
    }


    public function getcounty2(Request $request)
    {
        $data = taxstate::select('county', 'id', 'countycode', 'zip')->where('type', $request->id)->where('state', $request->state)->take(1000)->get();
        return response()->json($data);
    }

    public function getcountycode2(Request $request)
    {
        $data = taxstate::select('countycode', 'id')->where('county', $request->id)->take(1000)->get();
        return response()->json($data);
    }


    public function getcountycode4(Request $request)
    {
        $data = taxstate::select('countycode', 'id')->where('county', $request->id)->take(1000)->get();
        return response()->json($data);
    }


}