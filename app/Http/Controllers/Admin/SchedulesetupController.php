<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Schedule;
use App\Model\Schedulesetup;
use DB;
use Illuminate\Http\Request;

class SchedulesetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $schedule = Schedulesetup::where('type', 'employee')->get();
        return view('fac-Bhavesh-0554.schedulesetup.schedulesetup', compact(['schedule']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schedule = Schedulesetup::All();
        return view('fac-Bhavesh-0554/schedulesetup/create', compact('schedule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $position = new Schedulesetup;
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_date = $request->sch_end_date;
        $position->sch_end_day = $request->sch_end_day;
        $position->sch_pay_date = $request->sch_pay_date;
        $position->sch_pay_day = $request->sch_pay_day;
        $position->save();
        $lastId = $position->id;
        $note = $request->note;
        $i = 0;
        foreach ($note as $note1) {
            $note2 = $note[$i];
            $i++;
            $insert2 = DB::insert("insert into schedulesetup_notes(`note`,`she_setup_id`) values('" . $note2 . "','" . $lastId . "')");
        }
        return redirect('fac-Bhavesh-0554/schedulesetup')->with('success', 'Success fully add Schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedulesetup::where('id', $id)->first();
        $notes = DB::table('schedulesetup_notes')->where('she_setup_id', '=', $id)->get();
        return View('fac-Bhavesh-0554.schedulesetup.edit', compact(['schedule', 'notes']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $position = Schedulesetup::find($id);
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_date = $request->sch_end_date;
        $position->sch_end_day = $request->sch_end_day;
        $position->sch_pay_date = $request->sch_pay_date;
        $position->sch_pay_day = $request->sch_pay_day;
        $position->update();
        $note = $request->note;
        $noteid = $request->noteid;
        $i = 0;
        foreach ($note as $note1) {
            $note2 = $note[$i];
            $noteid1 = $noteid[$i];
            $i++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into schedulesetup_notes(`note`,`she_setup_id`) values('" . $note2 . "','" . $id . "')");
            } else {
                $returnValue = DB::table('schedulesetup_notes')->where('id', '=', $noteid1)
                    ->update(['note' => $note2]);

            }
        }

        return redirect('fac-Bhavesh-0554/schedulesetup')->with('success', 'Success fully Update Schedule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedulesetup::where('id', $id)->delete();
        return redirect(route('schedulesetup.index'))->with('success', 'Success Fully Delete Record');
    }
}