<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\taxstate;
use DB;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = City::All();
        return view('fac-Bhavesh-0554/city/city', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = City::All();
        $county = taxstate::All();
        return view('fac-Bhavesh-0554/city/create', compact(['position', 'county']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'states' => 'required',
            'tax_authority' => 'required',
            'country_code' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'website' => 'required',
        ]);
        $position = new City;
        $position->authority_name = $request->states;
        $position->short_name = $request->tax_authority;
        $position->type_of_tax = $request->country_code;
        $position->telephone = $request->telephone;
        $position->address = $request->address;
        $position->city = $request->city;
        $position->state = $request->state;
        $position->zip = $request->zip;
        $position->website = $request->website;
        $position->save();
        return redirect('fac-Bhavesh-0554/city')->with('success', 'Success fully add state');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return View('fac-Bhavesh-0554.city.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $this->validate($request, [
            'states' => 'required',
            'tax_authority' => 'required',
            'country_code' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'website' => 'required',
        ]);
        $position = $city;
        $position->authority_name = $request->states;
        $position->short_name = $request->tax_authority;
        $position->type_of_tax = $request->country_code;
        $position->telephone = $request->telephone;
        $position->address = $request->address;
        $position->city = $request->city;
        $position->state = $request->state;
        $position->zip = $request->zip;
        $position->website = $request->website;
        $position->update();
        return redirect('fac-Bhavesh-0554/city')->with('success', 'Success fully update state');
    }


    public function getcounty(Request $request)
    {
        $data = taxstate::select('county', 'id', 'countycode')->where('state', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode', 'id')->where('county', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect(route('city.index'));
    }
}