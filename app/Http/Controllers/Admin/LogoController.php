<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Logo;
use Illuminate\Http\Request;

class LogoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business = Logo::All();
        return view('fac-Bhavesh-0554.logo.logo', compact('business'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($request->hasFile('logo')) {
            $filname = $request->logo->getClientOriginalName();
            $request->logo->move('public/business', $filname);
        }

        $business = new Logo;
        $business->logo = $filname;
        $business->logoname = $request->logoname;
        $business->logourl = $request->logourl;
        $business->save();
        return redirect(route('logo.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bussiness = Logo::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.logo.logo', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Logo::where('id', $id)->first();
        return View('fac-Bhavesh-0554.logo.edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->hasFile('logo')) {
            $filname = $request->logo->getClientOriginalName();
            $request->logo->move('public/business', $filname);
        } else {
            $filname = $request->logo1;
        }

        $business = Logo::find($id);
        $business->logo = $filname;
        $business->logoname = $request->logoname;
        $business->logourl = $request->logourl;
        $business->update();
        return redirect(route('logo.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Logo::where('id', $id)->delete();
        return redirect(route('logo.index'));
    }
}