<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Ethnic;
use Illuminate\Http\Request;


class EthnicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ethnic = Ethnic::All();
        return view('fac-Bhavesh-0554.ethnic.ethnic', compact('ethnic'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.ethnic.ethnic-add-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ethnic_name' => 'required'

        ]);

        $ethnic = new Ethnic;
        $ethnic->ethnic_name = $request->ethnic_name;
        $ethnic->save();
        return redirect(route('ethnic.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
        $Ethnic = Ethnic::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.ethnic.ethnic', compact('ethnic'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ethnic = Ethnic::where('id', $id)->first();
        return View('fac-Bhavesh-0554.ethnic.ethnic-edit', compact('ethnic'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ethnic_name' => 'required'

        ]);

        $ethnic = Ethnic::find($id);
        $ethnic->ethnic_name = $request->ethnic_name;
        $ethnic->update();
        return redirect(route('ethnic.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Link $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ethnic::where('id', $id)->delete();
        return redirect(route('ethnic.index'));
    }
}