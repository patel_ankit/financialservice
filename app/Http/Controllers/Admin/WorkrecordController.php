<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Categorybusiness;
use App\Model\client_shareholder;
use App\Model\Contact_userinfo;
use App\Model\Currency;
use App\Model\Employee;
use App\Model\Ethnic;
use App\Model\Language;
use App\Model\Period;
use App\Model\Price;
use App\Model\taxstate;
use App\Model\Taxtitle;
use App\Model\Typeofser;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class WorkrecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {

        $id = $request->id;


        $lastformation = DB::table('client_formation')->whereRaw('FIND_IN_SET("' . date('Y') . '",formation_yearvalue)')->where('client_id', '=', $id)->orderby('id', 'desc')->first();

        $lastformation1 = DB::table('client_formation')->where('client_id', '=', $id)->orderby('id', 'desc')->first();

        $document = DB::table('clientdocument')->get();
        $documentupload = DB::table('clienttodocument')->where('client_id', '=', $id)->get();

        $taxstate = taxstate::All();
        $language = language::orderBy('language_name', 'asc')->get();
        $ethnic = Ethnic::orderBy('ethnic_name', 'asc')->get();
        $newclient12 = Commonregister::where('id', $id)->first();
        $newclient13 = Commonregister::where('id', $id)->first();


        $admin_shareholder = client_shareholder::orderBy('agent_position', 'ASC')->get();
        $shareholders = client_shareholder::where('client_id', '=', $id)->get();
        $formationsetup = DB::table('formationsetups')->select('formationsetups.annualfees as annualfees', 'formationsetups.processingfees as processingfees', 'formationsetups.renewalwebsite as renewalwebsite', 'formationsetups.date as date', 'formationsetups.expiredate2 as expiredate2', 'formationsetups.question as question', 'formationsetups.state as state')
            ->where('formationsetups.question', '=', "6")->get()->first();
        //print_r($formationsetup);die; 


        if (empty($newclient12)) {
            $other_first_language1 = '';
            $other_second_language1 = '';
            $category = '';
            $cb = '';
            $bid = '';
            $businessbrand = '';
            $stateform = '';
            $newclient13 = '';
            $clienttax = '';
            $clientfedext = '';
            $clientfed = '';
            $clientorg = '';


        } else {
            $bid = $newclient12->business_id;
            $catid = $newclient12->business_cat_id;
            $brand_id = $newclient12->business_brand_id;
            $businessbrand = BusinessBrand::where('business_cat_id', '=', $catid)->orderBy('business_brand_name', 'asc')->get();
            $cb = Categorybusiness::where('business_brand_id', '=', $brand_id)->orderBy('business_brand_category_name', 'asc')->get();
            $category = Category::where('bussiness_name', '=', $bid)->orderby('business_cat_name', 'asc')->get();
            $other_first_language1 = Employee::where('id', '=', $newclient12->other_first_language1)->first();
            $other_second_language1 = Employee::where('id', '=', $newclient12->other_second_language1)->first();
            $stateform = DB::table('statetax')->where('statename', '=', $newclient12->stateId)->first();
            $newclient13 = Commonregister::where('id', $id)->first();
            $clienttax = DB::table('client_to_taxation')->where('clientid', $id)->first();
            $clientfed = DB::table('client_taxfederal')->where('client_id', $id)->where('federalstax', 'Original')->count();
            $clientfedext = DB::table('client_taxfederal')->where('client_id', $id)->where('federalstax', 'Extension')->count();

            $clientorg = DB::table('client_taxfederal')->where('client_id', $id)->where('federalstax', 'Original')->get();


        }


        $clientposition = DB::table('clienttoofficeposition')->where('clientid', '=', $id)->get();
        $business = Business::orderBy('bussiness_name', 'asc')->get();

        $info = Contact_userinfo::where('user_id', '=', $id)->get();
        $info1 = Contact_userinfo::where('user_id', '=', $id)->first();
        $user_id = Auth::user()->id;
        $business_cat_id = Auth::user()->business_cat_id;
        $user = User::where('user_id', '=', $id)->first();
        $newclient = Commonregister::where('id', $id)->update(['newclient' => 2]);
        $subcustomer = User::get();
        $note = DB::table('notes')->get();
        $newlocations = DB::table('newlocations')->where('clientid', $id)->get();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
        $period = Period::All();

        $taxtitle = Taxtitle::All();
        $admin_notes = DB::table('notes')->where('type', '=', 'admin')->where('userid', '=', $id)->get();
        $clientser = DB::table('clientservices')->where('clientid', '=', $id)->get();
        $clientser5 = DB::table('clientservices')->where('clientid', '=', $id)->get()->first();
        $clientsertitle = DB::table('clientservicetitles')->where('clientid', '=', $id)->where('clientid', '=', $id)->get();
        $client = DB::table('notes')->where('admin_id', '=', $id)->where('type', '=', 'client')->get();
        $fsc = DB::table('notes')->where('admin_id', '=', '')->where('type', '=', 'fsc')->get();
        $every = DB::table('notes')->where('type', '=', 'Everybody')->get();
        $employee1 = Employee::where('check', '=', '1')->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        $employee = Fscemployee::orderBy('name', 'asc')->get();
        $common = DB::table('commonregisters')->select('commonregisters.type_form_file2', 'commonregisters.type_form', 'commonregisters.formation_penalty', 'commonregisters.formation_register_entity', 'commonregisters.paiddate', 'commonregisters.work_status', 'commonregisters.work_annualfees', 'commonregisters.work_processingfees', 'commonregisters.work_totalamt', 'commonregisters.work_paidby', 'commonregisters.work_paymentmethod', 'commonregisters.work_note', 'commonregisters.work_officer', 'commonregisters.work_annualreceipt', 'commonregisters.work_renewyear', 'commonregisters.work_zip', 'commonregisters.work_renewperiod', 'commonregisters.work_address', 'commonregisters.work_city', 'commonregisters.work_state', 'commonregisters.work_address as work_address', 'commonregisters.work_changes as work_changes', 'commonregisters.id as id', 'commonregisters.formation_address as formation_address', 'commonregisters.formation_city as formation_city', 'commonregisters.formation_state as formation_state', 'commonregisters.formation_zip as formation_zip', 'commonregisters.common_year as common_year', 'commonregisters.common_type as common_type', 'commonregisters.common_address as common_address', 'commonregisters.city as common_city', 'commonregisters.common_state as common_state', 'commonregisters.common_zip as common_zip', 'commonregisters.id as cid', 'commonregisters.contact_number as contact_number', 'commonregisters.contact_title as contact_title', 'commonregisters.type_of_entity_answer as type_of_entity_answer', 'commonregisters.paymentmode11 as paymentmode11', 'commonregisters.carttype1 as carttype1', 'commonregisters.cartno1 as cartno1', 'commonregisters.acountname1 as acountname1', 'commonregisters.paymentnote1 as paymentnote1', 'commonregisters.acountname1 as acountname1', 'commonregisters.acountno1 as acountno1', 'commonregisters.routingno1 as routingno1', 'commonregisters.bankname1 as bankname1', 'commonregisters.maritial_spouse1 as maritial_spouse1', 'commonregisters.maritial_spouse as maritial_spouse', 'commonregisters.ext2 as ext2', 'commonregisters.telephoneNo2Type as telephoneNo2Type', 'commonregisters.motelephoneile1 as motelephoneile1', 'commonregisters.maritial_last_name as maritial_last_name', 'commonregisters.maritial_middle_name as maritial_middle_name', 'commonregisters.maritial_first_name as maritial_first_name', 'commonregisters.personalname as personalname', 'commonregisters.CL as CL', 'commonregisters.other_maritial_status1 as other_maritial_status1', 'commonregisters.other_maritial_status as other_maritial_status', 'commonregisters.other_dob_month as other_dob_month', 'commonregisters.other_dob_day as other_dob_day', 'commonregisters.other_spouse_month as other_spouse_month', 'commonregisters.other_spouse_day as other_spouse_day', 'commonregisters.other_marriage_month as other_marriage_month', 'commonregisters.other_marriage_day as other_marriage_day', 'commonregisters.other_ethnic as other_ethnic', 'commonregisters.other_main_language as other_main_language', 'commonregisters.other_first_language as other_first_language', 'commonregisters.other_second_language as other_second_language', 'commonregisters.emailbli1 as emailbli1', 'commonregisters.faxbli1 as faxbli1', 'commonregisters.faxbli3 as faxbli3', 'commonregisters.cartnote as cartnote', 'commonregisters.billingtoo as billingtoo', 'commonregisters.cartno as cartno', 'commonregisters.carttype as carttype', 'commonregisters.paymentmode1 as paymentmode1', 'commonregisters.acountname as acountname', 'commonregisters.acountno as acountno', 'commonregisters.routingno as routingno', 'commonregisters.bankname as bankname', 'commonregisters.paymentnote as paymentnote', 'commonregisters.paymentmode as paymentmode', 'commonregisters.locations as locations', 'commonregisters.multilocation as multilocation', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.user_answer3 as user_answer3', 'commonregisters.user_answer2 as user_answer2', 'commonregisters.user_answer1 as user_answer1', 'commonregisters.limited_answer3 as limited_answer3', 'commonregisters.limited_answer2 as limited_answer2', 'commonregisters.limited_answer1 as limited_answer1', 'commonregisters.user_question3 as user_question3', 'commonregisters.user_question2 as user_question2', 'commonregisters.user_question1 as user_question1', 'commonregisters.limited_question3 as limited_question3', 'commonregisters.limited_question2 as limited_question2', 'commonregisters.limited_question1 as limited_question1', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdate as user_resetdate', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdays as user_resetdays', 'commonregisters.user_active as user_active', 'commonregisters.limited_resetdate as limited_resetdate', 'commonregisters.limited_resetdays as limited_resetdays', 'commonregisters.limited_active as limited_active', 'commonregisters.limited_user as limited_user', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_resetdays as subscription_resetdays', 'commonregisters.subscription_resetdate as subscription_resetdate', 'commonregisters.subscription_active as subscription_active', 'commonregisters.subscription_lock as subscription_lock', 'commonregisters.subscription_user as subscription_user', 'commonregisters.user_cell as user_cell', 'commonregisters.user_email as user_email', 'commonregisters.user_name as user_name', 'commonregisters.creationdate as creationdate', 'commonregisters.nametype as nametype', 'commonregisters.etelephone2 as etelephone2', 'commonregisters.eext2 as eext2', 'commonregisters.eteletype2 as eteletype2', 'commonregisters.eext1  as eext1', 'commonregisters.eteletype1 as eteletype1', 'commonregisters.filename as filename', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.businessext as businessext', 'commonregisters.businesstype as businesstype', 'commonregisters.contact_address1 as contact_address1', 'commonregisters.contact_address2 as contact_address2', 'commonregisters.city_1 as city_1', 'commonregisters.state_1 as state_1', 'commonregisters.zip_1 as zip_1', 'commonregisters.mobile_1 as mobile_1', 'commonregisters.mobiletype_1 as mobiletype_1', 'commonregisters.ext2_1 as ext2_1', 'commonregisters.mobile_2 as mobile_2', 'commonregisters.mobiletype_2 as mobiletype_2', 'commonregisters.ext2_2 as ext2_2', 'commonregisters.contact_fax_1 as contact_fax_1', 'commonregisters.email_1 as email_1', 'commonregisters.minss as minss', 'commonregisters.firstname as firstname', 'commonregisters.middlename as middlename', 'commonregisters.lastname as lastname', 'commonregisters.mailing_address1 as mailing_address1', 'commonregisters.bussiness_zip as bussiness_zip', 'commonregisters.due_date as due_date', 'commonregisters.department as department', 'commonregisters.type_of_activity as type_of_activity', 'commonregisters.county_no as county_no', 'commonregisters.county_name as county_name', 'commonregisters.level as level', 'commonregisters.setup_state as setup_state', 'commonregisters.business_state as business_state', 'commonregisters.business_city as business_city', 'commonregisters.business_country as business_country', 'commonregisters.business_address as business_address', 'commonregisters.business_store_name as business_store_name', 'commonregisters.mailing_address as mailing_address', 'commonregisters.legalname as legalname', 'commonregisters.dbaname as dbaname', 'commonregisters.mailing_city as mailing_city', 'commonregisters.mailing_state as mailing_state', 'commonregisters.mailing_zip as mailing_zip', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name', 'commonregisters.formation_yearbox', 'commonregisters.formation_yearvalue', 'commonregisters.formation_amount', 'commonregisters.formation_payment', 'commonregisters.record_status', 'commonregisters.annualreceipt', 'commonregisters.formation_work_officer', 'clienttopersonaltax.personal_taxid', 'clienttopersonaltax.client_id', 'clienttopersonaltax.filing_type', 'clienttopersonaltax.filing_year', 'clienttopersonaltax.filing_method', 'clienttopersonaltax.filing_date', 'clienttopersonaltax.filing_software', 'clienttopersonaltax.filing_state', 'clienttopersonaltax.date_of_filing')
            ->leftJoin('categories', function ($join) {
                $join->on('commonregisters.business_cat_id', '=', 'categories.id');
            })
            ->leftJoin('businesses', function ($join) {
                $join->on('commonregisters.business_id', '=', 'businesses.id');
            })
            ->leftJoin('business_brands', function ($join) {
                $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
            })
            ->leftJoin('categorybusinesses', function ($join) {
                $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
            })
            ->leftJoin('clienttopersonaltax', function ($join) {
                $join->on('clienttopersonaltax.client_id', '=', 'commonregisters.id');
            })
            ->where('commonregisters.id', '=', "$id")->get()->first();
        //echo "<pre>";
        //  print_r($common);die;

        //  print_r($stateform);
        //exit;

        $buslicense = DB::table('client_license')->select('*')->where('client_id', '=', $id)->get();
        // $formation = DB::table('client_formation')->where('client_id',$id)->orderBy('id', 'desc')->first();
        $formation = DB::table('client_formation')->where('client_id', $id)->whereRaw('FIND_IN_SET("' . date('Y') . '",formation_yearvalue)')->first();
        $formations = DB::table('client_formation')->where('client_id', $id)->first();
        $datastate = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate2 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();
        $datastate3 = DB::table('citystate')->where('country', 'USA')->orderBy('state', 'asc')->get();

        $Incometaxfederal = DB::table('client_taxfederal')->where('client_id', '=', $id)->where('federalsyear', '=', '2019')->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->count();
        //print_r($Incometaxfederal);die;
        $Incometax3 = DB::table('client_taxfederal')->orderBy('federalsyear', 'DESC')->get();
        $Incometaxstates = DB::table('client_taxstate')->orderBy('stateyear', 'DESC')->groupBy('stateyear')->get();
        $Incometax2 = DB::table('client_taxstate')->orderBy('stateyear', 'DESC')->get();
        $incomes = DB::table('incomes')->where('client_id', '=', $id)->orderby('wrkyear', 'desc')->get();

        return view('fac-Bhavesh-0554/workrecord/workrecord', compact(['clientorg', 'clientfedext', 'clientfed', 'clienttax', 'newclient13', 'stateform', 'lastformation', 'lastformation1', 'incomes', 'datastate3', 'Incometaxfederal', 'Incometax3', 'Incometaxstates', 'Incometax2', 'datastate2', 'datastate', 'formations', 'formation', 'documentupload', 'document', 'buslicense', 'shareholders', 'clientposition', 'formationsetup', 'admin_shareholder', 'other_first_language1', 'other_second_language1', 'ethnic', 'language', 'newlocations', 'subcustomer', 'employee1', 'employee', 'clientser5', 'clientser', 'clientsertitle', 'note', 'every', 'fsc', 'taxstate', 'admin_notes', 'client', 'common', 'category', 'business', 'businessbrand', 'cb', 'user', 'info', 'info1', 'position', 'currency', 'period', 'typeofser', 'taxtitle']));
    }


    function edit($id)
    {
        $state = DB::table('state')->get();
        $incomes = DB::table('incomes')->where('client_id', '=', $id)->orderby('wrkyear', 'desc')->get();
        $common = Commonregister::where('id', $id)->first();
        $incomes = DB::table('incomes')->where('client_id', '=', $id)->orderby('wrkyear', 'desc')->get();
        $weges = DB::table('weges')->where('client_id', '=', $id)->first();

        return view('fac-Bhavesh-0554.workrecord.edit', compact(['state', 'id', 'incomes', 'common', 'weges']));
    }

    public function getClientdata(Request $request)
    {
        $state = DB::table('state')->get();
        $documentRow = DB::table('employees')->where('client_id', $request->incomeid)->take(100)->get();
        //$documentRow = DB::table('employees')->where('client_id',$request->incomeid)->get();
        $output = '';

        // foreach($documentRow as $documentRow1)
        // {
        //     // print_r($documentRow1);die;
        //     $output.="<tr>
        //     <td style='width:10%;display:none;'>".$documentRow1->id."</td>
        //     <td style='width:10%;'></td>
        //     <td style='width:10%'>".$documentRow1->firstName." ".$documentRow1->lastName."</td>
        //     <td style='width:10%'><input type='text' class='form-control income_number txtinput_1 wagestotal2' name='wagesamounts'></td>
        //     <td style='width:10%'>".$documentRow1->stateId."</td>
        //   </tr>";
        // }
        //return response()->json($output);

        return view('fac-Bhavesh-0554.workrecord.edit', compact(['state', 'documentRow']));
    }

    public function destroyincomes(Request $request, $id)
    {
        DB::table('incomes')->where('id', '=', $id)->delete();
        return redirect()->back()->with('success', 'Your Record Deleted Successfully ');
    }

    public function getTaxstatedata(Request $request)
    {
        $documentRow = DB::table('statetax')->where('statename', $request->statetaxval)->first();
        return response()->json($documentRow);
    }


    function addincome(Request $request, $id)
    {
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $wrkyear = $request->wrkyear;
        $wagetotal = $request->wagetotal;
        $taxintrest = $request->taxintrest;
        $qulifieddividends = $request->qulifieddividends;
        $pension = $request->pension;
        $socialsecurity = $request->socialsecurity;
        $capital = $request->capital;
        $otherincome = $request->otherincome;
        $totalamount = $request->totalamount;

        if (isset($request->wrkyear) != '') {
            $ids = DB::table('incomes')
                ->insert([
                    'client_id' => $id,
                    'wrkyear' => $wrkyear,
                    'wagetotal' => $wagetotal,
                    'taxintrest' => $taxintrest,
                    'qulifieddividends' => $qulifieddividends,
                    'pension' => $pension,
                    'socialsecurity' => $socialsecurity,
                    'capital' => $capital,
                    'otherincome' => $otherincome,
                    'totalamount' => $totalamount
                ]);
        }

        return redirect()->back()->with('success', 'Income Added Successfully');

    }

    function updateincome(Request $request)
    {
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;
        $incomeid = $request->incomeid;
        $wrkyear = $request->wrkyear;
        $wagetotal = $request->wagetotal;
        $taxintrest = $request->taxintrest;
        $qulifieddividends = $request->qulifieddividends;
        $pension = $request->pension;
        $socialsecurity = $request->socialsecurity;
        $capital = $request->capital;
        $otherincome = $request->otherincome;
        $totalamount = $request->totalamount;

        $ids = DB::table('incomes')->where('id', $incomeid)
            ->update([
                'wrkyear' => $wrkyear,
                'wagetotal' => $wagetotal,
                'taxintrest' => $taxintrest,
                'qulifieddividends' => $qulifieddividends,
                'pension' => $pension,
                'socialsecurity' => $socialsecurity,
                'capital' => $capital,
                'otherincome' => $otherincome,
                'totalamount' => $totalamount
            ]);

        return redirect()->back()->with('success', 'Income Updated Successfully');

    }


    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->get();
            $output = '';
            $i;
            foreach ($data as $row) {
                // print_r($row);
                // echo $row->filename;
                // echo $query;
                if ($row->business_id == '6') {
                    $names = $row->first_name . ' ' . $row->middle_name . ' ' . $row->last_name;
                } else {
                    $names = $row->company_name;
                }
                $output .= '<li style="text-align:left;"><a href="' . url('fac-Bhavesh-0554/workrecord?id=') . '' . $row->id . '"><span class="clientalign">' . $row->filename . '</span><span class="entityname"> ' . $names . '</span></a></li>';
            }
            $output .= '';
            echo $output;
        }
    }

    function fetch1()
    {
        $clientformation = DB::table('client_formation')->where('client_id', '=', $_POST['clientsid'])->get();
        $clientformation1 = DB::table('client_formation')->where('client_id', '=', $_POST['clientsid'])->where('formation_yearbox', '!=', '')->get();


        //echo "<pre>";
        //   echo $_POST['formation_id'];exit;
        //     print_r($_POST);die;
        if (isset($_POST['work_changes'])) {
            $workchanges = $_POST['work_changes'];
        } else {
            $workchanges = '';
        }


        $path1 = public_path() . '/adminupload/' . $_FILES['annualreceipt']['name'];
        $path2 = public_path() . '/adminupload/' . $_FILES['formation_work_officer']['name'];
        if (move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) {
            $filesname1 = $_FILES['annualreceipt']['name'];
        } else {
            if (isset($_POST['annualreceipt_1']) != '') {
                $filesname1 = $_POST['annualreceipt_1'];
            } else {
                $filesname1 = '';
            }

        }


        if (move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) {
            $filesname2 = $_FILES['formation_work_officer']['name'];
        } else {
            if (isset($_POST['officers']) != '') {
                $filesname2 = $_POST['officers'];
            } else {
                $filesname2 = '';
            }

        }


        $birth = date("Y-m-d", strtotime($_POST['paiddate']));
        if (isset($_POST['common_year']) != '') {
            $common_year = $_POST['common_year'];
        } else {
            $common_year = '';
        }

        $id = DB::table('commonregisters')
            ->where('id', $_POST['clientsid'])
            ->update([
                'work_year' => $common_year,
                'work_changes' => $workchanges,
                'work_address' => $_POST['common_address'],
                'work_city' => $_POST['common_city'],
                'work_state' => $_POST['common_state'],
                'work_zip' => $_POST['common_zip'],

                'work_note' => $_POST['record_note'],
                'paiddate' => $birth


            ]);

        $clientformationcount = count($clientformation);
        $clientformationcount1 = count($clientformation1);
        // echo $clientformationcount;exit;
        if ($clientformationcount > '0' && $clientformationcount1 == '0') {
            if ($_POST['record_paid_by'] == 'Client') {
                $amounts = '';

            } else if ($_POST['record_paid_by'] == 'FSC') {
                $amounts = str_replace("$", "", $_POST['formation_amount']);
            } else {
                $amounts = '';
            }
            $id = DB::table('client_formation')
                ->where('id', $_POST['formation_id'])
                ->update([
                    'client_id' => $_POST['formation_client_id'],
                    'annualreceipt' => $filesname1,
                    'formation_work_officer' => $filesname2,
                    'formation_yearbox' => str_replace("Year", "", $_POST['formation_yearbox']),
                    'formation_yearvalue' => $_POST['formation_yearvalue'],
                    'record_status' => 'Active Compliance',
                    'formation_amount' => $amounts,
                    'formation_payment' => $_POST['formation_payment'],
                    'record_paid_by' => $_POST['record_paid_by'],
                    'formation_penalty' => str_replace("$", "", $_POST['formation_penalty']),
                    'work_processingfees' => str_replace("$", "", $_POST['work_processingfees']),
                    'record_totalamt' => str_replace("$", "", $_POST['record_totalamt']),
                    'paiddate' => $_POST['paiddate'],
                    'record_note' => $_POST['record_note'],

                ]);
        } else if ($clientformationcount1 > '0') {

            if ($_POST['record_paid_by'] == 'Client') {
                $amounts = '';

            } else if ($_POST['record_paid_by'] == 'FSC') {
                $amounts = str_replace("$", "", $_POST['formation_amount']);
            } else {
                $amounts = '';
            }
            $id = DB::table('client_formation')
                ->where('id', $_POST['formation_id'])
                ->update([
                    'client_id' => $_POST['formation_client_id'],
                    'annualreceipt' => $filesname1,
                    'formation_work_officer' => $filesname2,
                    'formation_yearbox' => str_replace("Year", "", $_POST['formation_yearbox']),
                    'formation_yearvalue' => $_POST['formation_yearvalue'],
                    'record_status' => 'Active Compliance',
                    'formation_amount' => $amounts,
                    'formation_payment' => $_POST['formation_payment'],
                    'record_paid_by' => $_POST['record_paid_by'],
                    'formation_penalty' => str_replace("$", "", $_POST['formation_penalty']),
                    'work_processingfees' => str_replace("$", "", $_POST['work_processingfees']),
                    'record_totalamt' => str_replace("$", "", $_POST['record_totalamt']),
                    'paiddate' => $_POST['paiddate'],
                    'record_note' => $_POST['record_note'],

                ]);


        } else {
            if ($_POST['record_paid_by'] == 'Client') {
                $amounts = '';

            } else if ($_POST['record_paid_by'] == 'FSC') {
                $amounts = str_replace("$", "", $_POST['formation_amount']);
            } else {
                $amounts = '';
            }
            $id = DB::table('client_formation')
                ->insert([
                    'client_id' => $_POST['formation_client_id'],
                    'annualreceipt' => $filesname1,
                    'formation_work_officer' => $filesname2,
                    'formation_yearbox' => str_replace("Year", "", $_POST['formation_yearbox']),
                    'formation_yearvalue' => $_POST['formation_yearvalue'],
                    'record_status' => 'Active Compliance',
                    'formation_amount' => $amounts,
                    'formation_payment' => $_POST['formation_payment'],
                    'record_paid_by' => $_POST['record_paid_by'],
                    'formation_penalty' => str_replace("$", "", $_POST['formation_penalty']),
                    'work_processingfees' => str_replace("$", "", $_POST['work_processingfees']),
                    'record_totalamt' => str_replace("$", "", $_POST['record_totalamt']),
                    'paiddate' => $_POST['paiddate'],
                    'record_note' => $_POST['record_note'],
                ]);
        }


        $fname = 0;
        $clientid = $_POST['clientsid'];

        if (isset($_POST['first_name'])) {
            foreach ($_POST['first_name'] as $first_name) {
                //  print_r($request->first_name);
                $id = $_POST['idss'][$fname];

                $firstname = $first_name;
                $last_name = $_POST['last_name'][$fname];
                $position = $_POST['position'][$fname];

                $usid1 = $id;
                $fname++;
                $insert2 = DB::update("update clienttoofficeposition set clientid='" . $clientid . "',first_name='" . $firstname . "',last_name='" . $last_name . "',position='" . $position . "' where id=" . $id);

            }
        }
        // die;

        return redirect('fac-Bhavesh-0554/workrecord?id=' . $clientid)->with('success', 'Your Formation Successfully Updated');


    }


    public function storefederal(Request $request)
    {
        //   echo "<pre>"; print_r($_FILES);EXIT;
        // echo "<pre>"; print_r($_POST);EXIT;

        $client_id = $request->client_id;
        $federalsyear = $request->federalsyear;
        $federalstax = $request->federalstax;
        $federalsduedate = date('Y-m-d', strtotime($request->federalsduedate));
        $federalsform = $request->federalsform;
        $federalsmethod = $request->federalsmethod;
        $federalsdate = date('Y-m-d', strtotime($request->federalsdate));
        $federalsstatus = $request->federalsstatus;
        $federalsnote = $request->federalsnote;
        $path1 = public_path() . '/adminupload/' . $_FILES['federalsfile']['name'];
        if (isset($_FILES['federalsfile']['name']) != '') {
            if (move_uploaded_file($_FILES['federalsfile']['tmp_name'], $path1)) {
                $federal_copy = $_FILES['federalsfile']['name'];
            } else {
                $federal_copy = '';
            }
        }
        $federalsfile = $federal_copy;

        $ids = DB::table('client_taxfederal')->insertGetId(array(
            'client_id' => $client_id,
            'federalsyear' => $federalsyear,
            'federalstax' => $federalstax,
            'federalsduedate' => $federalsduedate,
            'federalsform' => $federalsform,
            'federalsmethod' => $federalsmethod,
            'federalsdate' => $federalsdate,
            'federalsstatus' => $federalsstatus,
            'federalsfile' => $federalsfile,
            'federalsnote' => $federalsnote
        ));
        //$ids=$data->id;
        //echo $ids;die;

        if ($federalstax == 'Original') {

            $resetdata = DB::table('client_taxfederal')->where('federalsyear', $federalsyear)->where('client_id', $client_id)
                ->update([
                    'flag' => 1
                ]);

            $resetdata1 = DB::table('client_to_taxation')->where('taxyears', $federalsyear)->where('clientid', $client_id)
                ->update([
                    'flag' => 1
                ]);


        }

        $l = 0;
        if (empty($request->statetax)) {
        } else {
            foreach ($request->statetax as $typeofservice11) {
                $client_id1 = $request->client_id;
                $federal_id1 = $ids;
                $stateyear = $request->federalsyear;
                $statetax1 = $request->statetax[$l];
                $stateformno1 = $request->stateformno[$l];
                $statemethod1 = $request->statemethod[$l];
                $statedate1 = date('Y-m-d', strtotime($request->statedate[$l]));
                $fillingdate1 = $request->fillingdate[$l];
                $statestatus1 = $request->statestatus[$l];
                //$statefile1 = $request->statefile[$l];
                $statenote1 = $request->statenote[$l];

                $path1 = public_path() . '/adminupload/' . $_FILES['statefile']['name'][$l];
                if (isset($_FILES['statefile']['name'][$l]) != '') {
                    if (move_uploaded_file($_FILES['statefile']['tmp_name'][$l], $path1)) {
                        $state_copy = $_FILES['statefile']['name'][$l];
                    } else {
                        $state_copy = '';
                    }
                }
                $statefile1 = $state_copy;

                $l++;
                DB::insert("insert into client_taxstate(`statefile`,`client_id`,`federal_id`,`stateyear`,`statetax`,`stateformno`,`statemethod`,`statedate`,`statestatus`,`statenote`) 
                values('" . $statefile1 . "','" . $client_id1 . "','" . $federal_id1 . "','" . $stateyear . "','" . $statetax1 . "','" . $stateformno1 . "','" . $statemethod1 . "','" . $statedate1 . "','" . $statestatus1 . "','" . $statenote1 . "')");
            }
        }


        // $stateyear=$request->stateyear;
        // $statetax=$request->statetax;
        // $stateduedate=date('Y-m-d',strtotime($request->stateduedate));
        // $stateform=$request->stateform;
        // $statemethod=$request->statemethod;
        // $statedate=date('Y-m-d',strtotime($request->statedate));
        // $statestatus=$request->statestatus;
        // $statenote=$request->statenote;
        // $path1= public_path().'/adminupload/'.$_FILES['statefile']['name'];
        // if(isset($_FILES['statefile']['name'])!='')
        // {
        //     if(move_uploaded_file($_FILES['statefile']['tmp_name'], $path1)) 
        //     {
        //       $state_copy= $_FILES['statefile']['name']; 
        //     }
        //     else
        //     {
        //         $state_copy= ''; 
        //     }
        // }
        // $statefile=$state_copy;

        // $data=DB::table('client_taxstate')
        //     ->insert([
        //         'client_id' =>$client_id,
        //         'federal_id' =>$ids,
        //         'stateyear' =>$stateyear,
        //         'statetax' =>$statetax,
        //         'stateduedate' =>$stateduedate,
        //         'stateform' =>$stateform,
        //         'statemethod' =>$statemethod,
        //         'statedate' =>$statedate,
        //         'statestatus' =>$statestatus,
        //         'statefile' =>$statefile,
        //         'statenote' =>$statenote,
        //     ]);


        //  return redirect()->back()->with('success','Taxation Added Successfully');

        return redirect('fac-Bhavesh-0554/workstatus?id=' . $client_id . '&&action=tax')->with('success', 'Client added successfully');


    }


    public function storefederalupdate(Request $request)
    {
        //print_r($_POST);EXIT;
        $federalid = $request->federalid;
        //$client_id=$request->client_id;
        $federalsyear = $request->federalsyear;
        $federalstax = $request->federalstax;
        $federalsduedate = date('Y-m-d', strtotime($request->federalsduedate));
        $federalsform = $request->federalsform;
        $federalsmethod = $request->federalsmethod;
        $federalsdate = date('Y-m-d', strtotime($request->federalsdate));
        $federalsstatus = $request->federalsstatus;
        $federalsnote = $request->federalsnote;
        $path1 = public_path() . '/adminupload/' . $_FILES['federalsfile']['name'];
        if (isset($_FILES['federalsfile']['name']) != '') {
            if (move_uploaded_file($_FILES['federalsfile']['tmp_name'], $path1)) {
                $federal_copy = $_FILES['federalsfile']['name'];
            } else {
                $federal_copy = $_POST['federalsfile'];
            }
        } else {
            $federal_copy = $_POST['federalsfile'];
        }

        $federalsfile = $federal_copy;

        $ids = DB::table('client_taxfederal')->where('id', $federalid)
            ->update([
                //'client_id' =>$client_id,
                'federalsyear' => $federalsyear,
                'federalstax' => $federalstax,
                'federalsduedate' => $federalsduedate,
                'federalsform' => $federalsform,
                'federalsmethod' => $federalsmethod,
                'federalsdate' => $federalsdate,
                'federalsstatus' => $federalsstatus,
                'federalsfile' => $federalsfile,
                'federalsnote' => $federalsnote,
            ]);


        return redirect()->back()->with('success', 'Taxation Added Successfully');

    }


    public function stateupdate(Request $request)
    {
        $stateid = $request->stateid;
        $stateyear = $request->stateyear;
        $statetax = $request->statetax;
        $federalsduedate = date('Y-m-d', strtotime($request->federalsduedate));
        $stateformno = $request->stateformno;
        $statemethod = $request->statemethod;
        $statedate = date('Y-m-d', strtotime($request->statedate));
        $statestatus = $request->statestatus;
        $statenote = $request->statenote;
        $path1 = public_path() . '/adminupload/' . $_FILES['statefile']['name'];
        if (isset($_FILES['statefile']['name']) != '') {
            if (move_uploaded_file($_FILES['statefile']['tmp_name'], $path1)) {
                $statefile_copy = $_FILES['statefile']['name'];
            } else {
                $statefile_copy = $_POST['statefile_1'];
            }
        } else {
            $statefile_copy = $_POST['statefile_1'];
        }

        $statefile = $statefile_copy;

        $ids = DB::table('client_taxstate')->where('id', $stateid)
            ->update([
                //'client_id' =>$client_id,
                //'stateyear' =>$federalsyear,
                'statetax' => $statetax,
                //'federalsduedate' =>$federalsduedate,
                'stateformno' => $stateformno,
                'statemethod' => $statemethod,
                'statedate' => $statedate,
                'statestatus' => $statestatus,
                'statefile' => $statefile,
                'statenote' => $statenote,
            ]);


        return redirect()->back()->with('success', 'Taxation Added Successfully');

    }


    public function store(Request $request)
    {

        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request, [
            // 'license_year' =>'required',
            // 'license_gross' =>'required',
            // 'license_fee' =>'required',
            // 'license_status' =>'required',
            // 'license_copy	' =>'required', 
            // 'license_no' =>'required', 
            // 'website_link' =>'required', 
            // 'license_renew_date' =>'required',                   
        ]);

        $client_id = $request->client_id;
        $license_year = $request->license_year;
        $license_gross = $request->license_gross;
        $license_fee = $request->license_fee;
        $license_status = $request->license_status;
        // $license_copy=$request->license_copy;

        //public
        $path1 = public_path() . '/clientupload/' . $_FILES['license_copy']['name'];


        if (isset($_FILES['license_copy']['name']) != '') {
            if (move_uploaded_file($_FILES['license_copy']['tmp_name'], $path1)) {
                $license_copy = $_FILES['license_copy']['name'];
            }
        }

        //$license_copy=$request->license_copy;
        $license_no = $request->license_no;
        //  $website_link=$request->website_link;
        $license_note = $request->license_note;

        $license_renew_date = $request->license_renew_date;

        DB::insert("insert into client_license(`client_id`,`license_year`,`license_gross`,`license_fee`,`license_status`,`license_copy`,`license_no`,`license_note`,`license_renew_date`) 
         values('$client_id','$license_year','$license_gross','$license_fee','$license_status','$license_copy','$license_no','$license_note','$license_renew_date')");
        return redirect('fac-Bhavesh-0554/workrecord')->with('success', 'License Added Successfully');
    }

    public function show($id)
    {
        $common = Commonregister::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.workrecord.workrecord', compact(['common']));
    }


    public function documents()
    {

        $newopt = Input::get('newopt');
        $filename_id = Input::get('filename_id');
        $client_id = Input::get('client_ids');

        if (DB::table('clientdocument')->where('documentname', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = DB::insert("insert into clientdocument(`filename_id`,`client_id`,`documentname`)values('$filename_id','$client_id','$newopt')");
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }


    public function updatedocument(Request $request)
    {
        // print_r($_REQUEST);
        // print_r($_POST);die;
        $filename_id = $request->filename_id1;
        $client_id = $request->client_id1;
        $documentsname = $request->documentsname;
        $path1 = public_path() . '/clientupload/' . $_FILES['clientdocument']['name'];
        if (isset($_FILES['clientdocument']['name']) != '') {
            if (move_uploaded_file($_FILES['clientdocument']['tmp_name'], $path1)) {
                $filesname1 = $_FILES['clientdocument']['name'];
            }


        }

        $insert2 = DB::insert("insert into clienttodocument(filename_id,client_id,documentsname,clientdocument)
                                                        values('$filename_id','$client_id','$documentsname','$filesname1')");
        return redirect(route('fac-Bhavesh-0554/workrecord'))->with('success', 'Client Document Successfully Updated');
    }

    public function updatedocuments(Request $request)
    {
        // print_r($_REQUEST);die;
        // print_r($_POST);die;

        $idss = $request->idaa;
        $filename_id = $request->filename_idss;
        $client_id = $request->client_id;
        $documentsname = $request->documentsname;
        $path1 = public_path() . '/clientupload/' . $_FILES['clientdocument']['name'];
        if (isset($_FILES['clientdocument']['name']) != '') {
            if (move_uploaded_file($_FILES['clientdocument']['tmp_name'], $path1)) {
                $filesname1 = $_FILES['clientdocument']['name'];
            } else {
                $filesname1 = $_POST['clientdocument_1'];
            }

        } else {
            $filesname1 = $_POST['clientdocument_1'];
        }

        $returnValue = DB::table('clienttodocument')->where('id', '=', $idss)
            ->update([
                'filename_id' => $filename_id,
                'client_id' => $client_id,
                'documentsname' => $documentsname,
                'clientdocument' => $filesname1
            ]);

        return redirect(route('fac-Bhavesh-0554/workrecord'))->with('success', 'Your Document Successfully Updated');
    }


    public function getclientdocument(Request $request)
    {
        $documentRow = DB::table('clienttodocument')->where('id', $request->documentid)->first();
        return response()->json($documentRow);
    }

    public function getClientfederaldata(Request $request)
    {
        $documentRow = DB::table('client_taxfederal')->where('id', $request->federalid)->first();
        // $documentRow = DB::table('client_taxfederal as t1')->select('t1.id','t1.client_id','t1.federalsyear','t1.federalstax','t1.federalsduedate','t1.federalsform','t1.federalsmethod','t1.federalsdate','t1.federalsstatus','t1.federalsfile','t1.federalsnote','t2.id as cid','t2.federal_id as federal_id2','t2.statetax as statetax2')
        //     ->leftJoin('client_taxstate as t2', function($join){ $join->on('t2.federal_id', '=', 't1.id');})
        //     ->where('t1.id',$request->federalid)->first();
        return response()->json($documentRow);
    }


    public function getClientfederalyear(Request $request)
    {
        // exit('111');


        $documentRow2 = array();
        $documentRow1 = DB::table('client_taxfederal')->where('client_id', $request->id)->where('federalsyear', $request->year)->count();
        //echo $documentRow1;
        if ($documentRow1 > 0 && $documentRow1 == 1) {
            $documentRow2 = DB::table('client_taxfederal')->where('client_id', $request->id)->where('federalsyear', $request->year)->first();
            //echo $documentRow2->federalstax;

            //echo 'Extension';
        }
        echo $documentRow2->federalstax;
        //return response()->json($documentRow2->federalstax);
    }


    public function getIncomedata(Request $request)
    {
        $documentRow = DB::table('incomes')->where('id', $request->incomeid)->first();
        return response()->json($documentRow);
    }

    public function getClientstatedata(Request $request)
    {
        $documentRow = DB::table('client_taxstate')->where('id', $request->stateid)->first();
        return response()->json($documentRow);
    }


    public function destroydocument(Request $request, $id)
    {
        DB::table('clienttodocument')->where('id', '=', $id)->delete();
        return redirect(route('fac-Bhavesh-0554/workrecord'))->with('success', 'Client Document Successfully Deleted');
    }

    public function destroydocumenttitle(Request $request, $id)
    {
        DB::table('clientdocument')->where('id', '=', $id)->delete();
        return redirect(route('fac-Bhavesh-0554/workrecord'));
        //return redirect(route('fac-Bhavesh-0554/workrecord'))->with('success','Client Document Title Successfully Deleted');
    }


    public function updatetaxation()
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        if (isset($_POST['filing_year']) != '') {
            $filing_year = $_POST['filing_year'];
        } else {
            $filing_year = $_POST['filing_year2'];
        }

        if (isset($_POST['filing_method']) != '') {
            $filing_method = $_POST['filing_method'];
        } else {
            $filing_method = $_POST['filing_method2'];
        }
        if (isset($_POST['personal_taxid']) != '') {


            $ids = $_POST['personal_taxid'];
            $idmm = DB::table('clienttopersonaltax')
                ->where('personal_taxid', $ids)
                ->update([
                    'filing_type' => $_POST['filing_type'],
                    'filing_year' => $filing_year,
                    'filing_method' => $filing_method,
                    'filing_date' => $_POST['filing_date'],
                    'filing_software' => $_POST['filing_software'],
                    'filing_state' => $_POST['filing_state'],
                    'date_of_filing' => $_POST['date_of_filing'],
                ]);
        } else {
            // if(isset($_POST['filing_year'])!='')
            // {
            //     $filing_year=$_POST['filing_year'];
            // }
            // else
            // {
            //     $filing_year=$_POST['filing_year2'];
            // }

            // if(isset($_POST['filing_method'])!='')
            // {
            //     $filing_method=$_POST['filing_method'];
            // }
            // else
            // {
            //     $filing_method=$_POST['filing_method2'];
            // }   

            $idmm = DB::table('clienttopersonaltax')
                ->insert([

                    'client_id' => $_POST['client_id'],
                    'filing_type' => $_POST['filing_type'],
                    'filing_year' => $filing_year,
                    'filing_method' => $filing_method,
                    'filing_date' => $_POST['filing_date'],
                    'filing_software' => $_POST['filing_software'],
                    'filing_state' => $_POST['filing_state'],
                    'date_of_filing' => $_POST['date_of_filing'],
                ]);
        }


        return redirect('fac-Bhavesh-0554/workrecord')->with('success', 'Worktodo Update Successfully');

    }


    public function update(Request $request, $id)
    {
        $update = ['annualfees' => $request->annualfees, 'processingfees' => $request->processingfees];
        Commonregister::where('id', $id)->update($update);
        return Redirect::to('workrecord')->with('success', 'Workrecord updated successfully');
    }


    public function getcounty(Request $request)
    {
        $data = taxstate::select('county', 'id', 'countycode')->where('state', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode', 'id')->where('county', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function destroyfederal(Request $request, $id)
    {
        $documentRow = DB::table('client_taxfederal')->where('id', $id)->first();
        $ids = $documentRow->client_id;
        DB::table('commonregisters')->where('id', $ids)
            ->update([
                'federalstax' => '',
                'federalsduedate' => ''
            ]);

        DB::table('client_to_taxation')->where('clientid', $ids)
            ->update([
                'expiredate' => '2020-07-15'
            ]);

        DB::table('client_taxfederal')->where('id', '=', $id)->delete();
        DB::table('client_taxstate')->where('federal_id', '=', $id)->delete();
        return redirect()->back()->with('success', 'Your Record Deleted Successfully ');
    }

    public function destroystate(Request $request, $id)
    {
        DB::table('client_taxstate')->where('id', '=', $id)->delete();
        return redirect()->back()->with('success', 'Your Record Deleted Successfully ');
    }


    // public function destroy(City $city)
    // {
    //     $city->delete();
    //     return redirect(route('workrecord.index'));
    // }
}