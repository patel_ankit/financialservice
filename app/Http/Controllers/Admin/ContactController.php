<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::All();
        return view('fac-Bhavesh-0554/contact/contact', compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/contact/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required',
            'email' => 'required',
            'placename' => 'required',
            'phone' => 'required',
            'fax' => 'required',
        ]);
        $category = new Contact;
        $category->latitude = $request->latitude;
        $category->longitude = $request->longitude;
        $category->email = $request->email;
        $category->placename = $request->placename;
        $category->phone = $request->phone;
        $category->fax = $request->fax;
        $category->save();
        return redirect(route('contact.index'))->with('success', 'Your Information Successfully Add');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return View('fac-Bhavesh-0554.contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required',
            'email' => 'required',
            'placename' => 'required',
            'phone' => 'required',
            'fax' => 'required',
        ]);
        $category = $contact;
        $category->latitude = $request->latitude;
        $category->longitude = $request->longitude;
        $category->email = $request->email;
        $category->placename = $request->placename;
        $category->phone = $request->phone;
        $category->fax = $request->fax;
        $category->update();
        return redirect(route('contact.index'))->with('success', 'Your Information Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect(route('contact.index'));
    }
}