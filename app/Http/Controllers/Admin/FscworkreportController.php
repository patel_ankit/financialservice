<?php

namespace App\Http\Controllers\Admin;

use App\Front\Commonregister;
use App\Http\Controllers\Controller;
use App\Model\Business;
use App\Model\Category;
use App\Model\Logo;
use App\Model\Period;
use App\Model\Taxtitle;
use DB;
use Illuminate\Http\Request;
use Response;


class FscworkreportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $states = DB::table('commonregisters')->select('stateid')->where('stateid', '!=', null)->distinct()->orderBy('stateid', 'ASC')->get();
        //echo "<pre>";
        //print_r($states);die;
        $logo = Logo::where('id', '=', 1)->first();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        $taxtitle = Taxtitle::orderBy('title', 'asc')->get();

        $category = Category::orderBy('business_cat_name', 'asc')->get();
        $datastate = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();
        $datastate2 = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();


        $period = Period::All();
        return view('fac-Bhavesh-0554.fscworkreport/fscworkreport', compact(['taxtitle', 'datastate2', 'datastate', 'states', 'business', 'category', 'logo', 'period']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $states = DB::table('commonregisters')->select('stateid')->where('stateid', '!=', null)->distinct()->orderBy('stateid', 'ASC')->get();
        //echo "<pre>";
        //print_r($states);die;
        //  print_r($_POST);
        $taxtitle = Taxtitle::orderBy('title', 'asc')->get();

        $logo = Logo::where('id', '=', 1)->first();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        $category = Category::orderBy('business_cat_name', 'asc')->get();
        $datastate = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();
        $datastate2 = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();


        /*echo '11'.$request->formation_status;
        echo "<br>";
        echo $request->tax_subcategory;
        echo "<br>";
        echo $request->subcategory;
        echo "<br>";
        echo $request->mailing_year;
        echo "<br>";
        echo $request->status;
        echo "<br>";*/
        //  exit;
        //if($request->formation_status =='Taxation'

        if ($request->formation_status == 'Taxation'
            && $request->mailing_year != '' && $request->status == '0' && $request->fillstatus == 'Extension' && $request->taxation_type == '6' && $request->federalsstatus == '') {
            $client = DB::table("client_to_taxation as t1")->select('t2.formation_type_of_entity', 't2.typeofcorp1', "t4.federalsyear", "t4.federalsmethod", "t4.federalsdate",
                "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName",
                "t3.middleName", "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name",
                "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate",
                "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->
                where('t1.taxation_service', '=', $request->taxation_type)->where('t4.federalsyear', '=', $request->mailing_year)
                ->where('t4.federalstax', '=', $request->fillstatus)
                ->where('t2.personalstatus', '=', $request->status)->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        } else if ($request->formation_status == 'Taxation'
            && $request->mailing_year != '' && $request->status == 'Original' && $request->taxation_type == '6') {
            //exit('12222');
            $client = DB::table("client_to_taxation as t1")->select('t2.formation_type_of_entity', 't2.typeofcorp1', "t4.federalsyear", "t4.federalsmethod", "t4.federalsdate",
                "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName",
                "t3.middleName", "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name",
                "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate",
                "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->
                where('t1.taxation_service', '=', $request->taxation_type)->where('t4.federalsyear', '=', $request->mailing_year)
                ->where('t4.federalstax', '=', $request->status)
                ->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        } else if ($request->formation_status == 'Taxation'
            && $request->mailing_year != '' && $request->status == 'Original' && $request->taxation_type == '6') {
            //exit('12222');
            // echo 'TEST';exit;

            $client = DB::table("client_to_taxation as t1")->select('t2.formation_type_of_entity', 't2.typeofcorp1', "t4.federalsyear", "t4.federalsmethod", "t4.federalsdate",
                "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName",
                "t3.middleName", "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name",
                "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate",
                "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->
                where('t1.taxation_service', '=', $request->taxation_type)->where('t4.federalsyear', '=', $request->mailing_year)
                ->where('t4.federalstax', '=', $request->status)
                ->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        } else if ($request->formation_status == 'Taxation' && $request->mailing_year != '' && $request->status == '0' && $request->fillstatus == 'Extension'
            && $request->taxation_type == '6' && $request->federalsstatus != '' && $request->federalsmethod == '') {
            //exit('1222222222222222222222222');
            // echo $request->federalsstatus;exit;
            $client = DB::table("client_to_taxation as t1")->select('t2.formation_type_of_entity', 't2.typeofcorp1', "t4.federalsyear", "t4.federalsmethod", "t4.federalsdate",
                "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName",
                "t3.middleName", "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name",
                "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate",
                "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->
                where('t1.taxation_service', '=', $request->taxation_type)->where('t4.federalsyear', '=', $request->mailing_year)
                ->where('t4.federalstax', '=', $request->fillstatus)->where('t4.federalsstatus', '=', $request->federalsstatus)
                ->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        } else if ($request->formation_status == 'Taxation' && $request->mailing_year != '' && $request->status == '0' && $request->fillstatus == 'Extension'
            && $request->taxation_type == '6' && $request->federalsmethod != '') {
            exit('12222');
            // echo $request->federalsstatus;exit;
            $client = DB::table("client_to_taxation as t1")->select('t2.formation_type_of_entity', 't2.typeofcorp1', "t4.federalsyear", "t4.federalsmethod", "t4.federalsdate",
                "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName",
                "t3.middleName", "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name",
                "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate",
                "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t4.federalsmethods', '=', $request->federalsmethod)->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->
                where('t1.taxation_servicess', '=', $request->taxation_type)->where('t4.federalsyear', '=', $request->mailing_year)
                ->where('t4.federalstax', '=', $request->fillstatus)->where('t4.federalsstatus', '=', $request->federalsstatus)
                ->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        } else if ($request->formation_status == 'Taxation' && $request->taxation_type == '7'
            && $request->mailing_year != '' && $request->status != '') {
            $client = DB::table("client_to_taxation as t1")->select("t2.formation_type_of_entity", "t2.typeofcorp1", "t4.federalstax", "t4.federalsmethod", "t4.federalsdate", "t4.federalsstatus", "t2.business_no", "t2.email", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName",
                "t3.lastName", "t3.employee_id", "t2.federalstax", "t2.etelephone1", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t2.filename", "t2.company_name", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
                ->leftJoin('commonregisters as t2', function ($join) {
                    $join->on('t2.id', '=', 't1.clientid');
                })
                ->leftJoin('employees as t3', function ($join) {
                    $join->on('t3.id', '=', 't1.employee_id');
                })
                ->leftJoin('client_taxfederal as t4', function ($join) {
                    $join->on('t4.client_id', '=', 't1.clientid');
                })
                ->where('t2.business_id', '=', '6')->where('t2.status', '=', 'Active')->
                orWhereNull('t4.federalstax')
                ->where('t1.taxyears', '=', $request->mailing_year)->where('t1.taxation_service', '=', $request->taxation_type)->where('t4.federalstax', '=', $request->status)->groupBy('t2.filename')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

            // echo "<pre>"; print_r($client);
        } else if ($request->formation_status == 'Corporation' && $request->mailing_state == 'ALL' && $request->status == 'ALL') {
            //EXIT('111');
            $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_id', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                'client_formation.formation_yearvalue', 'client_formation.record_status')
                ->leftJoin('client_formation', function ($join) {
                    $join->on('commonregisters.id', '=', 'client_formation.client_id');
                })
                ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.business_id', '!=', '6')->
                orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

        } else if ($request->formation_status == 'Corporation' && $request->mailing_state != 'ALL' && $request->status == 'ALL') {
            //EXIT('111');
            $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_id', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                'client_formation.formation_yearvalue', 'client_formation.record_status')
                ->leftJoin('client_formation', function ($join) {
                    $join->on('commonregisters.id', '=', 'client_formation.client_id');
                })
                ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.business_id', '!=', '6')->where('commonregisters.stateid', '=', $request->mailing_state)->
                orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

        } else if ($request->formation_status == 'Corporation' && $request->mailing_state == 'ALL' && $request->status != 'ALL') {
            //EXIT('111');
            $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_id', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                'client_formation.formation_yearvalue', 'client_formation.record_status')
                ->leftJoin('client_formation', function ($join) {
                    $join->on('commonregisters.id', '=', 'client_formation.client_id');
                })
                ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.business_id', '!=', '6')->where('client_formation.record_status', '=', $request->status)->
                orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

        } else if ($request->formation_status == 'Corporation' && $request->mailing_state != 'ALL' && $request->status != 'ALL') {
            //EXIT('111');
            $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_id', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                'client_formation.formation_yearvalue', 'client_formation.record_status')
                ->leftJoin('client_formation', function ($join) {
                    $join->on('commonregisters.id', '=', 'client_formation.client_id');
                })
                ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.business_id', '!=', '6')->where('client_formation.record_status', '=', $request->status)->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->
                orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

        } else {
            // exit('121213');
            if ($request->mailing_state == 'ALL' && $request->mailing_client == 'ALL' && $request->status == 'ALL' && $request->mailing_year == '') {

                $client = Commonregister::where('business_id', '=', $request->emp_name)->where('service_period', '=', $request->monthlytype)->get();

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);


            } else if ($request->mailing_year == '') {


                $client = Commonregister::where('business_id', '=', $request->emp_name)->where('service_period', '=', $request->monthlytype)->get();

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->where('client_formation.record_status', '=', $request->status)->orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);


            } else if ($request->mailing_state != 'ALL' && $request->mailing_client != 'ALL' && $request->status != 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })->
                    whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.stateid', '=', $request->mailing_state)
                    ->where('commonregisters.status', '=', $request->mailing_client)->where('client_formation.record_status', '=', $request->status)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state != 'ALL' && $request->mailing_client == 'ALL' && $request->status == 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.stateid', '=', $request->mailing_state)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state != 'ALL' && $request->mailing_client != 'ALL' && $request->status == 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->
                    where('commonregisters.stateid', '=', $request->mailing_state)->
                    where('commonregisters.status', '=', $request->mailing_client)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state != 'ALL' && $request->mailing_client == 'ALL' && $request->status != 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.stateid', '=', $request->mailing_state)->where('client_formation.record_status', '=', $request->status)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state == 'ALL' && $request->mailing_client != 'ALL' && $request->status == 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.status', '=', $request->mailing_client)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state == 'ALL' && $request->mailing_client != 'ALL' && $request->status != 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('commonregisters.status', '=', $request->mailing_client)->where('client_formation.record_status', '=', $request->status)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else if ($request->mailing_state == 'ALL' && $request->mailing_client == 'ALL' && $request->status != 'ALL') {

                $client = DB::table('commonregisters')->select('commonregisters.typeofcorp1', 'commonregisters.etelephone1', 'commonregisters.id as ids', 'commonregisters.business_no', 'commonregisters.email', 'commonregisters.filename', 'commonregisters.company_name', 'commonregisters.id', 'client_formation.client_id',
                    'client_formation.formation_yearvalue', 'client_formation.record_status')
                    ->leftJoin('client_formation', function ($join) {
                        $join->on('commonregisters.id', '=', 'client_formation.client_id');
                    })
                    ->whereRaw('FIND_IN_SET("' . $request->mailing_year . '",client_formation.formation_yearvalue)')->where('client_formation.record_status', '=', $request->status)->
                    orderBy('commonregisters.filename', 'asc')->get();
//print_r($client);

            } else {
                //  print_r($_POST);
                $client = Commonregister::where('status', '=', $request->status)->where('business_id', '=', $request->emp_name)->where('service_period', '=', $request->monthlytype)->get();
            }
        }
        return view('fac-Bhavesh-0554.fscworkreport/fscworkreport', compact(['taxtitle', 'datastate2', 'datastate', 'client', 'states', 'logo', 'category', 'business']));
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}