<?php

namespace App\Http\Controllers\Admin;

use App\employee\Empschedule;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class ClientreportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'clientemployee')->get();
        $start = date('Y-m-d', strtotime($request->startdate));
        $end = date('Y-m-d', strtotime($request->enddate));
        $thisdate = date('Y-m-d', strtotime($start));
        $thismonday = date('Y-m-d', strtotime('monday this week', strtotime($thisdate)));
        $thisfriday = date('Y-m-d', strtotime('next sunday', strtotime($thisdate)));
        $nextmonday = date('Y-m-d', strtotime('+1 day', strtotime($thisfriday)));
        $nextfriday = date('Y-m-d', strtotime('+6 day', strtotime($nextmonday)));
        $user_id = $request->emp_name;
        $employee2 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second')->whereBetween('emp_in_date', array($thismonday, $thisfriday))->get();
        $employee3 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second')->whereBetween('emp_in_date', array($nextmonday, $nextfriday))->get();
        $employee1 = DB::Table('empschedules')->where('employee_id', '=', $user_id)->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,work_note,launch_out_second,launch_in_second')->whereBetween('emp_in_date', array($start, $end))->get();
        return view('fac-Bhavesh-0554.clientreport/clientreport', compact(['emp', 'employee1', 'employee2', 'employee3']));
    }


    public function report()
    {
        return Response::json($employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'employee')->get();
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d', strtotime($request->date));
        $clock = Input::get('clockin') . ':' . Input::get('clockin_second') . ':' . '00';
        $clockout1 = Input::get('clockout') . ':' . Input::get('clockout_second') . ':' . '00';
        $lunch = $request->lunchin . ':' . $request->lunchin_second . ':' . '00';
        $lunchout = $request->lunchout . ':' . $request->lunchout_second . ':' . '00';
        $branch = new Empschedule;
        $branch->employee_id = $request->fullname;
        $branch->emp_in_date = $date;
        $branch->emp_in = $date . ' ' . $clock;
        $branch->emp_out = $date . ' ' . $clockout1;
        $branch->work_note = $request->note;
        if ($request->lunchin == null) {
            $branch->launch_in = '';
            $branch->launch_out = '';
        } else {
            $branch->launch_in = $date . ' ' . $lunch;
            $branch->launch_out = $date . ' ' . $lunchout;
        }
        $branch->ip_address = $ip;
        $branch->save();
        if ($branch->save()) {
            return response()->json([
                'status' => 'success',
                'newopt' => $clock]);
        } else {
            return response()->json([
                'status' => 'error']);
        }

        //return view('fac-Bhavesh-0554.fscemployeereport/fscemployeereport',compact(['emp']));
    }

    public function reportedit1(Request $request)
    {
        $student = Empschedule::find($request->input('id'));


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Empschedule::find($id);
        return view('fac-Bhavesh-0554.fscemployeereport.edit', compact(['student']));
    }

    public function reportremove1(Request $request)
    {
        $student = Empschedule::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp = Employee::where('check', '=', 1)->where('type', '=', 'employee')->get();
        $branch = Empschedule::find($id);
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d', strtotime($request->date));
        //   $branch->employee_id= $request->fullname;
        $clock = $request->clockin . ':' . $request->clockin_second . ':' . '00';
        //  $clockin = date("H:i:s", strtotime($clock));
        $clock1 = $request->clockout . ':' . $request->clockout_second . ':' . '00';
        // $clockout = date("H:i:s", strtotime($clock1));
        $lunch = $request->lunchin . ':' . $request->lunchin_second . ':' . '00';
        // $lunchin = date("H:i:s", strtotime($lunch));
        $lunchout = $request->lunchout . ':' . $request->lunchout_second . ':' . '00';
        //  $lunchout1 = date("H:i:s", strtotime($lunchout));
        $branch->emp_in_date = $date;
        $branch->emp_in = $date . ' ' . $clock;
        $branch->emp_out = $date . ' ' . $clock1;
        $branch->work_note = $request->note;
        if ($request->lunchin == null) {
            $branch->launch_in = '';
            $branch->launch_out = '';
        } else {
            $branch->launch_in = $date . ' ' . $lunch;
            $branch->launch_out = $date . ' ' . $lunchout;
        }
        $branch->ip_address = $ip;
        $branch->update();
        if ($branch->update()) {
            return response()->json([
                'status' => 'success',
                'newopt' => $clock]);
        } else {
            return response()->json([
                'status' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}