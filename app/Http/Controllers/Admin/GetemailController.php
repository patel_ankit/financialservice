<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Email;
use App\Model\Employee;
use DB;
use Illuminate\Http\Request;

class GetemailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Email::where('for_whom', '=', '1')->get();
        $emp = Employee::All();
        return view('fac-Bhavesh-0554/getemail/getemail', compact(['emp', 'email']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp1 = Employee::where('check', '=', 1)->get();
        return view('fac-Bhavesh-0554/getemail/create', compact(['emp1']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'for_whom' => 'required',
            'email' => 'required',
            'access_address' => 'required',
            'password' => 'required',
        ]);
        $branch = new Email;
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->save();
        return redirect('fac-Bhavesh-0554/getemail')->with('success', 'Success fully add Email');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();
        $emp = Employee::All();
        $leave = Email::where('id', $id)->first();
        return view('fac-Bhavesh-0554/getemail.edit', compact(['leave', 'emp']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Email::find($id);
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->update();
        return redirect('fac-Bhavesh-0554/getemail')->with('success', 'Success fully Update Email');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Email::where('id', $id)->delete();
        return redirect(route('fac-Bhavesh-0554/getemail'));
    }
}