<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Slider;
use Illuminate\Http\Request;
use Validator;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::All();
        return view('fac-Bhavesh-0554.slider.slider', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'slider_name' => 'required|unique:sliders,slider_name',
            'slider_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|unique:sliders,slider_image',
        ]);
        if ($request->hasFile('slider_image')) {
            $filname = $request->slider_image->getClientOriginalName();
            $request->slider_image->move('public/slider', $filname);
        }
        $category = new Slider;
        $category->slider_name = $request->slider_name;
        $category->slider_image = $filname;
        $category->save();
        return redirect(route('slider.index'))->with('success', 'Slider image Successfully Add');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return View('fac-Bhavesh-0554.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $this->validate($request, [
            'slider_name' => 'required',
        ]);
        if ($request->hasFile('slider_image')) {
            $filname = $request->slider_image->getClientOriginalName();
            $request->slider_image->move('public/slider', $filname);
        } else {
            $filname = $request->slider_image1;
        }
        $category = $slider;
        $category->slider_name = $request->slider_name;
        $category->slider_image = $filname;
        $category->save();
        return redirect(route('slider.index'))->with('success', 'Slider image Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
        return redirect(route('slider.index'));
    }
}