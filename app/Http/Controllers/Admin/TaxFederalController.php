<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\taxfederal;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TaxFederalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = taxfederal::All();
        return view('fac-Bhavesh-0554/taxfederal/taxfederal', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = taxfederal::All();
        $entity = DB::table('typeofentity')->get();
        return view('fac-Bhavesh-0554/taxfederal/create', compact(['position', 'entity']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($_POST['filing_frequency_form1']);die;
        $position = new taxfederal;
        $position->short_name = $request->short_name;
        //$position->filling_frequency= $request->filling_frequency;
        $position->yearlytype = $request->yearlytype;
        $position->authority_name = $request->authority_name;
        $position->typeofform = $request->typeofform;
        $position->formname = $request->formname;
        $position->due_date = $request->duedate;
        $position->extension_due_date = $request->extdate;
        $position->authority_level = $request->authority_level;
        $position->authority_name1 = $request->authority_name1;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $position->payroll_name = $request->payroll_name;
        $position->renewlink = $request->renewlink;
        $position->uniques = $request->type;
        $position->payroll_department_name = $request->payroll_department_name;
        $position->payroll_link = $request->payroll_link;
        $position->payroll_telephone = $request->payroll_telephone;
        $position->payroll = $request->payroll;
        $position->save();
        $id = $position->id;

        $fil = 0;
        foreach ($request->filing_frequency_form as $filing_frequency_form) {
            $filing_frequency_form = $request->filing_frequency_form[$fil];
            $filing_frequency_name = $request->filing_frequency_name[$fil];
            $filing_frequency = $request->filing_frequency[$fil];
            $filing_frequency_due_date = $request->filing_frequency_due_date[$fil];
            $id;
            $fil++;
            $insert2 = DB::insert("insert into filingfrequencies(`filing_frequency_form`,`filing_frequency_name`,`filing_frequency`,`filing_frequency_due_date`,`t_id`) values('" . $filing_frequency_form . "','" . $filing_frequency_name . "','" . $filing_frequency . "','" . $filing_frequency_due_date . "','" . $id . "')");
        }

        $pay = 0;
        foreach ($request->filing_frequency_form1 as $filing_frequency_form1) {
            $filing_frequency_form1 = $request->filing_frequency_form1[$pay];
            $filing_frequency_name1 = $request->filing_frequency_name1[$pay];
            $filing_frequency1 = $request->filing_frequency1[$pay];
            $filing_frequency_due_date1 = $request->filing_frequency_due_date1[$pay];
            $id;
            $pay++;
            $insert3 = DB::insert("insert into paymentfrequency(`filing_frequency_form1`,`filing_frequency_name1`,`filing_frequency1`,`filing_frequency_due_date1`,`t_id`) values('" . $filing_frequency_form1 . "','" . $filing_frequency_name1 . "','" . $filing_frequency1 . "','" . $filing_frequency_due_date1 . "','" . $id . "')");
        }

        return redirect('fac-Bhavesh-0554/taxfederal')->with('success', 'Success fully add Federal');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function show(Federal $federal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $entity = DB::table('typeofentity')->get();
        $bussiness = taxfederal::where('id', $id)->first();
        $fills = DB::table('filingfrequencies')->where('t_id', '=', $id)->get();
        $fillsCount = $fills->count();
        $pay = DB::table('paymentfrequency')->where('t_id', '=', $id)->get();
        $payCount = $pay->count();
        //print_r($payCount);die;
        return View('fac-Bhavesh-0554.taxfederal.edit', compact(['bussiness', 'fills', 'pay', 'entity', 'payCount', 'fillsCount']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = taxfederal::find($id);
        $position->short_name = $request->short_name;
        $position->yearlytype = $request->yearlytype;
        //$position->filling_frequency=$request->filling_frequency;
        $position->authority_name = $request->authority_name;
        $position->typeofform = $request->typeofform;
        $position->formname = $request->formname;
        $position->due_date = $request->duedate;
        $position->extension_due_date = $request->extdate;
        $position->authority_level = $request->authority_level;
        $position->authority_name1 = $request->authority_name1;
        $position->website = $request->website;
        $position->telephone = $request->telephone;
        $position->payroll_name = $request->payroll_name;
        $position->renewlink = $request->renewlink;
        $position->uniques = $request->type;
        $position->payroll_department_name = $request->payroll_department_name;
        $position->payroll_link = $request->payroll_link;
        $position->payroll_telephone = $request->payroll_telephone;
        $fedrelid = $position->id;


        $fil = 0;
        foreach ($request->filing_frequency_form as $filing_frequency_form) {
            // echo "<pre>";
            //  print_r( $_POST);

            $filing_frequency_form = $request->filing_frequency_form[$fil];
            $filing_frequency_name = $request->filing_frequency_name[$fil];
            $filing_frequency = $request->filing_frequency[$fil];
            $filing_frequency_due_date = $request->filing_frequency_due_date[$fil];
            $fedrelid;
            $fil++;

            // DB::table('filingfrequencies')->where('t_id','=',$fedrelid)->delete();
            $insert2 = DB::insert("insert into filingfrequencies(`filing_frequency_form`,`filing_frequency_name`,`filing_frequency`,`filing_frequency_due_date`,`t_id`) 
            values('" . $filing_frequency_form . "','" . $filing_frequency_name . "','" . $filing_frequency . "','" . $filing_frequency_due_date . "','" . $fedrelid . "')");
        }
        die;

        // $pay=0;
        // foreach($request->filing_frequency_form1 as $filing_frequency_form1)
        // {
        //     //$pid =$request->pid[$pay];
        //     $filing_frequency_form1 =$request->filing_frequency_form1[$pay];
        //     $filing_frequency_name1 =$request->filing_frequency_name1[$pay];
        //     $filing_frequency1 =$request->filing_frequency1[$pay];
        //     $filing_frequency_due_date1 =$request->filing_frequency_due_date1[$pay];
        //     $fedrelid;
        //     $pay++;
        //     DB::table('paymentfrequency')->where('t_id','=',$fedrelid)->delete();
        //     $insert3 = DB::insert("insert into paymentfrequency(`filing_frequency_form1`,`filing_frequency_name1`,`filing_frequency1`,`filing_frequency_due_date1`,`t_id`) values('".$filing_frequency_form1."','".$filing_frequency_name1."','".$filing_frequency1."','".$filing_frequency_due_date1."','".$fedrelid."')");
        // } 


        // $fil=0;
        // foreach($request->filing_frequency_form as $filing_frequency_form)
        // {
        //     $fid=$request->fid;
        //     if($request->fid!='')
        //     {
        //         $fid =$request->fid;
        //         $filing_frequency_form =$request->filing_frequency_form[$fil];
        //         $filing_frequency_name =$request->filing_frequency_name[$fil];
        //         $filing_frequency =$request->filing_frequency[$fil];
        //         $filing_frequency_due_date =$request->filing_frequency_due_date[$fil];
        //         $fedrelid;
        //         $fil++;
        //         DB::table('filingfrequencies')->where('t_id','=',$fedrelid)->delete();
        //         $insert2 = DB::insert("insert into filingfrequencies(`filing_frequency_form`,`filing_frequency_name`,`filing_frequency`,`filing_frequency_due_date`,`t_id`) values('".$filing_frequency_form."','".$filing_frequency_name."','".$filing_frequency."','".$filing_frequency_due_date."','".$fedrelid."')");

        //     }

        // } 


        // $pay=0;
        // foreach($request->filing_frequency_form1 as $filing_frequency_form1)
        // {
        //     $pid =$request->pid[$pay];
        //     $filing_frequency_form1 =$request->filing_frequency_form1[$pay];
        //     $filing_frequency_name1 =$request->filing_frequency_name1[$pay];
        //     $filing_frequency1 =$request->filing_frequency1[$pay];
        //     $filing_frequency_due_date1 =$request->filing_frequency_due_date1[$pay];
        //     $fedrelid;
        //     $pay++;
        //     $insert3 = DB::update("update paymentfrequency set filing_frequency_form1='".$filing_frequency_form1."',filing_frequency_name1='".$filing_frequency_name1."',filing_frequency1='".$filing_frequency1."' ,filing_frequency_due_date1='".$filing_frequency_due_date1."',t_id='".$fedrelid."' where id='".$pid."'");
        // } 


        // if($request->pid!='')
        // {
        //     $pay=0;
        //     foreach($request->filing_frequency_form1 as $filing_frequency_form1)
        //     {
        //         $pid =$request->pid[$pay];
        //         $filing_frequency_form1 =$request->filing_frequency_form1[$pay];
        //         $filing_frequency_name1 =$request->filing_frequency_name1[$pay];
        //         $filing_frequency1 =$request->filing_frequency1[$pay];
        //         $filing_frequency_due_date1 =$request->filing_frequency_due_date1[$pay];
        //         $fedrelid;
        //         $pay++;
        //         $insert3 = DB::update("update paymentfrequency set filing_frequency_form1='".$filing_frequency_form1."',filing_frequency_name1='".$filing_frequency_name1."',filing_frequency1='".$filing_frequency1."' ,filing_frequency_due_date1='".$filing_frequency_due_date1."',t_id='".$fedrelid."' where id='".$pid."'");
        //     } 
        // }
        // else
        // {

        //     $pay=0;
        //     foreach($request->filing_frequency_form1 as $filing_frequency_form1)
        //     {
        //         $filing_frequency_form1 =$request->filing_frequency_form1[$pay];
        //         $filing_frequency_name1 =$request->filing_frequency_name1[$pay];
        //         $filing_frequency1 =$request->filing_frequency1[$pay];
        //         $filing_frequency_due_date1 =$request->filing_frequency_due_date1[$pay];
        //         $id;
        //         $pay++;
        //         $insert3 = DB::insert("insert into paymentfrequency(`filing_frequency_form1`,`filing_frequency_name1`,`filing_frequency1`,`filing_frequency_due_date1`,`t_id`) values('".$filing_frequency_form1."','".$filing_frequency_name1."','".$filing_frequency1."','".$filing_frequency_due_date1."','".$fedrelid."')");
        //     }
        // }


        $position->update();
        return redirect('fac-Bhavesh-0554/taxfederal')->with('success', 'Success fully update Federal');
    }


    public function taxfederaldelete($id)
    {
        DB::table('filingfrequencies')->where('id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/taxfederal')->with('success', 'Success fully deleted Filing Frequency');
    }

    public function taxfederaldeletepay($id)
    {
        DB::table('paymentfrequency')->where('id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/taxfederal')->with('success', 'Success fully deleted Payment Frequency');
    }

    public function entityies()
    {
        $newopt = Input::get('newopt');
        //$sign = Input::get('sign');
        if (DB::table('typeofentity')->where('typeentity', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {

            $position = DB::insert('insert into typeofentity (typeentity) values (?)', array($newopt));

            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Federal $federal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        taxfederal::where('id', $id)->delete();
        //  return redirect(route('taxfederal.index'));
        return redirect('fac-Bhavesh-0554/taxfederal')->with('success', 'Success fully Deleted Federal');

    }
}