<?php

namespace App\Http\Controllers\Admin;

use App\employees\Fscemployee;
use App\Front\ApplyEmployment;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Holiday;
use App\Model\Language;
use App\Model\Logo;
use App\Model\Msg;
use App\Model\Position;
use App\Model\Rules;
use App\Model\Schedule;
use App\Model\Task;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Session;
use Validator;
use View;

class EmployeesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $position = Position::All();
        $branch = Branch::All();
        $logo = Logo::where('id', '=', 1)->first();
        $employee = Employee::where('type', '=', 'clientemployee')->orderBy('firstName', 'asc')->get();
        return view('fac-Bhavesh-0554.employees.employees', compact(['employee', 'position', 'branch', 'logo']));
    }


    public function edit($id, Request $request)
    {
        exit('122');
        // $super = DB::table('super')->groupBy('username')->get();
        $super = DB::table('employees')->join('super', function ($join) {
            $join->on('employees.id', '=', 'super.username')
                ->groupBy('super.username');
        })
            ->get();
        $newclient = Employee::where('id', $id)->update(['newemp' => 2]);
        $emp = Employee::where('id', $id)->first();
        $super1 = Employee::where('super', '=', '1')->get();
        $empfsc = Fscemployee::where('user_id', $id)->first();
        $position = Position::orderBy('position', 'asc')->get();
        $language = Language::orderBy('language_name', 'asc')->get();
        $holiday = Holiday::orderBy('holiday_name', 'asc')->get();

        $rules = Rules::All();
        //$branch = Branch::All();
        $branch = DB::table('branches')->select('city')->groupBy('city')->get();
        $info1 = DB::table('employee_pay_info')->where('employee_id', $id)->first();
        $employes = DB::table('employee_rules')->where('emp_id', $id)->get();
        $employes2 = DB::table('employee_rules')->where('emp_id', $id)->first();
        $info = DB::table('employee_pay_info')->where('employee_id', $id)->get();
        $info3 = DB::table('employee_other_info')->where('employee_id', $id)->get();
        $info2 = DB::table('employee_other_info')->where('employee_id', $id)->first();
        $review1 = DB::table('employee_review')->where('employee_id', $id)->get();
        $review = DB::table('employee_review')->where('employee_id', $id)->first();
        $admin_notes = DB::table('notes')->where('admin_id', '=', $id)->where('type', '=', $emp->type)->get();
        return View('fac-Bhavesh-0554.employee.edit', compact(['holiday', 'language', 'super', 'super1', 'admin_notes', 'rules', 'emp', 'info1', 'info', 'empfsc', 'info2', 'info3', 'review1', 'review', 'position', 'branch', 'employes', 'employes2']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Employment $employment
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

//echo "<pre>";print_r($_POST);print_r($_FILES);
        exit('111');
        //  echo $request->firstName;
        $this->validate($request, []);
        if ($request->check == '0') {
            $status = 0;
            $password1 = '';
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 0, 'name' => $name]);
            DB::table('employees')->where('id', $id)->update(['check' => 0]);
        } else {
            $status = '1';
            $email = $request->email;
            $name = $request->firstName . ' ' . $request->middleName . ' ' . $request->lastName;
            DB::table('fscemployees')->where('user_id', $id)->update(['type' => 1, 'name' => $name]);
            $password = $request->password;
            $cc = $request->go;
            $password1 = bcrypt($password);

            $user = Employee::where('id', '=', $id)->first();
            //echo $user->flag;exit;
            //echo "<pre>";print_r($user);exit;
            if ($user->flag == '0') {


                //$insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','1')");
                $data = array('email' => $email, 'firstName' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' => $password, 'type' => $cc);
                \Mail::send('fac-Bhavesh-0554/employe', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['firstName'], $data['password'], $data['type'])->subject('FSC Employee');
                });


            }


        }
        $pay_method = $request->pay_method;

        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee = $request->employee;
        $work = $request->work;
        $work_responsibility1 = $request->work_responsibility;
        $i = 0;
        $j = 0;
        $k = 0;
        // DB::table('employee_pay_info')->where('employee_id', $id)->first();
        $first_rev_day = $request->first_rev_day;
        $reviewmonth = $request->reviewmonth;
        $hiring_comments = $request->hiring_comments;
        $ree = $request->ree;

        DB::table('employee_review')->where('employee_id', $id)->first();
        foreach ($first_rev_day as $post) {
            $first_rev_day1 = $first_rev_day[$k];
            $reviewmonth1 = $reviewmonth[$k];
            $hiring_comments1 = $hiring_comments[$k];
            $ree1 = $ree[$k];
            $k++;
            DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            if (empty($ree1)) {

                $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('" . $id . "','" . $post . "','" . $reviewmonth1 . "','" . $hiring_comments1 . "')");
                //  DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            } else {
                //DB::table('employee_review')->where('id', $ree1)->delete();
                $returnValue = DB::table('employee_review')->where('id', '=', $ree1)
                    ->update(['employee_id' => $id,
                        'first_rev_day' => $first_rev_day1,
                        'reviewmonth' => $reviewmonth1,
                        'hiring_comments' => $hiring_comments1
                    ]);
                //DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
                //$affectedRows = employee_pay_info::where('first_rev_day', '=', '')->delete();
            }
        }

        $noteid = $request->noteid;
        $adminnotes = $request->adminnotes;
        $k = 0;
        $type = $request->types;
        $users = DB::table('notes')->where('admin_id', '=', $id)->first();
        foreach ($adminnotes as $notess) {
            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('" . $note1 . "','" . $id . "','" . $type . "')");
            } else {
                $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                    ->update(['notes' => $note1,
                        'admin_id' => $id,
                        'type' => $type
                    ]);
                $users = DB::table('notes')->where('notes', '=', '')->delete();
            }
        }

        $employee = Employee::find($id);
        $employee->employee_id = $request->employee_id;
        $employee->super = $request->super;
        $employee->firstName = $request->firstName;


        if (isset($request->languages)) {
            $employee->languages = implode(',', $request->languages);
        } else {
            $employee->languages = '';
        }
        if (isset($request->holidays)) {
            $employee->holidays = implode(',', $request->holidays);
        } else {
            $employee->holidays = '';
        }
        // print_r($request);
        $employee->type = $request->types;

        //  $employee->type= $request->type;
        // $employee->read= $request->terms;
        $employee->middleName = $request->middleName;
        $employee->lastName = $request->lastName;
        $employee->team = $request->team;
        $employee->flag = '1';


        $employee->address1 = $request->address1;
        $employee->address2 = $request->address2;
        $employee->city = $request->city;
        $employee->password = $request->password;
        $employee->stateId = $request->stateId;
        $employee->zip = $request->zip;
        $employee->countryId = $request->countryId;
        $employee->telephoneNo1 = $request->telephoneNo1;
        $employee->telephoneNo2 = $request->telephoneNo2;
        $employee->ext1 = $request->ext1;
        $employee->ext2 = $request->ext2;
        $employee->telephoneNo1Type = $request->telephoneNo1Type;
        $employee->telephoneNo2Type = $request->telephoneNo2Type;
        $employee->email = $request->email;
        $employee->fscemail = $request->fscemail;

        $employee->hiremonth = $request->hiremonth;
        $employee->hireday = $request->hireday;
        $employee->hireyear = $request->hireyear;
        $employee->termimonth = $request->termimonth;
        $employee->termiday = $request->termiday;
        $employee->termiyear = $request->termiyear;
        $employee->tnote = $request->tnote;
        $employee->tnote1 = $request->tnote1;
        $employee->rehiremonth = $request->rehiremonth;
        $employee->rehireday = $request->rehireday;
        $employee->rehireyear = $request->rehireyear;
        $employee->branch_city = $request->branch_city;
        $employee->branch_name = $request->branch_name;
        $employee->position = $request->position;
        $employee->note = $request->note;
        $employee->pay_method = $request->pay_method;
        $employee->pay_frequency = $request->pay_frequency;
        $employee->gender = $request->gender;
        $employee->marital = $request->marital;
        $employee->month = $request->month;
        $employee->day = $request->day;
        $employee->year = $request->year;
        $employee->pf1 = $request->pf1;
        $employee->pf2 = $request->pf2;
        $employee->epname = $request->epname;
        $employee->relation = $request->relation;
        $employee->eaddress1 = $request->eaddress1;
        $employee->ecity = $request->ecity;
        $employee->estate = $request->estate;
        $employee->ezipcode = $request->ezipcode;
        $employee->etelephone1 = $request->etelephone1;
        $employee->eteletype1 = $request->eteletype1;
        $employee->eext1 = $request->eext1;
        $employee->etelephone2 = $request->etelephone2;
        $employee->eteletype2 = $request->eteletype2;
        $employee->eext2 = $request->eext2;
        $employee->comments1 = $request->comments1;
        $employee->uname = $request->uname;
        $employee->question1 = $request->question1;
        $employee->answer1 = $request->answer1;
        $employee->question2 = $request->question2;
        $employee->answer2 = $request->answer2;
        $employee->question3 = $request->question3;
        $employee->answer3 = $request->answer3;
        $employee->other_info = $request->other_info;
        $employee->computer_name = $request->computer_name;
        $employee->computer_ip = $request->computer_ip;
        $employee->comments = $request->comments;
        $employee->check = $request->check;
        $employee->filling_status = $request->filling_status;
        $employee->fedral_claim = $request->fedral_claim;
        $employee->additional_withholding = $request->additional_withholding;

        if ($request->hasFile('additional_attach')) {
            $attach1 = $request->additional_attach->getClientOriginalName();
            $request->additional_attach->move('public/uploads', $attach1);

        } else {
            $attach1 = $request->file_name_2;
        }


        if ($request->hasFile('additional_attach_1')) {

            $attach2 = $request->additional_attach_1->getClientOriginalName();
            $request->additional_attach_1->move('public/uploads', $attach2);
        } else {
            $attach2 = $request->file_name_1;
        }

        if ($request->hasFile('additional_attach_2')) {

            $attach3 = $request->additional_attach_2->getClientOriginalName();
            $request->additional_attach_2->move('public/uploads', $attach3);
        } else {
            $attach3 = $request->file_name;
        }

        if ($request->hasFile('pfid1')) {
            $prfid1 = $request->pfid1->getClientOriginalName();
            $request->pfid1->move('public/uploads', $prfid1);

        } else {
            $prfid1 = $request->pfid1_name;
        }


        if ($request->hasFile('pfid2')) {
            $prfid2 = $request->pfid2->getClientOriginalName();
            $request->pfid2->move('public/uploads', $prfid2);

        } else {
            $prfid2 = $request->pfid2_name;
        }
        $employee->additional_attach = $attach1;
        $employee->additional_attach_1 = $attach2;
        $employee->additional_attach_2 = $attach3;
        $employee->pfid1 = $prfid1;
        $employee->pfid2 = $prfid2;


        $employee->state_claim = $request->state_claim;
        $employee->additional_withholding_1 = $request->additional_withholding_1;
        $employee->local_claim = $request->local_claim;
        $employee->additional_withholding_2 = $request->additional_withholding_2;
        $employee->type_agreement = $request->type_agreement;
        $employee->firstName_1 = $request->firstName_1;
        $employee->middleName_1 = $request->middleName_1;
        $employee->lastName_1 = $request->lastName_1;
        $employee->address11 = $request->address11;
        $employee->efax = $request->efax;
        $employee->fax = $request->fax;
        $employee->reset = $request->reset;
        $employee->resetdate = $request->reset_date;
        $employee->nametype = $request->nametype;
        $employee->eemail = $request->eemail;
        $employee->technical_support = $request->technical_support;
        $employee->timing_support = $request->timing_support;
        $employee->system_support = $request->system_support;
        $employee->other_support = $request->other_support;
        $employee->timesheet = $request->timesheet;
        $employee->status = $status;
        if ($request->check == '0') {
            $employee->check = $request->check;
        }
        $employee->type = $request->types;
        $employee->update();
        if ($employee->update()) {
            return redirect()->route('employees.edit', $id)->with('success', 'Successfully update Employee');

        } else {
            return redirect()->route('employees.edit', $id)->with('erroe', 'Error');
        }
        //return redirect()->route('employee.edit', $id)->with('success','Successfully update Employee');
    }


    public function destroy($id)
    {
        $ss = Employee::where('id', $id)->first();
        $k = $ss->email;
        ApplyEmployment::where('email', $k)->delete();
        Employee::where('id', $id)->delete();
        Schedule::where('emp_name', $id)->delete();
        Schedule::where('emp_name', $id)->delete();
        DB::table('empschedules')->where('employee_id', $id);
        Msg::where('employeeid', $id)->delete();
        Msg::where('admin_id', $id)->delete();
        Task::where('employeeid', $id)->delete();
        Fscemployee::where('user_id', $id)->delete();
        DB::table('employee_review')->where('employee_id', '=', $id)->first();
        return redirect(route('employees.index'))->with('success', 'Success Fully Delete Record');
    }


}