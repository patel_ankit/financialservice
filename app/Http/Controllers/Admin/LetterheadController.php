<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;

class LetterheadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        //  $bankdata = DB::table('bankmaster')->get();
        return view('fac-Bhavesh-0554.letterhead.index');
    }

    public function create()
    {
        //  $bankdata = DB::table('bankmaster')->get();
        return view('fac-Bhavesh-0554.letterhead.add');
    }

    public function edit()
    {
        //  $bankdata = DB::table('bankmaster')->get();
        return view('fac-Bhavesh-0554.letterhead.edit');
    }


}