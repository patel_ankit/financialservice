<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\SendEmail;
use DB;
use Illuminate\Http\Request;

class EmailSetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = SendEmail::All();
        $emp = Employee::All();
        $logo = Logo::where('id', '=', 1)->first();
        return view('fac-Bhavesh-0554/emailsetup/emailsetup', compact(['emp', 'email', 'logo']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $email = SendEmail::All();
        $emp1 = Employee::where('check', '=', 1)->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->orderBy('firstName', 'asc')->get();
        return view('fac-Bhavesh-0554/emailsetup/create', compact(['emp1', 'email']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'for_whom' =>'required',
            // 'email' =>'required',
        ]);
        $branch = new SendEmail;
        $branch->type = $request->type;
        $branch->userid = $request->employee;
        $branch->subject = $request->subject;
        $branch->description = $request->description;
        $branch->save();
        return redirect('fac-Bhavesh-0554/emailsetup')->with('success', 'Success fully add Email');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  $branch = Commonregister::where('id',$id)->first();
        //$position = Employee::All();
        $leave = SendEmail::where('id', $id)->first();
        $type = $leave->type;
        $emp = Employee::where('check', '=', 1)->where('type', '!=', 'clientemployee')->where('type', '!=', 'Vendor')->where('type', '!=', '')->where('type', '=', $type)->orderBy('firstName', 'asc')->get();;
        //$email = Email::All(); 
        return view('fac-Bhavesh-0554/emailsetup.edit', compact(['leave', 'emp']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = SendEmail::find($id);
        $branch->type = $request->type;
        $branch->userid = $request->employee;
        $branch->subject = $request->subject;
        $branch->description = $request->description;
        $branch->update();
        return redirect('fac-Bhavesh-0554/emailsetup')->with('success', 'Success fully Update Email');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SendEmail::where('id', $id)->delete();
        return redirect('fac-Bhavesh-0554/emailsetup');
    }

    public function getemails(Request $request)
    {

        $data2 = Employee::select('email', 'id')->where('id', $request->id)->take(100)->get();
        return response()->json($data2);
    }
}