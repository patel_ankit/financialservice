<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\empschedule;
use App\Model\Schedule;
use App\Model\Schedulesetup;
use DB;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = Branch::All();
        $schedule = Schedule::where('type', '!=', 'clientemployee')->get();


        $sch = DB::table('schedules')->select('schedules.id as cid', 'schedules.entity_id', 'schedules.type', 'schedules.emp_city', 'schedules.emp_name', 'schedules.duration', 'schedules.sch_start_date',
            'schedules.sch_end_date', 'schedules.schedule_in_time', 'schedules.schedule_out_time',
            'employees.id as eid', 'employees.firstName', 'employees.lastName', 'employees.middleName')
            ->leftJoin('employees', function ($join) {
                $join->on('employees.id', '=', 'schedules.emp_name');
            })
            ->where('employees.type', '!=', 'clientemployee')->where('employees.status', '=', '1')->orderBy('employees.firstName', 'asc')->get();

        //  echo "<pre>"; print_r($sch);
        $emp = Employee::where('status', '=', '1')->orderBy('firstName', 'asc')->get();
        $empschedule = empschedule::All();
        $schedulesetup = Schedulesetup::All();
        return view('fac-Bhavesh-0554.schedule.schedule', compact(['sch', 'branch', 'schedule', 'emp', 'empschedule', 'schedulesetup']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = Branch::All();
        return view('fac-Bhavesh-0554/schedule/create', compact('branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'emp_city' => 'required',
            'emp_name' => 'required',
            'duration' => 'required',
            'sch_start_date' => 'required',
        ]);
        $position = new Schedule;
        $position->emp_city = $request->emp_city;
        $position->type = 'employee';
        $position->emp_name = $request->emp_name;
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_end_date = $request->sch_end_date;
        $position->schedule_in_time = $request->schedule_in_time;
        $position->schedule_out_time = $request->schedule_out_time;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_day = $request->sch_end_day;
        $position->save();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;
        $date_to1 = $request->sch_end_date;
        $date_to = strtotime($date_to);

        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            $day = date("D", $i);
            if ($day == 'Sun') {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $lastId . "','" . $i . "','','','1')");
            } else if ($day == 'Sat') {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('" . $lastId . "','" . $i . "','','','1')");
            } else {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $lastId . "','" . $i . "','" . $date_from2 . "','" . $date_from3 . "')");
            }
        }
        return redirect('fac-Bhavesh-0554/schedule')->with('success', 'Schedule Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    public function employee(Request $request)
    {
        $data = Employee::select('firstName', 'lastName', 'id')->where('branch_city', $request->id)->where('type', 'employee')->where('check', '1')->take(100)->get();
        return response()->json($data);
    }

    public function getduration1(Request $request)
    {
        $data = Employee::select('pay_frequency', 'id')->where('id', $request->id)->first();
        $data1 = $data->pay_frequency;
        $data2 = Schedulesetup::select('duration', 'id', 'sch_start_date', 'sch_start_day', 'sch_end_date', 'sch_end_day')->where('duration', $data1)->take(100)->get();
        return response()->json($data2);
    }

    public function schedules(Request $request)
    {
        if ($request->companyId1 == '1' && !empty($request->companyId2)) {
            $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['schedule_status11' => 2, 'status' => 0, 'clockin' => $request->companyId2, 'clockout' => $request->companyId3]);
        } else {
            $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['schedule_status11' => 1, 'status' => 1, 'clockin' => '', 'clockout' => '']);
        }
        return response()->json($returnValue);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        $branch = Branch::All();
        $na = $schedule->emp_name;
        $na1 = $schedule->id;
        $duration = $schedule->duration;

        $emp = Employee::where('id', $na)->first();
        $empschedule = empschedule::where('emp_sch_id', $na1)->orderBy('id', 'asc')->get();
        // echo "<pre>";print_r($empschedule);exit;
        $schedulesetup = Schedulesetup::where('duration', '=', $duration)->first();
        return View('fac-Bhavesh-0554.schedule.edit', compact(['branch', 'schedule', 'emp', 'empschedule', 'schedulesetup']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        // echo "<pre>";return $request;

        // ECHO "<PRE>"; print_r($_POST);EXIT;
        $this->validate($request, [
            'emp_city' => 'required',
            'emp_name' => 'required',
            'duration' => 'required',
            'sch_start_date' => 'required',
        ]);
        $position = $schedule;
        $position->emp_city = $request->emp_city;
        $position->emp_name = $request->emp_name;
        $position->duration = $request->duration;
        $position->sch_start_date = $request->sch_start_date;
        $position->sch_end_date = $request->sch_end_date;
        $position->schedule_in_time = $request->schedule_in_time;
        $position->schedule_out_time = $request->schedule_out_time;
        $position->sch_start_day = $request->sch_start_day;
        $position->sch_end_day = $request->sch_end_day;
        $position->type = 'employee';
        $position->update();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;
        $date_to1 = $request->sch_end_date;
        $date_to = strtotime($date_to);
//$date_to4 = $request->schedule_date; 
//$date_to4 = strtotime($date_to4);
        $na1 = $schedule->id;
//$empschedule = empschedule::where('emp_sch_id',$na1)->where('date_1',$date_to4)->get();
        for ($i = $date_from; $i <= $date_to; $i += 86400) {
//$insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
//DB::table('schedule_emp_dates')->where('emp_sch_id', $lastId)->where('date_1','!=', $i)->delete();
        }
        $schedule_date = $request->schedule_date;
        $schedule_clockin = $request->schedule_in_time;
        $schedule_id = $request->schedule_id;
        $schedule_clockout = $request->schedule_out_time;
        $j = 0;
        foreach ($schedule_date as $post) {
            $schedule_date1 = $schedule_date[$j];
            $schedule_date1 = strtotime($schedule_date1);
            $day = date("D", $schedule_date1);
            $schedule_clockin1 = $schedule_clockin;
            $schedule_id1 = $schedule_id[$j];
            $schedule_clockout1 = $schedule_clockout;

            if ($day == 'Sun') {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => '',
                        'clockout' => '',
                    ]);
            } else if ($day == 'Sat') {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => '',
                        'clockout' => '',
                    ]);
            } else {
                $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
                    ->update(['date_1' => $schedule_date1,
                        'clockin' => $request->schedule_clockin[$j],
                        'clockout' => $request->schedule_clockout[$j],
                    ]);
            }
            //return $schedule_date1;
            $j++;
        }
        return redirect('fac-Bhavesh-0554/schedule')->with('success', 'Schedule Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return redirect(route('schedule.index'))->with('success', 'Delete Record Successfully');
    }


    public function Clockin(Request $request)
    {
        $student = empschedule::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }
}