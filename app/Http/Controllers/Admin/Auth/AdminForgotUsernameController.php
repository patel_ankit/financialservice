<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Http\Request;

class AdminForgotUsernameController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fac-Bhavesh-0554/forgotusername/forgotusername');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->email;
        $user = Admin::where('email', '=', $email)->first();
        if ($user === null) {
            return redirect(route('forgotusername.index'))->with('error', 'This Email not found');
        } else {
            $activation_link = route('forgotusername.edit', ['id' => $user->id, 'email' => $email, 'token' => urlencode($user->remember_token)]);
            $data = array('email' => $email, 'token' => $activation_link);
            \Mail::send('resetadminusername', $data, function ($message) use ($data) {
                $message->to($data['email'])->from($data['email'], $data['token'])->subject('Welcome!');
            });
            return redirect(route('forgotusername.index'))->with('success', 'Please Check Your Email');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = Admin::where('id', $id)->first();
        return View('forgotusername.index', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Admin::where('id', $id)->first();
        return view('fac-Bhavesh-0554/forgotusername/edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required',

        ]);
        $business = Admin::find($id);
        $business->email = $request->email;
        $business->update();
        return redirect(route('forgotusername.index'))->with('success', 'Your Username Successfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}