<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Position;
use DB;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = Position::All();
        return view('fac-Bhavesh-0554/position/position', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Position::All();
        return view('fac-Bhavesh-0554/position/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'position' => 'required',
        ]);
        $position = new Position;
        $position->position = $request->position;
        $position->save();
        return redirect('fac-Bhavesh-0554/position')->with('success', 'Success fully add Position');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::where('id', $id)->first();
        return view('fac-Bhavesh-0554.position.edit', compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'position' => 'required',
        ]);
        $position = Position::find($id);
        $position->position = $request->position;
        $position->update();
        return redirect('fac-Bhavesh-0554/position')->with('success', 'Success fully update Position');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Position::where('id', $id)->delete();
        return redirect(route('position.index'));
    }
}