<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use View;

class ProposalAcceptController extends Controller
{

    public function index()
    {

    }

    public function accept_proposal(Request $request)
    {
        $id = $request->id;
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id', $id)->Where('status', 0)->first();
        $counts = $getProposal->id;

        $query = DB::table('proposal_client_detail')
            ->where('id', $counts)
            ->update(array('status' => 1));

        echo $query;
        exit();

    }

    public function getproposalDetailsForAccept($id)
    {
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id', $id)->Where('status', 0)->first();
        return view('prospect/', compact(['id', 'json']));
    }
}