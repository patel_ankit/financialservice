<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'admin':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('fac-Bhavesh-0554.dashboard');
                }
                break;
            case 'employee':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('fscemployee.dashboard');
                }
                break;
            case 'submission':
                if (Auth::guard($guard)->check()) {
                    return redirect('submission/dashboard');
                    // return redirect()->route('submission.dashboard');
                }
                break;

            case 'client':
                if (Auth::guard($guard)->check()) {
                    return redirect('clientemployee');
                    // return redirect()->route('submission.dashboard');
                }
                break;
            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('client/home');
                }
                break;
        }
        return $next($request);
    }
}