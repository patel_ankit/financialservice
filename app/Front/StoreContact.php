<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class StoreContact extends Model
{
    protected $fillable = ['name', 'email', 'telephone', 'message', 'captcha'];
}