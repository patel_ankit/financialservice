<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class Commonregister extends Model
{
    protected $fillable = ['register_id', 'company_name', 'business_name', 'first_name', 'middle_name', 'last_name', 'email', 'address', 'address2', 'city', 'stateId', 'zip', 'countryId', 'mobile_no', 'business_no', 'business_fax', 'website', 'user_type', 'business_id', 'business_cat_id', 'business_brand_id', 'business_brand_category_id', 'filename', 'mailing_address', 'mailing_address1', 'mailing_city', 'mailing_state', 'mailing_zip', 'legalname', 'dbaname', 'bussiness_zip', 'business_state', 'business_city', 'business_country', 'business_address', 'business_store_name', 'level', 'setup_state', 'county_name', 'county_no', 'type_of_activity', 'department', 'due_date', 'personname', 'relation', 'eaddress1', 'per_city', 'per_stateId', 'per_zip', 'etelephone1', 'eteletype1', 'eext1', 'eext2', 'eteletype2', 'etelephone2', 'comments', 'marital', 'month', 'year', 'day', 'pf1', 'pf2', 'eteletype3', 'eteletype4', 'gender', 'count'];
}