<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class ApplyEmployment extends Model
{
    protected $fillable = ['id', 'fname', 'mname', 'lname', 'address1', 'address2', 'country', 'state', 'city', 'zip', 'telephone1', 'telephone1_type', 'telephone2', 'telephone2_type', 'email', 'position', 'resume', 'employment_id', 'status', 'candidate_date'];
}