<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddLogo extends Model
{
    protected $fillable = ['logo_text', 'logo_image', 'status'];
}