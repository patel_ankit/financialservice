<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAjaxImageTabel extends Migration
{

    public function up()

    {

        Schema::create('ajax_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('image');
            $table->string('content');
            $table->integer('status');
            $table->timestamps();
        });

    }

    public function down()

    {

        Schema::drop("ajax_images");

    }

}