// JavaScript Document

j('document').ready(function () {

    formmodified = 0;
    j('form *').change(function () {
        formmodified = 1;
    });
    window.onbeforeunload = confirmExit1;

    function confirmExit1() {
        if (formmodified == 1) {
            return "New information not saved. Do you wish to leave the page?";
        }
    }

    j("input[name='save']").click(function () {
        formmodified = 0;
    });

    /* validation */
    j("#changepwd").validate({


        rules:
            {
                opassword: {
                    required: true,
                    equalTo: '#oldpwd'
                },
                password: {
                    required: true,
                },
                cpassword: {
                    required: true,
                    equalTo: '#password',
                },

            },
        messages:
            {
                opassword: {
                    required: "Please Enter Old Password",
                    equalTo: "Old Password values do not match"
                },
                password: {
                    required: "Please enter Password",

                },
                cpassword: {
                    required: "Please enter Re-Enter Password",
                    equalTo: "Password values do not match"
                },

            },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm() {
        var data = j("#changepwd").serialize();

        j.ajax({

            type: 'POST',
            url: 'changepwd.php',
            data: data,
            beforeSend: function () {
                j("#error").fadeOut();
                j("#save").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success: function (data) {
                if (data == "length") {

                    j("#error").fadeIn(1000, function () {


                        j("#error").html('<div class="alert alert-warning"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Password Must Be Eight Character Long !</div>');

                        j("#save").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');

                    });

                } else if (data == "success") {
                    j("#error").fadeIn(1000, function () {

                        j("#error").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Password Successfully Change!</div>');
                        window.location = "index.php";
                    });
                } else {

                    j("#error").fadeIn(1000, function () {

                        j("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' + data + ' !</div>');

                        j("#save").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Submit');

                    });

                }
            }
        });
        return false;
    }

    /* form submit */
});

function checkPassword() {
    var count = j("#pcount").val();
    ajaxUpdate("password.php", {action: "passwordChk", count: count}, function (data) {

        if (data.type == 'success') {
            j("#notif_pass").css({'display': 'none'});
        } else
            j("#notif_pass").css({'display': 'block'});
    });
}