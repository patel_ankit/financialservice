<!-- jquery-steps css -->
<link rel="stylesheet" href="vendors/jquery-steps/demo/css/jquery.steps.css">

<!-- Toggles CSS -->
<link href="vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
<link href="vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="dist/css/style.css" rel="stylesheet" type="text/css">

<!-- Breadcrumb -->
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="#">Forms</a></li>
        <li class="breadcrumb-item active" aria-current="page">Form Wizard</li>
    </ol>
</nav>
<!-- /Breadcrumb -->

<!-- Container -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <div id="example-basic">
                <h3>
                    <span class="wizard-icon-wrap"><i class="ion ion-md-basket"></i></span>
                    <span class="wizard-head-text-wrap">
									<span class="step-head">Review Cart</span>
								</span>
                </h3>
                <section>
                    <div class="row">
                        <div class="col-xl-12 pa-0">
                            <div class="emailapp-wrap">
                                <div class="emailapp-sidebar">
                                    <div class="nicescroll-bar">
                                        <div class="emailapp-nav-wrap">
                                            <a id="close_emailapp_sidebar" href="javascript:void(0)" class="close-emailapp-sidebar">
                                                <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                            </a>
                                            <ul class="nav flex-column mail-category">
                                                <li class="nav-item active">
                                                    <a class="nav-link" href="javascript:void(0);">Inbox<span class="badge badge-primary">50</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);">Sent mail</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);">Important</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);">Draft<span class="badge badge-secondary">5</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);">Trash</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);">Chat</a>
                                                </li>
                                            </ul>
                                            <button type="button" class="btn btn-primary btn-block mt-20 mb-20" data-toggle="modal" data-target="#exampleModalEmail">
                                                Compose email
                                            </button>
                                            <ul class="nav flex-column mail-labels">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);"><span class="badge badge-primary badge-indicator"></span>clients</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);"><span class="badge badge-danger badge-indicator"></span>personal</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);"><span class="badge badge-warning badge-indicator"></span>office</a>
                                                </li>
                                            </ul>
                                            <hr>
                                            <ul class="nav flex-column mail-settings">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="javascript:void(0);"><i class="zmdi zmdi-settings"></i>settings</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="email-box">
                                    <div class="emailapp-left">
                                        <header>
                                            <a href="javascript:void(0)" id="emailapp_sidebar_move" class="emailapp-sidebar-move">
                                                <span class="feather-icon"><i data-feather="menu"></i></span>
                                            </a>
                                            <span class="">Inbox</span>
                                            <a href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                                <span class="feather-icon"><i data-feather="edit"></i></span>
                                            </a>
                                        </header>
                                        <form role="search" class="email-search">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="feather-icon"><i data-feather="search"></i></span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Search">
                                            </div>
                                        </form>
                                        <div class="emailapp-emails-list">
                                            <div class="nicescroll-bar">
                                                <a href="javascript:void(0);" class="media">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar1.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 1</div>
                                                            <!-- <div class="email-subject">Creation timelines for the standard lorem ipsum</div>
                                                            <div class="email-text">
                                                                <p>So how did the classical Latin become so incoherent? According to McClintock.</p>
                                                            </div> -->
                                                        </div>
                                                        <div>
                                                            <!--  <div class="last-email-details"><span class="badge badge-danger badge-indicator"></span> 2:30 PM</div>
                                                             <span class="email-star"><span class="feather-icon"><i data-feather="star"></i></span></span> -->
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar2.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 2</div>
                                                            <!-- <div class="email-subject">McClintock wrote to Before & After to explain his discovery</div>
                                                            <div class="email-text">
                                                                <p>“What I find remarkable is that this text has been the industry's standard dummy text ever since some printer.</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media read-email">
                                                    <!--  <div class="media-img-wrap">
                                                         <div class="avatar">
                                                             <img src="dist/img/avatar3.jpg" alt="user" class="avatar-img rounded-circle">
                                                         </div>
                                                     </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 3</div>
                                                            <!--  <div class="email-subject">Whether a medieval typesetter</div>
                                                             <div class="email-text">
                                                                 <p>As an alternative theory, and because Latin scholars do this sort of thing someone tracked down a 1914 Latin edition of De Finibus which challenges.</p>
                                                             </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                                <a href="javascript:void(0);" class="media  read-email">
                                                    <!-- <div class="media-img-wrap">
                                                        <div class="avatar">
                                                            <img src="dist/img/avatar4.jpg" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div> -->
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="email-head">Menu 4</div>
                                                            <!-- <div class="email-subject">Purposefully designed to have no meaning</div>
                                                            <div class="email-text">
                                                                <p>Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China"</p>
                                                            </div> -->
                                                        </div>
                                                        <div>

                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="email-hr-wrap">
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="emailapp-right">
                                        <header>
                                            <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                                <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                            </a>
                                            <div class="email-options-wrap">
                                                <a href="javascript:void(0)" class=""><span class="feather-icon"><i data-feather="printer"></i></span></a>
                                                <a href="javascript:void(0)" class=""><span class="feather-icon"><i data-feather="trash"></i></span></a>
                                                <a href="javascript:void(0)" class="text-smoke"><span class="feather-icon"><i data-feather="more-vertical"></i></span></a>
                                            </div>
                                        </header>
                                        <div class="container-fluid">
                                            <!-- Title -->
                                            <div class="hk-pg-header" style="    background: #fff;
                                        margin: 10px 0px 0px 0px;
                                        padding: 10px 10px 5px;">
                                                <div>
                                                    <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i data-feather="book"></i></span></span>Invoice</h4>
                                                </div>
                                                <div class="d-flex">
                                                    <a href="#" class="text-secondary mr-15"><span class="feather-icon"><i data-feather="printer"></i></span></a>
                                                    <a href="#" class="text-secondary mr-15"><span class="feather-icon"><i data-feather="download"></i></span></a>
                                                    <button class="btn btn-primary btn-sm">Create New</button>
                                                </div>
                                            </div>
                                            <!-- /Title -->

                                            <!-- Row -->
                                            <div class="row">
                                                <div class="col-xl-12">

                                                    <section class="hk-sec-wrapper hk-invoice-wrap pa-35">
                                                        <div class="invoice-from-wrap">
                                                            <div class="row">
                                                                <div class="col-md-7 mb-20">
                                                                    <img class="img-fluid invoice-brand-img d-block mb-20" src="dist/img/invoice-logo.png" alt="brand"/>
                                                                    <h6 class="mb-5">Hencework Inc</h6>
                                                                    <address>
                                                                        <span class="d-block">4747, Pearl Street</span>
                                                                        <span class="d-block">Rainy Day Drive, Washington</span>
                                                                        <span class="d-block">Griffin@hencework.com</span>
                                                                    </address>
                                                                </div>

                                                                <div class="col-md-5 mb-20">
                                                                    <h4 class="mb-35 font-weight-600">Invoice / Receipt</h4>
                                                                    <span class="d-block">Date:<span class="pl-10 text-dark">Nov 17,2017 11:23 AM</span></span>
                                                                    <span class="d-block">Invoice / Receipt #<span class="pl-10 text-dark">21321434</span></span>
                                                                    <span class="d-block">Customer #<span class="pl-10 text-dark">321434</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr class="mt-0">
                                                        <div class="invoice-to-wrap pb-20">
                                                            <div class="row">
                                                                <div class="col-md-7 mb-30">
                                                                    <span class="d-block text-uppercase mb-5 font-13">billing to</span>
                                                                    <h6 class="mb-5">Supersonic Co.</h6>
                                                                    <address>
                                                                        <span class="d-block">Sycamore Street</span>
                                                                        <span class="d-block">San Antonio Valley, CA 34668</span>
                                                                        <span class="d-block">thompson_peter@super.co</span>
                                                                        <span class="d-block">ABC:325487</span>
                                                                    </address>
                                                                </div>
                                                                <div class="col-md-5 mb-30">
                                                                    <span class="d-block text-uppercase mb-5 font-13">Payment info</span>
                                                                    <span class="d-block">Scott L Thompson</span>
                                                                    <span class="d-block">MasterCard#########1234</span>
                                                                    <span class="d-block">Customer #<span class="text-dark">324148</span></span>
                                                                    <span class="d-block text-uppercase mt-20 mb-5 font-13">amount due</span>
                                                                    <span class="d-block text-dark font-18 font-weight-600">$22,010</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h5>Items</h5>
                                                        <hr>
                                                        <div class="invoice-details">
                                                            <div class="table-wrap">
                                                                <div class="table-responsive">
                                                                    <table class="table table-striped table-border mb-0">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="w-70">Items</th>
                                                                            <th class="text-right">Number</th>
                                                                            <th class="text-right">Unit Cost</th>
                                                                            <th class="text-right">Amount</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="w-70">Design Service</td>
                                                                            <td class="text-right">2</td>
                                                                            <td class="text-right">$1500</td>
                                                                            <td class="text-right">$3000</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="w-70">Website Development</td>
                                                                            <td class="text-right">1</td>
                                                                            <td class="text-right">$7500</td>
                                                                            <td class="text-right">$7500</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="w-70">Social Media Services</td>
                                                                            <td class="text-right">15</td>
                                                                            <td class="text-right">$180</td>
                                                                            <td class="text-right">$9000</td>
                                                                        </tr>
                                                                        <tr class="bg-transparent">
                                                                            <td colspan="3" class="text-right text-light">Subtotals</td>
                                                                            <td class="text-right">$19,500</td>
                                                                        </tr>
                                                                        <tr class="bg-transparent">
                                                                            <td colspan="3" class="text-right text-light border-top-0">Tax</td>
                                                                            <td class="text-right border-top-0">$3510</td>
                                                                        </tr>
                                                                        <tr class="bg-transparent">
                                                                            <td colspan="3" class="text-right text-light border-top-0">Discount</td>
                                                                            <td class="text-right border-top-0">$1000</td>
                                                                        </tr>
                                                                        </tbody>
                                                                        <tfoot class="border-bottom border-1">
                                                                        <tr>
                                                                            <th colspan="3" class="text-right font-weight-600">total</th>
                                                                            <th class="text-right font-weight-600">$22,010</th>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="invoice-sign-wrap text-right py-60">
                                                                <img class="img-fluid d-inline-block" src="dist/img/signature.png" alt="sign"/>
                                                                <span class="d-block text-light font-14">Digital Signature</span>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <ul class="invoice-terms-wrap font-14 list-ul">
                                                            <li>A buyer must settle his or her account within 30 days of the date listed on the invoice.</li>
                                                            <li>The conditions under which a seller will complete a sale. Typically, these terms specify the period allowed to a buyer to pay off the amount due.</li>
                                                        </ul>
                                                    </section>
                                                </div>
                                            </div>
                                            <!-- /Row -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <h3>
                    <span class="wizard-icon-wrap"><i class="ion ion-md-airplane"></i></span>
                    <span class="wizard-head-text-wrap">
									<span class="step-head">Shipping Address</span>
								</span>
                </h3>
                <section>
                    <h3 class="display-4 mb-40">Select a delivery address</h3>
                    <div class="row">
                        <div class="col-xl-4 mb-20">
                            <p class="mb-10">Donec at sapien aliquet nulla vulputate posuere. Ut sagittis nisl non tristique consectetur. Nullam et est orci.</p>
                            <p><a href="#">Ubique veritus mediocrem</a> Aliquam luctus viverra enim, ut dapibus nunc condimentum tempor.</p>
                            <div class="card mt-30">
                                <div class="card-body bg-light">
                                    <h5 class="card-title">Madalyn Shane</h5>
                                    <p class="card-text">1234 Main St xyz, Sacremento, 12 Riverside Drive Redding, Union Street, CA-961001, US</p>
                                    <a href="#" class="btn btn-block btn-primary">Deliver here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 mb-20">
                            <form>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="firstName">First name</label>
                                        <input class="form-control" id="firstName" placeholder="" value="Madalyn" type="text">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="lastName">Last name</label>
                                        <input class="form-control" id="lastName" placeholder="" value="Shane" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">@</span>
                                        </div>
                                        <input class="form-control" id="username" placeholder="Username" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" id="email" placeholder="you@example.com" type="email">
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input class="form-control" id="address" placeholder="1234 Main St" value="1234 Main St xyz" type="text">
                                </div>

                                <div class="form-group">
                                    <label for="address">Address 2(Optional)</label>
                                    <input class="form-control" id="address2" placeholder="1234 Main St" type="text">
                                </div>

                                <div class="row">
                                    <div class="col-md-5 mb-10">
                                        <label for="country">Country</label>
                                        <select class="form-control custom-select d-block w-100" id="country">
                                            <option value="">Choose...</option>
                                            <option selected>United States</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 mb-10">
                                        <label for="state">State</label>
                                        <select class="form-control custom-select d-block w-100" id="state">
                                            <option value="">Choose...</option>
                                            <option selected>California</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="zip">Zip</label>
                                        <input class="form-control" id="zip" value="19100" type="text">
                                    </div>
                                </div>
                                <hr>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="same-address" type="checkbox" checked>
                                    <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="save-info" type="checkbox">
                                    <label class="custom-control-label" for="save-info">Save this information for next time</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <h3>
                    <span class="wizard-icon-wrap"><i class="ion ion-md-card"></i></span>
                    <span class="wizard-head-text-wrap">
									<span class="step-head">Payment Method</span>
								</span>
                </h3>
                <section>
                    <h3 class="display-4 mb-40">Choose payment method</h3>
                    <div class="row">
                        <div class="col-xl-5 mb-20">
                            <p>The most common alternative payment methods are debit cards, charge cards, prepaid cards, direct debit, bank transfers, phone and mobile payments, checks, money orders and cash payments.</p>
                            <ul class="nav nav-tabs mt-30 mb-25">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Card payment</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Netbanking</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Paypal</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active">
                                    <h6 class="my-15"><i class="ion ion-md-card text-grey pr-10"></i>Stored card</h6>
                                    <div class="card bg-light">
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between mb-25">
                                                <span class="font-14 d-block font-weight-600 text-uppercase mb-10">Mastercard</span>
                                                <img src="dist/img/mastercard.png" alt="card"/>
                                            </div>
                                            <span class="d-block text-dark font-20 letter-spacing-20 font-weight-600 ">5678 **** **** 4321</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 mb-20">
                            <form>
                                <div class="d-flex align-items-center mb-30">
                                    <span class="font-12 pr-15 text-dark text-uppercase font-weight-600">We accept</span>
                                    <img class="mr-15" src="dist/img/card-visa.png" alt="card"/>
                                    <img class="mr-15" src="dist/img/card-mc.png" alt="card"/>
                                    <img src="dist/img/card-ae.png" alt="card"/>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="cc-name">Name on card</label>
                                        <input class="form-control" id="cc-name" placeholder="" type="text">
                                        <small class="form-text text-muted">Full name as displayed on card</small>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="cc-number">Credit card number</label>
                                        <input class="form-control" id="cc-number" placeholder="" data-mask="9999-9999-9999-9999" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="cc-expiration">Expiry</label>
                                        <input class="form-control" id="cc-expiration" placeholder="" data-mask="99-99" type="text">
                                        <div class="invalid-feedback">
                                            Expiration date required
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="cc-cvv">CVV</label>
                                        <input class="form-control" id="cc-cvv" data-mask="999" placeholder="" type="text">
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox checkbox-success mb-15">
                                    <input class="custom-control-input" id="same-card" type="checkbox" checked>
                                    <label class="custom-control-label" for="same-card">Securely save this card for faster checkout next time</label>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Pay $431</button>
                                <small class="form-text text-muted">Card details will be saved securely as per industry standard</small>
                            </form>
                        </div>
                    </div>
                </section>
                <h3>
                    <span class="wizard-icon-wrap"><i class="ion ion-md-checkmark-circle-outline"></i></span>
                    <span class="wizard-head-text-wrap">
									<span class="step-head">Place Order</span>
								</span>
                </h3>
                <section>
                    <h3 class="display-4 mb-40">Order summary</h3>
                    <div class="row">
                        <div class="col-xl-8 mb-20">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-sm mb-0">
                                        <tbody>
                                        <tr>
                                            <th class="w-70" scope="row">Sub Total</th>
                                            <th class="w-30" scope="row">$859</th>
                                        </tr>
                                        <tr>
                                            <td class="w-70">Offers Discount</td>
                                            <td class="w-30">$89</td>
                                        </tr>
                                        <tr>
                                            <td class="w-70">Packging charges</td>
                                            <td class="w-30">$8</td>
                                        </tr>
                                        <tr>
                                            <td class="w-70">Tax</td>
                                            <td class="w-30">18%</td>
                                        </tr>
                                        <tr>
                                            <td class="w-70 text-success">Delivery charges</td>
                                            <td class="w-30 text-success">Free</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr class="bg-light">
                                            <th class="text-dark text-uppercase" scope="row">To Pay</th>
                                            <th class="text-dark font-18" scope="row">$1245</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <textarea class="form-control mt-35" rows="2" placeholder="Any suggestions? We will pass it on.."></textarea>
                        </div>
                        <div class="col-xl-4 mb-20">
                            <div class="alert alert-success mb-20" role="alert">
                                You have saved $133 on the bill.
                            </div>
                            <p class="mb-10">Prime shipping benifits have been applied to your order. Rewards points will be charged when you placed your order.</p>
                            <a class="d-block mb-25" href="#">How are shipping cost calculated?</a>
                            <button class="btn btn-primary btn-block mb-10" type="submit">Place your order</button>
                            <small class="d-block text-center">By placing your order, you agree to our <a href="#">terms and conditions</a> to use.</small>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->